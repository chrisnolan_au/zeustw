# Values: DOUBLE, SIMPLE 
PRECISION=DOUBLE

# Values: DEBUG (for debug compilations)
#         HIGH (for production in some machines)
#         MEDIUM (for production in other machines)
OPTIMIZE=HIGH

# Machine: singleprocessor, tiara, hyades
MACHINE=HYDRA

# Compiler vendor: intel, gnu
VENDOR=intel

#Problem-dependent libraries
EXTRA_LIBRARIES=
#EXTRA_LIBRARIES=-lfftw3 -L /home/ruben/lib

#Problem-dependent include path
EXTRA_INCLUDE=
#EXTRA_INCLUDE=-I /home/ruben/include

# Compiler script: fortran_compile, empty
COMPILER_SCRIPT=empty

# Compiler and linker flags
ifeq ($(VENDOR),intel)
 libraries=-limf -lm
 LINKERFLAGS=
 FCOMPILERFLAGS=-c
 FCFLAGS=-assume minus0 -free -fpp -implicitnone \
         -nopad -extend-source 132 \
         -assume nobuffered_io \
         -names lowercase -nomixed_str_len_arg \
         -DF2003_ALLOCATABLES \
         -DFPCLASS -UMEMORY_DEBUG \
         -noaltparam
        # -assume std_intent_in
        # -fno-alias -ansi-alias -fno-fnalias
        # -ccdefault none
        # -ccdefault list
        # -ccdefault default
        #-fp-model strict vs -fast-transcendentals, -fma,
 CCFLAGS=-UNOUNDERSCORE
         # -ansi-alias -fno-alias
 ifeq ($(OPTIMIZE),HIGH)
  FCOPTION = -ipo -O3
# -profile-functions -fast
  CCOPTION = -ipo -O3
# -profile-functions
 endif
 ifeq ($(OPTIMIZE),MEDIUM)
  FCOPTION=-O
  CCOPTION=-O
 endif
 ifeq ($(OPTIMIZE),DEBUG)
  FCOPTION=-g -check all -debug all -debug extended \
           -debug-parameters all \
           -fltconsistency \
           -fpscomp none -fp-stack-check -ftrapuv -stand f95 \
           -traceback \
           -warn all \
           -IPF-fltacc -IPF-flt-eval-method0 -IPF-fp-speculationoff \
           -assume fpe_summary
	   # -profile-functions -profile-loops=all -profile-loops-report=2 
           # -diag-enable sc
           # -fpe0
           # -mp
           # -debug-parameters used
  CCOPTION=-g -fpstkchk -ftrapuv -strict-ansi -traceback -Wall 
           # -mp
 endif
 ifeq ($(PRECISION),DOUBLE)
  FCDEFAULTREALKIND=-r8
  FCFP=-DDOUBLEP -pc64
 else
  FCDEFAULTREALKIND=
  FCFP=-UDOUBLEP -pc32
 endif
 ifeq ($(MACHINE),singleprocessor)
   FCOMPILER=ifort
 endif
 CCOMPILER=icc
endif

ifeq ($(VENDOR),gnu)
 FCOMPILERFLAGS=-c -xf95-cpp-input -fno-underscoring
 FCFLAGS=-fimplicit-none -ffree-form -ffree-line-length-none \
  -fmax-identifier-length=63 \
  -UF2003_ALLOCATABLES -UFPCLASS -UMEMORY_DEBUG
 CCFLAGS=-DNOUNDERSCORE
 ifeq ($(OPTIMIZE),HIGH)
  FCOPTION=-O3
  # -funroll-loops -ffast-math -fno-math-errno
  # -funsafe-math-optimizations -ffinite-math-only -fno-trapping-math -funswitch-loops 
  CCOPTION=-O3
 endif
 ifeq ($(OPTIMIZE),MEDIUM)
  FCOPTION=-O
  CCOPTION=-O
 endif
 ifeq ($(OPTIMIZE),DEBUG)
 FCOPTION=-g -W -pedantic -Wall -Wconversion \
  -Wimplicit-interface -Wunderflow \
  -O -Wuninitialized -std=f95 -fbounds-check
 endif
 ifeq ($(PRECISION),DOUBLE)
  FCDEFAULTREALKIND=-fdefault-real-8
  FCFP=-DDOUBLEP
 else
  FCDEFAULTREALKIND=
  FCFP=-UDOUBLEP
 endif
 ifeq ($(MACHINE),singleprocessor)
   FCOMPILER=gfortran
  endif
  CCOMPILER=gcc
#-fdefault-integer-8 -fcray-pointer
#-fno-underscoring -fsecond-underscore -fmax-stack-var-size=n
endif

#ifeq ($(MACHINE),tungsten)
#  FCOMPILER= cmpif90c -DMPI
#endif
ifeq ($(MACHINE),tiara)
  FCOMPILER= mpif90 -DMPI
endif
ifeq ($(MACHINE),TIARA)
  FCOMPILER= mpif90 -DMPI
endif
ifeq ($(MACHINE),hyades)
  FCOMPILER= mpif90 -DMPI
endif
ifeq ($(MACHINE),HYADES)
  FCOMPILER= mpif90 -DMPI
endif
ifeq ($(MACHINE),HYDRA)
  FCOMPILER= mpiifort -DMPI
endif
#ifeq ($(MACHINE),laptop)
#  FCOMPILER= mpif90 -DMPI
#endif
#ifeq ($(MACHINE),lam)
#  FCOMPILER= mpif77 -DMPI
#endif

# Values: defaultrealkinds, explicitrealkinds
#
# Most Fortran compilers of today allow choosing the default real kind
# at the compilation line.  If that's not available, then explicit
# real kinds can be given
#
#
# The explicitrealkinds setup is not perfectly equivalent in I/O
# to the (recommended) defaultrealkinds.

SYSTEM=defaultrealkinds

ifeq ($(SYSTEM),defaultrealkinds)
    FCPRECISION=-DDEFAULTKINDS $(FCDEFAULTREALKIND) $(FCFP)
endif

ifeq ($(SYSTEM),explicitrealkinds)
    FCPRECISION=$(FCFP)
endif

LINKER = $(FCOMPILER) $(FCFLAGS) $(FCOPTION) $(FCPRECISION) $(LINKERFLAGS)
CC = $(CCOMPILER) $(CCFLAGS) $(CCOPTION) $(EXTRA_INCLUDE)
FC =  $(FCOMPILER) $(FCFLAGS) $(FCOPTION) $(FCPRECISION) $(FCOMPILERFLAGS) $(EXTRA_INCLUDE)

sources = \
zeus_variables_fixed_hardwired.f90 \
zeus_variables_fixed_namelists.f90 \
zeus_variables_fixed_other.f90 \
zeus_variables_changing.f90 \
zeus_variables_grid.f90 \
zeus_variables.f90 \
zeus_deadcode.f90 \
zeus_gravity.f90 \
zeus_io.f90 \
zeus_timer.f90 \
zeus_iochecks.f90 \
zeus_memory.f90 \
zeus_interpolators.f90 \
zeus_physics.f90 \
zeus_chemistry.f90 \
zeus_mpi.f90 \
zeus_grid_moving.f90 \
zeus_initial.f90 \
zeus_boundary.f90 \
zeus_grid_initial.f90 \
problem_variables.f90 \
problem_description.f90 \
problem_boundary.f90 \
problem_initial.f90 \
problem_final.f90 \
zeus.f90

variables = \
zeus_variables_fixed_hardwired.o \
zeus_variables_fixed_namelists.o \
zeus_variables_fixed_other.o \
zeus_variables_changing.o \
zeus_variables_grid.o \
zeus_variables.o

zeus = \
zeus_deadcode.o \
zeus_gravity.o \
zeus_io.o \
zeus_timer.o \
zeus_iochecks.o \
zeus_memory.o \
zeus_interpolators.o \
zeus_physics.o \
zeus_chemistry.o \
zeus_mpi.o \
zeus_grid_moving.o \
zeus_initial.o \
zeus_boundary.o \
zeus_grid_initial.o

problem = \
problem_variables.o \
problem_description.o \
problem_boundary.o \
problem_initial.o \
problem_final.o

variables_mod = \
zeus_variables_fixed_hardwired.mod \
zeus_variables_fixed_namelists.mod \
zeus_variables_fixed_other.mod \
zeus_variables_changing.mod \
zeus_variables_grid.mod \
zeus_variables.mod

zeus_mod = \
zeus_deadcode.mod \
zeus_gravity.mod \
zeus_io.mod \
zeus_timer.mod \
zeus_iochecks.mod \
zeus_memory.mod \
zeus_interpolators.mod \
zeus_physics.mod \
zeus_chemistry.mod \
zeus_mpi.mod \
zeus_grid_moving.mod \
zeus_initial.mod \
zeus_boundary.mod \
zeus_grid_initial.mod

problem_mod = \
problem_variables.mod \
problem_description.mod \
problem_boundary.mod \
problem_initial.mod \
problem_final.mod

modules = $(variables_mod) $(zeus_mod) $(problem_mod)

objects = $(variables) $(zeus) $(problem) zeus.o zeus_c.o

ifeq ($(MACHINE),lam)
  modules = $(variables_mod) $(zeus_mod) $(problem_mod) mpi.mod
  objects = $(variables) $(zeus) $(problem) zeus.o zeus_c.o mpi.o
endif

xzeus : $(objects) $(sources) macros makefile zeus_c.c zeus_c.h
	$(LINKER) -o xzeus $(objects) $(libraries) $(EXTRA_LIBRARIES) &> link.output

$(objects) : macros makefile
$(modules) : macros makefile

mpi.o : mpi.f90
mpi.mod : mpi.f90

zeus_c.o : zeus_c.c zeus_c.h makefile
	$(CC) -c zeus_c.c &> zeus_c.output

zeus_variables.o                   : zeus_variables.f90 \
                                     zeus_variables_fixed_hardwired.mod \
                                     zeus_variables_fixed_namelists.mod \
                                     zeus_variables_fixed_other.mod \
                                     zeus_variables_grid.mod \
                                     zeus_variables_changing.mod
zeus_variables.mod                 : zeus_variables.f90 \
                                     zeus_variables_fixed_hardwired.mod \
                                     zeus_variables_fixed_namelists.mod \
                                     zeus_variables_fixed_other.mod \
                                     zeus_variables_grid.mod \
                                     zeus_variables_changing.mod

zeus_variables_fixed_hardwired.o   : zeus_variables_fixed_hardwired.f90
zeus_variables_fixed_hardwired.mod : zeus_variables_fixed_hardwired.f90

zeus_variables_fixed_namelists.o   : zeus_variables_fixed_namelists.f90 \
                                     zeus_variables_fixed_hardwired.mod
zeus_variables_fixed_namelists.mod : zeus_variables_fixed_namelists.f90 \
                                     zeus_variables_fixed_hardwired.mod

zeus_variables_fixed_other.o       : zeus_variables_fixed_other.f90 \
                                     zeus_variables_fixed_hardwired.mod
zeus_variables_fixed_other.mod     : zeus_variables_fixed_other.f90 \
                                     zeus_variables_fixed_hardwired.mod

zeus_variables_grid.o              : zeus_variables_grid.f90
zeus_variables_grid.mod            : zeus_variables_grid.f90

zeus_variables_changing.o            : zeus_variables_changing.f90 \
                                     zeus_variables_grid.mod \
                                     zeus_variables_fixed_hardwired.mod
zeus_variables_changing.mod          : zeus_variables_changing.f90 \
                                     zeus_variables_grid.mod \
                                     zeus_variables_fixed_hardwired.mod

zeus_boundary.o : zeus_boundary.f90 \
                  zeus_variables.mod zeus_mpi.mod zeus_io.mod \
                  problem_boundary.mod
zeus_boundary.mod : zeus_boundary.f90 zeus_variables.mod \
                    | zeus_mpi.mod zeus_io.mod \
                    problem_boundary.mod

zeus_deadcode.o   : zeus_deadcode.f90
zeus_deadcode.mod : zeus_deadcode.f90

zeus_gravity.o   : zeus_gravity.f90 zeus_variables.mod zeus_io.mod zeus_mpi.mod
zeus_gravity.mod : zeus_gravity.f90 | zeus_variables.mod zeus_io.mod zeus_mpi.mod

zeus.o : zeus.f90 \
         zeus_variables.mod zeus_boundary.mod zeus_initial.mod \
         zeus_physics.mod zeus_deadcode.mod zeus_io.mod zeus_timer.mod \
         zeus_iochecks.mod zeus_grid_moving.mod problem_final.mod problem_description.mod

zeus_grid_initial.o   : zeus_grid_initial.f90 \
                        zeus_variables.mod zeus_io.mod
zeus_grid_initial.mod : zeus_grid_initial.f90 \
                        | zeus_variables.mod zeus_io.mod

zeus_grid_moving.o : zeus_grid_moving.f90 \
                     zeus_variables.mod zeus_io.mod
zeus_grid_moving.mod : zeus_grid_moving.f90 \
                       | zeus_variables.mod zeus_io.mod

zeus_initial.o : zeus_initial.f90 \
                 zeus_variables.mod zeus_io.mod zeus_mpi.mod \
                 zeus_deadcode.mod \
                 zeus_grid_initial.mod problem_initial.mod \
                 zeus_physics.mod zeus_timer.mod zeus_grid_moving.mod
zeus_initial.mod : zeus_initial.f90 \
                   | zeus_variables.mod zeus_io.mod zeus_mpi.mod \
                   zeus_deadcode.mod \
                   zeus_grid_initial.mod problem_initial.mod \
                   zeus_physics.mod zeus_timer.mod zeus_grid_moving.mod

zeus_interpolators.o   : zeus_interpolators.f90 \
                         zeus_variables.mod zeus_memory.mod
zeus_interpolators.mod : zeus_interpolators.f90 zeus_variables.mod \
                         | zeus_memory.mod

zeus_io.o   : zeus_io.f90 zeus_variables.mod zeus_io_writeranks_include.f90 zeus_io_readranks_include.f90
zeus_io.mod : zeus_io.f90 zeus_variables.mod zeus_io_writeranks_include.f90 zeus_io_readranks_include.f90

zeus_iochecks.o   : zeus_iochecks.f90 \
                    zeus_variables.mod zeus_physics.mod \
                    zeus_io.mod zeus_timer.mod zeus_deadcode.mod
zeus_iochecks.mod : zeus_iochecks.f90 \
                    | zeus_variables.mod zeus_physics.mod \
                    zeus_io.mod zeus_timer.mod zeus_deadcode.mod

zeus_memory.o   : zeus_memory.f90 zeus_variables.mod zeus_io.mod
zeus_memory.mod : zeus_memory.f90 | zeus_variables.mod zeus_io.mod

zeus_mpi.o   : zeus_mpi.f90 zeus_variables.mod zeus_io.mod
zeus_mpi.mod : zeus_mpi.f90 zeus_variables.mod | zeus_io.mod

zeus_physics.o   : zeus_physics.f90 zeus_variables.mod problem_variables.mod \
                   zeus_interpolators.mod zeus_io.mod zeus_mpi.mod \
                   zeus_boundary.mod zeus_memory.mod zeus_chemistry.mod
zeus_physics.mod : zeus_physics.f90 \
                   | zeus_variables.mod problem_variables.mod \
                   zeus_interpolators.mod zeus_io.mod zeus_mpi.mod \
                   zeus_boundary.mod zeus_memory.mod zeus_chemistry.mod

zeus_chemistry.o : zeus_chemistry.f90 zeus_io.mod zeus_variables.mod
zeus_chemistry.mod : zeus_chemistry.f90 | zeus_io.mod zeus_variables.mod

zeus_timer.o   :   zeus_timer.f90 zeus_variables.mod zeus_io.mod
zeus_timer.mod :   zeus_timer.f90 | zeus_variables.mod zeus_io.mod

problem_variables.o   : problem_variables.f90 zeus_variables.mod
problem_variables.mod : problem_variables.f90 zeus_variables.mod

problem_description.o   : problem_description.f90 zeus_variables.mod \
                       problem_variables.mod zeus_io.mod
problem_description.mod : problem_description.f90 | zeus_variables.mod \
                       problem_variables.mod zeus_io.mod

problem_boundary.o   : problem_boundary.f90 zeus_variables.mod \
                    problem_variables.mod problem_description.mod zeus_io.mod
problem_boundary.mod : problem_boundary.f90 zeus_variables.mod \
                    | problem_variables.mod problem_description.mod zeus_io.mod

problem_initial.o    : problem_initial.f90 zeus_variables.mod \
                    problem_variables.mod problem_boundary.mod \
                    problem_description.mod zeus_io.mod zeus_boundary.mod \
                    zeus_mpi.mod
problem_initial.mod  : problem_initial.f90 | zeus_variables.mod \
                    problem_variables.mod problem_boundary.mod \
                    problem_description.mod zeus_io.mod zeus_boundary.mod \
                    zeus_mpi.mod

problem_final.o     : problem_final.f90
problem_final.mod   : problem_final.f90

ifeq ($(COMPILER_SCRIPT),fortran_compile)
%.mod : %.f90
	./fortran_compile "$(FC)" $< mod

%.o : %.f90
	./fortran_compile "$(FC)" $< obj
endif
ifeq ($(COMPILER_SCRIPT),empty)
%.mod : %.f90
	$(FC) $<

%.o : %.f90
	$(FC) $<
endif

%.o : %.mod

%.o : %.F90

mostlyclean :
	rm -f [pz]*.o [pz]*.mod *.output *.F90 *.work compile_work

clean :
	rm -f [pz]*.o [pz]*.mod xzeus *.output *.F90 *.work compile_work
