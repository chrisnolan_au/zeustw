! Some systems implement MPI for Fortran by means of
! an include file called "mpif.h" instead of a module called "mpi".
! For those systems, this auxiliary module must be compiled and linked.

MODULE mpi
  IMPLICIT NONE
  PUBLIC
  include "mpif.h"
END MODULE mpi
