MODULE GLOBAL
#define REALNUM REAL(KIND=8)
#define namelist_vars(list, type, vars, vals) type :: vars = vals; namelist/list/vars
  IMPLICIT NONE
  namelist_vars(Controls, REALNUM, stdCR, 1.30D-17)
  namelist_vars(Controls, REALNUM, minT, 10)
  namelist_vars(Controls, REALNUM, maxT, 41000)
  namelist_vars(Controls, INTEGER, numMols, 11)
  namelist_vars(Controls, INTEGER, numIons, 22)
  namelist_vars(Controls, INTEGER, numReacts, 133)
  namelist_vars(Controls, INTEGER, numReactsGr, 52)
  namelist_vars(Controls, INTEGER, numReactsFr, 0)
  namelist_vars(Controls, INTEGER, phyDesorb, 0)
  namelist_vars(AllSpecies, CHARACTER(500), mol_names, '')
  namelist_vars(AllSpecies, CHARACTER(800), mol_weight, '')
  namelist_vars(AllSpecies, CHARACTER(800), mol_abunds, '')
  namelist_vars(AllSpecies, CHARACTER(500), ion_names, '')
  namelist_vars(AllSpecies, CHARACTER(800), ion_weight, '')
  namelist_vars(AllSpecies, CHARACTER(800), ion_abunds, '')
  !namelist_vars(Molecules,type(Molecule)(30),mols,Molecule('',0.0))
  !
  TYPE SPECIE
    CHARACTER(LEN=16) :: n
    REALNUM :: w, x
  END TYPE SPECIE
  TYPE(SPECIE), ALLOCATABLE :: mols(:), ions(:), specs(:)
  CHARACTER(LEN=16), ALLOCATABLE :: specNs(:)
  INTEGER :: nSpecs, nMoles
  !
  TYPE ODENTRY
    INTEGER :: reac(2)
    REALNUM :: kArr(5)
  END TYPE ODENTRY
  TYPE(ODENTRY), ALLOCATABLE :: EQSet(:, :)
  INTEGER, ALLOCATABLE :: nTerms(:)
  !
  REALNUM, PRIVATE :: testvariable
  INTEGER, PARAMETER :: pk = KIND(testvariable)
  REALNUM, PARAMETER :: zro = 0.0_pk
  !
  INTEGER, PARAMETER :: nbins = 1024, gbins = 20
  REALNUM, PARAMETER :: small = 1.0D-20, verysmall = 1.0D-99, vvsmall = 1.0D-199, Pi = 4.0*atan(1.0)
  REALNUM, PARAMETER :: e_0 = 4.8032068D-10, c_0 = 2.99792458D10, &
                        k_b = 1.38064852D-16, me = 9.10938356D-28, mp = 1.6726219D-24
  REALNUM, DIMENSION(gbins) :: ra, ma, xa, Tf, kcr
  REALNUM, DIMENSION(-1:1, gbins) :: xgrs
  REALNUM, DIMENSION(gbins, gbins) :: algg
  REALNUM, ALLOCATABLE :: al_ig(:, :, :)
  REALNUM, PARAMETER :: miu = 2.36, q_g = 0.01, rho_g = 3.0, &
                        Nsv = 1.5D15, f70 = 3.16D-19, indx = 3.5
  REALNUM :: rho, temp, zeta, eta_ad, eta_ohm, eta_hal, &
             a_min = 5.D-7, a_max = 2.5D-5, cr_zeta_17 = 1.0
  LOGICAL :: writebin = .true.
  !
END MODULE GLOBAL
!!
!!====================================================================!!
!!
MODULE TOOLKIT
  USE GLOBAL
  IMPLICIT NONE
  !
CONTAINS
  !
  !====================================================================!
  !
  FUNCTION getIndx(name, list)
    INTEGER :: getIndx, id
    CHARACTER(*), INTENT(IN) :: name
    CHARACTER(*), ALLOCATABLE, INTENT(IN) :: list(:)
    getIndx = -999
    DO id = LBOUND(list, 1), UBOUND(list, 1)
      IF (list(id) .EQ. name) THEN
        getIndx = id
        EXIT
      END IF
    END DO
    IF (name == 'G') getIndx = -99
    IF (name == 'Q') getIndx = -100
    RETURN
  END FUNCTION getIndx
  !
  !====================================================================!
  !
  SUBROUTINE replace(str, text, number)
    CHARACTER(*), INTENT(INOUT) :: str
    CHARACTER(*), INTENT(IN) :: text
    CHARACTER(32) :: strnum
    REALNUM :: number
    INTEGER :: pos, nlen
    nlen = LEN(text); pos = INDEX(str, text)
    IF (pos > 0) THEN
      WRITE (strnum, '(D10.2)') number
      str = str(1:pos - 1)//TRIM(ADJUSTL(strnum))//str(pos + nlen:)
    END IF
  END SUBROUTINE replace
  !
  !====================================================================!
  !
  SUBROUTINE tokenize(str, delim, arr, nlen, kth)
    CHARACTER, INTENT(IN) :: delim
    CHARACTER(*), INTENT(IN) :: str
    INTEGER, INTENT(IN) :: nlen
    CHARACTER(*), INTENT(INOUT) :: arr(nlen)
    INTEGER, INTENT(OUT) :: kth
    INTEGER :: pos1, pos2
    !
    pos1 = 1; pos2 = 0; kth = 0; arr = ''
    DO
      kth = kth + 1
      pos2 = INDEX(str(pos1:), delim)
      IF (pos2 == 0 .or. kth >= nlen) THEN
        arr(kth) = ADJUSTL(str(pos1:))
        EXIT
      END IF
      arr(kth) = ADJUSTL(str(pos1:pos1 + pos2 - 2))
      IF (arr(kth) == "") kth = kth - 1
      pos1 = pos1 + pos2
      IF (pos1 > LEN(str)) EXIT
    END DO
  END SUBROUTINE tokenize
  !
  !====================================================================!
  !
  FUNCTION krate(Kpars, Tk, Trang)
    REALNUM :: krate
    REALNUM, INTENT(IN) :: Kpars(3), Tk, Trang(2)
    krate = zro
    IF (Tk >= Trang(1) .and. Tk < Trang(2)) &
      krate = Kpars(1)*((Tk/300.)**Kpars(2))*exp(-Kpars(3)/Tk)
    ! USE OF THE X.500 TEMPERATURE
    IF (Tk < Trang(1) .and. abs(Trang(1) - aint(Trang(1), 8)) < 1.D-3 .and. Trang(1) < 100.) &
      krate = Kpars(1)*((Trang(1)/300.)**Kpars(2))*exp(-Kpars(3)/Trang(1))
    IF (Tk >= Trang(2) .and. abs(Trang(2) - aint(Trang(2), 8)) < 1.D-3) &
      krate = Kpars(1)*((Trang(2)/300.)**Kpars(2))*exp(-Kpars(3)/Trang(2))
    RETURN
  END FUNCTION krate
  !
END MODULE TOOLKIT
!!
!!====================================================================!!
!!
MODULE CHEMISTRY
  USE GLOBAL
  USE TOOLKIT
  IMPLICIT NONE
CONTAINS
  !
  !====================================================================!
  !
  SUBROUTINE PARSE_INPUT
    INTEGER, PARAMETER :: ioin = 0
    INTEGER :: selectMols(80), unslctMols(40)
    CHARACTER(16) :: chararr(80), elems(4)
    CHARACTER(100) :: linestr, substr, infile
    CHARACTER(32) :: parts(4), param(3)
    REALNUM :: abund, alpha, pfact, Kpars(3), Trang(2)
    INTEGER :: i, j, k, n, m, ios, pos, GReac, FReac, idsw, idx(6), &
               entri, first, firstF
    LOGICAL :: file_exists
    !!
    !! PARSE COMMAND-LINE
    CALL GETARG(1, param(1))
    CALL GETARG(2, infile)
    CALL GETARG(3, param(2))
    CALL GETARG(4, param(3))
    IF (param(1) == '') THEN
      PRINT '("WARNING: Unspecified a_min, Set to Standard MRN Size: ", A)', "5e-7"
      a_min = 5.D-7
    ELSE
      READ (param(1), *) a_min
      PRINT '("a_min= ", ES10.3)', a_min
    END IF
    IF (param(2) == '') THEN
      PRINT '("WARNING: Unspecified CR zeta, Set to Default: ", A)', "1e-17"
      cr_zeta_17 = 1.0
    ELSE
      READ (param(2), *) cr_zeta_17
      PRINT '("zeta_0= ", ES10.3)', 1.0D-17*cr_zeta_17
    END IF
    IF (param(3) == '') THEN
      PRINT '("WARNING: Unspecified a_max, Set to Standard MRN Size: ", A)', "2.5e-5"
      a_max = 2.5D-5
    ELSE
      READ (param(3), *) a_max
      PRINT '("a_max= ", ES10.3)', a_max
    END IF
    IF (infile == '') THEN
      PRINT '("WARNING: Missing Input Filename, Try Using Default Filename: ", A)', "aReacts.txt"
      infile = 'aReacts.txt'
    END IF
    INQUIRE (FILE=infile, EXIST=file_exists)
    IF (file_exists == .FALSE.) THEN
      PRINT '("ERROR: Input File  ", A ,"  Does Not Exist!")', TRIM(infile)
      STOP
    END IF
    !! START READING NETWORK FILE
    OPEN (unit=ioin, file=infile, status='old', action='read', position='rewind')
    READ (ioin, Controls)
    ALLOCATE (mols(numMols))
    ALLOCATE (ions(numIons))
    !! READ SPECIES
    REWIND (ioin)
    READ (ioin, AllSpecies)
    IF (mol_names /= '') THEN
      CALL tokenize(TRIM(mol_names), ',', chararr, numMols, k)
      IF (k < numMols) &
        PRINT '("WARNING: Missing ", I2," Molecules.")', numMols - k
      DO i = 1, k
        READ (chararr(i), *) mols(i)%n
      END DO
    END IF
    IF (mol_weight /= '') THEN
      CALL tokenize(TRIM(mol_weight), ',', chararr, numMols, k)
      IF (k < numMols) &
        PRINT '("WARNING: Missing ", I2," Molecular Weights.")', numMols - k
      DO i = 1, k
        READ (chararr(i), *) mols(i)%w
      END DO
    END IF
    IF (mol_abunds /= '') THEN
      nSpecs = 0; selectMols = 0; nMoles = 0; unslctMols = 0
      CALL tokenize(TRIM(mol_abunds), ',', chararr, numMols, k)
      IF (k < numMols) &
        PRINT '("WARNING: Missing ", I2," Molecular Abundances.")', numMols - k
      DO i = 1, k
        abund = 0.0
        READ (chararr(i), *) abund
        mols(i)%x = 10**abund
        !IF (abund<-5.00) THEN
        IF (i > 3) THEN
          nSpecs = nSpecs + 1
          selectMols(nSpecs) = i
        ELSE
          nMoles = nMoles + 1
          unslctMols(nMoles) = i
        END IF
      END DO
    END IF
    IF (ion_names /= '') THEN
      CALL tokenize(TRIM(ion_names), ',', chararr, numIons, k)
      IF (k < numIons) &
        PRINT '("WARNING: Missing ", I2," Ions.")', numIons - k
      DO i = 1, k
        READ (chararr(i), *) ions(i)%n
      END DO
    END IF
    IF (ion_weight /= '') THEN
      CALL tokenize(TRIM(ion_weight), ',', chararr, numIons, k)
      IF (k < numIons) &
        PRINT '("WARNING: Missing ", I2," Ion Weights.")', numIons - k
      DO i = 1, k
        READ (chararr(i), *) ions(i)%w
      END DO
    END IF
    IF (ion_abunds /= '') THEN
      CALL tokenize(TRIM(ion_abunds), ',', chararr, numIons, k)
      IF (k < numIons) &
        PRINT '("WARNING: Missing ", I2," Ion Abundances.")', numIons - k
      DO i = 1, k
        abund = 0.0
        READ (chararr(i), *) abund
        ions(i)%x = 10**abund
      END DO
    END IF
    ALLOCATE (specs(-nMoles:nSpecs + numIons + 4))
    ALLOCATE (specNs(-nMoles:nSpecs + numIons + 4))
    specs(0)%n = 'CR'; specs(0)%w = zro; specs(0)%x = 1.00  ! IMPORTANT TO SET TO 1.0
    DO k = 1, nSpecs
      specs(k) = mols(selectMols(k))
    END DO
    DO k = 1, nMoles
      specs(-k) = mols(unslctMols(k))
    END DO
    specs(nSpecs + 1)%n = 'e-'; specs(nSpecs + 1)%w = 1/1836.15267; specs(nSpecs + 1)%x = zro
    DO k = 1, numIons
      specs(nSpecs + 1 + k) = ions(k)
    END DO
    nSpecs = nSpecs + numIons + 4
    specs(nSpecs - 2)%n = 'G-'; specs(nSpecs - 1)%n = 'G+'; specs(nSpecs)%n = 'G0'
    specNs = specs%n
    PRINT *, specNs
    !PRINT *, specs%x
    ALLOCATE (EQSet(nSpecs, numReacts))
    ALLOCATE (nTerms(nSpecs))
    ALLOCATE (al_ig(-1:1, numIons + 1, gbins))
    !!
    !! PARSE REACTIONS
    !!
    WRITE (*, *) "BEGIN PARSE GAS REACTIONS..."
    n = 0; linestr = ''; nTerms = 0; first = 0; firstF = 0
    DO
      READ (ioin, '(A)', iostat=ios) linestr
      IF (ios == -1) EXIT
      IF (INDEX(linestr, ':') /= 0 .and. (linestr(1:1) /= '#')) THEN
        n = n + 1; idx = 0; entri = -1; GReac = 0; FReac = 0;
        parts = ''; elems = ''; Kpars = zro; Trang = zro;
        IF (INDEX(linestr, 'G') /= 0) GReac = 1
        IF (INDEX(linestr, 'G') /= 0 .and. INDEX(linestr, 's') /= 0) FReac = 1
        pos = INDEX(linestr, ':')
        IF (pos > 0) THEN
          READ (linestr(1:pos - 1), *) entri
          substr = ADJUSTL(linestr(pos + 1:))
        END IF
        pos = INDEX(substr, '#')
        IF (pos > 1) substr = substr(1:pos - 1)
        !! PROCESS CONTROL VARIABLES
        call replace(substr, 'minT', minT)
        call replace(substr, 'maxT', maxT)
        !! DISTRIBUTE TOKENS
        CALL tokenize(TRIM(substr), ';', parts, size(parts), k)
        IF (k >= 3) THEN
          IF (GReac == 0) THEN
            READ (parts(3), *) Kpars
            IF (k < 4) THEN
              Trang = (/minT, maxT/)
              PRINT '("WARNING: Unspecified Temperature Range In Entry ", I4,": Set To Default [", I3, ",", I6, "]")', &
                entri, INT(minT), INT(maxT)
            ELSE
              READ (parts(4), *) Trang
            END IF
          ELSE
            READ (parts(3), *) pfact
          END IF
        ELSE
          PRINT '("WARNING: Unsupported Format In Entry ", I4,": Ignored!")', entri
          CYCLE
        END IF
        !! PARSE 2 REACTANTS
        CALL tokenize(TRIM(parts(1)), ',', elems, 4, m)
        DO i = 1, 2
          idx(i) = getIndx(TRIM(elems(i)), specNs)
        END DO
        IF (idx(1) > idx(2)) THEN
          idsw = idx(1); idx(1) = idx(2); idx(2) = idsw
        END IF
        !! PARSE m PRODUCTS
        CALL tokenize(TRIM(parts(2)), ',', elems, 4, m)
        DO j = 1, m
          idx(j + 2) = getIndx(TRIM(elems(j)), specNs)
        END DO
        !! STORE ODE COMPONENTS
        IF (ALL(idx(1:2) .lt. 0, 1) .and. idx(1) /= -100) THEN !.or.(idx(1)==idx(2))) THEN
          PRINT '("WARNING: Ignoring Reaction Entry ", I4)', entri
          CYCLE
        ELSE
          DO i = 1, 2 + m
            IF (idx(i) > 0) THEN
              nTerms(idx(i)) = nTerms(idx(i)) + 1
              idsw = nTerms(idx(i))
              EQSet(idx(i), idsw)%reac = idx(1:2)
              IF (GReac == 0) THEN
                alpha = Kpars(1)
                IF (idx(1) == -100) alpha = Kpars(1)*sqrt(2.*Nsv*k_b*Kpars(3)/Pi/Pi/mp/specs(idx(2))%w)
                IF (idx(1)*idx(2) == 0) alpha = Kpars(1)/stdCR
                IF (i <= 2) alpha = -alpha
                EQSet(idx(i), idsw)%kArr(1) = alpha
                EQSet(idx(i), idsw)%kArr(2:3) = Kpars(2:3)
                EQSet(idx(i), idsw)%kArr(4:5) = Trang
              ELSE
                alpha = pfact
                IF (FReac == 1) alpha = pfact*sqrt(8.*k_b/Pi/mp/specs(idx(2))%w)
                IF (i <= 2) alpha = -alpha
                EQSet(idx(i), idsw)%kArr(1) = alpha
                EQSet(idx(i), idsw)%kArr(2:3) = zro
                EQSet(idx(i), idsw)%kArr(4:5) = (/minT, maxT/)
              END IF
              !print *, idx(i), idsw, TRIM(specNs(idx(i)))//':', EQSet(idx(i),idsw)
            END IF
          END DO
          !print *, ''
        END IF
        IF (n >= numReacts .and. first == 0) THEN
          first = 1
          WRITE (*, *) "BEGIN PARSE GRAIN REACTIONS..."
        END IF
        IF (numReactsFr > 0 .and. n >= numReacts + numReactsGr .and. firstF == 0) THEN
          firstF = 1
          WRITE (*, *) "BEGIN PARSE FREEZE-OUT REACTIONS..."
        END IF
        IF (n >= numReacts + numReactsGr + numReactsFr + 1) EXIT
      END IF
    END DO
    WRITE (*, *) ""
    CLOSE (ioin)
    !
  END SUBROUTINE PARSE_INPUT
  !
  !====================================================================
  !
  SUBROUTINE INIT(nden, Tk, NEQ, Y)
    INTEGER, INTENT(IN) :: NEQ
    REALNUM, INTENT(INOUT) :: Y(NEQ)
    REALNUM, INTENT(IN) :: nden, Tk
    INTEGER :: i, j, k, erst
    REALNUM :: C0, da, mw, con1, con2, rasum, mgred, E_dep
    !
    Y = zro; Y(1:nSpecs - numIons - 1) = specs(1:nSpecs - numIons - 1)%x
    mw = 29.*mp/me; E_dep = 0.060592182
    C0 = 3.*q_g*mp/2./Pi/rho_g*(4.-indx)/(a_max**(4.-indx) - a_min**(4.-indx))
    da = 10**((log10(a_max) - log10(a_min))/gbins)
    DO k = 1, gbins
      IF (k == 1) THEN
        ra(k) = a_min*sqrt(da)
      ELSE
        ra(k) = ra(k - 1)*da
      END IF
      ma(k) = rho_g*4.*Pi/3.*(ra(k))**3
      xa(k) = C0/(indx - 1.)*((ra(k)/sqrt(da))**(1.-indx) - (ra(k)*sqrt(da))**(1.-indx))
      ! INITIAL GRAIN ABUNDANCE
      xgrs(0, k) = xa(k)/(1.+(1.+mw)/sqrt(mw)/(1.+e_0*e_0/ra(k)/k_b/Tk))
      xgrs(-1, k) = xgrs(0, k)*sqrt(mw)/(1.+e_0*e_0/ra(k)/k_b/Tk)
      xgrs(1, k) = xgrs(-1, k)/mw
      !Y(nSpecs-3+k)= xgrs(-1,k)
      !Y(nSpecs-3+gbins+k)= xgrs(1,k)
      IF (ra(k) >= 1.592480913D-5) &
        Tf(k) = (10.**3 + E_dep*9./4./Pi/1.38D3/ra(k)/ra(k))**(1./3.)
      IF ((ra(k) < 1.592480913D-5) .and. (ra(k) >= 3.964054972D-6)) &
        Tf(k) = (50.**2.3 + (E_dep*3./4./Pi/ra(k)/ra(k) - 5.704D7)*2.3/2.1338215135D4)**(1./2.3)
      IF (ra(k) < 3.964054972D-6) &
        Tf(k) = (150.**1.7 + (E_dep*3./4./Pi/ra(k)/ra(k) - 8.63512690D8 - 5.704D7)*1.7/4.313331578D5)**(1./1.7)
      Tf(k) = MAX(35., MIN(Tf(k), 200.))
      kcr(k) = 0.36*EXP(1200./Tf(k))/1.D12/3160.*ra(k)*ra(k)
      !kcr(k)=0.36*EXP(1200./Tf(k))/1.D12/12.5*ra(k)*ra(k)
    END DO
    specs(nSpecs - 2)%x = SUM(xgrs(-1, 1:gbins))
    specs(nSpecs - 1)%x = SUM(xgrs(1, 1:gbins))
    specs(nSpecs)%x = SUM(xgrs(0, 1:gbins))
    !
    con1 = e_0*e_0/k_b/Tk
    al_ig = zro; erst = nSpecs - numIons - 3
    DO i = 1, numIons + 1
      DO k = 1, gbins
        con2 = sqrt(8.*k_b*Tk/Pi/mp/specs(erst + i - 1)%w)
        IF (i == 1) THEN
          al_ig(-1, i, k) = 0.
          al_ig(1, i, k) = Pi*ra(k)*ra(k)*con2* &
                           (1.+(con1/ra(k)))*(1.+sqrt(2./(2.+ra(k)/con1)))
        ELSE
          al_ig(-1, i, k) = Pi*ra(k)*ra(k)*con2* &
                            (1.+(con1/ra(k)))*(1.+sqrt(2./(2.+ra(k)/con1)))
          al_ig(1, i, k) = 0.
        END IF
        al_ig(0, i, k) = Pi*ra(k)*ra(k)*con2*(1.+sqrt(0.5*Pi*con1/ra(k)))
      END DO
    END DO
    algg = zro
    DO k = 1, gbins
      DO j = k, gbins
        mgred = ma(k)*ma(j)/(ma(k) + ma(j))
        rasum = ra(k) + ra(j)
        con2 = sqrt(8.*k_b*Tk/Pi/mgred)
        algg(k, j) = Pi*rasum*rasum*con2*(1.+(con1/rasum))* &
                     (1.+sqrt(2./(2.+rasum/con1)))
        IF (j > k) algg(j, k) = algg(k, j)
      END DO
    END DO
    Y(erst + 1:nSpecs - 3) = specs(erst + 1:nSpecs - 3)%x/stdCR/(rho/zeta)
    Y(erst) = SUM(Y(erst + 1:nSpecs - 3))!+specs(nSpecs-1)%x-specs(nSpecs-2)%x
    !
  END SUBROUTINE INIT
  !
  !====================================================================!
  !
  SUBROUTINE FEX(NEQ, TIN, Y, YDOT)
    IMPLICIT NONE
    INTEGER, INTENT(IN) :: NEQ
    REALNUM, INTENT(IN) :: TIN
    REALNUM, INTENT(IN) :: Y(NEQ)
    REALNUM, INTENT(OUT) :: YDOT(NEQ)
    REALNUM, DIMENSION(gbins) :: gr0
    INTEGER :: i, j, k, gk, gtk, gki, erst, ids(2)
    REALNUM :: curate, cuprod, Kps(5), cuspecs(-nMoles:NEQ), k_crd, k_fld
    !
    cuspecs(-nMoles:0) = specs(-nMoles:0)%x; cuspecs(1:NEQ) = Y(1:NEQ)
    DO k = 1, gbins
      gr0(k) = xa(k) - Y(nSpecs - 3 + k) - Y(nSpecs - 3 + gbins + k)
    END DO
    YDOT = zro; erst = nSpecs - numIons - 3
    DO i = 1, nSpecs - 1
      IF (i == erst) CYCLE
      DO j = 1, nTerms(i)
        ids = EQSet(i, j)%reac
        Kps = EQSet(i, j)%kArr
        curate = krate(Kps(1:3), temp, Kps(4:5))
        ! FREEZE/DESORB TERMS
        IF (ids(1) == -99) THEN
          cuprod = rho*cuspecs(ids(2))*Pi*SUM((ra(1:gbins)**2)*xa(1:gbins))
          YDOT(i) = YDOT(i) + curate*sqrt(temp)*cuprod
          CYCLE
        END IF
        IF (ids(1) == -100) THEN
          k_crd = 0.; k_fld = 0.
          IF (phyDesorb == 1) THEN
            DO k = 1, gbins
              k_crd = k_crd + kcr(k)*krate(Kps(1:3), Tf(k), Kps(4:5))*xa(k)*ra(k)*ra(k)
            END DO
            k_crd = (zeta/1.0D-17)*k_crd/SUM(xa(1:gbins)*ra(1:gbins)*ra(1:gbins))
            !k_crd=f70*(zeta/1.0D-17)*krate(Kps(1:3),70.,Kps(4:5))
            k_fld = (zeta/1.0D-17)*1830./8./Nsv*1.D-3
          END IF
          YDOT(i) = YDOT(i) + (curate + k_crd + k_fld)*cuspecs(ids(2))
          CYCLE
        END IF
        ! GRAIN TERMS
        IF (i > nSpecs - 3) THEN
          IF (ids(2) < nSpecs - 2) THEN
            WRITE (*, *) "ERROR: PURE GAS PRODUCES GRAIN. CHECK GRAIN REACTIONS!"
            STOP
          END IF
          gk = ids(2) - nSpecs + 2; gtk = 2*gk - 1; gki = i - nSpecs + 2;
          DO k = 1, gbins
            IF (ids(2) < nSpecs) THEN ! G- & G+
              IF (ids(1) >= erst) cuprod = rho*Y(ids(1))* &
                                           al_ig(gtk, ids(1) - erst + 1, k)*Y(nSpecs - 3 + gk*gbins + k)
            ELSE ! G0
              IF (ids(1) >= erst) cuprod = rho*Y(ids(1))* &
                                           al_ig(0, ids(1) - erst + 1, k)*gr0(k)
            END IF
            YDOT(nSpecs - 3 + gki*gbins + k) = YDOT(nSpecs - 3 + gki*gbins + k) + &
                                               curate*cuprod
          END DO
          CYCLE
        END IF
        ! GAS TERMS
        IF (ids(2) > nSpecs - 3) THEN
          IF (ids(1) > nSpecs - 3) THEN
            WRITE (*, *) "ERROR: PURE GRAIN PRODUCES GAS, CHECK GRAIN REACTIONS!"
            STOP
          END IF
          gk = ids(2) - nSpecs + 2; gtk = 2*gk - 1
          IF (ids(2) < nSpecs) THEN ! G- & G+
            IF (ids(1) >= erst) cuprod = rho*Y(ids(1))* &
                                         SUM(al_ig(gtk, ids(1) - erst + 1, 1:gbins)* &
                                             Y(nSpecs - 2 + gk*gbins:nSpecs - 3 + (gk + 1)*gbins))
          ELSE ! G0
            IF (ids(1) >= erst) cuprod = rho*Y(ids(1))* &
                                         SUM(al_ig(0, ids(1) - erst + 1, 1:gbins)* &
                                             gr0(1:gbins))
          END IF
        ELSE IF (ids(1)*ids(2) == 0) THEN
          cuprod = zeta*cuspecs(ids(1))*cuspecs(ids(2))
        ELSE
          cuprod = rho*cuspecs(ids(1))*cuspecs(ids(2))
        END IF
        YDOT(i) = YDOT(i) + curate*cuprod
      END DO
    END DO
    ! GRAIN-GRAIN INTERACTION
    DO k = nSpecs - 2, nSpecs - 3 + gbins
      YDOT(k) = YDOT(k) - rho*Y(k)* &
                SUM(algg(k - nSpecs + 3, 1:gbins)*Y(nSpecs - 2 + gbins:nSpecs - 3 + 2*gbins))
      YDOT(gbins + k) = YDOT(gbins + k) - rho*Y(gbins + k)* &
                        SUM(algg(1:gbins, k - nSpecs + 3)*Y(nSpecs - 2:nSpecs - 3 + gbins))
    END DO
    ! ELECTRON CLOSURE
    YDOT(erst) = SUM(YDOT(erst + 1:nSpecs - 3)) + &
                 SUM(YDOT(nSpecs - 2 + gbins:nSpecs - 3 + 2*gbins)) - &
                 SUM(YDOT(nSpecs - 2:nSpecs - 3 + gbins))
  END SUBROUTINE FEX
  !
  !====================================================================!
  !
  SUBROUTINE JEX(NEQ, TIN, Y, ML, MU, PD, NRPD)
    IMPLICIT NONE
    INTEGER, INTENT(IN) :: NEQ, ML, MU, NRPD
    REALNUM, INTENT(IN) :: TIN
    REALNUM, INTENT(IN) :: Y(NEQ)
    REALNUM, INTENT(OUT) :: PD(NRPD, NEQ)
    REALNUM, DIMENSION(gbins) :: gr0
    INTEGER :: i, j, k, gk, gtk, gki, erst, ids(2)
    REALNUM :: curate, Kps(5), cuspecs(-nMoles:NEQ), k_crd, k_fld
    !
    cuspecs(-nMoles:0) = specs(-nMoles:0)%x; cuspecs(1:NEQ) = Y(1:NEQ)
    DO k = 1, gbins
      gr0(k) = xa(k) - Y(nSpecs - 3 + k) - Y(nSpecs - 3 + gbins + k)
    END DO
    PD = 0; erst = nSpecs - numIons - 3
    DO i = 1, nSpecs - 1
      DO j = 1, nTerms(i)
        ids = EQSet(i, j)%reac
        Kps = EQSet(i, j)%kArr
        curate = krate(Kps(1:3), temp, Kps(4:5))
        ! FREEZE/DESORB JACOBIAN TERMS
        IF (ids(1) == -99) THEN
          IF (ids(2) > 0) PD(i, ids(2)) = PD(i, ids(2)) + curate*sqrt(temp)*rho* &
                                          Pi*SUM((ra(1:gbins)**2)*xa(1:gbins))
          CYCLE
        END IF
        IF (ids(1) == -100) THEN
          k_crd = 0.; k_fld = 0.
          IF (phyDesorb == 1) THEN
            DO k = 1, gbins
              k_crd = k_crd + kcr(k)*krate(Kps(1:3), Tf(k), Kps(4:5))*xa(k)*ra(k)*ra(k)
            END DO
            k_crd = (zeta/1.0D-17)*k_crd/SUM(xa(1:gbins)*ra(1:gbins)*ra(1:gbins))
            !k_crd=f70*(zeta/1.0D-17)*krate(Kps(1:3),70.,Kps(4:5))
            k_fld = (zeta/1.0D-17)*1830./8./Nsv*1.D-3
          END IF
          IF (ids(2) > 0) PD(i, ids(2)) = PD(i, ids(2)) + (curate + k_crd + k_fld)
          CYCLE
        END IF
        ! GRAIN JACOBIAN TERMS
        IF (i > nSpecs - 3) THEN
          IF (ids(2) < nSpecs - 2) THEN
            WRITE (*, *) "ERROR: PURE GAS PRODUCES GRAIN. CHECK GRAIN REACTIONS!"
            STOP
          END IF
          gk = ids(2) - nSpecs + 2; gtk = 2*gk - 1; gki = i - nSpecs + 2
          DO k = 1, gbins
            IF (ids(2) < nSpecs .and. ids(1) >= erst) THEN
              PD(nSpecs - 3 + gki*gbins + k, ids(1)) = &
                PD(nSpecs - 3 + gki*gbins + k, ids(1)) + curate*rho* &
                al_ig(gtk, ids(1) - erst + 1, k)*Y(nSpecs - 3 + gk*gbins + k)
              PD(nSpecs - 3 + gki*gbins + k, nSpecs - 3 + gk*gbins + k) = &
                PD(nSpecs - 3 + gki*gbins + k, nSpecs - 3 + gk*gbins + k) + &
                curate*rho*Y(ids(1))*al_ig(gtk, ids(1) - erst + 1, k)
            END IF
            IF (ids(2) == nSpecs .and. ids(1) >= erst) THEN
              PD(nSpecs - 3 + gki*gbins + k, ids(1)) = &
                PD(nSpecs - 3 + gki*gbins + k, ids(1)) + curate*rho* &
                al_ig(0, ids(1) - erst + 1, k)*gr0(k)
              PD(nSpecs - 3 + gki*gbins + k, nSpecs - 3 + k) = &
                PD(nSpecs - 3 + gki*gbins + k, nSpecs - 3 + k) - curate*rho* &
                Y(ids(1))*al_ig(0, ids(1) - erst + 1, k)
              PD(nSpecs - 3 + gki*gbins + k, nSpecs - 3 + gbins + k) = &
                PD(nSpecs - 3 + gki*gbins + k, nSpecs - 3 + gbins + k) - curate*rho* &
                Y(ids(1))*al_ig(0, ids(1) - erst + 1, k)
            END IF
          END DO
          CYCLE
        END IF
        ! GAS JACOBIAN TERMS
        IF (ids(2) > nSpecs - 3) THEN
          IF (ids(1) > nSpecs - 3) THEN
            WRITE (*, *) "ERROR: PURE GRAIN PRODUCES GAS, CHECK GRAIN REACTIONS!"
            STOP
          END IF
          gk = ids(2) - nSpecs + 2; gtk = 2*gk - 1
          IF (ids(2) < nSpecs .and. ids(1) >= erst) THEN
            PD(i, ids(1)) = PD(i, ids(1)) + &
                            curate*rho*SUM(al_ig(gtk, ids(1) - erst + 1, 1:gbins)* &
                                           Y(nSpecs - 2 + gk*gbins:nSpecs - 3 + (gk + 1)*gbins))
            PD(i, nSpecs - 2 + gk*gbins:nSpecs - 3 + (gk + 1)*gbins) = &
              PD(i, nSpecs - 2 + gk*gbins:nSpecs - 3 + (gk + 1)*gbins) + curate*rho* &
              Y(ids(1))*al_ig(gtk, ids(1) - erst + 1, 1:gbins)
          END IF
          IF (ids(2) == nSpecs .and. ids(1) >= erst) THEN
            PD(i, ids(1)) = PD(i, ids(1)) + curate*rho* &
                            SUM(al_ig(0, ids(1) - erst + 1, 1:gbins)*gr0(1:gbins))
            PD(i, nSpecs - 2:nSpecs - 3 + gbins) = &
              PD(i, nSpecs - 2:nSpecs - 3 + gbins) - curate*rho* &
              Y(ids(1))*al_ig(0, ids(1) - erst + 1, 1:gbins)
            PD(i, nSpecs - 2 + gbins:nSpecs - 3 + 2*gbins) = &
              PD(i, nSpecs - 2 + gbins:nSpecs - 3 + 2*gbins) - curate*rho* &
              Y(ids(1))*al_ig(0, ids(1) - erst + 1, 1:gbins)
          END IF
        ELSE IF (ids(1)*ids(2) == 0) THEN
          IF (ids(2) > 0) PD(i, ids(2)) = PD(i, ids(2)) + curate*zeta
        ELSE
          IF (ids(1) > 0) PD(i, ids(1)) = PD(i, ids(1)) + curate*rho*cuspecs(ids(2))
          PD(i, ids(2)) = PD(i, ids(2)) + curate*rho*cuspecs(ids(1))
        END IF
      END DO
    END DO
    ! GRAIN-GRAIN JACOBIAN TERMS
    DO k = nSpecs - 2, nSpecs - 3 + gbins
      PD(k, nSpecs - 2 + gbins:nSpecs - 3 + 2*gbins) = &
        PD(k, nSpecs - 2 + gbins:nSpecs - 3 + 2*gbins) - &
        rho*Y(k)*algg(k - nSpecs + 3, 1:gbins)
      PD(k, k) = PD(k, k) - rho* &
                 SUM(algg(k - nSpecs + 3, 1:gbins)*Y(nSpecs - 2 + gbins:nSpecs - 3 + 2*gbins))
      PD(gbins + k, nSpecs - 2:nSpecs - 3 + gbins) = &
        PD(gbins + k, nSpecs - 2:nSpecs - 3 + gbins) - &
        rho*Y(gbins + k)*algg(1:gbins, k - nSpecs + 3)
      PD(gbins + k, gbins + k) = PD(gbins + k, gbins + k) - rho* &
                                 SUM(algg(1:gbins, k - nSpecs + 3)*Y(nSpecs - 2:nSpecs - 3 + gbins))
    END DO
    ! ELECTRON CLOSURE
    DO i = 1, NEQ
      PD(erst, i) = SUM(PD(erst + 1:nSpecs - 3, i)) + &
                    SUM(PD(nSpecs - 2 + gbins:nSpecs - 3 + 2*gbins, i)) - &
                    SUM(PD(nSpecs - 2:nSpecs - 3 + gbins, i))
    END DO
  END SUBROUTINE JEX
  !
  !====================================================================
  !
  SUBROUTINE MHD_COEFS(rho, Tk, Bg, Y, NEQ)
    IMPLICIT NONE
    INTEGER, INTENT(IN) :: NEQ
    REALNUM, INTENT(IN) :: rho, Tk, Bg
    REALNUM, INTENT(IN) :: Y(NEQ)
    REALNUM :: mtr_iH2(numIons + 1), beta_iH2(numIons + 1)
    REALNUM :: mtr_gH2(gbins), beta_gH2(gbins)
    REALNUM :: thT, sgm_para, sgm_P, sgm_H
    INTEGER :: i, j, k, erst
    CHARACTER(LEN=16) :: sName
    !
    thT = log10(Tk); erst = nSpecs - numIons - 3
    DO i = 1, numIons + 1
      j = erst + i - 1
      sName = specNs(j)
      SELECT CASE (sName)
      CASE ('e-')
        mtr_iH2(i) = sqrt(Tk)*(0.535 + 0.203*thT - 0.163*thT**2 + 0.050*thT**3)
      CASE ('H+')
        mtr_iH2(i) = 1.003 + 0.050*thT + 0.136*thT**2 - 0.014*thT**3
      CASE ('H3+')
        mtr_iH2(i) = 2.693 - 1.238*thT + 0.664*thT**2 - 0.089*thT**3
      CASE ('HCO+')
        mtr_iH2(i) = sqrt(Tk)*(1.476 - 1.409*thT + 0.555*thT**2 - 0.0775*thT**3)
      CASE ('C+')
        mtr_iH2(i) = 1.983*0.425*thT - 0.431*thT**2 + 0.114*thT**3
      CASE DEFAULT
        mtr_iH2(i) = 2.81*sqrt(0.804/specs(j)%w)*4.
      END SELECT
    END DO
    mtr_iH2 = mtr_iH2*1.D-9
    beta_iH2 = (e_0*Bg/specs(erst:erst + numIons)%w/mp/c_0)*(specs(erst:erst + numIons)%w + 2.)/(miu*rho*mtr_iH2)
    DO k = 1, gbins
      mtr_gH2(k) = 4.*Pi/3.*ra(k)*ra(k)*1.3*sqrt(4.*k_b*Tk/Pi/mp)
      beta_gH2(k) = (e_0*Bg/ma(k)/c_0)*(ma(k) + 2.*mp)/(miu*mp*rho*mtr_gH2(k))
    END DO
    !
    sgm_para = SUM(Y(erst:erst + numIons)*beta_iH2)
    sgm_P = SUM(Y(erst:erst + numIons)*beta_iH2(1:numIons + 1) &
                /(1 + beta_iH2(1:numIons + 1)*beta_iH2(1:numIons + 1)))
    sgm_H = SUM(Y(erst + 1:erst + numIons) &
                /(1 + beta_iH2(2:numIons + 1)*beta_iH2(2:numIons + 1))) &
            - Y(erst)/(1 + beta_iH2(1)*beta_iH2(1))
    sgm_para = e_0*c_0*rho/Bg*(sgm_para + &
                               SUM(Y(nSpecs - 2:nSpecs - 3 + gbins)*beta_gH2) + &
                               SUM(Y(nSpecs - 2 + gbins:nSpecs - 3 + 2*gbins)*beta_gH2))
    sgm_P = e_0*c_0*rho/Bg*(sgm_P + &
                            SUM(Y(nSpecs - 2:nSpecs - 3 + gbins)*beta_gH2/(1 + beta_gH2*beta_gH2)) + &
                            SUM(Y(nSpecs - 2 + gbins:nSpecs - 3 + 2*gbins)*beta_gH2/(1 + beta_gH2*beta_gH2)))
    sgm_H = e_0*c_0*rho/Bg*(sgm_H &
                            - SUM(Y(nSpecs - 2:nSpecs - 3 + gbins)/(1 + beta_gH2*beta_gH2)) + &
                            SUM(Y(nSpecs - 2 + gbins:nSpecs - 3 + 2*gbins)/(1 + beta_gH2*beta_gH2)))
    eta_ad = c_0*c_0/4./Pi*(sgm_P/(sgm_P*sgm_P + sgm_H*sgm_H) - 1./sgm_para)
    eta_hal = c_0*c_0/4./Pi*(sgm_H/(sgm_P*sgm_P + sgm_H*sgm_H))
    eta_ohm = c_0*c_0/4./Pi/sgm_para
    IF (writebin) THEN
      WRITE (21) rho, Tk, Y(erst:nSpecs - 3 + gbins*2), specs(nSpecs - 2)%x, specs(nSpecs - 1)%x, eta_ad, eta_ohm, eta_hal
      !WRITE (21) rho, Tk, Y(1:nSpecs-3), specs(nSpecs-2)%x, specs(nSpecs-1)%x, specs(nSpecs)%x, eta_ad, eta_ohm, eta_hal
    ELSE
      WRITE (*, "(A)", advance="no") "(/"
      WRITE (*, "((d,:,',') \)", advance="no") rho, Tk, Y(erst:nSpecs - 3 + gbins*2), &
        specs(nSpecs - 2)%x, specs(nSpecs - 1)%x, eta_ad, eta_ohm, eta_hal
      WRITE (*, "(A)") " /)"
    END IF
    !
  END SUBROUTINE MHD_COEFS
  !
END MODULE CHEMISTRY
 !!====================================================================!!
 !!
 !!
 !!
 !!====================================================================!!
PROGRAM Archem
  USE GLOBAL
  USE CHEMISTRY
  USE DVODE_F90_M
  IMPLICIT NONE

  INTEGER :: i, j, k, erst, cr_lv = -1, therm = 1, &
             NEQ, ITASK, ISTATE, IOUT, IERROR, IFIN
  REALNUM, DIMENSION(nbins) :: Dn, Bf, Tp, indgen = (/(i, i=1, nbins)/)
  REALNUM :: limits, limitf, nh, dens, T0, zeta_anly, Sigma, chnet
  REALNUM :: beg_time, end_time, RTOL, TIN, TOUT, dt, year = 3.15576D7
  REALNUM, ALLOCATABLE :: Y(:), ATOL(:)
  REALNUM, DIMENSION(22) :: RSTATS
  INTEGER, DIMENSION(31) :: ISTATS
  TYPE(VODE_OPTS) :: OPTIONS
  !
  CALL CPU_TIME(beg_time)
  CALL PARSE_INPUT
  NEQ = nSpecs - 3 + gbins*2
  erst = nSpecs - numIons - 3
  ALLOCATE (Y(NEQ))
  ALLOCATE (ATOL(NEQ))
  dt = 3.15576D6; IFIN = 1000000
  T0 = miu*mp/k_b*(1.88e4**2)
  limits = 2.
  limitf = 15.0
  nh = (limitf - limits)/(nbins - 1)
  DO i = 1, nbins
    eta_ad = zro; eta_ohm = zro; eta_hal = zro
    Dn(i) = 10**((indgen(i) - 1)*nh + limits)
    rho = Dn(i)
    dens = Dn(i)*miu*mp
    Bf(i) = 1.43D-7*Dn(i)**0.5
    ! TEMPERATURE
    Tp(i) = T0
    IF (therm == 1) THEN
      IF (dens < 1.D-12) THEN
        Tp(i) = T0 + 1.5*dens/1.D-13
      ELSE IF (dens <= 1.D-11) THEN
        Tp(i) = (T0 + 15.)/(10**0.6)*(dens/1.D-13)**0.6
      ELSE
        Tp(i) = (T0 + 15.)*(10**0.6)*(dens/1.D-11)**0.44
      END IF
      Tp(i) = MIN(Tp(i), 2000.)
    END IF
    temp = Tp(i)
    ! CR RATE
    zeta_anly = cr_zeta_17*1.0D-17
    IF (cr_lv == -1) THEN
      !Sigma= 1.6231D-4*sqrt(Dn(i)*Tp(i)/10.)
      Sigma = 1.6231D-4*sqrt(Dn(i)*10./10.)
      zeta_anly = zeta_anly*exp(-Sigma/2/96.)
      zeta_anly = max(zeta_anly, 1.1D-22) !7.3D-19)
    END IF
    zeta = zeta_anly
    IF (writebin) OPEN (21, file="foo.dat", form='unformatted', &
                        access='stream')
    !
    ! SOLVE ODE
    TIN = 0.
    TOUT = 3.15576D6
    IERROR = 0; ITASK = 1; ISTATE = 1;
    RSTATS = 0.; ISTATS = 0.;
    RTOL = 1.D-6; ATOL = 1.D-40
    OPTIONS = SET_NORMAL_OPTS(DENSE_J=.TRUE., ABSERR_VECTOR=ATOL, RELERR=RTOL, &
                              USER_SUPPLIED_JACOBIAN=.TRUE.)
    CALL INIT(Dn(i), Tp(i), NEQ, Y)
    DO IOUT = 1, IFIN
      CALL DVODE_F90(FEX, NEQ, Y, TIN, TOUT, ITASK, ISTATE, OPTIONS, J_FCN=JEX)
      CALL GET_STATS(RSTATS, ISTATS)
      DO k = 1, gbins
        xgrs(-1, k) = Y(nSpecs - 3 + k)
        xgrs(1, k) = Y(nSpecs - 3 + gbins + k)
        xgrs(0, k) = xa(k) - Y(nSpecs - 3 + k) - Y(nSpecs - 3 + gbins + k)
      END DO
      specs(nSpecs - 2)%x = SUM(xgrs(-1, 1:gbins))
      specs(nSpecs - 1)%x = SUM(xgrs(1, 1:gbins))
      specs(nSpecs)%x = SUM(xgrs(0, 1:gbins))
      chnet = (SUM(Y(erst + 1:nSpecs - 3)) + specs(nSpecs - 1)%x - specs(nSpecs - 2)%x - Y(erst))/Y(erst)
      DO j = 1, NEQ
        IF (Y(j) < 0.) THEN
          IERROR = 1
          Y(j) = ATOL(j)
        END IF
      END DO
      IF (ISTATE < 0) THEN
        PRINT *, "Error halt: ISTATE =", ISTATE
        EXIT
      END IF
      TOUT = TOUT + dt
    END DO
    IF (IERROR == 1) THEN
      PRINT *, "An error occured."
    ELSE
      !PRINT *, "No error occured."
    END IF
    CALL MHD_COEFS(Dn(i), Tp(i), Bf(i), Y, NEQ)
    !PRINT "(d,\)", Dn(i), Tp(i), Y(1:nSpecs-3), specs(nSpecs-2)%x, specs(nSpecs-1)%x, specs(nSpecs)%x, eta_ad, eta_ohm, eta_hal
    !PRINT *, ""
    IF (abs(chnet) > 1.D-4) THEN
      PRINT *, "Check Neutrality: ", chnet
      PRINT *, ""
    ENDIF
  END DO
  !
  DEALLOCATE (Y)
  DEALLOCATE (ATOL)
  DEALLOCATE (mols)
  DEALLOCATE (ions)
  DEALLOCATE (specs)
  DEALLOCATE (specNs)
  DEALLOCATE (EQSet)
  DEALLOCATE (nTerms)
  DEALLOCATE (al_ig)
  CALL CPU_TIME(end_time)
  PRINT '("Elapsed Time= ", ES16.9)', end_time - beg_time
  IF (writebin) CLOSE (21)

END PROGRAM Archem
