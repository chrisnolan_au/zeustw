#include "macros"
MODULE problem_boundary
  !
  ! Problem-dependent boundary conditions
  !
  USE zeus_variables
  USE problem_variables
  USE problem_description
  USE zeus_io
  IMPLICIT NONE
  PRIVATE
  PUBLIC :: boundary_d_, boundary_e_, boundary_dfl, boundary_etas, &
            boundary_v1, boundary_v2, boundary_v3, boundary_emf1, &
            boundary_emf2, boundary_emf3, boundary_b3, boundary_tf, &
            boundary_emf, boundary_emf_dx, boundary_currents, boundary_vi, &
            boundary_vd
CONTAINS
  !
  !=======================================================================
  !
  SUBROUTINE boundary_d_(face, side)
    INTEGER, INTENT(IN) :: face, side
    !
    INTEGER i, j, k
    INTEGER kpi
    REALNUM temp, theta

    if (face == 1 .and. side == 1) then
      do k = ksg, keg
        do j = jsg, jeg
          !theta=x2b(j)
          !theta=abs(min(theta,pi-theta))

          !if (theta>=windcone_rho%theta) then
          !  temp= (wind_r0/x1ap(isp))**2 * &
          !      wind_rho0/sin(theta)**wind_n
          !else
          !  temp= windcone_rho%a*sin(theta)**windcone_rho%n+windcone_rho%b
          !end if

          do i = isg, ism1
            !d_(i,j,k)= ttspline( x2b(j) &
            !    , &
            !    temp * (x1ap(isp)*x1bi(i))**wind_radial_n_ghosts &
            !    , &
            !    d_(is,j,k) )

            !d_(i,j,k)=d_(i+ie-ism1,j,k)   ! Periodic BC
            d_(i, j, k) = d_(is, j, k)        ! Ouflow (continuity) BC
            !d_(i,j,k)=d_(is+ism1-i,j,k) ! Reflective BC
          end do
        end do
      end do
    end if
    if (face == 1 .and. side == 2) then
      do k = ksg, keg
        do j = jsg, jeg
          do i = iep1, ieg
            !d_(i,j,k)=d_(i+is-iep1,j,k)   ! Periodic BC
            d_(i, j, k) = d_(ie, j, k)        ! Ouflow (continuity) BC
            ! d_(i,j,k)=d_(ie+iep1-i,j,k) ! Reflective BC
          end do
        end do
      end do
    end if

    if (face == 2 .and. side == 1) then
      do k = ksg, keg
        kpi = k + knac/2; if (kpi > ke) kpi = kpi - knac
        do j = jsg, jsm1
          do i = isg, ieg
            !d_(i,j,k)=d_(i,j+je-jsm1,k)   ! Periodic BC
            ! d_(i,j,k)=d_(i,js,k)        ! Ouflow (continuity) BC
            d_(i, j, k) = d_(i, js + jsm1 - j, k) ! Reflective BC
            ! d_(i,j,k)=d_(i,js+jsm1-j,kpi) ! kpi BC
          end do
        end do
      end do
    end if
    if (face == 2 .and. side == 2) then
      do k = ksg, keg
        kpi = k + knac/2; if (kpi > ke) kpi = kpi - knac
        do j = jep1, jeg
          do i = isg, ieg
            !d_(i,j,k)=d_(i,j+js-jep1,k)   ! Periodic BC
            ! d_(i,j,k)=d_(i,je,k)        ! Ouflow (continuity) BC
            d_(i, j, k) = d_(i, je + jep1 - j, k) ! Reflective BC
            !d_(i,j,k)=d_(i,je+jep1-j,kpi) ! kpi BC
          end do
        end do
      end do
    end if

    if (face == 3 .and. side == 1) then
      do k = ksg, ksm1
        do j = jsg, jeg
          do i = isg, ieg
            d_(i, j, k) = d_(i, j, k + ke - ksm1)   ! Periodic BC
            ! d_(i,j,k)=d_(i,j,ks)        ! Ouflow (continuity) BC
            ! d_(i,j,k)=d_(i,j,ks+ksm1-k) ! Reflective BC
          end do
        end do
      end do
    end if
    if (face == 3 .and. side == 2) then
      do k = kep1, keg
        do j = jsg, jeg
          do i = isg, ieg
            d_(i, j, k) = d_(i, j, k + ks - kep1)   ! Periodic BC
            ! d_(i,j,k)=d_(i,j,ke)        ! Ouflow (continuity) BC
            ! d_(i,j,k)=d_(i,j,ke+kep1-k) ! Reflective BC
          end do
        end do
      end do
    end if
    !
  END SUBROUTINE boundary_d_
  !
  !=======================================================================
  !
  SUBROUTINE boundary_dfl(face, side)
    INTEGER, INTENT(IN) :: face, side
    !
    INTEGER i, j, k
    INTEGER kpi
    REALNUM temp, theta

    if (.not. alfven_dt_floor_forcelimiter) return

    if (face == 1 .and. side == 1) then
      do k = ksg, keg
        do j = jsg, jeg
          !theta=x2b(j)
          !theta=abs(min(theta,pi-theta))

          !if (theta>=windcone_rho%theta) then
          !  temp= (wind_r0/x1ap(isp))**2 * &
          !      wind_rho0/sin(theta)**wind_n
          !else
          !  temp= windcone_rho%a*sin(theta)**windcone_rho%n+windcone_rho%b
          !end if

          do i = isg, ism1
            !dfl(i,j,k)= ttspline( x2b(j) &
            !    , &
            !    temp * (x1ap(isp)*x1bi(i))**wind_radial_n_ghosts &
            !    , &
            !    dfl(is,j,k) )

            !dfl(i,j,k)=dfl(i+ie-ism1,j,k)   ! Periodic BC
            dfl(i, j, k) = dfl(is, j, k)        ! Ouflow (continuity) BC
            !dfl(i,j,k)=dfl(is+ism1-i,j,k) ! Reflective BC
          end do
        end do
      end do
    end if
    if (face == 1 .and. side == 2) then
      do k = ksg, keg
        do j = jsg, jeg
          do i = iep1, ieg
            !dfl(i,j,k)=dfl(i+is-iep1,j,k)   ! Periodic BC

            dfl(i, j, k) = dfl(ie, j, k)
            ! Ouflow (continuity) BC

            ! dfl(i,j,k)=dfl(ie+iep1-i,j,k) ! Reflective BC
          end do
        end do
      end do
    end if

    if (face == 2 .and. side == 1) then
      do k = ksg, keg
        kpi = k + knac/2; if (kpi > ke) kpi = kpi - knac
        do j = jsg, jsm1
          do i = isg, ieg
            !dfl(i,j,k)=dfl(i,j+je-jsm1,k)   ! Periodic BC
            ! dfl(i,j,k)=dfl(i,js,k)        ! Ouflow (continuity) BC
            dfl(i, j, k) = dfl(i, js + jsm1 - j, k) ! Reflective BC
            ! dfl(i,j,k)=dfl(i,js+jsm1-j,kpi) ! kpi BC
          end do
        end do
      end do
    end if
    if (face == 2 .and. side == 2) then
      do k = ksg, keg
        kpi = k + knac/2; if (kpi > ke) kpi = kpi - knac
        do j = jep1, jeg
          do i = isg, ieg
            !dfl(i,j,k)=dfl(i,j+js-jep1,k)   ! Periodic BC
            ! dfl(i,j,k)=dfl(i,je,k)        ! Ouflow (continuity) BC
            dfl(i, j, k) = dfl(i, je + jep1 - j, k) ! Reflective BC
            !dfl(i,j,k)=dfl(i,je+jep1-j,kpi) ! kpi BC
          end do
        end do
      end do
    end if

    if (face == 3 .and. side == 1) then
      do k = ksg, ksm1
        do j = jsg, jeg
          do i = isg, ieg
            dfl(i, j, k) = dfl(i, j, k + ke - ksm1)   ! Periodic BC
            ! dfl(i,j,k)=dfl(i,j,ks)        ! Ouflow (continuity) BC
            ! dfl(i,j,k)=dfl(i,j,ks+ksm1-k) ! Reflective BC
          end do
        end do
      end do
    end if
    if (face == 3 .and. side == 2) then
      do k = kep1, keg
        do j = jsg, jeg
          do i = isg, ieg
            dfl(i, j, k) = dfl(i, j, k + ks - kep1)   ! Periodic BC
            ! dfl(i,j,k)=dfl(i,j,ke)        ! Ouflow (continuity) BC
            ! dfl(i,j,k)=dfl(i,j,ke+kep1-k) ! Reflective BC
          end do
        end do
      end do
    end if
    !
  END SUBROUTINE boundary_dfl
  !
  !=======================================================================
  !
  SUBROUTINE boundary_e_(face, side)
    CHARACTER(LEN=*), PARAMETER :: routinename = 'z_boundary_e_'
    INTEGER, INTENT(IN) :: face, side
    !
    INTEGER i, j, k
    INTEGER kpi
    REALNUM temp, theta

    if (ienergy /= 1) return
    if (ienergy == 2) &
      call finalstop(routinename, 'Not implemented in this version')

    if (face == 1 .and. side == 1) then
      do k = ksg, keg
        do j = jsg, jeg
          !theta=x2b(j)
          !theta=abs(min(theta,pi-theta))

          !if (theta>=windcone_rho%theta) then
          !  temp= (wind_r0/x1ap(isp))**2 * &
          !      wind_rho0/sin(theta)**wind_n
          !else
          !  temp= windcone_rho%a*sin(theta)**windcone_rho%n+windcone_rho%b
          !end if
          !temp=ciso**2*temp

          do i = isg, ism1
            !e_(i,j,k)=  ttspline( x2b(j) &
            !    , &
            !    temp * (x1ap(isp)*x1bi(i))**wind_radial_n_ghosts &
            !    , &
            !    e_(is,j,k) )

            !e_(i,j,k)=e_(i+ie-ism1,j,k)   ! Periodic BC
            e_(i, j, k) = e_(is, j, k)        ! Ouflow (continuity) BC
            !e_(i,j,k)=e_(is+ism1-i,j,k) ! Reflective BC
          end do
        end do
      end do
    end if
    if (face == 1 .and. side == 2) then
      do k = ksg, keg
        do j = jsg, jeg
          do i = iep1, ieg
            !e_(i,j,k)=e_(i+is-iep1,j,k)   ! Periodic BC
            e_(i, j, k) = e_(ie, j, k)        ! Ouflow (continuity) BC
            ! e_(i,j,k)=e_(ie+iep1-i,j,k) ! Reflective BC
          end do
        end do
      end do
    end if

    if (face == 2 .and. side == 1) then
      do k = ksg, keg
        kpi = k + knac/2; if (kpi > ke) kpi = kpi - knac
        do j = jsg, jsm1
          do i = isg, ieg
            !e_(i,j,k)=e_(i,j+je-jsm1,k)   ! Periodic BC
            ! e_(i,j,k)=e_(i,js,k)        ! Ouflow (continuity) BC
            e_(i, j, k) = e_(i, js + jsm1 - j, k) ! Reflective BC
            ! e_(i,j,k)=e_(i,js+jsm1-j,kpi) ! kpi BC
          end do
        end do
      end do
    end if
    if (face == 2 .and. side == 2) then
      do k = ksg, keg
        kpi = k + knac/2; if (kpi > ke) kpi = kpi - knac
        do j = jep1, jeg
          do i = isg, ieg
            !e_(i,j,k)=e_(i,j+js-jep1,k)   ! Periodic BC
            ! e_(i,j,k)=e_(i,je,k)        ! Ouflow (continuity) BC
            e_(i, j, k) = e_(i, je + jep1 - j, k) ! Reflective BC
            ! e_(i,j,k)=e_(i,je+jep1-j,kpi) ! kpi BC
          end do
        end do
      end do
    end if

    if (face == 3 .and. side == 1) then
      do k = ksg, ksm1
        do j = jsg, jeg
          do i = isg, ieg
            e_(i, j, k) = e_(i, j, k + ke - ksm1)   ! Periodic BC
            ! e_(i,j,k)=e_(i,j,ks)        ! Ouflow (continuity) BC
            ! e_(i,j,k)=e_(i,j,ks+ksm1-k) ! Reflective BC
          end do
        end do
      end do
    end if
    if (face == 3 .and. side == 2) then
      do k = kep1, keg
        do j = jsg, jeg
          do i = isg, ieg
            e_(i, j, k) = e_(i, j, k + ks - kep1)   ! Periodic BC
            ! e_(i,j,k)=e_(i,j,ke)        ! Ouflow (continuity) BC
            ! e_(i,j,k)=e_(i,j,ke+kep1-k) ! Reflective BC
          end do
        end do
      end do
    end if
    !
  END SUBROUTINE boundary_e_
  !
  !=======================================================================
  !
  SUBROUTINE boundary_etas(face, side)
    INTEGER, INTENT(IN) :: face, side
    !
    INTEGER i, j, k
    INTEGER kpi
    REALNUM temp, theta

    if (face == 1 .and. side == 1) then
      do k = ksg, keg
        do j = jsg, jeg
          do i = isgo, ism1
            ! Outflow (continuity) BC
            ohmic_eta_array(i, j, k) = ohmic_eta_array(is, j, k)
            ad_eta_array(i, j, k) = ad_eta_array(is, j, k)
            hall_eta_array(i, j, k) = hall_eta_array(is, j, k)
            xr_array(i, j, k) = xr_array(is, j, k)
            cr_array(i, j, k) = cr_array(is, j, k)
          end do
        end do
      end do
    end if
    if (face == 1 .and. side == 2) then
      do k = ksg, keg
        do j = jsg, jeg
          do i = iep1, iego
            ! Outflow (continuity) BC
            ohmic_eta_array(i, j, k) = ohmic_eta_array(ie, j, k)
            ad_eta_array(i, j, k) = ad_eta_array(ie, j, k)
            hall_eta_array(i, j, k) = hall_eta_array(ie, j, k)
            xr_array(i, j, k) = xr_array(ie, j, k)
            cr_array(i, j, k) = cr_array(ie, j, k)
          end do
        end do
      end do
    end if

    if (face == 2 .and. side == 1) then
      do k = ksg, keg
        kpi = k + knac/2; if (kpi > ke) kpi = kpi - knac
        do j = jsgo, jsm1
          do i = isg, ieg
            ! Reflective BC
            ohmic_eta_array(i, j, k) = ohmic_eta_array(i, js + jsm1 - j, k)
            ad_eta_array(i, j, k) = ad_eta_array(i, js + jsm1 - j, k)
            hall_eta_array(i, j, k) = hall_eta_array(i, js + jsm1 - j, k)
            xr_array(i, j, k) = xr_array(i, js + jsm1 - j, k)
            cr_array(i, j, k) = cr_array(i, js + jsm1 - j, k)
          end do
        end do
      end do
    end if
    if (face == 2 .and. side == 2) then
      do k = ksg, keg
        kpi = k + knac/2; if (kpi > ke) kpi = kpi - knac
        do j = jep1, jego
          do i = isg, ieg
            ! Reflective BC
            ohmic_eta_array(i, j, k) = ohmic_eta_array(i, je + jep1 - j, k)
            ad_eta_array(i, j, k) = ad_eta_array(i, je + jep1 - j, k)
            hall_eta_array(i, j, k) = hall_eta_array(i, je + jep1 - j, k)
            xr_array(i, j, k) = xr_array(i, je + jep1 - j, k)
            cr_array(i, j, k) = cr_array(i, je + jep1 - j, k)
          end do
        end do
      end do
    end if

    if (face == 3 .and. side == 1) then
      do k = ksgo, ksm1
        do j = jsg, jeg
          do i = isg, ieg
            ! Periodic BC
            ohmic_eta_array(i, j, k) = ohmic_eta_array(i, j, k + ke - ksm1)
            ad_eta_array(i, j, k) = ad_eta_array(i, j, k + ke - ksm1)
            hall_eta_array(i, j, k) = hall_eta_array(i, j, k + ke - ksm1)
            xr_array(i, j, k) = xr_array(i, j, k + ke - ksm1)
            cr_array(i, j, k) = cr_array(i, j, k + ke - ksm1)
          end do
        end do
      end do
    end if
    if (face == 3 .and. side == 2) then
      do k = kep1, kego
        do j = jsg, jeg
          do i = isg, ieg
            ! Periodic BC
            ohmic_eta_array(i, j, k) = ohmic_eta_array(i, j, k + ks - kep1)
            ad_eta_array(i, j, k) = ad_eta_array(i, j, k + ks - kep1)
            hall_eta_array(i, j, k) = hall_eta_array(i, j, k + ks - kep1)
            xr_array(i, j, k) = xr_array(i, j, k + ks - kep1)
            cr_array(i, j, k) = cr_array(i, j, k + ks - kep1)
          end do
        end do
      end do
    end if
    !
  END SUBROUTINE boundary_etas
  !
  !=======================================================================
  !
  SUBROUTINE boundary_v1(face, side)
    INTEGER, INTENT(IN) :: face, side
    INTEGER i, j, k
    INTEGER kpi
    REALNUM temp, theta, delta, vr

    if (face == 1 .and. side == 1) then
      do k = ksg, keg
        do j = jsg, jeg
          !theta=x2b(j)
          !theta=min(theta,pi-theta)
          ! delta_edge is in degrees
          ! theta is in radians
          ! theta_max_wind%transition is in degrees
          ! delta is in radians
          !delta=delta_edge*theta/theta_max_wind%transition
          !vr=wind_v0*cos(delta)

          !temp = ttspline( x2b(j), &
          !    vr, &
          !    min(v1(is+ione,j,k),vg1(is)) )

          !do i=isg,is
          !    v1(i,j,k)=temp
          !end do

          ! Periodic BC
          !do i=isg,ism1
          !   v1(i,j,k)=v1(i+ie-ism1,j,k)
          !end do

           !! Ouflow (continuity) BC
          temp = min(v1(is + ione, j, k), vg1(is))
          do i = isg, is
            v1(i, j, k) = temp
          end do

           !! Reflective BC
          !temp = vg1(is)
          !v1(is,j,k)=temp
          !do i=isg,ism1
          !   v1(i,j,k)=two*temp-v1(2*is-i,j,k)
          !end do

        end do
      end do
    end if
    if (face == 1 .and. side == 2) then
      do k = ksg, keg
        do j = jsg, jeg

          ! Periodic BC
          !do i=iep1,ieg
          !   v1(i,j,k)=v1(i+is-iep1,j,k)
          !end do

           !! Ouflow (continuity) BC
          temp = max(v1(ie, j, k), vg1(iep1))
          do i = iep1, ieg
            v1(i, j, k) = temp
          end do

           !! Reflective BC
          !temp = vg1(iep1)
          !v1(iep1,j,k)=temp
          !do i=iep2,ieg
          !   v1(i,j,k)=two*temp-v1(2*iep1-i,j,k)
          !end do

        end do
      end do
    end if

    if (face == 2 .and. side == 1) then
      do k = ksg, keg
        kpi = k + knac/2; if (kpi > ke) kpi = kpi - knac
        do j = jsg, jsm1
          do i = isg, ieg
            ! v1(i,j,k)=v1(i,j+je-jsm1,k)   ! Periodic BC
            ! v1(i,j,k)=v1(i,js,k)        ! Ouflow (continuity) BC
            v1(i, j, k) = v1(i, js + jsm1 - j, k) ! Reflective BC
            ! v1(i,j,k)=v1(i,js+jsm1-j,kpi) ! kpi BC
          end do
        end do
      end do
    end if
    if (face == 2 .and. side == 2) then
      do k = ksg, keg
        kpi = k + knac/2; if (kpi > ke) kpi = kpi - knac
        do j = jep1, jeg
          do i = isg, ieg
            ! v1(i,j,k)=v1(i,j+js-jep1,k)   ! Periodic BC
            ! v1(i,j,k)=v1(i,je,k)        ! Ouflow (continuity) BC
            v1(i, j, k) = v1(i, je + jep1 - j, k) ! Reflective BC
            ! v1(i,j,k)=v1(i,je+jep1-j,kpi) ! kpi BC
          end do
        end do
      end do
    end if

    if (face == 3 .and. side == 1) then
      do k = ksg, ksm1
        do j = jsg, jeg
          do i = isg, ieg
            v1(i, j, k) = v1(i, j, k + ke - ksm1)   ! Periodic BC
            ! v1(i,j,k)=v1(i,j,ks)        ! Ouflow (continuity) BC
            ! v1(i,j,k)=v1(i,j,ks+ksm1-k) ! Reflective BC
          end do
        end do
      end do
    end if
    if (face == 3 .and. side == 2) then
      do k = kep1, keg
        do j = jsg, jeg
          do i = isg, ieg
            v1(i, j, k) = v1(i, j, k + ks - kep1)   ! Periodic BC
            ! v1(i,j,k)=v1(i,j,ke)        ! Ouflow (continuity) BC
            ! v1(i,j,k)=v1(i,j,ke+kep1-k) ! Reflective BC
          end do
        end do
      end do
    end if
    !
  END SUBROUTINE boundary_v1
  !
  !=======================================================================
  !
  SUBROUTINE boundary_v2(face, side)
    INTEGER, INTENT(IN) :: face, side
    INTEGER i, j, k
    INTEGER kpi
    REALNUM temp, delta, theta, vtheta
    ! REALNUM vx, vy

    if (.not. withv2) return

    if (face == 1 .and. side == 1) then
      do k = ksg, keg
        do j = jsg, jeg
          !theta=x2a(j)
          !theta=min(theta,pi-theta)
          ! delta_edge is in degrees
          ! theta is in radians
          ! theta_max_wind%transition is in degrees
          ! delta is in radians
          !delta=delta_edge*theta/theta_max_wind%transition
          !vtheta=wind_v0*sin(delta)*sign(one,haf*pi-x2a(j))

          !temp=ttspline( theta, &
          !    vtheta, &
          !    v2(is,j,k) )

          do i = isg, ism1
            !v2(i,j,k)=temp
            !v2(i,j,k)=v2(i+ie-ism1,j,k)   ! Periodic BC
            v2(i, j, k) = v2(is, j, k)        ! Ouflow (continuity) BC
            ! v2(i,j,k)=v2(is+ism1-i,j,k) ! Reflective BC
            ! v2(i,j,k)=-v2(is+ism1-i,j,k) ! Reflective- BC
          end do
        end do
      end do
    end if
    if (face == 1 .and. side == 2) then
      do k = ksg, keg
        do j = jsg, jeg
          do i = iep1, ieg
            !v2(i,j,k)=v2(i+is-iep1,j,k)   ! Periodic BC
            v2(i, j, k) = v2(ie, j, k)        ! Ouflow (continuity) BC
            ! v2(i,j,k)=v2(ie+iep1-i,j,k) ! Reflective BC
          end do
        end do
      end do
    end if

    if (face == 2 .and. side == 1) then
      do k = ksg, keg
        kpi = k + knac/2; if (kpi > ke) kpi = kpi - knac
        do i = isg, ieg

          ! Periodic BC
          !do j=jsg,jsm1
          !   v2(i,j,k)=v2(i,j+je-jsm1,k)
          !end do

           !! Ouflow (continuity) BC
          !temp = min(v2(i,js+1,k),vg2(js))
          !do j=jsg,js
          !   v2(i,j,k)=temp
          !end do

          ! Reflective BC
          temp = vg2(js)
          v2(i, js, k) = temp
          do j = jsg, jsm1
            v2(i, j, k) = two*temp - v2(i, 2*js - j, k)
          end do

           !! line average
          !v2(i,js,k) = haf*(v2(i,js+1,k)-v2(i,js+1,kpi))
          ! wall
          !v2(i,js,k) = zro
          ! kpi BC
          !do j=jsg,jsm1
          !   v2(i,j,k)=-v2(i,2*js-j,kpi)
          !end do

        end do
      end do
     !! Circle average
      !do i=isg,ieg
      !  vx=zro
      !  vy=zro
      !  do k=ks,ke
      !    vx=vx &
      !        +v2(i,js+1,k)*cos(x3b(k))*dx3a(k) &
      !        -v3(i,js,k)*sin(x3a(k))*dx3b(k)
      !    vy=vy &
      !        +v2(i,js+1,k)*sin(x3b(k))*dx3a(k) &
      !        +v3(i,js,k)*cos(x3a(k))*dx3b(k)
      !  end do
      !  vx=vx/(two*pi)
      !  vy=vy/(two*pi)
      !  temp=cos(x3b(k))*vx+sin(x3b(k))*vy
      !  v2(i,js,:)=temp
      !end do

    end if
    if (face == 2 .and. side == 2) then
      do k = ksg, keg
        kpi = k + knac/2; if (kpi > ke) kpi = kpi - knac
        do i = isg, ieg

          ! Periodic BC
          !do j=jep1,jeg
          !   v2(i,j,k)=v2(i,j+js-jep1,k)
          !end do

           !! Ouflow (continuity) BC
          !temp = max(v2(i,je,k),vg2(jep1))
          !do j=jep1,jeg
          !   v2(i,j,k)=temp
          !end do

          ! Reflective BC
          temp = vg2(jep1)
          v2(i, jep1, k) = temp
          do j = jep2, jeg
            v2(i, j, k) = two*temp - v2(i, 2*jep1 - j, k)
          end do

           !! line average
          !v2(i,jep1,k) = haf*(v2(i,je,k)-v2(i,je,kpi))
          ! wall
          !v2(i,jep1,k) = zro
          ! kpi BC
          !do j=jep2,jeg
          !   v2(i,j,k)=-v2(i,2*jep1-j,kpi)
          !end do

        end do
      end do
     !! Circle average
      !do i=isg,ieg
      !  vx=zro
      !  vy=zro
      !  do k=ks,ke
      !    vx=vx &
      !        -v2(i,je,k)*cos(x3b(k))*dx3a(k) &
      !        -v3(i,je,k)*sin(x3a(k))*dx3b(k)
      !    vy=vy &
      !        -v2(i,je,k)*sin(x3b(k))*dx3a(k) &
      !        +v3(i,je,k)*cos(x3a(k))*dx3b(k)
      !  end do
      !  vx=vx/(two*pi)
      !  vy=vy/(two*pi)
      !  temp=-cos(x3b(k))*vx-sin(x3b(k))*vy
      !  v2(i,jep1,:)=temp
      !end do

    end if

    if (face == 3 .and. side == 1) then
      do k = ksg, ksm1
        do j = jsg, jeg
          do i = isg, ieg
            v2(i, j, k) = v2(i, j, k + ke - ksm1)   ! Periodic BC
            ! v2(i,j,k)=v2(i,j,ks)        ! Ouflow (continuity) BC
            ! v2(i,j,k)=v2(i,j,ks+ksm1-k) ! Reflective BC
          end do
        end do
      end do
    end if
    if (face == 3 .and. side == 2) then
      do k = kep1, keg
        do j = jsg, jeg
          do i = isg, ieg
            v2(i, j, k) = v2(i, j, k + ks - kep1)   ! Periodic BC
            ! v2(i,j,k)=v2(i,j,ke)        ! Ouflow (continuity) BC
            ! v2(i,j,k)=v2(i,j,ke+kep1-k) ! Reflective BC
          end do
        end do
      end do
    end if

  END SUBROUTINE boundary_v2
  !
  !=======================================================================
  !
  SUBROUTINE boundary_v3(face, side)
    INTEGER, INTENT(IN) :: face, side
    INTEGER i, j, k
    INTEGER kpi
    REALNUM temp

    if (.not. withv3) return

    if (face == 1 .and. side == 1) then
      do k = ksg, keg
        do j = jsg, jeg

          !temp=ttspline( x2b(j), &
          !    zro, &
          !    v3(is,j,k) )

          do i = isg, ism1
            !v3(i,j,k)=temp
            ! v3(i,j,k)=v3(i+ie-ism1,j,k)   ! Periodic BC
            v3(i, j, k) = v3(is, j, k)        ! Ouflow (continuity) BC
            ! v3(i,j,k)=v3(is+ism1-i,j,k) ! Reflective BC
            ! v3(i,j,k)=-v3(is+ism1-i,j,k)! Reflective- BC
          end do
        end do
      end do
    end if
    if (face == 1 .and. side == 2) then
      do k = ksg, keg
        do j = jsg, jeg
          do i = iep1, ieg
            !v3(i,j,k)=v3(i+is-iep1,j,k)   ! Periodic BC
            v3(i, j, k) = v3(ie, j, k)        ! Ouflow (continuity) BC
            ! v3(i,j,k)=v3(ie+iep1-i,j,k) ! Reflective BC
          end do
        end do
      end do
    end if

    if (face == 2 .and. side == 1) then
      do k = ksg, keg
        kpi = k + knac/2; if (kpi > ke) kpi = kpi - knac
        do j = jsg, jsm1
          do i = isg, ieg
            ! v3(i,j,k)=v3(i,j+je-jsm1,k)   ! Periodic BC
            ! v3(i,j,k)=v3(i,js,k)        ! Ouflow (continuity) BC
            ! v3(i,j,k)=v3(i,js+jsm1-j,k) ! Reflective BC
            v3(i, j, k) = -v3(i, js + jsm1 - j, k)! Reflective- BC
            ! v3(i,j,k)=-v3(i,js+jsm1-j,kpi)  ! kpi BC
          end do
        end do
      end do
    end if
    if (face == 2 .and. side == 2) then
      do k = ksg, keg
        kpi = k + knac/2; if (kpi > ke) kpi = kpi - knac
        do j = jep1, jeg
          do i = isg, ieg
            ! v3(i,j,k)=v3(i,j+js-jep1,k)   ! Periodic BC
            ! v3(i,j,k)=v3(i,je,k)        ! Ouflow (continuity) BC
            ! v3(i,j,k)=v3(i,je+jep1-j,k) ! Reflective BC
            ! v3(i,j,k)=-v3(i,je+jep1-j,k)! Reflective- BC
            ! v3(i,j,k)=-v3(i,je+jep1-j,kpi)! kpi BC
            v3(i, j, k) = bc_sign*v3(i, je + jep1 - j, k) ! +/- Reflective BC
          end do
        end do
      end do
    end if

    if (face == 3 .and. side == 1) then
      do j = jsg, jeg
        do i = isg, ieg

          ! Periodic BC
          do k = ksg, ksm1
            v3(i, j, k) = v3(i, j, k + ke - ksm1)
          end do

           !! Ouflow (continuity) BC
          !temp = min(v3(i,j,ksp1),vg3(ks))
          !do k=ksg,ks
          !   v3(i,j,k)=temp
          !end do

           !! Reflective BC
          !temp = vg3(ks)
          !v3(i,j,ks)=temp
          !do k=ksg,ksm1
          !   v3(i,j,k)=two*temp-v3(i,j,2*ks-k)
          !end do

        end do
      end do
    end if
    if (face == 3 .and. side == 2) then
      do j = jsg, jeg
        do i = isg, ieg

          ! Periodic BC
          do k = kep1, keg
            v3(i, j, k) = v3(i, j, k + ks - kep1)
          end do

           !! Ouflow (continuity) BC
          !temp = max(v3(i,j,ke),vg3(kep1))
          !do k=kep1,keg
          !   v3(i,j,k)=temp
          !end do

           !! Reflective BC
          !temp = vg3(kep1)
          !v3(i,j,kep1)=temp
          !do k=kep2,keg
          !   v3(i,j,k)=two*temp-v3(i,j,2*kep1-k)
          !end do

        end do
      end do
    end if
    !
  END SUBROUTINE boundary_v3
  !
  !=======================================================================
  !
  SUBROUTINE boundary_b3(face, side)
    INTEGER, INTENT(IN) :: face, side
    INTEGER i, j, k
    REALNUM temp, theta, signvalue
    REALNUM tempminus
    REALNUM wind_v0_scale

    ! Invalid or unimplemented usage
    if (jnac == 0 .or. imhd == 0 .or. .not. withb3 .or. .not. withb3bc) return
    ! Allowed to be used with mhddimension /= 2; it may be useful sometimes.
    ! Allowed to be used with knac/=0.  It may be useful sometimes.

    !if (wind_bphi_unchanging) then
    !  wind_v0_scale=wind_v0_nominal
    !else
    !  wind_v0_scale=wind_v0
    !end if

    if (face == 1 .and. side == 1) then
      do k = ksg, keg
        do j = jsg, jeg
          !theta=x2b(j)
          !theta=abs(min(theta,pi-theta))

          !if (theta>=windcone_bphi%theta) then
          !  temp= (wind_r0/x1ap(isp)) * &
          !      wind_v0_scale*wind_bphi_alfven_number_inverse*&
          !      sqrt(wind_rho0)/&
          !      sin(theta)**(haf*wind_n)
          !else
          !  temp= (wind_r0/x1ap(isp)) * &
          !      wind_v0_scale*wind_bphi_alfven_number_inverse*&
          !      sqrt(wind_rho0)/&
          !      sin(windcone_bphi%theta)**(haf*wind_n)
          !  tempminus=windcone_bphi%alpha*temp+windcone_bphi%beta
          !  if (windcone_bphi%n==zro) then
          !    windcone_bphi%b=tempminus
          !    windcone_bphi%a=zro
          !  else
          !    windcone_bphi%a=(tempminus-windcone_bphi%b)/ &
          !        sin(windcone_bphi%theta)**windcone_bphi%n
          !  end if
          !  temp= windcone_bphi%a*sin(theta)**windcone_bphi%n+windcone_bphi%b
          !end if

          do i = isg, ism1
            !b3(i,j,k)= ttspline( x2b(j) &
            !    , &
            !    temp * (x1ap(isp)*x1bi(i))**(haf*wind_radial_n_ghosts) &
            !    , &
            !    b3(is,j,k) )

            ! b3(i,j,k)=b3(i+ie-ism1,j,k)   ! Periodic BC
            !b3(i,j,k)=b3(is,j,k)        ! Ouflow (continuity) BC
            ! b3(i,j,k)=b3(is+ism1-i,j,k) ! Reflective BC
            b3(i, j, k) = zro                 ! Fixed to zero
          end do
        end do
      end do
    end if
    if (face == 1 .and. side == 2) then
      do k = ksg, keg
        do j = jsg, jeg
          do i = iep1, ieg
            ! b3(i,j,k)=b3(i+is-iep1,j,k)   ! Periodic BC
            b3(i, j, k) = b3(ie, j, k)        ! Ouflow (continuity) BC
            ! b3(i,j,k)=b3(ie+iep1-i,j,k) ! Reflective BC
          end do
        end do
      end do
    end if

    if (face == 2 .and. side == 1) then
      if (bphiaxiszero) then
        b3(:, js, :) = zro
      end if
      do k = ksg, keg
        do j = jsg, jsm1
          do i = isg, ieg
            ! b3(i,j,k)=b3(i,j+je-jsm1,k)   ! Periodic BC
            ! b3(i,j,k)=b3(i,js,k)        ! Ouflow (continuity) BC
            ! b3(i,j,k)=b3(i,js+jsm1-j,k) ! Reflective BC
            ! b3(i,j,k)=-b3(i,js+jsm1-j,k) ! Reflective- BC
            b3(i, j, k) = zro                 ! Fixed to zero
          end do
        end do
      end do
    end if

    if (face == 2 .and. side == 2) then
      if (bc_sign == -one) then
        if (bphiaxiszero) then
          b3(:, je, :) = zro
        end if
      end if
      do k = ksg, keg
        do j = jep1, jeg
          do i = isg, ieg
            ! b3(i,j,k)=b3(i,j+js-jep1,k)   ! Periodic BC
            ! b3(i,j,k)=b3(i,je,k)        ! Ouflow (continuity) BC
            ! b3(i,j,k)=b3(i,je+jep1-j,k) ! Reflective BC
            ! b3(i,j,k)=-b3(i,je+jep1-j,k) ! Reflective- BC
            b3(i, j, k) = zro                 ! Fixed to zero
          end do
        end do
      end do
    end if

    if (face == 3 .and. side == 1) then
      do k = ksg, ksm1
        do j = jsg, jeg
          do i = isg, ieg
            b3(i, j, k) = b3(i, j, k + ke - ksm1)   ! Periodic BC
            ! b3(i,j,k)=b3(i,j,ks)        ! Ouflow (continuity) BC
            ! b3(i,j,k)=b3(i,j,ks+ksm1-k) ! Reflective BC
          end do
        end do
      end do
    end if
    if (face == 3 .and. side == 2) then
      do k = kep1, keg
        do j = jsg, jeg
          do i = isg, ieg
            b3(i, j, k) = b3(i, j, k + ks - kep1)   ! Periodic BC
            ! b3(i,j,k)=b3(i,j,ke)        ! Ouflow (continuity) BC
            ! b3(i,j,k)=b3(i,j,ke+kep1-k) ! Reflective BC
          end do
        end do
      end do
    end if
    !
  END SUBROUTINE boundary_b3
  !
  !=======================================================================
  !
  SUBROUTINE boundary_tf(face, side)
    CHARACTER(LEN=*), PARAMETER :: routinename = 'z_boundary_tf'
    INTEGER, INTENT(IN) :: face, side
    !
    call finalstop(routinename, 'Not used in this version')
    !
  END SUBROUTINE boundary_tf
  !
  !=======================================================================
  !
  SUBROUTINE boundary_emf_dx(face, side, emfdx1, emfdx2, emfdx3)
    CHARACTER(LEN=*), PARAMETER :: routinename = 'z_boundary_emf_dx'
    INTEGER, INTENT(IN) :: face, side
    REALNUM, DIMENSION(:, :, :), INTENT(INOUT) :: emfdx1, emfdx2, emfdx3
    !
    call finalstop(routinename, 'Not used in this version')
    !
  END SUBROUTINE boundary_emf_dx
  !
  !=======================================================================
  !
  SUBROUTINE boundary_emf(face, side, emf1, emf2, emf3)
    CHARACTER(LEN=*), PARAMETER :: routinename = 'z_boundary_emf'
    INTEGER, INTENT(IN) :: face, side
    REALNUM, DIMENSION(:, :, :), INTENT(INOUT) :: emf1, emf2, emf3
    !
    call finalstop(routinename, 'Not used in this version')
    !
  END SUBROUTINE boundary_emf
  !
  !=======================================================================
  !
  SUBROUTINE boundary_emf1(face, side, emf1, emf2, emf3)
    INTEGER, INTENT(IN) :: face, side
    REALNUM, DIMENSION(:, :, :), INTENT(INOUT) :: emf1
    REALNUM, DIMENSION(:, :, :), INTENT(IN) :: emf2, emf3

    INTEGER i, j, k
    INTEGER kpi
    REALNUM temp, theta, vtheta, delta
    REALNUM tempminus
    REALNUM wind_v0_scale
    ! REALNUM emf1sum, emf1average

    !if (wind_bphi_unchanging) then
    !  wind_v0_scale=wind_v0_nominal
    !else
    !  wind_v0_scale=wind_v0
    !end if

    if (face == 1 .and. side == 1) then
      do k = ksg, keg
        do j = jsg, jeg
          !theta=x2a(j)
          !theta=abs(min(theta,pi-theta))
          !if (theta>=windcone_bphi%theta) then
          !  temp= (wind_r0/x1ap(isp)) * &
          !      wind_v0_scale*wind_bphi_alfven_number_inverse*&
          !      sqrt(wind_rho0)/&
          !      sin(theta)**(haf*wind_n)
          !else
          !  temp= (wind_r0/x1ap(isp)) * &
          !      wind_v0_scale*wind_bphi_alfven_number_inverse*&
          !      sqrt(wind_rho0)/&
          !      sin(windcone_bphi%theta)**(haf*wind_n)
          !  tempminus=windcone_bphi%alpha*temp+windcone_bphi%beta
          !  if (windcone_bphi%n==zro) then
          !    windcone_bphi%b=tempminus
          !    windcone_bphi%a=zro
          !  else
          !    windcone_bphi%a=(tempminus-windcone_bphi%b)/ &
          !        sin(windcone_bphi%theta)**windcone_bphi%n
          !  end if
          !  temp= windcone_bphi%a*sin(theta)**windcone_bphi%n+windcone_bphi%b
          !end if

          ! delta_edge is in degrees
          ! theta is in radians
          ! theta_max_wind%transition is in degrees
          ! delta is in radians
          !delta=delta_edge*theta/theta_max_wind%transition
          !temp=wind_v0_scale*temp*sin(delta)

          do i = isg, ism1
            !emf1(i,j,k)= ttspline( x2a(j) &
            !    , &
            !    temp * (x1ap(isp)*x1bi(i))**(haf*wind_radial_n_ghosts) &
            !    , &
            !    emf1(is,j,k) )

            !emf1(i,j,k)=temp
            ! emf1(i,j,k)=emf1(i+ie-ism1,j,k)   ! Periodic BC
            emf1(i, j, k) = emf1(is, j, k)        ! Ouflow (continuity) BC
            ! emf1(i,j,k)=emf1(is+ism1-i,j,k) ! Reflective BC
            ! emf1(i,j,k)=-emf1(is+ism1-i,j,k)! Reflective- BC
          end do
        end do
      end do
    end if
    if (face == 1 .and. side == 2) then
      do k = ksg, keg
        do j = jsg, jeg
          do i = iep1, ieg
            ! emf1(i,j,k)=emf1(i+is-iep1,j,k)   ! Periodic BC
            emf1(i, j, k) = emf1(ie, j, k)        ! Ouflow (continuity) BC
            ! emf1(i,j,k)=emf1(ie+iep1-i,j,k) ! Reflective BC
            ! emf1(i,j,k)=-emf1(ie+iep1-i,j,k)! Reflective- BC
          end do
        end do
      end do
    end if

    if (face == 2 .and. side == 1) then
      do k = ksg, keg
        kpi = k + knac/2; if (kpi > ke) kpi = kpi - knac
        do i = isg, ieg

          !do j=jsg,jsm1
          !   emf1(i,j,k)=emf1(i,j+je-jsm1,k) ! Periodic BC
          !   ! emf1(i,j,k)=emf1(i,js,k)      ! Ouflow (continuity) BC
          !end do

          emf1(i, js, k) = zro ! Reflective+- BC
          do j = jsg, jsm1
            emf1(i, j, k) = emf1(i, 2*js - j, k)  ! Reflective BC
            ! emf1(i,j,k)=-emf1(i,2*js-j,k) ! Reflective- BC
          end do

           !! emf1(i,js,k)=haf*(emf1(i,js+1,k)+emf1(i,js+1,kpi)) ! line average
           !! kpi BC
          !do j=jsg,jsm1
          !    emf1(i,j,k)=emf1(i,2*js-j,kpi)  ! kpi BC
          !end do

        end do
      end do

     !! circle average
      !do i=isg,ieg
      !  emf1sum=zro
      !  do k=ks,ke
      !    emf1sum=emf1sum+emf1(i,js+1,k)*dx3a(k)
      !  end do
      !  emf1average=emf1sum/(two*pi)
      !  emf1(i,js,:)=emf1average
      !end do

    end if
    if (face == 2 .and. side == 2) then
      do k = ksg, keg
        kpi = k + knac/2; if (kpi > ke) kpi = kpi - knac
        do i = isg, ieg

          !do j=jep2,jeg
          !   emf1(i,j,k)=emf1(i,j+js-jep1,k) ! Periodic BC
          !   ! emf1(i,j,k)=emf1(i,jep1,k)    ! Ouflow (continuity) BC
          !end do

          emf1(i, jep1, k) = zro ! Reflective+- BC
          do j = jep2, jeg
            emf1(i, j, k) = emf1(i, 2*jep1 - j, k)  ! Reflective BC
            ! emf1(i,j,k)=-emf1(i,2*jep1-j,k) ! Reflective- BC
          end do

          ! emf1(i,jep1,k)=haf*(emf1(i,je,k)+emf1(i,je,kpi)) ! line average
          ! kpi BC
          !do j=jep2,jeg
          !    emf1(i,j,k)=emf1(i,2*jep1-j,kpi)  ! kpi BC
          !end do

        end do
      end do

     !! circle average
      !do i=isg,ieg
      !  emf1sum=zro
      !  do k=ks,ke
      !    emf1sum=emf1sum+emf1(i,je,k)*dx3a(k)
      !  end do
      !  emf1average=emf1sum/(two*pi)
      !  emf1(i,jep1,:)=emf1average
      !end do

    end if

    if (face == 3 .and. side == 1) then
      do j = jsg, jeg
        do i = isg, ieg

          do k = ksg, ksm1
            emf1(i, j, k) = emf1(i, j, k + ke - ksm1) ! Periodic BC
            ! emf1(i,j,k)=emf1(i,j,ks)      ! Ouflow (continuity) BC
          end do

          !emf1(i,j,ks)=zro ! Reflective+- BC
          !do k=ksg,ksm1
          ! emf1(i,j,k)=emf1(i,j,2*ks-k)  ! Reflective BC
          ! emf1(i,j,k)=-emf1(i,j,2*ks-k) ! Reflective- BC
          !end do

        end do
      end do
    end if
    if (face == 3 .and. side == 2) then
      do j = jsg, jeg
        do i = isg, ieg

          do k = kep2, keg
            emf1(i, j, k) = emf1(i, j, k + ks - kep1) ! Periodic BC
            ! emf1(i,j,k)=emf1(i,j,kep1)    ! Ouflow (continuity) BC
          end do

          !emf1(i,j,kep1)=zro ! Reflective+- BC
          !do k=kep2,keg
          ! emf1(i,j,k)=emf1(i,j,2*kep1-k)  ! Reflective BC
          ! emf1(i,j,k)=-emf1(i,j,2*kep1-k) ! Reflective- BC
          !end do

        end do
      end do
    end if

  END SUBROUTINE boundary_emf1
  !
  !=======================================================================
  !
  SUBROUTINE boundary_emf2(face, side, emf1, emf2, emf3)
    INTEGER, INTENT(IN) :: face, side
    REALNUM, DIMENSION(:, :, :), INTENT(INOUT) :: emf2
    REALNUM, DIMENSION(:, :, :), INTENT(IN) :: emf1, emf3

    INTEGER i, j, k
    INTEGER kpi
    REALNUM temp, theta, signvalue, delta
    REALNUM tempminus

    REALNUM wind_v0_scale

    !if (wind_bphi_unchanging) then
    !  wind_v0_scale=wind_v0_nominal
    !else
    !  wind_v0_scale=wind_v0
    !end if

    if (face == 2 .and. side == 1) then
      do k = ksg, keg
        kpi = k + knac/2; if (kpi > ke) kpi = kpi - knac
        do i = isg, ieg
          do j = jsg, jsm1
            ! emf2(i,j,k)=emf2(i,j+je-jsm1,k)   ! Periodic BC
            ! emf2(i,j,k)=emf2(i,js,k)        ! Ouflow (continuity) BC
            ! emf2(i,j,k)=emf2(i,js+jsm1-j,k) ! Reflective BC
            emf2(i, j, k) = -emf2(i, js + jsm1 - j, k)! Reflective- BC
            ! emf2(i,j,k)=-emf2(i,js+jsm1-j,kpi)! kpi BC
          end do
        end do
      end do
    end if
    if (face == 2 .and. side == 2) then
      do k = ksg, keg
        kpi = k + knac/2; if (kpi > ke) kpi = kpi - knac
        do i = isg, ieg
          do j = jep1, jeg
            ! emf2(i,j,k)=emf2(i,j+js-jep1,k)   ! Periodic BC
            ! emf2(i,j,k)=emf2(i,je,k)        ! Ouflow (continuity) BC
            ! emf2(i,j,k)=emf2(i,je+jep1-j,k) ! Reflective BC
            emf2(i, j, k) = -emf2(i, je + jep1 - j, k)! Reflective- BC
            ! emf2(i,j,k)=-emf2(i,je+jep1-j,k)  ! kpi BC
          end do
        end do
      end do
    end if

    if (face == 1 .and. side == 1) then
      do k = ksg, keg
        do j = jsg, jeg
          !theta=x2b(j)
          !theta=abs(min(theta,pi-theta))

          !if (theta>=windcone_bphi%theta) then
          !  temp= (wind_r0/x1ap(isp)) * &
          !      wind_v0_scale*wind_bphi_alfven_number_inverse*&
          !      sqrt(wind_rho0)/&
          !      sin(theta)**(haf*wind_n)
          !else
          !  temp= (wind_r0/x1ap(isp)) * &
          !      wind_v0_scale*wind_bphi_alfven_number_inverse*&
          !      sqrt(wind_rho0)/&
          !      sin(windcone_bphi%theta)**(haf*wind_n)
          !  tempminus=windcone_bphi%alpha*temp+windcone_bphi%beta
          !  if (windcone_bphi%n==zro) then
          !    windcone_bphi%b=tempminus
          !    windcone_bphi%a=zro
          !  else
          !    windcone_bphi%a=(tempminus-windcone_bphi%b)/ &
          !        sin(windcone_bphi%theta)**windcone_bphi%n
          !  end if
          !  temp= windcone_bphi%a*sin(theta)**windcone_bphi%n+windcone_bphi%b
          !end if

          ! delta_edge is in degrees
          ! theta is in radians
          ! theta_max_wind%transition is in degrees
          ! delta is in radians
          !delta=delta_edge*theta/theta_max_wind%transition
          !temp=-wind_v0_scale*temp*cos(delta)

          do i = isg, ism1
            !emf2(i,j,k)= ttspline( x2b(j) &
            !    , &
            !    temp * (x1ap(isp)*x1bi(i))**(haf*wind_radial_n_ghosts) &
            !    , &
            !    emf2(is,j,k) )
            ! emf2(i,j,k)=emf2(i+ie-ism1,j,k) ! Periodic BC
            emf2(i, j, k) = emf2(is, j, k)      ! Ouflow (continuity) BC
          end do

          !emf2(is,j,k)=zro ! Reflective+- BC
          !do i=isg,ism1
          ! emf2(i,j,k)=emf2(2*is-i,j,k)  ! Reflective BC
          ! emf2(i,j,k)=-emf2(2*is-i,j,k) ! Reflective- BC
          !end do

        end do
      end do
    end if
    if (face == 1 .and. side == 2) then
      do k = ksg, keg
        do j = jsg, jeg

          do i = iep2, ieg
            ! emf2(i,j,k)=emf2(i+is-iep1,j,k) ! Periodic BC
            emf2(i, j, k) = emf2(iep1, j, k)    ! Ouflow (continuity) BC
          end do

          !emf2(iep1,j,k)=zro ! Reflective+- BC
          !do i=iep2,ieg
          ! emf2(i,j,k)=emf2(2*iep1-i,j,k)  ! Reflective BC
          ! emf2(i,j,k)=-emf2(2*iep1-i,j,k) ! Reflective- BC
          !end do

        end do
      end do
    end if

    if (face == 3 .and. side == 1) then
      do j = jsg, jeg
        do i = isg, ieg

          do k = ksg, ksm1
            emf2(i, j, k) = emf2(i, j, k + ke - ksm1) ! Periodic BC
            ! emf2(i,j,k)=emf2(i,j,ks)      ! Ouflow (continuity) BC
          end do

          !emf2(i,j,ks)=zro ! Reflective+- BC
          !do k=ksg,ksm1
          ! emf2(i,j,k)=emf2(i,j,2*ks-k)  ! Reflective BC
          ! emf2(i,j,k)=-emf2(i,j,2*ks-k) ! Reflective- BC
          !end do

        end do
      end do
    end if
    if (face == 3 .and. side == 2) then
      do j = jsg, jeg
        do i = isg, ieg

          do k = kep2, keg
            emf2(i, j, k) = emf2(i, j, k + ks - kep1) ! Periodic BC
            ! emf2(i,j,k)=emf2(i,j,kep1)    ! Ouflow (continuity) BC
          end do

          !emf2(i,j,kep1)=zro ! Reflective+- BC
          !do k=kep2,keg
          ! emf2(i,j,k)=emf2(i,j,2*kep1-k)  ! Reflective BC
          ! emf2(i,j,k)=-emf2(i,j,2*kep1-k) ! Reflective- BC
          !end do

        end do
      end do
    end if

  END SUBROUTINE boundary_emf2
  !
  !=======================================================================
  !
  SUBROUTINE boundary_emf3(face, side, emf1, emf2, emf3)
    INTEGER, INTENT(IN) :: face, side
    REALNUM, DIMENSION(:, :, :), INTENT(INOUT) :: emf3
    REALNUM, DIMENSION(:, :, :), INTENT(IN) :: emf1, emf2

    INTEGER i, j, k
    INTEGER kpi

    REALNUM temp
    ! REALNUM ex, ey

    if (face == 3 .and. side == 1) then
      do j = jsg, jeg
        do i = isg, ieg
          do k = ksg, ksm1
            emf3(i, j, k) = emf3(i, j, k + ke - ksm1)   ! Periodic BC
            ! emf3(i,j,k)=emf3(i,j,ks)        ! Ouflow (continuity) BC
            ! emf3(i,j,k)=emf3(i,j,ks+ksm1-k) ! Reflective BC
            ! emf3(i,j,k)=-emf3(i,j,ks+ksm1-k)! Reflective- BC
          end do
        end do
      end do
    end if
    if (face == 3 .and. side == 2) then
      do j = jsg, jeg
        do i = isg, ieg
          do k = kep1, keg
            emf3(i, j, k) = emf3(i, j, k + ks - kep1)   ! Periodic BC
            ! emf3(i,j,k)=emf3(i,j,ke)        ! Ouflow (continuity) BC
            ! emf3(i,j,k)=emf3(i,j,ke+kep1-k) ! Reflective BC
            ! emf3(i,j,k)=-emf3(i,j,ke+kep1-k)! Reflective- BC
          end do
        end do
      end do
    end if

    if (face == 1 .and. side == 1) then
      do k = ksg, keg
        do j = jsg, jeg

          !temp = ttspline( x2a(j), &
          !   zro, &
          !   emf3(is,j,k) )

          do i = isg, ism1
            ! emf3(i,j,k)=emf3(i+ie-ism1,j,k) ! Periodic BC
            emf3(i, j, k) = emf3(is, j, k)      ! Ouflow (continuity) BC
            !emf3(i,j,k)=temp
          end do

          !emf3(is,j,k)=zro ! Reflective+- BC
          !do i=isg,ism1
          ! emf3(i,j,k)=emf3(2*is-i,j,k)  ! Reflective BC
          ! emf3(i,j,k)=-emf3(2*is-i,j,k) ! Reflective- BC
          !end do

        end do
      end do
    end if
    if (face == 1 .and. side == 2) then
      do k = ksg, keg
        do j = jsg, jeg

          do i = iep2, ieg
            ! emf3(i,j,k)=emf3(i+is-iep1,j,k) ! Periodic BC
            emf3(i, j, k) = emf3(iep1, j, k)    ! Ouflow (continuity) BC
          end do

          !emf3(iep1,j,k)=zro ! Reflective+- BC
          !do i=iep2,ieg
          ! emf3(i,j,k)=emf3(2*iep1-i,j,k)  ! Reflective BC
          ! emf3(i,j,k)=-emf3(2*iep1-i,j,k) ! Reflective- BC
          !end do

        end do
      end do
    end if

    if (face == 2 .and. side == 1) then
      do k = ksg, keg
        kpi = k + knac/2; if (kpi > ke) kpi = kpi - knac
        do i = isg, ieg

          !do j=jsg,jsm1
          !   emf3(i,j,k)=emf3(i,j+je-jsm1,k) ! Periodic BC
          !   ! emf3(i,j,k)=emf3(i,js,k)      ! Ouflow (continuity) BC
          !end do

          emf3(i, js, k) = zro ! Reflective+- BC
          do j = jsg, jsm1
            ! emf3(i,j,k)=emf3(i,2*js-j,k)  ! Reflective BC
            emf3(i, j, k) = -emf3(i, 2*js - j, k) ! Reflective- BC
          end do

          ! line average
          !emf3(i,js,k)=haf*(emf3(i,js+1,k)-emf3(i,js+1,kpi))
          ! kpi BC
          !do j=jsg,jsm1
          !   emf3(i,j,k)=-emf3(i,2*js-j,kpi) ! kpi BC
          !end do

        end do
      end do
     !! circle average
      !do i=isg,ieg
      !  ex=zro
      !  ey=zro
      !  do k=ks,ke
      !    ex=ex &
      !        +emf2(i,js,k)*cos(x3a(k))*dx3b(k) &
      !        -emf3(i,js+1,k)*sin(x3b(k))*dx3a(k)
      !    ey=ey &
      !        +emf2(i,js,k)*sin(x3a(k))*dx3b(k) &
      !        +emf3(i,js+1,k)*cos(x3b(k))*dx3a(k)
      !  end do
      !  ex=ex/(two*pi)
      !  ey=ey/(two*pi)
      !  temp=-sin(x3b(k))*ex+cos(x3b(k))*ey
      !  emf3(i,js,:)=temp
      !end do
    end if
    if (face == 2 .and. side == 2) then
      do k = ksg, keg
        kpi = k + knac/2; if (kpi > ke) kpi = kpi - knac
        do i = isg, ieg

          !do j=jep2,jeg
          !   emf3(i,j,k)=emf3(i,j+js-jep1,k) ! Periodic BC
          !   ! emf3(i,j,k)=emf3(i,jep1,k)    ! Ouflow (continuity) BC
          !end do

          emf3(i, jep1, k) = zro ! Reflective+- BC
          do j = jep2, jeg
            !emf3(i,j,k)=emf3(i,2*jep1-j,k)  ! Reflective BC
            !emf3(i,j,k)=-emf3(i,2*jep1-j,k) ! Reflective- BC
            emf3(i, j, k) = bc_sign*emf3(i, 2*jep1 - j, k)  ! +/- Reflective BC
          end do

          ! line average
          !emf3(i,jep1,k)=haf*(emf3(i,je,k)-emf3(i,je,kpi))
          ! kpi BC
          !do j=jep2,jeg
          !   emf3(i,j,k)=-emf3(i,2*jep1-j,kpi) ! kpi BC
          !end do

        end do
      end do
      !do i=isg,ieg
      !  ex=zro
      !  ey=zro
      !  do k=ks,ke
      !    ex=ex &
      !        -emf2(i,je,k)*cos(x3a(k))*dx3b(k) &
      !        -emf3(i,je,k)*sin(x3b(k))*dx3a(k)
      !    ey=ey &
      !        -emf2(i,je,k)*sin(x3a(k))*dx3b(k) &
      !        +emf3(i,je,k)*cos(x3b(k))*dx3a(k)
      !  end do
      !  ex=ex/(two*pi)
      !  ey=ey/(two*pi)
      !  temp=-sin(x3b(k))*ex+cos(x3b(k))*ey
      !  emf3(i,jep1,:)=temp
      !end do
    end if

  END SUBROUTINE boundary_emf3
  !
  !=======================================================================
  !
  SUBROUTINE boundary_currents(face, side, j1, j2, j3)
    INTEGER, INTENT(IN) :: face, side
    REALNUM, DIMENSION(:, :, :), INTENT(INOUT) :: j1, j2, j3

    INTEGER i, j, k

    if (face == 1 .and. side == 1) then
      !j1(isg,:,:)=j1(isgo,:,:)
      j2(isg, :, :) = j2(isgo, :, :)
      j3(isg, :, :) = j3(isgo, :, :)
    end if
    if (face == 1 .and. side == 2) then
      continue
    end if
    if (face == 2 .and. side == 1) then
      do k = ksg, keg
        do i = isg, ieg
          do j = jsg, jsm1
            j1(i, j, k) = j1(i, 2*js - j, k)  ! Reflective BC
          end do
        end do
      end do
      do k = ksg, keg
        do i = isg, ieg
          do j = jsg, jsm1
            j2(i, j, k) = -j2(i, js + jsm1 - j, k)  ! Reflective- BC
          end do
        end do
      end do
      ! REVISE FOR 3D
      do k = ksg, keg
        do i = isg, ieg
          j3(i, js, k) = zro ! Reflective+- BC
          do j = jsg, jsm1
            j3(i, j, k) = -j3(i, 2*js - j, k) ! Reflective- BC
          end do
        end do
      end do
    end if
    if (face == 2 .and. side == 2) then
      do k = ksg, keg
        do i = isg, ieg
          do j = jep2, jeg
            j1(i, j, k) = j1(i, 2*jep1 - j, k)  ! Reflective BC
          end do
        end do
      end do
      do k = ksg, keg
        do i = isg, ieg
          do j = jep1, jeg
            j2(i, j, k) = -j2(i, je + jep1 - j, k)  ! Reflective- BC
          end do
        end do
      end do
      do k = ksg, keg
        do i = isg, ieg
          if (bc_sign == -one) j3(i, jep1, k) = zro
          do j = jep2, jeg
            j3(i, j, k) = bc_sign*j3(i, 2*jep1 - j, k)  ! +/- Reflective BC
            !j3(i,j,k)=j3(i,2*jep1-j,k)  ! + Reflective BC
          end do
        end do
      end do
    end if
    if (face == 3 .and. side == 1) then
      do j = jsg, jeg
        do i = isg, ieg
          do k = ksg, ksm1
            j1(i, j, k) = j1(i, j, k + ke - ksm1) ! Periodic BC
          end do
        end do
      end do
      do j = jsg, jeg
        do i = isg, ieg
          do k = ksg, ksm1
            j2(i, j, k) = j2(i, j, k + ke - ksm1) ! Periodic BC
          end do
        end do
      end do
      do j = jsg, jeg
        do i = isg, ieg
          do k = ksg, ksm1
            j3(i, j, k) = j3(i, j, k + ke - ksm1) ! Periodic BC
          end do
        end do
      end do
    end if
    if (face == 3 .and. side == 2) then
      do j = jsg, jeg
        do i = isg, ieg
          do k = kep2, keg
            j1(i, j, k) = j1(i, j, k + ks - kep1) ! Periodic BC
          end do
        end do
      end do
      do j = jsg, jeg
        do i = isg, ieg
          do k = kep2, keg
            j2(i, j, k) = j2(i, j, k + ks - kep1) ! Periodic BC
          end do
        end do
      end do
      do j = jsg, jeg
        do i = isg, ieg
          do k = kep1, keg
            j3(i, j, k) = j3(i, j, k + ks - kep1) ! Periodic BC
          end do
        end do
      end do
    end if
  END SUBROUTINE boundary_currents
  !
  !=======================================================================
  !
  SUBROUTINE boundary_vd(face, side, vd1, vd2, vd3)

    INTEGER, INTENT(IN) :: face, side
    REALNUM, DIMENSION(:, :, :), INTENT(INOUT) :: vd1, vd2, vd3
    !
    call boundary_vd1(face, side, vd1)
    call boundary_vd2(face, side, vd2)
    call boundary_vd3(face, side, vd3)
    !
  END SUBROUTINE boundary_vd
  !
  !=======================================================================
  !
  SUBROUTINE boundary_vd1(face, side, vd1)
    CHARACTER(LEN=*), PARAMETER :: routinename = 'z_boundary_vd1'
    INTEGER, INTENT(IN) :: face, side
    REALNUM, DIMENSION(:, :, :), INTENT(INOUT) :: vd1
    INTEGER i, j, k
    REALNUM temp

    if (face == 1 .and. side == 1) then
      do k = ksg, keg
        do j = jsg, jeg

          ! Periodic BC
          !do i=isg,ism1
          !   vd1(i,j,k)=vd1(i+ie-ism1,j,k)
          !end do

           !! Ouflow (continuity) BC
          ! temp = min(vd1(is+ione,j,k),vg1(is))
          temp = vd1(is + ione, j, k)
          do i = isg, is
            vd1(i, j, k) = temp
          end do

           !! Reflective BC
          !temp = vg1(is)
          !vd1(is,j,k)=temp
          !do i=isg,ism1
          !   vd1(i,j,k)=two*temp-vd1(2*is-i,j,k)
          !end do

        end do
      end do
    end if
    if (face == 1 .and. side == 2) then
      do k = ksg, keg
        do j = jsg, jeg

          ! Periodic BC
          !do i=iep1,ieg
          !   vd1(i,j,k)=vd1(i+is-iep1,j,k)
          !end do

           !! Ouflow (continuity) BC
          ! temp = max(vd1(ie,j,k),vg1(iep1))
          temp = vd1(ie, j, k)
          do i = iep1, ieg
            vd1(i, j, k) = temp
          end do

           !! Reflective BC
          !temp = vg1(iep1)
          !vd1(iep1,j,k)=temp
          !do i=iep2,ieg
          !   vd1(i,j,k)=two*temp-vd1(2*iep1-i,j,k)
          !end do

        end do
      end do
    end if

    if (face == 2 .and. side == 1) then

      if (vd_axiszero_start > 0) vd1(:, js:js + vd_axiszero_start - 1, :) = zro

      do k = ksg, keg
        do j = jsg, jsm1
          do i = isg, ieg
            ! vd1(i,j,k)=vd1(i,j+je-jsm1,k)   ! Periodic BC
            ! vd1(i,j,k)=vd1(i,js,k)        ! Ouflow (continuity) BC
            vd1(i, j, k) = vd1(i, js + jsm1 - j, k) ! Reflective BC
          end do
        end do
      end do
    end if
    if (face == 2 .and. side == 2) then

      if (vd_axiszero_end > 0) vd1(:, je - vd_axiszero_end + 1:je, :) = zro

      do k = ksg, keg
        do j = jep1, jeg
          do i = isg, ieg
            ! vd1(i,j,k)=vd1(i,j+js-jep1,k)   ! Periodic BC
            ! vd1(i,j,k)=vd1(i,je,k)        ! Ouflow (continuity) BC
            vd1(i, j, k) = vd1(i, je + jep1 - j, k) ! Reflective BC
          end do
        end do
      end do
    end if

    if (face == 3 .and. side == 1) then
      do k = ksg, ksm1
        do j = jsg, jeg
          do i = isg, ieg
            vd1(i, j, k) = vd1(i, j, k + ke - ksm1)   ! Periodic BC
            ! vd1(i,j,k)=vd1(i,j,ks)        ! Ouflow (continuity) BC
            ! vd1(i,j,k)=vd1(i,j,ks+ksm1-k) ! Reflective BC
          end do
        end do
      end do
    end if
    if (face == 3 .and. side == 2) then
      do k = kep1, keg
        do j = jsg, jeg
          do i = isg, ieg
            vd1(i, j, k) = vd1(i, j, k + ks - kep1)   ! Periodic BC
            ! vd1(i,j,k)=vd1(i,j,ke)        ! Ouflow (continuity) BC
            ! vd1(i,j,k)=vd1(i,j,ke+kep1-k) ! Reflective BC
          end do
        end do
      end do
    end if
    !
  END SUBROUTINE boundary_vd1
  !
  !=======================================================================
  !
  SUBROUTINE boundary_vd2(face, side, vd2)
    INTEGER, INTENT(IN) :: face, side
    REALNUM, DIMENSION(:, :, :), INTENT(INOUT) :: vd2
    INTEGER i, j, k
    REALNUM temp

    if (.not. withv2) return

    if (face == 1 .and. side == 1) then
      do k = ksg, keg
        do j = jsg, jeg
          do i = isg, ism1
            !vd2(i,j,k)=vd2(i+ie-ism1,j,k)   ! Periodic BC
            vd2(i, j, k) = vd2(is, j, k)        ! Ouflow (continuity) BC
            ! vd2(i,j,k)=vd2(is+ism1-i,j,k) ! Reflective BC
            ! vd2(i,j,k)=-vd2(is+ism1-i,j,k) ! Reflective- BC
          end do
        end do
      end do
    end if
    if (face == 1 .and. side == 2) then
      do k = ksg, keg
        do j = jsg, jeg
          do i = iep1, ieg
            !vd2(i,j,k)=vd2(i+is-iep1,j,k)   ! Periodic BC
            vd2(i, j, k) = vd2(ie, j, k)        ! Ouflow (continuity) BC
            ! vd2(i,j,k)=vd2(ie+iep1-i,j,k) ! Reflective BC
          end do
        end do
      end do
    end if

    if (face == 2 .and. side == 1) then
      do k = ksg, keg
        do i = isg, ieg

          ! Periodic BC
          !do j=jsg,jsm1
          !   vd2(i,j,k)=vd2(i,j+je-jsm1,k)
          !end do

           !! Ouflow (continuity) BC
          !temp = min(vd2(i,js+1,k),vg2(js))
          !do j=jsg,js
          !   vd2(i,j,k)=temp
          !end do

           !! Reflective BC
          vd2(i, js, k) = zro

          if (vd_axiszero_start > 0) &
            vd2(:, js + 1:js + vd_axiszero_start, :) = zro

          do j = jsg, jsm1
            vd2(i, j, k) = -vd2(i, 2*js - j, k)
          end do

        end do
      end do
    end if
    if (face == 2 .and. side == 2) then
      do k = ksg, keg
        do i = isg, ieg

          ! Periodic BC
          !do j=jep1,jeg
          !   vd2(i,j,k)=vd2(i,j+js-jep1,k)
          !end do

           !! Ouflow (continuity) BC
          !temp = max(vd2(i,je,k),vg2(jep1))
          !do j=jep1,jeg
          !   vd2(i,j,k)=temp
          !end do

           !! Reflective BC
          vd2(i, jep1, k) = zro

          if (vd_axiszero_end > 0) vd2(:, je - vd_axiszero_end + 1:je, :) = zro

          do j = jep2, jeg
            vd2(i, j, k) = -vd2(i, 2*jep1 - j, k)
          end do

        end do
      end do
    end if

    if (face == 3 .and. side == 1) then
      do k = ksg, ksm1
        do j = jsg, jeg
          do i = isg, ieg
            vd2(i, j, k) = vd2(i, j, k + ke - ksm1)   ! Periodic BC
            ! vd2(i,j,k)=vd2(i,j,ks)        ! Ouflow (continuity) BC
            ! vd2(i,j,k)=vd2(i,j,ks+ksm1-k) ! Reflective BC
          end do
        end do
      end do
    end if
    if (face == 3 .and. side == 2) then
      do k = kep1, keg
        do j = jsg, jeg
          do i = isg, ieg
            vd2(i, j, k) = vd2(i, j, k + ks - kep1)   ! Periodic BC
            ! vd2(i,j,k)=vd2(i,j,ke)        ! Ouflow (continuity) BC
            ! vd2(i,j,k)=vd2(i,j,ke+kep1-k) ! Reflective BC
          end do
        end do
      end do
    end if
    !
  END SUBROUTINE boundary_vd2
  !
  !=======================================================================
  !
  SUBROUTINE boundary_vd3(face, side, vd3)
    INTEGER, INTENT(IN) :: face, side
    REALNUM, DIMENSION(:, :, :), INTENT(INOUT) :: vd3
    INTEGER i, j, k
    REALNUM temp

    if (.not. withv3) return

    if (face == 1 .and. side == 1) then
      do k = ksg, keg
        do j = jsg, jeg
          do i = isg, ism1
            ! vd3(i,j,k)=vd3(i+ie-ism1,j,k)   ! Periodic BC
            vd3(i, j, k) = vd3(is, j, k)        ! Ouflow (continuity) BC
            ! vd3(i,j,k)=vd3(is+ism1-i,j,k) ! Reflective BC
            ! vd3(i,j,k)=-vd3(is+ism1-i,j,k)! Reflective- BC
          end do
        end do
      end do
    end if
    if (face == 1 .and. side == 2) then
      do k = ksg, keg
        do j = jsg, jeg
          do i = iep1, ieg
            !vd3(i,j,k)=vd3(i+is-iep1,j,k)   ! Periodic BC
            vd3(i, j, k) = vd3(ie, j, k)        ! Ouflow (continuity) BC
            ! vd3(i,j,k)=vd3(ie+iep1-i,j,k) ! Reflective BC
          end do
        end do
      end do
    end if

    if (face == 2 .and. side == 1) then

      if (vd_axiszero_start > 0) vd3(:, js:js + vd_axiszero_start - 1, :) = zro

      do k = ksg, keg
        do j = jsg, jsm1
          do i = isg, ieg
            ! vd3(i,j,k)=vd3(i,j+je-jsm1,k)   ! Periodic BC
            ! vd3(i,j,k)=vd3(i,js,k)        ! Ouflow (continuity) BC
            ! vd3(i,j,k)=vd3(i,js+jsm1-j,k) ! Reflective BC
            vd3(i, j, k) = -vd3(i, js + jsm1 - j, k)! Reflective- BC
          end do
        end do
      end do
    end if
    if (face == 2 .and. side == 2) then

      if (vd_axiszero_end > 0) vd3(:, je - vd_axiszero_end + 1:je, :) = zro

      do k = ksg, keg
        do j = jep1, jeg
          do i = isg, ieg
            ! vd3(i,j,k)=vd3(i,j+js-jep1,k)   ! Periodic BC
            ! vd3(i,j,k)=vd3(i,je,k)        ! Ouflow (continuity) BC
            ! vd3(i,j,k)=vd3(i,je+jep1-j,k) ! Reflective BC
            ! vd3(i,j,k)=-vd3(i,je+jep1-j,k) ! Reflective- BC
            vd3(i, j, k) = bc_sign*vd3(i, je + jep1 - j, k) ! +/- Reflective BC
          end do
        end do
      end do
    end if

    if (face == 3 .and. side == 1) then
      do j = jsg, jeg
        do i = isg, ieg

          ! Periodic BC
          do k = ksg, ksm1
            vd3(i, j, k) = vd3(i, j, k + ke - ksm1)
          end do

           !! Ouflow (continuity) BC
          !temp = min(vd3(i,j,ksp1),vg3(ks))
          !do k=ksg,ks
          !   vd3(i,j,k)=temp
          !end do

           !! Reflective BC
          !temp = vg3(ks)
          !vd3(i,j,ks)=temp
          !do k=ksg,ksm1
          !   vd3(i,j,k)=two*temp-vd3(i,j,2*ks-k)
          !end do

        end do
      end do
    end if
    if (face == 3 .and. side == 2) then
      do j = jsg, jeg
        do i = isg, ieg

          ! Periodic BC
          do k = kep1, keg
            vd3(i, j, k) = vd3(i, j, k + ks - kep1)
          end do

           !! Ouflow (continuity) BC
          !temp = max(vd3(i,j,ke),vg3(kep1))
          !do k=kep1,keg
          !   vd3(i,j,k)=temp
          !end do

           !! Reflective BC
          !temp = vg3(kep1)
          !vd3(i,j,kep1)=temp
          !do k=kep2,keg
          !   vd3(i,j,k)=two*temp-vd3(i,j,2*kep1-k)
          !end do

        end do
      end do
    end if
    !
  END SUBROUTINE boundary_vd3
  !
  !=======================================================================
  !
  SUBROUTINE boundary_vi(face, side, vi1, vi2, vi3)
    CHARACTER(LEN=*), PARAMETER :: routinename = 'z_boundary_vi'
    INTEGER, INTENT(IN) :: face, side
    REALNUM, DIMENSION(:, :, :), INTENT(INOUT) :: vi1, vi2, vi3
    !
    call finalstop(routinename, 'Not used in this version')
  END SUBROUTINE boundary_vi
  !
  !=======================================================================
  !
  REALNUM &
    FUNCTION ttspline(theta, wind_value, ambient_value)
    REALNUM, INTENT(IN) :: theta, wind_value, ambient_value

    if (wind_v0 <= zro .or. theta_max_wind%transition <= zro) then
      ttspline = ambient_value
      return
    end if
    if (theta_max_wind%transition >= 90.0_pk) then
      ttspline = wind_value
      return
    end if

    ttspline = spline(abs(min(theta, pi - theta)), &
                      theta_max_wind, wind_value, ambient_value)

  END FUNCTION ttspline
  !
  !=======================================================================
  !
  REALNUM &
    FUNCTION spline(x, s, va, vb)
    REALNUM, INTENT(IN) :: x, va, vb
    TYPE(splinecontrol), INTENT(IN) :: s

    REALNUM f, xscaled

    if (x <= s%a) then
      spline = va
      return
    end if
    if (x >= s%b) then
      spline = vb
      return
    end if

    if (s%width == zro) then
      ! In the unexpected case this line ever runs,
      ! the value would be the same as for x<s%a
      spline = va
      return
    end if

    xscaled = (x - s%a)/s%width

    f = x**2*(three - two*x) ! Simple cubic spline
    ! f=haf*(one-cos(pi*x)) ! Simple cosine spline
    ! Maybe also forms with steepness control, guaranteed symmetries, etc.

    spline = (one - f)*va + f*vb
  END FUNCTION spline
!=======================================================================
  SUBROUTINE prompt_accretion
#ifdef MPI
    USE mpi
    INTEGER ierr
    REALNUM buffer
#endif /* MPI */
    REALNUM :: local_mass_loss, total_mass_loss

    INTEGER :: i, j, k
    REALNUM :: theta, temp, rho_wind

    if (.true.) return

    if (accretion_wedge_depth <= 0) return

    local_mass_loss = zro
    total_mass_loss = zro

    if (l_firstnode1) then
      do k = ks, ke
        do j = js, je
          theta = x2b(j)
          theta = abs(min(theta, pi - theta))

          if (theta >= theta_max_wind%b) cycle

          if (theta >= windcone_rho%theta) then
            temp = (wind_r0/x1ap(isp))**2* &
                   wind_rho0/sin(theta)**wind_n
          else
            temp = windcone_rho%a*sin(theta)**windcone_rho%n + windcone_rho%b
          end if

          rho_wind = temp*(x1ap(isp)*x1bi(ism1))**wind_radial_n_ghosts

          do i = is, is + accretion_wedge_depth - 1
            if (d_(i, j, k) > rho_wind) then
              local_mass_loss = local_mass_loss + &
                                (d_(i, j, k) - rho_wind)* &
                                dvl1a(i)*dvl2a(j)*dvl3a(k)
              d_(i, j, k) = rho_wind
            end if
          end do
        end do
      end do
    end if

    if (withcentralmass) then
#ifdef MPI
      call MPI_REDUCE(local_mass_loss, total_mass_loss, 1, &
                      MPI_FLOATMPI, MPI_SUM, rootnode, MPI_COMM_WORLD, ierr)
#else /* MPI */
      total_mass_loss = local_mass_loss
#endif /* MPI */
      if (node == rootnode) then
        if (bc_sign == -one) then
          centralmass = centralmass + total_mass_loss
        else
          centralmass = centralmass + two*total_mass_loss
        end if
      end if
#ifdef MPI
      if (node == rootnode) then
        buffer = centralmass
      end if
      call MPI_BCAST(buffer, 1, MPI_FLOATMPI, rootnode, &
                     MPI_COMM_WORLD, ierr)
      centralmass = buffer
#endif /* MPI */
    end if
  END SUBROUTINE prompt_accretion
END MODULE problem_boundary
