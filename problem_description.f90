#include "macros"
MODULE problem_description
  USE problem_variables
  USE zeus_variables
  USE zeus_io

  IMPLICIT NONE
  PRIVATE
  SAVE
  PUBLIC :: problem_bc, problem_bc_start
  REALNUM, ALLOCATABLE, DIMENSION(:) :: table_time, table_v0
CONTAINS
  SUBROUTINE problem_bc
    CHARACTER(LEN=*), PARAMETER :: routinename = 'z_problem_bc'
    !character(len=messagelinelength) :: message

    !write(message,*) 'Now entering subroutine ',routinename
    !call threeoutput(routinename,message)

    !write(message,*) 'Selecting case wave_type -> ',wave_type
    !call threeoutput(routinename,message)

    select case (wave_type)
    case ('constant')
      wind_v0 = wind_v0_nominal
    case ('cosine')
      wind_v0 = wind_v0_nominal &
                *(one + wave_height*cos((two*pi)*time/wave_period &
                                        + wave_phase*degree))
    case ('square')
      if (modulo(time/wave_period + wave_phase/360.0_pk, one) < haf) then
        wind_v0 = wind_v0_nominal*(one - wave_height)
      else
        wind_v0 = wind_v0_nominal*(one + wave_height)
      end if
      !case('table')
      !  wind_v0=read_table(time)
    case default
      call finalstop(routinename, &
                     'Value of wave_type not assigned,' &
                     //' not implemented, or not in use.')
    end select

    !write(message,*) 'wind_v0 is now set to: ',wind_v0
    !call threeoutput(routinename,message)

    ! These values might be overwritten during the initial problem setup
    select case (wind_direction_type)
    case ('radial')
      alpha_edge = theta_max_wind%transition
    case ('alpha_edge')
      alpha_edge = min(alpha_edge + dt*alpha_dot, alpha_edge_max)
    case default
      call finalstop(routinename, &
                     'Value of wind_direction_type not assigned,' &
                     //' not implemented, or not in use.')
    end select
    delta_edge = alpha_edge - theta_max_wind%transition
  END SUBROUTINE problem_bc

  SUBROUTINE problem_bc_start
    CHARACTER(LEN=*), PARAMETER :: routinename = 'z_problem_bc_start'
    !character(len=messagelinelength) :: message

    !write(message,*) 'Entering subroutine ',routinename
    !call threeoutput(routinename,message)

    select case (wave_type)
    case ('constant', 'cosine', 'square')  ! Nothing to do
      continue
      !case('table')
      !  call fill_up_tables
    case default
      call finalstop(routinename, &
                     'Value of wave_type not assigned,' &
                     //' not implemented, or not in use.')
    end select

    !write(message,*) 'Calling problem_bc from ',routinename
    !call threeoutput(routinename,message)

    call problem_bc

    ! Reset wind direction parameters during the first call (dt not to be applied)
    select case (wind_direction_type)
    case ('radial')
      alpha_edge = theta_max_wind%transition
    case ('alpha_edge')
      alpha_edge = alpha_edge_initial
      if (alpha_dot < zro) then
        call warnoutput(routinename, 'alpha_dot probably should be >=0')
      end if
    case default
      call finalstop(routinename, &
                     'Value of wind_direction_type not assigned,' &
                     //' not implemented, or not in use.')
    end select
    delta_edge = alpha_edge - theta_max_wind%transition

  END SUBROUTINE problem_bc_start
END MODULE problem_description
