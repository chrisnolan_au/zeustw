#include "macros"
MODULE problem_final
CONTAINS

  ! Define tasks to be performed after the end of the main loop

  SUBROUTINE problem_finish
  END SUBROUTINE problem_finish
END MODULE problem_final
