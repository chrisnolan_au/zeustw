#include "macros"
MODULE problem_initial
  USE zeus_variables
  USE problem_variables
  USE problem_description
  USE problem_boundary
  USE zeus_physics, ONLY: col_dens, xrays, cosmicrays, pressure, chemistry
  USE zeus_io
  USE zeus_boundary
  USE zeus_memory

  IMPLICIT NONE

  PRIVATE
  PUBLIC :: problem_start, problem_restart

  INTERFACE readfield
    MODULE PROCEDURE readfield_2d
    MODULE PROCEDURE readfield_0d
  END INTERFACE

  namelist_variable(wavegen, REALNUM, waves_lower_wavelength, zro)
  namelist_variable(wavegen, REALNUM, waves_upper_wavelength, zro)
  namelist_variable(wavegen, INTEGER, waves_total_number, 0)
  namelist_variable(wavegen, INTEGER, waves_random_seed, 1)
  namelist_variable(wavegen, REALNUM, waves_amplitude_index, two)
  namelist_variable(wavegen, REALNUM, waves_amplitude_scale, ten)

CONTAINS
!** GROUP: PROBLEM INITIALIZATION *****************************************
!*                                                                     *
!*        G R O U P : U S E R   I N I T I A L I Z A T I O N            *
!*                                                                     *
!***********************************************************************
!
! Here the problem defines the implementation of routines called by the
! wrappers defined in the module zeus_problem_wrappers_initial.
!
!=======================================================================
  SUBROUTINE problem_restart
    call problem_common
    if (with_restart_perturbations) then
      call add_waves
      call problem_perturb
    end if
    call deplete_z
  END SUBROUTINE problem_restart
!=======================================================================
  SUBROUTINE problem_start
    call problem_common
    call problem_setup
    call problem_perturb
  END SUBROUTINE problem_start
!=======================================================================
  SUBROUTINE problem_setup
    !CHARACTER(LEN=*), PARAMETER :: routinename = 'z_problem_setup'
    LOGICAL :: success

    !call threeoutput(routinename,'begin')

    call start_from_two_d(success)
    if (success) return

    !call threeoutput(routinename,'try to start from scratch')

    ! Set fields in the volume

    ! density, gravity, central mass
    call rho_gp_cm_e
    v1 = zro
    v2 = zro
    v3 = zro
    call add_waves
    call velocities
    call wind
    call magnetic_fields
    call ionisations
    call etas

    ! Adjust remaining BCs
    call boundary('tf')

  END SUBROUTINE problem_setup
!=======================================================================
  SUBROUTINE field_poloidal_tilted(f1, f2, f3, f)
    USE zeus_mpi, only: mpifield
    CHARACTER(LEN=*), PARAMETER :: routinename = 'z_field_poloidal_tilted'
    REALNUM, INTENT(INOUT), DIMENSION(isa:iea, jsa:jea, ksa:kea) :: f1, f2, f3
    type(fieldcontrol), intent(in) :: f
    INTEGER i, j, k, ip1, jp1, kp1
    REALNUM r, theta, phi
    REALNUM costheta, sintheta
    REALNUM costheta_f, sintheta_f
    REALNUM q0, q1, q2
    REALNUM temp
    REALNUM deltaphi, cosdeltaphi, sindeltaphi
    REALNUM rmax

    REALNUM, PARAMETER :: degree = pi/180.0_pk

    !CHARACTER(LEN=messagelinelength) :: message

    costheta_f = cos(f%theta*degree)
    sintheta_f = sin(f%theta*degree)
    rmax = x1ap(iep1p)

    if (f%algorithm == 'direct') call field_poloidal_tilted_direct
    if (f%algorithm == 'potential') call field_poloidal_tilted_potential

  CONTAINS
    SUBROUTINE field_poloidal_tilted_direct
      do k = ksg, keg
        phi = x3b(k)
        deltaphi = phi - f%phi*degree
        cosdeltaphi = cos(deltaphi)
        do j = jsg, jeg
          theta = x2b(j)
          costheta = cos(theta)
          sintheta = sin(theta)
          q0 = costheta*costheta_f + sintheta*sintheta_f*cosdeltaphi
          temp = f%strength*q0

          select case (f%kind)
          case ('uniform')
            do i = isg, ieg
              f1(i, j, k) = f1(i, j, k) + temp
            end do
          case ('lambda')
            do i = isg, ieg
              f1(i, j, k) = f1(i, j, k) + temp* &
                            sqrt(one - min((one - q0**2)*(x1a(i)/rmax)**2, one))
            end do
          case default
            call finalstop(routinename, 'Unrecognized poloidal field kind')
          end select
        end do
      end do
      do k = ksg, keg
        phi = x3b(k)
        deltaphi = phi - f%phi*degree
        cosdeltaphi = cos(deltaphi)
        do j = jsg, jeg
          theta = x2a(j)
          costheta = cos(theta)
          sintheta = sin(theta)
          q1 = costheta_f*sintheta - sintheta_f*costheta*cosdeltaphi
          temp = -f%strength*q1

          select case (f%kind)
          case ('uniform')
            do i = isg, ieg
              f2(i, j, k) = f2(i, j, k) + temp
            end do
          case ('lambda')
            q0 = costheta*costheta_f + sintheta*sintheta_f*cosdeltaphi
            do i = isg, ieg
              f2(i, j, k) = f2(i, j, k) + temp* &
                            sqrt(one - min((one - q0**2)*(x1b(i)/rmax)**2, one))
            end do
          case default
            call finalstop(routinename, 'Unrecognized poloidal field kind')
          end select
        end do
      end do
      do k = ksg, keg
        phi = x3a(k)
        deltaphi = phi - f%phi*degree
        sindeltaphi = sin(deltaphi)
        q2 = sintheta_f*sindeltaphi
        temp = -f%strength*q2

        select case (f%kind)
        case ('uniform')
          do j = jsg, jeg
            do i = isg, ieg
              f3(i, j, k) = f3(i, j, k) + temp
            end do
          end do
        case ('lambda')
          cosdeltaphi = cos(deltaphi)
          do j = jsg, jeg
            theta = x2a(j)
            costheta = cos(theta)
            sintheta = sin(theta)
            q0 = costheta*costheta_f + sintheta*sintheta_f*cosdeltaphi
            do i = isg, ieg
              f3(i, j, k) = f3(i, j, k) + temp* &
                            sqrt(one - min((one - q0**2)*(x1b(i)/rmax)**2, one))
            end do
          end do
        case default
          call finalstop(routinename, 'Unrecognized poloidal field kind')
        end select
      end do
      call writedata(f1, 'z_f1_direct')
      call writedata(f2, 'z_f2_direct')
      call writedata(f3, 'z_f3_direct')

      call mpifield(f1)
      call mpifield(f2)
      call mpifield(f3)

    END SUBROUTINE field_poloidal_tilted_direct

    SUBROUTINE field_poloidal_tilted_potential
      REALNUM, ALLOCATABLE, DIMENSION(:, :, :) :: aphi
      REALNUM, ALLOCATABLE, DIMENSION(:, :, :) :: atheta

      call writedata(x1al, 'z_x1al')
      call writedata(x2al, 'z_x2al')
      call writedata(x3al, 'z_x3al')

      allocate (atheta(isa:iea + 1, jsa:jea, ksa:kea + 1))
      allocate (aphi(isa:iea + 1, jsa:jea + 1, ksa:kea))

      ! Construct vector potential components
      select case (f%kind)
      case ('uniform')
        do k = ksa, kea + 1
          phi = x3al(k)
          deltaphi = phi - f%phi*degree
          sindeltaphi = sin(deltaphi)

          q2 = sintheta_f*sindeltaphi
          temp = -haf*f%strength*q2

          do i = isa, iea + 1
            r = x1al(i)
            atheta(i, :, k) = temp*r
          end do
        end do

        do k = ksa, kea
          phi = x3b(k)
          deltaphi = phi - f%phi*degree
          cosdeltaphi = cos(deltaphi)

          do j = jsa, jea + 1
            theta = x2al(j)
            costheta = cos(theta)
            sintheta = sin(theta)
            q1 = costheta_f*sintheta - sintheta_f*costheta*cosdeltaphi
            temp = haf*f%strength*q1
            do i = isa, iea + 1
              r = x1al(i)
              aphi(i, j, k) = temp*r
            end do
          end do
        end do
      case ('lambda')
        do k = ksa, kea + 1
          phi = x3al(k)
          deltaphi = phi - f%phi*degree
          sindeltaphi = sin(deltaphi)

          q2 = sintheta_f*sindeltaphi
          temp = -f%strength*rmax**2*q2/three
          do j = jsa, jea
            theta = x2b(j)
            costheta = cos(theta)
            sintheta = sin(theta)
            q0 = costheta*costheta_f + sintheta*sintheta_f*cosdeltaphi

            do i = isa, iea + 1
              r = x1al(i)
              atheta(i, j, k) = temp*(one - (one &
                                             - min((one - q0**2)*(r/rmax)**2, &
                                                   one))**(haf*three)) &
                                /(r*(one - q0**2))
            end do
          end do
        end do

        do k = ksa, kea
          phi = x3b(k)
          deltaphi = phi - f%phi*degree
          cosdeltaphi = cos(deltaphi)

          do j = jsa, jea + 1
            theta = x2al(j)
            costheta = cos(theta)
            sintheta = sin(theta)
            q1 = costheta_f*sintheta - sintheta_f*costheta*cosdeltaphi
            q0 = costheta*costheta_f + sintheta*sintheta_f*cosdeltaphi

            temp = f%strength*rmax**2*q1/three

            do i = isa, iea + 1
              r = x1al(i)
              aphi(i, j, k) = temp*(one - (one &
                                           - min((one - q0**2)*(r/rmax)**2, &
                                                 one))**(haf*three)) &
                              /(r*(one - q0**2))
            end do
          end do
        end do

      case default
        call finalstop(routinename, 'Unrecognized poloidal field kind')
      end select

      call writedata(aphi, 'z_aphi')
      call writedata(atheta, 'z_atheta')

      do k = ksg, keg
        kp1 = k + kone
        do j = jsg, jeg
          jp1 = j + jone
          if (jp1 > jeg) then
            temp = sin(x2al(jp1))
          else
            temp = g32a(jp1)
          end if
          do i = isg, ieg
            f1(i, j, k) = f1(i, j, k) + ( &
                          (temp*aphi(i, jp1, k) - g32a(j)*aphi(i, j, k)) &
                          *dx2ai(j) &
                          - (atheta(i, j, kp1) - atheta(i, j, k)) &
                          *dx3ai(k)) &
                          *x1ai(i)*g32bi(j)
          end do
        end do
      end do

      do k = ksg, keg
        do j = jsg, jeg
          do i = isg, ieg
            ip1 = i + ione
            f2(i, j, k) = f2(i, j, k) + &
                          (-x1al(ip1)*aphi(ip1, j, k) + x1al(i)*aphi(i, j, k)) &
                          *x1bi(i)*dx1ai(i)
          end do
        end do
      end do

      do k = ksg, keg
        do j = jsg, jeg
          do i = isg, ieg
            ip1 = i + ione
            f3(i, j, k) = f3(i, j, k) + (x1al(ip1)*atheta(ip1, j, k) &
                                         - x1al(i)*atheta(i, j, k)) &
                          *x1bi(i)*dx1ai(i)
          end do
        end do
      end do

      call writedata(f1, 'z_f1_potential')
      call writedata(f2, 'z_f2_potential')
      call writedata(f3, 'z_f3_potential')

      call mpifield(f1)
      call mpifield(f2)
      call mpifield(f3)

      deallocate (aphi)
      deallocate (atheta)

    END SUBROUTINE field_poloidal_tilted_potential

  END SUBROUTINE field_poloidal_tilted
!=======================================================================
  SUBROUTINE field_poloidal_untilted(f1, f2, f3, f)
    USE zeus_mpi, only: mpifield
    CHARACTER(LEN=*), PARAMETER :: routinename = 'z_field_poloidal_untilted'
    REALNUM, INTENT(INOUT), DIMENSION(isa:iea, jsa:jea, ksa:kea) :: f1, f2, f3
    type(fieldcontrol), intent(in) :: f
    INTEGER i, j, k
    REALNUM :: r, theta, sintheta, cottheta, varpi
    REALNUM :: brvalue, bthetavalue
    REALNUM, ALLOCATABLE :: psi(:, :)
    REALNUM :: rmin, rmax

    allocate (psi(isa:iea + 1, jsa:jea + 1))

    select case (f%kind)
    case ('psi')
      rmin = x1ap(is)
      do j = jsa, jea + 1
        theta = x2al(j)
        sintheta = abs(sin(theta))
        cottheta = one/tan(theta)
        do i = isa, iea + 1
          r = x1al(i)
          varpi = r*sintheta
          psi(i, j) = (three*quarter)*rmin**2*f%strength &
                      *(varpi/rmin)**(three*quarter)*m**(five*quarter) &
                      *(m**2 + cottheta**2)**(-five*eighth)
        end do
      end do
    case ('uniform')
      do j = jsa, jea + 1
        theta = x2al(j)
        sintheta = abs(sin(theta))
        do i = isa, iea + 1
          r = x1al(i)
          varpi = r*sintheta
          psi(i, j) = varpi**2*f%strength*haf
        end do
      end do
    case ('lambda')
      rmax = x1ap(iep1p)
      do j = jsa, jea + 1
        theta = x2al(j)
        sintheta = abs(sin(theta))
        do i = isa, iea + 1
          r = x1al(i)
          varpi = r*sintheta
          psi(i, j) = (one - (one - (varpi/rmax)**2)**(haf*three)) &
                      *f%strength*rmax**2/three
        end do
      end do
    case default
      call finalstop(routinename, 'Unrecognized poloidal field kind')
    end select

    do k = ksg, keg
      do j = jsg, jeg
        do i = isg, ieg
          brvalue = ((psi(i, j + 1) - psi(i, j)) &
                     *dx2ai(j))*x1ai(i)**2/(sin(x2b(j)) + verysmall)
          f1(i, j, k) = f1(i, j, k) + brvalue
        end do
      end do
    end do
    call mpifield(f1)

    do k = ksg, keg
      do j = jsg, jeg
        do i = isg, ieg
          bthetavalue = -((psi(i + 1, j) - psi(i, j)) &
                          *dx1ai(i))/x1b(i)/(sin(x2a(j)) + verysmall)
          f2(i, j, k) = f2(i, j, k) + bthetavalue
        end do
      end do
    end do
    call mpifield(f2)

    deallocate (psi)
  END SUBROUTINE field_poloidal_untilted
!=======================================================================
  SUBROUTINE magnetic_fields
    CHARACTER(LEN=*), PARAMETER :: routinename = 'z_magnetic_fields'
    if (imhd == 0) return
    if (.not. (withbp .and. withb3)) then
      call finalstop(routinename, 'All magnetic components required')
    end if

    b1 = zro
    b2 = zro
    b3 = zro

    call field_toroidal(b1, b2, b3, bt)

    call field_poloidal(b1, b2, b3, bp)

    if (withb3bc) call boundary('b3')

    call writedata(b1, 'z_b1_b')
    call writedata(b2, 'z_b2_b')
    call writedata(b3, 'z_b3_b')

  END SUBROUTINE magnetic_fields
!=======================================================================
  SUBROUTINE velocities
    CHARACTER(LEN=*), PARAMETER :: routinename = 'z_velocities'

    if (.not. (withv2 .and. withv3)) then
      call finalstop(routinename, 'All velocity components required')
    end if

    call field_toroidal(v1, v2, v3, vt)

    call field_poloidal(v1, v2, v3, vp)

    call boundary('v1')
    call boundary('v2')
    call boundary('v3')

    call writedata(v1, 'z_v1_b')
    call writedata(v2, 'z_v2_b')
    call writedata(v3, 'z_v3_b')

  END SUBROUTINE velocities
!=======================================================================
  SUBROUTINE ionisations
    CHARACTER(LEN=*), PARAMETER :: routinename = 'z_ionisations'
    REALNUM, POINTER, SAVE :: rin_cd(:, :, :) => null(), rout_cd(:, :, :) => null(), &
                              top_cd(:, :, :) => null(), bottom_cd(:, :, :) => null()
    if (imhd == 0) return
    if (.not. withchem) return

    xr_array = zro
    cr_array = zro

    call get_memory(rin_cd)
    call get_memory(rout_cd)
    call get_memory(top_cd)
    call get_memory(bottom_cd)
    call col_dens(rin_cd, rout_cd, top_cd, bottom_cd)
    call xrays(rin_cd, top_cd, bottom_cd, xr_array)
    call cosmicrays(top_cd, bottom_cd, cr_array)

    call release_memory(rin_cd)
    call release_memory(rout_cd)
    call release_memory(top_cd)
    call release_memory(bottom_cd)

  END SUBROUTINE ionisations
!=======================================================================
  SUBROUTINE etas
    CHARACTER(LEN=*), PARAMETER :: routinename = 'z_etas'
    REALNUM, POINTER, SAVE :: p(:, :, :) => null()
    if (imhd == 0) return
    if (.not. withchem) return

    ! Calculate pressure
    call get_memory(p)
    call pressure(p, 1)

    ad_eta_array = zro
    ohmic_eta_array = zro
    hall_eta_array = zro

    call chemistry(p, ad_eta_array, ohmic_eta_array, hall_eta_array)

    call release_memory(p)
  END SUBROUTINE etas
!=======================================================================
  SUBROUTINE field_poloidal(f1, f2, f3, f)
    !CHARACTER(LEN=*), PARAMETER :: routinename = 'z_field_poloidal'
    REALNUM, INTENT(INOUT), DIMENSION(isa:iea, jsa:jea, ksa:kea) :: f1, f2, f3
    type(fieldcontrol), intent(in) :: f
    !CHARACTER(LEN=messagelinelength) :: message

    if (f%theta == zro) then
      !write(message,*) 'symmetric!'
      !call writemessage(routinename,message)
      call field_poloidal_untilted(f1, f2, f3, f)
    else
      call field_poloidal_tilted(f1, f2, f3, f)
    end if
  END SUBROUTINE field_poloidal
!=======================================================================
  SUBROUTINE field_toroidal(f1, f2, f3, f)
    !CHARACTER(LEN=*), PARAMETER :: routinename = 'z_field_toroidal'
    REALNUM, INTENT(INOUT), DIMENSION(isa:iea, jsa:jea, ksa:kea) :: f1, f2, f3
    type(fieldcontrol), intent(in) :: f
    !CHARACTER(LEN=messagelinelength) :: message

    if (f%theta == zro) then
      !write(message,*) 'symmetric!'
      !call writemessage(routinename,message)
      call field_toroidal_untilted(f1, f2, f3, f)
    else
      call field_toroidal_tilted(f1, f2, f3, f)
    end if
  END SUBROUTINE field_toroidal
!=======================================================================
  SUBROUTINE field_toroidal_untilted(f1, f2, f3, f)
    CHARACTER(LEN=*), PARAMETER :: routinename = 'z_field_toroidal_untilted'
    REALNUM, INTENT(INOUT), DIMENSION(isa:iea, jsa:jea, ksa:kea) :: f1, f2, f3
    type(fieldcontrol), intent(in) :: f
    INTEGER i, j, k
    REALNUM rmin, disk_angle, f_theta

    ! if (f%strength==zro) return
    disk_angle = atan(disk_angle_factor*epsilon)

    select case (f%kind)
    case ('constant')
      do k = ksg, keg
        do j = jsg, jeg
          do i = isg, ieg
            f3(i, j, k) = f3(i, j, k) + f%strength
          end do
        end do
      end do
    case ('linear')
      do k = ksg, keg
        do j = jsg, jeg
          do i = isg, ieg
            f3(i, j, k) = f3(i, j, k) + x1b(i)*g32b(j)*f%strength
          end do
        end do
      end do
    case ('tanh')
      do k = ksg, keg
        do j = jsg, jeg
          do i = isg, ieg
            f3(i, j, k) = f3(i, j, k) + &
                          tanh(x1b(i)*g32b(j)/f%parameter)*f%strength
          end do
        end do
      end do
    case ('power-law')
      rmin = x1ap(is)
      do k = ksg, keg
        do j = jsg, jeg
          do i = isg, ieg
            f3(i, j, k) = -f%strength*(rmin/x1b(i))**(five*quarter)*cos(x2b(j))
          end do
        end do
      end do
    case ('keplerian')
      do k = ksg, keg
        do j = jsg, jeg
          do i = isg, ieg
            f3(i, j, k) = sqrt(gptmass/(x1b(i)*sin(x2b(j))))
          end do
        end do
      end do
    case ('sub-keplerian')
      do k = ksg, keg
        do j = jsg, jeg
          do i = isg, ieg
            f3(i, j, k) = sqrt(one - (one + rho_exponent)*epsilon**2) &
                          *sqrt(gptmass/(x1b(i)*sin(x2b(j))))
          end do
        end do
      end do
    case ('keplerian-disk')
      do k = ksg, keg
        do j = jsg, jeg
          do i = isg, ieg
            if ((x2b(j) >= pi/2.-disk_angle) &
                .and. (x2b(j) <= pi/2.+disk_angle)) then
              f3(i, j, k) = sqrt(gptmass/(x1b(i)*sin(x2b(j))))
            else
              f3(i, j, k) = zro
            end if
          end do
        end do
      end do
    case ('sub-kep-disk')
      do k = ksg, keg
        do j = jsg, jeg
          f_theta = exp(-cos(x2b(j))**2/(2.*epsilon**2))
          do i = isg, ieg
            if ((x2b(j) >= pi/2.-disk_angle) &
                .and. (x2b(j) <= pi/2.+disk_angle)) then
              f3(i, j, k) = sqrt(one &
                                 - (one + rho_exponent)*epsilon**2/f_theta) &
                            *sqrt(gptmass/(x1b(i)*sin(x2b(j))))
            else
              f3(i, j, k) = zro
            end if
          end do
        end do
      end do
    case ('keplerian-sinsq')
      do k = ksg, keg
        do j = jsg, jeg
          do i = isg, ieg
            if ((x2b(j) >= pi/2.-disk_angle) &
                .and. (x2b(j) <= pi/2.+disk_angle)) then
              f3(i, j, k) = sqrt(gptmass/(x1b(i)*sin(x2b(j))))
            end if
            if (x2b(j) < pi/2.-disk_angle) then
              f3(i, j, k) = sqrt(gptmass/(x1b(i)*sin(x2b(j)))) &
                            *(sin(x2b(j))/sin(pi/2.-disk_angle))**2
            endif
            if (x2b(j) > pi/2.+disk_angle) then
              f3(i, j, k) = sqrt(gptmass/(x1b(i)*sin(x2b(j)))) &
                            *(sin(x2b(j))/sin(pi/2.+disk_angle))**2
            endif
          end do
        end do
      end do
    case default
      call finalstop(routinename, 'Unrecognized toroidal field kind')
    end select

  END SUBROUTINE field_toroidal_untilted
!=======================================================================
  SUBROUTINE field_toroidal_tilted(f1, f2, f3, f)
    CHARACTER(LEN=*), PARAMETER :: routinename = 'z_field_toroidal_tilted'
    REALNUM, INTENT(INOUT), DIMENSION(isa:iea, jsa:jea, ksa:kea) :: f1, f2, f3
    type(fieldcontrol), intent(in) :: f
    if (f%strength == zro) return
    call finalstop(routinename, &
                   'Routine '//routinename//' not fully implemented')
  END SUBROUTINE field_toroidal_tilted
!=======================================================================

  SUBROUTINE rho_gp_cm_e
    USE zeus_mpi, only: mpifield, mpifield_e
    INTEGER i, j, k
    REALNUM r, theta
    REALNUM rmin, disk_angle, theta0, gammaone
    REALNUM vk0, cd0, cc0

    ! Obtain rho, gp, cm, and adjust their boundary conditions
    if (slab_thickness <= zro) then
    if (ellipsoid_radius_x < zro .and. ellipsoid_radius_y < zro &
        .and. ellipsoid_radius_z < zro) then
      call rho_gp_cm
    else
      call rho_gp_cm_ellipsoid
    end if
    else
    call rho_gp_cm_slab
    end if
    call boundary('d_')
    call writedata(d_, 'z_density_initial')
    if (withgp) then
      call mpifield(gp)
      call writedata(gp, 'z_initial_gravity_guess')
      call mpifield_e(gp_e)
      call writedata(gp_e, 'z_extended_initial_gravity_guess')
    end if

    !Obtain energy, and adjust its boundary conditions
    !if (ienergy == 1 ) then
    !  e_ =  d_ * ciso**2 / (gamma-one)
    !end if

    !Hydrostatic Equilibrium
    if (ienergy == 1) then
      rmin = x1ap(is)
      disk_angle = atan(disk_angle_factor*epsilon)
      gammaone = gamma - one

      do j = jsgp, jegp
        theta0 = x2bp(j)
        if (theta0 >= pi/2.-disk_angle) exit
      end do

      vk0 = sqrt(gptmass/rmin)
      cd0 = epsilon*vk0
      cc0 = vk0/sqrt(one + rho_exponent)

      do j = jsg, jeg
        theta = x2b(j)
        do i = isg, ieg
          r = x1b(i)
          if ((theta >= pi/2.-disk_angle) &
              .and. (theta <= pi/2.+disk_angle)) then
            e_(i, j, :) = d_(i, j, :)*cd0**2*rmin/(r*gammaone)
          else
            e_(i, j, :) = d_(i, j, :)*cc0**2*rmin/(r*gammaone)
          end if
        end do
      end do
    end if
    if (ienergy == 2) then
      e_ = ciso**2
    end if
    if (ienergy > 0) call boundary('e_')

  END SUBROUTINE rho_gp_cm_e
!=======================================================================
  SUBROUTINE rho_gp_cm
#ifdef MPI
    USE mpi
    integer ierr
#endif /* MPI */

    INTEGER i, j, k
    CHARACTER(LEN=*), PARAMETER :: routinename = 'z_rho_gp_cm'
    REALNUM :: r, theta, sintheta, costheta
    REALNUM :: z, varpi
    REALNUM :: dsq, x, y, sinphi, cosphi, phi, rhodifference
    REALNUM :: sphere_extra_mass
    !
    REALNUM :: rhovalue
    !
    REALNUM :: d_p(isap:ieap)
    REALNUM :: d_rt(isap:ieap, jsap:jeap)

    REALNUM :: massr(isap:ieap), sigma(isap:ieap), sigmamax, massmax
    REALNUM :: massrb(isap:ieap), sigmab(isap:ieap)
    !
    REALNUM :: rmin, disk_angle, theta0
    REALNUM :: f_theta0, rhoc0

    if (withoffsetspheretest) then
      ! Initialize density, and calculate exact potential
      rhodifference = rho_sphere - rho_background
      sphere_extra_mass = four*pi*rhodifference*sphere_radius**3/three
      write (*, *) rhodifference, sphere_extra_mass
      do k = ksg, keg
        phi = x3b(k)
        sinphi = sin(phi)
        cosphi = cos(phi)
        do j = jsg, jeg
          theta = x2b(j)
          sintheta = sin(theta)
          costheta = cos(theta)
          do i = isa, iea
            r = x1b(i)
            z = r*costheta
            varpi = r*sintheta
            x = varpi*cosphi
            y = varpi*sinphi
            dsq = (x - sphere_center_x)**2 &
                  + (y - sphere_center_y)**2 &
                  + (z - sphere_center_z)**2

            if (dsq > sphere_radius**2) then
              d_(i, j, k) = rho_background
            else
              d_(i, j, k) = rho_sphere
            end if
          end do
        end do
      end do

      if (withgp) then
        do k = ksg, keg
          phi = x3b(k)
          sinphi = sin(phi)
          cosphi = cos(phi)
          do j = jsg, jeg
            theta = x2b(j)
            sintheta = sin(theta)
            costheta = cos(theta)
            do i = isa_e, iea_e
              r = x1b_e(i)
              z = r*costheta
              varpi = r*sintheta
              x = varpi*cosphi
              y = varpi*sinphi
              dsq = (x - sphere_center_x)**2 &
                    + (y - sphere_center_y)**2 &
                    + (z - sphere_center_z)**2

              ! potential due to the background
              gp_e(i, j, k) = two*pi*rho_background &
                              *(x1ap(iep1p)**2 - r**2/three)

              if (dsq > sphere_radius**2) then
                ! Outer potential of the sphere
                gp_e(i, j, k) = gp_e(i, j, k) + sphere_extra_mass/sqrt(dsq)
              else
                ! Inner potential of the sphere
                gp_e(i, j, k) = gp_e(i, j, k) + two*pi*rhodifference &
                                *(sphere_radius**2 - dsq/three)
              end if
            end do
          end do
        end do
      end if

      if (withcentralmass) then
        ! Assume that the inner boundary is spherically symmetric (unchecked)
        ! Assume that rootnode touches the hole (checked)
        if (node == rootnode) then
          if (.not. l_firstnode1) then
            call finalstop(routinename, &
                           'Central mass must be calculated on the'// &
                           'first node in radial direction')
          end if
          rhovalue = d_(is, js, ks)
          centralmass = four*pi*rhovalue*x1ap(isp)**3/three
          write (*, *) centralmass, rhovalue
        end if
#ifdef MPI
        call MPI_BCAST(centralmass, 1, MPI_FLOATMPI, rootnode, &
                       MPI_COMM_WORLD, ierr)
#endif /* MPI */
      else
        centralmass = zro
      end if

      ! Subtract the outer potential of the hole (even for i<is)
      if (withgp) then
        do k = ksg, keg
          do j = jsg, jeg
            do i = isa_e, iea_e
              gp_e(i, j, k) = gp_e(i, j, k) - centralmass*x1bi_e(i)
            end do
          end do
        end do
        ! Multiply gp_e by Newton's constant
        gp_e = gp_e*gnewton
        ! Initialize gp
        gp = gp_e(isa:iea, :, :)
      end if
    end if

    if (.not. withoffsetspheretest) then
      if (node == rootnode) then
        if (rho_exponent == three/two) then
          !call threeoutput(routinename,'power law exponent -3/2')
          rmin = x1ap(is)
          disk_angle = atan(disk_angle_factor*epsilon)
          do j = jsgp, jegp
            theta0 = x2bp(j)
            if (theta0 >= pi/2.-disk_angle) exit
          end do
          f_theta0 = exp(-cos(theta0)**2/(2.*epsilon**2))
          rhoc0 = rho0*f_theta0*(one + rho_exponent)*epsilon**2
          do j = jsgp, jegp
            theta = x2bp(j)
            do i = isgp, iegp
              r = x1bp(i)
              if ((theta < pi/2.-disk_angle) &
                  .or. (theta > pi/2.+disk_angle)) then
                rhovalue = rhoc0*(rmin/r)**rho_exponent
                d_p(i) = rhovalue
                d_rt(i, j) = rhovalue
              else
                rhovalue = rho0*(rmin/r)**rho_exponent &
                           *exp(-cos(theta)**2/(2.*epsilon**2))
                d_p(i) = rhovalue
                d_rt(i, j) = rhovalue
              end if
            end do
          end do
        else if (r_c == zro) then
          !call threeoutput(routinename,'Pure rho power law')
          do i = isgp, iegp
            rhovalue = rho0*(x1bp(i)/r_reference)**(-rho_exponent)
            d_p(i) = rhovalue
          end do
          rhovalue = rho0*(x1ap(isp)/r_reference)**(-rho_exponent)
          centralmass = four*pi*rhovalue*x1ap(isp)**3/(three - rho_exponent)
        else
          !call threeoutput(routinename,'Flattened rho towards the center')
          !call threeoutput(routinename,'Power law outside')
          if (rho_exponent == two) then
            !call threeoutput(routinename,'special power law exponent 2')
            do i = isgp, iegp
              rhovalue = rho0/(one + (x1bp(i)/r_c)**2)
              d_p(i) = rhovalue
            end do
          else
            do i = isgp, iegp
              rhovalue = rho0/ &
                         ((one + (x1bp(i)/r_c)**2)**(haf*rho_exponent))
              d_p(i) = rhovalue
            end do
          end if
          rhovalue = rho0
          centralmass = four*pi*rhovalue*x1ap(isp)**3/three
        end if
      end if
#ifdef MPI
      call MPI_BCAST(centralmass, 1, MPI_FLOATMPI, rootnode, &
                     MPI_COMM_WORLD, ierr)
      call MPI_BCAST(d_p(isgp), iegp - isgp + 1, MPI_FLOATMPI, rootnode, &
                     MPI_COMM_WORLD, ierr)
      call MPI_BCAST(d_rt(isgp, jsgp), (iegp - isgp + 1)*(jegp - jsgp + 1), &
                     MPI_FLOATMPI, rootnode, MPI_COMM_WORLD, ierr)
#endif /* MPI */
      if (rho_exponent == three*haf) then
        do j = jsg, jeg
          do i = isg, ieg
            d_(i, j, :) = d_rt(i + inac*node1, j + jnac*node2)
          end do
        end do
      else
        do i = isg, ieg
          d_(i, :, :) = d_p(i + inac*node1)
        end do
      end if

      if (withgp) then

        if (node == rootnode) then
          ! Initial gp guess valid for an arbitrary spherically
          ! symmetric distribution
          massr = zro
          sigma = zro
          do i = isp, iep
            sigma(i + 1) = sigma(i) + d_p(i)*x1bp(i)*dx1ap(i)   ! divided by 4pi
            massr(i + 1) = massr(i) + d_p(i)*x1bp(i)**2*dx1ap(i)! divided by 4pi
          end do
          massmax = massr(iep1p)
          sigmamax = sigma(iep1p)
          do i = iep + 2, iegp
            sigma(i) = sigmamax
            massr(i) = massmax
          end do

          do i = isgp, iegop
            massrb(i) = haf*(massr(i) + massr(i + 1))
            sigmab(i) = haf*(sigma(i) + sigma(i + 1))
          end do
          massrb(iegp) = massmax
          sigmab(iegp) = sigmamax
        end if

#ifdef MPI
        call MPI_BCAST(massrb, ieap - isap + 1, MPI_FLOATMPI, rootnode, &
                       MPI_COMM_WORLD, ierr)
        call MPI_BCAST(sigmab, ieap - isap + 1, MPI_FLOATMPI, rootnode, &
                       MPI_COMM_WORLD, ierr)
        call MPI_BCAST(sigmamax, 1, MPI_FLOATMPI, rootnode, &
                       MPI_COMM_WORLD, ierr)
#endif /* MPI */

        do i = isg, ieg
          gp(i, :, :) = gnewton*four*pi*(massrb(i + inac*node1)*x1bi(i) &
                                         + sigmamax - sigmab(i + inac*node1))
        end do

        gp_e(isg:ieg, :, :) = gp(isg:ieg, :, :)
        if (l_firstnode1) gp_e(isap_e:isg - 1, :, :) = gnewton*four*pi*sigmamax
        if (l_lastnode1) then
          do i = ieg + 1, ieap_e
            gp_e(i, :, :) = gnewton*four*pi*massmax/x1b_e(i)
          end do
        end if

        ! Initialize gp
        gp = gp_e(isa:iea, :, :)
      end if
    end if

  END SUBROUTINE rho_gp_cm
!=======================================================================
  SUBROUTINE problem_common
    CHARACTER(LEN=*), PARAMETER :: routinename = 'z_problem_common'
    !CHARACTER(LEN=messagelinelength) message
    LOGICAL :: file_exists
    REALNUM :: temp, tempminus

    !call threeoutput(routinename,'begin')
    !
    !      Read PGEN
    !
    rewind (ioin)
    read (ioin, pgen)

    wave_period = wave_period*time_factor
    if (wind_r0 == zro) wind_r0 = x1ap(isp)
    if (spherical_r0 == zro) spherical_r0 = x1ap(isp)
    if (toomre_r0 == zro) toomre_r0 = x1ap(isp)

    if (wind_bphi_alfven_number /= 0) then
      wind_bphi_alfven_number_inverse = one/wind_bphi_alfven_number
    else
      wind_bphi_alfven_number_inverse = zro ! SPECIAL CASE
    end if

    ! Adjust the spline parameters
    if (theta_max_wind%transition >= ninety) then
      theta_max_wind%transition = ninety ! Always wind
    end if
    if (theta_max_wind%transition <= zro) then
      theta_max_wind%transition = zro ! Always infall
    end if
    if (theta_max_wind%width < zro) then
      call finalstop(routinename, 'Bad value: theta_max_wind%width<0.  Stop.')
    end if
    theta_max_wind%a_degree = theta_max_wind%transition &
                              - haf*theta_max_wind%width
    theta_max_wind%b_degree = theta_max_wind%transition &
                              + haf*theta_max_wind%width
    theta_max_wind%a = theta_max_wind%a_degree*degree
    theta_max_wind%b = theta_max_wind%b_degree*degree
    if (theta_max_wind%a > theta_max_wind%b) then
      call finalstop(routinename, 'Bad values in theta_max_wind.  Stop.')
    end if

    ! Adjust wind cone parameters
    if (windcone_rho%theta_degree >= theta_max_wind%a_degree) then
      call finalstop(routinename, 'Bad value: windcone_rho%theta_degree' &
                     //'>=theta_max_wind%a_degree.  Stop.')
    end if
    windcone_rho%theta = windcone_rho%theta_degree*degree

    temp = (wind_r0/x1ap(isp))**2* &
           wind_rho0/sin(windcone_rho%theta)**wind_n
    tempminus = windcone_rho%alpha*temp + windcone_rho%beta
    if (windcone_rho%n == zro) then
      windcone_rho%b = tempminus
      windcone_rho%a = zro
    else
      windcone_rho%a = (tempminus - windcone_rho%b)/ &
                       sin(windcone_rho%theta)**windcone_rho%n
    end if

    if (windcone_bphi%theta_degree >= theta_max_wind%a_degree) then
      call finalstop(routinename, 'Bad value: windcone_bphi%theta_degree' &
                     //'>=theta_max_wind%a_degree.  Stop.')
    end if
    windcone_bphi%theta = windcone_bphi%theta_degree*degree

    ! Reject unimplemented or invalid inzeus settings
    if (geometry /= 'rtp') call finalstop(routinename, 'Spherical RTP only')

    if (nodecyc1) call finalstop(routinename, &
                                 'Bad value: nodecyc1 should be .false.')
    if (nodecyc2) call finalstop(routinename, &
                                 'Bad value: nodecyc2 should be .false.')
    if (nnodes3 > 1) then
      if (.not. nodecyc3) then
        call finalstop(routinename, &
                       'Bad value: nodecyc3 should be .true. if nnodes3>1')
      end if
    end if

    if (number_of_tracer_fields > 0) &
      call finalstop(routinename, 'Tracer fields not implemented')

    if (ctboxtype /= 21) then
      ctboxtype = 21
      call warnoutput(routinename, &
                      'Variable ctboxtype set to 21')
    end if

    if (imhd /= 0) then
      if (mhddimension == 2) &
        call finalstop(routinename, 'Zeus2D-style not implemented')
      if (.not. withb3) call finalstop(routinename, 'MHD requires withb3')
      if (.not. withbp) call finalstop(routinename, 'MHD requires bp')
      !
      if (imhd /= 3) then
        call finalstop(routinename, 'The current MHD setup routine requires' &
                       //' imhd=0 or 3 because of the BCs')
      end if
    end if

    if (.not. (withv2 .and. withv3)) then
      call finalstop(routinename, 'All velocity components required')
    end if

    if (axisshift < 0) then
      call finalstop(routinename, 'Do not know what to do with a negative' &
                     //' axis shift')
    end if

    if (knac /= 0) then
      if (withb3bc) then
        withb3bc = .false.
        if (iolog /= NO_OUTPUT) write (iolog, parcon)
        if (iotty /= NO_OUTPUT) write (iotty, parcon)
        call warnoutput(routinename, 'No BC for Bphi in 3D. Variable withb3bc' &
                        //' set to .false.')
      end if
      if (bphiaxiszero) then
        bphiaxiszero = .false.
        call warnoutput(routinename, 'No BC for Bphi in 3D. Variable' &
                        //' bphiaxiszero set to .false.')
      end if

      if (with_ad_1f) then
        call warnoutput(routinename, 'AD implementation may be unfinished,' &
                        //' especially for kpi')
      end if

      !if (nnodes3>1.and..not.nodecyc3) &
      ! call finalstop(routinename, &
      ! 'Set nodecyc3=.true. for a parallel run with nnodes3>1 and active phi-zones.')

      if (nnodes3 > 1) then
        call warnoutput(routinename, 'kpi requires nnodes3=1, wall allows it,' &
                        //' testing needed')
      end if
      if (mod(knac, 2) /= 0) then
        call warnoutput(routinename, 'kpi implementation of flow-through BCs' &
                        //' requires knac to be even')
      end if
      call warnoutput(routinename, 'kpi implementation of flow-through BCs' &
                      //' requires the phi grid to have (phi,phi+pi)' &
                      //' symmetry. That is not checked, but it is' &
                      //' nevertheless required.')

      if (imhd /= 0) then
        if (imhd /= 3) then
          call finalstop(routinename, 'The current MHD setup routine requires' &
                         //' imhd=0 or 3 because of the BCs')
        end if
        if (withb3 .and. .not. withb3bc) then
          call warnoutput(routinename, 'The current wind setup routine may' &
                          //' work strangely without withb3bc')
        end if
        if (axisshift == 0) then
          call finalstop(routinename, 'The current MHD setup routine requires' &
                         //' axisshift in 3D')
        end if
        if (singularity_treatment) then
          call warnoutput(routinename, 'j1 singularity treatment probably' &
                          //' best reserved for 2D or non-MHD')
          call warnoutput(routinename, 'Value of singularity_treatment' &
                          //' probably should be reset to .false.')
          call warnoutput(routinename, 'Ask if you really want to utilize the' &
                          //' singularity treatment.')
          ! singularity_treatment=.false.
        end if
      end if

    end if

    if (knac == 0) then
      if (axisshift /= 0) then
        call finalstop(routinename, 'axisshift/=0 for a 2D problem.' &
                       //' Might be correct, but looks wrong.')
      end if
      if (.not. singularity_treatment) then
        call warnoutput(routinename, &
                        'j1 singularity treatment probably required for 2D')
        call warnoutput(routinename, &
                        'Value of singularity_treatment reset to .true.')
        call warnoutput(routinename, 'Ask if you really want to avoid the' &
                        //' singularity treatment.')
        singularity_treatment = .true.
      end if
    end if

    if (x2ap(jep1p) >= three*quarter*pi) then
      bc_sign = -one
    else
      bc_sign = +one
    end if

    if (theta_max_wind%transition == zro) then
      inner_i_outflow = .true. ! Might be true in some other cases
    else
      inner_i_outflow = .false.
    end if
    inner_j_outflow = .false.
    inner_k_outflow = .false.

    outer_i_outflow = .true.
    outer_j_outflow = .false.
    outer_k_outflow = .false.

    if (moving_grid1 .or. moving_grid2 .or. moving_grid3 .or. allow_moving) then
      call finalstop(routinename, &
                     'Grid motion not allowed for this problem setup')
    end if

    select case (gravity_algorithm)
    case ('none', 'sor', 'onlycentralmass')
      continue
    case default
      call finalstop(routinename, 'This gravity algorithm not implemented' &
                     //' for this problem setup')
    end select

    if (irestart == 0) then
      if (als_file_threecolumn /= '') then
        inquire (file=als_file_threecolumn, exist=file_exists)
        if (.not. file_exists) then
          call warnoutput(routinename, &
                          'File '//trim(als_file_threecolumn)// &
                          ' does not exist.  Variable reset.')
          als_file_threecolumn = ''
        end if
      end if
      if (als_file_sixcolumn /= '') then
        inquire (file=als_file_sixcolumn, exist=file_exists)
        if (.not. file_exists) then
          call warnoutput(routinename, &
                          'File '//trim(als_file_sixcolumn)// &
                          ' does not exist.  Variable reset.')
          als_file_sixcolumn = ''
        end if
      end if
      if (als_file_threecolumn /= '' .and. als_file_sixcolumn /= '') then
        call warnoutput(routinename, &
                        'Both files '//trim(als_file_threecolumn)// &
                        ' and '//trim(als_file_sixcolumn)// &
                        ' are present.  Choose using ' &
                        //trim(als_file_sixcolumn)// &
                        ' as an als_six_column file')
        als_file_threecolumn = ''
      end if

      if (als_density_factor <= 0) then
        call finalstop(routinename, &
                       'The als_density_factor must be >0')
      end if

      if (.not. (ienergy == 0 .or. ienergy == -1 .and. eoskind == 0)) then
        call warnoutput(routinename, 'Equilibrium of the ALS toroids' &
                        //' requires isothermal EQOS')
      end if

    end if

    call grid_validation_and_setup

    !
    ! Write down the PGEN setting that will be used, and the other
    ! possibly adjusted namelists:
    !
    if (iolog /= NO_OUTPUT) then
      write (iolog, pgen)
    end if
    if (iotty /= NO_OUTPUT) then
      write (iotty, pgen)
    end if

    select case (ohmic_exponent)
    case (0)
    case (1)
    case (100)
    case (200)
      call initialize_etar
    case default
      call finalstop(routinename, &
                     'This ohmic_exponent has not been implemented yet')
    end select

    ! Call problem_bc_start, and then update the bc data
    call problem_bc_start
    call problem_bc

  END SUBROUTINE problem_common
!=======================================================================
  SUBROUTINE grid_validation_and_setup
    CHARACTER(LEN=*), PARAMETER :: routinename = 'z_grid_validation_and_setup'
    !CHARACTER(LEN=messagelinelength) message
    INTEGER :: x2a_begin, x2a_end ! Estimated, in degrees

    REALNUM gridpoint, zonewidth

    ! Detect the nominal beginning and end of the grid
    gridpoint = x2ap(jsp)
    zonewidth = (x2ap(jsp + 1) - x2ap(jsp))*1e-2_pk
    if (abs(gridpoint - zro) < zonewidth .or. &
        abs(gridpoint - axisshift*degree) <= zonewidth*1e-4_pk) then
      ! If north pole is at the beginning of the grid
      x2a_begin = 0
    else if (abs(gridpoint - haf*pi) < zonewidth) then
      ! If equator is at the beginning of the grid
      x2a_begin = 90
    else if (abs(gridpoint - pi) < zonewidth) then
      x2a_begin = 180
    else
      x2a_begin = -1  ! Token value, meaning non-detection
    end if

    gridpoint = x2ap(jep + 1)
    zonewidth = (gridpoint - x2ap(jep))*1e-2_pk
    if (abs(gridpoint - pi) < zonewidth .or. &
        abs(gridpoint - (pi - axisshift*degree)) <= zonewidth*1e-4_pk) then
      ! If south pole is at the end of the grid
      x2a_end = 180
    else if (abs(gridpoint - haf*pi) < zonewidth) then
      x2a_end = 90
    else
      x2a_end = -1  ! Token value, meaning non-detection
    end if
    if (x2a_begin /= 0) then
      !write(message,*) 'Grid start : ', x2ap(jsp), ' Token: ', x2a_begin
      !call threeoutput(routinename,message)
      call finalstop(routinename, &
                     'The x2 grid should start near the north pole (theta=0).')
    end if

    if (x2a_end == 180) then
      bc_sign = -one
    else if (x2a_end == 90) then
      bc_sign = +one
    else
      !write(message,*) 'Grid end : ', x2ap(jep+1), ' Token: ', x2a_end
      !call threeoutput(routinename,message)
      call finalstop(routinename, 'The x2 grid should end near the south pole' &
                     //' or the equator (theta=pi, or pi/2).')
    end if

    if (bc_sign == +one) then
      select case (gravity_algorithm)
      case ('none', 'onlycentralmass')
        continue
      case default
        call finalstop(routinename, 'This gravity algorithm not implemented' &
                       //' for a theta grid ending at the equator')
      end select
    end if

  END SUBROUTINE grid_validation_and_setup
!=======================================================================
  SUBROUTINE start_from_two_d(success)
    LOGICAL, INTENT(OUT) :: success
    CHARACTER(LEN=*), PARAMETER :: routinename = 'z_start_from_two_d'

    INTEGER :: report
    INTEGER :: i, j, k, istart, iend

    success = .false.

    if ((knac == 0 .or. &
         (two_d_directory == '' .and. &
          two_d_prefix == '' .and. two_d_suffix == ''))) then
      return
    end if
    !call threeoutput(routinename,'try to start from 2D')

    ! Declare and allocate space for the read buffers
    ! Construct the filenames where the data to read is expected to be present
    ! Attempt to read
    !   (a) Transport the data from the read buffers to the main field arrays.
    !       Adjust data if needed.  More or less complex interpolations may be
    !       necessary.  Be careful to keep div(B)=0 in the process.
    !       Try to recover in cases of certain recoverable
    !       failures to read, such as a missing gp.
    !   (b) Report failure if reading failed, return gracefully

    ! Version 1: assumes that the 2D data is given in already joined files,
    ! with exactly the same x1 and x2 grids as the 3D run.

    report = -1
    call readfield(d_, 'd_', report)
    if (report > 0) then
      call warnoutput(routinename, &
                      'd_ field not read: try to start from scratch')
      success = .false.
      return
    end if

    call readfield(v1, 'v1')

    if (withv2) call readfield(v2, 'v2')
    if (withv3) call readfield(v3, 'v3')
    if (withbp) call readfield(b1, 'b1')
    if (withbp) call readfield(b2, 'b2')
    if (withb3) call readfield(b3, 'b3')
    if (ienergy > 0) call readfield(e_, 'e_')
    if (withgp) then
      report = -1
      call readfield(gp, 'gp', report)
      if (report > 0) then
        call warnoutput(routinename, 'gp field not read: gp set to one.')
        gp = one
      end if

      if (withextendedgrid) then
        if (l_firstnode1) then
          istart = is
        else
          istart = isa
        end if
        if (l_lastnode1) then
          iend = ie
        else
          iend = iea
        end if

        gp_e(istart:iend, :, :) = gp(istart:iend, :, :)
        if (l_firstnode1) then
          do k = ksa, kea
            do j = jsa, jea
              do i = isa_e, ism1
                gp_e(i, j, k) = gp(is, j, k) ! *x1b(is)*x1bi_e(i)
              end do
            end do
          end do
        end if
        if (l_lastnode1) then
          do k = ksa, kea
            do j = jsa, jea
              do i = iep1, iea_e
                gp_e(i, j, k) = gp(ie, j, k)*x1b(ie)*x1bi_e(i)
              end do
            end do
          end do
        end if
      end if
    end if

    if (withcentralmass) then
      report = -1
      call readfield(centralmass, 'cm', report)
      if (report > 0) then
        call warnoutput(routinename, &
                        'cm field not read: centralmass set to zero.')
        centralmass = zro
      end if
    end if

    ! Assume success, because failure would have crashed the run
    ! or would have forced an early exit from this routine by now.
    success = .true.

    ! Code written for a future version where failure
    ! could be possible at this stage.
    if (.not. success) then
      call warnoutput(routinename, &
                      'Failure in initializing the fields' &
                      //' using files with prefix (' &
                      //trim(two_d_prefix)//') and suffix ('// &
                      trim(two_d_suffix)//')')
    end if

  END SUBROUTINE start_from_two_d
!=======================================================================
  SUBROUTINE readfield_2d(field, name, reportfailure)
    CHARACTER(LEN=*), PARAMETER :: routinename = 'z_readfield_2d'
    REALNUM, INTENT(inout) :: field(:, :, :)
    CHARACTER(LEN=*), INTENT(in) :: name

    ! On input: zero (or absent) if a crash is desired on failure;
    !           a strictly negative value if a report is desired.
    ! On output: zero in case of success; a strictly positive error code
    !            in case of failure
    INTEGER, INTENT(inout), OPTIONAL :: reportfailure
    INTEGER :: l_reportfailure

    REALNUM, ALLOCATABLE :: field_2d(:, :)
    INTEGER :: k
    CHARACTER(LEN=messagelinelength) :: message

    if (present(reportfailure)) then
      l_reportfailure = reportfailure
    else
      l_reportfailure = 0
    end if

    allocate (field_2d(ieap - isap + 1, jeap - jsap + 1))

    call threeoutput(routinename, 'Trying to read file '// &
                     trim(two_d_directory)// &
                     '/'// &
                     trim(two_d_prefix)// &
                     trim(name)// &
                     trim(two_d_suffix) &
                     )

    call readdata(field_2d, &
                  trim(two_d_directory)// &
                  '/'// &
                  trim(two_d_prefix)// &
                  trim(name)// &
                  trim(two_d_suffix), &
                  SINGLEFILE=.true., REPORTFAILURE=l_reportfailure)

    if (l_reportfailure == 0) then
      do k = ksa, kea
        field(isa:iea, jsa:jea, k) = &
          field_2d(isa + node1*inac:iea + node1*inac, &
                   jsa + node2*jnac:jea + node2*jnac)
      end do
      call threeoutput(routinename, &
                       'Field '//trim(name)//' has been transferred')
    else
      write (message, '(i8)') l_reportfailure
      call warnoutput(routinename, 'Failure in reading field '//trim(name)// &
                      ' Error value: '//trim(message))
    end if

    if (present(reportfailure)) reportfailure = l_reportfailure

    deallocate (field_2d)

  END SUBROUTINE readfield_2d
!=======================================================================
  SUBROUTINE readfield_0d(field, name, reportfailure)
    CHARACTER(LEN=*), PARAMETER :: routinename = 'z_readfield_0d'
    REALNUM, INTENT(inout) :: field
    CHARACTER(LEN=*), INTENT(in) :: name

    ! On input: zero (or absent) if a crash is desired on failure;
    !           a strictly negative value if a report is desired.
    ! On output: zero in case of success; a strictly positive error code
    !            in case of failure
    INTEGER, INTENT(inout), OPTIONAL :: reportfailure
    INTEGER :: l_reportfailure

    CHARACTER(LEN=messagelinelength) :: message

    if (present(reportfailure)) then
      l_reportfailure = reportfailure
    else
      l_reportfailure = 0
    end if

    call threeoutput(routinename, 'Trying to read file '// &
                     trim(two_d_directory)// &
                     '/'// &
                     trim(two_d_prefix)// &
                     trim(name)// &
                     trim(two_d_suffix) &
                     )

    call readdata(field, &
                  trim(two_d_directory)// &
                  '/'// &
                  trim(two_d_prefix)// &
                  trim(name)// &
                  trim(two_d_suffix), &
                  SINGLEFILE=.true., REPORTFAILURE=l_reportfailure)

    if (l_reportfailure == 0) then
      call threeoutput(routinename, &
                       'Field '//trim(name)//' has been transferred')
    else
      write (message, '(i8)') l_reportfailure
      call warnoutput(routinename, 'Failure in reading field '//trim(name)// &
                      ' Error value: '//trim(message))
    end if

    if (present(reportfailure)) reportfailure = l_reportfailure

  END SUBROUTINE readfield_0d
!=======================================================================
  SUBROUTINE problem_perturb
    CHARACTER(LEN=*), PARAMETER :: routinename = 'z_problem_perturb'

    call threeoutput(routinename, 'Perturbing the data if required')

    if (perturb_d_ /= zro) then
      call perturb_field(d_, perturb_d_, perturb_d__m, perturb_d__phase, 1)
    end if
    call boundary('d_')

    if (perturb_v1 /= zro) then
      call perturb_field(v1, perturb_v1, perturb_v1_m, perturb_v1_phase, 1)
    end if
    call boundary('v1')

    if (perturb_v2 /= zro .and. withv2) then
      call perturb_field(v2, perturb_v2, perturb_v2_m, perturb_v2_phase, 1)
    end if
    call boundary('v2')

    if (perturb_v3 /= zro .and. withv3) then
      call perturb_field(v3, perturb_v3, perturb_v3_m, perturb_v3_phase, 0)
    end if
    call boundary('v3')

    if (perturb_bp /= zro .and. withbp) then
      call perturb_field(b1, perturb_bp, perturb_bp_m, perturb_bp_phase, 1)
      call perturb_field(b2, perturb_bp, perturb_bp_m, perturb_bp_phase, 1)
    end if

    if (perturb_e_ /= zro .and. ienergy > 0) &
      call perturb_field(e_, perturb_e_, perturb_e__m, perturb_e__phase, 1)

  END SUBROUTINE problem_perturb
!=======================================================================
  SUBROUTINE perturb_field(field, amplitude, mnumber, phase, kcentering)
    CHARACTER(LEN=*), PARAMETER :: routinename = 'z_perturb_field'
    REALNUM, DIMENSION(:, :, :), INTENT(INOUT) :: field
    REALNUM, INTENT(IN) :: amplitude, mnumber, phase
    INTEGER, INTENT(IN) :: kcentering
    INTEGER k

    if (kcentering == 1) then
      do k = ksa, kea
        field(:, :, k) = field(:, :, k) &
                         *(one + amplitude*sin(x3b(k)*mnumber + phase))
      end do
      return
    end if

    if (kcentering == 0) then
      do k = ksa, kea
        field(:, :, k) = field(:, :, k) &
                         *(one + amplitude*sin(x3a(k)*mnumber + phase))
      end do
      return
    end if

    call finalstop(routinename, 'Error.  Should not arrive here.')

  END SUBROUTINE perturb_field
!=======================================================================
  SUBROUTINE initialize_etar
    integer i
    allocate (eta_ra(isa:iea))
    allocate (eta_rb(isa:iea))
    do i = isa, iea
      eta_ra(i) = eta_r(x1a(i))
      eta_rb(i) = eta_r(x1b(i))
    end do
  END SUBROUTINE initialize_etar
!=======================================================================
  REALNUM FUNCTION eta_r(r)
    CHARACTER(LEN=*), PARAMETER :: routinename = 'z_eta_r'
    REALNUM, INTENT(in) :: r
    if (eta_width /= zro) call finalstop(routinename, &
                                         'Zero width only (in this version)')
    if (r < eta_transition) then
      eta_r = eta_inner
    else
      eta_r = eta_outer
    end if
  END FUNCTION eta_r
!=======================================================================
  SUBROUTINE deplete_z
    CHARACTER(LEN=*), PARAMETER :: routinename = 'z_deplete_z'
    INTEGER i, j, k
    REALNUM z

    if (depletion_z <= zro) return
    if (irestart /= 5) call finalstop(routinename, &
                                      'irestart=5 only for this functionality')
    do k = ks, ke
      do j = js, je
        do i = is, ie
          z = abs(x1b(i)*cos(x2b(j)))
          d_(i, j, k) = d_(i, j, k)/(one + (z/depletion_z)**depletion_exponent)
        end do
      end do
    end do

    ! Copied and pasted from the enforcedensityfloor routine, but without
    ! incrementing the mass accumulator

    do k = ks, ke
      if (x3b(k) < densityfloor_x3_min) cycle
      if (x3b(k) > densityfloor_x3_max) exit
      do j = js, je
        if (x2b(j) < densityfloor_x2_min) cycle
        if (x2b(j) > densityfloor_x2_max) exit
        do i = is, ie
          if (x1b(i) < densityfloor_x1_min) cycle
          if (x1b(i) > densityfloor_x1_max) exit

          d_(i, j, k) = max(d_(i, j, k), densityfloor)

        end do
      end do
    end do

    call boundary('d_')
  END SUBROUTINE deplete_z
!=======================================================================
  SUBROUTINE rho_gp_cm_slab
#ifdef MPI
    USE mpi
    integer ierr
#endif /* MPI */

    INTEGER i, j, k
    CHARACTER(LEN=*), PARAMETER :: routinename = 'z_rho_gp_cm_slab'

    REALNUM z
    REALNUM mtile, mtotal, density0

    do k = ksg, keg
      do j = jsg, jeg
        do i = isg, ieg
          z = x1b(i)*cos(x2b(j))
          d_(i, j, k) = one/(one + (abs(z)/slab_thickness)**slab_exponent)
        end do
      end do
    end do

    mtile = zro
    do k = ks, ke
      do j = js, je
        do i = is, ie
          mtile = mtile + d_(i, j, k)*dvl1a(i)*dvl2a(j)*dvl3a(k)
        end do
      end do
    end do

#ifdef MPI
    call MPI_REDUCE(mtile, mtotal, 1, &
                    MPI_FLOATMPI, MPI_SUM, rootnode, MPI_COMM_WORLD, ierr)
#else /* MPI */
    mtotal = mtile
#endif /* MPI */

    if (node == rootnode) then
      density0 = initial_total_mass/mtotal
    end if
#ifdef MPI
    call MPI_BCAST(density0, 1, MPI_FLOATMPI, rootnode, &
                   MPI_COMM_WORLD, ierr)
#endif /* MPI */

    d_ = density0*d_

    ! Copied and pasted from the enforcedensityfloor routine, but without
    ! incrementing the mass accumulator
    do k = ks, ke
      if (x3b(k) < densityfloor_x3_min) cycle
      if (x3b(k) > densityfloor_x3_max) exit
      do j = js, je
        if (x2b(j) < densityfloor_x2_min) cycle
        if (x2b(j) > densityfloor_x2_max) exit
        do i = is, ie
          if (x1b(i) < densityfloor_x1_min) cycle
          if (x1b(i) > densityfloor_x1_max) exit

          d_(i, j, k) = max(d_(i, j, k), densityfloor)

        end do
      end do
    end do

    if (withcentralmass) then
      if (node == rootnode) then
        centralmass = density0*(four/three)*pi*x1ap(isp)**3
      end if
#ifdef MPI
      call MPI_BCAST(centralmass, 1, MPI_FLOATMPI, rootnode, &
                     MPI_COMM_WORLD, ierr)
#endif /* MPI */
    else
      centralmass = zro
    end if

    if (withgp) then
      ! Do not even try seriously to get an initial gp and gp_e
      ! Just have something in
      do k = ksg, keg
        do j = jsg, jeg
          do i = jsg, ieg
            gp = gnewton*initial_total_mass*x1bi(i)
          end do
        end do
      end do
      do k = ksg, keg
        do j = jsg, jeg
          do i = isa_e, iea_e
            gp_e(i, j, k) = gnewton*initial_total_mass*x1bi_e(i)
          end do
        end do
      end do
    end if
  END SUBROUTINE rho_gp_cm_slab
!=======================================================================
  SUBROUTINE rho_gp_cm_ellipsoid
#ifdef MPI
    USE mpi
    integer ierr
#endif /* MPI */
    CHARACTER(LEN=*), PARAMETER :: routinename = 'z_rho_gp_cm_ellipsoid'
    !CHARACTER(LEN=messagelinelength) :: message

    INTEGER i, j, k

    REALNUM x, y, z, w, onex, oney, onez, varpi
    REALNUM mtile, mtotal, density0

    onex = one; if (ellipsoid_radius_x <= zro) onex = zro
    oney = one; if (ellipsoid_radius_y <= zro) oney = zro
    onez = one; if (ellipsoid_radius_z <= zro) onez = zro

    if (onek /= zro) then
      do k = ksg, keg
        do j = jsg, jeg
          do i = isg, ieg
            x = x1b(i)*sin(x2b(j))*cos(x3b(k))
            y = x1b(i)*sin(x2b(j))*sin(x3b(k))
            z = x1b(i)*cos(x2b(j))
            w = sqrt(onex*(x/ellipsoid_radius_x)**2 + &
                     oney*(y/ellipsoid_radius_y)**2 + &
                     onez*(z/ellipsoid_radius_z)**2)
            d_(i, j, k) = one/(one + w**ellipsoid_exponent)
          end do
        end do
      end do
    else
      do k = ksg, keg
        do j = jsg, jeg
          do i = isg, ieg
            varpi = x1b(i)*sin(x2b(j))
            z = x1b(i)*cos(x2b(j))
            w = sqrt(onex*(varpi/ellipsoid_radius_x)**2 + &
                     onez*(z/ellipsoid_radius_z)**2)
            d_(i, j, k) = one/(one + w**ellipsoid_exponent)
          end do
        end do
      end do
    end if

    mtile = zro
    do k = ks, ke
      do j = js, je
        do i = is, ie
          mtile = mtile + d_(i, j, k)*dvl1a(i)*dvl2a(j)*dvl3a(k)
        end do
      end do
    end do

    !write(message,*) 'mtile, mtotal, node, before MP', mtile, mtotal, node
    !call threeoutput(routinename,message)

#ifdef MPI
    call MPI_REDUCE(mtile, mtotal, 1, &
                    MPI_FLOATMPI, MPI_SUM, rootnode, MPI_COMM_WORLD, ierr)
#else /* MPI */
    mtotal = mtile
#endif /* MPI */

    !write(message,*) 'mtile, mtotal, node, after  MP', mtile, mtotal, node
    !call threeoutput(routinename,message)

    if (node == rootnode) then
      density0 = initial_total_mass/mtotal
    end if
#ifdef MPI
    call MPI_BCAST(density0, 1, MPI_FLOATMPI, rootnode, &
                   MPI_COMM_WORLD, ierr)
#endif /* MPI */

    d_ = density0*d_

    ! Copied and pasted from the enforcedensityfloor routine, but without
    ! incrementing the mass accumulator
    do k = ks, ke
      if (x3b(k) < densityfloor_x3_min) cycle
      if (x3b(k) > densityfloor_x3_max) exit
      do j = js, je
        if (x2b(j) < densityfloor_x2_min) cycle
        if (x2b(j) > densityfloor_x2_max) exit
        do i = is, ie
          if (x1b(i) < densityfloor_x1_min) cycle
          if (x1b(i) > densityfloor_x1_max) exit

          d_(i, j, k) = max(d_(i, j, k), densityfloor)

        end do
      end do
    end do

    if (withcentralmass) then
      if (node == rootnode) then
        centralmass = density0*(four/three)*pi*x1ap(isp)**3
      end if
#ifdef MPI
      call MPI_BCAST(centralmass, 1, MPI_FLOATMPI, rootnode, &
                     MPI_COMM_WORLD, ierr)
#endif /* MPI */
    else
      centralmass = zro
    end if

    if (withgp) then
      ! Do not even try seriously to get an initial gp and gp_e
      ! Just have something in
      do k = ksg, keg
        do j = jsg, jeg
          do i = jsg, ieg
            gp = gnewton*initial_total_mass*x1bi(i)
          end do
        end do
      end do
      do k = ksg, keg
        do j = jsg, jeg
          do i = isa_e, iea_e
            gp_e(i, j, k) = gnewton*initial_total_mass*x1bi_e(i)
          end do
        end do
      end do
    end if
  END SUBROUTINE rho_gp_cm_ellipsoid
!=======================================================================
  SUBROUTINE add_waves
#ifdef MPI
    USE mpi
    integer ierr
    REALNUM, DIMENSION(2) ::  bufferin, bufferout
#endif /* MPI */

    CHARACTER(LEN=*), PARAMETER :: routinename = 'z_waves'
    !CHARACTER(LEN=4*messagelinelength) :: message
    INTEGER n, idum
    REALNUM, ALLOCATABLE :: wave_parameters(:, :)
    REALNUM kx, ky, kz, kr, krsq, kvarpi, ktheta, psi, phase
    REALNUM lambda, loglower, logupper, logspacing, loglambda
    REALNUM, DIMENSION(3) :: kvr, kvtheta, kvphi, v
    REALNUM :: x, y, z, r, theta, phi, cosine
    REALNUM :: dm, total_mass, unadjusted_kinetic_energy
    REALNUM :: kinetic_energy, adjustment_factor

    REALNUM, POINTER, DIMENSION(:, :, :) :: lv1 => null(), lv2 => null(), &
                                            lv3 => null()

    INTEGER i, j, k

    INTEGER, PARAMETER :: &
      i_kx = 1, i_ky = 2, i_kz = 3, i_psi = 4, i_phase = 5, &
      i_final = i_phase

    call get_memory(lv1)
    call get_memory(lv2)
    call get_memory(lv3)
    lv1 = zro
    lv2 = zro
    lv3 = zro

    waves_lower_wavelength = x1ap(isp)
    waves_upper_wavelength = x1ap(iep + 1)

    rewind (ioin)
    read (ioin, wavegen, end=10)
    goto 20
10  continue
    call warnoutput(routinename, 'Namelist wavegen not found on input file.')
20  continue
    if (waves_total_number <= 0) return

    if (iolog /= NO_OUTPUT) write (iolog, wavegen)
    if (iotty /= NO_OUTPUT) write (iotty, wavegen)

    allocate (wave_parameters(i_final, waves_total_number))

    if (node == rootnode) then
      idum = waves_random_seed

      do n = 1, waves_total_number

        do  ! Find the unit vector kr as kx, ky, kz normalized to 1.0
          kx = two*ran2(idum) - one
          ky = two*ran2(idum) - one
          kz = two*ran2(idum) - one
          krsq = kx**2 + ky**2 + kz**2
          if (krsq < one) then
            kvarpi = sqrt(kx**2 + ky**2)
            ktheta = atan2(kvarpi, kz)
            if (ktheta /= zro .and. ktheta /= pi .and. &
                ktheta >= x2ap(jsp) .and. ktheta <= x2ap(jep + 1)) exit
          end if
        end do
        kr = sqrt(krsq)
        kx = kx/kr
        ky = ky/kr
        kz = kz/kr

        psi = two*pi*ran2(idum)
        phase = two*pi*ran2(idum)

        wave_parameters(i_kx, n) = kx
        wave_parameters(i_ky, n) = ky
        wave_parameters(i_kz, n) = kz
        wave_parameters(i_psi, n) = psi
        wave_parameters(i_phase, n) = phase

      end do
    end if
#ifdef MPI
    call MPI_BCAST(wave_parameters(1, 1), waves_total_number*i_final, &
                   MPI_FLOATMPI, rootnode, MPI_COMM_WORLD, ierr)
#endif /* MPI */

    loglower = log(waves_lower_wavelength)
    logupper = log(waves_upper_wavelength)
    logspacing = (logupper - loglower)/real(waves_total_number, pk)
    do n = 1, waves_total_number
      loglambda = loglower + (n - haf)*logspacing
      lambda = exp(loglambda)

      ! The unit vector kr is stored in wave_parameters(1:3,n)
      ! The other two unit vectors can now be found
      kvr = wave_parameters(i_kx:i_kz, n)
      kx = kvr(1)
      ky = kvr(2)
      kz = kvr(3)
      psi = wave_parameters(i_psi, n)
      phase = wave_parameters(i_phase, n)

      !write(message,*) 'Wave# ', n, ' kx    = ', kx,     ' ky  = ', ky,  ' kz   = ', kz
      !call threeoutput(routinename,message)
      !write(message,*) 'Wave# ', n, ' lambda= ', lambda, ' psi = ', psi, ' phase= ', phase
      !call threeoutput(routinename,message)

      kvarpi = sqrt(kx**2 + ky**2)
      kvtheta(1) = kx*kz/kvarpi ! South unit vector in k-space
      kvtheta(2) = ky*kz/kvarpi
      kvtheta(3) = -kvarpi
      kvphi(1) = -ky/kvarpi ! East unit vector in k-space
      kvphi(2) = +kx/kvarpi
      kvphi(3) = zro

      ! Cartesian velocity vector, obtained using a value of
      ! psi defined as starting from the north, and moving to the east
      v = (cos(psi)*(-kvtheta) + sin(psi)*kvphi)*lambda**waves_amplitude_index

      do k = ksg, keg
        phi = x3b(k)
        do j = jsg, jeg
          theta = x2b(j)
          do i = isg, ieg
            r = x1a(i)
            x = r*sin(theta)*cos(phi)
            y = r*sin(theta)*sin(phi)
            z = r*cos(theta)
            cosine = cos((two*pi/lambda)*(kx*x + ky*y + kz*z) + phase)
            ! Project v into the local spherical components
            lv1(i, j, k) = lv1(i, j, k) &
                           + cosine*(v(1)*sin(theta)*cos(phi) &
                                     + v(2)*sin(theta)*sin(phi) &
                                     + v(3)*cos(theta))
          end do
        end do
      end do
      do k = ksg, keg
        phi = x3b(k)
        do j = jsg, jeg
          theta = x2a(j)
          do i = isg, ieg
            r = x1b(i)
            x = r*sin(theta)*cos(phi)
            y = r*sin(theta)*sin(phi)
            z = r*cos(theta)
            cosine = cos((two*pi/lambda)*(kx*x + ky*y + kz*z) + phase)
            ! Project v into the local spherical components
            lv2(i, j, k) = lv2(i, j, k) &
                           + cosine*(v(1)*cos(theta)*cos(phi) &
                                     + v(2)*cos(theta)*sin(phi) &
                                     - v(3)*sin(theta))
          end do
        end do
      end do
      do k = ksg, keg
        phi = x3a(k)
        do j = jsg, jeg
          theta = x2b(j)
          do i = isg, ieg
            r = x1b(i)
            x = r*sin(theta)*cos(phi)
            y = r*sin(theta)*sin(phi)
            z = r*cos(theta)
            cosine = cos((two*pi/lambda)*(kx*x + ky*y + kz*z) + phase)
            ! Project v into the local spherical components
            lv3(i, j, k) = lv3(i, j, k) + cosine* &
                           (-v(1)*sin(phi) + v(2)*cos(phi))
          end do
        end do
      end do
    end do

    ! Adjust velocity magnitude, globally
    total_mass = zro
    unadjusted_kinetic_energy = zro
    do k = ks, ke
      do j = ks, je
        do i = is, ie
          dm = d_(i, j, k)*dvl1a(i)*dvl2a(j)*dvl3a(k)
          total_mass = total_mass + dm
          unadjusted_kinetic_energy = unadjusted_kinetic_energy + &
                                      eighth*dm*((lv1(i, j, k) &
                                                  + lv1(i + ione, j, k))**2 + &
                                                 (lv2(i, j, k) &
                                                  + lv2(i, j + jone, k))**2 + &
                                                 (lv3(i, j, k) &
                                                  + lv3(i, j, k + kone))**2)
        end do
      end do
    end do

! Sum over all tiles
#ifdef MPI
    bufferin(1) = total_mass
    bufferin(2) = unadjusted_kinetic_energy
    call MPI_REDUCE(bufferin, bufferout, 2, &
                    MPI_FLOATMPI, MPI_SUM, rootnode, MPI_COMM_WORLD, ierr)
    if (node == rootnode) then
      total_mass = bufferout(1)
      unadjusted_kinetic_energy = bufferout(2)
    end if
#endif /* MPI */

! Find the adjustment factor
    if (node == rootnode) then
      kinetic_energy = haf*total_mass*(waves_amplitude_scale*ciso)**2
      adjustment_factor = sqrt(kinetic_energy/unadjusted_kinetic_energy)
    end if

! Broadcast the adjustment factor

#ifdef MPI
    call MPI_BCAST(adjustment_factor, 1, &
                   MPI_FLOATMPI, rootnode, MPI_COMM_WORLD, ierr)
#endif /* MPI */

! Adjust
    lv1 = lv1*adjustment_factor
    lv2 = lv2*adjustment_factor
    lv3 = lv3*adjustment_factor

! Add
    v1 = v1 + lv1
    v2 = v2 + lv2
    v3 = v3 + lv3

    !write(message,*) total_mass, unadjusted_kinetic_energy, adjustment_factor
    !call threeoutput(routinename,message)

    call boundary('v1')
    call boundary('v2')
    call boundary('v3')

    call release_memory(lv1)
    call release_memory(lv2)
    call release_memory(lv3)

  END SUBROUTINE add_waves
!=======================================================================
  REALNUM FUNCTION ran2(idum)  ! From the Numerical Recipes
    INTEGER, INTENT(inout) :: idum

!     Long period (> 2e18) random number generator of L'Ecuyer
!     with Bays-Durham shuffle and added safeguards.
!     Returns a uniform random deviate between 0.0 and 1.0 (exclusive
!     of the endpoint values).
!     Call with idum a negative integer to initialize; thereafter, do not
!     alter idum between successive deviates in a sequence.
!     RNMX should approximate the largest
!     floating value that is less than 1.

    INTEGER, PARAMETER :: IM1 = 2147483563, IM2 = 2147483399, IMM1 = IM1 - 1, &
                          IA1 = 40014, IA2 = 40692, IQ1 = 53668, IQ2 = 52774, &
                          IR1 = 12211, IR2 = 3791, NTAB = 32, &
                          NDIV = 1 + IMM1/NTAB
    REALNUM, PARAMETER :: AM = one/IM1, RNMX = nearest(one, -one)

    INTEGER, SAVE :: idum2 = 123456789
    INTEGER, SAVE :: iy = 0
    INTEGER, SAVE :: iv(NTAB) = 0

    INTEGER j, k

    if (idum <= 0) then ! Initialize.
      idum = max(-idum, 1) ! Be sure to prevent idum = 0.
      idum2 = idum
      do j = NTAB + 8, 1, -1 ! Load the shuffle table (after 8 warm-ups).
        k = idum/IQ1
        idum = IA1*(idum - k*IQ1) - k*IR1
        if (idum < 0) idum = idum + IM1
        if (j <= NTAB) iv(j) = idum
      end do
      iy = iv(1)
    end if

    k = idum/IQ1 ! Start here when not initializing.
    idum = IA1*(idum - k*IQ1) - k*IR1  ! Compute idum=mod(IA1*idum,IM1) without
    if (idum < 0) idum = idum + IM1    ! overflows by Schrage's method.
    k = idum2/IQ2
    idum2 = IA2*(idum2 - k*IQ2) - k*IR2 !Compute idum2=mod(IA2*idum2,IM2) likewise.
    if (idum2 < 0) idum2 = idum2 + IM2
    j = 1 + iy/NDIV               ! Will be in the range 1:NTAB.
    iy = iv(j) - idum2            ! Here idum is shuffled, idum and idum2 are
    iv(j) = idum                ! combined to generate output.
    if (iy < 1) iy = iy + IMM1
    ran2 = min(AM*iy, RNMX) ! Because users don't expect endpoint values.

  END FUNCTION ran2
!=======================================================================
  SUBROUTINE wind
    CHARACTER(LEN=*), PARAMETER :: routinename = 'z_wind'
    !CHARACTER(LEN=messagelinelength) :: message

    INTEGER i

    if (.not. l_firstnode1) return

    ! REVISE OR CRASH THIS CODE if wind_initial_zones/=0 and
    ! theta_max_wind % transition is not 90.0

    if (wind_initial_zones <= 0) return ! Nothing to do

    if (theta_max_wind%transition < 90.0_pk) &
      call finalstop(routinename, &
                     'In the current implementation, wind_initial_zones=0' &
                     //' if the wind does not cover all theta values')

    !write(message,*) 'wind_v0 = ',wind_v0
    !call writemessage(routinename,message)

    if (wind_v0 <= zro) &
      call finalstop(routinename, &
                     'In the current implementation, set wind_initial_zones=0' &
                     //' if the initial wind_v0 is not positive')

    do i = is, is + wind_initial_zones - 1
      d_(i, :, :) = d_(ism1, :, :)*(x1b(ism1)*x1bi(i))**2
      if (ienergy == 1) e_(i, :, :) = e_(ism1, :, :)*(x1b(ism1)*x1bi(i))**2
      v1(i, :, :) = v1(ism1, :, :)
    end do
    if (withb3) then
      do i = is, is + wind_initial_zones - 1
        b3(i, :, :) = b3(ism1, :, :)*(x1b(ism1)*x1bi(i))
      end do
    end if

    if (withv2) v2(is:is + wind_initial_zones - 1, :, :) = zro
    if (withv3) v3(is:is + wind_initial_zones - 1, :, :) = zro

  END SUBROUTINE wind
!=======================================================================
END MODULE problem_initial
