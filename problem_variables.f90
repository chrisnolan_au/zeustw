#include "macros"
MODULE problem_variables
  USE zeus_variables
  IMPLICIT NONE
  PUBLIC
  SAVE

  namelist_variable(pgen, REALNUM, bc_sign, -one)  ! Always overriden
  namelist_variable(pgen, LOGICAL, singularity_treatment, .false.) ! FOR TESTING

  namelist_variable(pgen, REALNUM, initial_total_mass, 2e33_pk)
  namelist_variable(pgen, REALNUM, slab_thickness, -one)
  namelist_variable(pgen, REALNUM, slab_exponent, two)

  namelist_variable(pgen, REALNUM, ellipsoid_radius_x, -one)
  namelist_variable(pgen, REALNUM, ellipsoid_radius_y, -one)
  namelist_variable(pgen, REALNUM, ellipsoid_radius_z, -one)
  namelist_variable(pgen, REALNUM, ellipsoid_exponent, two)

  namelist_variable(pgen, REALNUM, rho0, 1.4e-19_pk)
  namelist_variable(pgen, REALNUM, rho_exponent, zro)
  namelist_variable(pgen, REALNUM, r_reference, 1.5e17_pk)
  namelist_variable(pgen, REALNUM, r_c, zro)
  namelist_variable(pgen, REALNUM, epsilon, 0.1_pk)
  ! namelist_variable(pgen, REALNUM, delta, 1e-4_pk)
  namelist_variable(pgen, REALNUM, m, one)
  namelist_variable(pgen, REALNUM, disk_angle_factor, two)

  namelist_variable(pgen, LOGICAL, bphiaxiszero, .false.)

  TYPE fieldcontrol
    REALNUM strength, theta, phi, parameter
    CHARACTER(LEN=16) kind, algorithm
  END TYPE fieldcontrol

  namelist_variable(pgen, type(fieldcontrol), bp, fieldcontrol(zro, zro, zro, zro, 'uniform', 'potential'))
  namelist_variable(pgen, type(fieldcontrol), bt, fieldcontrol(zro, zro, zro, zro, 'linear', 'potential'))
  namelist_variable(pgen, type(fieldcontrol), vp, fieldcontrol(zro, zro, zro, zro, 'uniform', 'direct'))
  namelist_variable(pgen, type(fieldcontrol), vt, fieldcontrol(zro, zro, zro, zro, 'linear', 'direct'))

  ! If knac/=0, and any of the two_d_run variables is not empty,
  ! the program will try to read the data from a 2D run as input,
  ! and ignore other initial condition information
  namelist_variable(pgen, CHARACTER(LEN=filenamelength), two_d_directory, '')
  namelist_variable(pgen, CHARACTER(LEN=filenamelength), two_d_prefix, '')
  namelist_variable(pgen, CHARACTER(LEN=filenamelength), two_d_suffix, '')

  namelist_variable(pgen, LOGICAL, withoffsetspheretest, .false.)
  namelist_variable(pgen, REALNUM, rho_background, zro)
  namelist_variable(pgen, REALNUM, rho_sphere, one)
  namelist_variable(pgen, REALNUM, sphere_radius, one)
  namelist_variable(pgen, REALNUM, sphere_center_x, zro)
  namelist_variable(pgen, REALNUM, sphere_center_y, zro)
  namelist_variable(pgen, REALNUM, sphere_center_z, zro)

  namelist_variable(pgen, LOGICAL, with_restart_perturbations, .false.)

  namelist_variable(pgen, REALNUM, perturb_d_, zro)
  namelist_variable(pgen, REALNUM, perturb_d__m, zro)
  namelist_variable(pgen, REALNUM, perturb_d__phase, zro)
  namelist_variable(pgen, REALNUM, perturb_v1, zro)
  namelist_variable(pgen, REALNUM, perturb_v1_m, zro)
  namelist_variable(pgen, REALNUM, perturb_v1_phase, zro)
  namelist_variable(pgen, REALNUM, perturb_v2, zro)
  namelist_variable(pgen, REALNUM, perturb_v2_m, zro)
  namelist_variable(pgen, REALNUM, perturb_v2_phase, zro)
  namelist_variable(pgen, REALNUM, perturb_v3, zro)
  namelist_variable(pgen, REALNUM, perturb_v3_m, zro)
  namelist_variable(pgen, REALNUM, perturb_v3_phase, zro)
  namelist_variable(pgen, REALNUM, perturb_bp, zro)
  namelist_variable(pgen, REALNUM, perturb_bp_m, zro)
  namelist_variable(pgen, REALNUM, perturb_bp_phase, zro)
  namelist_variable(pgen, REALNUM, perturb_e_, zro)
  namelist_variable(pgen, REALNUM, perturb_e__m, zro)
  namelist_variable(pgen, REALNUM, perturb_e__phase, zro)

  namelist_variable(pgen, REALNUM, eta_inner, zro)
  namelist_variable(pgen, REALNUM, eta_outer, zro)
  namelist_variable(pgen, REALNUM, eta_transition, zro)
  namelist_variable(pgen, REALNUM, eta_width, zro)

  !Artificial envelope depletion
  namelist_variable(pgen, REALNUM, depletion_z, -one)
  namelist_variable(pgen, REALNUM, depletion_exponent, two)

!***************************************************************
!                   wind variables
!***************************************************************

  REALNUM wind_v0
!  namelist_variable(pgen, REALNUM, wind_v0, one)
  REALNUM wind_bphi_alfven_number_inverse
  REALNUM, PARAMETER :: ninety = 90.0_pk ! Right angle in degrees

  TYPE splinecontrol
    REALNUM transition, width, a_degree, b_degree, a, b
    ! a, b are overriden
    ! Other possible variables
    ! kind (cosine, cubic)
    ! exponent (steepen the spline by raising it to some power)
  END TYPE splinecontrol

  ! REMINDER: DO NOT CONTINUE LINES CONTAINING THE namelist_variable MACRO.
  !  IT IS JUST A MACRO AND IT DOES NOT KNOW Fortran.
  namelist_variable(pgen, type(splinecontrol), theta_max_wind, splinecontrol(90.0_pk, ten, -one, -one, -one, -one))

  !namelist_variable(pgen, REALNUM, bc_sign, -one)  ! Always overriden

  namelist_variable(pgen, CHARACTER(LEN=20), als_file_threecolumn, '')
  namelist_variable(pgen, CHARACTER(LEN=20), als_file_sixcolumn, '')
  namelist_variable(pgen, REALNUM, als_density_factor, one)
  namelist_variable(pgen, REALNUM, als_bp_factor, one)

  namelist_variable(pgen, REALNUM, toomre_n, zro)
  namelist_variable(pgen, REALNUM, toomre_rho0_equator, zro)
  namelist_variable(pgen, REALNUM, toomre_r0, zro)

  namelist_variable(pgen, REALNUM, spherical_n, two)
  namelist_variable(pgen, REALNUM, spherical_rho0, zro)
  namelist_variable(pgen, REALNUM, spherical_r0, zro)

  namelist_variable(pgen, REALNUM, centralmass_factor, one)
  namelist_variable(pgen, REALNUM, centralmass_addition, zro)

  namelist_variable(pgen, INTEGER, wind_initial_zones, 0) ! radial_n=2 in these zones
  namelist_variable(pgen, REALNUM, wind_radial_n_ghosts, zro)
  namelist_variable(pgen, REALNUM, wind_r0, zro) !Zero flags use of x1ap(isp)
  namelist_variable(pgen, REALNUM, wind_rho0, one)
  namelist_variable(pgen, REALNUM, wind_n, two)
  namelist_variable(pgen, REALNUM, wind_bphi_alfven_number, ten) ! Zero value is special

  namelist_variable(pgen, LOGICAL, wind_bphi_unchanging, .false.)

  namelist_variable(pgen, INTEGER, accretion_wedge_depth, 0)

  TYPE innercone
    REALNUM theta_degree ! Always an input value.  Negative values disable the feature.
    REALNUM theta ! Overriden by a calculated value
    REALNUM alpha, beta ! WIND(theta-)=alpha*WIND(theta+)+beta
    REALNUM n ! WIND(theta)=a*sin(theta)**n+b, where 'a' is always calculated
    REALNUM b ! Overriden by a calculated value if n=0
    REALNUM a ! Overriden by a calculated value
  END type innercone
  namelist_variable(pgen, type(innercone), windcone_rho, innercone(zro, zro, one, zro, zro, zro, zro))
  namelist_variable(pgen, type(innercone), windcone_bphi, innercone(zro, zro, one, zro, zro, zro, zro))

  namelist_variable(pgen, REALNUM, wind_v0_nominal, one)
  namelist_variable(pgen, CHARACTER(LEN=20), wave_type, 'constant')
  namelist_variable(pgen, REALNUM, wave_period, one)
  namelist_variable(pgen, REALNUM, wave_phase, zro)
  namelist_variable(pgen, REALNUM, wave_height, one)

  !namelist_variable(pgen, LOGICAL, bphiaxiszero, .false.)

  namelist_variable(pgen, CHARACTER(LEN=20), wind_direction_type, 'radial')
  REALNUM :: alpha_edge, delta_edge ! Possibly time dependent quantities
  namelist_variable(pgen, REALNUM, alpha_edge_initial, zro)
  namelist_variable(pgen, REALNUM, alpha_dot, zro)
  namelist_variable(pgen, REALNUM, alpha_edge_max, ninety)

END MODULE problem_variables
