#include "macros"
!** GROUP: MAIN PROGRAM ************************************************
!**                                                                   **
!**                   G R O U P :   M A I N  P R O G R A M            **
!**                                                                   **
!***********************************************************************
!
!=======================================================================
!
PROGRAM zeus
  USE zeus_variables
  USE zeus_boundary
  USE zeus_initial
  USE zeus_physics
  USE zeus_deadcode
  USE zeus_io
  USE zeus_timer
  USE zeus_iochecks
  USE zeus_grid_moving
  USE problem_final
  USE zeus_gravity

  USE problem_description

  IMPLICIT NONE
  CHARACTER(LEN=*), PARAMETER :: routinename = 'z_zeus'
  REALNUM :: dtqqi2

  !-----------------------------------------------------------------------
  !----------------------------  INITIALIZATION  -------------------------
  !-----------------------------------------------------------------------

  call mstart
  call writemessage(routinename//'_start', 1)
  call writedata(routinename//'_start_', 3)
  !
  !      Initial dumps
  !
  call dataoutput
  !
  call writemessage(routinename//'_dataoutput', 1)
  call writedata(routinename//'_dataoutput_', 3)
  !
  !-----------------------------------------------------------------------
  !--------------------------  START OF MAIN LOOP  -----------------------
  !-----------------------------------------------------------------------
  !
  !      Execution ends when IOCHECK returns a value of 1 for the terminate_flag.
  !
  call timer_loopstart
  call writemessage(routinename//'_timer_loopstart', 1)
  !
  mainloop: do
    if (terminate_flag == 1) exit mainloop

    call newgravity
    call writemessage(routinename//'_newgravity', 1)

    call source_terms
    call writemessage(routinename//'_source', 1)
    call writedata(routinename//'_source_', 3)

    !
    !  Update momenta and energy from the artificial viscosity source terms.
    !
    call artificial_viscosity(dtqqi2)
    call writedata(routinename//'_artificial_viscosity_', 3)

    if (ienergy == 1 .and. itote == 0) then
      !
      !  Update energy with pdv source term, for itote=0
      !  only (internal energy equation being solved).
      !
      call pdv
      call writedata(e_, routinename//'_e_pdv_', 3)
      call boundary('e_')
      call writedata(e_, routinename//'_e_boundary_', 3)
    end if

    if (imhd /= 0) then
      call magnetic_transport
      call writemessage(routinename//'_magnetic_t_', 1)
      call writedata(routinename//'_magnetic_t_', 3)
    end if

    call hydro_transport
    call writemessage(routinename//'_hydro_t_', 1)
    call writedata(routinename//'_hydro_t_', 3)

    ! Advance time
    call settimestep(ntimestep + 1)
    time = time + dt
    if (ntimestep == ntimestepdebug) debug = 4

    ! Update the BC information
    call problem_bc

    call new_dt(dtqqi2, .false.)
    call writemessage(routinename//'_new_dt_', 1)
    call writedata(routinename//'_new_dt_', 3)

    call movegrid
    call writemessage(routinename//'_movegrid_', 1)
    call writedata(routinename//'_movegrid_', 3)
    call writedata(x1a, routinename//'_movegrid_x1a_', 3)
    call writedata(x2a, routinename//'_movegrid_x2a_', 3)
    call writedata(x3a, routinename//'_movegrid_x3a_', 3)

    call dataoutput
    call writemessage(routinename//'_dataotput', 1)
    call writedata(routinename//'_dataoutput_', 3)

    call timer_step
  end do mainloop
  !
  !-----------------------------------------------------------------------
  !---------------------------  END OF MAIN LOOP  ------------------------
  !-----------------------------------------------------------------------
  !
  !      Perform problem-specified finish tasks.
  !      (For instance, close problem-defined output files, release locks,
  !      or start post-processing)
  !
  call problem_finish
  call writemessage(routinename//'_finish', 1)
  call writedata(routinename//'_finish_', 3)

  !
  !      Calculation finished; final housekeeping duties.
  !
  call timer_end

  !
  !      Close ascii files: action moved to the finalstop routine.
  !
  !      Do not rename these files, as that may require a non-Fortran standard
  !      function, for very little gain.
  !      This gain is also dubious, as killed or crashed runs will
  !      still have the old names for the ascii files
  !

  !
  ! Close other files if needed
  !
  call timeslice_end
  !
  !      THE END
  !
  call finalstop(routinename, "ZEUS-TW : THE END")
  !
END PROGRAM zeus
