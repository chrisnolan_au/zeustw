#include "macros"
MODULE zeus_boundary
  USE zeus_variables
  USE zeus_io
  USE zeus_mpi
  USE problem_boundary
  IMPLICIT NONE
  PRIVATE
  PUBLIC :: boundary

  INTERFACE boundary
    MODULE PROCEDURE boundary_one_field, boundary_three_fields
  END INTERFACE

CONTAINS

  !** GROUP: BOUNDARY ****************************************************
  !**                                                                   **
  !**                   G R O U P :   B O U N D A R Y                   **
  !**                                                                   **
  !***********************************************************************
  !
  !=======================================================================
  !
  SUBROUTINE boundary_one_field(fieldname)
    CHARACTER(LEN=*), INTENT(IN) :: fieldname
    CHARACTER(LEN=*), PARAMETER :: routinename = 'z_boundary'

    INTEGER face, side

    do face = 1, 3
      !        Check if grid is substantive
      if (iwnac(face) > 0) then
        !          Do both inner and outer boundary
        do side = 1, 2
          !    Check if node is the first or last:
          !    if that's the case, call physical boundary conditions
          if (l_firstnodew(face, side)) then
            select case (fieldname)
            case ('d_'); call boundary_d_(face, side)
            case ('dfl'); if (alfven_dt_floor_forcelimiter) &
 call boundary_dfl(face, side)
            case ('e_'); if (ienergy > 0) call boundary_e_(face, side)
            case ('etas'); call boundary_etas(face, side)
            case ('v1'); call boundary_v1(face, side)
            case ('v2'); if (withv2) call boundary_v2(face, side)
            case ('v3'); if (withv3) call boundary_v3(face, side)
            case ('b3'); if (withb3 .and. withb3bc) call boundary_b3(face, side)
            case ('tf'); if (number_of_tracer_fields > 0) &
 call boundary_tf(face, side)
            case default
              call finalstop(routinename, 'Unknown BC. Execution stops.')
            end select
          end if
        end do
      end if
    end do
    ! Do node boundaries
    select case (fieldname)
    case ('d_'); call mpifield(d_)
    case ('dfl'); if (alfven_dt_floor_forcelimiter) call mpifield(dfl)
    case ('e_'); if (ienergy > 0) call mpifield(e_)
    case ('etas')
      call mpifield(ohmic_eta_array)
      call mpifield(ad_eta_array)
      call mpifield(hall_eta_array)
      call mpifield(xr_array)
      call mpifield(cr_array)
    case ('v1'); call mpifield(v1)
    case ('v2'); if (withv2) call mpifield(v2)
    case ('v3'); if (withv3) call mpifield(v3)
    case ('b3'); if (withb3) call mpifield(b3) ! Notice: no withb3bc check
    case ('tf'); if (number_of_tracer_fields > 0) call mpifield(tracerfields)
    case default
      call finalstop(routinename, 'Unknown BC. Execution stops.')
    end select
    !
  END SUBROUTINE boundary_one_field
  !
  !=======================================================================
  !
  SUBROUTINE boundary_three_fields(fieldname, field1, field2, field3)
    REALNUM, DIMENSION(:, :, :), INTENT(INOUT) :: field1, field2, field3
    CHARACTER(LEN=*), INTENT(IN) :: fieldname

    INTEGER :: face, side
    CHARACTER(LEN=*), PARAMETER :: routinename = 'z_boundary_three_fields'

    !The fields involved are either the current components j1,j2,j3
    !or the emf fields emf1,emf2,emf3.
    !As these fields are not global variables, they must be passed
    !through the subroutine call.

    do face = 1, 3
      !        Check if grid is substantive
      if (iwnac(face) > 0) then
        !          Do both inner and outer boundary
        do side = 1, 2
          !    Check if node is the first or last:
          !    if that's the case, call physical boundary conditions
          if (l_firstnodew(face, side)) then
            select case (fieldname)
            case ('emf1')
              call boundary_emf1(face, side, field1, field2, field3)
            case ('emf2')
              call boundary_emf2(face, side, field1, field2, field3)
            case ('emf3')
              call boundary_emf3(face, side, field1, field2, field3)
            case ('emf')
              call boundary_emf(face, side, field1, field2, field3)
            case ('emf_dx')
              call boundary_emf_dx(face, side, field1, field2, field3)
            case ('currents')
              call boundary_currents(face, side, field1, field2, field3)
            case ('vi')
              call boundary_vi(face, side, field1, field2, field3)
            case ('vd')
              call boundary_vd(face, side, field1, field2, field3)
            case default
              call finalstop(routinename, 'Unknown BC. Execution stops.')
            end select
          end if
        end do
      end if
    end do
    ! Do node boundaries
    select case (fieldname)
    case ('emf1'); call mpifield(field1)
    case ('emf2'); call mpifield(field2)
    case ('emf3'); call mpifield(field3)
    case ('emf')
      call mpifield(field1); call mpifield(field2); call mpifield(field3)
    case ('emf_dx')
      call mpifield(field1); call mpifield(field2); call mpifield(field3)
    case ('currents')
      call mpifield(field1); call mpifield(field2); call mpifield(field3)
    case ('vi')
      call mpifield(field1); call mpifield(field2); call mpifield(field3)
    case ('vd')
      call mpifield(field1); call mpifield(field2); call mpifield(field3)
    case default
      call finalstop(routinename, 'Unknown BC. Execution stops.')
    end select
    !
  END SUBROUTINE boundary_three_fields

END MODULE zeus_boundary
