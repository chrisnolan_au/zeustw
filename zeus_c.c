/* These functions are the C part of the Zeus36 code.                      */
/* It contains I/O functions, earlier versions also contained sanity check */
/* functions                                                               */

/* I/O portability may require changing some datatypes to make sure that   */
/* Fortran and C agree regarding floating point and integer datatypes.     */
/* Consider using autoconfiguration tools for this in some future.         */
/* The correspondences depend on the compilers, the machine, environment   */
/* flags, and compilation options.                                         */

/* Check for portability the passing of character strings from Fortran to  */
/* and to the OS.  Look in particular for NUL-termination of strings and   */
/* the passing of length parameters.                                       */

/* This version uses write() system calls instead of standard C fwrite()   */


/* The failure codes reported must be positive (for failure) or zero (for success) */

#include <unistd.h>
#include <sys/types.h>
#include <sys/stat.h>
#include <fcntl.h>
#include <stdio.h>
#include <stdlib.h>
#include "zeus_c.h"

#define _GNU_SOURCE
#include <math.h>

void BINARYOUT(const int *size,const int *kind,const int *classtype,int *reportfailure,
	       const char *arr,
	       const char *filename,const size_t len)
{
  ssize_t bytecount ;
  int fd;
  unsigned long long totalsize;
  int objectsize;

  objectsize=*kind;
  totalsize=objectsize * (*size);
  if (totalsize<=0) return;

  /* Because some modes of usage may need allowing overwriting an existent file,
     and because O_EXCL may fail on some file systems, the O_EXCL flag is not turned on */
  /* fd = open(filename,O_WRONLY|O_CREAT|O_EXCL,0666); */

  fd = open(filename,O_WRONLY|O_CREAT|O_TRUNC,0666);
  if (fd == -1) {
    fprintf(stderr,"Error in opening %s for writing\n",filename);
    if (*reportfailure){
      *reportfailure=135;
      return;
    }else{
      exit(135);
    }
  }
  bytecount = write(fd,arr,totalsize);
  if (bytecount != totalsize) {
    fprintf(stderr,"Error in writing to file %s\n",filename);
    fprintf(stderr,"Total size in bytes: %llu\n",totalsize);
    fprintf(stderr,"Bytecount: %ld\n",bytecount);
    if (*reportfailure){
      *reportfailure=136;
      return;
    }else{
      exit(136);
    }
  } 
  // fsync(fd);
  if (close(fd)){
    fprintf(stderr,"Error in closing %s\n",filename);
    if (*reportfailure){
      *reportfailure=137;
      return;
    }else{
      exit(137);
    }
  }
  *reportfailure=0;
}

void BINARYIN (const int *size,const int *kind,const int *classtype,int *reportfailure,
	       char *arr,
	       const char *filename,const size_t len)
{
  ssize_t bytecount ;
  int fd;
  unsigned long long totalsize;
  int objectsize;

  objectsize=*kind;
  totalsize=objectsize * (*size);
  if (totalsize<=0) return;
  //if (totalsize>SSIZE_MAX){
  //  fprintf(stderr,"Trying to read too much from %s\n",filename);exit(144);
  //}

  fd = open(filename,O_RDONLY);
  if (fd == -1) {
    fprintf(stderr,"Error in opening %s for reading\n",filename);
    if (*reportfailure){
      *reportfailure=145;
      return;
    }else{
      exit(145);
    }
  }
  bytecount = read(fd,arr,totalsize);
  if (bytecount != totalsize) {
    fprintf(stderr,"Error in reading from file %s\n",filename);
    fprintf(stderr,"Total size in bytes: %llu\n",totalsize);
    fprintf(stderr,"Bytecount: %ld\n",bytecount);
    if (*reportfailure){
      *reportfailure=146;
      return;
    }else{
      exit(146);
    }
  } 
  if (close(fd)){
    fprintf(stderr,"Error in closing %s\n",filename);
    if (*reportfailure){
      *reportfailure=147;
      return;
    }else{
      exit(147);
    }
  }
  *reportfailure=0;
}
