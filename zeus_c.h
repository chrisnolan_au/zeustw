/* These macros may depend on the specific way the Fortran and C compilers  */
/* perform name-mangling by appending or not a trailing underscore. */

#ifdef NOUNDERSCORE
#define   BINARYIN    binaryin
#define   BINARYOUT   binaryout
#else
#define   BINARYIN    binaryin_
#define   BINARYOUT   binaryout_
#endif

/* The datatypes used here depend on the machine and on the particular */
/* pair of Fortran and C compiler versions used.                       */
/* Expect having to change them once in a while.                       */

/* Intel Fortran 9.1 */
/* Default integer kind: 4, corresponding to Intel C int, long */
/* All Fortran INTEGER and REAL types have a size in bytes equal */
/* to their KIND. */
/* So for this Fortran compiler, the classtype information is not needed */
/* to calculate sizes */

void BINARYOUT(const int *size,const int *kind,const int *classtype,int *reportfailure,
	       const char *arr,
	       const char *filename,const size_t len);
void BINARYIN (const int *size,const int *kind,const int *classtype,int *reportfailure,
	       char *arr,
	       const char *filename,const size_t len);

/* Other Fortran to C 9.1 data equivalences, */
/* according to the Building Applications part of the manual */
/* (which is wrong) */

/* INTEGER(1)   -> char      */
/* INTEGER(2)   -> short     */
/* INTEGER(4)   -> int, long */
/* INTEGER(8)   -> _int64 (sic)   */
/* REAL(4)      -> float     */
/* REAL(8)      -> double    */
/* REAL(16)     -> N/A       */
/* CHARACTER(1) -> unsigned char */
/* Strings: by default, Intel Fortran passes a hidden length argument */
/* This argument is an unsigned 4-byte integer in IA-32 systems       */
/* and an unsigned 8-byte integer in Intel EM64T and Itanium systems  */
/* There are ways to avoid sending this hidden argument.              */
/* At least for version 9.0 of these compilers it is possible to use  */
/* size_t, and unsigned long, and to match both IA-32 and EM64T       */
