#include "macros"
MODULE zeus_chemistry
  USE zeus_io
  USE zeus_variables
  IMPLICIT NONE
  REALNUM, PARAMETER :: &
    mp = 1.6726219e-24_pk, &
    me = 9.10938356e-28_pk, &
    c_0 = 2.99792458e10_pk, &
    k_b = 1.3806485e-16_pk, &
    e_0 = 4.8032068e-10_pk, &
    miu = 2.36, & ! Mean molecular weight
    q_g = 0.01, & ! Total grain mass as a percent of gas mass
    rho_g = 2.3, & ! Grain density (g cm^-3)
    indx = 3.5 ! MRN distribution index
  ! CHARACTER(16), PARAMETER :: intype = 'log'
  TYPE SPECIE
    CHARACTER(LEN=16) :: n
    REALNUM :: w, x
  END TYPE SPECIE

  PRIVATE
  PUBLIC :: minimal_network, full_network

CONTAINS
  !
  !=======================================================================
  !
  INTEGER FUNCTION full_network(rho, B, T, zeta_H2, a_min, a_max, eta_ad, &
                                eta_ohm, eta_hal, check)
    !
    !  PURPOSE: Calculates the non-ideal mhd coeffs using a full chemical network
    !
    !  INPUT VARIABLES:
    !    rho      = H2 number density (cm^-3)
    !    B        = Magnetic field strength (G)
    !    T        = Temperature (K)
    !    zeta_H2  = Ionisation rate for H2 molecules
    !    a_min    = Minimum grain radius
    !    a_min    = Maximum grain radius
    !
    !  OUTPUT VARIABLES:
    !    eta_ad   = Ambipolar diffusion term
    !    eta_ohm  = Ohmic diffusion term
    !    eta_hal  = Hall diffusion term
    !
    !    Full Chemical Network
    !    Purpose:
    !    Variables: 1D rho-interpolation with EOS; 3D (rho, T, zeta)
    !
    !----------------------------------------------------------------------
    !
    TYPE(SPECIE), DIMENSION(numIons) :: ions
    REALNUM, INTENT(IN) :: rho, B, T, zeta_H2, a_min, a_max
    REALNUM, INTENT(OUT) :: eta_ad, eta_ohm, eta_hal
    INTEGER, OPTIONAL, INTENT(IN) :: check
    REALNUM, DIMENSION(gbins) :: ra, ma, xa
    REALNUM, DIMENSION(-1:1, gbins) :: xgrs
    REALNUM, DIMENSION(numIons + gbins*2 + 3) :: abund_interp
    REALNUM, DIMENSION(numIons + gbins*2 + 3, 8) :: abunds
    INTEGER, DIMENSION(8) :: idx
    REALNUM :: limits, limitf, nh, dens, T0, rhoi, tempi, C0, da, mw, &
               d_d, d_t, d_z
    INTEGER :: i, j, k, nEntri, loci_d, loci_t, loci_z, &
               i_d, j_d, i_t, j_t, i_z, j_z, i0
    CHARACTER(32) :: chararr(100), strnum1, strnum2
    CHARACTER(100) :: infile
    CHARACTER(3000) :: linestr1, linestr2
    CHARACTER(LEN=16), DIMENSION(numIons) :: ions_n
    REALNUM, DIMENSION(numIons) :: ions_w
    LOGICAL :: file_exists
    ! Added variables for interpolation over diffusivities
    REALNUM, DIMENSION(8) :: eta_ohm_in, eta_hal_in, eta_ad_in
    TYPE(SPECIE), DIMENSION(8, numIons) :: ions_in
    REALNUM, DIMENSION(8, -1:1, gbins) :: xgrs_in
    CHARACTER(LEN=*), PARAMETER :: routinename = 'z_full_network'

    full_network = 1
    nEntri = numIons + gbins*2 + 8

    ! Specify the electron & ion names and weights
    ions_n = (/'e-   ', 'H+   ', 'H2+  ', 'H3+  ', 'He+  ', 'HeH+ ', 'O+   ', &
               'N+   ', 'CO+  ', 'C+   ', 'O2+  ', 'O2H+ ', 'OH+  ', 'H2O+ ', &
               'HCO+ ', 'N2+  ', 'NH+  ', 'HN2+ ', 'H3O+ ', 'NO+  ', 'CH+  ', &
               'Mg+  ', 'Fe+  ', 'CH2+ ', 'CH3+ ', 'CH4+ ', 'CH5+ ', 'NH2+ ', &
               'NH3+ ', 'NH4+ ', 'CO2+ ', 'HCO2+'/)
    ions_w = (/1./1836.15267, 1.0, 2.0, 3.0, 4.0, 5.0, 16.0, 14.0, 28.0, 12.0, &
               32.0, 33.0, 17.0, 18.0, 29.0, 28.0, 15.0, 29.0, 19.0, 30.0, &
               13.0, 24.3, 55.8, 14.0, 15.0, 16.0, 17.0, 16.0, 17.0, 18.0, &
               44.0, 45.0/)

    ! Calculating the grain size distribution (see ZhaoEtAl2016, sec 2.2)
    mw = 29.*mp/me
    C0 = 3.*q_g*mp/2./Pi/rho_g*(4.-indx)/(a_max**(4.-indx) - a_min**(4.-indx))
    da = 10**((log10(a_max) - log10(a_min))/gbins)
    i0 = 1
    if (PAH > 0.) then
       i0 = 2
       da = 10**((log10(a_max) - log10(a_min))/(gbins-1))
       ra(1) = 5e-8
       ma(1) = rho_g*4.*Pi/3.*(ra(1))**3
       xa(1) = PAH
    end if
    do k = i0, gbins
      if (k == i0) then
        ra(k) = a_min*sqrt(da)
      else
        ra(k) = ra(k - 1)*da
      end if
      ! Calculating grain mass for each bin
      ma(k) = rho_g*4.*Pi/3.*(ra(k))**3
      ! Calculating initial grain abundances for each bin
      xa(k) = C0/(indx - 1.) &
              *((ra(k)/sqrt(da))**(1.-indx) - (ra(k)*sqrt(da))**(1.-indx))
    end do

    ! Importing abundances from external tables

    ! Calculate the bracketing indices for the density using bisection
    loci_d = 0
    i_d = 1
    j_d = nbins
    do while (i_d < j_d - 1)
      loci_d = (i_d + j_d)/2
      if (rho < allRec(1, loci_d)) then
        j_d = loci_d
      else
        i_d = loci_d
      end if
    end do
    d_d = (log10(rho) - log10(allRec(1, i_d))) &
          / (log10(allRec(1, j_d)) - log10(allRec(1, i_d)))
    ! Calculate the bracketing indices for the temperature using bisection
    loci_t = 0
    i_t = 0
    j_t = tbins-1
    do while (i_t < j_t - 1)
      loci_t = (i_t + j_t)/2
      if (T < allRec(2, loci_t*nbins+1)) then
        j_t = loci_t
      else
        i_t = loci_t
      end if
    end do
    d_t = (log10(T) - log10(allRec(2, i_t*nbins + 1))) &
          / (log10(allRec(2, j_t*nbins + 1)) - log10(allRec(2, i_t*nbins + 1)))
    ! Calculate the bracketing indices for the ionisation rate using bisection
    loci_z = 0
    i_z = 0
    j_z = ibins-1
    do while (i_z < j_z - 1)
       loci_z = (i_z + j_z)/2
       if (zeta_H2 < allRec(3, loci_z*nbins*tbins+1)) then
          j_z = loci_z
       else
          i_z = loci_z
       end if
    end do
    d_z = (log10(zeta_H2) - log10(allRec(3, i_z*nbins*tbins + 1))) &
          / (log10(allRec(3, j_z*nbins*tbins + 1)) - log10(allRec(3, i_z*nbins*tbins + 1)))

    ! Calculate the table indices for the eight corner points on the
    ! interpolation cube
    idx(1) = (i_z*tbins + i_t)*nbins + i_d; idx(2) = (i_z*tbins + i_t)*nbins + j_d
    idx(3) = (j_z*tbins + i_t)*nbins + i_d; idx(4) = (j_z*tbins + i_t)*nbins + j_d
    idx(5) = (i_z*tbins + j_t)*nbins + i_d; idx(6) = (i_z*tbins + j_t)*nbins + j_d
    idx(7) = (j_z*tbins + j_t)*nbins + i_d; idx(8) = (j_z*tbins + j_t)*nbins + j_d

    if (interpchem == 0) then ! Interpolate over abundances
      ! Specify the electron & ion names and weights
      ions%n = ions_n
      ions%w = ions_w

      do k = 1, gbins
        ! Calculating initial grain abundances for each bin
        xgrs(0, k) = xa(k)/(1.+(1.+mw)/sqrt(mw)/(1.+e_0*e_0/ra(k)/k_b/T))
        xgrs(-1, k) = xgrs(0, k)*sqrt(mw)/(1.+e_0*e_0/ra(k)/k_b/T)
        xgrs(1, k) = xgrs(-1, k)/mw
      end do

      ! Pulls out the abundances of all species + rho/temp/zeta for each of the
      ! eight corner points
      do i = 1, 8
        abunds(:,i) = log10(allRec(1:numIons+gbins*2+3, idx(i)))
      end do
      ! Interpolates the abundances for the current cell using trilinear
      ! interpolation
      abund_interp = zro
      abund_interp(1) = rho
      abund_interp(2) = T
      abund_interp(3) = zeta_H2
      call interpol3D(abunds, abund_interp, d_d, d_t, d_z, nEntri-5)

      ! Adding ion abundances
      do i = 1, numIons
        ions(i)%x = 10**(abund_interp(3 + i))
      end do

      ! Adding ionised grain abundances
      do k = 1, gbins
        xgrs(-1, k) = 10**(abund_interp(3 + numIons + k))
        xgrs(1, k) = 10**(abund_interp(3 + numIons + gbins + k))
      end do

      ! Calculate the non-ideal MHD coefficients from the abundances and
      ! ionisations of the ions and grains
      call mhd_coef(rho, T, B, ions, ra, ma, xgrs, eta_ad, eta_ohm, eta_hal)
    else if (interpchem == 1) then ! Interpolate over diffusivities
      ! Specify the electron & ion names and weights
      do i = 1, 8
        ions_in(i,:)%n = ions_n
        ions_in(i,:)%w = ions_w
      end do

      do i = 1, 8
        do k = 1, gbins
          ! Calculating initial grain abundances for each bin
          xgrs_in(i, 0, k) = xa(k)/(1.+(1.+mw)/sqrt(mw)/(1.+e_0*e_0/ra(k)/k_b/T))
          xgrs_in(i, -1, k) = xgrs_in(i, 0, k)*sqrt(mw)/(1.+e_0*e_0/ra(k)/k_b/T)
          xgrs_in(i, 1, k) = xgrs_in(i, -1, k)/mw
        end do
      end do

      ! Adding ion abundances
      do i = 1, 8
        do k = 1, numIons
          ions_in(i, k)%x = allRec(3 + k, idx(i))
        end do
      end do

      ! Adding ionised grain abundances
      do i = 1, 8
        do k = 1, gbins
          xgrs_in(i, -1, k) = allRec(3 + numIons + k, idx(i))
          xgrs_in(i, 1, k) = allRec(3 + numIons + gbins + k, idx(i))
        end do
      end do

      ! Calculate diffusivities at all eight corner points on the interpolation cube
      do i = 1, 8
        call mhd_coef(allRec(1, idx(i)), allRec(2, idx(i)), B, ions_in(i, :), &
                      ra(:), ma(:), xgrs_in(i, :, :), eta_ad_in(i), &
                      eta_ohm_in(i), eta_hal_in(i))
      end do

      ! Interpolate the diffusivities
      call interpol3D_norm(eta_ohm_in, eta_ohm, d_d, d_t, d_z, 1)
      call interpol3D_norm(eta_hal_in, eta_hal, d_d, d_t, d_z, 1)
      call interpol3D_norm(eta_ad_in, eta_ad, d_d, d_t, d_z, 1)
    else
      call finalstop(routinename, 'Error. InterpChem parameter must be either 0 or 1.')
    end if

    !---------------------------------------------------------------------------

    if (present(check)) then
      write(*,'(a20)') "Within FULL_NETWORK:"
      write(*,'(a15,100ES16.8)') ' Abundances:   ', abund_interp(:)
      write(*,'(a15,100ES16.8)') ' Grain Radii:  ', ra(:)
      write(*,'(a15,100ES16.8)') ' Grain mass:   ', ma(:)
      write(*,'(a20,i5,a5,i5,a5,ES16.8)') ' Density bisec: i_d=', i_d, ' j_d=', j_d, ' d_d=', d_d
      write(*,'(a20,i5,a5,i5,a5,ES16.8)') ' Temp bisec:    i_t=', i_t, ' j_t=', j_t, ' d_t=', d_t
      write(*,'(a20,i5,a5,i5,a5,ES16.8)') ' Ionise bisec:  i_z=', i_z, ' j_z=', j_z, ' d_z=', d_z
      write(*,'(3(a10,ES16.8))') "Eta_Ohm=", eta_ohm, "Eta_AD=", eta_ad, "Eta_Hall=", eta_hal
    end if
  END FUNCTION full_network
  !
  !=======================================================================
  !
  SUBROUTINE mhd_coef(rho, Tk, Bg, ions, ra, ma, xgrs, eta_ad, eta_ohm, eta_hal)
    !
    !  PURPOSE: Compute Non-ideal MHD Coefficients (full_network)
    !
    !  INPUT VARIABLES:
    !    rho      = H2 number density (cm^-3)
    !    Tk       = Temperature (K)
    !    Bg       = Magnetic field strength (G)
    !    ions     = SPECIE structure including ion names, weights and abundances
    !    ra       = Grain radii per bin (array)
    !    ma       = Grain mass per bin (array)
    !    xgrs     = Grain abundances per bin (2D array, includes ions)
    !
    !  OUTPUT VARIABLES:
    !    eta_ad   = Ambipolar diffusion term
    !    eta_ohm  = Ohmic diffusion term
    !    eta_hal  = Hall diffusion term
    !
    !----------------------------------------------------------------------
    !
    TYPE(SPECIE), DIMENSION(numIons), INTENT(IN) :: ions
    REALNUM, INTENT(IN) :: rho, Tk, Bg
    REALNUM, INTENT(OUT) :: eta_ad, eta_ohm, eta_hal
    REALNUM, DIMENSION(gbins), INTENT(IN) :: ra, ma
    REALNUM, DIMENSION(-1:1, gbins), INTENT(IN) :: xgrs
    REALNUM :: mtr_iH2(numIons), beta_iH2(numIons)
    REALNUM :: mtr_gH2(gbins), beta_gH2(gbins)
    REALNUM :: thT, sgm_para, sgm_P, sgm_H
    INTEGER :: i, j, k
    CHARACTER(LEN=16) :: sName
    ! Momentum transfer rate coefficients and Hall parameters for ions and grains
    thT = log10(Tk)
    do i = 1, numIons
      sName = ions(i)%n
      select case (sName)
      ! Momentum Transfer Rates: Pinto & Galli 2008, Table 2
      case ('e-')
        mtr_iH2(i) = sqrt(Tk)*(0.535 + 0.203*thT - 0.163*thT**2 + 0.050*thT**3)
      case ('H+')
        mtr_iH2(i) = 1.003 + 0.050*thT + 0.136*thT**2 - 0.014*thT**3
      case ('H3+')
        mtr_iH2(i) = 2.693 - 1.238*thT + 0.664*thT**2 - 0.089*thT**3
      case ('HCO+')
        mtr_iH2(i) = sqrt(Tk)*(1.476 - 1.409*thT + 0.555*thT**2 - 0.0775*thT**3)
      CASE ('C+')
        mtr_iH2(i) = 1.983*0.425*thT - 0.431*thT**2 + 0.114*thT**3
      CASE DEFAULT
        mtr_iH2(i) = 2.81*sqrt(0.804/ions(i)%w)*4. ! Eqn. A.5
      end select
    end do
    mtr_iH2 = mtr_iH2*1.D-9
    beta_iH2 = (e_0*Bg/ions(1:numIons)%w/mp/c_0)*(ions(1:numIons)%w + 2.) &
               /(miu*rho*mtr_iH2)
    do k = 1, gbins
      mtr_gH2(k) = 4.*Pi/3.*ra(k)*ra(k)*1.3*sqrt(4.*k_b*Tk/Pi/mp) ! Eqn 25.
      beta_gH2(k) = (e_0*Bg/ma(k)/c_0)*(ma(k) + 2.*mp)/(miu*mp*rho*mtr_gH2(k))
    end do
    ! Conductivities - Ion sums
    sgm_para = sum(ions(1:numIons)%x*beta_iH2)
    sgm_P = sum(ions(1:numIons)%x*beta_iH2(1:numIons) &
                /(1 + beta_iH2(1:numIons)*beta_iH2(1:numIons)))
    sgm_H = sum(ions(2:numIons)%x &
                /(1 + beta_iH2(2:numIons)*beta_iH2(2:numIons))) &
            - ions(1)%x/(1 + beta_iH2(1)*beta_iH2(1))
    ! Conductivities - Full prescription including grains
    sgm_para = e_0*c_0*rho/Bg*(sgm_para + sum(xgrs(-1, :)*beta_gH2) + &
                               sum(xgrs(1, :)*beta_gH2))
    sgm_P = e_0*c_0*rho/Bg*(sgm_P + &
                            sum(xgrs(-1, :)*beta_gH2 &
                                /(1 + beta_gH2*beta_gH2)) + &
                            sum(xgrs(1, :)*beta_gH2/(1 + beta_gH2*beta_gH2)))
    sgm_H = e_0*c_0*rho/Bg*(sgm_H - sum(xgrs(-1, :)/(1 + beta_gH2*beta_gH2)) + &
                            sum(xgrs(1, :)/(1 + beta_gH2*beta_gH2)))
    ! Diffusivities
    eta_ad = c_0*c_0/4./Pi*(sgm_P/(sgm_P*sgm_P + sgm_H*sgm_H) - 1./sgm_para)
    eta_hal = c_0*c_0/4./Pi*(sgm_H/(sgm_P*sgm_P + sgm_H*sgm_H))
    eta_ohm = c_0*c_0/4./Pi/sgm_para
    !
  END SUBROUTINE mhd_coef
  !
  !=======================================================================
  !
  SUBROUTINE tokenize(str, delim, arr, nlen, kth)
    !
    !    Parse String Into Multiple Parts
    !
    !----------------------------------------------------------------------
    !
    CHARACTER, INTENT(IN) :: delim
    CHARACTER(*), INTENT(IN) :: str
    INTEGER, INTENT(IN) :: nlen
    CHARACTER(*), INTENT(INOUT) :: arr(nlen)
    INTEGER, INTENT(OUT) :: kth
    INTEGER :: pos1, pos2
    !
    pos1 = 1; pos2 = 0; kth = 0; arr = ''
    do
      kth = kth + 1
      pos2 = index(str(pos1:), delim)
      !DO WHILE (ichar(str(pos1 + pos2:pos1 + pos2)) == 32)
      !   pos2 = pos2 + 1
      !END DO
      if (pos2 == 0 .or. kth >= nlen) then
        arr(kth) = trim(adjustl(str(pos1:)))
        exit
      end if
      arr(kth) = trim(adjustl(str(pos1:pos1 + pos2 - 2)))
      if (arr(kth) == "") kth = kth - 1
      pos1 = pos1 + pos2
      if (pos1 > len(str)) exit
    end do
  END SUBROUTINE tokenize
  !
  !=======================================================================
  !
  SUBROUTINE interpol3D(V, V_interp, xd, yd, zd, nSpecs)
    !
    !    Trilinear Interpolation
    !
    !----------------------------------------------------------------------
    !
    REALNUM, DIMENSION(:,:), INTENT(IN) :: V
    REALNUM, DIMENSION(:), INTENT(INOUT) :: V_interp
    REALNUM, INTENT(IN) :: xd, yd, zd
    INTEGER, INTENT(IN) :: nSpecs
    REALNUM :: C00, C01, C10, C11, C0, C1
    INTEGER :: i
    !
    do i = 4, nSpecs
       C00 = V(i,1)*(1-xd) + V(i,2)*xd
       C01 = V(i,3)*(1-xd) + V(i,4)*xd
       C10 = V(i,5)*(1-xd) + V(i,6)*xd
       C11 = V(i,7)*(1-xd) + V(i,8)*xd
       C0 = C00*(1-yd) + C10*yd
       C1 = C01*(1-yd) + C11*yd
       V_interp(i) = C0*(1-zd) + C1*zd
    end do
  END SUBROUTINE interpol3D
  !
  !=======================================================================
  !
  SUBROUTINE interpol3D_norm(V, V_interp, xd, yd, zd, nSpecs)
    !
    !    Trilinear Interpolation
    !
    !----------------------------------------------------------------------
    !
    REALNUM, DIMENSION(:), INTENT(IN) :: V
    REALNUM, INTENT(INOUT) :: V_interp
    REALNUM, INTENT(IN) :: xd, yd, zd
    INTEGER, INTENT(IN) :: nSpecs
    REALNUM :: C00, C01, C10, C11, C0, C1
    !
     C00 = V(1)*(1-xd) + V(2)*xd
     C01 = V(3)*(1-xd) + V(4)*xd
     C10 = V(5)*(1-xd) + V(6)*xd
     C11 = V(7)*(1-xd) + V(8)*xd
     C0 = C00*(1-yd) + C10*yd
     C1 = C01*(1-yd) + C11*yd
     V_interp = C0*(1-zd) + C1*zd
  END SUBROUTINE interpol3D_norm
  !
  !
  !=======================================================================
  !
  INTEGER FUNCTION minimal_network(rho, B, T, zeta_H2, a_min, a_max, eta_ad, &
                                   eta_ohm, eta_hal)!, xHCO)
    !
    !    Minimal Chemical Network
    !    Purpose: Good For Collapse Problem. For PPDs, Needs More Work or Avoid Using
    !    Variables:
    !
    !----------------------------------------------------------------------
    !
    REALNUM, INTENT(IN) :: rho, B, T, zeta_H2, a_min, a_max
    REALNUM, INTENT(OUT) :: eta_ad, eta_ohm, eta_hal!, xHCO
    REALNUM, PARAMETER :: xmol = 6.0e-4, xMet = 4.0e-8, epsilon = 0.05, &
                          beta = 2.5e-9
    REALNUM :: Acoef, qmol, qMet, tmol, tMet, rcoef, scoef, bcoef, pcoef, &
               hcoef, alpha_gr, psi, Temp, amin, amax, da, acb, C0, a0, a_0, &
               xg0, x_e, x_g, xgch, xgnt, xgm, xgp, G_g, u_g, y_g, z_g, ccoef, &
               chnet, chrat, pchs, xgmed, xgleft, xgright, delg, g1, g2, grat, &
               mw, gauge, gaugeT, xgp0, avg
    REALNUM, DIMENSION(3) :: rootcube = (/0., 0., 0./)
    REALNUM, DIMENSION(8) :: x_i
    REALNUM, DIMENSION(gbins) :: ra, ma, xa
    REALNUM, DIMENSION(-1:1, gbins) :: xgrs
    REALNUM :: Coefa, Coefb, Coefc, Coefd, &
               Coefga, Coefgb, Coefgc, Coefgd, alpha_dr, alpha_rec
    INTEGER :: i, j, k3, l, ln, m3, m3n, sigg, count, i0

    minimal_network = 1!; xHCO= 0.
    alpha_dr = 2.0e-7*(300/T)**0.75; alpha_rec = 2.8e-12*(300/T)**0.86
    amin = a_min/1.0e-8 ! UNITS Of Amstron
    amax = a_max/1.0e-8 ! UNITS Of Amstron
    Temp = T/10.0
    psi = -3.8

    C0 = 3.*q_g*mp/2./Pi/rho_g*(4.-indx)/(a_max**(4.-indx) - a_min**(4.-indx))
    a0 = 8.*e_0*e_0*me/(Pi*k_b*T*25*mp)/1.e-8
    a_0 = a0*1.e-8
    mw = 25.*mp/me
    ! GRAIN DISTRIBUTION
    da = 10**((log10(a_max) - log10(a_min))/gbins)
    i0 = 1
    if (PAH > 0.) then
       i0 = 2
       da = 10**((log10(a_max) - log10(a_min))/(gbins-1))
       ra(1) = 5e-8
       ma(1) = rho_g*4.*Pi/3.*(ra(1))**3
       xa(1) = PAH
    end if
    do i = i0, gbins
      if (i == i0) then
        ra(i) = a_min*sqrt(da)
      else
        ra(i) = ra(i - 1)*da
      end if
      ma(i) = rho_g*4.*Pi/3.*(ra(i))**3
      xa(i) = C0/(indx - 1.) &
              *((ra(i)/sqrt(da))**(1.-indx) - (ra(i)*sqrt(da))**(1.-indx))
    end do
    do i = 1, gbins
      xgrs(0, i) = xa(i)/(1.+(1.+mw)/sqrt(mw)/(1.+e_0*e_0/ra(i)/k_b/T))
      xgrs(-1, i) = xgrs(0, i)*sqrt(mw)/(1.+e_0*e_0/ra(i)/k_b/T)
      xgrs(1, i) = xgrs(-1, i)/mw
    end do
    xg0 = sum(xa)
    xgnt = sum(xgrs(0, :))
    xgm = sum(xgrs(-1, :))
    xgp0 = sum(xgrs(1, :))
    !
    ! Draine & Sutin 1987 - Normalized
    alpha_gr = 5.*e_0*e_0*sqrt(8.*Pi/(25.*mp*k_b*T)) &
               *(a_0**(-1.5)*(log(amax/amin) - 2. &
                              *log((sqrt(amax) + sqrt(a0)) &
                                   /(sqrt(amin) + sqrt(a0))) &
                              + (a0/amin - a0/amax) &
                              - 2.*(sqrt(a0/amin) - sqrt(a0/amax))) + &
                 k_b*T/e_0/e_0*(1 - psi)*(1./sqrt(amin) - 1./sqrt(amax))) &
               /(a_min**(-2.5) - a_max**(-2.5))
    ! Grain- only
    alpha_gr = 5./6.*sqrt(8.*Pi*k_b*T/25/mp)/(a_min**(-2.5) - a_max**(-2.5))*( &
               f_rec(e_0*e_0/k_b/T, a_min) - f_rec(e_0*e_0/k_b/T, a_max) + &
               6.*(1 - psi)*(1./sqrt(a_min) - 1./sqrt(a_max)))
    ! Galli's formula
    !alpha_gr= 1.604534796e-7*amin/sqrt(Temp)*(1+3.590664213e-4*amin*Temp*(1-psi)/4)
    avg = 5./3.*1.e-8*(amin**(-1.5) - amax**(-1.5)) &
          /(amin**(-2.5) - amax**(-2.5))
    acb = 5.e-24*(amax**(0.5) - amin**(0.5))/(amin**(-2.5) - amax**(-2.5))
    Acoef = zeta_H2/rho/alpha_dr
    qmol = beta*xmol/alpha_dr
    qMet = beta*xMet/alpha_dr
    rcoef = alpha_gr/alpha_dr
    scoef = alpha_rec/alpha_dr
    bcoef = 0.6*sqrt(25*mp/me)*rcoef
    pcoef = 2.5*sqrt(8.*Pi*k_b*T/25/mp)/(a_min**(-2.5) - a_max**(-2.5))*( &
            sqrt(0.5*Pi*e_0*e_0/k_b/T)*(1./a_min - 1./a_max) + &
            2.*(1./sqrt(a_min) - 1./sqrt(a_max)))
    pcoef = pcoef/alpha_dr
    hcoef = 0.6*sqrt(25*mp/me)*pcoef
    ccoef = 4.0*sqrt(25*mp/(8.0*Pi/3.0*rho_g*avg**3))/2.0*rcoef
    !xg0= 0.15972374*q_g/rho_g/(sqrt(amax)-sqrt(amin))*(amin**(-2.5)-amax**(-2.5))
    !xg0= 3.e-25/2.5*(a_min**(-2.5)-a_max**(-2.5))
    grat = 2.5*(1.+(25.*mp/me))/sqrt(25.*mp/me) &
           /(a_min**(-2.5) - a_max**(-2.5))* &
           (f_rat(e_0*e_0/k_b/T, a_max) - f_rat(e_0*e_0/k_b/T, a_min))
    !xgnt= xg0/(1.+grat)
    !xgch= xg0-xgnt
    xgch = xgm + xgp0
    z_g = sqrt(xgch*xgch - 4.*pcoef*hcoef/bcoef/rcoef*xgnt*xgnt)
    !xgm= 0.5*(xgch+z_g)
    xgp = 0.5*(xgch - z_g)
    !
    ! Solve X(e)
    z_g = xgm - xgp
    G_g = rcoef*xgm + pcoef*xgnt
    tmol = qmol + G_g
    tMet = qMet + G_g
    y_g = z_g - epsilon*Acoef/tmol
    Coefa = scoef
    Coefb = scoef*(y_g + tMet) + G_g
    Coefc = y_g*(scoef*tMet + G_g) + tMet*G_g - (1 - epsilon)*Acoef*scoef
    Coefd = y_g*tMet*G_g - (1 - epsilon)*Acoef*tMet
    rootcube = (/0., 0., 0./)
    k3 = solve_cubic(Coefa, Coefb, Coefc, Coefd, rootcube)
    !
    select case (k3)
    case (0)
      print *, 'x(e): No Root Found!'
      minimal_network = 0
      return
    case (1)
      if (abs(rootcube(1)) >= 0.1) then
        print *, 'x(e): No Root (<0.1) Found!'
        minimal_network = 0
        return
      else
        x_e = rootcube(1)
        !print *, 'x(e)=', x_e
      end if
    case (2:)
      m3 = 0; l = 0; m3n = 0; ln = 0
      do j = 1, k3
        if (abs(rootcube(j)) < 0.1) then
          if (rootcube(j) > verysmall) then
            m3 = m3 + 1
            l = j
          else if (rootcube(j) < 0.) then
            m3n = m3n + 1
            ln = j
          end if
        end if
      end do
      if (m3 >= 1) then
        x_e = rootcube(l)
        !print *, 'Choose', l, '-th Among', m3, '+Cubic Root: x(e)=', x_e
        if (m3 > 1) then
          print *, 'CUBIC warning: ', m3, ' Suitable Roots!'
          print *, 'x(e) Roots: ', rootcube
        end if
      else if (m3n >= 1) then
        x_e = rootcube(1)
        !print *, 'x(e): All-Negative CRoots!'
      end if
    end select
    !
    x_i = 0.0
    if (x_e > 0.0) then ! Calculate Abundances
      x_i(1) = x_e
      x_i(2) = xgnt
      x_i(3) = xgm
      x_i(4) = xgp
      x_i(5) = epsilon*Acoef/tmol
      x_i(6) = (1.-epsilon)*Acoef/(tmol + x_e)
      x_i(7) = (1.-epsilon)*Acoef*qmol/(tMet + x_e)/(tmol + x_e)
      x_i(8) = (1.-epsilon)*Acoef*qMet/(scoef*x_e + G_g)/(tMet + x_e)
      chnet = (x_e + xgm - xgp - x_i(5) - x_i(6) - x_i(7) - x_i(8))/x_e
    end if
    if ((x_e < 0.0) .or. (abs(chnet) > 1.e-3 + T/2.e4)) then
      if (amin < 925./T) gauge = (4.-0.02*amin)
      if ((amin >= 925./T) .and. amin <= 7906./T) gauge = 7.9 - 0.92*log(amin)
      if (amin > 7906./T) gauge = 5.3 - 0.47*log(amin)
      if (T <= 5860./amin) gaugeT = max((1.1 - 0.01*T), 0.6)
      if (T > 5860./amin) &
        gaugeT = (1/T)**(0.08 + T/5.e4)*min(1., 2.5/amin**(0.2))
      x_e = (zeta_H2/rho)*1.e11*amin*gauge*gaugeT
      !x_e= (zeta_H2/rho)*3.14e11*amin*max(2.03-0.2167*log(amin),0.40)*(1/T)**(0.032+sqrt(T/4.8e4))
      xgleft = 0.0; xgright = xg0; xgmed = 0.5*(xgleft + xgright)
      count = 0
      do
        sigg = 0; count = count + 1
        u_g = (xg0 - xgmed)*(xg0 - xgmed) &
              - 4.*pcoef*hcoef/bcoef/rcoef*xgmed*xgmed
        if ((u_g < 0.) .or. (count > 100)) then
          sigg = 1
          exit
        end if
        z_g = sqrt(u_g)
        G_g = 0.5*rcoef*(xg0 - xgmed + z_g) + pcoef*xgmed
        Coefga = (z_g + x_e)
        Coefgb = (z_g + x_e)*(qMet + qmol + (1.+scoef)*x_e) - Acoef
        Coefgc = (z_g + x_e) &
                 *(scoef*x_e*x_e + (1.+scoef)*x_e*qmol + scoef*x_e*qMet &
                   + qMet*qmol) - epsilon*Acoef*(x_e - qmol) &
                 - Acoef*(scoef*x_e + qMet + qmol)
        Coefgd = ((z_g + x_e)*qmol - epsilon*Acoef)*(qMet + x_e)*scoef*x_e - &
                 (1.-epsilon)*Acoef*(scoef*x_e + qMet)*qmol
        rootcube = (/0., 0., 0./); x_g = 0.
        k3 = solve_cubic(Coefga, Coefgb, Coefgc, Coefgd, rootcube)
        !
        select case (k3)
        case (0)
          print *, 'x(g): No Root Found!'
          minimal_network = 0
          return
        case (1)
          if (abs(rootcube(1)) >= 0.1) then
            print *, 'x(g): No Root (<0.1) Found!'
            minimal_network = 0
            return
          else
            x_g = rootcube(1)
            !print *, 'x(g)=', x_g
          end if
        case (2:)
          m3 = 0; l = 1; m3n = 0; ln = 1
          do j = 1, k3
            if (abs(rootcube(j)) < 0.1) then
              if (rootcube(j) > verysmall) then
                m3 = m3 + 1
                if (rootcube(j) >= rootcube(l)) l = j
              else if (rootcube(j) < 0.0) then
                m3n = m3n + 1
                ln = j
              end if
            end if
          end do
          if (m3 >= 1) then
            x_g = rootcube(l)
            if (m3 > 1) then
              !print *, 'CUBIC warning: ', m3, ' Suitable G Roots!'
              !print *, "CUBIC Roots=", rootcube
              !print *, "Choose ", l, "-th Root= ", x_g
            end if
          else if (m3n >= 1) then
            x_g = rootcube(1)
            print *, 'x(g): All-Negative CRoots!'
          end if
        end select
        if (abs((x_g - G_g)/x_g) < 1.e-4) then
          exit
        else if (x_g > G_g) then
          xgright = xgmed
        else if (x_g < G_g) then
          xgleft = xgmed
        end if
        xgmed = 0.5*(xgleft + xgright)
      end do
      ! Grain Abundances in Transition Region
      if (sigg == 0) then
        xgnt = xgmed
        xgm = (G_g - pcoef*xgnt)/rcoef
        xgp = xg0 - xgnt - xgm
      else
        xgleft = 0.0; xgright = xg0; xgmed = 0.5*(xgleft + xgright)
        count = 0
        do ! Below, xgmed represents x(g-), x_g represents x(g)
          g1 = 0.; g2 = 0.; count = count + 1
          x_g = ((2.*rcoef - ccoef)*xgmed*xgmed &
                 + (rcoef*x_e - (rcoef - ccoef)*xg0) &
                 *xgmed)/(hcoef*x_e - (rcoef - ccoef)*xgmed)
          Coefa = ccoef
          Coefb = (2.*pcoef + ccoef)*x_g + bcoef*x_e - ccoef*xg0
          Coefc = pcoef*x_g*x_g + (bcoef*x_e - pcoef*(xg0 - x_e))*x_g &
                  - bcoef*x_e*xg0
          delg = Coefb*Coefb - 4.*Coefa*Coefc
          if ((delg < 0.) .or. (count > 100)) then
            if (delg < 0.) print *, "2nd Regime: x(g-) has no solution!"
            minimal_network = 0
            return
          else
            g1 = (-Coefb + sqrt(delg))/2./Coefa
            g2 = (-Coefb - sqrt(delg))/2./coefa
            !if (g1<0.) print *, "2nd Regime: x(g-) Negative quadratic roots!"
            if (abs((xgmed - g1)/g1) < 1.e-8) then
              exit
            else if (xgmed < g1) then
              xgright = xgmed
            else if (xgmed > g1) then
              xgleft = xgmed
            end if
            xgmed = 0.5*(xgleft + xgright)
          end if
        end do
        G_g = rcoef*xgmed + pcoef*x_g
        xgnt = x_g
        xgm = xgmed
        xgp = xg0 - xgnt - xgm
      end if
      x_i(1) = x_e
      x_i(2) = xgnt
      x_i(3) = xgm
      x_i(4) = xgp
      x_i(5) = epsilon*Acoef/(qmol + G_g)
      x_i(6) = (1.-epsilon)*Acoef/(qmol + G_g + x_e)
      x_i(7) = (1.-epsilon)*Acoef*qmol/(qMet + G_g + x_e)/(qmol + G_g + x_e)
      x_i(8) = (1.-epsilon)*Acoef*qMet/(scoef*x_e + G_g)/(qMet + G_g + x_e)
      !
      chnet = x_e + xgm - xgp - sum(x_i(5:8))
      chrat = xgp/sum(x_i(5:8))
      !if ((sigg==1).and.(abs(chrat)>10.).and.(xgm/xgp<1.2)) x_i(4)= x_e+xgm-sum(x_i(5:8))
      if ((sigg == 1) .and. ((abs(chnet/xgm) > 1.e-3) &
                             .or. (abs(chrat) > 1.))) then
        pchs = (x_i(7) + x_i(8))/(x_e + xgm - xgp - x_i(5) - x_i(6))
        x_i(7) = x_i(7)/pchs
        x_i(8) = x_i(8)/pchs
      end if
    end if
    !xHCO=x_i(7)
    !
    call mhd_coefs(rho, B, T, a_min, a_max, x_i, da, ra, ma, xa, eta_ad, &
                   eta_ohm, eta_hal)
    !
  END FUNCTION minimal_network
  !
  REALNUM FUNCTION f_rec(b, a)
    !
    REALNUM, INTENT(IN) :: b, a
    f_rec = a**(-1.5)*(sqrt(2./b/(2.*b + a))*(2.*b*b + 5.*b*a + 2.*a*a) &
                       + 2.*b + 6.*a)
    !
  END FUNCTION f_rec
  !
  REALNUM FUNCTION f_rat(b, a)
    !
    REALNUM, INTENT(IN) :: b, a
    f_rat = 2./(b**2.5)*atan(sqrt(a/b)) + 2./3.*(3.*a - b)/(b*b)/(a**1.5)
    !
  END FUNCTION f_rat
  !
  !=======================================================================
  !
  SUBROUTINE mhd_coefs(rho, B, T, amin, amax, x_i, da, ra, ma, xa, eta_ad, &
                       eta_ohm, eta_hal)
    !
    !    Compute Non-ideal MHD Coefficients (minimal_network)
    !
    !----------------------------------------------------------------------
    !
    REALNUM, INTENT(OUT) :: eta_ad, eta_ohm, eta_hal
    REALNUM, INTENT(IN) :: rho, B, T, amin, amax, da
    REALNUM, DIMENSION(gbins), INTENT(IN) :: ra, ma, xa
    REALNUM, DIMENSION(:), INTENT(IN) :: x_i
    REALNUM, DIMENSION(8) :: beta_iH2, Z_i, m_i, mtr_iH2, beta2, xt_i
    REALNUM, DIMENSION(gbins) :: beta_g, mtr_g
    REALNUM, DIMENSION(-1:1, gbins) :: xgrs
    REALNUM :: sgm_P, sgm_H, sgm_para, thT, mg, asq, acb, gbcoef, gccoef, &
               mw, xgm0, xgp0, xgnt0
    INTEGER :: i
    !
    mw = 25.*mp/me
    do i = 1, gbins
      xgrs(0, i) = xa(i)/(1.+(1.+mw)/sqrt(mw)/(1.+e_0*e_0/ra(i)/k_b/T))
      xgrs(-1, i) = xgrs(0, i)*sqrt(mw)/(1.+e_0*e_0/ra(i)/k_b/T)
      xgrs(1, i) = xgrs(-1, i)/mw
      mtr_g(i) = 4.*Pi/3.*ra(i)*ra(i)*1.3*sqrt(4.*k_b*T/Pi/mp)
      beta_g(i) = (e_0*B/ma(i)/c_0)*(ma(i) + 2*mp)/(miu*mp*rho*mtr_g(i))
    end do
    xgnt0 = sum(xgrs(0, :))
    xgm0 = sum(xgrs(-1, :))
    xgp0 = sum(xgrs(1, :))
    do i = 1, gbins
      xgrs(0, i) = x_i(2)/xgnt0*xgrs(0, i)
      xgrs(-1, i) = x_i(3)/xgm0*xgrs(-1, i)
      xgrs(1, i) = x_i(4)/xgp0*xgrs(1, i)
    end do
    !
    asq = 5.0*(amin**(-0.5) - amax**(-0.5))/(amin**(-2.5) - amax**(-2.5))
    acb = 5.0*(amax**(0.5) - amin**(0.5))/(amin**(-2.5) - amax**(-2.5))
    thT = log10(T)
    mg = rho_g*4.*Pi/3.*acb
    gbcoef = 4./3.*Pi*rho_g
    gccoef = 4./3.*Pi*1.3*sqrt(4.*k_b*T/Pi/mp)
    m_i = (/me, mg, mg, mg, mp, 3.*mp, 29.*mp, 24.3*mp/)
    Z_i = (/-1., 0., -1., 1., 1., 1., 1., 1./)
    !mtr_iH2= (/ 1.97e-9, 1.77e-3, 1.17d-9, 1.99d-9, 1.73d-9, 1.77d-9 /)
    !
    ! Pinto & Galli 2008: Table 1
    mtr_iH2(1) = sqrt(T)*(0.535 + 0.203*thT - 0.163*thT**2 + 0.050*thT**3)  ! e-
    mtr_iH2(2) = 1.0                                                        ! g0
    mtr_iH2(3) = 4.*Pi/3.*asq*1.3*sqrt(4.*k_b*T/Pi/mp)/1.0e-9               ! g-
    mtr_iH2(4) = 4.*Pi/3.*asq*1.3*sqrt(4.*k_b*T/Pi/mp)/1.0e-9               ! g+
    mtr_iH2(5) = 1.003 + 0.050*thT + 0.136*thT**2 - 0.014*thT**3            ! H+
    mtr_iH2(6) = 2.693 - 1.238*thT + 0.664*thT**2 - 0.089*thT**3            ! H3+
    mtr_iH2(7) = sqrt(T)*(1.476 - 1.409*thT + 0.555*thT**2 - 0.0775*thT**3) ! HCO+
    mtr_iH2(8) = 2.81*sqrt(Z_i(8)*0.804/m_i(8)*mp)*4.                       ! Mg+
    ! mtr_iH2(6)= 1.983*0.425*thT-0.431*thT**2+0.114*thT**3                 ! C+ & H
    !
    ! Array Operations
    mtr_iH2 = mtr_iH2*1.0e-9
    beta_iH2 = (Z_i*e_0*B/m_i/c_0)*(m_i + 2.*mp)/(miu*mp*rho*mtr_iH2)
    beta_iH2(3) = Z_i(3)*e_0*B/c_0/(miu*mp*rho)/(9.*gbcoef*gccoef) &
                  /(amin**(-2.5) - amax**(-2.5))*( &
                  (6.*mp + 5.*gbcoef*amin**3)/(amin**7.5) - &
                  (6.*mp + 5.*gbcoef*amax**3)/(amax**7.5))
    beta_iH2(4) = beta_iH2(3)/Z_i(3)*Z_i(4)
    beta2 = beta_iH2*beta_iH2
    !
    xt_i = x_i
    xt_i(3) = 0.
    xt_i(4) = 0.
    sgm_para = e_0*c_0*rho/B*sum(Z_i*xt_i*beta_iH2)
    sgm_P = e_0*c_0*rho/B*sum(Z_i*xt_i*beta_iH2/(1.+beta2))
    sgm_H = e_0*c_0*rho/B*sum(Z_i*xt_i/(1.+beta2))
    sgm_para = sgm_para + e_0*c_0*rho/B*(sum(xgrs(-1, :)*beta_g(:)) + &
                                         sum(xgrs(1, :)*beta_g(:)))
    sgm_P = sgm_P + e_0*c_0*rho/B &
            *(sum(xgrs(-1, :)*beta_g(:)/(1 + beta_g(:)*beta_g(:))) &
              + sum(xgrs(1, :)*beta_g(:)/(1 + beta_g(:)*beta_g(:))))
    sgm_H = sgm_H + &
            e_0*c_0*rho/B*(-sum(xgrs(-1, :)/(1 + beta_g(:)*beta_g(:))) &
                           + sum(xgrs(1, :)/(1 + beta_g(:)*beta_g(:))))
    !
    eta_ad = c_0*c_0/4./Pi*(sgm_P/(sgm_P*sgm_P + sgm_H*sgm_H) - 1./sgm_para)
    eta_hal = c_0*c_0/4./Pi*sgm_H/(sgm_P*sgm_P + sgm_H*sgm_H)
    eta_ohm = c_0*c_0/4./Pi/sgm_para
    !
  END SUBROUTINE mhd_coefs
  !
  !
  !=======================================================================
  !
  INTEGER FUNCTION solve_cubic(a, b, c, d, rootcube)
    !
    !    Find Real Roots of Cubic Equation
    !
    !----------------------------------------------------------------------
    !
    REALNUM, INTENT(IN) :: a, b, c, d
    REALNUM, INTENT(INOUT), DIMENSION(3) :: rootcube
    REALNUM :: delta, deltal, deltar, r, theta
    !
    solve_cubic = 0
    deltal = b*c/(6.*a*a) - (b/a/3.)**3 - 0.5*d/a
    deltar = c/a/3.-b*b/(a*a)/9.
    delta = deltal*deltal + deltar*deltar*deltar
    ! DELTA > 0
    if (delta >= verysmall) then
      rootcube(1) = -b/a/3.+(deltal + sqrt(delta))**(1./3.) + &
                    (deltal - sqrt(delta))**(1./3.)
      solve_cubic = 1
      return
    end if
    ! DELTA = 0
    if ((delta > 0) .and. (delta < verysmall)) then
      if ((abs(deltal) < verysmall) .and. (abs(deltar) < verysmall)) then
        rootcube(1) = -b/a/3.
        solve_cubic = 1
      else
        rootcube(1) = -b/a/3.+2.*(deltal**(1./3.))
        rootcube(2) = -b/a/3.-deltal**(1./3.)
        solve_cubic = 2
      end if
      return
    end if
    ! DELTA < 0
    if (delta < 0) then
      r = sqrt(deltal*deltal - delta)
      theta = acos(deltal/r)
      rootcube(1) = -b/a/3.+2.*cos(theta/3.)*r**(1./3.)
      rootcube(2) = -b/a/3.+2.*cos((theta + 2.*Pi)/3.)*r**(1./3.)
      rootcube(3) = -b/a/3.+2.*cos((theta + 4.*Pi)/3.)*r**(1./3.)
      solve_cubic = 3
    end if
  END FUNCTION solve_cubic
  !
  !=======================================================================
  !
  INTEGER FUNCTION solve_quart(a, b, c, d, e, rootquart)
    !
    !    Find Real Roots of Quartic Equation
    !
    !----------------------------------------------------------------------
    !
    REALNUM, PARAMETER :: vvsmall = 1.0e-199
    REALNUM, INTENT(IN) :: a, b, c, d, e
    REALNUM, INTENT(INOUT), DIMENSION(4) :: rootquart
    REALNUM :: deltal, deltar, delta, factl, factr, Pl, Pr, Qc, Sc, r, phi, tmp
    INTEGER :: signal, count
    !
    ! print *, 'a,b,c,d,e=', a,b,c,d,e
    solve_quart = 0
    deltal = 2*c*c*c - 9*b*c*d + 27*a*d*d + 27*b*b*e - 72*a*c*e
    deltar = c*c - 3*b*d + 12*a*e
    delta = deltal*deltal - 4*deltar*deltar*deltar
    factl = 0.25*b*b/(a*a) - 2/3*c/a
    factr = -0.25*(b/a)**3 + b*c/(a*a) - 2*d/a
    Pl = -12*a*a*factl
    Pr = 16*a*a*(4*a*e - c*c - b*d) + b*b*(16*a*c - 3*b*b)
    !
    ! DELTA < 0
    if ((delta < 0) .and. (Pl < 0) .and. (Pr < 0)) then
      ! deltar > 0 always satisfy
      ! The last two conditions guarantee 4 real roots
      r = deltar**(1.5)
      phi = acos(0.5*deltal/r)
      Sc = sqrt(factl + 2/(3*a)*sqrt(deltar)*cos(phi/3))
      if (abs(Sc) > verysmall) then
        count = 0
        if (-Sc*Sc + 3*factl + factr/Sc > 0) then
          rootquart(1) = -b/(4*a) + Sc/2 &
                         - 0.5*sqrt(-Sc*Sc + 3*factl + factr/Sc)
          rootquart(2) = -b/(4*a) + Sc/2 &
                         + 0.5*sqrt(-Sc*Sc + 3*factl + factr/Sc)
          count = 2
        end if
        if (-Sc*Sc + 3*factl - factr/Sc > 0) then
          rootquart(count + 1) = -b/(4*a) - Sc/2 &
                                 - 0.5*sqrt(-Sc*Sc + 3*factl - factr/Sc)
          rootquart(count + 2) = -b/(4*a) - Sc/2 &
                                 + 0.5*sqrt(-Sc*Sc + 3*factl - factr/Sc)
          count = count + 2
        end if
        solve_quart = count
      else
        print *, "ZEUS_CHEM FAIL: Solver Error!"
        solve_quart = 0
      end if
      return
    end if
    !
    signal = 0
    ! DELTA > 0
    if (delta >= vvsmall) then
      if (abs(deltar) < verysmall) then
        Qc = (deltal)**(1.0/3.0)
      else
        Qc = ((deltal + sqrt(delta))/2)**(1.0/3.0)
      end if
      signal = 1
    end if
    ! DELTA = 0
    if ((delta >= 0) .and. (delta < vvsmall)) then
      if (abs(deltal) >= verysmall) then
        Qc = (deltal/2)**(1.0/3.0)
        signal = 2
      else ! deltal = 0
        if (abs(Pr) >= verysmall) then
          signal = 3 ! Trial, All Delta = 0
        else ! Normally Won't Trigger
          rootquart(1) = -0.25*b/a
          solve_quart = 1
          if (rootquart(1) < 0) solve_quart = 0
        end if
      end if
    end if
    ! Combine DELTA = 0 & DELTA > 0 Calculation
    if (signal > 0) then
      tmp = factl + (Qc + deltar/Qc)/(3*a)
      if (signal == 3) tmp = factl
      if (tmp < 0) then
        print *, "ZEUS_CHEM FAIL: S not Real!"
        solve_quart = 0
      else ! Sc is Real
        Sc = sqrt(tmp)
        if ((Sc >= 0) .and. (Sc < verysmall)) then
          ! Sc = 0, Reduces to Double-Square. Normally Won't Trigger
          tmp = 0.25*factl - (Qc + deltar/Qc)/(6*a)
          if (signal == 3) tmp = 0.25*factl
          if (tmp < 0) then
            print *, "ZEUS_CHEM FAIL: Double-Square has Complex Roots!"
            solve_quart = 0
          else
            rootquart(1) = -0.25*b/a + sqrt(tmp)
            rootquart(2) = -0.25*b/a - sqrt(tmp)
            solve_quart = 2
          end if
        else ! Sc > 0, Solve Normally
          count = 0
          if (-Sc*Sc + 3*factl + factr/Sc > 0) then
            rootquart(1) = -b/(4*a) + Sc/2 &
                           - 0.5*sqrt(-Sc*Sc + 3*factl + factr/Sc)
            rootquart(2) = -b/(4*a) + Sc/2 &
                           + 0.5*sqrt(-Sc*Sc + 3*factl + factr/Sc)
            count = 2
          end if
          if (-Sc*Sc + 3*factl - factr/Sc > 0) then
            rootquart(count + 1) = -b/(4*a) - Sc/2 &
                                   - 0.5*sqrt(-Sc*Sc + 3*factl - factr/Sc)
            rootquart(count + 2) = -b/(4*a) - Sc/2 &
                                   + 0.5*sqrt(-Sc*Sc + 3*factl - factr/Sc)
            count = count + 2
          end if
          solve_quart = count
          if (solve_quart == 0) print *, "ZEUS_CHEM FAIL: All Complex Roots!"
        end if
      end if
    end if
    !
  END FUNCTION solve_quart
END MODULE zeus_chemistry
