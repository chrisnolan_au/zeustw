#include "macros"
MODULE zeus_deadcode
  ! This module contains placeholders for routines currently not being used.
  ! As of today (2004-04-01) these are the routines related
  ! to moving grids, the timeslice output, and the Poisson
  ! gravity solvers.

  ! 2007-02-18: moving grids will be reactivated, taken out from here

  IMPLICIT NONE
  PUBLIC

CONTAINS

  SUBROUTINE timeslice_init
    ! call tslice_init
  END SUBROUTINE timeslice_init

  SUBROUTINE timeslice_check
    ! call tslice_check
  END SUBROUTINE timeslice_check

  SUBROUTINE timeslice
    ! call tslice_wrapper
  END SUBROUTINE timeslice

  SUBROUTINE timeslice_end
    ! call tslice_end
  END SUBROUTINE timeslice_end

END MODULE zeus_deadcode
