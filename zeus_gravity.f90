#include "macros"
MODULE zeus_gravity
  USE zeus_variables
  USE zeus_io
  IMPLICIT NONE

  PRIVATE
  PUBLIC :: newgravity

  ! Associated Legendre functions (semi-normalized, no phase factor)
  ! Wasteful storage version
  REALNUM, SAVE, ALLOCATABLE :: plm_s(:, :, :)
  INTEGER, SAVE :: lmax

  ! Multipoles of the distribution of mass.  Wasteful storage version
  REALNUM, SAVE, ALLOCATABLE, TARGET :: &
    qlm_lower_cosine(:), qlm_lower_sine(:), &
    qlm_upper_cosine(:), qlm_upper_sine(:)

  REALNUM, SAVE, POINTER :: &
    qlm_lower_cosine_local(:), qlm_lower_sine_local(:), &
    qlm_upper_cosine_local(:), qlm_upper_sine_local(:)

CONTAINS
  !
  !=============================================================
  !
  SUBROUTINE newgravity
    CHARACTER(LEN=*), PARAMETER :: routinename = 'z_newgravity'
    select case (gravity_algorithm)
    case ('none')
      return
    case ('fixed')
      return
    case ('onlycentralmass')
      call centralmassforce
    case ('monopole')
      call gravity_monopole ! This takes care of centralmass force
    case ('sor')
      if (jnac == 0 .or. inac == 0) then
        call finalstop(routinename, 'Only for substantial x1 and x2.')
      end if
      call centralmassforce
      if (knac == 0) then
        call gravity_sor_2D
      else
        call gravity_sor_3D
      end if
    case default
      call finalstop(routinename, &
                     'Value of gravity_algorithm not assigned or not in use.')
    end select
    call writedata(gp, 'z_newgravity_gp_', 4)
  END SUBROUTINE newgravity
  !
  !=============================================================
  !
  SUBROUTINE gravity_sor_2D
#ifdef MPI
    USE mpi
    USE zeus_mpi
    integer ierr, status(MPI_STATUS_SIZE), dest
    REALNUM buffer(3)

    integer, parameter :: depth = 1

    integer ic, jc, kc
    REALNUM accumulator
#endif /* MPI */
    CHARACTER(LEN=*), PARAMETER :: routinename = 'z_gravity_sor_2D'

    integer i, j, k, jp1, ip1

    REALNUM, SAVE, ALLOCATABLE, DIMENSION(:, :) :: coeff_iminus, &
                                                   coeff_jminus, &
                                                   coeff_diagonal, &
                                                   omega, rjac2

    REALNUM, SAVE, ALLOCATABLE, DIMENSION(:, :) :: &
      coeff_iplus, coeff_jplus

    integer ipass, isw, jsw, n
    integer, save :: imin, imax, jmin, jmax
    REALNUM rjac, resid
    REALNUM relative_worst

    CHARACTER(LEN=120) :: message

    LOGICAL, SAVE :: firstcall = .true.

    if (firstcall) then
      if (gravity_algorithm /= 'sor') then
        call finalstop(routinename, &
                       'Only for SOR algorithm.')
      end if
      ! Reject non-implemented geometries
      if (knac /= 0 .or. geometry /= 'rtp') then
        call finalstop(routinename, &
                       'Only for spherical axisymmetric gravity.')
        ! Note (2011-02-20): adapting this code to cylindrical
        ! axisymmetric could be useful.
        ! Adapting the code to
        ! Cartesian is possible, but not necessarily advisable.
      end if

      ! These values of min and max indices were calculated for
      ! something on the a-grid.  Do they work well also on the
      ! b-grid?  That needs to be checked.
      imin = is
      jmin = js
      imax = ie
      jmax = je

#ifdef MPI
      ! Adjust BC extremes properly for a parallel run
      if (geometry == 'zrp' .or. geometry == 'xyz') then
        if (lg_lastnode1) imax = iego
        if (lg_lastnode2) jmax = jego
      else if (geometry == 'rtp') then
        if (lg_lastnode1) imax = ie
        if (lg_lastnode2) jmax = je
      end if

      !   Define "chunk" datatypes used to pass field blocks
      !ic=iea-isa+1
      !jc=jea-jsa+1
      !kc=1
      !call mpi_subsection_type(chunkpsi(1), &
      !     (/ic, jc, kc/), (/depth,jc,kc/) )
      !call mpi_subsection_type(chunkpsi(2), &
      !     (/ic, jc, kc/), (/ic,depth,kc/) )
#endif /* MPI */

      ! Memory not to be released
      ALLOCATE (coeff_iminus(isa:iea, jsa:jea))
      ALLOCATE (coeff_jminus(isa:iea, jsa:jea))
      ALLOCATE (coeff_iplus(isa:iea, jsa:jea))
      ALLOCATE (coeff_jplus(isa:iea, jsa:jea))
      ALLOCATE (coeff_diagonal(isa:iea, jsa:jea))
      ALLOCATE (omega(isa:iea, jsa:jea))
      ALLOCATE (rjac2(isa:iea, jsa:jea))

      !write(*,*) 'g2ai', g2ai
      !write(*,*) 'g31ai', g31a
      !write(*,*) 'dx1bi', dx1bi
      !write(*,*) 'dvl2a', dvl2a

      !write(*,*) 'g32a', g32a
      !write(*,*) 'g2bi', g2bi
      !write(*,*) 'dx2bi', dx2bi
      !write(*,*) 'dvl1a', dvl1a

      ! Code written for 2D non-moving grids
      do j = jsg, jeg
        jp1 = min(jeg, j + 1)
        do i = isg, ieg
          ip1 = min(ieg, i + 1)

          !coeff_iminus(i,j)= -g2ai(i)*g31a(i)*dx1bi(i)*dvl2a(j)
          !coeff_iplus(i,j)=  -g2ai(ip1)*g31a(ip1)*dx1bi(ip1)*dvl2a(j)
          !coeff_jminus(i,j)= -g32a(j)*g2bi(i)**2*dx2bi(j)*dvl1a(i)
          !coeff_jplus(i,j)= -g32a(jp1)*g2bi(i)**2*dx2bi(jp1)*dvl1a(i)

          !coeff_iminus(i,j)= -g2ai(i)*g31a(i)*dx1bi(i)*dvl1ai(i)
          !coeff_iplus(i,j)= -g2ai(ip1)*g31a(ip1)*dx1bi(ip1)*dvl1ai(i)
          !coeff_jminus(i,j)= -g32a(j)*g2bi(i)**2*dx2bi(j)*dvl2ai(j)
          !coeff_jplus(i,j)=-g32a(jp1)*g2bi(i)**2*dx2bi(jp1)*dvl2ai(j)

          ! Code for RTP spherical only
          coeff_iminus(i, j) = -(x1a(i)*x1bi(i))**2* &
                               dx1bi(i)*dx1ai(i)
          coeff_iplus(i, j) = -(x1a(ip1)*x1bi(i))**2* &
                              dx1bi(ip1)*dx1ai(i)
          coeff_jminus(i, j) = &
            -abs(g32a(j)*g32bi(j))*dx2bi(j)* &
            dx2ai(j)*x1bi(i)**2
          coeff_jplus(i, j) = &
            -abs(g32a(jp1)*g32bi(j))*dx2bi(jp1)* &
            dx2ai(j)*x1bi(i)**2

          coeff_diagonal(i, j) = &
            -(coeff_iminus(i, j) + coeff_iplus(i, j) + &
              coeff_jminus(i, j) + coeff_jplus(i, j))

          rjac = two/(coeff_diagonal(i, j) + verysmall)* &
                 (cos(pi/(iep - isp))* &
                  sqrt(abs(coeff_iminus(i, j)*coeff_iplus(i, j))) &
                  + cos(pi/(jep - jsp))* &
                  sqrt(abs(coeff_jminus(i, j)*coeff_jplus(i, j))))

          rjac2(i, j) = rjac**2

        end do
      end do

      firstcall = .false.

    end if
#if 0
    ! Check change in d_ to see if new calculation is needed.
    ! Do it before updating BCs, because those are expensive.
    relative_worst = zro
    k = ks
    do j = jmin, jmax
      do i = imin, imax
        resid = coeff_iplus(i, j)*gp(i + 1, j, k) &
                + coeff_iminus(i, j)*gp(i - 1, j, k) &
                + coeff_jplus(i, j)*gp(i, j + 1, k) &
                + coeff_jminus(i, j)*gp(i, j - 1, k) &
                + coeff_diagonal(i, j)*gp(i, j, k) &
                - four*pi*gnewton*d_(i, j, k)

        ! Formula based on the expected SOR change in gp, written
        ! as a relative value of the change due to the residual
        ! May need change.
        resid = resid/coeff_diagonal(i, j)
        relative_worst = max(relative_worst, &
                             abs(resid/gp(i, j, k)))

      end do
    end do
#ifdef MPI
    call MPI_ALLREDUCE(relative_worst, accumulator, 1, &
                       MPI_FLOATMPI, MPI_MAX, MPI_COMM_WORLD, ierr)
    relative_worst = accumulator
#endif /* MPI */

    if (debug >= 1) then
      write (message, *) relative_worst, ntimestep, 'SKIP'
      call writemessage(routinename, message)
    end if

    if (relative_worst <= graverr) return
#endif /* 0 */
    call writedata(gp, 'z_gp_before_calling_bc', 4)
    call gp_boundary_2D

    ! Reset omega
    do j = jsg, jeg
      do i = isg, ieg
        omega(i, j) = one/(one - haf*rjac2(i, j))
      end do
    end do

    k = ks
    do n = 1, maximum_sor_iterations
      relative_worst = zro
      ! isw=1+mod(node1*inac,2)
      isw = 1 + mod(node1*inac + node2*jnac, 2)
      do ipass = 1, 2
        jsw = isw
        do i = imin, imax
          do j = jsw - 1 + jmin, jmax, 2
            resid = coeff_iplus(i, j)*gp(i + 1, j, k) &
                    + coeff_iminus(i, j)*gp(i - 1, j, k) &
                    + coeff_jplus(i, j)*gp(i, j + 1, k) &
                    + coeff_jminus(i, j)*gp(i, j - 1, k) &
                    + coeff_diagonal(i, j)*gp(i, j, k) &
                    - four*pi*gnewton*d_(i, j, k)
            !   -four*pi*gnewton*dvl1a(i)*dvl2a(j)*d_(i,j,k)

            resid = resid/coeff_diagonal(i, j)
            relative_worst = &
              max(relative_worst, abs(resid/gp(i, j, k)))

            gp(i, j, k) = gp(i, j, k) - omega(i, j)*resid

            ! Chebyshev acceleration (done as in dzeus35)
            omega(i, j) = one/(one - 0.25*rjac2(i, j)*omega(i, j))

          end do
          jsw = 3 - jsw
        end do
        isw = 3 - isw
#ifdef MPI
        ! MESSAGE PASSING OF gp
        call mpifield(gp)
#endif /* MPI */

      end do
#ifdef MPI
      call MPI_ALLREDUCE(relative_worst, accumulator, 1, &
                         MPI_FLOATMPI, MPI_MAX, MPI_COMM_WORLD, ierr)
      relative_worst = accumulator
#endif /* MPI */

      ! Code to check convergence.  Return to caller in case of
      ! success.  Keep running in case it still needs to work.
      ! Have something prepared for the case of failure to
      ! converge.

      !if (iotty /= NO_OUTPUT) write(iotty,*) &
      !     n, maxval(omega(imin:imax,jmin:jmax)), &
      !     minval(omega(imin:imax,jmin:jmax)), &
      !     maxval(gp(imin:imax,jmin:jmax,k)), &
      !     minval(gp(imin:imax,jmin:jmax,k)), &
      !     relative_worst
      !if (iolog /= NO_OUTPUT) write(iolog,*) &
      !     n, maxval(omega(imin:imax,jmin:jmax)), &
      !     minval(omega(imin:imax,jmin:jmax)), &
      !     maxval(gp(imin:imax,jmin:jmax,k)), &
      !     minval(gp(imin:imax,jmin:jmax,k)), &
      !     relative_worst

      if (relative_worst <= graverr) then
        exit
      end if

    end do

    if (debug >= 1) then
      write (message, *) relative_worst, n, ntimestep, ' SOR'
      call writemessage(routinename, message)
    end if

    call writedata(gp, 'z_gp_file', 4)
    call writedata(d_, 'z_rho_file', 4)

    if (relative_worst > graverr) then
      call finalstop(routinename, &
                     'Unhappy exit.  Gravity could not converge.')
    end if

    ! Adjust axial BCs:

    if (l_firstnode2) then
      do k = ksg, keg
        do i = isg, ieg
          gp(i, jsm1, k) = gp(i, js, k)
        end do
      end do
    end if
    if (l_lastnode2) then
      do k = ksg, keg
        do i = isg, ieg
          gp(i, jep1, k) = gp(i, je, k)
        end do
      end do
    end if

  END SUBROUTINE gravity_sor_2D
  !============================================================
  SUBROUTINE gp_boundary_2D
#ifdef MPI
    USE mpi
    USE zeus_mpi
    INTEGER :: ierr
    INTEGER :: n1, n2, n3, size1, size2
    REALNUM, SAVE, ALLOCATABLE, DIMENSION(:) :: receivesum
    INTEGER, SAVE, ALLOCATABLE, DIMENSION(:, :) :: receivecounts
#endif /* MPI */
    CHARACTER(LEN=*), PARAMETER :: routinename = 'z_gp_boundary_2D'

    REALNUM, SAVE, ALLOCATABLE, DIMENSION(:, :, :, :) :: term
    REALNUM, SAVE, ALLOCATABLE, DIMENSION(:) :: localsum

    INTEGER :: i, j, iprime, jprime
    INTEGER :: index
    INTEGER, SAVE :: indexfinal
    INTEGER, PARAMETER :: indexmax = 2
    INTEGER, SAVE :: ivalues(indexmax), ivalues_local(indexmax)
    REALNUM :: kfactor, kmodulus_square, elliptick, m1
    REALNUM :: cylrprime, cylr, zprime, z
    !REALNUM :: rprime, r
    LOGICAL, SAVE :: firstcall = .true.

    if (firstcall) then
      ! Reject non-implemented geometries
      if (knac /= 0 .or. geometry /= 'rtp') then
        call finalstop(routinename, &
                       'Only for spherical axisymmetric gravity BC.')
      end if

      ! Calculate the invariant part of the integrand at the i
      ! boundary conditions.

      ! Lower and upper radius, global and local index
      ivalues = isa - 1  ! Default values out of range
      ivalues_local = isa - 1

      ivalues(1) = ism1p
      if (l_firstnode1) ivalues_local(1) = ism1

      indexfinal = 1
      if (iep1p > ism1p) then
        ivalues(2) = iep1p
        if (l_lastnode1) ivalues_local(2) = iep1
        indexfinal = 2
      end if

      ALLOCATE (term(1:indexfinal, jsap:jeap, isa:iea, jsa:jea))
      ALLOCATE (localsum(jsp:jep))

#ifdef MPI
      ALLOCATE (receivesum(js:je))
      ALLOCATE (receivecounts(0:nnodes1*nnodes2*nnodes3 - 1, &
                              indexfinal))
#endif /* MPI */

      ! write(*,*) 'inside boundary, allocations done ', nodename

      do index = 1, indexfinal
        i = ivalues(index)

        !write(*,*) 'inside boundary, first major loop', &
        !    index, indexfinal, ivalues, ' ', nodename

        do j = jsp, jep
          z = x1bp(i)*cos(x2bp(j))
          cylr = x1bp(i)*sin(x2bp(j))

          !write(*,*) 'inside boundary, inside the 1st loop', &
          !    index, j, jsp, jep, ' ', nodename

          do jprime = js, je
            do iprime = is, ie
              zprime = x1b(iprime)*cos(x2b(jprime))
              cylrprime = x1b(iprime)*sin(x2b(jprime))

              kfactor = one/((cylr + cylrprime)**2 + (z - zprime)**2)
              kmodulus_square = four*cylr*cylrprime*kfactor

              m1 = one - kmodulus_square

              elliptick = &
                (((.01451196212_pk*m1 + .03742563713_pk)*m1 &
                  + .03590092383_pk)*m1 + .09666344259_pk)*m1 + &
                1.38629436112_pk &
                - ( &
                (((.00441787012_pk*m1 + .03328355346_pk)*m1 + &
                  .06880248576_pk)*m1 + .12498593597_pk)*m1 + haf &
                )*log(m1)

              term(index, j, iprime, jprime) = &
                four*gnewton* &
                elliptick*sqrt(kfactor)* &
                dvl1a(iprime)*dvl2a(jprime)

            end do
          end do
        end do
      end do

      !write(*,*) 'inside boundary, forcing a stop', &
      !    ' ', nodename

#ifdef MPI
      do index = 1, indexfinal
        do n3 = 0, nnodes3 - 1
          do n2 = 0, nnodes2 - 1
            if (n2 == nnodes2 - 1) then
              ! Last processor may be short.  No consideration
              !   for redundant processors is given.
              size2 = (jep - jsp + 1) - (nnodes2 - 1)*jnac
            else
              size2 = jnac
            end if
            do n1 = 0, nnodes1 - 1
              size1 = 0
              if (index == 1 .and. n1 == 0) size1 = 1
              if (index > 1 .and. n1 == nnodes1 - 1) size1 = 1
              receivecounts(nodefun(n1, n2, n3), index) = &
                size1*size2
            end do
          end do
        end do
        !write(*,*) 'inside boundary', &
        !  nodename, index, 'counts ', receivecounts(:,index)
      end do
#endif /* MPI */

      firstcall = .false.
    end if

    ! Compute local sum
    do index = 1, indexfinal
      !write(*,*) 'Entering second loop', index, ' ', nodename
      do j = jsp, jep
        localsum(j) = zro

        do jprime = js, je
          do iprime = is, ie
            localsum(j) = localsum(j) + d_(iprime, jprime, ks)* &
                          term(index, j, iprime, jprime)
          end do
        end do
      end do

#ifdef MPI
      !write(*,*) 'before reduce-scatter', index, ' ', nodename

      ! Scatter results to the relevant processors
      call MPI_REDUCE_SCATTER(localsum, receivesum, &
                              receivecounts(:, index), &
                              MPI_FLOATMPI, MPI_SUM, MPI_COMM_WORLD, ierr)

      !write(*,*) 'after reduce-scatter', &
      !    index, ivalues_local, ' ', nodename

      ! Store in gp

      if (receivecounts(node, index) > 0) then
        !write(*,*) 'before storing', &
        !    index, ivalues_local, ' ', nodename
        i = ivalues_local(index)
        gp(i, js:je, ks) = receivesum
        !write(*,*) 'after storing', index, ' ', nodename
      end if

#else /* MPI */
      ! Just store in gp
      !write(*,*) 'before storing', &
      !    index, ivalues_local, ' ', nodename
      i = ivalues_local(index)
      gp(i, js:je, ks) = localsum
      !write(*,*) 'after storing', index, ' ', nodename
#endif /* MPI */

      !write(*,*) 'end of second loop', index, ' ', nodename

    end do

    !write(*,*) 'end op gp_boundary', index, ' ', nodename

  END SUBROUTINE gp_boundary_2D
  !
  !=============================================================
  !
  SUBROUTINE gravity_sor_3D
    USE zeus_mpi
#ifdef MPI
    USE mpi
    integer ierr, status(MPI_STATUS_SIZE), dest
    REALNUM buffer(3)

    integer, parameter :: depth = 1
    integer ic, jc, kc
    REALNUM accumulator
#endif /* MPI */
    CHARACTER(LEN=*), PARAMETER :: routinename = 'z_gravity_sor_3D'
    CHARACTER(LEN=120) :: message

    integer i, j, k, jp1, ip1
    integer kotherside

    REALNUM, SAVE, ALLOCATABLE, DIMENSION(:, :) :: coeff_iminus, &
                                                   coeff_jminus, &
                                                   coeff_diagonal, rjac2

    REALNUM, SAVE, ALLOCATABLE, DIMENSION(:, :, :) :: omega

    REALNUM, SAVE, ALLOCATABLE, DIMENSION(:, :) :: &
      coeff_iplus, coeff_jplus !, coeff_kminus, coeff_kplus

    REALNUM, SAVE, ALLOCATABLE, DIMENSION(:, :) :: &
      coeff_k

    integer ipass, jsw, n
    integer, save :: imin, imax, jmin, jmax, kmin, kmax
    REALNUM rjac, resid
    REALNUM relative_worst
    REALNUM gpadjust1, gpadjust2

    INTEGER, PARAMETER :: sordebug = 2

    LOGICAL, SAVE :: firstcall = .true.

    ! integer, save :: chunkpsi(2)

    call writedata(routinename//'_begin', 4)

    if (firstcall) then
      if (gravity_algorithm /= 'sor') then
        call finalstop(routinename, &
                       'Only for SOR algorithm.')
      end if

      ! Reject non-implemented geometries
      if (knac == 0 .or. geometry /= 'rtp') then
        call finalstop(routinename, 'Only for spherical 3D.')
      end if
      !if (nnodes3 /= 1) then
      !   call finalstop(routinename,'Only for nnodes3=1.')
      !end if

      ! To make coeff_k independent of the index k,
      ! we force the phi-grid to be uniform
      ! This is checked here.
      ! The number 100.0_pk is a more or less arbitrary choice.
      ! False positives and false negatives are conceivable.
      if ((maxval(dx3a) - minval(dx3a)) &
          /epsilon(dx3a) >= 100.0_pk) then
        call finalstop(routinename, 'The phi grid looks non-uniform.' &
                       //'  Only for uniform phi grid.')
      end if

      if (mod(knac, 2) /= 0) then
        call finalstop(routinename, &
                       'Only for even number of phi zones on each processor.')
      end if

      imin = is_e
      imax = ie_e
      jmin = js
      jmax = je
      kmin = ks
      kmax = ke

#ifdef MPI
      ! Adjust BC extremes properly for a parallel run
      if (geometry == 'zrp' .or. geometry == 'xyz') then
        !if (lg_lastnode1) imax = iego check later for parallelism
        if (lg_lastnode2) jmax = jego
      else if (geometry == 'rtp') then
        !if (lg_lastnode1) imax = ie check later for parallelism
        if (lg_lastnode2) jmax = je
      end if
#endif /* MPI */

      ! Memory not to be released
      ALLOCATE (coeff_iminus(isa_e:iea_e, jsa:jea))
      ALLOCATE (coeff_jminus(isa_e:iea_e, jsa:jea))
      !ALLOCATE(coeff_kminus(isa_e:iea_e,jsa:jea))
      ALLOCATE (coeff_iplus(isa_e:iea_e, jsa:jea))
      ALLOCATE (coeff_jplus(isa_e:iea_e, jsa:jea))
      !ALLOCATE(coeff_kplus(isa_e:iea_e,jsa:jea))
      ALLOCATE (coeff_diagonal(isa_e:iea_e, jsa:jea))
      !ALLOCATE(omega(isa_e:iea_e,jsa:jea))
      ALLOCATE (rjac2(isa_e:iea_e, jsa:jea))

      ALLOCATE (omega(isa_e:iea_e, jsa:jea, ksa:kea))

      ALLOCATE (coeff_k(isa_e:iea_e, jsa:jea))

      !write(*,*) 'g2ai', g2ai
      !write(*,*) 'g31ai', g31a
      !write(*,*) 'dx1bi', dx1bi
      !write(*,*) 'dvl2a', dvl2a

      !write(*,*) 'g32a', g32a
      !write(*,*) 'g2bi', g2bi
      !write(*,*) 'dx2bi', dx2bi
      !write(*,*) 'dvl1a', dvl1a

      ! Code written for 2D non-moving grids
      do j = jmin, jmax
        jp1 = j + 1
        do i = imin, imax
          ip1 = i + 1

          !coeff_iminus(i,j)= -g2ai(i)*g31a(i)*dx1bi(i)*dvl2a(j)
          !coeff_iplus(i,j)=  -g2ai(ip1)*g31a(ip1)*dx1bi(ip1)*dvl2a(j)
          !coeff_jminus(i,j)= -g32a(j)*g2bi(i)**2*dx2bi(j)*dvl1a(i)
          !coeff_jplus(i,j)= -g32a(jp1)*g2bi(i)**2*dx2bi(jp1)*dvl1a(i)

          !coeff_iminus(i,j)= -g2ai(i)*g31a(i)*dx1bi(i)*dvl1ai(i)
          !coeff_iplus(i,j)= -g2ai(ip1)*g31a(ip1)*dx1bi(ip1)*dvl1ai(i)
          !coeff_jminus(i,j)= -g32a(j)*g2bi(i)**2*dx2bi(j)*dvl2ai(j)
          !coeff_jplus(i,j)=-g32a(jp1)*g2bi(i)**2*dx2bi(jp1)*dvl2ai(j)

          ! Code for RTP spherical only
          coeff_iminus(i, j) = -(x1a_e(i)*x1bi_e(i))**2* &
                               dx1bi_e(i)*dx1ai_e(i)
          coeff_iplus(i, j) = -(x1a_e(ip1)*x1bi_e(i))**2* &
                              dx1bi_e(ip1)*dx1ai_e(i)
          coeff_jminus(i, j) = &
            -abs(g32a(j)*g32bi(j))*dx2bi(j)* &
            dx2ai(j)*x1bi_e(i)**2
          coeff_jplus(i, j) = &
            -abs(g32a(jp1)*g32bi(j))*dx2bi(jp1)* &
            dx2ai(j)*x1bi_e(i)**2
          !             coeff_kminus(i,j)= &
          !                 x1bi_e(i)**2*g32bi(j)**2* &
          !                 dx3bi(k)*dx3ai(k)
          !             coeff_plus(i,j)= &
          !                 x1bi_e(i)**2*g32bi(j)**2* &
          !                 dx3bi(kp1)*dx3ai(k)

          coeff_k(i, j) = -x1bi_e(i)**2*g32bi(j)**2*dx3bi(ks)**2

          coeff_diagonal(i, j) = &
            -(coeff_iminus(i, j) + coeff_iplus(i, j) + &
              coeff_jminus(i, j) + &
              coeff_jplus(i, j) + two*coeff_k(i, j))

          rjac = two/(coeff_diagonal(i, j) + verysmall)* &
                 (cos(pi/(iep - isp))* &
                  sqrt(abs(coeff_iminus(i, j)*coeff_iplus(i, j))) &
                  + cos(pi/(jep - jsp))* &
                  sqrt(abs(coeff_jminus(i, j)*coeff_jplus(i, j))) &
                  + cos(pi/(kep - ksp))* &
                  sqrt(abs(coeff_k(i, j)*coeff_k(i, j))))

          rjac2(i, j) = rjac**2

        end do
      end do

      if (debug >= 2) then
        write (message, *) routinename, 'i:', imin, imax
        call threeoutput(routinename, message)
        write (message, *) routinename, 'j:', jmin, jmax
        call threeoutput(routinename, message)
        write (message, *) routinename, 'k:', kmin, kmax
        call threeoutput(routinename, message)

        call writedata(coeff_iplus, routinename//'_coeff_iplus')
        call writedata(coeff_iminus, routinename//'_coeff_iminus')
        call writedata(coeff_jplus, routinename//'_coeff_jplus')
        call writedata(coeff_jminus, routinename//'_coeff_jminus')
        call writedata(coeff_k, routinename//'_coeff_k')
        call writedata(coeff_diagonal, routinename//'_coeff_diagonal')
      end if

      firstcall = .false.

    end if

    call writedata(gp_e, routinename//'_gp_e', 4)

    ! If needed, copy density from the hydro array to the extended array
    if (withextendedgrid) then
      d_e(imin:is - 1, :, :) = zro
      d_e(is:ie, :, :) = d_(is:ie, :, :)
      d_e(ie + 1:imax, :, :) = zro
    end if

    call writedata(d_e, routinename//'_rho_1', 4)

    !call writedata(d_e(imin:imax,jmin:jmax,kmin:kmax),'d_e')

#if 0
    ! Check change in density to see if new calculation is needed.
    ! Do it before updating BCs, because those are expensive.
    relative_worst = zro
    do k = kmin, kmax
      do j = jmin, jmax
        do i = imin, imax
          resid = coeff_iplus(i, j)*gp_e(i + 1, j, k) &
                  + coeff_iminus(i, j)*gp_e(i - 1, j, k) &
                  + coeff_jplus(i, j)*gp_e(i, j + 1, k) &
                  + coeff_jminus(i, j)*gp_e(i, j - 1, k) &
                  + coeff_k(i, j)*gp_e(i, j, k - 1) &
                  + coeff_k(i, j)*gp_e(i, j, k + 1) &
                  + coeff_diagonal(i, j)*gp_e(i, j, k) &
                  - four*pi*gnewton*d_e(i, j, k)

          ! Formula based on the expected SOR change in gp_e,
          ! written as a relative value of the change due
          ! to the residual
          ! May need change.
          resid = resid/coeff_diagonal(i, j)
          relative_worst = max(relative_worst, &
                               abs(resid/gp_e(i, j, k)))
        end do
      end do
    end do

#ifdef MPI
    call MPI_ALLREDUCE(relative_worst, accumulator, 1, &
                       MPI_FLOATMPI, MPI_MAX, MPI_COMM_WORLD, ierr)
    relative_worst = accumulator
#endif /* MPI */

    if (debug >= 1) then
      write (message, *) relative_worst, ntimestep, 'SKIP'
      call writemessage(routinename, message)
    end if

    if (relative_worst <= graverr) return
#endif /* 0 */

    call writedata(gp_e, routinename//'_gp_before_calling_bc', 4)

    call gp_boundary_3D
    call writedata(gp_e, routinename//'_gp_after_calling_bc', 4)
    call mpifield_e(gp_e)
    if (nnodes3 == 1) then
      !Adjust periodic BC in direction phi
      gp_e(:, :, ksm1) = gp_e(:, :, ke)
      gp_e(:, :, kep1) = gp_e(:, :, ks)
    end if
    ! Rigid wall on the axis.
    if (l_firstnode2) then
      do k = ksg, keg
        do i = isg_e, ieg_e
          gp_e(i, jsm1, k) = gp_e(i, js, k)
        end do
      end do
    end if
    if (l_lastnode2) then
      do k = ksg, keg
        do i = isg_e, ieg_e
          gp_e(i, jep1, k) = gp_e(i, je, k)
        end do
      end do
    end if

    ! Reset omega (3D)
    do k = kmin, kmax
      do j = jmin, jmax
        do i = imin, imax
          omega(i, j, k) = one/(one - haf*rjac2(i, j))
        end do
      end do
    end do
    !call writedata(rjac2,routinename//'_rjac2')
    !call writedata(omega,routinename//'_omega_init')

    do n = 1, maximum_sor_iterations
      if (debug >= 5 .or. debug >= 1 .and. n == 1) then
        write (message, *) routinename, ' sor iteration ', n
        call threeoutput(routinename, message)
      end if

      relative_worst = zro
      ! isw=1+mod(node1*inac,2)
      !isw=1+mod(node1*inac+node2*jnac+node3*knac,2)
      do ipass = 1, 2
        do k = kmin, kmax
          do i = imin, imax

            !k even: mod(k+node3*knac,2)==0
            !i even: mod(i+node1*inac,2)==0

            !jsw: chosen so that for ipass=1
            !the sum of i,j,k (on the global grid) is odd
            ! that is, the sum of i,j,k,ipass is even
            ! enough for that that the sum of jstart,i,k,ipass
            ! is even with jstart=jmin+jsw
            ! that is, jmin+i+k+ipass+jsw must be even: therefore,
            ! jsw=modulo(jmin+i+k+ipass,2)

            ! modulo, and not mod, in case the array bounds are negative

            jsw = modulo(jmin + node2*jnac &
                         + i + node1*inac &
                         + k + node3*knac &
                         + ipass, 2)

            do j = jmin + jsw, jmax, 2
              resid = coeff_iplus(i, j)*gp_e(i + 1, j, k) &
                      + coeff_iminus(i, j)*gp_e(i - 1, j, k) &
                      + coeff_jplus(i, j)*gp_e(i, j + 1, k) &
                      + coeff_jminus(i, j)*gp_e(i, j - 1, k) &
                      + coeff_k(i, j)*gp_e(i, j, k - 1) &
                      + coeff_k(i, j)*gp_e(i, j, k + 1) &
                      + coeff_diagonal(i, j)*gp_e(i, j, k) &
                      - four*pi*gnewton*d_e(i, j, k)
              !  -four*pi*gnewton*dvl1a(i)*dvl2a(j)*d_(i,j,k)

              resid = resid/coeff_diagonal(i, j)
              relative_worst = &
                max(relative_worst, abs(resid/gp_e(i, j, k)))

              gp_e(i, j, k) = gp_e(i, j, k) - omega(i, j, k)*resid

              ! Chebyshev acceleration (done as in dzeus35 - 3D now)
              omega(i, j, k) = one/(one - 0.25*rjac2(i, j)*omega(i, j, k))
              ! REALLY reset it (for debug purposes)
              ! omega(i,j)=one

            end do
          end do
        end do

#ifdef MPI
        if (debug >= 5) then
          ! MESSAGE PASSING OF gp_e
          write (message, *) routinename, n, 'entering mpifield_e'
          call threeoutput(routinename, message)

          write (message, *) routinename, n, 'lbounds gp_e: ', lbound(gp_e)
          call threeoutput(routinename, message)
          write (message, *) routinename, n, 'ubounds gp_e: ', ubound(gp_e)
          call threeoutput(routinename, message)

          write (message, *) routinename, n, 'lbounds gp  : ', lbound(gp)
          call threeoutput(routinename, message)
          write (message, *) routinename, n, 'ubounds gp  : ', ubound(gp)
          call threeoutput(routinename, message)
        end if

        call mpifield_e(gp_e)
#if 1
        if (nnodes3 == 1) then
          !Adjust periodic BC in direction phi
          gp_e(:, :, ksm1) = gp_e(:, :, ke)
          gp_e(:, :, kep1) = gp_e(:, :, ks)
        end if
#endif /* 1 */

        if (debug >= 5) then
          write (message, *) routinename, n, ' exiting mpifield_e'
          call threeoutput(routinename, message)
        end if
#endif /* MPI */
      end do

      if (debug >= 5) then
        write (message, '(i4.4)') n
        call writedata(gp_e, routinename//'_gpb_'//trim(message), 5)
        call writedata(omega, routinename//'_omega_'//trim(message), 5)
      end if

      if (nnodes3 == 1) then
        !Adjust periodic BC in direction phi
        gp_e(:, :, ksm1) = gp_e(:, :, ke)
        gp_e(:, :, kep1) = gp_e(:, :, ks)
      end if

#ifdef MPI
      call MPI_ALLREDUCE(relative_worst, accumulator, 1, &
                         MPI_FLOATMPI, MPI_MAX, MPI_COMM_WORLD, ierr)
      relative_worst = accumulator
#endif /* MPI */

      ! Code to check convergence.  Return to caller in case of
      ! success.  Keep running in case it still needs to work.
      ! Have something prepared for the case of failure
      ! to converge.

      !if (iotty /= NO_OUTPUT) write(iotty,*) &
      !     n, maxval(omega(imin:imax,jmin:jmax)), &
      !     minval(omega(imin:imax,jmin:jmax)), &
      !     maxval(gp(imin:imax,jmin:jmax,k)), &
      !     minval(gp(imin:imax,jmin:jmax,k)), &
      !     relative_worst
      !if (iolog /= NO_OUTPUT) write(iolog,*) &
      !     n, maxval(omega(imin:imax,jmin:jmax)), &
      !     minval(omega(imin:imax,jmin:jmax)), &
      !     maxval(gp(imin:imax,jmin:jmax,k)), &
      !     minval(gp(imin:imax,jmin:jmax,k)), &
      !     relative_worst

      if (debug >= 5 .or. debug >= sordebug .and. n == 1) then
        write (message, *) 'it ', n, relative_worst, ntimestep
        call threeoutput(routinename, message)
      end if

      if (debug >= 5) then
        write (message, '(i4.4)') n
        call writedata(relative_worst, routinename//'_rw_'//trim(message), 5)
        call writedata(gp_e, routinename//'_gpe_'//trim(message), 5)
      end if

      if (relative_worst <= graverr) then
        exit
      end if

    end do

    if (debug >= sordebug) then
      write (message, *) 'SOR', n, relative_worst, ntimestep
      call threeoutput(routinename, message)
    end if

    call writedata(gp_e, routinename//'_gp_file', 4)
    call writedata(d_e, routinename//'_rho_file', 4)

    if (relative_worst > graverr) then
      call finalstop(routinename, &
                     'Unhappy exit.  Gravity could not converge.')
    end if

#if 0
    ! Adjust axial BCs for continuity
    if (l_firstnode2) then
      do k = ksg, keg
        kotherside = k + knac/2
        if (kotherside > kep1) kotherside = kotherside - knac
        if (kotherside < ksm1) kotherside = kotherside + knac
        do i = isg_e, ieg_e
          gp_e(i, jsm1, k) = gp_e(i, js, kotherside)
        end do
      end do
    end if
    if (l_lastnode2) then
      do k = ksg, keg
        kotherside = k + knac/2
        if (kotherside > kep1) kotherside = kotherside - knac
        if (kotherside < ksm1) kotherside = kotherside + knac
        do i = isg_e, ieg_e
          gp_e(i, jep1, k) = gp_e(i, je, kotherside)
        end do
      end do
    end if
#endif /* 0 */

    ! Rigid wall on the axis.
    if (l_firstnode2) then
      do k = ksg, keg
        do i = isg_e, ieg_e
          gp_e(i, jsm1, k) = gp_e(i, js, k)
        end do
      end do
    end if
    if (l_lastnode2) then
      do k = ksg, keg
        do i = isg_e, ieg_e
          gp_e(i, jep1, k) = gp_e(i, je, k)
        end do
      end do
    end if

    call writedata(gp_e, routinename//'_gp_file_bc', 4)

    if (withextendedgrid) then
      if (dx1b(is) == dx1b_e(is)) then
        do k = ksm1, kep1
          do j = jsm1, jep1
            do i = ism1, iep1
              gp(i, j, k) = gp_e(i, j, k)
            end do
          end do
        end do
      else
        do k = ksm1, kep1
          do j = jsm1, jep1
            do i = is, iep1
              gp(i, j, k) = gp_e(i, j, k)
            end do
          end do
        end do

        gpadjust1 = dx1b(is)*dx1bi_e(is)
        gpadjust2 = one - gpadjust1
        do k = ksm1, kep1
          do j = jsm1, jep1
            gp(ism1, j, k) = gp_e(ism1, j, k)*gpadjust1 &
                             + gp_e(is, j, k)*gpadjust2
          end do
        end do
      end if
    end if

    call writedata(routinename//'_end', 4)

  END SUBROUTINE gravity_sor_3D
  !============================================================
  SUBROUTINE gp_boundary_3D

    if (max(lmax_lower, lmax_upper) >= 0) call gp_boundary_3D_multipole
    if (min(lmax_lower, lmax_upper) < 0) call gp_boundary_3D_direct_sum

  END SUBROUTINE gp_boundary_3D
  !============================================================
  SUBROUTINE gp_boundary_3D_direct_sum
#ifdef MPI
    USE mpi
    USE zeus_mpi
    INTEGER :: ierr
    !INTEGER :: n1, n2, n3, size1, size2
    !REALNUM, SAVE, ALLOCATABLE, DIMENSION(:,:) :: receivesum
    REALNUM, SAVE, ALLOCATABLE, DIMENSION(:) :: localsumpack
    REALNUM, SAVE, ALLOCATABLE, DIMENSION(:, :) :: completesum
    REALNUM, SAVE, ALLOCATABLE, DIMENSION(:) :: completesumpack
    !INTEGER, SAVE, ALLOCATABLE, DIMENSION(:,:) :: receivecounts
#endif /* MPI */

    CHARACTER(LEN=*), PARAMETER :: routinename = 'z_gp_boundary_3D_direct_sum'
    CHARACTER(LEN=120) :: message

    !REALNUM, SAVE, ALLOCATABLE, DIMENSION(:,:,:,:,:,:) :: term
    REALNUM, SAVE, ALLOCATABLE, DIMENSION(:, :) :: localsum

    INTEGER :: i, j, iprime, jprime
    INTEGER :: k, kprime
    INTEGER :: jk
    INTEGER :: index
    INTEGER, SAVE :: indexfinal
    INTEGER, PARAMETER :: indexmax = 2
    INTEGER, SAVE :: ivalues(indexmax)
    !REALNUM :: kfactor,kmodulus_square,elliptick,m1
    !REALNUM :: cylrprime, cylr, zprime, z
    REALNUM :: rprime, r
    !REALNUM :: x,y,xprime,yprime
    LOGICAL, SAVE :: firstcall = .true.

    REALNUM :: cosphidifference
    REALNUM :: costhetaprime, sinthetaprime
    REALNUM :: distance, cosalpha
    REALNUM :: costheta, sintheta
    REALNUM :: dvol2, dvol3

    if (firstcall) then
      ! Reject non-implemented geometries
      if (knac == 0 .or. geometry /= 'rtp') then
        call finalstop(routinename, &
                       'Only for spherical 3D gravity BC.')
      end if
      if (nnodes3 /= 1) then
        call finalstop(routinename, 'Only for nnodes3=1.')
      end if

      ! Lower and upper radius, global index
      ivalues = isa - 1  ! Default values out of range

      indexfinal = 0
      if (lmax_lower < 0) then
        indexfinal = indexfinal + 1
        ivalues(indexfinal) = ism1p
      end if
      if (lmax_upper < 0 .and. iep1p > ism1p) then
        indexfinal = indexfinal + 1
        ivalues(indexfinal) = iep1p
      end if

      if (debug >= 1) then
        write (message, *) 'ivalues', node, ivalues
        call threeoutput(routinename, message)

        write (message, *) 'indexfinal', indexfinal
        call threeoutput(routinename, message)
      end if

      ALLOCATE (localsum(jsp:jep, ksp:kep))

#ifdef MPI
      ALLOCATE (completesum(jsp:jep, ksp:kep))
      ALLOCATE (localsumpack((jep - jsp + 1)*(kep - ksp + 1)))
      ALLOCATE (completesumpack((jep - jsp + 1)*(kep - ksp + 1)))

      !ALLOCATE(receivesum(js:je,ks:ke))
      !ALLOCATE(receivecounts(0:nnodes1*nnodes2*nnodes3-1, indexfinal))
#endif /* MPI */

      !   !PROBABLY INCORRECT FOR 3D IN GENERAL
      !   ! CURRENT VERSION INTENDED ONLY for nnodes3=1
      !   do index=1,indexfinal
      !     do n3=0,nnodes3-1
      !       do n2=0,nnodes2-1
      !         if (n2==nnodes2-1) then
      !           ! Last processor may be short.  No consideration
      !           !   for redundant processors is given.
      !           size2=(jep-jsp+1)-(nnodes2-1)*jnac
      !         else
      !           size2=jnac
      !         end if
      !         do n1=0,nnodes1-1
      !           size1=0
      !           if (index==1 .and. n1==0) size1=1
      !           if (index>1 .and. n1==nnodes1-1) size1=1
      !           receivecounts(nodefun(n1,n2,n3),index) = &
      !               size1*size2*(kep-ksp+1)
      !         end do
      !       end do
      !     end do
      !     !write(message,*) 'ind', index, 'cts', receivecounts
      !     !call threeoutput(routinename,message)
      !     !write(*,*) 'inside boundary', &
      !     !    nodename, index, 'counts ', receivecounts(:,index)
      !  end do

      firstcall = .false.
    end if

    ! Compute local sum
    do index = 1, indexfinal
      i = ivalues(index)
      r = x1bp(i)

      do k = ksp, kep
        do j = jsp, jep
          costheta = cos(x2bp(j))
          sintheta = sin(x2bp(j))

          localsum(j, k) = zro

          do kprime = ks, ke
            cosphidifference = cos(x3b(kprime) - x3bp(k))
            dvol3 = dvl3a(kprime)
            do jprime = js, je

              costhetaprime = dg32bd2(jprime) !cos(x2b(jprime))
              sinthetaprime = g32b(jprime)    !sin(x2b(jprime))
              ! Hoping that fetching may be faster than calculating.
              ! Notice that this uses the arrays for a non-intended
              ! purpose, and that D. Clarke's staggering of
              ! dg32bd2 differs from ZeusMP

              cosalpha = costheta*costhetaprime &
                         + sintheta*sinthetaprime*cosphidifference

              dvol2 = dvl2a(jprime)
              do iprime = is, ie
                rprime = x1b(iprime)

                distance = sqrt( &
                           r**2 + rprime**2 &
                           - two*r*rprime*cosalpha)

                localsum(j, k) = localsum(j, k) &
                                 + d_(iprime, jprime, kprime) &
                                 *gnewton/(distance + verysmall) &
                                 *dvl1a(iprime)*dvol2*dvol3
              end do
            end do
          end do
        end do
      end do

#ifdef MPI
      !write(*,*) 'before reduce-scatter', index, ' ', nodename

      if (debug >= 3) then
        write (message, *) 'b', index, node
        call threeoutput(routinename, message)
      end if

      jk = 1
      do k = ksp, kep
        do j = jsp, jep
          localsumpack(jk) = localsum(j, k)
          jk = jk + 1
        end do
      end do

      call MPI_ALLREDUCE(localsumpack, completesumpack, size(localsum), &
                         MPI_FLOATMPI, MPI_SUM, MPI_COMM_WORLD, ierr)

      jk = 1
      do k = ksp, kep
        do j = jsp, jep
          completesum(j, k) = completesumpack(jk)
          jk = jk + 1
        end do
      end do

      ! Scatter results to the relevant processors
      !call MPI_REDUCE_SCATTER(localsum,receivesum, &
      !    receivecounts(:,index), &
      !    MPI_FLOATMPI, MPI_SUM, MPI_COMM_WORLD, ierr)

      if (debug >= 3) then
        write (message, *) 'a', index, ks, ke, knac, node3
        call threeoutput(routinename, message)
      end if

      !write(*,*) 'after reduce-scatter', &
      !    index, ivalues_local, ' ', nodename

      ! Store in gp_e

      if (debug >= 4) then
        if (index == 1) then
          call writedata(completesum, routinename//'_completesum_1', 4)
          call writedata(completesumpack, routinename//'_completesumpack_1', 4)
          call writedata(localsumpack, routinename//'_gp_localsumpack_1', 4)
        end if

        if (index == 2) then
          call writedata(completesum, routinename//'_completesum_2', 4)
          call writedata(completesumpack, routinename//'_completesumpack_2', 4)
          call writedata(localsumpack, routinename//'_gp_localsumpack_2', 4)
        end if
      end if

      if (index == 1 .and. l_firstnode1) then
        i = ism1
        !gp(i,js:je,ks:ke)=completesum(js+jnac*node2:je+jnac*node2,ks+knac*node3:ke+knac*node3)
        !call writedata(gp,'gp_bc_1',4)
        gp_e(i, js:je, ks:ke) = completesum(js + jnac*node2:je + jnac*node2, &
                                            ks + knac*node3:ke + knac*node3)
        call writedata(gp_e, 'z_gp_e_bc_1', 4)
      end if
      if (index == 2 .and. l_lastnode1) then
        i = iep1
        !gp(i,js:je,ks:ke)=completesum(js+jnac*node2:je+jnac*node2,ks+knac*node3:ke+knac*node3)
        !call writedata(gp,'gp_bc_2',4)
        gp_e(i, js:je, ks:ke) = completesum(js + jnac*node2:je + jnac*node2, &
                                            ks + knac*node3:ke + knac*node3)
        call writedata(gp_e, 'z_gp_e_bc_2', 4)
      end if

      !    if (receivecounts(node,index)>0) then
      !      !write(*,*) 'before storing', &
      !      !    index, ivalues_local, ' ', nodename
      !
      !      write(message,*) 's', index, ivalues_local
      !      call threeoutput(routinename,message)
      !
      !
      !      i=ivalues_local(index)
      !      gp(i,js:je,ks:ke)=receivesum
      !
      !      write(message,*) 'f', index
      !      call threeoutput(routinename,message)!
      !
      !      !write(*,*) 'after storing', index, ' ', nodename
      !    end if

#else /* MPI */
      ! Just store in gp
      !write(*,*) 'before storing', &
      !    index, ivalues_local, ' ', nodename
      i = ivalues(index)
      !gp(i,js:je,ks:ke)=localsum
      gp_e(i, js:je, ks:ke) = localsum

      !write(*,*) 'after storing', index, ' ', nodename
#endif /* MPI */

      if (debug >= 4) then
        write (message, *) 'l', index
        call threeoutput(routinename, message)

        if (index == 1) then
          call writedata(localsum, 'z_gp_localsum_1', 4)
          !call writedata(receivesum,'z_gp_receivesum_1')
        else
          call writedata(localsum, 'z_gp_localsum_2', 4)
          !call writedata(receivesum,'z_gp_receivesum_2')
        end if
      end if

      !write(*,*) 'end of second loop', index, ' ', nodename

    end do

    if (debug >= 4) then
      write (message, *) 'e', index
      call threeoutput(routinename, message)

      call writedata(gp_e, routinename//'_gp_bc', 4)

      write (message, *) 'shape localsum', shape(localsum)
      call threeoutput(routinename, message)

      !write(message,*) 'shape receivesum', shape(receivesum)
      !call threeoutput(routinename,message)

      !write(*,*) 'end of gp_boundary', index, ' ', nodename
    end if

  END SUBROUTINE gp_boundary_3D_direct_sum
  !============================================================
  SUBROUTINE gp_boundary_3D_multipole
    CHARACTER(LEN=*), PARAMETER :: routinename = 'z_gp_boundary_3D_multipole'
    LOGICAL, SAVE :: firstcall = .true.

    if (firstcall) then

      ! Possible optimization:
      ! Allocate, calculate, and store useful factors, such as
      ! cos(phi), sin(phi), and the result of multiplying them by dphi.

      ! Allocate, calculate, and store associated Legendre functions.
      ! Semi-normalized in the "geophysical convention" (Schmidt, or
      ! Racah), and excluding the Condon-Shortley phase factor.

      lmax = max(lmax_lower, lmax_upper)
      if (lmax < 0) call finalstop(routinename, 'Negative lmax.')

      allocate (plm_s(0:lmax, 0:lmax, js:je))
      call plm_calculate

      ! Allocate the density multipole arrays
      if (lmax_lower >= 0) then
        allocate (qlm_lower_cosine(((lmax_lower + 1)*(lmax_lower + 2))/2))
        allocate (qlm_lower_sine((lmax_lower*(lmax_lower + 1))/2))
        if (nnodes1*nnodes2*nnodes3 > 1) then
          allocate (qlm_lower_cosine_local(((lmax_lower + 1) &
                                            *(lmax_lower + 2))/2))
          allocate (qlm_lower_sine_local((lmax_lower*(lmax_lower + 1))/2))
        else
          qlm_lower_cosine_local => qlm_lower_cosine
          qlm_lower_sine_local => qlm_lower_sine
        end if
      end if
      if (lmax_upper >= 0) then
        allocate (qlm_upper_cosine(((lmax_upper + 1)*(lmax_upper + 2))/2))
        allocate (qlm_upper_sine((lmax_upper*(lmax_upper + 1))/2))
        if (nnodes1*nnodes2*nnodes3 > 1) then
          allocate (qlm_upper_cosine_local(((lmax_upper + 1) &
                                            *(lmax_upper + 2))/2))
          allocate (qlm_upper_sine_local((lmax_upper*(lmax_upper + 1))/2))
        else
          qlm_upper_cosine_local => qlm_upper_cosine
          qlm_upper_sine_local => qlm_upper_sine
        end if
      end if

      firstcall = .false.
    end if

    ! Calculate multipoles
    call density_multipoles

    ! Calculate potential
    call potential_multipoles

  END SUBROUTINE gp_boundary_3D_multipole
  !============================================================
  SUBROUTINE plm_calculate
    CHARACTER(LEN=*), PARAMETER :: routinename = 'z_plm_calculate'
    ! No attempt to improve the overflow and underflow errors is made;
    ! in particular, there is no rescaling.

    INTEGER ell, m
    INTEGER j
    REALNUM costheta, sintheta

    plm_s = zro

    do j = js, je
      costheta = cos(x2b(j))
      sintheta = sin(x2b(j))
      do m = 0, lmax

        ell = m
        select case (m)
        case (0)
          plm_s(ell, m, j) = one
        case (1)
          plm_s(ell, m, j) = sintheta
        case default
          plm_s(ell, m, j) = plm_s(m - 1, m - 1, j)*sintheta*sqrt(one - haf/m)
        end select

        if (m >= lmax) exit
        ell = m + 1
        plm_s(ell, m, j) = &
          (two*ell - one)*costheta*plm_s(ell - 1, m, j) &
          /sqrt(real((ell - m)*(ell + m), pk))

        do ell = m + 2, lmax
          plm_s(ell, m, j) = ((two*ell - one)*costheta*plm_s(ell - 1, m, j) &
                              - sqrt(real((ell - 1 - m)*(ell - 1 + m), pk)) &
                              *plm_s(ell - 2, m, j)) &
                             /sqrt(real((ell - m)*(ell + m), pk))
        end do
      end do
    end do

    call writedata(plm_s, routinename//'_plm_s', 4)

  END SUBROUTINE plm_calculate
  !============================================================
  SUBROUTINE density_multipoles
#ifdef MPI
    USE mpi
    integer ierr

#endif /* MPI */

    CHARACTER(LEN=*), PARAMETER :: routinename = 'z_density_multipoles'
    INTEGER i, j, k
    INTEGER ell, m
    REALNUM r, dm, q1, phi
    REALNUM mass

    INTEGER lm

    if (lmax_lower >= 0) then
      qlm_lower_cosine_local = zro
      qlm_lower_sine_local = zro
    end if

    if (lmax_upper >= 0) then
      qlm_upper_cosine_local = zro
      qlm_upper_sine_local = zro
    end if

    mass = zro
    do k = ks, ke
      phi = x3b(k)
      do j = js, je
        do i = is, ie
          dm = dvl1a(i)*dvl2a(j)*dvl3a(k)*d_(i, j, k)
          r = x1b(i)
          mass = mass + dm

          q1 = dm
          do ell = 0, lmax_lower
            q1 = q1/r ! integrand w/o the Y function
            do m = 0, ell
              lm = (ell*(ell + 1))/2 + m + 1
              qlm_lower_cosine_local(lm) = qlm_lower_cosine_local(lm) + &
                                           plm_s(ell, m, j)*cos(m*phi)*q1
            end do
            do m = 1, ell
              lm = (ell*(ell - 1))/2 + m
              qlm_lower_sine_local(lm) = qlm_lower_sine_local(lm) + &
                                         plm_s(ell, m, j)*sin(m*phi)*q1
            end do
          end do

          q1 = dm
          do ell = 0, lmax_upper
            do m = 0, ell
              lm = (ell*(ell + 1))/2 + m + 1
              qlm_upper_cosine_local(lm) = qlm_upper_cosine_local(lm) + &
                                           plm_s(ell, m, j)*cos(m*phi)*q1
            end do
            do m = 1, ell
              lm = (ell*(ell - 1))/2 + m
              qlm_upper_sine_local(lm) = qlm_upper_sine_local(lm) + &
                                         plm_s(ell, m, j)*sin(m*phi)*q1
            end do
            q1 = q1*r ! integrand w/o the Y function
          end do
        end do
      end do
    end do

    if (lmax_upper >= 0) then
      call writedata(qlm_upper_cosine_local, 'z_qlm_upper_cosine_local', 4)
    end if

#ifdef MPI
    if (nnodes1*nnodes2*nnodes3 > 1) then
      if (lmax_lower >= 0) then
        call MPI_ALLREDUCE(qlm_lower_cosine_local, qlm_lower_cosine, &
                           size(qlm_lower_cosine), MPI_FLOATMPI, MPI_SUM, &
                           MPI_COMM_WORLD, ierr)
        call MPI_ALLREDUCE(qlm_lower_sine_local, qlm_lower_sine, &
                           size(qlm_lower_sine), MPI_FLOATMPI, MPI_SUM, &
                           MPI_COMM_WORLD, ierr)
      end if
      if (lmax_upper >= 0) then
        call MPI_ALLREDUCE(qlm_upper_cosine_local, qlm_upper_cosine, &
                           size(qlm_upper_cosine), MPI_FLOATMPI, MPI_SUM, &
                           MPI_COMM_WORLD, ierr)
        call MPI_ALLREDUCE(qlm_upper_sine_local, qlm_upper_sine, &
                           size(qlm_upper_sine), MPI_FLOATMPI, MPI_SUM, &
                           MPI_COMM_WORLD, ierr)
      end if
    end if
#endif /* MPI */

    if (lmax_upper >= 0) then
      call writedata(qlm_upper_cosine, 'z_qlm_upper_cosine', 4)
    end if

    call writedata(mass, 'z_mass', 4)

  END SUBROUTINE density_multipoles
  !============================================================
  SUBROUTINE potential_multipoles
    CHARACTER(LEN=*), PARAMETER :: routinename = 'z_potential_multipoles'
    CHARACTER(LEN=120) :: message
    INTEGER i, j, k
    INTEGER ell, m
    INTEGER lm
    REALNUM phi, r
    REALNUM q1

    ! Think the edge BC situation carefully...
    ! Also think about the necessary grid extension.

    if (lmax_lower >= 0 .and. l_firstnode1) then
      i = isg_e
      if (debug >= 4) then
        write (message, *) 'lower multipole', i
        call threeoutput(routinename, message)
      end if

      r = x1b_e(i)

      do k = ks, ke ! Consider ksm1 and kep1 if needed
        phi = x3b(k)
        do j = js, je ! Consider jsm1 and jep1 if needed
          gp_e(i, j, k) = zro

          q1 = gnewton
          do ell = 0, lmax_lower
            do m = 0, ell
              lm = (ell*(ell + 1))/2 + m + 1
              gp_e(i, j, k) = gp_e(i, j, k) + plm_s(ell, m, j) &
                              *qlm_lower_cosine(lm)*cos(m*phi)*q1
            end do
            do m = 1, ell
              lm = (ell*(ell - 1))/2 + m
              gp_e(i, j, k) = gp_e(i, j, k) + plm_s(ell, m, j) &
                              *qlm_lower_sine(lm)*sin(m*phi)*q1
            end do
            q1 = q1*r
          end do

        end do
      end do
    end if

    if (lmax_upper >= 0 .and. l_lastnode1) then
      i = ieg_e
      if (debug >= 4) then
        write (message, *) 'upper multipole', i
        call threeoutput(routinename, message)
      end if

      r = x1b_e(i)

      do k = ks, ke ! Consider ksm1 and kep1 if needed
        phi = x3b(k)
        do j = js, je ! Consider jsm1 and jep1 if needed
          gp_e(i, j, k) = zro

          q1 = gnewton
          do ell = 0, lmax_upper
            q1 = q1/r
            do m = 0, ell
              lm = (ell*(ell + 1))/2 + m + 1
              gp_e(i, j, k) = gp_e(i, j, k) + plm_s(ell, m, j) &
                              *qlm_upper_cosine(lm)*cos(m*phi)*q1
            end do
            do m = 1, ell
              lm = (ell*(ell - 1))/2 + m
              gp_e(i, j, k) = gp_e(i, j, k) + &
                              plm_s(ell, m, j)*qlm_upper_sine(lm)*sin(m*phi)*q1
            end do
          end do

        end do
      end do
    end if

    call writedata(gp_e, 'z_gp_multipole', 4)

  END SUBROUTINE potential_multipoles
  !============================================================
  SUBROUTINE gravity_monopole
#ifdef MPI
    USE mpi
    integer ierr
    integer color, key
#endif /* MPI */
    CHARACTER(LEN=*), PARAMETER :: routinename = 'z_gravity_monopole'
    integer i, j, k
    LOGICAL, SAVE :: firstcall = .true.

    REALNUM, SAVE, ALLOCATABLE, DIMENSION(:), TARGET :: &
      shellmass
    REALNUM, SAVE, ALLOCATABLE, DIMENSION(:) :: &
      enclosedmass, topmasses, cumulative
    REALNUM :: innermass
    REALNUM :: deltav

    REALNUM, SAVE, POINTER, DIMENSION(:) :: &
      shellmass_local
    INTEGER, SAVE :: shellcomm, raycomm

    INTEGER, PARAMETER :: monodebug = 5

    if (firstcall) then
      if (gravity_algorithm /= 'monopole') then
        call finalstop(routinename, 'Only for monopole gravity algorithm.')
      end if

      ! Allocate arrays
      allocate (shellmass(is:ie))
      allocate (enclosedmass(is:ie + 1))

#ifdef MPI
      allocate (shellmass_local(is:ie))
      !if (node==rootnode) then
      allocate (topmasses(0:nnodes1 - 1))
      allocate (cumulative(0:nnodes1))
      !end if
#else /* MPI */
      shellmass_local => shellmass
      ! enclosedmass_local => enclosedmass
#endif /* MPI */

#ifdef MPI
      ! Define groups of processors
      color = node1
      key = node
      call mpi_comm_split(MPI_COMM_WORLD, color, key, shellcomm, ierr)

      if (node2 == 0 .and. node3 == 0) then
        color = 1
      else
        color = MPI_UNDEFINED
      end if
      call mpi_comm_split(MPI_COMM_WORLD, color, key, raycomm, ierr)
#endif /* MPI */

      firstcall = .false.
    end if

    shellmass_local = zro
    do k = ks, ke
      do j = js, je
        do i = is, ie
          shellmass_local(i) = shellmass_local(i) + &
                               d_(i, j, k)*dvl1a(i)*dvl2a(j)*dvl3a(k)
        end do
      end do
    end do

    call writedata(shellmass_local, routinename//'_shellmass_l', monodebug)
    call writedata(dvl1a, routinename//'_dvl1a', monodebug)
    call writedata(dvl2a, routinename//'_dvl2a', monodebug)
    call writedata(dvl3a, routinename//'_dvl3a', monodebug)
    call writedata(d_, routinename//'_d_', monodebug)

#ifdef MPI
    call MPI_REDUCE(shellmass_local, shellmass, ie - is + 1, &
                    MPI_FLOATMPI, MPI_SUM, 0, shellcomm, ierr)

    call writedata(shellmass, routinename//'_shellmass', monodebug)
#endif /* MPI */

    if (node2 == 0 .and. node3 == 0) then
      if (nnodes1 > 1) then
        ! Do the local sum of enclosed mass
        enclosedmass(is) = zro
        do i = is, ie
          enclosedmass(i + 1) = enclosedmass(i) + shellmass(i)
        end do

        call writedata(enclosedmass, routinename//'_enclosedmass_1', monodebug)

#ifdef MPI
        ! Gather all the values of enclosedmass(ie+1) from raycomm
        call MPI_GATHER(enclosedmass(ie + 1), 1, MPI_FLOATMPI, &
                        topmasses, 1, MPI_FLOATMPI, &
                        0, raycomm, ierr)

        ! Find their cumulative sum, adding in the central mass to start
        if (node1 == 0) then
          cumulative(0) = centralmass
          do i = 1, nnodes1
            cumulative(i) = cumulative(i - 1) + topmasses(i - 1)
          end do

          call writedata(centralmass, routinename//'_centralmass', monodebug)
          call writedata(topmasses, routinename//'_topmasses', monodebug)
          call writedata(cumulative, routinename//'_cumulative', monodebug)

        end if

        ! Scatter the resulting cumulative sum to other members of raycomm
        call MPI_SCATTER(cumulative, 1, MPI_FLOATMPI, &
                         innermass, 1, MPI_FLOATMPI, &
                         0, raycomm, ierr)

        ! Adjust the enclosed mass array
        do i = is, ie + 1
          enclosedmass(i) = innermass + enclosedmass(i)
        end do

        call writedata(enclosedmass, routinename//'_enclosedmass_2', monodebug)

#endif /* MPI */
      else
        ! Do the total sum of enclosed mass, including the central mass
        enclosedmass(is) = centralmass
        do i = is, ie
          enclosedmass(i + 1) = enclosedmass(i) + shellmass(i)
        end do

        call writedata(enclosedmass, routinename//'_enclosedmass', monodebug)

      end if
    end if
#ifdef MPI
    if (nnodes2*nnodes3 > 1) then
      ! Propagate enclosedmass from the members of raycomm to all processors
      call MPI_BCAST(enclosedmass, ie - is + 2, MPI_FLOATMPI, &
                     0, shellcomm, ierr)

      call writedata(enclosedmass, routinename//'_enclosedmass_3', monodebug)
    end if
#endif /* MPI */

    ! Apply gravity force, including the central mass
    do i = is, ie
      deltav = -dt*gnewton*enclosedmass(i)*x1ai(i)**2
      do k = ks, ke
        do j = js, je
          v1(i, j, k) = v1(i, j, k) + deltav
        end do
      end do
    end do

    call writedata(v1, routinename//'_v1_', monodebug)

  END SUBROUTINE gravity_monopole
  !============================================================
  SUBROUTINE centralmassforce
    CHARACTER(LEN=*), PARAMETER :: routinename = 'z_centralmassforce'
    INTEGER i, j, k
    REALNUM factor, deltav
    factor = -dt*gnewton*centralmass
    do i = is, ie
      deltav = factor*x1ai(i)**2
      do k = ks, ke
        do j = js, je
          v1(i, j, k) = v1(i, j, k) + deltav
        end do
      end do
    end do
    call writedata(v1, routinename//'_v1_', 4)
  END SUBROUTINE centralmassforce
END MODULE zeus_gravity
