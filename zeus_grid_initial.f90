#include "macros"
MODULE zeus_grid_initial
  USE zeus_variables
  USE zeus_io
  IMPLICIT NONE
  PRIVATE
  PUBLIC :: initialize_grid, get_array_size
  SAVE

  !
  ! Local namelist variables
  !
  ! The absurd default values are on purpose.
  ! The reasonable ones too.
  namelist_variable(ggen, CHARACTER(LEN=2), gridname, '00')
  namelist_variable(ggen, INTEGER, nbl, -1)
  namelist_variable(ggen, INTEGER, igrid, 999)
  namelist_variable(ggen, REALNUM, xmin, zro)
  namelist_variable(ggen, REALNUM, xmax, zro)
  namelist_variable(ggen, REALNUM, xrat, one)
  namelist_variable(ggen, REALNUM, dxmin, zro)
  namelist_variable(ggen, CHARACTER(LEN=2), units, 'rd')
  namelist_variable(ggen, CHARACTER(LEN=filenamelength), gridfile, 'o_x1ap')
  namelist_variable(ggen, INTEGER, griditer, 20)
  namelist_variable(ggen, REALNUM, griderror, millionth)

CONTAINS

!** GROUP: GRID INITIALIZATION *****************************************
!**                                                                   **
!**     G R O U P :   G R I D   I N I T I A L I Z A T I O N           **
!**                                                                   **
!***********************************************************************

  !
  !======================================================================
  !
  SUBROUTINE initialize_grid
    call gridx1
    call gridx2
    call gridx3
    call gsetx1
    call gsetx2
    call gsetx3
    call grid_e

    ijknea = ijknsa + max(iea - isa, jea - jsa, kea - ksa)

    call show_grid

  END SUBROUTINE initialize_grid
  !
  !======================================================================
  !
  SUBROUTINE show_grid
    CHARACTER(LEN=*), PARAMETER :: routinename = 'z_show_grid'
    CHARACTER(LEN=messagelinelength) message
    ! Produce output of the main and extended grids.
    if (node == rootnode) then
      if (inac > 0) then
        call writedata(x1ap, 'z_x1ap', SINGLEFILE=.true.)
        call writedata(x1bp, 'z_x1bp', SINGLEFILE=.true.)
        call writedata(x1apl, 'z_x1apl', SINGLEFILE=.true.)
      end if
      call writedata(ieap - isap + 1, 'z_x1_size', SINGLEFILE=.true.)
      call writedata(iea - isa + 1, 'z_x1_tilesize', SINGLEFILE=.true.)
      call writedata(nnodes1, 'z_x1_nodesize', SINGLEFILE=.true.)

      if (jnac > 0) then
        call writedata(x2ap, 'z_x2ap', SINGLEFILE=.true.)
        call writedata(x2bp, 'z_x2bp', SINGLEFILE=.true.)
        call writedata(x2apl, 'z_x2apl', SINGLEFILE=.true.)
      end if
      call writedata(jeap - jsap + 1, 'z_x2_size', SINGLEFILE=.true.)
      call writedata(jea - jsa + 1, 'z_x2_tilesize', SINGLEFILE=.true.)
      call writedata(nnodes2, 'z_x2_nodesize', SINGLEFILE=.true.)

      if (knac > 0) then
        call writedata(x3ap, 'z_x3ap', SINGLEFILE=.true.)
        call writedata(x3bp, 'z_x3bp', SINGLEFILE=.true.)
        call writedata(x3apl, 'z_x3apl', SINGLEFILE=.true.)
      end if
      call writedata(keap - ksap + 1, 'z_x3_size', SINGLEFILE=.true.)
      call writedata(kea - ksa + 1, 'z_x3_tilesize', SINGLEFILE=.true.)
      call writedata(nnodes3, 'z_x3_nodesize', SINGLEFILE=.true.)
    endif

    write (message, *) 'isap, ieap:', isap, ieap
    call threeoutput(routinename, message)
    write (message, *) 'isa, iea:', isa, iea
    call threeoutput(routinename, message)
    write (message, *) 'is, ie:', is, ie
    call threeoutput(routinename, message)
    write (message, *) 'isg, ieg:', isg, ieg
    call threeoutput(routinename, message)

    write (message, *) 'isap_e, ieap_e:', isap_e, ieap_e
    call threeoutput(routinename, message)
    write (message, *) 'isa_e, iea_e:', isa_e, iea_e
    call threeoutput(routinename, message)
    write (message, *) 'is_e, ie_e:', is_e, ie_e
    call threeoutput(routinename, message)
    write (message, *) 'isg_e, ieg_e:', isg_e, ieg_e
    call threeoutput(routinename, message)

    call writedata(x1ap_e, 'z_x1ap_e', SINGLEFILE=.true.)
    call writedata(x1bp_e, 'z_x1bp_e', SINGLEFILE=.true.)

    call writedata(x1a_e, 'z_x1a_e')
    call writedata(x1b_e, 'z_x1b_e')

    call writedata(x1ai_e, 'z_x1ai_e')
    call writedata(x1bi_e, 'z_x1bi_e')

    call writedata(x1ai, 'z_x1ai')
    call writedata(x1bi, 'z_x1bi')

    call writedata(ieap_e - isap_e + 1, 'z_x1_e_size')
    call writedata(iea_e - isa_e + 1, 'z_x1_e_tilesize')

  END SUBROUTINE show_grid
  !
  !======================================================================
  !
  SUBROUTINE defaults_ggen
    gridname = '00'
    nbl = -1
    igrid = 999
    xmin = zro
    xmax = zro
    xrat = one
    dxmin = zro
    units = 'rd'
    gridfile = 'o_x1ap'
    griditer = 20
    griderror = millionth
  END SUBROUTINE defaults_ggen
  !
  !======================================================================
  !
  INTEGER FUNCTION get_array_size(name)
    CHARACTER(LEN=*), PARAMETER :: routinename = 'z_get_array_size'
    CHARACTER(LEN=2), INTENT(IN) :: name
    CHARACTER(LEN=messagelinelength) message
    !  Obtain the number of active zones corresponding to 'name'
    !  in the "ggen" namelists.

    get_array_size = 0
    rewind (ioin)
    do
      call defaults_ggen
      read (ioin, ggen, end=10)
      call lowercase(gridname)
      if (gridname == name) then
        if (nbl <= 0) then
          write (message, '(a,i8,a)') 'Error: nbl= ', nbl, ' <=0'
          call threeoutput(routinename, message)
          write (message, '(a)') 'while trying to generate grid '//name//'.'
          call finalstop(routinename, message)
        end if
        get_array_size = get_array_size + nbl
      end if
    end do

10  continue

  END FUNCTION get_array_size
  !
  !======================================================================
  !
  SUBROUTINE gridx1
    !
    !  Initializes the x1ap-grid as specified by the parameters in
    !  the "ggen" namelists.
    !
    CHARACTER(LEN=20*messagelinelength) messagelong
    CHARACTER(LEN=messagelinelength) message
    CHARACTER(LEN=*), PARAMETER :: routinename = 'z_gridx1'

    INTEGER :: inacp
    INTEGER in, inp
    INTEGER remainder

    ! Obtain the array active size
    ! Store it in the variables inacp, inac and iwnac
    inacp = get_array_size('x1')

    if (inacp == 0 .and. nnodes1 > 1) then
      write (message, '(a,i2,a,i4,a)') &
        'inacp= ', inacp, ' but nnodes1 =', nnodes1, ' >1'
      call threeoutput(routinename, message)
      call finalstop(routinename, &
                     'Only one processor needed for a symmetric direction!')
    end if

    inac = inacp/nnodes1

    if (inacp > 0 .and. inac <= 2*nghosts) then
      call finalstop(routinename, 'Tile size too small in this direction.')
    end if

    remainder = modulo(inacp, nnodes1)
    if (remainder /= 0) then

      ! Commented out because it makes the relation between
      ! local and global coordinates more complex.
      ! (it would invalidate ip=i+node1*inac)
      !
      !if (node1<remainder) then ! coordinate node1 counts from 0.
      !  inac=inac+1 ! Give one more point to the first nodes
      !              ! (0<=node1<remainder)
      !end if

      call finalstop(routinename, &
                     'nnodes1 does not divide the number of 1-zones exactly')

    end if

    iwnac(1) = inac

    ! Calculate array bounds for both local and global arrays
    in = 1
    if (inac > 0) in = inac + 2*nghosts
    iea = isa + in - 1
    inp = 1
    if (inac > 0) inp = inacp + 2*nghosts
    ieap = isap + inp - 1

    ! Allocate primary and secondary 1-grid arrays
    call allocate_gridx1

    ! Report the sizes and bounds found and other useful information
    write (message, *) 'inacp', inacp
    call threeoutput(routinename//'_inacp', message)
    write (message, *) 'isap', isap
    call threeoutput(routinename//'_isap', message)
    write (message, *) 'ieap', ieap
    call threeoutput(routinename//'_ieap', message)

    write (message, *) 'inac', inac
    call threeoutput(routinename//'_inac', message)
    write (message, *) 'isa', isa
    call threeoutput(routinename//'_isa', message)
    write (message, *) 'iea', iea
    call threeoutput(routinename//'_iea', message)

    write (message, *) 'nnodes1', nnodes1
    call threeoutput(routinename//'_nnodes1', message)
    write (message, *) 'node1', node1
    call threeoutput(routinename//'_node1', message)
    !
    !---------------------------------------------------------------------
    !
    if (inacp == 0) then
      !
      ! Set up a token x1-grid with one zone for problems with 1-symmetry
      !
      isp = isap ! lowerbound
      iep = isp
      x1ap(isp) = zro

      return
    end if

    !      Set up a full x1-grid.

    isp = isap + nghosts  ! First active zone set up here
    iep = isp + inacp - 1 ! Same as ieap-nghosts, or isap+nghosts+inacp-1

    call makegrid(x1ap, isap, ieap, isp, iep, 'x1')

    !      Grid in 1-direction is complete.

    ! Construct extended grid
    x1apl(isap:ieap) = x1ap
    ! Uniform assumption
    x1apl(ieap + 1) = two*x1ap(ieap) - x1ap(ieap - 1)

    !
    !  Grid consistency check
    !
    if (geometry == 'rtp' .or. geometry == 'rmp') then
      if ((x1ap(isp - 1) <= zro) .and. (x1ap(isp) /= zro)) then
        write (messagelong, '(a,1es12.5,a)') &
          'GRIDX1: Possibly ill-defined grid: Non-zero x1ap(isp)=', x1ap(isp), &
          ' but x1ap(isp-1)<=0 in RTP or RMP geometry.  Set up a warning.'
        call warnoutput(routinename, messagelong)
      end if
    end if

  END SUBROUTINE gridx1
  !
  !======================================================================
  !
  SUBROUTINE gridx2
    !
    !  Initializes the x2a-grid as specified by the parameters in
    !  the namelist "ggen".
    !
    CHARACTER(LEN=*), PARAMETER :: routinename = 'z_gridx2'
    CHARACTER(LEN=20*messagelinelength) messagelong
    CHARACTER(LEN=messagelinelength) message

    INTEGER :: jnacp
    INTEGER jn, jnp
    INTEGER remainder
    INTEGER j

    ! Obtain the array active size
    ! Store it in the variables jnacp, jnac and iwnac
    jnacp = get_array_size('x2')

    if (jnacp == 0 .and. nnodes2 > 1) then
      write (message, '(a,i2,a,i4,a)') &
        'jnacp= ', jnacp, ' but nnodes2 =', nnodes2, ' >1'
      call threeoutput(routinename, message)
      call finalstop(routinename, &
                     'Only one processor needed for a symmetric direction!')
    end if

    jnac = jnacp/nnodes2

    if (jnacp > 0 .and. jnac <= 2*nghosts) then
      call finalstop(routinename, 'Tile size too small in this direction.')
    end if

    remainder = modulo(jnacp, nnodes2)
    if (remainder /= 0) then

      ! Commented out because it makes the relation between
      ! local and global coordinates more complex.
      ! (it would invalidate jp=j+node2*jnac)
      !
      !if (node2<remainder) then ! coordinate node2 counts from 0.
      !  jnac=jnac+1 ! Give one more point to the first nodes
      !              ! (0<=node2<remainder)
      !end if

      call finalstop(routinename, &
                     'nnodes2 does not divide the number of 2-zones exactly')

    end if

    iwnac(2) = jnac

    ! Calculate array bounds for both local and global arrays
    jn = 1
    if (jnac > 0) jn = jnac + 2*nghosts
    jea = jsa + jn - 1
    jnp = 1
    if (jnac > 0) jnp = jnacp + 2*nghosts
    jeap = jsap + jnp - 1

    ! Allocate primary and secondary 2-grid arrays
    call allocate_gridx2

    ! Report the sizes and bounds found and other useful information
    write (message, *) 'jnacp', jnacp
    call threeoutput(routinename//'_jnacp', message)
    write (message, *) 'jsap', jsap
    call threeoutput(routinename//'_jsap', message)
    write (message, *) 'jeap', jeap
    call threeoutput(routinename//'_jeap', message)

    write (message, *) 'jnac', jnac
    call threeoutput(routinename//'_jnac', message)
    write (message, *) 'jsa', jsa
    call threeoutput(routinename//'_jsa', message)
    write (message, *) 'jea', jea
    call threeoutput(routinename//'_jea', message)

    write (message, *) 'nnodes2', nnodes2
    call threeoutput(routinename//'_nnodes2', message)
    write (message, *) 'node2', node2
    call threeoutput(routinename//'_node2', message)
    !
    !---------------------------------------------------------------------
    !
    if (jnacp == 0) then
      !
      ! Set up a token x2-grid with one zone for problems with 2-symmetry
      !
      jsp = jsap ! lowerbound
      jep = jsp
      if (geometry == 'rmp') then
        x2ap(jsp) = -haf*pi
      else
        x2ap(jsp) = zro
      end if

      return
    end if

    !      Set up a full x2-grid.

    jsp = jsap + nghosts  ! First active zone set up here
    jep = jsp + jnacp - 1 ! Same as ieap-nghosts, or isap+nghosts+inacp-1

    call makegrid(x2ap, jsap, jeap, jsp, jep, 'x2')

    !      Grid in 2-direction is complete.

    ! Construct extended grid
    x2apl(jsap:jeap) = x2ap
    ! Uniform assumption
    x2apl(jeap + 1) = two*x2ap(jeap) - x2ap(jeap - 1)

    !      Correct some of the symmetries if needed.
    if (geometry == 'rtp') then
      ! If north pole is at the beginning of the grid
      if (abs(x2ap(jsp) - zro) < (x2ap(jsp + 1) - x2ap(jsp))*1e-2_pk) then
        do j = jsap, jsp - 1
          x2ap(j) = -x2ap(2*jsp - j)
        end do
        do j = jsap, jsp - 1
          x2apl(j) = -x2apl(2*jsp - j)
        end do
        if (axisshift /= zro) then
          x2ap(jsp) = axisshift*degree
          x2apl(jsp) = axisshift*degree
        end if
      end if

      ! If south pole is at the end of the grid
      if (abs(x2ap(jep + 1) - pi) < (x2ap(jep + 1) - x2ap(jep))*1e-2_pk) then
        do j = jep + 2, jeap
          x2ap(j) = two*pi - x2ap(2*(jep + 1) - j)
        end do
        do j = jep + 2, jeap + 1
          x2apl(j) = two*pi - x2ap(2*(jep + 1) - j)
        end do
        if (axisshift /= zro) then
          x2ap(jep + 1) = pi - axisshift*degree
          x2apl(jep + 1) = pi - axisshift*degree
        end if
      end if

      ! If equator is at the end of the grid
      if (abs(x2ap(jep + 1) - haf*pi) < (x2ap(jep + 1) - x2ap(jep))*1e-2_pk) &
        then
        do j = jep + 2, jeap
          x2ap(j) = pi - x2ap(2*(jep + 1) - j)
        end do
        do j = jep + 2, jeap + 1
          x2apl(j) = pi - x2ap(2*(jep + 1) - j)
        end do
      end if

      ! If equator is at the beginning of the grid
      if (abs(x2ap(jsp) - haf*pi) < (x2ap(jsp + 1) - x2ap(jsp))*1e-2_pk) then
        do j = jsap, jsp - 1
          x2ap(j) = pi - x2ap(2*jsp - j)
        end do
      end if

    end if
    if (geometry == 'zrp' .or. geometry == 'rtp') then
      !
      !  Grid consistency checks.
      !
      if ((x2ap(jsp - 1) <= zro) .and. (x2ap(jsp) /= zro)) then
        write (messagelong, '(a,1es12.5,a,1es12.5,a)') &
          'GRIDX2: Possibly ill-defined grid: Non-zero x2ap(jsp)=', x2ap(jsp), &
          ' but x2ap(jsp-1)=', x2ap(jsp - 1), '<=0 in ' &
          //trim(geometry)//' geometry.  Set up a warning.'
        call warnoutput(routinename, messagelong)
      end if
    end if
    ! Other grid consistency checks omitted for now.

  END SUBROUTINE gridx2
  !
  !======================================================================
  !
  SUBROUTINE gridx3
    !
    !  Initializes the x3a-grid as specified by the parameters in
    !  the namelist "ggen".
    !
    CHARACTER(LEN=*), PARAMETER :: routinename = 'z_gridx3'
    CHARACTER(LEN=messagelinelength) message

    INTEGER :: knacp
    INTEGER kn, knp
    INTEGER remainder

    ! Obtain the array active size
    ! Store it in the variables knacp, knac and iwnac
    knacp = get_array_size('x3')

    if (knacp == 0 .and. nnodes3 > 1) then
      write (message, '(a,i2,a,i4,a)') &
        'knacp= ', knacp, ' but nnodes3 =', nnodes3, ' >1'
      call threeoutput(routinename, message)
      call finalstop(routinename, &
                     'Only one processor needed for a symmetric direction!')
    end if

    knac = knacp/nnodes3

    if (knacp > 0 .and. knac <= 2*nghosts) then
      call finalstop(routinename, 'Tile size too small in this direction.')
    end if

    remainder = modulo(knacp, nnodes3)
    if (remainder /= 0) then

      ! Commented out because it makes the relation between
      ! local and global coordinates more complex.
      ! (it would invalidate kp=k+node3*knac)
      !
      !if (node3<remainder) then ! coordinate node3 counts from 0.
      !  knac=knac+1 ! Give one more point to the first nodes
      !              ! (0<=node3<remainder)
      !end if

      call finalstop(routinename, &
                     'nnodes3 does not divide the number of 3-zones exactly')

    end if

    iwnac(3) = knac

    ! Calculate array bounds for both local and global arrays
    kn = 1
    if (knac > 0) kn = knac + 2*nghosts
    kea = ksa + kn - 1
    knp = 1
    if (knac > 0) knp = knacp + 2*nghosts
    keap = ksap + knp - 1

    ! Allocate primary and secondary 3-grid arrays
    call allocate_gridx3

    ! Report the sizes and bounds found and other useful information
    write (message, *) 'knacp', knacp
    call threeoutput(routinename//'_knacp', message)
    write (message, *) 'ksap', ksap
    call threeoutput(routinename//'_ksap', message)
    write (message, *) 'keap', keap
    call threeoutput(routinename//'_keap', message)

    write (message, *) 'knac', knac
    call threeoutput(routinename//'_knac', message)
    write (message, *) 'ksa', ksa
    call threeoutput(routinename//'_ksa', message)
    write (message, *) 'kea', kea
    call threeoutput(routinename//'_kea', message)

    write (message, *) 'nnodes3', nnodes3
    call threeoutput(routinename//'_nnodes3', message)
    write (message, *) 'node3', node3
    call threeoutput(routinename//'_node3', message)
    !
    !---------------------------------------------------------------------
    !
    if (knacp == 0) then
      !
      ! Set up a token x3-grid with one zone for problems with 3-symmetry
      !
      ksp = ksap ! lowerbound
      kep = ksp
      x3ap(ksp) = zro
      return
    end if

    !      Set up a full x3-grid.

    ksp = ksap + nghosts  ! First active zone set up here
    kep = ksp + knacp - 1 ! Same as ieap-nghosts, or isap+nghosts+inacp-1

    call makegrid(x3ap, ksap, keap, ksp, kep, 'x3')

    !      Grid in 3-direction is complete.

    ! Construct extended grid
    x3apl(ksap:keap) = x3ap
    ! Uniform assumption
    x3apl(keap + 1) = two*x3ap(keap) - x3ap(keap - 1)

    ! Grid consistency checks omitted for now.
  END SUBROUTINE gridx3
  !
  !======================================================================
  !
  SUBROUTINE gsetx1
    CHARACTER(LEN=*), PARAMETER :: routinename = 'z_gsetx1'
    CHARACTER(LEN=messagelinelength) message
    !  Determines the secondary grid variables from "x1ap" set by GRIDX1.
    !
    INTEGER i
    REALNUM vol1ap(isap:ieap), vol1bp(isap:ieap)

    if (inac == 0) then
      !
      !------- Set the secondary variables for a token x1-grid ---------
      !
      is = isp
      ie = is
      ione = 0
      onei = real(ione, pk)

      ism2 = is
      ism1 = is
      iep1 = ie
      iep2 = ie
      isg = is
      isgo = is
      ieg = ie
      iego = ie
      !
      ism2p = isp
      ism1p = isp
      iep1p = iep
      iep2p = iep
      isgp = isp
      isgop = isp
      iegp = iep
      iegop = iep
      !
      x1a(is) = zro
      dx1a(is) = one
      x1b(is) = haf
      dx1b(is) = one
      g2a(is) = one
      g2b(is) = one
      g31a(is) = one
      g31b(is) = one
      dg2ad1(is) = zro
      dg2bd1(is) = zro
      dg31ad1(is) = zro
      dg31bd1(is) = zro
      if (geometry == 'xyz' .or. geometry == 'zrp') then
        dvl1a(is) = dx1a(is)
        dvl1b(is) = dx1b(is)
      end if
      if (geometry == 'rtp' .or. geometry == 'rmp') then
        dvl1a(is) = dx1a(is)**3/three
        dvl1b(is) = dx1b(is)*x1b(is)**2/three
      end if
      !
      x1ai(is) = one/(x1a(is) + verysmall)
      dx1ai(is) = one/(dx1a(is) + verysmall)
      x1bi(is) = one/(x1b(is) + verysmall)
      dx1bi(is) = one/(dx1b(is) + verysmall)
      g2ai(is) = one/(g2a(is) + verysmall)
      g2bi(is) = one/(g2b(is) + verysmall)
      g31ai(is) = one/(g31a(is) + verysmall)
      g31bi(is) = one/(g31b(is) + verysmall)
      dvl1ai(is) = one/(dvl1a(is) + verysmall)
      dvl1bi(is) = one/(dvl1b(is) + verysmall)
      !
      dx1ap(is) = dx1a(is)
      dvl1ap(is) = dvl1a(is)
      x1bp(is) = x1b(is)
      dx1bp(is) = dx1b(is)
      dvl1bp(is) = dvl1b(is)
      !
    else
      !
      !----- Set the secondary variables for a substantive x1-grid ------
      !
      ione = 1
      onei = real(ione, pk)

      is = isp
      ie = is + inac - 1

      ism2 = is - 2
      ism1 = is - 1
      iep1 = ie + 1
      iep2 = ie + 2
      isg = is - nghosts
      isgo = is - nghosts + 1
      ieg = ie + nghosts
      iego = ie + nghosts - 1
      !
      ism2p = isp - 2
      ism1p = isp - 1
      iep1p = iep + 1
      iep2p = iep + 2
      isgp = isp - nghosts
      isgop = isp - nghosts + 1
      iegp = iep + nghosts
      iegop = iep + nghosts - 1

      ! Report the sizes and bounds found and other useful information
      write (message, *) 'is', is
      call threeoutput(routinename//'_is', message)
      write (message, *) 'ie', ie
      call threeoutput(routinename//'_ie', message)
      write (message, *) 'isg', isg
      call threeoutput(routinename//'_isg', message)
      write (message, *) 'ieg', ieg
      call threeoutput(routinename//'_ieg', message)
      write (message, *) 'isgo', isgo
      call threeoutput(routinename//'_isgo', message)
      write (message, *) 'iego', iego
      call threeoutput(routinename//'_iego', message)

      write (message, *) 'isp', isp
      call threeoutput(routinename//'_isp', message)
      write (message, *) 'iep', iep
      call threeoutput(routinename//'_iep', message)
      write (message, *) 'isgp', isgp
      call threeoutput(routinename//'_isgp', message)
      write (message, *) 'iegp', iegp
      call threeoutput(routinename//'_iegp', message)
      write (message, *) 'isgop', isgop
      call threeoutput(routinename//'_isgop', message)
      write (message, *) 'iegop', iegop
      call threeoutput(routinename//'_iegop', message)
      !
      !      Set differentials for the "x1ap" grid.
      !
      do i = isgp, iegop
        dx1ap(i) = x1ap(i + 1) - x1ap(i)
      end do
      dx1ap(iegp) = dx1ap(iegop)

      !
      !      Set "x1bp" grid and its differentials.
      !
      do i = isgp, iegp
        x1bp(i) = x1ap(i) + haf*dx1ap(i)
      end do
      do i = isgop, iegp
        dx1bp(i) = x1bp(i) - x1bp(i - 1)
      end do
      dx1bp(isgp) = dx1ap(isgp)
      !
      !      Set zone volumes.
      !
      do i = isgp, iegp
        if (geometry == 'xyz' .or. geometry == 'zrp') then
          vol1ap(i) = x1ap(i)
          vol1bp(i) = x1bp(i)
        end if
        if (geometry == 'rtp' .or. geometry == 'rmp') then
          vol1ap(i) = x1ap(i)**3/three
          vol1bp(i) = x1bp(i)**3/three
        end if
      end do
      !
      !    Set volume factors used in integral form of difference equations
      !  and in transport module.
      !
      do i = isgp, iegop
        dvl1ap(i) = vol1ap(i + 1) - vol1ap(i)
      end do
      do i = isgop, iegp
        dvl1bp(i) = vol1bp(i) - vol1bp(i - 1)
      end do
      dvl1ap(iegp) = dvl1bp(iegp)
      dvl1bp(isgp) = dvl1ap(isgp)
      !
      !      Set local arrays
      !
      do i = isg, ieg
        x1a(i) = x1ap(i + node1*inac)
        dx1a(i) = dx1ap(i + node1*inac)
        x1b(i) = x1bp(i + node1*inac)
        dx1b(i) = dx1bp(i + node1*inac)
        dvl1a(i) = dvl1ap(i + node1*inac)
        dvl1b(i) = dvl1bp(i + node1*inac)
      end do

      x1al(isg:ieg) = x1a(isg:ieg)
      x1al(ieg + 1) = x1apl(ieg + 1 + node1*inac)

      !
      !      Set covariant quantities
      !
      do i = isg, ieg
        if (geometry == 'xyz' .or. geometry == 'zrp') then
          g2a(i) = one
          g31a(i) = one
          dg2bd1(i) = zro
          dg31bd1(i) = zro
          !
          g2b(i) = one
          g31b(i) = one
          dg2ad1(i) = zro
          dg31ad1(i) = zro
        end if
        if (geometry == 'rtp' .or. geometry == 'rmp') then
          g2a(i) = x1a(i)
          g31a(i) = x1a(i)
          dg2bd1(i) = one
          dg31bd1(i) = one
          !
          g2b(i) = x1b(i)
          g31b(i) = x1b(i)
          dg2ad1(i) = one
          dg31ad1(i) = one
        end if
      end do
      !
      !      Set inverse quantities.
      !
      do i = isg, ieg
        x1ai(i) = one/(x1a(i) + verysmall)
        dx1bi(i) = one/(dx1b(i) + verysmall)
        g2ai(i) = one/(g2a(i) + verysmall)
        g31ai(i) = one/(g31a(i) + verysmall)
        dvl1bi(i) = one/(dvl1b(i) + verysmall)
      end do
      do i = isg, ieg
        x1bi(i) = one/(x1b(i) + verysmall)
        dx1ai(i) = one/(dx1a(i) + verysmall)
        g2bi(i) = one/(g2b(i) + verysmall)
        g31bi(i) = one/(g31b(i) + verysmall)
        dvl1ai(i) = one/(dvl1a(i) + verysmall)
      end do
      !
    end if
    !
    !      Copy grid over to "n+1/2" and "n+1" grids.
    !
    do i = isg, ieg
      !
      x1ah(i) = x1a(i)
      x1bh(i) = x1b(i)
      dx1ah(i) = dx1a(i)
      dx1bh(i) = dx1b(i)
      g2ah(i) = g2a(i)
      g2bh(i) = g2b(i)
      g31ah(i) = g31a(i)
      g31bh(i) = g31b(i)
      dvl1ah(i) = dvl1a(i)
      dvl1bh(i) = dvl1b(i)
      !
      x1ahi(i) = x1ai(i)
      x1bhi(i) = x1bi(i)
      dx1ahi(i) = dx1ai(i)
      dx1bhi(i) = dx1bi(i)
      g2ahi(i) = g2ai(i)
      g2bhi(i) = g2bi(i)
      g31ahi(i) = g31ai(i)
      g31bhi(i) = g31bi(i)
      dvl1ahi(i) = dvl1ai(i)
      dvl1bhi(i) = dvl1bi(i)
      !
      x1an(i) = x1a(i)
      x1bn(i) = x1b(i)
      dx1an(i) = dx1a(i)
      dx1bn(i) = dx1b(i)
      g2an(i) = g2a(i)
      g2bn(i) = g2b(i)
      g31an(i) = g31a(i)
      g31bn(i) = g31b(i)
      dvl1an(i) = dvl1a(i)
      dvl1bn(i) = dvl1b(i)
      !
      x1ani(i) = x1ai(i)
      x1bni(i) = x1bi(i)
      dx1ani(i) = dx1ai(i)
      dx1bni(i) = dx1bi(i)
      g2ani(i) = g2ai(i)
      g2bni(i) = g2bi(i)
      g31ani(i) = g31ai(i)
      g31bni(i) = g31bi(i)
      dvl1ani(i) = dvl1ai(i)
      dvl1bni(i) = dvl1bi(i)
      !
    end do
    !
  END SUBROUTINE gsetx1
  !
  !======================================================================
  !
  SUBROUTINE gsetx2
    CHARACTER(LEN=*), PARAMETER :: routinename = 'z_gsetx2'
    CHARACTER(LEN=messagelinelength) message
    !  Determines the secondary grid variables from "x2ap" set by GRIDX2.
    !
    INTEGER j
    REALNUM vol2ap(jsap:jeap), vol2bp(jsap:jeap)

    if (jnac == 0) then

      js = jsp
      je = js
      jone = 0
      onej = real(jone, pk)
      !
      !------- Set the secondary variables for a token x2-grid ---------
      !
      jsm2 = js
      jsm1 = js
      jep1 = je
      jep2 = je
      jsg = js
      jsgo = js
      jeg = je
      jego = je
      !
      jsm2p = jsp
      jsm1p = jsp
      jep1p = jep
      jep2p = jep
      jsgp = jsp
      jsgop = jsp
      jegp = jep
      jegop = jep
      !

      x2a(js) = x2ap(jsp)

      if (geometry == 'xyz' .or. geometry == 'zrp') then
        dx2a(js) = one
        x2b(js) = haf
        dx2b(js) = one
      end if
      if (geometry == 'rtp') then
        dx2a(js) = pi
        x2b(js) = pi/two
        dx2b(js) = pi
      end if
      if (geometry == 'rmp') then
        dx2a(js) = pi
        x2b(js) = zro
        dx2b(js) = pi
      end if

      g32a(js) = one
      g32b(js) = one
      dg32ad2(js) = zro
      dg32bd2(js) = zro
      if (geometry == 'xyz') then
        dvl2a(js) = dx2a(js)
        dvl2b(js) = dx2b(js)
      end if
      if (geometry == 'zrp') then
        dvl2a(js) = dx2a(js)*x2b(js)
        dvl2b(js) = haf*x2b(js)**2
      end if
      if (geometry == 'rtp') then
        dvl2a(js) = cos(x2a(js)) - cos(dx2a(js))
        dvl2b(js) = cos(x2a(js)) - cos(x2b(js))
      end if
      if (geometry == 'rmp') then
        dvl2a(js) = -sin(x2a(js)) + sin(dx2a(js))
        dvl2b(js) = -sin(x2a(js)) + sin(x2b(js))
      end if
      !
      x2ai(js) = one/(x2a(js) + verysmall)
      dx2ai(js) = one/(dx2a(js) + verysmall)
      x2bi(js) = one/(x2b(js) + verysmall)
      dx2bi(js) = one/(dx2b(js) + verysmall)
      g32ai(js) = one/(g32a(js) + verysmall)
      g32bi(js) = one/(g32b(js) + verysmall)
      dvl2ai(js) = one/(dvl2a(js) + verysmall)
      dvl2bi(js) = one/(dvl2b(js) + verysmall)
      !
      dx2ap(js) = dx2a(js)
      dvl2ap(js) = dvl2a(js)
      x2bp(js) = x2b(js)
      dx2bp(js) = dx2b(js)
      dvl2bp(js) = dvl2b(js)
      !
    else
      !
      !----- Set the secondary variables for a substantive x2-grid ------
      !
      jone = 1
      onej = real(jone, pk)

      js = jsp
      je = js + jnac - 1

      jsm2 = js - 2
      jsm1 = js - 1
      jep1 = je + 1
      jep2 = je + 2
      jsg = js - nghosts
      jsgo = js - nghosts + 1
      jeg = je + nghosts
      jego = je + nghosts - 1
      !
      jsm2p = jsp - 2
      jsm1p = jsp - 1
      jep1p = jep + 1
      jep2p = jep + 2
      jsgp = jsp - nghosts
      jsgop = jsp - nghosts + 1
      jegp = jep + nghosts
      jegop = jep + nghosts - 1

      ! Report the sizes and bounds found and other useful information
      write (message, *) 'js', js
      call threeoutput(routinename//'_js', message)
      write (message, *) 'je', je
      call threeoutput(routinename//'_je', message)
      write (message, *) 'jsg', jsg
      call threeoutput(routinename//'_jsg', message)
      write (message, *) 'jeg', jeg
      call threeoutput(routinename//'_jeg', message)
      write (message, *) 'jsgo', jsgo
      call threeoutput(routinename//'_jsgo', message)
      write (message, *) 'jego', jego
      call threeoutput(routinename//'_jego', message)

      write (message, *) 'jsp', jsp
      call threeoutput(routinename//'_jsp', message)
      write (message, *) 'jep', jep
      call threeoutput(routinename//'_jep', message)
      write (message, *) 'jsgp', jsgp
      call threeoutput(routinename//'_jsgp', message)
      write (message, *) 'jegp', jegp
      call threeoutput(routinename//'_jegp', message)
      write (message, *) 'jsgop', jsgop
      call threeoutput(routinename//'_jsgop', message)
      write (message, *) 'jegop', jegop
      call threeoutput(routinename//'_jegop', message)

      !
      !      Set differentials for the "x2ap" grid.
      !
      do j = jsgp, jegop
        dx2ap(j) = x2ap(j + 1) - x2ap(j)
      end do
      dx2ap(jegp) = dx2ap(jegop)

      ! BOUNDARY INFO
      ! Uniform assumption
      ! dx2ap(jegp)=dx2ap(jegop)
      ! Reflection assumption
      dx2ap(jegp) = x2apl(jegp + 1) - x2apl(jegp)

      !
      !      Set "x2bp" grid and its differentials.
      !
      do j = jsgp, jegp
        x2bp(j) = x2ap(j) + haf*dx2ap(j)
      end do
      do j = jsgop, jegp
        dx2bp(j) = x2bp(j) - x2bp(j - 1)
      end do
      dx2bp(jsgp) = dx2ap(jsgp)
      !
      !      Set zone volumes.
      !
      do j = jsgp, jegp
        if (geometry == 'xyz') then
          vol2ap(j) = x2ap(j)
          vol2bp(j) = x2bp(j)
        end if
        if (geometry == 'zrp') then
          vol2ap(j) = x2ap(j)**2/two
          vol2bp(j) = x2bp(j)**2/two
        end if
        if (geometry == 'rtp') then
          vol2ap(j) = -cos(x2ap(j))
          vol2bp(j) = -cos(x2bp(j))
        end if
        if (geometry == 'rmp') then
          vol2ap(j) = sin(x2ap(j))
          vol2bp(j) = sin(x2bp(j))
        end if
      end do
      !
      !    Set volume factors used in integral form of difference equations
      !  and in transport module.
      !
      do j = jsgp, jegop
        dvl2ap(j) = vol2ap(j + 1) - vol2ap(j)
      end do
      do j = jsgop, jegp
        dvl2bp(j) = vol2bp(j) - vol2bp(j - 1)
      end do
      dvl2ap(jegp) = dvl2bp(jegp)
      dvl2bp(jsgp) = dvl2a(jsgp)
      !
      !      Set local arrays
      !
      do j = jsg, jeg
        x2a(j) = x2ap(j + node2*jnac)
        dx2a(j) = dx2ap(j + node2*jnac)
        x2b(j) = x2bp(j + node2*jnac)
        dx2b(j) = dx2bp(j + node2*jnac)
        dvl2a(j) = dvl2ap(j + node2*jnac)
        dvl2b(j) = dvl2bp(j + node2*jnac)
      end do

      x2al(jsg:jeg) = x2a(jsg:jeg)
      x2al(jeg + 1) = x2apl(jeg + 1 + node2*jnac)

      !
      !      Set covariant quantities
      !
      do j = jsg, jeg
        if (geometry == 'xyz') then
          g32a(j) = one
          dg32bd2(j) = zro
          !
          g32b(j) = one
          dg32ad2(j) = zro
        end if
        if (geometry == 'zrp') then
          g32a(j) = x2a(j)
          dg32bd2(j) = one
          !
          g32b(j) = x2b(j)
          dg32ad2(j) = one
        end if
        if (geometry == 'rtp') then
          g32a(j) = sin(x2a(j))
          dg32bd2(j) = cos(x2b(j))
          g32b(j) = sin(x2b(j))
          dg32ad2(j) = cos(x2a(j))
        end if
        if (geometry == 'rmp') then
          g32a(j) = cos(x2a(j))
          dg32bd2(j) = -sin(x2b(j))
          g32b(j) = cos(x2b(j))
          dg32ad2(j) = -sin(x2a(j))
        end if
      end do
      !
      !      Set inverse quantities.
      !
      do j = jsg, jeg
        x2ai(j) = one/(x2a(j) + verysmall)
        dx2bi(j) = one/(dx2b(j) + verysmall)
        g32ai(j) = one/(g32a(j) + verysmall)
        dvl2bi(j) = one/(dvl2b(j) + verysmall)
      end do
      do j = jsg, jeg
        x2bi(j) = one/(x2b(j) + verysmall)
        dx2ai(j) = one/(dx2a(j) + verysmall)
        g32bi(j) = one/(g32b(j) + verysmall)
        dvl2ai(j) = one/(dvl2a(j) + verysmall)
      end do
      !
    end if
    !
    onej = real(jone, pk)
    !
    !      Copy grid over to "n+1/2" and "n+1" grids.
    !
    do j = jsg, jeg
      !
      x2ah(j) = x2a(j)
      x2bh(j) = x2b(j)
      dx2ah(j) = dx2a(j)
      dx2bh(j) = dx2b(j)
      g32ah(j) = g32a(j)
      g32bh(j) = g32b(j)
      dvl2ah(j) = dvl2a(j)
      dvl2bh(j) = dvl2b(j)
      !
      x2ahi(j) = x2ai(j)
      x2bhi(j) = x2bi(j)
      dx2ahi(j) = dx2ai(j)
      dx2bhi(j) = dx2bi(j)
      g32ahi(j) = g32ai(j)
      g32bhi(j) = g32bi(j)
      dvl2ahi(j) = dvl2ai(j)
      dvl2bhi(j) = dvl2bi(j)
      !
      x2an(j) = x2a(j)
      x2bn(j) = x2b(j)
      dx2an(j) = dx2a(j)
      dx2bn(j) = dx2b(j)
      g32an(j) = g32a(j)
      g32bn(j) = g32b(j)
      dvl2an(j) = dvl2a(j)
      dvl2bn(j) = dvl2b(j)
      !
      x2ani(j) = x2ai(j)
      x2bni(j) = x2bi(j)
      dx2ani(j) = dx2ai(j)
      dx2bni(j) = dx2bi(j)
      g32ani(j) = g32ai(j)
      g32bni(j) = g32bi(j)
      dvl2ani(j) = dvl2ai(j)
      dvl2bni(j) = dvl2bi(j)
      !
    end do
    !
  END SUBROUTINE gsetx2
  !
  !======================================================================
  !
  SUBROUTINE gsetx3
    CHARACTER(LEN=*), PARAMETER :: routinename = 'z_gsetx3'
    CHARACTER(LEN=messagelinelength) message
    !  Determines the secondary grid variables from "x3ap" set by GRIDX3.
    !
    INTEGER k
    REALNUM vol3ap(ksap:keap), vol3bp(ksap:keap)

    if (knac == 0) then
      ks = ksp
      ke = ks
      kone = 0
      onek = real(kone, pk)
      !
      !------- Set the secondary variables for a token x3-grid ---------
      !
      ksm2 = ks
      ksm1 = ks
      kep1 = ke
      kep2 = ke
      ksg = ks
      ksgo = ks
      keg = ke
      kego = ke
      !
      ksm2p = ksp
      ksm1p = ksp
      kep1p = kep
      kep2p = kep
      ksgp = ksp
      ksgop = ksp
      kegp = kep
      kegop = kep
      !
      x3a(ks) = zro
      if (geometry == 'xyz') then
        dx3a(ks) = one
        x3b(ks) = haf
        dx3b(ks) = one
      end if
      if (geometry == 'zrp' .or. geometry == 'rtp' .or. geometry == 'rmp') then
        dx3a(ks) = two*pi
        x3b(ks) = pi
        dx3b(ks) = two*pi
      end if
      dvl3a(ks) = dx3a(ks)
      dvl3b(ks) = dx3b(ks)
      !
      x3ai(ks) = one/(x3a(ks) + verysmall)
      dx3ai(ks) = one/(dx3a(ks) + verysmall)
      x3bi(ks) = one/(x3b(ks) + verysmall)
      dx3bi(ks) = one/(dx3b(ks) + verysmall)
      dvl3ai(ks) = one/(dvl3a(ks) + verysmall)
      dvl3bi(ks) = one/(dvl3b(ks) + verysmall)
      !
      dx3ap(ks) = dx3a(ks)
      dvl3ap(ks) = dvl3a(ks)
      x3bp(ks) = x3b(ks)
      dx3bp(ks) = dx3b(ks)
      dvl3bp(ks) = dvl3b(ks)
      !
    else
      !
      !---- Set the secondary variables for a substantive x3-grid ------
      !
      kone = 1
      onek = real(kone, pk)

      ks = ksp
      ke = ks + knac - 1

      ksm2 = ks - 2
      ksm1 = ks - 1
      kep1 = ke + 1
      kep2 = ke + 2
      ksg = ks - nghosts
      ksgo = ks - nghosts + 1
      keg = ke + nghosts
      kego = ke + nghosts - 1
      !
      ksm2p = ksp - 2
      ksm1p = ksp - 1
      kep1p = kep + 1
      kep2p = kep + 2
      ksgp = ksp - nghosts
      ksgop = ksp - nghosts + 1
      kegp = kep + nghosts
      kegop = kep + nghosts - 1

      ! Report the sizes and bounds found and other useful information
      write (message, *) 'ks', ks
      call threeoutput(routinename//'_ks', message)
      write (message, *) 'ke', ke
      call threeoutput(routinename//'_ke', message)
      write (message, *) 'ksg', ksg
      call threeoutput(routinename//'_ksg', message)
      write (message, *) 'keg', keg
      call threeoutput(routinename//'_keg', message)
      write (message, *) 'ksgo', ksgo
      call threeoutput(routinename//'_ksgo', message)
      write (message, *) 'kego', kego
      call threeoutput(routinename//'_kego', message)

      write (message, *) 'ksp', ksp
      call threeoutput(routinename//'_ksp', message)
      write (message, *) 'kep', kep
      call threeoutput(routinename//'_kep', message)
      write (message, *) 'ksgp', ksgp
      call threeoutput(routinename//'_ksgp', message)
      write (message, *) 'kegp', kegp
      call threeoutput(routinename//'_kegp', message)
      write (message, *) 'ksgop', ksgop
      call threeoutput(routinename//'_ksgop', message)
      write (message, *) 'kegop', kegop
      call threeoutput(routinename//'_kegop', message)

      !
      !      Set differentials for the "x3ap" grid.
      !
      do k = ksgp, kegop
        dx3ap(k) = x3ap(k + 1) - x3ap(k)
      end do
      dx3ap(kegp) = dx3ap(kegop)

      !
      !      Set "x3bp" grid and its differentials.
      !
      do k = ksgp, kegp
        x3bp(k) = x3ap(k) + haf*dx3ap(k)
      end do
      do k = ksgop, kegp
        dx3bp(k) = x3bp(k) - x3bp(k - 1)
      end do
      dx3bp(ksgp) = dx3ap(ksgp)
      !
      !      Set zone volumes.
      !
      do k = ksgp, kegp
        vol3ap(k) = x3ap(k)
        vol3bp(k) = x3bp(k)
      end do
      !
      !    Set volume factors used in integral form of difference equations
      !  and in transport module.
      !
      do k = ksgp, kegop
        dvl3ap(k) = vol3ap(k + 1) - vol3ap(k)
      end do
      do k = ksgop, kegp
        dvl3bp(k) = vol3bp(k) - vol3bp(k - 1)
      end do
      dvl3ap(kegp) = dvl3bp(kegp)
      dvl3bp(ksgp) = dvl3a(ksgp)
      !
      !      Set local arrays
      !
      do k = ksg, keg
        x3a(k) = x3ap(k + node3*knac)
        dx3a(k) = dx3ap(k + node3*knac)
        x3b(k) = x3bp(k + node3*knac)
        dx3b(k) = dx3bp(k + node3*knac)
        dvl3a(k) = dvl3ap(k + node3*knac)
        dvl3b(k) = dvl3bp(k + node3*knac)
      end do

      x3al(ksg:keg) = x3a(ksg:keg)
      x3al(keg + 1) = x3apl(keg + 1 + node3*knac)

      !
      !      Set inverse quantities.
      !
      do k = ksg, keg
        x3ai(k) = one/(x3a(k) + verysmall)
        dx3bi(k) = one/(dx3b(k) + verysmall)
        dvl3bi(k) = one/(dvl3b(k) + verysmall)
      end do
      do k = ksg, keg
        x3bi(k) = one/(x3b(k) + verysmall)
        dx3ai(k) = one/(dx3a(k) + verysmall)
        dvl3ai(k) = one/(dvl3a(k) + verysmall)
      end do
      !
    end if
    !
    onek = real(kone, pk)
    !
    !      Copy grid over to "n+1/2" and "n+1" grids.
    !
    do k = ksg, keg
      !
      x3ah(k) = x3a(k)
      x3bh(k) = x3b(k)
      dx3ah(k) = dx3a(k)
      dx3bh(k) = dx3b(k)
      dvl3ah(k) = dvl3a(k)
      dvl3bh(k) = dvl3b(k)
      !
      x3ahi(k) = x3ai(k)
      x3bhi(k) = x3bi(k)
      dx3ahi(k) = dx3ai(k)
      dx3bhi(k) = dx3bi(k)
      dvl3ahi(k) = dvl3ai(k)
      dvl3bhi(k) = dvl3bi(k)
      !
      x3an(k) = x3a(k)
      x3bn(k) = x3b(k)
      dx3an(k) = dx3a(k)
      dx3bn(k) = dx3b(k)
      dvl3an(k) = dvl3a(k)
      dvl3bn(k) = dvl3b(k)
      !
      x3ani(k) = x3ai(k)
      x3bni(k) = x3bi(k)
      dx3ani(k) = dx3ai(k)
      dx3bni(k) = dx3bi(k)
      dvl3ani(k) = dvl3ai(k)
      dvl3bni(k) = dvl3bi(k)
      !
    end do
    !
  END SUBROUTINE gsetx3
  !
  !======================================================================
  !
  SUBROUTINE makegrid(xa, nsa, nea, ns, ne, name)
    CHARACTER(LEN=*), PARAMETER :: routinename = 'z_makegrid'
    CHARACTER(LEN=messagelinelength) message

    ! Create a grid on the array xa(nsa:nea)
    ! Data are given in blocks, each of them of length
    ! nbl: the sum of the all nbl is equal to the extent ns:ne
    ! Most usually, nsa < ns; in that case, the remaining points
    ! are ghost zones and they are either extrapolated from the
    ! active zones (igrid/=0), or they should be already present
    ! in the file to read in the case igrid==0.
    ! Similar concerns apply to the case ne < nea.

    !  Each block can be zoned independently of the others.
    !  The only constraint is that the starting position of the
    !  current block ("xmin") must correspond to the ending
    !  position ("xmax") of the previous block.
    !  Over the current block, the zone widths are either ratioed
    !  or scaled, as explained below.
    !
    !      Case 1:  RATIOED ZONES
    !
    !      Ratioed zones means that the ratio dx(i+1)/dx(i) is constant over
    !  a specified region of the grid.  The zones can then be computed using
    !  the zoning equation:
    !
    !      x1(I) = xmin + SUM OVER i UP TO I [dxmin*xrat**i]
    !
    !  Not all parameters are independent.  Since xmax is specified, then
    !
    !      xmax = x1(N) = xmin + SUM OVER i UP TO N [dxmin*xrat**i]
    !
    !  will allow "dxmin" to be computed from "xrat" ("igrid" = +1, -1),
    !  or "xrat" to be computed from "dxmin" ("igrid" = +2, -2).
    !
    !      Case 2:  SCALED ZONES
    !
    !      Scaled zones are zones where dx is proportional to x. Thus, zones
    !  with uniform shapes in spherical coordinates can be achieved with
    !  this option.  In this case, the ratio x(i+1)/x(i) is constant over
    !  the specified region of the grid.  The zones are computed using the
    !  zoning equation:
    !
    !      x(I) = xmin * xrat**(I)
    !
    !  Since xmax = x(N) is specified, N and "xrat" are not independent.
    !
    !      xmax = xmin * xrat**(N)
    !
    !  Thus, in this case ("igrid" = 3) the grid can be generated from N,
    !  "xmin", and "xmax" and the user specified values of "xrat" and
    !  "dxmin" are ignored.
    !
    !   nbl        number of active zones in block being generated
    !   xmin       x1ap(imin), bottom position of block
    !   xmax       x1ap(imax), top    position of block
    !   igrid      selects zoning type.
    !              = 0  => read this block from a named file
    !              =+1  => (ratioed) use input "xrat" to compute "dxmin",
    !                      where x1a(imin+1) = x1a(imin) + dxmin.
    !              =-1  => (ratioed) use input "xrat" to compute "dxmin",
    !                      where x1a(imax) = x1a(imax-1) + dxmin.
    !              =+2  => (ratioed) use input "dxmin" to compute "xrat",
    !                      where x1a(imin+1) = x1a(imin) + dxmin.
    !              =-2  => (ratioed) use input "dxmin" to compute "xrat",
    !                      where x1a(imax) = x1a(imax-1) + dxmin.
    !              = 3  => (scaled) compute "xrat" and "dxmin" from "nbl".
    !

    INTEGER, INTENT(IN) :: nsa, nea, ns, ne
    REALNUM, INTENT(INOUT) :: xa(nsa:nea)
    CHARACTER(LEN=2), INTENT(IN) :: name

    CHARACTER(LEN=2) :: gridname_upper, gridname_lower
    LOGICAL firstblock, lastblock

    INTEGER nmin, nmax ! Beginning and end of active block
    INTEGER nbeg, nend ! Beginning and end of storage area
    ! (may be longer due to ghost zones)
    INTEGER iter, n
    REALNUM dfndxr, xr, deltxr, gerrxr, fn
    REALNUM fact

    firstblock = .true.

    rewind (ioin)
    gridblock: do
      call defaults_ggen
      read (ioin, ggen, end=10)
      gridname_upper = gridname
      call uppercase(gridname_upper)

      gridname_lower = gridname
      call lowercase(gridname_lower)

      if (gridname_lower /= name) cycle
      if (iolog /= NO_OUTPUT) write (iolog, ggen)
      if (iotty /= NO_OUTPUT) write (iotty, ggen)
      if (nbl == 0) cycle
      if (nbl < 0) then
        write (message, '(a,i8,a)') 'Error: nbl= ', nbl, ' <0'
        call threeoutput(routinename, message)
        write (message, '(a)') 'while trying to generate grid '//name//'.'
        call finalstop(routinename, message)
      end if
      !
      !  Set the units.
      !  Do that for all grid directions - do not check
      !  for geometry or grid direction - the user should simply
      !  be careful when defining the grid, and it is conceivable
      !  (although weird) to want degrees as a unit even
      !  for a Cartesian grid.
      !
      !  Adjust the extrema xmin and xmax, and also dxmin,
      !  as needed for the cases abs(igrid)==2.
      !
      !  DO NOT reset the extrema to the input values modulo pi:
      !  there can be legitimate reasons to want to use other ranges.
      !

      fact = one
      call lowercase(units)
      if (units == 'dg') fact = degree
      if (units == 'pi') fact = pi
      xmin = xmin*fact ! modulo_eps( xmin*fact, pi, small )
      xmax = xmax*fact ! modulo_eps( xmax*fact, pi, small )
      ! if (xmin+small > xmax) xmin = xmin - pi
      dxmin = dxmin*fact
      if (iolog /= NO_OUTPUT) write (iolog, ggen)
      if (iotty /= NO_OUTPUT) write (iotty, ggen)

      if (firstblock) then
        nmin = ns
        nbeg = nsa
      else
        nmin = nmax + 1
        nbeg = nmin
      end if

      !Check nmax, nend, and the if block condition
      nmax = nmin + nbl - 1
      if (nmax >= ne) then
        nend = nea
        lastblock = .true.
      else
        nend = nmax + 1
        lastblock = .false.
      end if
      !
      !  IGRID = 0
      !  Read this block from file
      !
      if (igrid == 0) then
        call readdata(xa(nbeg:nend), trim(gridfile), SINGLEFILE=.true.)
        if (iolog /= NO_OUTPUT) then
          write (iolog, 2050) gridname_upper, gridname_upper, gridname_lower, &
            nbeg, nend
          write (iolog, 2060) gridname_upper, trim(gridfile)
          write (iolog, 2100) gridname_upper, (xa(n), n=nbeg, nend)
        end if
        if (lastblock) then
          exit gridblock
        else
          cycle gridblock
        end if
      end if
      !
      !  IGRID = 1:  For ratioed zones, compute "dxmin" from given value of
      !              "xrat".
      !
      if (abs(igrid) == 1) then
        if (igrid == -1) xrat = one/xrat
        if (xrat == one) then
          dxmin = (xmax - xmin)/real(nbl, pk)
        else
          dxmin = (xmax - xmin)*(xrat - one) &
                  /(xrat**nbl - one)
        end if
      end if
      !
      !  IGRID = 2 and -2:
      !  For ratioed zones, compute "xrat" from given value of "dxmin".
      !
      !  A Newton Raphson iteration is used to find the root ("xrat") of the
      !  function:
      !
      !    fn(xr) = (xmax-xmin) - dxmin * [(xr)**nbl - 1] / [xr - 1] = 0
      !
      !  dfndxr = derivative of fn wrt xr
      !  deltxr = correction term to xr
      !  gerrxr = relative error of correction
      !
      if (abs(igrid) == 2) then
        xr = 1.01_pk
        griditeration: do
          iter = 1
          fn = xmax - xmin &
               - dxmin*(xr**nbl - one)/(xr - one)
          dfndxr = dxmin*(xr**nbl - one)/(xr - one)**2 &
                   - nbl*dxmin*xr**(nbl - 1)/(xr - one)
          deltxr = -fn/dfndxr
          gerrxr = abs(deltxr/xr)
          xr = xr + deltxr
          if (gerrxr < griderror) exit griditeration
          iter = iter + 1
          if (iter > griditer) then
            write (message, '(a)') &
              'Generation of grid '//gridname_lower//' failed.'
            call threeoutput(routinename, message)
            write (message, '(a)') 'Newton-Raphson did not converge for xrat.'
            call threeoutput(routinename, message)
            write (message, '(a,i8,a,es12.5)') 'nmin= ', nmin, 'xr= ', xr
            call threeoutput(routinename, message)
            write (message, '(a,es12.5,a,es12.5)') &
              'deltxr= ', deltxr, 'fn= ', fn
            call finalstop(routinename, message)
          end if
        end do griditeration
        xrat = xr
      end if
      if (igrid == -2) then
        dxmin = dxmin*xrat**(nbl - 1)
        xrat = one/xrat
      end if
      !
      !  IGRID = 3:  For scaled zones, compute "xrat", the ratio of
      !              adjacent zone-centre coordinates.
      !
      if (igrid == 3) then
        if (xmin <= zro) then
          write (message, '(a)') &
            'Generation of grid '//gridname_lower//' failed.'
          call threeoutput(routinename, message)
          write (message, '(a)') &
            'The scaled zone option igrid=3 is incompatible with xmin<0.'
          call threeoutput(routinename, message)
          write (message, '(a,es12.5)') 'xmin= ', xmin
          call finalstop(routinename, message)
        end if
        xrat = (xmax/xmin)**(one/real(nbl, pk))
      end if
      !
      !  Default = error in input of "igrid".  Issue error messagelong and
      !  terminate execution (only legal values of "igrid" are -2, -1, 0, 1,
      !  2, and 3).
      !
      if ((igrid < -2) .or. (igrid > 3)) then
        write (message, '(a)') 'Generation of grid '//gridname_lower//' failed.'
        call threeoutput(routinename, message)
        write (message, '(a)') 'Illegal value of igrid.'
        call threeoutput(routinename, message)
        write (message, '(a,i8,a,i8)') 'nmin= ', nmin, 'igrid= ', igrid
        call finalstop(routinename, message)
      end if
      !
      !  Set "x1a" grid from i = "ibeg" to "iend", using known values of
      !  "xmin" and "xrat".
      !
      if (igrid <= 2 .and. igrid >= -2) then
        if (xrat == one) then
          do n = nbeg, nend
            xa(n) = xmin + dxmin*real(n - nmin, pk)
          end do
        else
          do n = nbeg, nend
            xa(n) = xmin + dxmin*(xrat**(n - nmin) - one) &
                    /(xrat - one)
          end do
        end if
      end if
      if (igrid == 3) then
        do n = nbeg, nend
          xa(n) = xmin*xrat**(n - nmin)
        end do
      end if
      !
      !      Write grid to log file
      !
      if (iolog /= NO_OUTPUT) then
        write (iolog, 2050) gridname_upper, gridname_upper, gridname_lower, &
          nbeg, nend
        if (xrat >= one) write (iolog, 2070) gridname_upper, dxmin, xrat
        if (xrat < one) write (iolog, 2080) gridname_upper, dxmin, xrat
        write (iolog, 2100) (xa(n), n=nbeg, nend)
      end if
      !
      ! Finish up the reading of grid blocks if that is the case
      !
      if (lastblock) exit gridblock
      firstblock = .false.
    end do gridblock
    return

10  continue
    write (message, *) &
      'Tried to read ggen beyond end of inzeus file.  Continue.'
    call threeoutput(routinename, message)
    return

!-----------------------------------------------------------------------
!----------------------- Write format statements -----------------------
!-----------------------------------------------------------------------
2050 format('GRID', a, '  :', / &
           , 'GRID', a, '  : --------------------> ', a, 'a (face-centred) ' &
           , 'grid, zones ', i4, ' through ', i4)
2060 format('GRID', a, '  : These zones read from file ', a)
2070 format('GRID', a, '  : grid parameters:  dxmin  = ', 1p, g12.5 &
           , ', xrat = ', g12.5)
2080 format('GRID', a, '  : grid parameters:  dxmax  = ', 1p, g12.5 &
           , ', xrat = ', g12.5)
2100 format('GRID    : ', 1p, 5g14.6)
    !
  END SUBROUTINE makegrid
  !
  !======================================================================
  !
  SUBROUTINE allocate_gridx1

    allocate (x1ap(isap:ieap))
    allocate (dx1ap(isap:ieap))
    allocate (dvl1ap(isap:ieap))
    allocate (x1bp(isap:ieap))
    allocate (dx1bp(isap:ieap))
    allocate (dvl1bp(isap:ieap))
    allocate (x1apl(isap:ieap + 1))

    allocate (x1a(isa:iea))
    allocate (dx1a(isa:iea))
    allocate (dvl1a(isa:iea))
    allocate (g2a(isa:iea))
    allocate (g31a(isa:iea))

    allocate (dg2ad1(isa:iea))
    allocate (dg31ad1(isa:iea))

    allocate (x1b(isa:iea))
    allocate (dx1b(isa:iea))
    allocate (dvl1b(isa:iea))
    allocate (g2b(isa:iea))
    allocate (g31b(isa:iea))

    allocate (dg2bd1(isa:iea))
    allocate (dg31bd1(isa:iea))

    allocate (x1ai(isa:iea))
    allocate (dx1ai(isa:iea))
    allocate (dvl1ai(isa:iea))
    allocate (g2ai(isa:iea))
    allocate (g31ai(isa:iea))
    allocate (x1bi(isa:iea))
    allocate (dx1bi(isa:iea))
    allocate (dvl1bi(isa:iea))
    allocate (g2bi(isa:iea))
    allocate (g31bi(isa:iea))
    !
    !      Moving 1-grid variables:
    !
    allocate (x1ah(isa:iea))
    allocate (dx1ah(isa:iea))
    allocate (dvl1ah(isa:iea))
    allocate (g2ah(isa:iea))
    allocate (g31ah(isa:iea))
    allocate (x1bh(isa:iea))
    allocate (dx1bh(isa:iea))
    allocate (dvl1bh(isa:iea))
    allocate (g2bh(isa:iea))
    allocate (g31bh(isa:iea))
    allocate (x1an(isa:iea))
    allocate (dx1an(isa:iea))
    allocate (dvl1an(isa:iea))
    allocate (g2an(isa:iea))
    allocate (g31an(isa:iea))
    allocate (x1bn(isa:iea))
    allocate (dx1bn(isa:iea))
    allocate (dvl1bn(isa:iea))
    allocate (g2bn(isa:iea))
    allocate (g31bn(isa:iea))

    allocate (x1ahi(isa:iea))
    allocate (dx1ahi(isa:iea))
    allocate (dvl1ahi(isa:iea))
    allocate (g2ahi(isa:iea))
    allocate (g31ahi(isa:iea))
    allocate (x1bhi(isa:iea))
    allocate (dx1bhi(isa:iea))
    allocate (dvl1bhi(isa:iea))
    allocate (g2bhi(isa:iea))
    allocate (g31bhi(isa:iea))
    allocate (x1ani(isa:iea))
    allocate (dx1ani(isa:iea))
    allocate (dvl1ani(isa:iea))
    allocate (g2ani(isa:iea))
    allocate (g31ani(isa:iea))
    allocate (x1bni(isa:iea))
    allocate (dx1bni(isa:iea))
    allocate (dvl1bni(isa:iea))
    allocate (g2bni(isa:iea))
    allocate (g31bni(isa:iea))

    allocate (vg1(isa:iea))

    allocate (x1al(isa:iea + 1))

  END SUBROUTINE allocate_gridx1
  !
  !======================================================================
  !
  SUBROUTINE allocate_gridx2

    allocate (x2ap(jsap:jeap))
    allocate (dx2ap(jsap:jeap))
    allocate (dvl2ap(jsap:jeap))
    allocate (x2bp(jsap:jeap))
    allocate (dx2bp(jsap:jeap))
    allocate (dvl2bp(jsap:jeap))
    allocate (x2apl(jsap:jeap + 1))

    allocate (x2a(jsa:jea))
    allocate (dx2a(jsa:jea))
    allocate (dvl2a(jsa:jea))
    allocate (g32a(jsa:jea))

    allocate (dg32ad2(jsa:jea))

    allocate (x2b(jsa:jea))
    allocate (dx2b(jsa:jea))
    allocate (dvl2b(jsa:jea))
    allocate (g32b(jsa:jea))

    allocate (dg32bd2(jsa:jea))

    allocate (x2ai(jsa:jea))
    allocate (dx2ai(jsa:jea))
    allocate (dvl2ai(jsa:jea))
    allocate (g32ai(jsa:jea))
    allocate (x2bi(jsa:jea))
    allocate (dx2bi(jsa:jea))
    allocate (dvl2bi(jsa:jea))
    allocate (g32bi(jsa:jea))
    !
    !      Moving 2-grid variables:
    !
    allocate (x2ah(jsa:jea))
    allocate (dx2ah(jsa:jea))
    allocate (dvl2ah(jsa:jea))
    allocate (g32ah(jsa:jea))
    allocate (x2bh(jsa:jea))
    allocate (dx2bh(jsa:jea))
    allocate (dvl2bh(jsa:jea))
    allocate (g32bh(jsa:jea))
    allocate (x2an(jsa:jea))
    allocate (dx2an(jsa:jea))
    allocate (dvl2an(jsa:jea))
    allocate (g32an(jsa:jea))
    allocate (x2bn(jsa:jea))
    allocate (dx2bn(jsa:jea))
    allocate (dvl2bn(jsa:jea))
    allocate (g32bn(jsa:jea))

    allocate (x2ahi(jsa:jea))
    allocate (dx2ahi(jsa:jea))
    allocate (dvl2ahi(jsa:jea))
    allocate (g32ahi(jsa:jea))
    allocate (x2bhi(jsa:jea))
    allocate (dx2bhi(jsa:jea))
    allocate (dvl2bhi(jsa:jea))
    allocate (g32bhi(jsa:jea))
    allocate (x2ani(jsa:jea))
    allocate (dx2ani(jsa:jea))
    allocate (dvl2ani(jsa:jea))
    allocate (g32ani(jsa:jea))
    allocate (x2bni(jsa:jea))
    allocate (dx2bni(jsa:jea))
    allocate (dvl2bni(jsa:jea))
    allocate (g32bni(jsa:jea))

    allocate (vg2(jsa:jea))
    allocate (x2al(jsa:jea + 1))

  END SUBROUTINE allocate_gridx2
  !
  !======================================================================
  !
  SUBROUTINE allocate_gridx3

    allocate (x3ap(ksap:keap))
    allocate (dx3ap(ksap:keap))
    allocate (dvl3ap(ksap:keap))
    allocate (x3bp(ksap:keap))
    allocate (dx3bp(ksap:keap))
    allocate (dvl3bp(ksap:keap))
    allocate (x3apl(ksap:keap + 1))

    allocate (x3a(ksa:kea))
    allocate (dx3a(ksa:kea))
    allocate (dvl3a(ksa:kea))

    allocate (x3b(ksa:kea))
    allocate (dx3b(ksa:kea))
    allocate (dvl3b(ksa:kea))

    allocate (x3ai(ksa:kea))
    allocate (dx3ai(ksa:kea))
    allocate (dvl3ai(ksa:kea))
    allocate (x3bi(ksa:kea))
    allocate (dx3bi(ksa:kea))
    allocate (dvl3bi(ksa:kea))
    !
    !      Moving 3-grid variables:
    !
    allocate (x3ah(ksa:kea))
    allocate (dx3ah(ksa:kea))
    allocate (dvl3ah(ksa:kea))
    allocate (x3bh(ksa:kea))
    allocate (dx3bh(ksa:kea))
    allocate (dvl3bh(ksa:kea))
    allocate (x3an(ksa:kea))
    allocate (dx3an(ksa:kea))
    allocate (dvl3an(ksa:kea))
    allocate (x3bn(ksa:kea))
    allocate (dx3bn(ksa:kea))
    allocate (dvl3bn(ksa:kea))

    allocate (x3ahi(ksa:kea))
    allocate (dx3ahi(ksa:kea))
    allocate (dvl3ahi(ksa:kea))
    allocate (x3bhi(ksa:kea))
    allocate (dx3bhi(ksa:kea))
    allocate (dvl3bhi(ksa:kea))
    allocate (x3ani(ksa:kea))
    allocate (dx3ani(ksa:kea))
    allocate (dvl3ani(ksa:kea))
    allocate (x3bni(ksa:kea))
    allocate (dx3bni(ksa:kea))
    allocate (dvl3bni(ksa:kea))

    allocate (vg3(ksa:kea))

    allocate (x3al(ksa:kea + 1))

  END SUBROUTINE allocate_gridx3
  !
  !======================================================================
  !
  SUBROUTINE grid_e
    CHARACTER(LEN=*), PARAMETER :: routinename = 'z_grid_e'
    !
    !  Initializes the extended x1ap-grid as specified by the parameters in
    !  the "ggen" namelists.
    !
    ! This grid extends x1a both up and down, potentially replacing the original
    ! ghost zones.  Indexing coincides where x1a and x1a_e coincide, which is
    ! from isp to iep (ideally, they should coincide for iep+1 too, but that's
    ! neither enforced not checked).

    CHARACTER(LEN=messagelinelength) message

    INTEGER :: lower_active, upper_active

    if (nnodes1*nnodes2*nnodes3 > 1) then
      call threeoutput(routinename, &
                       'Parallel gp_e code not yet completely tested')
    end if

    ! Obtain the lower active size

    ! Deal with the case of absence of l1 or u1;
    ! it is desirable to keep the old ghost zones in such a case.
    ! Optionally, overwrite relevant ghost zones
    ! (which requires rerunning gsetx1).

    if (lmax_lower < 0) then
      lower_active = 0
    else
      lower_active = get_array_size('l1')
      if (lower_active < 0) call finalstop(routinename, &
                                           'Grid l1 has negative size')
      if (lower_active <= nghosts) &
        call warnoutput(routinename, 'Grid l1 is small')
      if (lower_active > 1e7) call warnoutput(routinename, 'Grid l1 is large')
    end if
    if (lmax_upper < 0) then
      upper_active = 0
    else
      upper_active = get_array_size('u1')
      if (upper_active < 0) call finalstop(routinename, &
                                           'Grid u1 has negative size')
      if (upper_active <= nghosts) &
        call warnoutput(routinename, 'Grid u1 is small')
      if (upper_active > 1e7) call warnoutput(routinename, 'Grid u1 is large')
    end if

    ! array bounds for global arrays
    if (lower_active == 0) then
      isap_e = isap
    else
      isap_e = isp - lower_active
    end if
    if (upper_active == 0) then
      ieap_e = ieap
    else
      ieap_e = iep + upper_active
    end if

    ! array bounds for local arrays
    if (lower_active == 0 .or. .not. l_firstnode1) then
      isa_e = isa
    else
      isa_e = is - lower_active
    end if
    if (upper_active == 0 .or. .not. l_lastnode1) then
      iea_e = iea
    else
      iea_e = ie + upper_active
    end if

    ! main gravity do loop bounds
    if (lower_active == 0 .or. .not. l_firstnode1) then
      is_e = is
    else
      is_e = isa_e + 1
    end if
    if (upper_active == 0 .or. .not. l_lastnode1) then
      ie_e = ie
    else
      ie_e = iea_e - 1
    end if

    ! main gravity boundary ('ghost') zones - just one layer
    isg_e = is_e - 1
    ieg_e = ie_e + 1

    ! Report the sizes and bounds found and other useful information
    write (message, *) 'lower_active', lower_active
    call threeoutput(routinename//'_lower_active', message)
    write (message, *) 'upper_active', upper_active
    call threeoutput(routinename//'_upper_active', message)

    write (message, *) 'isap_e', isap_e
    call threeoutput(routinename//'_isap_e', message)
    write (message, *) 'ieap_e', ieap_e
    call threeoutput(routinename//'_ieap_e', message)

    write (message, *) 'isa_e', isa_e
    call threeoutput(routinename//'_isa_e', message)
    write (message, *) 'iea_e', iea_e
    call threeoutput(routinename//'_iea_e', message)
    write (message, *) 'is_e', is_e
    call threeoutput(routinename//'_is_e', message)
    write (message, *) 'ie_e', ie_e
    call threeoutput(routinename//'_ie_e', message)
    write (message, *) 'isg_e', isg_e
    call threeoutput(routinename//'_isg_e', message)
    write (message, *) 'ieg_e', ieg_e
    call threeoutput(routinename//'_ieg_e', message)

    ! Set up pointers if grid extension is unnecessary, then leave
    if (.not. withextendedgrid) then
      x1ap_e => x1ap
      dx1ap_e => dx1ap
      x1bp_e => x1bp
      dx1bp_e => dx1bp

      x1a_e => x1a
      dx1a_e => dx1a
      x1b_e => x1b
      dx1b_e => dx1b

      x1ai_e => x1ai
      dx1ai_e => dx1ai
      x1bi_e => x1bi
      dx1bi_e => dx1bi
      return
    end if

    ! Allocate primary and secondary extended 1-grid arrays
    call allocate_grid_e

    !      Set up a full x1-grid.

    if (lower_active /= 0) then
      call makegrid(x1ap_e(isap_e:isp), isap_e, isp, isap_e, isp, 'l1')
      if (ghost_override) x1ap(isap:isp - 1) = x1ap_e(isap:isp - 1)
    else
      x1ap_e(isap_e:isp) = x1ap(isap:isp)
    end if

    x1ap_e(isp:iep) = x1ap(isp:iep)

    if (upper_active /= 0) then
      call makegrid(x1ap_e(iep + 1:ieap_e), iep + 1, ieap_e, iep + 1, &
                    ieap_e, 'u1')
      if (ghost_override) x1ap(iep + 1:ieap) = x1ap_e(iep + 1:ieap)
    else
      x1ap_e(iep + 1:ieap_e) = x1ap(iep + 1:ieap)
    end if

    if (ghost_override) then
      if (lower_active /= 0 .or. upper_active /= 0) then
        call gsetx1
      end if
    end if

    !      Main extended grid in 1-direction is complete.

    !      Set up the secondary extended grid.
    call gset_e

  END SUBROUTINE grid_e
  !
  !======================================================================
  !
  SUBROUTINE gset_e
    !
    INTEGER i
    INTEGER istart_pe, iend_pe
    ! indices on the extended parallel grid
    ! corresponding to the local extended grid indices isa_e and iea_e

    do i = isap_e, ieap_e - 1
      dx1ap_e(i) = x1ap_e(i + 1) - x1ap_e(i)
    end do
    dx1ap_e(ieap_e) = dx1ap_e(ieap_e - 1)

    do i = isap_e, ieap_e
      x1bp_e(i) = x1ap_e(i) + haf*dx1ap_e(i)
    end do
    do i = isap_e + 1, ieap_e
      dx1bp_e(i) = x1bp_e(i) - x1bp_e(i - 1)
    end do
    dx1bp_e(isap_e) = dx1ap_e(isap_e + 1)

    !
    !      Set local arrays
    !

    if (l_firstnode1) then
      istart_pe = isap_e
    else
      istart_pe = isa_e + node1*inac
    end if

    if (l_lastnode1) then
      iend_pe = ieap_e
    else
      iend_pe = iea_e + node1*inac
    end if

    x1a_e(isa_e:iea_e) = x1ap_e(istart_pe:iend_pe)
    dx1a_e(isa_e:iea_e) = dx1ap_e(istart_pe:iend_pe)
    x1b_e(isa_e:iea_e) = x1bp_e(istart_pe:iend_pe)
    dx1b_e(isa_e:iea_e) = dx1bp_e(istart_pe:iend_pe)

    do i = isa_e, iea_e
      x1ai_e(i) = one/(x1a_e(i) + verysmall)
      dx1bi_e(i) = one/(dx1b_e(i) + verysmall)
      x1bi_e(i) = one/(x1b_e(i) + verysmall)
      dx1ai_e(i) = one/(dx1a_e(i) + verysmall)
    end do
    !
  END SUBROUTINE gset_e
  !
  !======================================================================
  !
  SUBROUTINE allocate_grid_e
    ! This is probably more arrays than strictly necessary for now.

    allocate (x1ap_e(isap_e:ieap_e))
    allocate (dx1ap_e(isap_e:ieap_e))
    allocate (x1bp_e(isap_e:ieap_e))
    allocate (dx1bp_e(isap_e:ieap_e))

    allocate (x1a_e(isa_e:iea_e))
    allocate (dx1a_e(isa_e:iea_e))
    allocate (x1b_e(isa_e:iea_e))
    allocate (dx1b_e(isa_e:iea_e))

    allocate (x1ai_e(isa_e:iea_e))
    allocate (dx1ai_e(isa_e:iea_e))
    allocate (x1bi_e(isa_e:iea_e))
    allocate (dx1bi_e(isa_e:iea_e))

  END SUBROUTINE allocate_grid_e
  !
  !======================================================================
  !
END MODULE zeus_grid_initial
