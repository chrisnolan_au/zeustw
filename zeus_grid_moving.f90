#include "macros"
MODULE zeus_grid_moving
  USE zeus_io
  USE zeus_variables
  IMPLICIT NONE
  PRIVATE
  PUBLIC :: movegrid, movegrid_init

CONTAINS

  SUBROUTINE movegrid
    call newgrid
  END SUBROUTINE movegrid

  SUBROUTINE movegrid_init
    call newgrid_init
  END SUBROUTINE movegrid_init

  SUBROUTINE newgrid_init
    ! Read namelist values
    rewind (ioin)
    read (ioin, gcon)
    ! Currently (2007-02-19) all possible namelist values make
    ! some kind of sense, so accept them all without overriding,
    ! and send the result to the log files and tty.
    if (iolog /= NO_OUTPUT) write (iolog, gcon)
    if (iotty /= NO_OUTPUT) write (iotty, gcon)

    ! Set grid motion arrays to zero
    vg1 = zro
    vg2 = zro
    vg3 = zro
  END SUBROUTINE newgrid_init

  SUBROUTINE newgrid
    CHARACTER(LEN=*), PARAMETER :: routinename = 'z_newgrid'
    ! If no grid motion is used, return.
    if (.not. (moving_grid1 .or. moving_grid2 .or. moving_grid3)) return

    if (alfven_dt_floor_forcelimiter) then
      call finalstop(routinename, 'Grid motion incompatible with force' &
                     //' limiter implementation')
    end if

    ! Update grid velocities.
    call newvg

    ! Update x1-grid, x2-grid, and x3-grid
    if (moving_grid1 .and. inac /= 0) call newx1
    if (moving_grid2 .and. jnac /= 0 .and. withv2) call newx2
    if (moving_grid3 .and. knac /= 0 .and. withv3) call newx3

    ! Update grid functions peculiar to ppa interpolation algorithm
    ! if (ippa == 1) call ppafnc
    ! Disconnected, together with the rest of PPA
  END SUBROUTINE newgrid

  SUBROUTINE newvg
    ! Currently only a Lagrangian-like tracking is kept.
    ! That needs only a small fraction of the apparatus, and so most is excised.
    ! The averaging is replaced by accepting directly the first zone value;
    ! that is intended to be used only in 1D.  For 2D work, our situation is
    ! in any case too complicated for the average used in Zeus3D to have
    ! worked at all anyway.

    !
    !  Lagrangian-like tracking of grid lines.
    !  In 1-D this formalism gives the real Lagrangian tracking.
    !
    if (moving_grid1 .and. inac /= 0) vg1 = v1(:, js, ks)
    if (moving_grid2 .and. jnac /= 0 .and. withv2) vg2 = v2(is, :, ks)
    if (moving_grid3 .and. knac /= 0 .and. withv3) vg3 = v3(is, js, :)
  END SUBROUTINE newvg

  SUBROUTINE newx1
    !
    !    jms:zeus3d.newx1 <----------------------- updates x1 grid variables
    !                                                         november, 1987
    !
    !    written by: Jim Stone
    !    modified 1: June, 1988 by Jim Stone; incorporated into ZEUS2D
    !    modified 2: January, 1990 by David Clarke; incorporated into ZEUS3D
    !    modified 3: 2007-02-19 by RFK: incorporated into Zeus36
    !                Calculation of secondary grids not parallel
    !
    !  PURPOSE: Computes "new" x1 grid variables (grid variables at the
    !  advanced time) to be used in TRANSPRT.  Grid values are calculated
    !  for i = isg to ieg.

    INTEGER i
    REALNUM, DIMENSION(isa:iea) :: vol1ah, vol1bh, vol1an, vol1bn

    !-----------------------------------------------------------------------
    !
    !      Update grid with "new" grid from previous timestep.
    !
    x1a = x1an
    x1b = x1bn
    dx1a = dx1an
    dx1b = dx1bn
    g2a = g2an
    g2b = g2bn
    g31a = g31an
    g31b = g31bn
    dvl1a = dvl1an
    dvl1b = dvl1bn
    !
    x1ai = x1ani
    x1bi = x1bni
    dx1ai = dx1ani
    dx1bi = dx1bni
    g2ai = g2ani
    g2bi = g2bni
    g31ai = g31ani
    g31bi = g31bni
    dvl1ai = dvl1ani
    dvl1bi = dvl1bni
    !
    !      Update a- and b-grids with grid velocity in 1-direction.
    !
    ! Primary grids
    x1ah = x1a + vg1*dt*0.5
    x1an = x1a + vg1*dt
    ! Secondary grids (not parallel)
    do i = isg, iego   ! Observe that ieg values are not calculated
      dx1ah(i) = x1ah(i + 1) - x1ah(i)
      dx1an(i) = x1an(i + 1) - x1an(i)
    end do
    do i = isg, iego   ! Observe that ieg values are not calculated
      x1bh(i) = x1ah(i) + 0.5*dx1ah(i)
      x1bn(i) = x1an(i) + 0.5*dx1an(i)
    end do
    dx1bh(isg) = dx1ah(isg)
    dx1bn(isg) = dx1an(isg)
    do i = isgo, iego
      dx1bh(i) = x1bh(i) - x1bh(i - 1)
      dx1bn(i) = x1bn(i) - x1bn(i - 1)
    end do
    dx1bh(ieg) = dx1ah(iego)
    dx1bn(ieg) = dx1an(iego)
    ! Update metric factors and zone volumes
    if (geometry == 'xyz' .or. geometry == 'zrp') then
      do i = isg, iego
        g2bh(i) = 1.0
        g2bn(i) = 1.0
        g31bh(i) = 1.0
        g31bn(i) = 1.0
        vol1bh(i) = x1bh(i)
        vol1bn(i) = x1bn(i)
      end do
      do i = isg, ieg
        g2ah(i) = 1.0
        g2an(i) = 1.0
        g31ah(i) = 1.0
        g31an(i) = 1.0
        vol1ah(i) = x1ah(i)
        vol1an(i) = x1an(i)
      end do
    end if
    if (geometry == 'rtp' .or. geometry == 'rmp') then
      do i = isg, iego
        g2bh(i) = x1bh(i)
        g2bn(i) = x1bn(i)
        g31bh(i) = x1bh(i)
        g31bn(i) = x1bn(i)
        vol1bh(i) = x1bh(i)**3/3.0
        vol1bn(i) = x1bn(i)**3/3.0
      end do
      do i = isg, ieg
        g2ah(i) = x1ah(i)
        g2an(i) = x1an(i)
        g31ah(i) = x1ah(i)
        g31an(i) = x1an(i)
        vol1ah(i) = x1ah(i)**3/3.0
        vol1an(i) = x1an(i)**3/3.0
      end do
    end if

    !
    !  Update volume factors used in integral form of difference
    !  equations and in transport module.
    !
    !  Details of ghost zone treatment and counting might need checking.
    !  Consider imitating that and the treatment of parallelization for
    !  the initialization of secondary grids.
    !
    do i = ism1, iep2
      dvl1bh(i) = vol1bh(i) - vol1bh(i - 1)
      dvl1bn(i) = vol1bn(i) - vol1bn(i - 1)
    end do
    do i = isg, ism2
      dvl1bh(i) = dvl1bh(ism1)
      dvl1bn(i) = dvl1bn(ism1)
    end do
    do i = iep2 + ione, ieg
      dvl1bh(i) = dvl1bh(iep2)
      dvl1bn(i) = dvl1bn(iep2)
    end do
    do i = isg, iego
      dvl1ah(i) = vol1ah(i + 1) - vol1ah(i)
      dvl1an(i) = vol1an(i + 1) - vol1an(i)
    end do
    !
    !      Update inverse quantities.
    !
    do i = isg, iego
      x1bhi(i) = 1.0/(x1bh(i) + verysmall)
      dx1ahi(i) = 1.0/(dx1ah(i) + verysmall)
      g2bhi(i) = 1.0/(g2bh(i) + verysmall)
      g31bhi(i) = 1.0/(g31bh(i) + verysmall)
      dvl1ahi(i) = 1.0/(dvl1ah(i) + verysmall)
      x1bni(i) = 1.0/(x1bn(i) + verysmall)
      dx1ani(i) = 1.0/(dx1an(i) + verysmall)
      g2bni(i) = 1.0/(g2bn(i) + verysmall)
      g31bni(i) = 1.0/(g31bn(i) + verysmall)
      dvl1ani(i) = 1.0/(dvl1an(i) + verysmall)
    end do
    do i = isg, ieg
      x1ahi(i) = 1.0/(x1ah(i) + verysmall)
      dx1bhi(i) = 1.0/(dx1bh(i) + verysmall)
      g2ahi(i) = 1.0/(g2ah(i) + verysmall)
      g31ahi(i) = 1.0/(g31ah(i) + verysmall)
      dvl1bhi(i) = 1.0/(dvl1bh(i) + verysmall)
      x1ani(i) = 1.0/(x1an(i) + verysmall)
      dx1bni(i) = 1.0/(dx1bn(i) + verysmall)
      g2ani(i) = 1.0/(g2an(i) + verysmall)
      g31ani(i) = 1.0/(g31an(i) + verysmall)
      dvl1bni(i) = 1.0/(dvl1bn(i) + verysmall)
    end do
  END SUBROUTINE newx1

  SUBROUTINE newx2
    INTEGER j
    REALNUM, DIMENSION(jsa:jea) :: vol2ah, vol2bh, vol2an, vol2bn
    x2a = x2an
    x2b = x2bn
    dx2a = dx2an
    dx2b = dx2bn
    g32a = g32an
    g32b = g32bn
    dvl2a = dvl2an
    dvl2b = dvl2bn

    x2ai = x2ani
    x2bi = x2bni
    dx2ai = dx2ani
    dx2bi = dx2bni
    g32ai = g32ani
    g32bi = g32bni
    dvl2ai = dvl2ani
    dvl2bi = dvl2bni

    ! Primary grids
    x2ah = x2a + vg2*dt*0.5
    x2an = x2a + vg2*dt
    ! Secondary grids (not parallel)
    do j = jsg, jego   ! Observe that jeg values are not calculated
      dx2ah(j) = x2ah(j + 1) - x2ah(j)
      dx2an(j) = x2an(j + 1) - x2an(j)
    end do
    do j = jsg, jego   ! Observe that jeg values are not calculated
      x2bh(j) = x2ah(j) + 0.5*dx2ah(j)
      x2bn(j) = x2an(j) + 0.5*dx2an(j)
    end do
    dx2bh(jsg) = dx2ah(jsg)
    dx2bn(jsg) = dx2an(jsg)
    do j = jsgo, jego
      dx2bh(j) = x2bh(j) - x2bh(j - 1)
      dx2bn(j) = x2bn(j) - x2bn(j - 1)
    end do
    dx2bh(jeg) = dx2ah(jego)
    dx2bn(jeg) = dx2an(jego)

    ! Update metric factors and zone volumes.
    if (geometry == 'xyz') then
      do j = jsg, jego
        g32bh(j) = 1.0
        g32bn(j) = 1.0
        vol2bh(j) = x2bh(j)
        vol2bn(j) = x2bn(j)
      end do
      do j = jsg, jeg
        g32ah(j) = 1.0
        g32an(j) = 1.0
        vol2ah(j) = x2ah(j)
        vol2an(j) = x2an(j)
      end do
    end if
    if (geometry == 'zrp') then
      do j = jsg, jego
        g32bh(j) = x2bh(j)
        g32bn(j) = x2bn(j)
        vol2bh(j) = x2bh(j)**2/2.0
        vol2bn(j) = x2bn(j)**2/2.0
      end do
      do j = jsg, jeg
        g32ah(j) = x2ah(j)
        g32an(j) = x2an(j)
        vol2ah(j) = x2ah(j)**2/2.0
        vol2an(j) = x2an(j)**2/2.0
      end do
    end if
    if (geometry == 'rtp') then
      do j = jsg, jego
        g32bh(j) = sin(x2bh(j))
        g32bn(j) = sin(x2bn(j))
        vol2bh(j) = -cos(x2bh(j))
        vol2bn(j) = -cos(x2bn(j))
        dg32bd2(j) = cos(x2b(j)) ! Think the time-centering of this and the others
      end do
      do j = jsg, jeg
        g32ah(j) = sin(x2ah(j))
        g32an(j) = sin(x2an(j))
        vol2ah(j) = -cos(x2ah(j))
        vol2an(j) = -cos(x2an(j))
        dg32ad2(j) = cos(x2a(j))
      end do
    end if
    if (geometry == 'rmp') then
      do j = jsg, jego
        g32bh(j) = cos(x2bh(j))
        g32bn(j) = cos(x2bn(j))
        vol2bh(j) = sin(x2bh(j))
        vol2bn(j) = sin(x2bn(j))
        dg32bd2(j) = -sin(x2b(j))
      end do
      do j = jsg, jeg
        g32ah(j) = cos(x2ah(j))
        g32an(j) = cos(x2an(j))
        vol2ah(j) = sin(x2ah(j))
        vol2an(j) = sin(x2an(j))
        dg32ad2(j) = -sin(x2a(j))
      end do
    end if

    do j = jsm1, jep2
      dvl2bh(j) = vol2bh(j) - vol2bh(j - 1)
      dvl2bn(j) = vol2bn(j) - vol2bn(j - 1)
    end do
    do j = jsg, jsm2
      dvl2bh(j) = dvl2bh(jsm1)
      dvl2bn(j) = dvl2bn(jsm1)
    end do
    do j = jep2 + jone, jeg
      dvl2bh(j) = dvl2bh(jep2)
      dvl2bn(j) = dvl2bn(jep2)
    end do
    do j = jsg, jego
      dvl2ah(j) = vol2ah(j + 1) - vol2ah(j)
      dvl2an(j) = vol2an(j + 1) - vol2an(j)
    end do
    !
    !      Update inverse quantities.
    !
    do j = jsg, jego
      x2bhi(j) = 1.0/(x2bh(j) + verysmall)
      dx2ahi(j) = 1.0/(dx2ah(j) + verysmall)
      g32bhi(j) = 1.0/(g32bh(j) + verysmall)
      dvl2ahi(j) = 1.0/(dvl2ah(j) + verysmall)
      x2bni(j) = 1.0/(x2bn(j) + verysmall)
      dx2ani(j) = 1.0/(dx2an(j) + verysmall)
      g32bni(j) = 1.0/(g32bn(j) + verysmall)
      dvl2ani(j) = 1.0/(dvl2an(j) + verysmall)
    end do
    do j = jsg, jego
      x2ahi(j) = 1.0/(x2ah(j) + verysmall)
      dx2bhi(j) = 1.0/(dx2bh(j) + verysmall)
      g32ahi(j) = 1.0/(g32ah(j) + verysmall)
      dvl2bhi(j) = 1.0/(dvl2bh(j) + verysmall)
      x2ani(j) = 1.0/(x2an(j) + verysmall)
      dx2bni(j) = 1.0/(dx2bn(j) + verysmall)
      g32ani(j) = 1.0/(g32an(j) + verysmall)
      dvl2bni(j) = 1.0/(dvl2bn(j) + verysmall)
    end do
  END SUBROUTINE newx2

  SUBROUTINE newx3
    INTEGER k
    REALNUM, DIMENSION(ksa:kea) :: vol3ah, vol3bh, vol3an, vol3bn
    !      Update grid with "new" grid from previous timestep.
    x3a = x3an
    x3b = x3bn
    dx3a = dx3an
    dx3b = dx3bn
    dvl3a = dvl3an
    dvl3b = dvl3bn
!
    x3ai = x3ani
    x3bi = x3bni
    dx3ai = dx3ani
    dx3bi = dx3bni
    dvl3ai = dvl3ani
    dvl3bi = dvl3bni
    ! Update a- and b-grids with grid velocity in 3-direction.
    ! Primary grids
    x3ah = x3a + vg3*dt*0.5
    x3an = x3a + vg3*dt
    ! Secondary grids (not parallel)
    do k = ksg, kego   ! Observe that keg values are not calculated
      dx3ah(k) = x3ah(k + 1) - x3ah(k)
      dx3an(k) = x3an(k + 1) - x3an(k)
    end do
    do k = ksg, kego   ! Observe that keg values are not calculated
      x3bh(k) = x3ah(k) + 0.5*dx3ah(k)
      x3bn(k) = x3an(k) + 0.5*dx3an(k)
    end do
    dx3bh(ksg) = dx3ah(ksg)
    dx3bn(ksg) = dx3an(ksg)
    do k = ksgo, kego
      dx3bh(k) = x3bh(k) - x3bh(k - 1)
      dx3bn(k) = x3bn(k) - x3bn(k - 1)
    end do
    dx3bh(keg) = dx3ah(kego)
    dx3bn(keg) = dx3an(kego)

    ! Update zone volumes.
    do k = ksg, kego
      vol3bh(k) = x3bh(k)
      vol3bn(k) = x3bn(k)
    end do
    do k = ksg, keg
      vol3ah(k) = x3ah(k)
      vol3an(k) = x3an(k)
    end do
    !  Update volume factors used in integral form of difference
    !  equations and in transport module.
    do k = ksm1, kep2
      dvl3bh(k) = vol3bh(k) - vol3bh(k - 1)
      dvl3bn(k) = vol3bn(k) - vol3bn(k - 1)
    end do
    do k = ksg, ksm2
      dvl3bh(k) = dvl3bh(ksm1)
      dvl3bn(k) = dvl3bn(ksm1)
    end do
    do k = kep2 + kone, keg
      dvl3bh(k) = dvl3bh(kep2)
      dvl3bn(k) = dvl3bn(kep2)
    end do
    do k = ksg, kego
      dvl3ah(k) = vol3ah(k + 1) - vol3ah(k)
      dvl3an(k) = vol3an(k + 1) - vol3an(k)
    end do
    !
    !      Update inverse quantities.
    !
    do k = ksg, kego
      x3bhi(k) = 1.0/(x3bh(k) + verysmall)
      dx3ahi(k) = 1.0/(dx3ah(k) + verysmall)
      dvl3ahi(k) = 1.0/(dvl3ah(k) + verysmall)
      x3bni(k) = 1.0/(x3bn(k) + verysmall)
      dx3ani(k) = 1.0/(dx3an(k) + verysmall)
      dvl3ani(k) = 1.0/(dvl3an(k) + verysmall)
    end do
    do k = ksg, keg
      x3ahi(k) = 1.0/(x3ah(k) + verysmall)
      dx3bhi(k) = 1.0/(dx3bh(k) + verysmall)
      dvl3bhi(k) = 1.0/(dvl3bh(k) + verysmall)
      x3ani(k) = 1.0/(x3an(k) + verysmall)
      dx3bni(k) = 1.0/(dx3bn(k) + verysmall)
      dvl3bni(k) = 1.0/(dvl3bn(k) + verysmall)
    end do
  END SUBROUTINE newx3

END MODULE zeus_grid_moving
