#include "macros"
MODULE zeus_initial
  USE zeus_variables
  USE zeus_io
  USE zeus_grid_initial
  IMPLICIT NONE
  PRIVATE
  PUBLIC :: mstart
CONTAINS
  !** GROUP: INITIAL *****************************************************
  !**                                                                   **
  !**                   G R O U P :   I N I T I A L                     **
  !**                                                                   **
  !***********************************************************************
  !
  !=======================================================================
  !
  SUBROUTINE mstart
    USE zeus_mpi
    USE zeus_deadcode
    USE zeus_timer
    USE zeus_grid_moving
#ifdef MPI
    USE mpi
    INTEGER ierr
#endif /* MPI */
    CHARACTER(LEN=*), PARAMETER :: routinename = 'z_mstart'
    CHARACTER(LEN=messagelinelength) message
    INTEGER keepwarn
    INTEGER nnodes_total

#ifdef MPI
    !======================================================================
    ! Initialize MPI if needed, and obtain the true or token node rank.
    !
    INTEGER :: nodenamelength
    CHARACTER(LEN=8 + 2*formatlength) nodeformat

    call MPI_INIT(ierr)
    call MPI_COMM_RANK(MPI_COMM_WORLD, node, ierr)
    call MPI_COMM_SIZE(MPI_COMM_WORLD, nnodes_total, ierr)
    if (nnodes_total > 1) then
      nodenamelength = LEN(nodename(2:))   ! Length not counting the initial _.
      write (nodeformat, format_format) &
        '("_",i', nodenamelength, '.', nodenamelength, ')'
      write (nodename, nodeformat) node
    else
      ! A nodename made out of blanks, to be trimmed away.
      ! Making it zero length would require a character string of length
      ! defined at runtime, which is not currently done.
      nodename = ' '
    end if
#else /* MPI */
    !
    ! Token value for the node rank and name
    !
    node = rootnode
    nodename = ''
    nnodes_total = 1
#endif /* MPI */

    !======================================================================
    ! Start the timer
    call timer_start ! After MPI_INIT, because the timer might use MPI_WTIME()

    !======================================================================
    ! Read and adjust the namelist values, and the quantities
    ! directly dependent from them.
    !

    !
    !  Open the file containining the input namelists
    !
    open (unit=ioin, file='inzeus', status='old', &
          action='read', position='rewind')
    !
    !----------------------------  I/O CONTROL  ----------------------------
    !
    rewind (ioin)
    read (ioin, iocon)
    !
    ! Correct some values
    !
    if (iolog == iotty) iotty = NO_OUTPUT  ! avoid duplicate unit names
    if (iolog == iowarn) iowarn = NO_OUTPUT  ! avoid duplicate unit names
    if (node /= rootnode) then   ! only one tty output (usually standard)
      iotty = NO_OUTPUT
    end if
    !
    !      Open log output file
    !
    if (iolog /= NO_OUTPUT) then
      open (unit=iolog, &
            file='z_log'//trim(nodename), &
            status='new', action='write')
    end if
    ! Open warn output file
    if (iowarn /= NO_OUTPUT) then
      open (unit=iowarn, &
            file='z_warning'//trim(nodename), &
            status='new', action='write')
    end if

    if (time_factor <= zro) then
      select case (time_unit)
      case ('year', 'yr')
        time_factor = 365.25_pk*24.0_pk*3600.0_pk
      case ('day', 'd')
        time_factor = 24.0_pk*3600.0_pk
      case ('hour', 'h')
        time_factor = 3600.0_pk
      case ('minute', 'min')
        time_factor = 60.0_pk
      case ('sec', 's')
        time_factor = one
      case default
        time_factor = one
      end select
    end if

    !
    if (iolog /= NO_OUTPUT) write (iolog, iocon)
    if (iotty /= NO_OUTPUT) write (iotty, iocon)
    !
    !-----------------  GRID CONTROL  -------------------
    !
    rewind (ioin)
    read (ioin, gridcon)
    !
    ! Check consistency of the number of processors
    !

#ifndef MPI
    if (nnodes1 /= 1 .or. nnodes2 /= 1 .or. nnodes3 /= 1) then
      call warnoutput(routinename, &
                      'MPI needed for multiprocessor operation!')
      call threeoutput(routinename, 'Execution continuing, with the number of' &
                       //' processors set to 1.')
      nnodes1 = 1
      nnodes2 = 1
      nnodes3 = 1
    end if
#endif /* MPI */
    if (nnodes1 < 1 .or. nnodes2 < 1 .or. nnodes3 < 1) then
      call warnoutput(routinename, &
                      'All processor dimensions must be strictly positive!')
      call finalstop(routinename, 'Execution stops.')
    end if

    if (nnodes1*nnodes2*nnodes3 /= nnodes_total) then
      call finalstop(routinename, 'Number of processes mismatch.  Abort.')
    end if

    call lowercase(geometry)

    select case (geometry)
    case ('xyz', 'zrp', 'rtp', 'rmp')   ! All these are OK, do nothing.
    case default
      geometry = 'xyz'
      call warnoutput(routinename, &
                      "Value of geometry currently not assigned"// &
                      " or not in use.  Adjusted to 'xyz'")
    end select
    !
    ! Set up many of the MPI variables, adjust gridcon variables if needed.
    !
    call mpisetup

    if (iolog /= NO_OUTPUT) write (iolog, gridcon)
    if (iotty /= NO_OUTPUT) write (iotty, gridcon)

    !
    !-----  PROBLEM TOGGLES, AND MPI SETUP ----------------
    !
    rewind (ioin)
    read (ioin, parcon)

    if (ienergy < -1 .or. ienergy > 2) then
      ienergy = min(2, max(-1, ienergy))
      call warnoutput(routinename, &
                      'Value of ienergy currently not assigned'// &
                      'or not in use. Adjusted to closest available.')
    end if

    if (imhd < 0 .or. imhd > 3) then
      imhd = min(3, max(0, imhd))
      call warnoutput(routinename, &
                      'Value of imhd currently not assigned'// &
                      'or not in use. Adjusted to closest available.')
    end if

    if (mhddimension < 2 .or. mhddimension > 3) then
      mhddimension = min(3, max(2, mhddimension))
      call warnoutput(routinename, &
                      'Value of mhddimension currently not assigned'// &
                      'or not in use. Adjusted to closest available.')
    end if

    if (sweepdimension < 2 .or. sweepdimension > 3) then
      sweepdimension = min(3, max(2, sweepdimension))
      call warnoutput(routinename, &
                      'Value of sweepdimension currently not assigned'// &
                      'or not in use. Adjusted to closest available.')
    end if

    if ((mhddimension == 2 .or. sweepdimension == 2) .and. &
        get_array_size('x3') /= 0) then
      mhddimension = 3
      sweepdimension = 3
      call warnoutput(routinename, 'No x3-symmetry: mhddimension and' &
                      //' sweepdimension adjusted to 3')
    end if

    if (imhd == 0) then
      if (alfven_dt_floor > zro) then
        alfven_dt_floor = zro
        call threeoutput(routinename, 'Variable alfven_dt_floor set to zero' &
                         //' for a hydro problem')
      end if
      if (withb3) then
        withb3 = .false.
        call threeoutput(routinename, &
                         'Variable withb3 set to false for a hydro problem')
      end if
      if (withbp) then
        withbp = .false.
        call threeoutput(routinename, &
                         'Variable withbp set to false for a hydro problem')
      end if
      if (withb3bc) then
        withb3bc = .false.
        call threeoutput(routinename, &
                         'Variable withb3bc set to false for a hydro problem')
      end if
      if (withchem) then
        withchem = .false.
        call threeoutput(routinename, &
                         'Variable withchem set to false for a hydro problem')
      end if
    end if

    if (alfven_dt_floor == zro) then
      alfven_dt_floor_forcelimiter = .false.
      call threeoutput(routinename, 'Variable alfven_dt_floor_forcelimiter' &
                       //' set to false when alfven_dt_floor is turned off')
    end if

    if (alfven_dt_floor_forcelimiter) then
      if (alfven_dt_floor_conservative) then
        alfven_dt_floor_conservative = .false.
        call threeoutput(routinename, 'Variable alfven_dt_floor_conservative' &
                         //' set to false when the force limiter is turned on')
      end if
      if (alfven_dt_floor_vkill >= zro) then
        alfven_dt_floor_vkill = -one
        call threeoutput(routinename, 'Variable alfven_dt_floor_vkill set to' &
                         //' negative (disabled) when the force limiter is' &
                         //' turned on')
      end if
    end if

    if (mhddimension == 2 .and. imhd /= 0) then
      if (withbp) then
        mhddimension = 3
        call threeoutput(routinename, &
                         'Variable mhddimension set to 3 for a problem with bp')
      end if
    end if

    if (mhddimension == 3 .and. imhd /= 0) then
      if (.not. withv2) then
        withv2 = .true.
        call threeoutput(routinename, 'Variable withv2 set to true for a' &
                         //' problem with mhddimension=3')
      end if
      if (.not. withv3) then
        withv3 = .true.
        call threeoutput(routinename, 'Variable withv3 set to true for a' &
                         //' problem with mhddimension=3')
      end if
      if (.not. withb3) then
        withb3 = .true.
        call threeoutput(routinename, 'Variable withb3 set to true for a' &
                         //' problem with mhddimension=3')
      end if
      if (.not. withbp) then
        withbp = .true.
        call threeoutput(routinename, 'Variable withbp set to true for a' &
                         //' problem with mhddimension=3')
      end if
    end if

    if (.not. withfloormass) then
      withfloormass = .true.
      call threeoutput(routinename, &
                       'Variable withfloormass set to true')
    end if
    if (.not. withboundarymass) then
      withboundarymass = .true.
      call threeoutput(routinename, &
                       'Variable withboundarymass set to true')
    end if

    if (withb3bc .and. .not. withb3) then
      withb3bc = .false.
      call threeoutput(routinename, 'Variable withb3bc set to false for a' &
                       //' problem without b3')
    end if

    if (withb3bc .and. get_array_size('x3') /= 0) then
      call warnoutput(routinename, 'Check for magnetic monopoles: withb3bc' &
                      //' used without x3-symmetry.')
    end if

    if (alfven_dt_floor_vkill >= zro) then
      if (alfven_dt_floor_conservative) then
        alfven_dt_floor_conservative = .false.
        call threeoutput(routinename, &
                         'Variable alfven_dt_floor_conservative set to false.')
        call threeoutput(routinename, ' It is redundant in the presence of' &
                         //' alfven_dt_floor_vkill>=0')
      end if
    end if

    if (iohmic < 0 .or. iohmic > 3) then
      iohmic = min(3, max(0, iohmic))
      call warnoutput(routinename, &
                      'Value of iohmic currently not assigned'// &
                      'or not in use. Adjusted to closest available.')
    end if

    if (iohmic_cycle < 1 .or. iohmic_cycle > 2) then
      iohmic_cycle = min(2, max(1, iohmic_cycle))
      call warnoutput(routinename, &
                      'Value of iohmic_cycle currently not assigned'// &
                      'or not in use. Adjusted to closest available.')
    end if

    if (imhd == 0 .and. with_ad_1f) then
      with_ad_1f = .false.
      call threeoutput(routinename, 'Value of with_ad_1f set to false for a' &
                       //' non-magnetic problem.')
    end if

    if (imhd == 0 .and. ihall /= 0) then
      ihall = 0
      call threeoutput(routinename, &
                       'Value of ihall set to 0 for a non-magnetic problem.')
    end if

    if (imhd == 0 .and. iohmic /= 0) then
      iohmic = 0
      call threeoutput(routinename, &
                       'Value of iohmic set to 0 for a non-magnetic problem.')
    end if

    if ((iohmic == 1 .or. iohmic == 2) .and. iohmic_cycle /= 1) then
      iohmic_cycle = 1
      call warnoutput(routinename, 'Value of iohmic_cycle adjusted to 1 for' &
                      //' an unsplit algorithm.')
    end if

    if (iohmic > 0 .and. iohmic_cycle == 1 .and. max_ohmic_steps /= 1) then
      max_ohmic_steps = 1
      call warnoutput(routinename, 'Value of max_ohmic_steps set to 1 for a' &
                      //' non-subcycling algorithm.')
    end if

    if (iohmic == 0 .and. max_ohmic_steps /= 1) then
      max_ohmic_steps = 1
      call threeoutput(routinename, 'Value of max_ohmic_steps set to 1 for a' &
                       //' non-resistive algorithm.')
    end if

    if (iohmic > 0 .and. iohmic_cycle == 2 .and. max_ohmic_steps < 1) then
      max_ohmic_steps = 1
      call warnoutput(routinename, &
                      'Value of max_ohmic_steps set to 1 for a'// &
                      '(formally) subcycling algorithm.')
    end if

    if (iohmic /= 0 .and. ienergy == 1) then
      call warnoutput(routinename, 'Ohmic heating disabled.' &
                      //'  It seems to fail stability tests.')
    end if

    if (iolog /= NO_OUTPUT) write (iolog, parcon)
    if (iotty /= NO_OUTPUT) write (iotty, parcon)
    !
    !--------------------------  GRAVITY CONTROL  --------------------------
    !
    rewind (ioin)
    read (ioin, grvcon)

    call lowercase(gravity_algorithm)

    select case (gravity_algorithm)
    case ('none', 'fixed', 'onlycentralmass', 'monopole', 'sor')
      continue
    case default
      gravity_algorithm = 'none'
      call warnoutput(routinename, 'Value of gravity_algorithm not assigned' &
                      //' or not in use.  Adjusted to "none".')
    end select

    if ((geometry /= 'rtp' .and. geometry /= 'rmp') &
        .or. gnewton == zro .or. get_array_size('x1') == 0) then
      select case (gravity_algorithm)
      case ('onlycentralmass', 'monopole', 'sor')
        gravity_algorithm = 'none'
        call threeoutput(routinename, &
                         "Non-spherical geometry or gnewton==0 or x1-symmetry")
        call threeoutput(routinename, &
                         "gravity_algorithm should be 'none' or 'fixed'")
        call threeoutput(routinename, "Reset to 'none'")
      case ('none', 'fixed')
        continue
      case default
        call finalstop(routinename, &
                       'Value of gravity_algorithm not assigned or not in use.')
      end select
    end if

    if (get_array_size('x2') == 0 .and. gravity_algorithm == 'sor') then
      gravity_algorithm = 'none'
      call warnoutput(routinename, 'gravity_algorithm sor not implemented' &
                      //' for x2-symmetry.  Adjusted to "none".')
    end if

    if (gnewton < zro) then
      select case (gravity_algorithm)
      case ('onlycentralmass', 'monopole', 'sor')
        call warnoutput(routinename, 'Negative gnewton may be incompatible' &
                        //' with gravity algorithm')
      case ('none', 'fixed')
        continue
      case default
        call finalstop(routinename, &
                       'Value of gravity_algorithm not assigned or not in use.')
      end select
    end if

    select case (gravity_algorithm)
    case ('none')
      withgp = .false.
      withcentralmass = .false.
    case ('fixed')
      withgp = .true.
      withcentralmass = .false.
    case ('onlycentralmass', 'monopole')
      withgp = .false.
      withcentralmass = .true.
    case ('sor')
      withgp = .true.
      withcentralmass = .true.
    case default
      call finalstop(routinename, &
                     'Value of gravity_algorithm not assigned or not in use.')
    end select

    if (withcentralmass .and. .not. withboundarymass) then
      withboundarymass = .true.
      call threeoutput(routinename, &
                       'Variable withboundarymass turned on,')
      call threeoutput(routinename, &
                       'because central mass will be used.')
      if (iolog /= NO_OUTPUT) write (iolog, parcon)
      if (iotty /= NO_OUTPUT) write (iotty, parcon)
    end if

    select case (gravity_algorithm)
    case ('sor')
      if (get_array_size('x3') == 0 &
          .and. (lmax_lower >= 0 .or. lmax_upper >= 0)) then
        lmax_lower = -1
        lmax_upper = -1
        call threeoutput(routinename, "The current 2D sor algorithm does only" &
                         //" direct sum BC, and does not need non-negative" &
                         //" lmax values. lmax_upper and lmax_lower set to -1.")
      end if
    case default
      if (lmax_lower >= 0 .or. lmax_upper >= 0) then
        lmax_lower = -1
        lmax_upper = -1
        call threeoutput(routinename, "Only the sor algorithm might need" &
                         //" non-negative lmax values.")
        call threeoutput(routinename, &
                         "lmax_upper and lmax_lower set to -1.")
      end if
    end select

    if (lmax_lower >= 0 .or. lmax_upper >= 0) then
      withextendedgrid = .true.
    else
      withextendedgrid = .false.
    end if

    write (message, *) 'withgp', withgp
    call threeoutput(routinename, message)
    write (message, *) 'withcentralmass', withcentralmass
    call threeoutput(routinename, message)
    write (message, *) 'withextendedgrid', withextendedgrid
    call threeoutput(routinename, message)

    if (iolog /= NO_OUTPUT) write (iolog, grvcon)
    if (iotty /= NO_OUTPUT) write (iotty, grvcon)

    !--------------------------  INITIALIZE GRID  --------------------------
    !
    ! Setup arrays and array bounds.  Adjust gridcon variables if needed
    !
    !
    ! GRIDX1, GRIDX2, GRIDX3, and GRID_E read namelists called GGEN from
    ! unit "ioin", made distinct by the local namelist variable "gridname"
    !
    call initialize_grid
    !
    ! Continue with setting up the MPI variables which depend on knowledge
    ! of the main and the extended grid
    !
    call mpisetup_datatypes

    !
    !-----------------------  ALLOCATE FIELD ARRAYS  ----------------------
    !
    !  This is the first opportunity to do it, now that the array sizes
    !  have been found, and the problem and geometry toggles have been read,
    !  and the existence of gravity field arrays has been determined.
    !
    call allocatefields
    call writemessage(routinename//'_allocatefields', 1)

    !--------------------  RESTART AND DUMP CONTROL  -----------------------
    ! This is the first opportunity to do it, now that all fields have been
    ! allocated, and all problem and geometry toggles have been read,
    ! and all the MPI setup has been done.
    !
    !    Plans:
    !      There must be some control about what quantities are desired
    !      to be redefined; such as setting nwarn to zero or not,
    !      reading gp from file or allow the problem subroutine to
    !      create it anew, adjusting dtmin again or using the old one,
    !      etcetera.
    !
    !     Possible choices:
    !      Restart type 1: Seamless restart: all old values are accepted,
    !                      even sundry info like the number of warnings,
    !                      but *ignoring* warnings produced during setup.
    !      Restart type 2: Close to that, but reset nwarn and hotzonewarn
    !                      to 0, however accept warnings produced during setup,
    !      Restart type 3: Read the fields, but reset to default values
    !                      most of the sundry initial data.  Accept however
    !                      the initial time as valid.
    !      Restart type 4: Close to restart type 3, but also set the initial
    !                      time to the namelist value time_initial (default 0).

    rewind (ioin)
    read (ioin, rescon)

    time_initial = time_initial*time_factor

    ! Plans: construct the restartfile basename based on consideration of
    ! prefix, suffix, dump index (which might be equal to ntimestep),
    ! nodename, and fieldnames.

    if (iotty /= NO_OUTPUT) write (iotty, rescon)
    if (iolog /= NO_OUTPUT) write (iolog, rescon)

    ! It is good to set up the initial time as soon as possible,
    ! in case the problem generators or a moving grid setup
    ! needs the time variable for something.
    ! The IF statement avoids overwriting time values given in
    ! restart files.
    if (irestart == 0 .or. irestart == 2 .or. irestart == 4) then
      time = time_initial
    end if

    rewind (ioin)
    read (ioin, dumpcon)

    dtdump = dtdump*time_factor
    tdump_initial = tdump_initial*time_factor

    if (clockdump_policy < 0 .or. clockdump_policy > 2) then
      clockdump_policy = min(2, max(0, clockdump_policy))
      call warnoutput(routinename, &
                      'Value of clockdump_policy currently not assigned'// &
                      'or not in use. Adjusted to closest available.')
    end if

    if (iolog /= NO_OUTPUT) write (iolog, dumpcon)
    if (iotty /= NO_OUTPUT) write (iotty, dumpcon)

    if (irestart == 0 .or. irestart == 3 .or. irestart == 4) then
      ! Initial timestep and ndump can now be set to zero
      call settimestep(0)
      ndump = 0
    end if
    !
    !--------------------------  READ MORE NAMELISTS  --------------------
    !
    !  Subroutine NAMELISTS reads certain namelists
    !  from unit "ioin" and performs some consistency checks on the values of
    !  their input parameters, making adjustments as appropriate.  A few
    !  other global variables which can be computed directly
    !  from these namelist parameters might also be set.
    !
    call namelists
    call writemessage(routinename//'_namelists', 1)

    !
    !----------------------------  GRID MOTION  ----------------------------
    !
    ! If this subroutine wrapper is not empty, read the corresponding
    ! namelist, and initialize grid motion variables.
    !
    call movegrid_init
    !
    !
    !--------------------------  INITIALIZE FIELDS  ------------------------
    !
    !  If this is a new run, SETUP will initialize grids and fields,
    !  otherwise we should read data
    !

    if (irestart /= 0) then     ! Read fields
      if (irestart == 2 .or. irestart == 3 .or. irestart == 4 &
          .or. irestart == 5) keepwarn = nwarn
      call readdata(trim(restartprefix), trim(restartsuffix))
      if (irestart == 2 .or. irestart == 3 .or. irestart == 4 &
          .or. irestart == 5) nwarn = keepwarn
      if (irestart == 3 .or. irestart == 4) then
        ! In case these are overwritten by readdadta
        call settimestep(0)
        ndump = 0
      end if
    end if

    call setup
    call writemessage(routinename//'_setup', 1)
    call writedata(routinename//'_setup', 4)

    !
    ! For moving grid problems, compute the grid velocities
    ! and the new grid positions.
    !
    call movegrid
    !
    !  Close the main input file: all namelists have been read by now.
    !
    close (unit=ioin)

    !
    ! Force dump of the first dataframe,
    ! no matter the values of tdump and time.
    !
    if (dtdump > zro) then
      dump_switch = 1
    else
      dump_switch = 0
    end if
    !
    !      Initialize other output types if present.
    !
    call timeslice_init

    !
    !  MPI field consolidation (if needed)
    !
    call mpifield

    !  Write down the current number of warnings
    if (nwarn == 0) &
      write (message, '(a)') &
      'MSTART  : Setup complete with no warnings issued'
    if (nwarn == 1) &
      write (message, '(a)') &
      'MSTART  : Setup complete with one warning issued'
    if (nwarn > 1) &
      write (message, '(a,i2,a)') &
      'MSTART  : Setup complete with ', nwarn, ' warnings issued'
    call threeoutput(routinename, message)
  END SUBROUTINE mstart
  !
  !=======================================================================
  !
  SUBROUTINE allocatefields
    ! Performs dynamic allocation of the main allocatable field variables

    !
    !  Fields
    !
    allocate (d_(isa:iea, jsa:jea, ksa:kea))
    allocate (v1(isa:iea, jsa:jea, ksa:kea))
    if (withv2) allocate (v2(isa:iea, jsa:jea, ksa:kea))
    if (withv3) allocate (v3(isa:iea, jsa:jea, ksa:kea))
    if (ienergy == 1 .or. ienergy == 2) then
      allocate (e_(isa:iea, jsa:jea, ksa:kea))
    end if
    if (imhd > 0) then
      if (withbp) then
        allocate (b1(isa:iea, jsa:jea, ksa:kea))
        allocate (b2(isa:iea, jsa:jea, ksa:kea))
      end if
      if (withb3) allocate (b3(isa:iea, jsa:jea, ksa:kea))
      if (withchem) then
        allocate (ad_eta_array(isa:iea, jsa:jea, ksa:kea))
        allocate (ohmic_eta_array(isa:iea, jsa:jea, ksa:kea))
        allocate (hall_eta_array(isa:iea, jsa:jea, ksa:kea))
        allocate (xr_array(isa:iea, jsa:jea, ksa:kea))
        allocate (cr_array(isa:iea, jsa:jea, ksa:kea))
        if (tabulate > 0) allocate (allRec(numIons + gbins*2 + 8, nbins*tbins*ibins))
      end if
    end if

    allocate (tracerfields(isa:iea, jsa:jea, ksa:kea, number_of_tracer_fields))

    if (withextendedgrid) then
      allocate (d_e(isa_e:iea_e, jsa:jea, ksa:kea))
    else
      d_e => d_
    end if

    if (withgp) then
      allocate (gp(isa:iea, jsa:jea, ksa:kea))

      if (withextendedgrid) then
        ! Consider having gp as a pointer in this case.
        ! Be careful of ghost zones if that is done.
        allocate (gp_e(isa_e:iea_e, jsa:jea, ksa:kea))
      else
        gp_e => gp
      end if
    end if

    if (alfven_dt_floor_forcelimiter) then
      allocate (dfl(isa:iea, jsa:jea, ksa:kea))
    else
      dfl => d_
    end if

  END SUBROUTINE allocatefields
  !
  !=======================================================================
  !
  SUBROUTINE namelists
    !
    !    dac:zeus3d.namelists <---------------- reads namelists from input deck
    !
    !    written by: David Clarke
    !    modified 1: 09/12/94 by Robert Fiedler so that when the units are
    !                specified for angular variables in a plot or dump, the
    !                default values do not get multiplied by an additional
    !                factor, since they are already in radians.
    !    modified 2: 11/17/94 by Robert Fiedler to use a better test to
    !                see if we need to multiply by units.
    !
    !    More modifications by RFK.
    !
    !  PURPOSE: Reads the input parameters from certain namelists:
    !  at the beginning of a run.  Called by MSTART.
    !

    CHARACTER(LEN=*), PARAMETER :: routinename = 'z_namelists'
    !
    INTEGER :: ippa  ! Auxiliary hydrodynamic control variable
    !
    !--------------------------  PROBLEM CONTROL  --------------------------

    rewind (ioin)
    read (ioin, pcon)

    nlim = max(0, nlim)
    tlim = max(zro, tlim*time_factor)
    clocktotal = max(zro, clocktotal)

    if (iolog /= NO_OUTPUT) write (iolog, pcon)
    if (iotty /= NO_OUTPUT) write (iotty, pcon)
    !
    !---------------------------  HYDRO CONTROL  ---------------------------
    !
    rewind (ioin)
    read (ioin, hycon)
    !
    ! The following code is in practice seldom used,
    ! especially now that PPA is discontinued.
    !
    if ((iord < 1) .or. (iord > 3)) iord = 2
    if ((iordd_ < 1) .or. (iordd_ > 3)) iordd_ = iord
    if ((iorde_ < 1) .or. (iorde_ > 3)) iorde_ = iord
    if ((iords1 < 1) .or. (iords1 > 3)) iords1 = iord
    if ((iords2 < 1) .or. (iords2 > 3)) iords2 = iord
    if ((iords3 < 1) .or. (iords3 > 3)) iords3 = iord
    if ((iordb1 < 1) .or. (iordb1 > 3)) iordb1 = iord
    if ((iordb2 < 1) .or. (iordb2 > 3)) iordb2 = iord
    if ((iordb3 < 1) .or. (iordb3 > 3)) iordb3 = iord

    ! The following code is irrelevant, now that PPA is discontinued
    ! and its steepeners stay unused.
    ! However, it is not erased, in case PPA makes a comeback.
    !
    if ((istp < 0) .or. (istp > 2)) istp = 0
    if ((istpd_ < 0) .or. (istpd_ > 2)) istpd_ = istp
    if (istp == 2) istp = 0
    if ((istpe_ < 0) .or. (istpe_ > 1)) istpe_ = istp
    if ((istps1 < 0) .or. (istps1 > 1)) istps1 = istp
    if ((istps2 < 0) .or. (istps2 > 1)) istps2 = istp
    if ((istps3 < 0) .or. (istps3 > 1)) istps3 = istp
    if ((istpb1 < 0) .or. (istpb1 > 1)) istpb1 = istp
    if ((istpb2 < 0) .or. (istpb2 > 1)) istpb2 = istp
    if ((istpb3 < 0) .or. (istpb3 > 1)) istpb3 = istp
    !
    if (iolog /= NO_OUTPUT) write (iolog, hycon)
    if (iotty /= NO_OUTPUT) write (iotty, hycon)
    !
    ! Detect usage of PPA
    !
    ippa = 0
    if ((iordd_ == 3) .or. (iorde_ == 3) .or. &
        (iords1 == 3) .or. (iords2 == 3) .or. &
        (iords3 == 3) .or. (iordb1 == 3) .or. &
        (iordb2 == 3) .or. (iordb3 == 3)) ippa = 1
    if (ippa /= 0) then
      call threeoutput(routinename, 'PPA currently discontinued.')
      call threeoutput(routinename, 'Set interpolation order to 2 or less.')
      call finalstop(routinename, 'Execution stops.')
    end if

    if (densityfloor_vkill > zro) then
      call threeoutput(routinename, 'densityfloor_vkill>0 not implemented')
      call finalstop(routinename, 'Execution stops.')
    end if
    !
    !  Initialize grid functions peculiar to the PPA interpolation
    !  algorithm (commented out while PPA is discontinued).
    !
    !  if (ippa == 1) call ppafnc
    !
    !-----------------  OUTFLOW BC CONTROL  ------------------
    !
    rewind (ioin)
    read (ioin, outflowcon)
    if (iolog /= NO_OUTPUT) write (iolog, outflowcon)
    if (iotty /= NO_OUTPUT) write (iotty, outflowcon)
    !
    !-------------------------  EQUATION OF STATE  -------------------------
    !
    rewind (ioin)
    read (ioin, eqos)

    ! Adjust values

    if (ienergy == 0 .or. (ienergy == -1 .and. eoskind == 0)) then
      call threeoutput(routinename, 'Set gamma for an isothermal problem.')
      gamma = one
    end if
    if (ienergy == 1) then
      call threeoutput(routinename, &
                       'Set eoskind for a problem with energy equation.')
      eoskind = 3
    end if
    if (ienergy == 0) then
      call threeoutput(routinename, 'Set eoskind for an isothermal problem.')
      eoskind = 0
    end if

    if (itote /= 0) then
      if (ienergy /= 1) then
        itote = 0
        call threeoutput(routinename, &
                         'Variable itote reset to 0 for ienergy/=1.')
      end if
      if (.not. (withv2 .and. withv3)) then
        itote = 0
        call threeoutput(routinename, 'Setting v2 or v3 to a fixed zero is ' &
                         //'not fully implemented with a total energy equation')
        call threeoutput(routinename, &
                         'Using internal instead of total energy equation.')
        call threeoutput(routinename, 'Variable itote reset to 0.')
      end if
      if (imhd /= 0) then
        itote = 0
        call threeoutput(routinename, 'MHD implemented only with an internal' &
                         //' energy equation.')
        call threeoutput(routinename, &
                         'Using internal instead of total energy equation.')
        call threeoutput(routinename, 'Variable itote reset to 0.')
      end if
    end if

    if (iolog /= NO_OUTPUT) write (iolog, eqos)
    if (iotty /= NO_OUTPUT) write (iotty, eqos)
  END SUBROUTINE namelists
  !
  !=======================================================================
  !
  SUBROUTINE setup
    !
    !  PURPOSE: Initialize all grids and fields.
    !
    !  ROUTINES CALLED:
    !    PROBLEM_START   a problem-dependent subroutine which
    !                    initializes field variables for the specific problem
    !    PROBLEM_RESTART similar to PROBLEM_START, intended for restart runs
    !    NEW_DT          wrapper for the subroutine to compute dt
    !
    !-----------------------------------------------------------------------
    !
    USE zeus_deadcode
    USE problem_initial
    USE zeus_physics
    USE zeus_gravity

    CHARACTER(LEN=*), PARAMETER :: routinename = 'z_setup'
    !
    !-------------------------  PROBLEM GENERATOR  -------------------------
    !
    !  PROBLEM_START should initialize the field arrays in their entirety,
    !  including the boundary zones.
    !  The namelist PGEN has been set aside in the input file
    !  for PROBLEM_START and PROBLEM_RESTART to use.
    !
    call readdata(trim(tablename), tabulate)
    if (irestart == 0) then
      call problem_start
      call writemessage(routinename//'_problem', 1)
      call writedata(routinename//'_problem', 4)
    end if
    !
    !-------------------------  PROBLEM RESTART GENERATOR  ----------------
    !
    !   Conceptually similar to PROBLEM, to be used in restart runs,
    !   for things such as reading parameters and data from the input file
    !   and specialized output files, and also for modifying the fields.
    !
    !   It is often empty.
    !
    if (irestart /= 0) then
      call problem_restart
      call writemessage(routinename//'_problemrestart', 1)
      call writedata(routinename//'_problemrestart', 4)
    end if

    !
    !  Check sanity of gravity variables if needed
    !
    if (withcentralmass .and. centralmass < 0) then
      call warnoutput(routinename, &
                      'The negative initial central mass may be nonsense.')
      call writedata('z_negativecm_')
    end if
    !
    !--------------------------  INITIAL TIMESTEP  -------------------------
    !
    if (irestart == 0 .or. irestart == 2 .or. irestart == 3 .or. irestart == 4 &
        .or. irestart == 5) then
      hotzonewarn = 0
      ! This initial timestep is not limited by artificial viscosity
      ! and it uses a reduced-size first timestep.
      ! Doing this also recalculates dtmin, used to detect hot zones.

      call new_dt(zro, .true.)
      call writemessage(routinename//'_newtim', 1)
    end if

    !
    !---------------  INITIALIZE TDUMP AND SWEEP CYCLE  --------------------
    !
    if (irestart == 0 .or. irestart == 3 .or. irestart == 4) then
      ix1x2x3 = 1
      if (tdump_initial_used) then
        tdump = tdump_initial
      else
        tdump = time
      end if
    end if
    !
  END SUBROUTINE setup

END MODULE zeus_initial
