#include "macros"
MODULE zeus_interpolators
  USE zeus_variables
  USE zeus_memory
  IMPLICIT NONE
  PRIVATE
  PUBLIC :: x1fc3d, x1zc1d, x1zc3d, x1int1d
  PUBLIC :: x2fc3d, x2zc1d, x2zc3d, x2int1d
  PUBLIC :: x3fc3d, x3zc1d, x3zc3d, x3int1d

CONTAINS
  !
  !** GROUP: INTERPOLATORS ***********************************************
  !**                                                                   **
  !**            G R O U P :   I N T E R P O L A T O R S                **
  !**                                                                   **
  !***********************************************************************
  !
  !=======================================================================
  !
  SUBROUTINE x1fc3d(qty, vel1, jbeg, kbeg, jend, kend, iorder, qi)
    !
    !    dac:zeus3d.x1fc3d <----- x1-interpolation of face-centred 3-D array
    !    from dac:zeus2d.x1intfc, dac.zeus04.zint                  may, 1990
    !
    !    written by: David Clarke
    !    modified 1: September, 1990 by David Clarke; moved all functions
    !                of the grid coordinates for the PPA algorithm to a new
    !                subroutine PPAFNC, which is called each time the grid
    !                is updated.  Removed all "if" statements from inner
    !                do-loops.  Execution times for the donor cell, van
    !                Leer, and PPA algorithms have been reduced by 30%, 25%,
    !                and 50% respectively.  The contact steepening algorithm
    !                has been removed since it is never needed for face-
    !                centred quantities.
    !    modified 2: April 1995 by Robert Fiedler; rewritten for running in
    !                parallel on shared memory multiprocessors.
    !
    !  PURPOSE:  The face-centred 3-D array "qty" is interpolated to the
    !  zone centres along the 1-direction, and the results are stored in
    !  "qi".  The interpolated values can be first order (donor cell),
    !  second order (van Leer), or third order (piecewise parabolic
    !  advection) accurate, depending on the value of "iorder".  All
    !  interpolation schemes are monotonic and upwinded.  Global extrema are
    !  not monotonized in the PPA algorithm, ensuring monotonic derivatives.
    !
    !  The PPA algorithm is based on Colella and Woodward, J. Comp. Phys.
    !  54:174 (1984).  References to equation numbers refer to that paper.
    !
    !  The following grid functions used in the PPA algorithm are computed
    !  in PPAFNC.
    !
    !    f1dx1b, f2dx1b, f3dx1b, f4dx1b, f5dx1b, f6dx1b, f7dx1b, f8dx1b,
    !
    !  INPUT VARIABLES:
    !    qty       3-D array to be interpolated at zone-centres (NOTE: the
    !              active zones for "qty" in the 1-direction should be
    !              i=isp1 to ie)
    !    vel1      1-velocities at zone-centres, possibly averaged in i.  It
    !              is assumed that the grid velocities (if any) have been
    !              subtracted from "vel1".
    !    jbeg      beginning index for j-sweeps
    !    kbeg      beginning index for k-sweeps
    !    jend      ending    index for j-sweeps
    !    kend      ending    index for k-sweeps
    !    iorder    desired order of interpolation
    !
    !  OUTPUT VARIABLES:
    !    qi        3-D array containing "qty" interpolated at zone centres
    !
    !  LOCAL VARIABLES:
    !    q         j, k sweep of "qty" currently being interpolated (allows
    !              "qty" and "qi" to use the same array)
    !    dq        zone-centred difference of "q"
    !    d2q       face-centred product    of "dq" (van Leer)
    !              face-centred difference of "dq" (ppa)
    !    dqi       face-centred difference of "q" between interfaces
    !    dql       left  zone-centred difference in "q" (ppa)
    !    dqr       right zone-centred difference in "q" (ppa)
    !    qli       "q" interpolated to left  zone centre
    !    qri       "q" interpolated to right zone centre
    !    qlm       monotonized value of "qli" (ppa)
    !    qrm       monotonized value of "qri" (ppa)
    !    glexa     =0.0 => face-centred quantity should be monotonized
    !                      because "q" is a LOCAL extremum (ppa)
    !              =1.0 => face-centred quantity should NOT be monotonized
    !                      because "q" is a GLOBAL extremum (ppa)
    !    glexb     similar to "glexa", but for zone-centred quantities (ppa)
    !
    !-----------------------------------------------------------------------
    !
    REALNUM, DIMENSION(:, :, :), INTENT(IN) :: qty, vel1
    INTEGER, INTENT(IN) ::  jbeg, kbeg, jend, kend, iorder
    REALNUM, INTENT(OUT) ::      qi(:, :, :)
    !
    INTEGER i, j, k
    REALNUM q1, q2, q3, xi
    REALNUM dqim, dqip
    REALNUM, POINTER, SAVE :: dqii(:, :, :) => null()
    !
    !-----------------------------------------------------------------------
    !
    !      If problem is i-symmetric, set "qi" to "qty" and return.
    !
    if (inac == 0) then
      do k = kbeg, kend
        do j = jbeg, jend
          qi(is, j, k) = qty(is, j, k)
        end do
      end do
      return
    end if
    !
    !------------- 1st order (donor cell) interpolated values --------------
    !
    if (iorder == 1) then
      do k = kbeg, kend
        do j = jbeg, jend
          !
          !  1.  Choosing upwind value for the interface value "qi".
          !
          !  RFK       do i=is,ie
          do i = ism1, ie
            q1 = sign(haf, vel1(i, j, k))
            qi(i, j, k) = (haf + q1)*qty(i, j, k) &
                          + (haf - q1)*qty(i + 1, j, k)
          end do
          !
        end do
      end do
      return
    end if
    !
    !-------------- 2nd order (van Leer) interpolated values ---------------
    !
    if (iorder == 2) then
      call get_memory(dqii)
      !
      !  1.  Evaluate monotonized, van Leer difference in "q" across the zone.
      !
      do k = kbeg, kend
        do j = jbeg, jend
          ! RFK        do i=is,iep1
          do i = ism1, iep1
            dqim = (qty(i, j, k) - qty(i - 1, j, k))*dx1ai(i - 1)
            dqip = (qty(i + 1, j, k) - qty(i, j, k))*dx1ai(i)
            dqii(i, j, k) = max(dqim*dqip, zro) &
                            *sign(one, dqim + dqip) &
                            /max(abs(dqim + dqip), verysmall)
          end do
        end do
      end do
      !
      !  2.  Choose time averaged, upwinded interface value.
      !
      do k = kbeg, kend
        do j = jbeg, jend
          ! RFK        do i=is,ie
          do i = ism1, ie
            xi = vel1(i, j, k)*dt
            q1 = sign(haf, xi)
            qi(i, j, k) = (haf + q1)*(qty(i, j, k) &
                                      + (dx1b(i) - xi)*dqii(i, j, k)) &
                          + (haf - q1)*(qty(i + 1, j, k) &
                                        - (dx1b(i + 1) + xi)*dqii(i + 1, j, k))
          end do
        end do
      end do
      call release_memory(dqii)
      return
    end if
    !
  END SUBROUTINE x1fc3d
  !
  !=======================================================================
  !
  SUBROUTINE x1zc1d(q, vp, vm, iorder, isteep, j, k, qp, qm)
    !
    !    dac:zeus3d.x1zc1d <----- x1-interpolation of zone-centred 1-D array
    !    from dac:zeus3d.x1zc3d                              september, 1990
    !
    !    written by: David Clarke
    !    modified 1: October 1992, by David Clarke; made compatable with the
    !                new MOCEMFS routine.
    !    modified 2: April 1995 by Robert Fiedler; rewritten for running in
    !                parallel on shared memory multiprocessors.
    !
    !  PURPOSE:  This subroutine performs interpolations on 1-D vectors
    !  for the routine MOCEMFS in the 1-direction.  It uses the same
    !  interpolating algorithms used by X1ZC3D and X1FC3D.  The steepener
    !  algorithm is included only for testing purposes.  Thus, the detecting
    !  algorithm for contact discontinuities has been omitted.  See the
    !  comments for X1ZC3D for additional details.
    !
    !  INPUT VARIABLES:
    !    q         1-D array to be interpolated (NOTE: the active zones for
    !              "q" should be i=is to ie)
    !    vp        velocity along the "plus"  characteristic
    !    vm        velocity along the "minus" characteristic
    !    iorder    desired order of interpolation
    !    isteep    steepener switch
    !              = 0 => always off
    !              > 0 => always on
    !    j,k       2- and 3-indices in calling routine
    !
    !  OUTPUT VARIABLES:
    !    qp        "q" interpolated to base of "plus"  characteristic
    !    qm        "q" interpolated to base of "minus" characteristic
    !
    !  LOCAL VARIABLES:
    !    dq        face-centred difference of "q"  (van Leer)
    !    d2q       zone-centred product    of "dq" (van Leer)
    !              zone-centred difference of "dq" (ppa)
    !    dqi       zone-centred difference of "q" between interfaces
    !    dql       left  face-centred difference of "q" (ppa)
    !    dqr       right face-centred difference in "q" (ppa)
    !    glexb     =0.0 => zone-centred quantity should be monotonized
    !                      because "q" is a LOCAL extremum
    !              =1.0 => zone-centred quantity should NOT be monotonized
    !                      because "q" is a GLOBAL extremum (ppa)
    !    glexa     similar to "glexb", but for face-centred quantities (ppa)
    !    qli       "q" interpolated to left  interface (ppa)
    !    qri       "q" interpolated to right interface (ppa)
    !    qlm       monotonized value of "qli" (ppa)
    !    qrm       monotonized value of "qri" (ppa)
    !
    !-----------------------------------------------------------------------
    !
    REALNUM, DIMENSION(isa:iea), INTENT(IN) :: q, vp, vm
    INTEGER, INTENT(IN) ::  j, k, iorder, isteep
    REALNUM, DIMENSION(isa:iea), INTENT(OUT) :: qp, qm
    !
    INTEGER i
    REALNUM q1, q2, q3, xi, dqim, dqip
    REALNUM dqi(isa:iea)
    !
    !-----------------------------------------------------------------------
    !
    !      If problem is i-symmetric, set "qm" and "qp" to "q" and return.
    !
    if (inac == 0) then
      qp(is) = q(is)
      qm(is) = q(is)
      return
    end if
    !
    !------------- 1st order (donor cell) interpolated values --------------
    !
    if (iorder == 1) then
      !
      !  1.  Choose upwind value for characteristic bases "qp" and "qm".
      !
      do i = is, iep1
        q1 = sign(haf, vp(i))
        q2 = sign(haf, vm(i))
        qp(i) = (haf + q1)*q(i - 1) + (haf - q1)*q(i)
        qm(i) = (haf + q2)*q(i - 1) + (haf - q2)*q(i)
      end do
      go to 130
    end if
    !
    !-------------- 2nd order (van Leer) interpolated values ---------------
    !
    if (iorder == 2) then
      !
      !  1.  Evaluate monotonized, van Leer difference in "q" across the zone.
      !
      do i = ism1, iep1
        dqim = (q(i) - q(i - 1))*dx1bi(i)
        dqip = (q(i + 1) - q(i))*dx1bi(i + 1)
        dqi(i) = max(dqim*dqip, zro) &
                 *sign(one, dqim + dqip) &
                 /max(abs(dqim + dqip), verysmall)
      end do
      !
      !  2.  Perform an upwinded interpolation of "q" to the time-centred
      !      bases of the characteristics.
      !
      do i = is, iep1
        q1 = q(i - 1) + dx1a(i - 1)*dqi(i - 1)
        q2 = q(i) - dx1a(i)*dqi(i)
        !
        xi = vp(i)*dt
        q3 = sign(haf, xi)
        qp(i) = (haf + q3)*(q1 - xi*dqi(i - 1)) &
                + (haf - q3)*(q2 - xi*dqi(i))
        !
        xi = vm(i)*dt
        q3 = sign(haf, xi)
        qm(i) = (haf + q3)*(q1 - xi*dqi(i - 1)) &
                + (haf - q3)*(q2 - xi*dqi(i))
      end do
      go to 130
    end if
    !
    !----------------------- interpolations complete -----------------------
    !
130 continue
    !
    !      Reset boundary values for outflow boundary conditions.
    !
    !  RFK : adapted for MPI usage
    !
    if (inner_i_outflow .and. l_firstnode1) qm(is) = qp(is)
    if (outer_i_outflow .and. l_lastnode1) qp(iep1) = qm(iep1)
    !
  END SUBROUTINE x1zc1d
  !
  !=======================================================================
  !
  SUBROUTINE x1zc3d(qty, vel1, jbeg, kbeg, jend, kend, iorder, isteep, qi)
    !
    !    dac:zeus3d.x1zc3d <----- x1-interpolation of zone-centred 3-D array
    !    from dac:zeus2d.x1intzc, dac.zeus04.zint                  may, 1990
    !
    !    written by: David Clarke
    !    modified 1: September, 1990 by David Clarke; moved all functions
    !                of the grid coordinates for the PPA algorithm to a new
    !                subroutine PPAFNC, which is called each time the grid
    !                is updated.  Removed all "if" statements from inner
    !                do-loops.  Execution times for the donor cell, van
    !                Leer, and PPA algorithms have been reduced by 30%, 25%,
    !                and 50% respectively.  The contact steepening algorithm
    !                has been implemented.
    !    modified 2: April 1995 by Robert Fiedler; rewritten for running in
    !                parallel on shared memory multiprocessors.
    !
    !  PURPOSE:  The zone-centred 3-D array "qty" is interpolated to the
    !  1-interfaces, and the results are stored in "qi".  The interpolated
    !  values can be first order (donor cell), second order (van Leer), or
    !  third order (piecewise parabolic advection) accurate, depending on
    !  the value of "iorder".  All interpolation schemes are monotonic and
    !  upwinded, and for PPA, contact discontinuities are steepened when
    !  "isteep" = 2.  Global extrema are not monotonized in the PPA
    !  algorithm, ensuring monotonic derivatives.
    !
    !  The PPA algorithm is based on Colella and Woodward, J. Comp. Phys.
    !  54:174 (1984).  References to equation numbers refer to that paper.
    !
    !  The following grid functions used in the PPA algorithm are computed
    !  in PPAFNC.
    !
    !    f1dx1a, f2dx1a, f3dx1a, f4dx1a, f5dx1a, f6dx1a, f7dx1a, f8dx1a,
    !    f9dx1b
    !
    !  INPUT VARIABLES:
    !    qty       3-D array to be interpolated at the 1-interfaces (NOTE:
    !              the active zones for "qty" in the 1-direction should be
    !              i=is to ie)
    !    vel1      1-velocities at 1-interfaces, possibly averaged in j or
    !              k, but not in i.  It is assumed that the grid velocities
    !              (if any) have NOT been subtracted from "vel1".
    !    jbeg      beginning index for j-sweeps
    !    kbeg      beginning index for k-sweeps
    !    jend      ending    index for j-sweeps
    !    kend      ending    index for k-sweeps
    !    iorder    desired order of interpolation
    !    isteep    steepener switch
    !              = 0 => always off
    !              = 1 => always on
    !              = 2 => on only for contact discontinuity
    !
    !  OUTPUT VARIABLES:
    !    qi        3-D array containing "qty" interpolated at 1-interfaces
    !
    !  LOCAL VARIABLES:
    !    q         j, k sweep of "qty" currently being interpolated (allows
    !              "qty" and "qi" to use the same array)
    !    dq        face-centred difference of "q"
    !    d2q       zone-centred product    of "dq" (van Leer)
    !              zone-centred difference of "dq" (ppa)
    !    dqi       zone-centred difference of "q" between interfaces
    !    dql       left  face-centred difference in "q" (ppa)
    !    dqr       right face-centred difference in "q" (ppa)
    !    qli       "q" interpolated to left  interface
    !    qri       "q" interpolated to right interface
    !    qlm       monotonized value of "qli" (ppa)
    !    qrm       monotonized value of "qri" (ppa)
    !    glexb     =0.0 => zone-centred quantity should be monotonized
    !                      because "q" is a LOCAL extremum (ppa)
    !              =1.0 => zone-centred quantity should NOT be monotonized
    !                      because "q" is a GLOBAL extremum (ppa)
    !    glexa     similar to "glexb", but for face-centred quantities (ppa)
    !    steep     indicates whether interpolation is to be steepened (ppa
    !              steepener)
    !    p         3-D pressure array - used by ppa steepener, equivalenced
    !              to "qi" in the subroutine parameter list.
    !
    !-----------------------------------------------------------------------
    !
    REALNUM, DIMENSION(:, :, :), INTENT(IN) :: qty, vel1
    REALNUM, INTENT(OUT) :: qi(:, :, :)
    INTEGER, INTENT(IN) :: jbeg, kbeg, jend, kend, iorder, isteep
    !
    INTEGER i, j, k, jp1, kp1
    REALNUM q1, q2, q3, xi, dqim, dqip
    REALNUM, POINTER, SAVE :: dqii(:, :, :) => null()
    !
    !-----------------------------------------------------------------------
    !
    !      If problem is i-symmetric, set "qi" to "qty" and return.
    !
    if (inac == 0) then
      do k = kbeg, kend
        do j = jbeg, jend
          qi(is, j, k) = qty(is, j, k)
        end do
      end do
      return
    end if
    !
    !------------- 1st order (donor cell) interpolated values --------------
    !
    if (iorder == 1) then
      do k = kbeg, kend
        do j = jbeg, jend
          !
          !  1.  Choosing upwind value for the interface value "qi".
          !
          ! RFK        do i=is,iep1
          do i = ism1, iep1
            q1 = sign(haf, vel1(i, j, k) - vg1(i))
            qi(i, j, k) = (haf + q1)*qty(i - 1, j, k) &
                          + (haf - q1)*qty(i, j, k)
          end do
          !
        end do
      end do
      return
    end if
    !
    !-------------- 2nd order (van Leer) interpolated values ---------------
    !
    if (iorder == 2) then
      call get_memory(dqii)
      !
      !  1.  Evaluate monotonized, van Leer difference in "q" across the zone.
      !
      do k = kbeg, kend
        do j = jbeg, jend
          ! RFK        do i=ism1,iep1
          do i = ism2, iep1
            dqim = (qty(i, j, k) - qty(i - 1, j, k))*dx1bi(i)
            dqip = (qty(i + 1, j, k) - qty(i, j, k))*dx1bi(i + 1)
            dqii(i, j, k) = max(dqim*dqip, zro) &
                            *sign(one, dqim + dqip) &
                            /max(abs(dqim + dqip), verysmall)
          end do
        end do
      end do
      !
      !  2.  Choose time averaged, upwinded interface value.
      !
      do k = kbeg, kend
        do j = jbeg, jend
          ! RFK        do i=is,iep1
          do i = ism1, iep1
            xi = (vel1(i, j, k) - vg1(i))*dt
            q1 = sign(haf, xi)
            qi(i, j, k) = (haf + q1)*(qty(i - 1, j, k) &
                                      + (dx1a(i - 1) - xi)*dqii(i - 1, j, k)) &
                          + (haf - q1)*(qty(i, j, k) &
                                        - (dx1a(i) + xi)*dqii(i, j, k))
          end do
        end do
      end do
      call release_memory(dqii)
      return
    end if
    !
  END SUBROUTINE x1zc3d
  !
  !=======================================================================
  !
  SUBROUTINE x2fc3d(qty, vel2, kbeg, ibeg, kend, iend, iorder, qi)
    !
    !    dac:zeus3d.x2fc3d <----- x2-interpolation of face-centred 3-D array
    !    from dac:zeus2d.x2intfc, dac.zeus04.rint                  may, 1990
    !
    !    written by: David Clarke
    !    modified 1: September, 1990 by David Clarke; moved all functions
    !                of the grid coordinates for the PPA algorithm to a new
    !                subroutine PPAFNC, which is called each time the grid
    !                is updated.  Removed all "if" statements from inner
    !                do-loops.  Execution times for the donor cell, van
    !                Leer, and PPA algorithms have been reduced by 30%, 25%,
    !                and 50% respectively.  The contact steepening algorithm
    !                has been removed since it is never needed for face-
    !                centred quantities.
    !    modified 2: April 1995 by Robert Fiedler; rewritten for running in
    !                parallel on shared memory multiprocessors.
    !
    !  PURPOSE:  The face-centred 3-D array "qty" is interpolated to the
    !  zone centres along the 2-direction, and the results are stored in
    !  "qi".  The interpolated values can be first order (donor cell),
    !  second order (van Leer), or third order (piecewise parabolic
    !  advection) accurate, depending on the value of "iorder".  All
    !  interpolation schemes are monotonic and upwinded.  Global extrema are
    !  not monotonized in the PPA algorithm, ensuring monotonic derivatives.
    !
    !  The PPA algorithm is based on Colella and Woodward, J. Comp. Phys.
    !  54:174 (1984).  References to equation numbers refer to that paper.
    !
    !  The following grid functions used in the PPA algorithm are computed
    !  in PPAFNC.
    !
    !    f1dx2b, f2dx2b, f3dx2b, f4dx2b, f5dx2b, f6dx2b, f7dx2b, f8dx2b,
    !
    !  INPUT VARIABLES:
    !    qty       3-D array to be interpolated at zone-centres (NOTE: the
    !              active zones for "qty" in the 2-direction should be
    !              j=jsp1 to je)
    !    vel2      2-velocities at zone-centres, possibly averaged in j.  It
    !              is assumed that the grid velocities (if any) have been
    !              subtracted from "vel2".
    !    kbeg      beginning index for k-sweeps
    !    ibeg      beginning index for i-sweeps
    !    kend      ending    index for k-sweeps
    !    iend      ending    index for i-sweeps
    !    iorder    desired order of interpolation
    !
    !  OUTPUT VARIABLES:
    !    qi        3-D array containing "qty" interpolated at zone centres
    !
    !  LOCAL VARIABLES:
    !    q         k, i sweep of "qty" currently being interpolated (allows
    !              "qty" and "qi" to use the same array)
    !    dq        zone-centred difference of "q"
    !    d2q       face-centred product    of "dq" (van Leer)
    !              face-centred difference of "dq" (ppa)
    !    dqi       face-centred difference of "q" between interfaces
    !    dql       left  zone-centred difference in "q" (ppa)
    !    dqr       right zone-centred difference in "q" (ppa)
    !    qli       "q" interpolated to left  zone centre
    !    qri       "q" interpolated to right zone centre
    !    qlm       monotonized value of "qli" (ppa)
    !    qrm       monotonized value of "qri" (ppa)
    !    glexa     =0.0 => face-centred quantity should be monotonized
    !                      because "q" is a LOCAL extremum (ppa)
    !              =1.0 => face-centred quantity should NOT be monotonized
    !                      because "q" is a GLOBAL extremum (ppa)
    !    glexb     similar to "glexa", but for zone-centred quantities (ppa)
    !
    !-----------------------------------------------------------------------
    !
    REALNUM, DIMENSION(:, :, :), INTENT(IN) :: qty, vel2
    REALNUM, INTENT(OUT) :: qi(:, :, :)
    INTEGER, INTENT(IN) :: ibeg, kbeg, iend, kend, iorder
    !
    INTEGER i, j, k
    REALNUM q1, q2, q3, xi, dqjm, dqjp
    REALNUM, POINTER, SAVE :: dqji(:, :, :) => null()
    !
    !-----------------------------------------------------------------------
    !
    !      If problem is j-symmetric, set "qi" to "qty" and return.
    !
    if (jnac == 0) then
      do k = kbeg, kend
        do i = ibeg, iend
          qi(i, js, k) = qty(i, js, k)
        end do
      end do
      return
    end if
    !
    !------------- 1st order (donor cell) interpolated values --------------
    !
    if (iorder == 1) then
      do k = kbeg, kend
        ! RFK      do j=js,je
        do j = jsm1, je
          do i = ibeg, iend
            !
            !  1.  Choosing upwind value for the interface value "qi".
            !
            q1 = sign(haf, vel2(i, j, k))
            qi(i, j, k) = (haf + q1)*qty(i, j, k) &
                          + (haf - q1)*qty(i, j + 1, k)
          end do
          !
        end do
      end do
      return
    end if
    !
    !-------------- 2nd order (van Leer) interpolated values ---------------
    !
    if (iorder == 2) then
      call get_memory(dqji)
      !
      !  1.  Evaluate monotonized, van Leer difference in "q" across the zone.
      !
      do k = kbeg, kend
        ! RFK      do j=js,jep1
        do j = jsm1, jep1
          do i = ibeg, iend
            dqjm = (qty(i, j, k) - qty(i, j - 1, k))*dx2ai(j - 1)
            dqjp = (qty(i, j + 1, k) - qty(i, j, k))*dx2ai(j)
            dqji(i, j, k) = max(dqjm*dqjp, zro) &
                            *sign(one, dqjm + dqjp) &
                            /max(abs(dqjm + dqjp), verysmall)
          end do
        end do
      end do
      !
      !  2.  Choose time averaged, upwinded interface value.
      !
      do k = kbeg, kend
        ! RFK      do j=js,je
        do j = jsm1, je
          do i = ibeg, iend
            xi = vel2(i, j, k)*dt*g2bi(i)
            q1 = sign(haf, xi)
            qi(i, j, k) = (haf + q1)*(qty(i, j, k) &
                                      + (dx2b(j) - xi)*dqji(i, j, k)) &
                          + (haf - q1)*(qty(i, j + 1, k) &
                                        - (dx2b(j + 1) + xi)*dqji(i, j + 1, k))
          end do
        end do
      end do
      call release_memory(dqji)
      return
    end if
    !
  END SUBROUTINE x2fc3d
  !
  !=======================================================================
  !
  SUBROUTINE x2zc1d(q, vp, vm, iorder, isteep, k, i, g2, g2i, qp, qm)
    !
    !    dac:zeus3d.x2zc1d <----- x2-interpolation of zone-centred 1-D array
    !    from dac:zeus3d.x2zc3d                              september, 1990
    !
    !    written by: David Clarke
    !    modified 1: October 1992, by David Clarke; made compatable with the
    !                new MOCEMFS routine.
    !    modified 2: April 1995 by Robert Fiedler; rewritten for running in
    !                parallel on shared memory multiprocessors.
    !
    !  PURPOSE:  This subroutine performs interpolations on 1-D vectors
    !  for the routine MOCEMFS in the 2-direction.  It uses the same
    !  interpolating algorithms used by X2ZC3D and X2FC3D.  The steepener
    !  algorithm is included only for testing purposes.  Thus, the detecting
    !  algorithm for contact discontinuities has been omitted.  See the
    !  comments for X2ZC3D for additional details.
    !
    !  INPUT VARIABLES:
    !    q         1-D array to be interpolated (NOTE: the active zones for
    !              "q" should be j=js to je)
    !    vp        velocity along the "plus"  characteristic
    !    vm        velocity along the "minus" characteristic
    !    iorder    desired order of interpolation
    !    isteep    steepener switch
    !              = 0 => always off
    !              > 0 => always on
    !    k,i       3- and 1-indices in calling routine
    !    g2        metric factor (either "g2a" or "g2b")
    !    g2i       inverse of "g2".
    !
    !  OUTPUT VARIABLES:
    !    qp        "q" interpolated to base of "plus"  characteristic
    !    qm        "q" interpolated to base of "minus" characteristic
    !
    !  LOCAL VARIABLES:
    !    dq        face-centred difference of "q"  (van Leer)
    !    d2q       zone-centred product    of "dq" (van Leer)
    !              zone-centred difference of "dq" (ppa)
    !    dqi       zone-centred difference of "q" between interfaces
    !    dql       left  face-centred difference of "q" (ppa)
    !    dqr       right face-centred difference in "q" (ppa)
    !    glexb     =0.0 => zone-centred quantity should be monotonized
    !                      because "q" is a LOCAL extremum
    !              =1.0 => zone-centred quantity should NOT be monotonized
    !                      because "q" is a GLOBAL extremum (ppa)
    !    glexa     similar to "glexb", but for face-centred quantities (ppa)
    !    qli       "q" interpolated to left  interface (ppa)
    !    qri       "q" interpolated to right interface (ppa)
    !    qlm       monotonized value of "qli" (ppa)
    !    qrm       monotonized value of "qri" (ppa)
    !
    !-----------------------------------------------------------------------
    !
    REALNUM, DIMENSION(jsa:jea), INTENT(IN) ::   q, vp, vm
    REALNUM, DIMENSION(isa:iea), INTENT(IN) ::   g2, g2i
    INTEGER, INTENT(IN) :: iorder, isteep, k, i
    REALNUM, DIMENSION(jsa:jea), INTENT(OUT) ::  qp, qm
    !
    INTEGER j
    REALNUM q1, q2, q3, xi, dqjm, dqjp, fact
    REALNUM dqi(jsa:jea)
    !
    !-----------------------------------------------------------------------
    !
    !      If problem is j-symmetric, set "qm" and "qp" to "q" and return.
    !
    if (jnac == 0) then
      qp(js) = q(js)
      qm(js) = q(js)
      return
    end if
    !
    !------------- 1st order (donor cell) interpolated values --------------
    !
    if (iorder == 1) then
      !
      !  1.  Choose upwind value for characteristic bases "qp" and "qm".
      !
      do j = js, jep1
        q1 = sign(haf, vp(j))
        q2 = sign(haf, vm(j))
        qp(j) = (haf + q1)*q(j - 1) + (haf - q1)*q(j)
        qm(j) = (haf + q2)*q(j - 1) + (haf - q2)*q(j)
      end do
      go to 130
    end if
    !
    !-------------- 2nd order (van Leer) interpolated values ---------------
    !
    if (iorder == 2) then
      !
      !  1.  Evaluate monotonized, van Leer difference in "q" across the zone.
      !
      do j = jsm1, jep1
        dqjm = (q(j) - q(j - 1))*dx2bi(j)
        dqjp = (q(j + 1) - q(j))*dx2bi(j + 1)
        dqi(j) = max(dqjm*dqjp, zro) &
                 *sign(one, dqjm + dqjp) &
                 /max(abs(dqjm + dqjp), verysmall)
      end do
      !
      !  2.  Perform an upwinded interpolation of "q" to the time-centred
      !      bases of the characteristics.
      !
      fact = dt*g2i(i)
      do j = js, jep1
        q1 = q(j - 1) + dx2a(j - 1)*dqi(j - 1)
        q2 = q(j) - dx2a(j)*dqi(j)
        !
        xi = vp(j)*fact
        q3 = sign(haf, xi)
        qp(j) = (haf + q3)*(q1 - xi*dqi(j - 1)) &
                + (haf - q3)*(q2 - xi*dqi(j))
        !
        xi = vm(j)*fact
        q3 = sign(haf, xi)
        qm(j) = (haf + q3)*(q1 - xi*dqi(j - 1)) &
                + (haf - q3)*(q2 - xi*dqi(j))
      end do
      go to 130
    end if
    !
    !----------------------- interpolations complete -----------------------
    !
130 continue
    !
    !      Reset boundary values for outflow boundary conditions.
    !
    if (inner_j_outflow .and. l_firstnode2) qm(js) = qp(js)
    if (outer_i_outflow .and. l_lastnode2) qp(jep1) = qm(jep1)
    !
  END SUBROUTINE x2zc1d
  !
  !=======================================================================
  !
  SUBROUTINE x2zc3d(qty, vel2, kbeg, ibeg, kend, iend, iorder, isteep, g2, &
                    g2i, qi)
    !
    !    dac:zeus3d.x2zc3d <----- x2-interpolation of zone-centred 3-D array
    !    from dac:zeus2d.x2intzc, dac.zeus04.rint                  may, 1990
    !
    !    written by: David Clarke
    !    modified 1: September, 1990 by David Clarke; moved all functions
    !                of the grid coordinates for the PPA algorithm to a new
    !                subroutine PPAFNC, which is called each time the grid
    !                is updated.  Removed all "if" statements from inner
    !                do-loops.  Execution times for the donor cell, van
    !                Leer, and PPA algorithms have been reduced by 30%, 25%,
    !                and 50% respectively.  The contact steepening algorithm
    !                has been implemented.
    !    modified 2: April 1995 by Robert Fiedler; rewritten for running in
    !                parallel on shared memory multiprocessors.
    !
    !  PURPOSE:  The zone-centred 3-D array "qty" is interpolated to the
    !  2-interfaces, and the results are stored in "qi".  The interpolated
    !  values can be first order (donor cell), second order (van Leer), or
    !  third order (piecewise parabolic advection) accurate, depending on
    !  the value of "iorder".  All interpolation schemes are monotonic and
    !  upwinded, and for PPA, contact discontinuities are steepened when
    !  "isteep" = 2.  Global extrema are not monotonized in the PPA
    !  algorithm, ensuring monotonic derivatives.
    !
    !  The PPA algorithm is based on Colella and Woodward, J. Comp. Phys.
    !  54:174 (1984).  References to equation numbers refer to that paper.
    !
    !  The following grid functions used in the PPA algorithm are computed
    !  in PPAFNC.
    !
    !    f1dx2a, f2dx2a, f3dx2a, f4dx2a, f5dx2a, f6dx2a, f7dx2a, f8dx2a,
    !    f9dx2b
    !
    !  INPUT VARIABLES:
    !    qty       3-D array to be interpolated at the 2-interfaces (NOTE:
    !              the active zones for "qty" in the 1-direction should be
    !              j=js to je)
    !    vel2      2-velocities at 2-interfaces, possibly averaged in k or
    !              i, but not in j.  It is assumed that the grid velocities
    !              (if any) have NOT been subtracted from "vel2".
    !    kbeg      beginning index for k-sweeps
    !    ibeg      beginning index for i-sweeps
    !    kend      ending    index for k-sweeps
    !    iend      ending    index for i-sweeps
    !    g2        metric scale factor which is either "g2a" or "g2b",
    !              depending on centring of variable "q"
    !    g2i       inverse of "g2"
    !    iorder    desired order of interpolation
    !    isteep    steepener switch
    !              = 0 => always off
    !              = 1 => always on
    !              = 2 => on only for contact discontinuity
    !
    !  OUTPUT VARIABLES:
    !    qi        3-D array containing "qty" interpolated at 2-interfaces
    !
    !  LOCAL VARIABLES:
    !    q         k, i sweep of "qty" currently being interpolated (allows
    !              "qty" and "qi" to use the same array)
    !    dq        face-centred difference of "q"
    !    d2q       zone-centred product    of "dq" (van Leer)
    !              zone-centred difference of "dq" (ppa)
    !    dqi       zone-centred difference of "q" between interfaces
    !    dql       left  face-centred difference in "q" (ppa)
    !    dqr       right face-centred difference in "q" (ppa)
    !    qli       "q" interpolated to left  interface
    !    qri       "q" interpolated to right interface
    !    qlm       monotonized value of "qli" (ppa)
    !    qrm       monotonized value of "qri" (ppa)
    !    glexb     =0.0 => zone-centred quantity should be monotonized
    !                      because "q" is a LOCAL extremum (ppa)
    !              =1.0 => zone-centred quantity should NOT be monotonized
    !                      because "q" is a GLOBAL extremum (ppa)
    !    glexa     similar to "glexb", but for face-centred quantities (ppa)
    !    steep     indicates whether interpolation is to be steepened (ppa
    !              steepener)
    !    p         3-D pressure array - used by ppa steepener, equivalenced
    !              to "qi" in the subroutine parameter list.
    !
    !-----------------------------------------------------------------------
    !
    REALNUM, DIMENSION(:, :, :), INTENT(IN) :: qty, vel2
    REALNUM, DIMENSION(isa:iea), INTENT(IN) :: g2, g2i
    REALNUM, INTENT(OUT) :: qi(:, :, :)
    INTEGER, INTENT(IN) :: kbeg, ibeg, kend, iend, iorder, isteep
    !
    INTEGER i, j, k, kp1, ip1
    REALNUM q1, q2, q3, xi, dqjm, dqjp
    REALNUM, POINTER, SAVE :: dqji(:, :, :) => null()
    !
    !-----------------------------------------------------------------------
    !
    !      If problem is j-symmetric, set "qi" to "qty" and return.
    !
    if (jnac == 0) then
      do k = kbeg, kend
        do i = ibeg, iend
          qi(i, js, k) = qty(i, js, k)
        end do
      end do
      return
    end if
    !
    !------------- 1st order (donor cell) interpolated values --------------
    !
    if (iorder == 1) then
      do k = kbeg, kend
        ! RFK      do j=js,jep1
        do j = jsm1, jep1
          do i = ibeg, iend
            !
            !  1.  Choose upwind value for the interface value "qi".
            !
            q1 = sign(haf, vel2(i, j, k) - vg2(j))
            qi(i, j, k) = (haf + q1)*qty(i, j - 1, k) &
                          + (haf - q1)*qty(i, j, k)
          end do
          !
        end do
      end do
      return
    end if
    !
    !-------------- 2nd order (van Leer) interpolated values ---------------
    !
    if (iorder == 2) then
      call get_memory(dqji)
      !
      !  1.  Evaluate monotonized, van Leer difference in "q" across the zone.
      !
      do k = kbeg, kend
        ! RFK      do j=jsm1,jep1
        do j = jsm2, jep1
          do i = ibeg, iend
            dqjm = (qty(i, j, k) - qty(i, j - 1, k))*dx2bi(j)
            dqjp = (qty(i, j + 1, k) - qty(i, j, k))*dx2bi(j + 1)
            dqji(i, j, k) = max(dqjm*dqjp, zro) &
                            *sign(one, dqjm + dqjp) &
                            /max(abs(dqjm + dqjp), verysmall)
          end do
        end do
      end do
      !
      !  2.  Choose time averaged, upwinded interface value.
      !
      do k = kbeg, kend
        ! RFK      do j=js,jep1
        do j = jsm1, jep1
          do i = ibeg, iend
            xi = (vel2(i, j, k) - vg2(j))*dt*g2i(i)
            q1 = sign(haf, xi)
            qi(i, j, k) = (haf + q1)*(qty(i, j - 1, k) &
                                      + (dx2a(j - 1) - xi)*dqji(i, j - 1, k)) &
                          + (haf - q1)*(qty(i, j, k) &
                                        - (dx2a(j) + xi)*dqji(i, j, k))
          end do
        end do
      end do
      call release_memory(dqji)
      return
    end if
    !
  END SUBROUTINE x2zc3d
  !
  !=======================================================================
  !
  SUBROUTINE x3fc3d(qty, vel3, ibeg, jbeg, iend, jend, iorder, qi)
    !
    !    dac:zeus3d.x3fc3d <----- x3-interpolation of face-centred 3-D array
    !                                                              may, 1990
    !
    !    written by: David Clarke
    !    modified 1: September, 1990 by David Clarke; moved all functions
    !                of the grid coordinates for the PPA algorithm to a new
    !                subroutine PPAFNC, which is called each time the grid
    !                is updated.  Removed all "if" statements from inner
    !                do-loops.  Execution times for the donor cell, van
    !                Leer, and PPA algorithms have been reduced by 30%, 25%,
    !                and 50% respectively.  The contact steepening algorithm
    !                has been removed since it is never needed for face-
    !                centred quantities.
    !    modified 2: April 1995 by Robert Fiedler; rewritten for running in
    !                parallel on shared memory multiprocessors.
    !
    !  PURPOSE:  The face-centred 3-D array "qty" is interpolated to the
    !  zone centres along the 3-direction, and the results are stored in
    !  "qi".  The interpolated values can be first order (donor cell),
    !  second order (van Leer), or third order (piecewise parabolic
    !  advection) accurate, depending on the value of "iorder".  All
    !  interpolation schemes are monotonic and upwinded.  Global extrema are
    !  not monotonized in the PPA algorithm, ensuring monotonic derivatives.
    !
    !  The PPA algorithm is based on Colella and Woodward, J. Comp. Phys.
    !  54:174 (1984).  References to equation numbers refer to that paper.
    !
    !  The following grid functions used in the PPA algorithm are computed
    !  in PPAFNC.
    !
    !    f1dx3b, f2dx3b, f3dx3b, f4dx3b, f5dx3b, f6dx3b, f7dx3b, f8dx3b,
    !
    !  INPUT VARIABLES:
    !    qty       3-D array to be interpolated at zone-centres (NOTE: the
    !              active zones for "qty" in the 3-direction should be
    !              k=ksp1 to ke)
    !    vel3      3-velocities at zone-centres, possibly averaged in k.  It
    !              is assumed that the grid velocities (if any) have been
    !              subtracted from "vel3".
    !    ibeg      beginning index for i-sweeps
    !    jbeg      beginning index for j-sweeps
    !    iend      ending    index for i-sweeps
    !    jend      ending    index for j-sweeps
    !    iorder    desired order of interpolation
    !
    !  OUTPUT VARIABLES:
    !    qi        3-D array containing "qty" interpolated at zone centres
    !
    !  LOCAL VARIABLES:
    !    q         i, j sweep of "qty" currently being interpolated (allows
    !              "qty" and "qi" to use the same array)
    !    dq        zone-centred difference of "q"
    !    d2q       face-centred product    of "dq" (van Leer)
    !              face-centred difference of "dq" (ppa)
    !    dqi       face-centred difference of "q" between interfaces
    !    dql       left  zone-centred difference in "q" (ppa)
    !    dqr       right zone-centred difference in "q" (ppa)
    !    qli       "q" interpolated to left  zone centre
    !    qri       "q" interpolated to right zone centre
    !    qlm       monotonized value of "qli" (ppa)
    !    qrm       monotonized value of "qri" (ppa)
    !    glexa     =0.0 => face-centred quantity should be monotonized
    !                      because "q" is a LOCAL extremum (ppa)
    !              =1.0 => face-centred quantity should NOT be monotonized
    !                      because "q" is a GLOBAL extremum (ppa)
    !    glexb     similar to "glexa", but for zone-centred quantities (ppa)
    !
    !-----------------------------------------------------------------------
    !
    REALNUM, DIMENSION(:, :, :), INTENT(IN) :: qty, vel3
    INTEGER, INTENT(IN) ::  ibeg, jbeg, iend, jend, iorder
    REALNUM, INTENT(OUT) ::      qi(:, :, :)
    !
    INTEGER i, j, k
    REALNUM q1, q2, q3, xi, dqkm, dqkp
    REALNUM, POINTER, SAVE :: dqki(:, :, :) => null()
    !
    !-----------------------------------------------------------------------
    !
    !      If problem is k-symmetric, set "qi" to "qty" and return.
    !
    if (knac == 0) then
      do j = jbeg, jend
        do i = ibeg, iend
          qi(i, j, ks) = qty(i, j, ks)
        end do
      end do
      return
    end if
    !
    !------------- 1st order (donor cell) interpolated values --------------
    !
    if (iorder == 1) then
      ! RFK    do k=ks,ke
      do k = ksm1, ke
        do j = jbeg, jend
          do i = ibeg, iend
            !
            !  1.  Choose upwind value for the interface value "qi".
            !
            q1 = sign(haf, vel3(i, j, k))
            qi(i, j, k) = (haf + q1)*qty(i, j, k) &
                          + (haf - q1)*qty(i, j, k + 1)
          end do
          !
        end do
      end do
      return
    end if
    !
    !-------------- 2nd order (van Leer) interpolated values ---------------
    !
    if (iorder == 2) then
      call get_memory(dqki)
      !
      !  1.  Evaluate monotonized, van Leer difference in "q" across the zone.
      !
      ! RFK    do k=ks,kep1
      do k = ksm1, kep1
        do j = jbeg, jend
          do i = ibeg, iend
            dqkm = (qty(i, j, k) - qty(i, j, k - 1))*dx3ai(k - 1)
            dqkp = (qty(i, j, k + 1) - qty(i, j, k))*dx3ai(k)
            dqki(i, j, k) = max(dqkm*dqkp, zro) &
                            *sign(one, dqkm + dqkp) &
                            /max(abs(dqkm + dqkp), verysmall)
          end do
        end do
      end do
      !
      !  2.  Choose time averaged, upwinded interface value.
      !
      ! RFK    do k=ks,ke
      do k = ksm1, ke
        do j = jbeg, jend
          do i = ibeg, iend
            xi = vel3(i, j, k)*dt*g31bi(i)*g32bi(j)
            q1 = sign(haf, xi)
            qi(i, j, k) = (haf + q1)*(qty(i, j, k) &
                                      + (dx3b(k) - xi)*dqki(i, j, k)) &
                          + (haf - q1)*(qty(i, j, k + 1) &
                                        - (dx3b(k + 1) + xi)*dqki(i, j, k + 1))
          end do
        end do
      end do
      call release_memory(dqki)
      return
    end if
    !
  END SUBROUTINE x3fc3d
  !
  !=======================================================================
  !
  SUBROUTINE x3zc1d(q, vp, vm, iorder, isteep, i, j, g31, g31i, g32, g32i, &
                    qp, qm)
    !
    !    dac:zeus3d.x3zc1d <----- x3-interpolation of zone-centred 1-D array
    !    from dac:zeus3d.x3zc3d                              september, 1990
    !
    !    written by: David Clarke
    !    modified 1: October 1992, by David Clarke; made compatable with the
    !                new MOCEMFS routine.
    !    modified 2: April 1995 by Robert Fiedler; rewritten for running in
    !                parallel on shared memory multiprocessors.
    !
    !  PURPOSE:  This subroutine performs interpolations on 1-D vectors
    !  for the routine MOCEMFS in the 3-direction.  It uses the same
    !  interpolating algorithms used by X3ZC3D and X3FC3D.  The steepener
    !  algorithm is included only for testing purposes.  Thus, the detecting
    !  algorithm for contact discontinuities has been omitted.  See the
    !  comments for X3ZC3D for additional details.
    !
    !  INPUT VARIABLES:
    !    q         1-D array to be interpolated (NOTE: the active zones for
    !              "q" should be k=ks to ke)
    !    vp        velocity along the "plus"  characteristic
    !    vm        velocity along the "minus" characteristic
    !    iorder    desired order of interpolation
    !    isteep    steepener switch
    !              = 0 => always off
    !              > 0 => always on
    !    i,j       1- and 2-indices in calling routine
    !    g31       metric factor (either "g31a" or "g31b")
    !    g31i      inverse of "g31".
    !    g32       metric factor (either "g32a" or "g32b")
    !    g32i      inverse of "g32".
    !
    !  OUTPUT VARIABLES:
    !    qp        "q" interpolated to base of "plus"  characteristic
    !    qm        "q" interpolated to base of "minus" characteristic
    !
    !  LOCAL VARIABLES:
    !    dq        face-centred difference of "q"  (van Leer)
    !    d2q       zone-centred product    of "dq" (van Leer)
    !              zone-centred difference of "dq" (ppa)
    !    dqi       zone-centred difference of "q" between interfaces
    !    dql       left  face-centred difference of "q" (ppa)
    !    dqr       right face-centred difference in "q" (ppa)
    !    glexb     =0.0 => zone-centred quantity should be monotonized
    !                      because "q" is a LOCAL extremum
    !              =1.0 => zone-centred quantity should NOT be monotonized
    !                      because "q" is a GLOBAL extremum (ppa)
    !    glexa     similar to "glexb", but for face-centred quantities (ppa)
    !    qli       "q" interpolated to left  interface (ppa)
    !    qri       "q" interpolated to right interface (ppa)
    !    qlm       monotonized value of "qli" (ppa)
    !    qrm       monotonized value of "qri" (ppa)
    !
    !-----------------------------------------------------------------------
    !
    REALNUM, INTENT(IN) ::   q(ksa:kea), vp(ksa:kea), vm(ksa:kea)
    REALNUM, INTENT(IN) ::   g31(isa:iea), g31i(isa:iea)
    REALNUM, INTENT(IN) ::   g32(jsa:jea), g32i(jsa:jea)
    INTEGER, INTENT(IN) :: iorder, isteep, i, j
    REALNUM, INTENT(OUT) ::  qp(ksa:kea), qm(ksa:kea)
    !
    INTEGER k
    REALNUM q1, q2, q3, xi, dqkm, dqkp, fact
    REALNUM dqi(ksa:kea)
    !
    !-----------------------------------------------------------------------
    !
    !      If problem is k-symmetric, set "qm" and "qp" to "q" and return.
    !
    if (knac == 0) then
      qp(ks) = q(ks)
      qm(ks) = q(ks)
      return
    end if
    !
    !------------- 1st order (donor cell) interpolated values --------------
    !
    if (iorder == 1) then
      !
      !  1.  Choose upwind value for characteristic bases "qp" and "qm".
      !
      do k = ks, kep1
        q1 = sign(haf, vp(k))
        q2 = sign(haf, vm(k))
        qp(k) = (haf + q1)*q(k - 1) + (haf - q1)*q(k)
        qm(k) = (haf + q2)*q(k - 1) + (haf - q2)*q(k)
      end do
      go to 130
    end if
    !
    !-------------- 2nd order (van Leer) interpolated values ---------------
    !
    if (iorder == 2) then
      !
      !  1.  Evaluate monotonized, van Leer difference in "q" across the zone.
      !
      do k = ksm1, kep1
        dqkm = (q(k) - q(k - 1))*dx3bi(k)
        dqkp = (q(k + 1) - q(k))*dx3bi(k + 1)
        dqi(k) = max(dqkm*dqkp, zro) &
                 *sign(one, dqkm + dqkp) &
                 /max(abs(dqkm + dqkp), verysmall)
      end do
      !
      !  2.  Perform an upwinded interpolation of "q" to the time-centred
      !      bases of the characteristics.
      !
      fact = dt*g31i(i)*g32i(j)
      do k = ks, kep1
        q1 = q(k - 1) + dx3a(k - 1)*dqi(k - 1)
        q2 = q(k) - dx3a(k)*dqi(k)
        !
        xi = vp(k)*fact
        q3 = sign(haf, xi)
        qp(k) = (haf + q3)*(q1 - xi*dqi(k - 1)) &
                + (haf - q3)*(q2 - xi*dqi(k))
        !
        xi = vm(k)*fact
        q3 = sign(haf, xi)
        qm(k) = (haf + q3)*(q1 - xi*dqi(k - 1)) &
                + (haf - q3)*(q2 - xi*dqi(k))
      end do
      go to 130
    end if
    !
    !----------------------- interpolations complete -----------------------
    !
130 continue
    !
    !      Reset boundary values for outflow boundary conditions.
    !
    if (inner_k_outflow .and. l_firstnode3) qm(ks) = qp(ks)
    if (outer_k_outflow .and. l_lastnode3) qp(kep1) = qm(kep1)
    !
  END SUBROUTINE x3zc1d
  !
  !=======================================================================
  !
  SUBROUTINE x3zc3d(qty, vel3, ibeg, jbeg, iend, jend, iorder, isteep, g31, &
                    g31i, g32, g32i, qi)
    !
    !    dac:zeus3d.x3zc3d <----- x3-interpolation of zone-centred 3-D array
    !                                                              may, 1990
    !
    !    written by: David Clarke
    !    modified 1: September, 1990 by David Clarke; moved all functions
    !                of the grid coordinates for the PPA algorithm to a new
    !                subroutine PPAFNC, which is called each time the grid
    !                is updated.  Removed all "if" statements from inner
    !                do-loops.  Execution times for the donor cell, van
    !                Leer, and PPA algorithms have been reduced by 30%, 25%,
    !                and 50% respectively.  The contact steepening algorithm
    !                has been implemented.
    !    modified 2: April 1995 by Robert Fiedler; rewritten for running in
    !                parallel on shared memory multiprocessors.
    !
    !  PURPOSE:  The zone-centred 3-D array "qty" is interpolated to the
    !  3-interfaces, and the results are stored in "qi".  The interpolated
    !  values can be first order (donor cell), second order (van Leer), or
    !  third order (piecewise parabolic advection) accurate, depending on
    !  the value of "iorder".  All interpolation schemes are monotonic and
    !  upwinded, and for PPA, contact discontinuities are steepened when
    !  "isteep" = 2.  Global extrema are not monotonized in the PPA
    !  algorithm, ensuring monotonic derivatives.
    !
    !  The PPA algorithm is based on Colella and Woodward, J. Comp. Phys.
    !  54:174 (1984).  References to equation numbers refer to that paper.
    !
    !  The following grid functions used in the PPA algorithm are computed
    !  in PPAFNC.
    !
    !    f1dx3a, f2dx3a, f3dx3a, f4dx3a, f5dx3a, f6dx3a, f7dx3a, f8dx3a,
    !    f9dx3b
    !
    !  INPUT VARIABLES:
    !    qty       3-D array to be interpolated at the 3-interfaces (NOTE:
    !              the active zones for "qty" in the 3-direction should be
    !              k=ks to ke)
    !    vel3      3-velocities at 3-interfaces, possibly averaged in i or
    !              j, but not in k.  It is assumed that the grid velocities
    !              (if any) have NOT been subtracted from "vel3".
    !    ibeg      beginning index for i-sweeps
    !    jbeg      beginning index for j-sweeps
    !    iend      ending    index for i-sweeps
    !    jend      ending    index for j-sweeps
    !    g31       metric scale factor which is either "g31a" or "g31b",
    !              depending on centring of variable "q"
    !    g31i      inverse of "g31"
    !    g32       metric scale factor which is either "g32a" or "g32b",
    !              depending on centring of variable "q"
    !    g32i      inverse of "g32"
    !    iorder    desired order of interpolation
    !    isteep    steepener switch
    !              = 0 => always off
    !              = 1 => always on
    !              = 2 => on only for contact discontinuity
    !
    !  OUTPUT VARIABLES:
    !    qi        3-D array containing "qty" interpolated at 3-interfaces
    !
    !  LOCAL VARIABLES:
    !    q         i, j sweep of "qty" currently being interpolated (allows
    !              "qty" and "qi" to use the same array)
    !    dq        face-centred difference of "q"
    !    d2q       zone-centred product    of "dq" (van Leer)
    !              zone-centred difference of "dq" (ppa)
    !    dqi       zone-centred difference of "q" between interfaces
    !    dql       left  face-centred difference in "q" (ppa)
    !    dqr       right face-centred difference in "q" (ppa)
    !    qli       "q" interpolated to left  interface
    !    qri       "q" interpolated to right interface
    !    qlm       monotonized value of "qli" (ppa)
    !    qrm       monotonized value of "qri" (ppa)
    !    glexb     =0.0 => zone-centred quantity should be monotonized
    !                      because "q" is a LOCAL extremum (ppa)
    !              =1.0 => zone-centred quantity should NOT be monotonized
    !                      because "q" is a GLOBAL extremum (ppa)
    !    glexa     similar to "glexb", but for face-centred quantities (ppa)
    !    steep     indicates whether interpolation is to be steepened (ppa
    !              steepener)
    !    p         3-D pressure array - used by ppa steepener, equivalenced
    !              to "qi" in the subroutine parameter list.
    !
    !-----------------------------------------------------------------------
    !
    REALNUM, DIMENSION(:, :, :), INTENT(IN) :: qty, vel3
    REALNUM, INTENT(OUT) :: qi(:, :, :)
    REALNUM, INTENT(IN) :: g31(isa:iea), g31i(isa:iea)
    REALNUM, INTENT(IN) :: g32(jsa:jea), g32i(jsa:jea)
    INTEGER, INTENT(IN) :: ibeg, jbeg, iend, jend, iorder, isteep
    !
    INTEGER i, j, k, ip1, jp1
    REALNUM q1, q2, q3, xi, dqkm, dqkp
    REALNUM, POINTER, SAVE :: dqki(:, :, :) => null()
    !
    !-----------------------------------------------------------------------
    !
    !      If problem is k-symmetric, set "qi" to "qty" and return.
    !
    if (knac == 0) then
      do j = jbeg, jend
        do i = ibeg, iend
          qi(i, j, ks) = qty(i, j, ks)
        end do
      end do
      return
    end if
    !
    !------------- 1st order (donor cell) interpolated values --------------
    !
    if (iorder == 1) then
      ! RFK    do k=ks,kep1
      do k = ksm1, kep1
        do j = jbeg, jend
          do i = ibeg, iend
            !
            !  1.  Choose upwind value for the interface value "qi".
            !
            q1 = sign(haf, vel3(i, j, k) - vg3(k))
            qi(i, j, k) = (haf + q1)*qty(i, j, k - 1) &
                          + (haf - q1)*qty(i, j, k)
          end do
          !
        end do
      end do
      return
    end if
    !
    !-------------- 2nd order (van Leer) interpolated values ---------------
    !
    if (iorder == 2) then
      call get_memory(dqki)
      !
      !  1.  Evaluate monotonized, van Leer difference in "q" across the zone.
      !
      ! RFK    do k=ksm1,kep1
      do k = ksm2, kep1
        do j = jbeg, jend
          do i = ibeg, iend
            dqkm = (qty(i, j, k) - qty(i, j, k - 1))*dx3bi(k)
            dqkp = (qty(i, j, k + 1) - qty(i, j, k))*dx3bi(k + 1)
            dqki(i, j, k) = max(dqkm*dqkp, zro) &
                            *sign(one, dqkm + dqkp) &
                            /max(abs(dqkm + dqkp), verysmall)
          end do
        end do
      end do
      !
      !  2.  Choose time averaged, upwinded interface value.
      !
      ! RFK    do k=ks,kep1
      do k = ksm1, kep1
        do j = jbeg, jend
          do i = ibeg, iend
            q2 = dt*g31i(i)*g32i(j)
            xi = (vel3(i, j, k) - vg3(k))*q2
            q1 = sign(haf, xi)
            qi(i, j, k) = (haf + q1)*(qty(i, j, k - 1) &
                                      + (dx3a(k - 1) - xi)*dqki(i, j, k - 1)) &
                          + (haf - q1)*(qty(i, j, k) &
                                        - (dx3a(k) + xi)*dqki(i, j, k))
          end do
        end do
      end do
      call release_memory(dqki)
      return
    end if
    !
  END SUBROUTINE x3zc3d
  !
  !=======================================================================
  !
  SUBROUTINE x1int1d(q, vp, iorder, isteep, j, k, qp)
    !
    !    dac:zeus3d.x1zc1d <----- x1-interpolation of zone-centred 1-D array
    !    from dac:zeus3d.x1zc3d                              september, 1990
    !
    !    written by: David Clarke
    !    modified 1: June 1994, by Byung-Il Jun; made compatable with the
    !                new MOCEMFS routine.
    !    modified 2: April 1995 by Robert Fiedler; rewritten for running in
    !                parallel on shared memory multiprocessors.
    !
    !  PURPOSE:  This subroutine performs interpolations on 1-D vectors
    !  for the routine MOCEMFS in the 1-direction.  It uses the same
    !  interpolating algorithms used by X1ZC3D and X1FC3D.  The steepener
    !  algorithm is included only for testing purposes.  Thus, the detecting
    !  algorithm for contact discontinuities has been omitted.  See the
    !  comments for X1ZC3D for additional details.
    !
    !  INPUT VARIABLES:
    !    q         1-D array to be interpolated (NOTE: the active zones for
    !              "q" should be i=is to ie)
    !    vp        velocity along the   characteristic
    !    iorder    desired order of interpolation
    !    isteep    steepener switch
    !              = 0 => always off
    !              > 0 => always on
    !    j,k       2- and 3-indices in calling routine
    !              (not used, kept for regularity)
    !
    !  OUTPUT VARIABLES:
    !    qp        "q" interpolated to base of  characteristic
    !
    !  LOCAL VARIABLES:
    !    dq        face-centred difference of "q"  (van Leer)
    !    d2q       zone-centred product    of "dq" (van Leer)
    !              zone-centred difference of "dq" (ppa)
    !    dqi       zone-centred difference of "q" between interfaces
    !    dql       left  face-centred difference of "q" (ppa)
    !    dqr       right face-centred difference in "q" (ppa)
    !    glexb     =0.0 => zone-centred quantity should be monotonized
    !                      because "q" is a LOCAL extremum
    !              =1.0 => zone-centred quantity should NOT be monotonized
    !                      because "q" is a GLOBAL extremum (ppa)
    !    glexa     similar to "glexb", but for face-centred quantities (ppa)
    !    qli       "q" interpolated to left  interface (ppa)
    !    qri       "q" interpolated to right interface (ppa)
    !    qlm       monotonized value of "qli" (ppa)
    !    qrm       monotonized value of "qri" (ppa)
    !
    !-----------------------------------------------------------------------
    !
    REALNUM, INTENT(IN) :: q(isa:iea), vp(isa:iea)
    REALNUM, INTENT(OUT) :: qp(isa:iea)
    INTEGER, INTENT(IN) :: j, k, iorder, isteep
    !
    INTEGER i
    REALNUM q1, q2, q3, xi, dqim, dqip
    REALNUM dqi(isa:iea)
    !
    !-----------------------------------------------------------------------
    !
    !      If problem is i-symmetric, set "qp" to "q" and return.
    !
    if (inac == 0) then
      qp(is) = q(is)
      return
    end if
    !
    !------------- 1st order (donor cell) interpolated values --------------
    !
    if (iorder == 1) then
      !
      !  1.  Choose upwind value for characteristic bases "qp".
      !
      do i = is, iep1
        q1 = sign(haf, vp(i))
        qp(i) = (haf + q1)*q(i - 1) + (haf - q1)*q(i)
      end do
      return
    end if
    !
    !-------------- 2nd order (van Leer) interpolated values ---------------
    !
    if (iorder == 2) then
      !
      !  1.  Evaluate monotonized, van Leer difference in "q" across the zone.
      !
      do i = ism1, iep1
        dqim = (q(i) - q(i - 1))*dx1bi(i)
        dqip = (q(i + 1) - q(i))*dx1bi(i + 1)
        dqi(i) = max(dqim*dqip, zro) &
                 *sign(one, dqim + dqip) &
                 /max(abs(dqim + dqip), verysmall)
      end do
      !
      !  2.  Perform an upwinded interpolation of "q" to the time-centred
      !      bases of the characteristics.
      !
      do i = is, iep1
        q1 = q(i - 1) + dx1a(i - 1)*dqi(i - 1)
        q2 = q(i) - dx1a(i)*dqi(i)
        !
        xi = vp(i)*dt
        q3 = sign(haf, xi)
        qp(i) = (haf + q3)*(q1 - xi*dqi(i - 1)) &
                + (haf - q3)*(q2 - xi*dqi(i))
        !
      end do
      return
    end if
  END SUBROUTINE x1int1d
  !
  !=======================================================================
  !
  SUBROUTINE x2int1d(q, vp, iorder, isteep, k, i, g2, g2i, qp)
    !
    !    dac:zeus3d.x2zc1d <----- x2-interpolation of zone-centred 1-D array
    !    from dac:zeus3d.x2zc3d                              september, 1990
    !
    !    written by: David Clarke
    !    modified 1: June 1994, by Byung-Il Jun; made compatable with the
    !                new MOCEMFS routine.
    !    modified 2: April 1995 by Robert Fiedler; rewritten for running in
    !                parallel on shared memory multiprocessors.
    !
    !  PURPOSE:  This subroutine performs interpolations on 1-D vectors
    !  for the routine MOCEMFS in the 2-direction.  It uses the same
    !  interpolating algorithms used by X2ZC3D and X2FC3D.  The steepener
    !  algorithm is included only for testing purposes.  Thus, the detecting
    !  algorithm for contact discontinuities has been omitted.  See the
    !  comments for X2ZC3D for additional details.
    !
    !  INPUT VARIABLES:
    !    q         1-D array to be interpolated (NOTE: the active zones for
    !              "q" should be j=js to je)
    !    vp        velocity along the  characteristic
    !    iorder    desired order of interpolation
    !    isteep    steepener switch
    !              = 0 => always off
    !              > 0 => always on
    !    k,i       3- and 1-indices in calling routine
    !    g2        metric factor (either "g2a" or "g2b")
    !    g2i       inverse of "g2".
    !
    !  OUTPUT VARIABLES:
    !    qp        "q" interpolated to base of  characteristic
    !
    !  LOCAL VARIABLES:
    !    dq        face-centred difference of "q"  (van Leer)
    !    d2q       zone-centred product    of "dq" (van Leer)
    !              zone-centred difference of "dq" (ppa)
    !    dqi       zone-centred difference of "q" between interfaces
    !    dql       left  face-centred difference of "q" (ppa)
    !    dqr       right face-centred difference in "q" (ppa)
    !    glexb     =0.0 => zone-centred quantity should be monotonized
    !                      because "q" is a LOCAL extremum
    !              =1.0 => zone-centred quantity should NOT be monotonized
    !                      because "q" is a GLOBAL extremum (ppa)
    !    glexa     similar to "glexb", but for face-centred quantities (ppa)
    !    qli       "q" interpolated to left  interface (ppa)
    !    qri       "q" interpolated to right interface (ppa)
    !    qlm       monotonized value of "qli" (ppa)
    !    qrm       monotonized value of "qri" (ppa)
    !
    !-----------------------------------------------------------------------
    !
    REALNUM, INTENT(IN) ::  q(jsa:jea), vp(jsa:jea)
    REALNUM, INTENT(IN) :: g2(isa:iea), g2i(isa:iea)
    REALNUM, INTENT(OUT) :: qp(jsa:jea)
    INTEGER, INTENT(IN) :: k, i, iorder, isteep
    !
    INTEGER j
    REALNUM q1, q2, q3, xi, dqjm, dqjp, fact
    REALNUM dqi(jsa:jea)
    !
    !-----------------------------------------------------------------------
    !
    !      If problem is j-symmetric, set "qm" and "qp" to "q" and return.
    !
    if (jnac == 0) then
      qp(js) = q(js)
      return
    end if
    !
    !------------- 1st order (donor cell) interpolated values --------------
    !
    if (iorder == 1) then
      !
      !  1.  Choose upwind value for characteristic bases "qp" and "qm".
      !
      do j = js, jep1
        q1 = sign(haf, vp(j))
        qp(j) = (haf + q1)*q(j - 1) + (haf - q1)*q(j)
      end do
      return
    end if
    !
    !-------------- 2nd order (van Leer) interpolated values ---------------
    !
    if (iorder == 2) then
      !
      !  1.  Evaluate monotonized, van Leer difference in "q" across the zone.
      !
      do j = jsm1, jep1
        dqjm = (q(j) - q(j - 1))*dx2bi(j)
        dqjp = (q(j + 1) - q(j))*dx2bi(j + 1)
        dqi(j) = max(dqjm*dqjp, zro) &
                 *sign(one, dqjm + dqjp) &
                 /max(abs(dqjm + dqjp), verysmall)
      end do
      !
      !  2.  Perform an upwinded interpolation of "q" to the time-centred
      !      bases of the characteristics.
      !
      fact = dt*g2i(i)
      do j = js, jep1
        q1 = q(j - 1) + dx2a(j - 1)*dqi(j - 1)
        q2 = q(j) - dx2a(j)*dqi(j)
        !
        xi = vp(j)*fact
        q3 = sign(haf, xi)
        qp(j) = (haf + q3)*(q1 - xi*dqi(j - 1)) &
                + (haf - q3)*(q2 - xi*dqi(j))
        !
      end do
      return
    end if
  END SUBROUTINE x2int1d
  !
  !=======================================================================
  !
  SUBROUTINE x3int1d(q, vp, iorder, isteep, i, j, g31, g31i, g32, g32i, qp)
    !
    !    dac:zeus3d.x3zc1d <----- x3-interpolation of zone-centred 1-D array
    !    from dac:zeus3d.x3zc3d                              september, 1990
    !
    !    written by: David Clarke
    !    modified 1: June 1994, by Byung-Il Jun; made compatable with the
    !                new MOCEMFS routine.
    !    modified 2: April 1995 by Robert Fiedler; rewritten for running in
    !                parallel on shared memory multiprocessors.
    !
    !  PURPOSE:  This subroutine performs interpolations on 1-D vectors
    !  for the routine MOCEMFS in the 3-direction.  It uses the same
    !  interpolating algorithms used by X3ZC3D and X3FC3D.  The steepener
    !  algorithm is included only for testing purposes.  Thus, the detecting
    !  algorithm for contact discontinuities has been omitted.  See the
    !  comments for X3ZC3D for additional details.
    !
    !  INPUT VARIABLES:
    !    q         1-D array to be interpolated (NOTE: the active zones for
    !              "q" should be k=ks to ke)
    !    vp        velocity along the  characteristic
    !    iorder    desired order of interpolation
    !    isteep    steepener switch
    !              = 0 => always off
    !              > 0 => always on
    !    i,j       1- and 2-indices in calling routine
    !    g31       metric factor (either "g31a" or "g31b")
    !    g31i      inverse of "g31".
    !    g32       metric factor (either "g32a" or "g32b")
    !    g32i      inverse of "g32".
    !
    !  OUTPUT VARIABLES:
    !    qp        "q" interpolated to base of   characteristic
    !
    !  LOCAL VARIABLES:
    !    dq        face-centred difference of "q"  (van Leer)
    !    d2q       zone-centred product    of "dq" (van Leer)
    !              zone-centred difference of "dq" (ppa)
    !    dqi       zone-centred difference of "q" between interfaces
    !    dql       left  face-centred difference of "q" (ppa)
    !    dqr       right face-centred difference in "q" (ppa)
    !    glexb     =0.0 => zone-centred quantity should be monotonized
    !                      because "q" is a LOCAL extremum
    !              =1.0 => zone-centred quantity should NOT be monotonized
    !                      because "q" is a GLOBAL extremum (ppa)
    !    glexa     similar to "glexb", but for face-centred quantities (ppa)
    !    qli       "q" interpolated to left  interface (ppa)
    !    qri       "q" interpolated to right interface (ppa)
    !    qlm       monotonized value of "qli" (ppa)
    !    qrm       monotonized value of "qri" (ppa)
    !
    !-----------------------------------------------------------------------
    !
    REALNUM, INTENT(IN) :: q(ksa:kea), vp(ksa:kea)
    REALNUM, INTENT(IN) :: g31(isa:iea), g31i(isa:iea)
    REALNUM, INTENT(IN) :: g32(jsa:jea), g32i(jsa:jea)
    REALNUM, INTENT(OUT) :: qp(ksa:kea)
    INTEGER, INTENT(IN) :: i, j, iorder, isteep
    !
    INTEGER k
    REALNUM q1, q2, q3, xi, dqkm, dqkp, fact
    REALNUM dqi(ksa:kea)
    !
    !-----------------------------------------------------------------------
    !
    !      If problem is k-symmetric, set "qp" to "q" and return.
    !
    if (knac == 0) then
      qp(ks) = q(ks)
      return
    end if
    !
    !------------- 1st order (donor cell) interpolated values --------------
    !
    if (iorder == 1) then
      !
      !  1.  Choose upwind value for characteristic bases "qp".
      !
      do k = ks, kep1
        q1 = sign(haf, vp(k))
        qp(k) = (haf + q1)*q(k - 1) + (haf - q1)*q(k)
      end do
      return
    end if
    !
    !-------------- 2nd order (van Leer) interpolated values ---------------
    !
    if (iorder == 2) then
      !
      !  1.  Evaluate monotonized, van Leer difference in "q" across the zone.
      !
      do k = ksm1, kep1
        dqkm = (q(k) - q(k - 1))*dx3bi(k)
        dqkp = (q(k + 1) - q(k))*dx3bi(k + 1)
        dqi(k) = max(dqkm*dqkp, zro) &
                 *sign(one, dqkm + dqkp) &
                 /max(abs(dqkm + dqkp), verysmall)
      end do
      !
      !  2.  Perform an upwinded interpolation of "q" to the time-centred
      !      bases of the characteristics.
      !
      fact = dt*g31i(i)*g32i(j)
      do k = ks, kep1
        q1 = q(k - 1) + dx3a(k - 1)*dqi(k - 1)
        q2 = q(k) - dx3a(k)*dqi(k)
        !
        xi = vp(k)*fact
        q3 = sign(haf, xi)
        qp(k) = (haf + q3)*(q1 - xi*dqi(k - 1)) &
                + (haf - q3)*(q2 - xi*dqi(k))
        !
      end do
      return
    end if
  END SUBROUTINE x3int1d
END MODULE zeus_interpolators
