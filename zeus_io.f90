#include "macros"
MODULE zeus_io
  USE zeus_variables
  IMPLICIT NONE
  PRIVATE

  INTERFACE writedata
    ! Most of the options of this generic function are used because
    ! of the need to do the same things over and over again with
    ! data of different ranks.  Some precompiler tricks could
    ! avoid this inconvenience.  Assumed-size arrays would not work,
    ! because they strip away the size information.
    MODULE PROCEDURE write_real_rank4
    MODULE PROCEDURE write_real_rank3
    MODULE PROCEDURE write_real_rank2
    MODULE PROCEDURE write_real_rank1
    MODULE PROCEDURE write_real_rank0
    MODULE PROCEDURE write_integer_rank4
    MODULE PROCEDURE write_integer_rank3
    MODULE PROCEDURE write_integer_rank2
    MODULE PROCEDURE write_integer_rank1
    MODULE PROCEDURE write_integer_rank0
    MODULE PROCEDURE write_all
    MODULE PROCEDURE write_all_nosuffix
  END INTERFACE

  INTERFACE readdata
    MODULE PROCEDURE read_real_rank4
    MODULE PROCEDURE read_real_rank3
    MODULE PROCEDURE read_real_rank2
    MODULE PROCEDURE read_real_rank1
    MODULE PROCEDURE read_real_rank0
    MODULE PROCEDURE read_integer_rank4
    MODULE PROCEDURE read_integer_rank3
    MODULE PROCEDURE read_integer_rank2
    MODULE PROCEDURE read_integer_rank1
    MODULE PROCEDURE read_integer_rank0
    MODULE PROCEDURE read_all
    MODULE PROCEDURE read_all_nosuffix
    MODULE PROCEDURE read_table
  END INTERFACE

  INTERFACE writemessage
    MODULE PROCEDURE writemessage_message
    MODULE PROCEDURE writemessage_nomessage
  END INTERFACE

  ! Strings needed to construct the C-style strings to be
  ! passed to the routines in zeus_c.c
  CHARACTER(LEN=*), PARAMETER :: outend = achar(0)
  CHARACTER(LEN=*), PARAMETER :: inend = achar(0)
  ! Class flags
  INTEGER, PARAMETER :: realclass = 1
  INTEGER, PARAMETER :: integerclass = 2
  ! External statements for C functions
  ! (no full interface, there could be rank inconsistencies if tried)
  EXTERNAL binaryin, binaryout

  PUBLIC :: check_timestep, &
            writemessage, threeoutput, warnoutput, finalstop, &
            sanity, settimestep
  PUBLIC :: writedata, readdata
  PUBLIC :: lowercase, uppercase

! Guard against improper usage of certain dangerous macros defined in this file:
#ifdef RANK_ZERO
#error  The RANK _ ZERO macro must not be defined at this stage: compilation aborts here.
#endif /* RANK_ZERO */

#ifdef Z_FILENAME
#error  The Z _ FILENAME macro must not be defined at this stage: compilation aborts here.
#endif /* Z_FILENAME */

! Define a slightly ugly convenience macro.  Might be replaced by a function in some future.
#define Z_FILENAME trim(prefix)//trim(fieldname)//trim(suffix)

CONTAINS
  !** GROUP: IO **********************************************************
  !**                                                                   **
  !**                   G R O U P :   I O                               **
  !**                                                                   **
  !***********************************************************************
  !
  !=======================================================================
  !
  LOGICAL FUNCTION check_timestep(frequency)
    INTEGER, INTENT(IN) :: frequency
    INTEGER, PARAMETER :: check_the_first_few = 4
    if (debug > 0) then
      check_timestep = .true.
      return
    end if
    if (frequency < 0) then
      check_timestep = .false.
    else
      if (ntimestep <= check_the_first_few) then
        check_timestep = .true.
      else
        if (frequency == 0) then
          check_timestep = .false.
        else
          if (frequency == 1) then
            check_timestep = .true.
          else
            check_timestep = (mod(ntimestep, frequency) == 0)
          end if
        end if
      end if
    end if
  END FUNCTION check_timestep
  !
  !=======================================================================
  !
  SUBROUTINE writemessage_message(filename, messageline, debuglevel)
    ! Sends a message to a specified file.
    CHARACTER(LEN=*), INTENT(IN) :: filename
    CHARACTER(LEN=*), INTENT(IN) :: messageline
    INTEGER, INTENT(IN), OPTIONAL :: debuglevel
    !
    INTEGER, PARAMETER :: lunit = io_output

    if (present(debuglevel)) then
      if (debug < debuglevel) return
    end if

    open (unit=lunit, file=filename//trim(nodename), &
          status='unknown', &
          form='formatted', &
          access='sequential', &
          position='append', &
          action='write')

    write (lunit, '(a)') trim(messageline)

    close (lunit)
  END SUBROUTINE writemessage_message
  !
  !=======================================================================
  !
  SUBROUTINE writemessage_nomessage(filename, debuglevel)
    ! Sends a message to a specified file.
    CHARACTER(LEN=*), INTENT(IN) :: filename
    INTEGER, INTENT(IN), OPTIONAL :: debuglevel
    call writemessage_message(filename, trim(ntimestepname), debuglevel)
  END SUBROUTINE writemessage_nomessage
  !
  !=======================================================================
  !
  SUBROUTINE warnoutput(filename, messageline, warn)
    ! Increase warning level, send a message to fouroutput
    CHARACTER(LEN=*), INTENT(IN) :: filename
    CHARACTER(LEN=*), INTENT(IN) :: messageline
    INTEGER, INTENT(INOUT), OPTIONAL :: warn

    if (present(warn)) then
      warn = warn + 1
    else
      nwarn = nwarn + 1
    end if
    if (iowarn /= NO_OUTPUT) write (iowarn, '(a)') trim(messageline)
    call threeoutput(filename, messageline)

  END SUBROUTINE warnoutput
  !
  !=======================================================================
  !
  SUBROUTINE threeoutput(filename, messageline, debuglevel)
    ! Sends one given message line three times: first to a specified file,
    ! and then to the log and tty output streams.
    CHARACTER(LEN=*), INTENT(IN) :: filename
    CHARACTER(LEN=*), INTENT(IN) :: messageline
    INTEGER, INTENT(IN), OPTIONAL :: debuglevel

    if (present(debuglevel)) then
      if (debug < debuglevel) return
    end if

    call writemessage(filename, messageline)
    if (iolog /= NO_OUTPUT) write (iolog, '(a)') trim(messageline)
    if (iotty /= NO_OUTPUT) write (iotty, '(a)') trim(messageline)
  END SUBROUTINE threeoutput
  !
  !=======================================================================
  !
  SUBROUTINE lowercase(string)
    CHARACTER(LEN=*), INTENT(INOUT) :: string
    INTEGER i, n, char_i
    CHARACTER(LEN=1) char
    n = len(string)

    string_loop: do i = 1, n
      char_i = iachar(string(i:i)) ! ASCII encoding of the character
      select case (char_i)
      case (:-1, 128:)                ! Out of standard ASCII range
        char = ' '
      case (65:90)                    ! Upper case. make it lower case
        char = achar(char_i + 32)
      case default                    ! Do nothing, look up next character
        cycle string_loop
      end select
      string(i:i) = char  ! This line is not accessible from the default case
    end do string_loop
  END SUBROUTINE lowercase
  !
  !=======================================================================
  !
  SUBROUTINE uppercase(string)
    CHARACTER(LEN=*), INTENT(INOUT) :: string
    INTEGER i, n, char_i
    CHARACTER(LEN=1) char
    n = len(string)

    string_loop: do i = 1, n
      char_i = iachar(string(i:i)) ! ASCII encoding of the character
      select case (char_i)
      case (:-1, 128:)                ! Out of standard ASCII range
        char = ' '
      case (65 + 32:90 + 32)              ! Lower case. make it upper case
        char = achar(char_i - 32)
      case default                    ! Do nothing, look up next character
        cycle string_loop
      end select
      string(i:i) = char  ! This line is not accessible from the default case
    end do string_loop
  END SUBROUTINE uppercase
  !
  !=======================================================================
  !
  SUBROUTINE finalstop(filename, messageline)
#ifdef MPI
    USE mpi
#endif /* MPI */
    CHARACTER(LEN=*), INTENT(IN) :: filename
    CHARACTER(LEN=*), INTENT(IN) :: messageline
    call warnoutput(filename, messageline)
#ifdef MPI
    ! A broadcast telling everyone to finish could be desirable
    ! in this place.
    call MPI_FINALIZE(node)
#endif /* MPI */
    !
    ! Close text output files
    !
    if (iolog /= NO_OUTPUT) then
      close (unit=iolog)
    end if
    stop
  END SUBROUTINE finalstop
  !
  !=======================================================================
  !
  SUBROUTINE sanity(isanity)
    INTEGER, INTENT(OUT) :: isanity
    CHARACTER(LEN=*), PARAMETER :: routinename = 'z_sanity'
    CHARACTER(LEN=messagelinelength) message
    !
    !     Binary sanity check.  This is a bare minimum.
    !     A more complete sanity check would also test
    !     physical constraints such as positivity of density
    !     and internal energy.  Problem-dependent sanity check
    !     conditions are also a conceivable addition.
    !
    isanity = 0
    isanity = isanity + check(v1, 'v1')
    if (withv2) isanity = isanity + check(v2, 'v2')
    if (withv3) isanity = isanity + check(v3, 'v3')
    if (imhd > 0) then
      if (withbp) then
        isanity = isanity + check(b1, 'b1')
        isanity = isanity + check(b2, 'b2')
      end if
      if (withb3) then
        isanity = isanity + check(b3, 'b3')
      end if
      if (withchem) then
        isanity = isanity + check(ad_eta_array, 'eta_ad')
        isanity = isanity + check(ohmic_eta_array, 'eta_ohm')
        isanity = isanity + check(hall_eta_array, 'eta_hal')
        isanity = isanity + check(xr_array, 'xr')
        isanity = isanity + check(cr_array, 'cr')
      end if
    endif
    isanity = isanity + check(d_, 'd_')
    if (ienergy > 0) then
      isanity = isanity + check(e_, 'e_')
    endif
    if (withgp) then
      isanity = isanity + check(gp, 'gp')
    endif
    if (isanity > 0) then
      write (message, '(a,i2,a)') &
        'SANITY: Infinities or NaNs present in ', &
        isanity, ' of the fields.'
      call threeoutput(routinename, message)
      debug = max(debug, 2)
      call threeoutput(routinename, 'SANITY: Debug level set to at least 2.')
      call writedata('z_sanityfailed_', debug)
      call threeoutput(routinename, 'SANITY: Data written to file.')
    endif
    !
  END SUBROUTINE sanity
  !
  !=======================================================================
  !
  INTEGER FUNCTION check(field, fieldname)
    CHARACTER(LEN=*), PARAMETER :: routinename = 'z_check'
    REALNUM, INTENT(IN) :: field(:, :, :)
    CHARACTER(LEN=*), INTENT(IN) :: fieldname
    INTEGER ierr
    CHARACTER(LEN=messagelinelength) message
#ifdef FPCLASS
    INTEGER i, j, k
#include <for_fpclass.h>

    !Do it unportably using Intel Fortran extensions:
    ierr = 0
    loop: do k = lbound(field, 3), ubound(field, 3)
      do j = lbound(field, 2), ubound(field, 2)
        do i = lbound(field, 1), ubound(field, 1)
          if (fp_class(field(i, j, k)) < FOR_K_FP_POS_NORM) then
            ierr = 1
            exit loop
          end if
        end do
      end do
    end do loop
    if (ierr == 1) then
      write (message, '(a,i3,a,a,a,i5,a,i5,a,i5)') &
        'Weird fp_class: ', &
        fp_class(field(i, j, k)), &
        ' Field ', fieldname, &
        ' at i=', i, ' j=', j, ' k=', k
      call threeoutput(routinename, message)
    end if
    check = ierr
#else /* FPCLASS */
!  For now, omit the check.  Code for portable checking
!  may be written later.

    !call checkieee( &
    !     size(field,1), &
    !     size(field,2), &
    !     size(field,3), &
    !     field,ierr,fieldname)

    check = 0
#endif /* FPCLASS */
  END FUNCTION check
  !
  !=======================================================================
  !
  SUBROUTINE settimestep(next_ntimestep)
    INTEGER, INTENT(IN) :: next_ntimestep
    !
    INTEGER, PARAMETER :: ntimestepnamelength = LEN(ntimestepname)

    CHARACTER(LEN=4 + 2*formatlength) ntimestepformat

    write (ntimestepformat, format_format) &
      '(i', ntimestepnamelength, '.', ntimestepnamelength, ')'

    ntimestep = next_ntimestep
    write (ntimestepname, ntimestepformat) ntimestep
  END SUBROUTINE settimestep

  !** SUBGROUP: GENERIC I/O ROUTINES *************************************
  !*                                                                     *
  !*  S U B G R O U P :      G E N E R I C   I / O   R O U T I N E S     *
  !*                                                                     *
  !***********************************************************************
  !
  !=======================================================================
  !
  SUBROUTINE write_real_rank4(field, prefix, debuglevel, singlefile, &
                              reportfailure)
    REALNUM, INTENT(IN) :: field(:, :, :, :)
    INTEGER, PARAMETER :: class = realclass
#include "zeus_io_writeranks_include.f90"
  END SUBROUTINE write_real_rank4
  SUBROUTINE write_real_rank3(field, prefix, debuglevel, singlefile, &
                              reportfailure)
    REALNUM, INTENT(IN) :: field(:, :, :)
    INTEGER, PARAMETER :: class = realclass
#include "zeus_io_writeranks_include.f90"
  END SUBROUTINE write_real_rank3
  SUBROUTINE write_real_rank2(field, prefix, debuglevel, singlefile, &
                              reportfailure)
    REALNUM, INTENT(IN) :: field(:, :)
    INTEGER, PARAMETER :: class = realclass
#include "zeus_io_writeranks_include.f90"
  END SUBROUTINE write_real_rank2
  SUBROUTINE write_real_rank1(field, prefix, debuglevel, singlefile, &
                              reportfailure)
    REALNUM, INTENT(IN) :: field(:)
    INTEGER, PARAMETER :: class = realclass
#include "zeus_io_writeranks_include.f90"
  END SUBROUTINE write_real_rank1
  SUBROUTINE write_real_rank0(field, prefix, debuglevel, singlefile, &
                              reportfailure)
    REALNUM, INTENT(IN) :: field
    INTEGER, PARAMETER :: class = realclass
#define RANK_ZERO
#include "zeus_io_writeranks_include.f90"
#undef RANK_ZERO
  END SUBROUTINE write_real_rank0
  !
  SUBROUTINE write_integer_rank4(field, prefix, debuglevel, singlefile, &
                                 reportfailure)
    INTEGER, INTENT(IN) :: field(:, :, :, :)
    INTEGER, PARAMETER :: class = integerclass
#include "zeus_io_writeranks_include.f90"
  END SUBROUTINE write_integer_rank4
  SUBROUTINE write_integer_rank3(field, prefix, debuglevel, singlefile, &
                                 reportfailure)
    INTEGER, INTENT(IN) :: field(:, :, :)
    INTEGER, PARAMETER :: class = integerclass
#include "zeus_io_writeranks_include.f90"
  END SUBROUTINE write_integer_rank3
  SUBROUTINE write_integer_rank2(field, prefix, debuglevel, singlefile, &
                                 reportfailure)
    INTEGER, INTENT(IN) :: field(:, :)
    INTEGER, PARAMETER :: class = integerclass
#include "zeus_io_writeranks_include.f90"
  END SUBROUTINE write_integer_rank2
  SUBROUTINE write_integer_rank1(field, prefix, debuglevel, singlefile, &
                                 reportfailure)
    INTEGER, INTENT(IN) :: field(:)
    INTEGER, PARAMETER :: class = integerclass
#include "zeus_io_writeranks_include.f90"
  END SUBROUTINE write_integer_rank1
  SUBROUTINE write_integer_rank0(field, prefix, debuglevel, singlefile, &
                                 reportfailure)
    INTEGER, INTENT(IN) :: field
    INTEGER, PARAMETER :: class = integerclass
#define RANK_ZERO
#include "zeus_io_writeranks_include.f90"
#undef RANK_ZERO
  END SUBROUTINE write_integer_rank0
  !
  !=======================================================================
  !
  SUBROUTINE read_real_rank4(field, prefix, singlefile, reportfailure)
    REALNUM, INTENT(IN) :: field(:, :, :, :)
    INTEGER, PARAMETER :: class = realclass
#include "zeus_io_readranks_include.f90"
  END SUBROUTINE read_real_rank4
  SUBROUTINE read_real_rank3(field, prefix, singlefile, reportfailure)
    REALNUM, INTENT(IN) :: field(:, :, :)
    INTEGER, PARAMETER :: class = realclass
#include "zeus_io_readranks_include.f90"
  END SUBROUTINE read_real_rank3
  SUBROUTINE read_real_rank2(field, prefix, singlefile, reportfailure)
    REALNUM, INTENT(IN) :: field(:, :)
    INTEGER, PARAMETER :: class = realclass
#include "zeus_io_readranks_include.f90"
  END SUBROUTINE read_real_rank2
  SUBROUTINE read_real_rank1(field, prefix, singlefile, reportfailure)
    REALNUM, INTENT(IN) :: field(:)
    INTEGER, PARAMETER :: class = realclass
#include "zeus_io_readranks_include.f90"
  END SUBROUTINE read_real_rank1
  SUBROUTINE read_real_rank0(field, prefix, singlefile, reportfailure)
    REALNUM, INTENT(IN) :: field
    INTEGER, PARAMETER :: class = realclass
#define RANK_ZERO
#include "zeus_io_readranks_include.f90"
#undef RANK_ZERO
  END SUBROUTINE read_real_rank0
  !
  SUBROUTINE read_integer_rank4(field, prefix, singlefile, reportfailure)
    INTEGER, INTENT(IN) :: field(:, :, :, :)
    INTEGER, PARAMETER :: class = integerclass
#include "zeus_io_readranks_include.f90"
  END SUBROUTINE read_integer_rank4
  SUBROUTINE read_integer_rank3(field, prefix, singlefile, reportfailure)
    INTEGER, INTENT(IN) :: field(:, :, :)
    INTEGER, PARAMETER :: class = integerclass
#include "zeus_io_readranks_include.f90"
  END SUBROUTINE read_integer_rank3
  SUBROUTINE read_integer_rank2(field, prefix, singlefile, reportfailure)
    INTEGER, INTENT(IN) :: field(:, :)
    INTEGER, PARAMETER :: class = integerclass
#include "zeus_io_readranks_include.f90"
  END SUBROUTINE read_integer_rank2
  SUBROUTINE read_integer_rank1(field, prefix, singlefile, reportfailure)
    INTEGER, INTENT(IN) :: field(:)
    INTEGER, PARAMETER :: class = integerclass
#include "zeus_io_readranks_include.f90"
  END SUBROUTINE read_integer_rank1
  SUBROUTINE read_integer_rank0(field, prefix, singlefile, reportfailure)
    INTEGER, INTENT(IN) :: field
    INTEGER, PARAMETER :: class = integerclass
#define RANK_ZERO
#include "zeus_io_readranks_include.f90"
#undef RANK_ZERO
  END SUBROUTINE read_integer_rank0
  !
  !=======================================================================
  !
  SUBROUTINE write_all_nosuffix(prefix, debuglevel)
    CHARACTER(LEN=*), INTENT(IN) :: prefix
    INTEGER, INTENT(IN), OPTIONAL :: debuglevel
    call write_all(prefix, '', debuglevel)
  END SUBROUTINE write_all_nosuffix
  !
  !=======================================================================
  !
  SUBROUTINE write_all(prefix, suffix, debuglevel)
    CHARACTER(LEN=*), INTENT(IN) :: prefix
    CHARACTER(LEN=*), INTENT(IN) :: suffix
    INTEGER, INTENT(IN), OPTIONAL :: debuglevel

    CHARACTER(LEN=20) fieldname
    INTEGER buffi(10)
    REALNUM buffr(6)
    LOGICAL, SAVE :: firststep = .true.

    REALNUM :: mt

    fieldname = 'v1'
    call write_real_rank3(v1, Z_FILENAME, debuglevel)
    if (withv2) then
      fieldname = 'v2'
      call write_real_rank3(v2, Z_FILENAME, debuglevel)
    end if
    if (withv3) then
      fieldname = 'v3'
      call write_real_rank3(v3, Z_FILENAME, debuglevel)
    end if
    fieldname = 'd_'
    call write_real_rank3(d_, Z_FILENAME, debuglevel)
    if (ienergy > 0) then
      fieldname = 'e_'
      call write_real_rank3(e_, Z_FILENAME, debuglevel)
    end if
    if (withgp) then
      if (gravity_algorithm /= 'fixed' &
          .or. (gravity_algorithm == 'fixed' .and. firststep)) then
        fieldname = 'gp'
        call write_real_rank3(gp, Z_FILENAME, debuglevel)
        firststep = .false.
      end if
      if (withextendedgrid) then
        fieldname = 'gp_e'
        call write_real_rank3(gp_e, Z_FILENAME, debuglevel)
      end if
    end if
    if (imhd > 0) then
      if (withbp) then
        fieldname = 'b1'
        call write_real_rank3(b1, Z_FILENAME, debuglevel)
        fieldname = 'b2'
        call write_real_rank3(b2, Z_FILENAME, debuglevel)
      end if
      if (withb3) then
        fieldname = 'b3'
        call write_real_rank3(b3, Z_FILENAME, debuglevel)
      end if
      if (withchem) then
        fieldname = 'eta_ad'
        call write_real_rank3(ad_eta_array, Z_FILENAME, debuglevel)
        fieldname = 'eta_ohm'
        call write_real_rank3(ohmic_eta_array, Z_FILENAME, debuglevel)
        fieldname = 'eta_hal'
        call write_real_rank3(hall_eta_array, Z_FILENAME, debuglevel)
        fieldname = 'xr'
        call write_real_rank3(xr_array, Z_FILENAME, debuglevel)
        fieldname = 'cr'
        call write_real_rank3(cr_array, Z_FILENAME, debuglevel)
      end if
    end if
    !
    if (withcentralmass) then
      fieldname = 'cm'
      call write_real_rank0(centralmass, Z_FILENAME, debuglevel, &
                            SINGLEFILE=.true.)
    end if
    fieldname = 'mtotal'
    call total_mass(mt)
    call write_real_rank0(mt, Z_FILENAME, debuglevel)
    if (withboundarymass) then
      fieldname = 'mx1i_flux'
      call write_real_rank0(mass_x1_inner, Z_FILENAME, debuglevel)
      fieldname = 'mx1i_loss'
      call write_real_rank0(mass_x1_inner_loss, Z_FILENAME, debuglevel)
      fieldname = 'mx1i_gain'
      call write_real_rank0(mass_x1_inner_gain, Z_FILENAME, debuglevel)
      fieldname = 'mx1o_flux'
      call write_real_rank0(mass_x1_outer, Z_FILENAME, debuglevel)
      fieldname = 'mx1o_loss'
      call write_real_rank0(mass_x1_outer_loss, Z_FILENAME, debuglevel)
      fieldname = 'mx1o_gain'
      call write_real_rank0(mass_x1_outer_gain, Z_FILENAME, debuglevel)
    end if
    if (withfloormass) then
      if (alfven_dt_floor > zro) then
        fieldname = 'mfloor_alfven'
        call write_real_rank0(mass_gain_alfven, Z_FILENAME, debuglevel, &
                              SINGLEFILE=.true.)
      end if
      if (withdensityfloor) then
        fieldname = 'mfloor_rho'
        call write_real_rank0(mass_gain_rhofloor, Z_FILENAME, debuglevel, &
                              SINGLEFILE=.true.)
      end if
    end if
    if (number_of_tracer_fields > 0) then
      fieldname = 'tf'
      call write_real_rank4(tracerfields, Z_FILENAME, debuglevel)
    end if
    !
    if (moving_grid1) then
      fieldname = 'x1a'
      call write_real_rank1(x1a, Z_FILENAME, debuglevel)
      fieldname = 'x1b'
      call write_real_rank1(x1b, Z_FILENAME, debuglevel)
    end if
    if (moving_grid2) then
      fieldname = 'x2a'
      call write_real_rank1(x2a, Z_FILENAME, debuglevel)
      fieldname = 'x2b'
      call write_real_rank1(x2b, Z_FILENAME, debuglevel)
    end if
    if (moving_grid3) then
      fieldname = 'x3a'
      call write_real_rank1(x3a, Z_FILENAME, debuglevel)
      fieldname = 'x3b'
      call write_real_rank1(x3b, Z_FILENAME, debuglevel)
    end if
    !
    buffr(1) = time
    buffr(2) = dt
    buffr(3) = dtmin
    buffr(4) = dtohm
    buffr(5) = tdump
    fieldname = 'tr'
    call write_real_rank1(buffr, Z_FILENAME, debuglevel, SINGLEFILE=.true.)

    buffi(1) = ntimestep
    buffi(2) = ix1x2x3
    buffi(3) = nwarn
    buffi(4) = hotzonewarn
    buffi(5) = nsubcycles_ohm
    buffi(8) = ndump
    buffi(9) = dump_switch
    fieldname = 'ti'
    call write_integer_rank1(buffi, Z_FILENAME, debuglevel, SINGLEFILE=.true.)

  END SUBROUTINE write_all
  !
  !=======================================================================
  !
  SUBROUTINE read_all_nosuffix(prefix)
    CHARACTER(LEN=*), INTENT(IN) :: prefix
    call read_all(prefix, '')
  END SUBROUTINE read_all_nosuffix
  !
  SUBROUTINE read_table(filename, tab)
    INTEGER, INTENT(IN) :: tab
    CHARACTER(LEN=*), INTENT(IN) :: filename
    CHARACTER(LEN=*), PARAMETER :: routinename = 'z_read_table'
    INTEGER :: report

    if (tab > 0) then
      report = -1
      call read_real_rank2(allRec, filename, SINGLEFILE=.true., &
                           REPORTFAILURE=report)
      if (report > 0) then
        call warnoutput(routinename, 'chemistry table not read: use implicit' &
                        //' chemistry routine')
        tabulate = 0
      end if
    end if
  END SUBROUTINE read_table
  !
  !=======================================================================
  !
  SUBROUTINE read_all(prefix, suffix)  ! Not ready for moving grids yet
#ifdef MPI
    USE mpi
    INTEGER ierr
#endif /* MPI */
    CHARACTER(LEN=*), PARAMETER :: routinename = 'z_read_all'
    CHARACTER(LEN=*), INTENT(IN) :: prefix
    CHARACTER(LEN=*), INTENT(IN) :: suffix
    CHARACTER(LEN=20) fieldname
    INTEGER buffi(10)
    REALNUM buffr(6)
    INTEGER :: report

    ! Consider adding code to cover missing fields in the data being read
    fieldname = 'v1'
    call read_real_rank3(v1, Z_FILENAME)
    if (withv2) then
      fieldname = 'v2'
      call read_real_rank3(v2, Z_FILENAME)
    end if
    if (withv3) then
      fieldname = 'v3'
      call read_real_rank3(v3, Z_FILENAME)
    end if
    fieldname = 'd_'
    call read_real_rank3(d_, Z_FILENAME)
    if (ienergy > 0) then
      fieldname = 'e_'
      call read_real_rank3(e_, Z_FILENAME)
    end if
    if (withgp) then
      fieldname = 'gp'
      report = -1
      call read_real_rank3(gp, Z_FILENAME, REPORTFAILURE=report)
      if (report > 0) then
        call warnoutput(routinename, &
                        'gp field not read: gp set to one.')
        gp = one
      end if
      if (withextendedgrid) then
        report = -1
        fieldname = 'gp_e'
        call read_real_rank3(gp_e, Z_FILENAME, REPORTFAILURE=report)
        if (report > 0) then
          call warnoutput(routinename, &
                          'gp_e field not read: gp_e set to one.')
          gp_e = one
        end if
      end if
    end if
    if (imhd > 0) then
      if (withbp) then
        fieldname = 'b1'
        call read_real_rank3(b1, Z_FILENAME)
        fieldname = 'b2'
        call read_real_rank3(b2, Z_FILENAME)
      end if
      if (withb3) then
        fieldname = 'b3'
        call read_real_rank3(b3, Z_FILENAME)
      end if
    end if
    if (number_of_tracer_fields > 0) then
      fieldname = 'tf'
      call read_real_rank4(tracerfields, Z_FILENAME)
    end if
    !
    if (withcentralmass) then
      if (node == rootnode) then
        fieldname = 'cm'
        report = -1
        call read_real_rank0(centralmass, Z_FILENAME, REPORTFAILURE=report)
        if (report > 0) then
          report = -1
          call read_real_rank0(centralmass, Z_FILENAME, SINGLEFILE=.true., &
                               REPORTFAILURE=report)
        end if
        if (report > 0) then
          call warnoutput(routinename, &
                          'cm field not read: centralmass set to zero.')
          centralmass = zro
        end if
      end if
#ifdef MPI
      if (node == rootnode) buffr(1) = centralmass
      call MPI_BCAST(buffr, 1, MPI_FLOATMPI, rootnode, MPI_COMM_WORLD, ierr)
      centralmass = buffr(1)
#endif /* MPI */
    end if

    if (withboundarymass) then
      fieldname = 'mx1i_flux'
      call read_real_rank0(mass_x1_inner, Z_FILENAME, REPORTFAILURE=report)
      if (report > 0) then
        call warnoutput(routinename, &
                        trim(fieldname)//' field not read: set to zero.')
        mass_x1_inner = zro
      end if
      fieldname = 'mx1i_loss'
      call read_real_rank0(mass_x1_inner_loss, Z_FILENAME, REPORTFAILURE=report)
      if (report > 0) then
        call warnoutput(routinename, &
                        trim(fieldname)//' field not read: set to zero.')
        mass_x1_inner_loss = zro
      end if
      fieldname = 'mx1i_gain'
      call read_real_rank0(mass_x1_inner_gain, Z_FILENAME, REPORTFAILURE=report)
      if (report > 0) then
        call warnoutput(routinename, &
                        trim(fieldname)//' field not read: set to zero.')
        mass_x1_inner_gain = zro
      end if
      fieldname = 'mx1o_flux'
      call read_real_rank0(mass_x1_outer, Z_FILENAME, REPORTFAILURE=report)
      if (report > 0) then
        call warnoutput(routinename, &
                        trim(fieldname)//' field not read: set to zero.')
        mass_x1_outer = zro
      end if
      fieldname = 'mx1o_loss'
      call read_real_rank0(mass_x1_outer_loss, Z_FILENAME, REPORTFAILURE=report)
      if (report > 0) then
        call warnoutput(routinename, &
                        trim(fieldname)//' field not read: set to zero.')
        mass_x1_outer_loss = zro
      end if
      fieldname = 'mx1o_gain'
      call read_real_rank0(mass_x1_outer_gain, Z_FILENAME, REPORTFAILURE=report)
      if (report > 0) then
        call warnoutput(routinename, &
                        trim(fieldname)//' field not read: set to zero.')
        mass_x1_outer_gain = zro
      end if
    end if
    if (withfloormass) then
      if (alfven_dt_floor > zro) then
        fieldname = 'mfloor_alfven'
        report = -1
        call read_real_rank0(mass_gain_alfven, Z_FILENAME, REPORTFAILURE=report)
        if (report > 0 .and. node == rootnode) then
          ! If it is the root processor, try reading again
          report = -1
          call read_real_rank0(mass_gain_alfven, Z_FILENAME, &
                               SINGLEFILE=.true., REPORTFAILURE=report)
        end if
        if (report > 0) then
          call warnoutput(routinename, 'mfloor_alfven field not read: ' &
                          //'mass_gain_alfven set to zero.')
          mass_gain_alfven = zro
        end if
      end if
      if (withdensityfloor) then
        fieldname = 'mfloor_rho'
        report = -1
        call read_real_rank0(mass_gain_rhofloor, Z_FILENAME, &
                             REPORTFAILURE=report)
        if (report > 0 .and. node == rootnode) then
          ! If it is the root processor, try reading again
          report = -1
          call read_real_rank0(mass_gain_rhofloor, Z_FILENAME, &
                               SINGLEFILE=.true., REPORTFAILURE=report)
        end if
        if (report > 0) then
          call warnoutput(routinename, 'mfloor_rho field not read: ' &
                          //'mass_gain_rhofloor set to zero.')
          mass_gain_rhofloor = zro
        end if
      end if
    end if
    !
    fieldname = 'tr'
    call read_real_rank1(buffr, Z_FILENAME, SINGLEFILE=.true.)
    time = buffr(1)
    dt = buffr(2)
    dtmin = buffr(3)
    dtohm = buffr(4)
    tdump = buffr(5)

    fieldname = 'ti'
    call read_integer_rank1(buffi, Z_FILENAME, SINGLEFILE=.true.)
    call settimestep(buffi(1))
    ix1x2x3 = buffi(2)
    nwarn = buffi(3)
    hotzonewarn = buffi(4)
    nsubcycles_ohm = buffi(5)
    ndump = buffi(8)
    dump_switch = buffi(9)
  END SUBROUTINE read_all

  SUBROUTINE total_mass(mt)
    REALNUM, INTENT(out) :: mt
    INTEGER :: i, j, k
    mt = zro
    do k = ks, ke
      do j = js, je
        do i = is, ie
          mt = mt + d_(i, j, k)*dvl1a(i)*dvl2a(j)*dvl3a(k)
        end do
      end do
    end do
  END SUBROUTINE total_mass

#undef Z_FILENAME

END MODULE zeus_io
