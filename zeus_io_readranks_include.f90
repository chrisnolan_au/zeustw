!  SUBROUTINE read_real_rank3(field,prefix,singlefile,reportfailure)
!    REALNUM, INTENT(IN) :: field(:,:,:)
!    INTEGER, PARAMETER :: class=realclass
!#include "zeus_io_readranks_include.f90"
    CHARACTER(LEN=*), INTENT(IN) :: prefix
    LOGICAL, INTENT(IN), OPTIONAL :: singlefile

    ! On input: zero (or absent) if a crash is desired on failure;
    !           a strictly negative value if a report is desired.
    ! On output: zero in case of success; a strictly positive error code in case of failure
    INTEGER, INTENT(INOUT), OPTIONAL :: reportfailure

    LOGICAL :: l_singlefile
    INTEGER :: l_reportfailure
    CHARACTER(LEN=4*filenamelength) filename  ! Consider dynamic allocation

    if (present(singlefile)) then
      l_singlefile = singlefile
    else
      l_singlefile = .false.
    end if

    if (l_singlefile) then
      filename = trim(prefix)
    else
      filename = trim(prefix)//nodename
    end if

    if (present(reportfailure)) then
      l_reportfailure = reportfailure
    else
      l_reportfailure = 0
    end if

#ifdef RANK_ZERO
#define FIELD_SIZE_ 1
#else /* RANK_ZERO */
#define FIELD_SIZE_ size(field)
#endif /* RANK_ZERO */

    call binaryin(FIELD_SIZE_, kind(field), class, l_reportfailure, field, &
                  trim(filename)//outend)

    if (present(reportfailure)) then
      reportfailure = l_reportfailure
    end if

#undef FIELD_SIZE_

!  END SUBROUTINE read_real_rank3
