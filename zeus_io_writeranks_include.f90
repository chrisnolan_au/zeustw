!  SUBROUTINE write_real_rank3(field,prefix,debuglevel,singlefile,reportfailure)
!    REALNUM, INTENT(IN) :: field(:,:,:)
!    INTEGER, PARAMETER :: class=realclass
! #include "zeus_io_writeranks_include.f90"
    CHARACTER(LEN=*), INTENT(IN) :: prefix
    INTEGER, INTENT(IN), OPTIONAL :: debuglevel
    LOGICAL, INTENT(IN), OPTIONAL :: singlefile

    ! On input: zero (or absent) if a crash is desired on failure;
    !           non-zero if a report is desired
    ! On output: zero in case of success; a non-zero error code in case of failure
    INTEGER, INTENT(INOUT), OPTIONAL :: reportfailure

    LOGICAL :: l_singlefile
    INTEGER :: l_reportfailure
    CHARACTER(LEN=4*filenamelength) filename  ! Consider dynamic allocation

    if (present(debuglevel)) then
      if (debug < debuglevel) return
    end if
    if (present(singlefile)) then
      l_singlefile = singlefile
    else
      l_singlefile = .false.
    end if
    if (l_singlefile .and. node /= rootnode) then
      return
    end if

    if (present(debuglevel)) then
      if (l_singlefile) then
        filename = trim(prefix)//'_'//ntimestepname
      else
        filename = trim(prefix)//'_'//ntimestepname//nodename
      end if
    else
      if (l_singlefile) then
        filename = trim(prefix)
      else
        filename = trim(prefix)//nodename
      end if
    end if

    if (present(reportfailure)) then
      l_reportfailure = reportfailure
    else
      l_reportfailure = 0
    end if

#ifdef RANK_ZERO
#define FIELD_SIZE_ 1
#else /* RANK_ZERO */
#define FIELD_SIZE_ size(field)
#endif /* RANK_ZERO */

    call binaryout(FIELD_SIZE_,kind(field),class,l_reportfailure,field,trim(filename)//outend)

    if (present(reportfailure)) then
      reportfailure = l_reportfailure
    end if

#undef FIELD_SIZE_

!  END SUBROUTINE write_real_rank3
