#include "macros"
MODULE zeus_iochecks
  USE zeus_variables
  USE zeus_io
  USE zeus_deadcode
  USE zeus_timer
  USE zeus_physics
  IMPLICIT NONE
  PRIVATE
  PUBLIC :: dataoutput

  INTEGER, SAVE :: clockdump_switch

CONTAINS
  !** GROUP: I/O CHECKS **************************************************
  !**                                                                   **
  !**                   G R O U P :   I / O   C H E C K S               **
  !**                                                                   **
  !***********************************************************************
  !
  !** SUBGROUP: WRAPPERS *************************************************
  !*                                                                     *
  !*                 S U B G R O U P : W R A P P E R S                   *
  !*                                                                     *
  !***********************************************************************
  !
  !=======================================================================
  !
  SUBROUTINE dataoutput
    call dataio
  END SUBROUTINE dataoutput

  !** SUBGROUP: OUTER INTERFACE ******************************************
  !*                                                                     *
  !*          S U B G R O U P : O U T E R   I N T E R F A C E            *
  !*                                                                     *
  !***********************************************************************
  !
  !=======================================================================
  !
  SUBROUTINE dataio
    CHARACTER(LEN=*), PARAMETER :: routinename = 'z_dataio'
    !
    !    jms:zeus3d.dataio <--------------------------- controls data output
    !                                                             june, 1988
    !
    !    written by: Jim Stone
    !    modified 1: March, 1989 by Jim Stone; user dumps added
    !    modified 2: March, 1989 by Jim Stone; HDF  dumps added
    !    modified 3: May, 1989 by Jim Stone; timeslice (history) dumps added
    !    modified 4: January, 1990 by David Clarke; incorporated into ZEUS3D
    !    modified 5: November, 1990 by David Clarke; added DISPLAY dumps
    !    modified 6: March, 1991 by David Clarke; added RADIO dumps
    !
    !  PURPOSE:  Controls all output of data from the program.  Current
    !  output modes are only:
    !
    !      * user dumps for user defined or controlled i/o
    !
    !  Timeslice dumps could be reinstated with little effort;
    !  it is planned to reinstate restart dumps in the near future
    !  (RFK, 2004/01/21)
    !
    !-----------------------------------------------------------------------
    !
    !      Check for interrupt messages, execution limits, expected output,
    !      sanity of the fields, etcetera.
    !
    call iocheck
    call writemessage(routinename//'_iocheck', 3)
    !
    !----------------------------  DATA DUMPS  -----------------------------
    !
    !  Make data dump.  After doing that, increment the dump index
    !  and also reset the dump switch.
    !
    if (dump_switch == 1) then
      call dumpdata(ndump, outputprefix, 'z_outputrecord')
      dump_switch = 0
      ndump = ndump + 1
    end if
    !
    !---------------------------  CLOCK DUMPS  -----------------------------
    !
    !  Make clock dump.  After doing that, increment the dump index
    !  and also reset the dump switch.
    !
    if (clockdump_switch == 1) then
      call dumpdata(clockdump_n, clockdump_prefix, 'z_clockrecord')
      clockdump_switch = 0
      select case (clockdump_policy)
      case (2)
        clockdump_n = clockdump_n + 1
      case (1)
        clockdump_n = clockdump_n + 1
        if (clockdump_n >= 3) clockdump_n = 1
      case default
        call finalstop(routinename, 'Absurd clockdump policy case.  Abort.')
      end select
    end if
    !
    !---------------------------  OTHER OUTPUT  ----------------------------
    !
    call timeslice
    !
  END SUBROUTINE dataio
  !
  !=======================================================================
  !
  SUBROUTINE dumpdata(dump_index, dump_prefix, recordfile)
    INTEGER, INTENT(in) :: dump_index
    CHARACTER(LEN=*), INTENT(in) :: dump_prefix
    CHARACTER(LEN=*), INTENT(in) :: recordfile

    CHARACTER(LEN=messagelinelength) message
    CHARACTER(LEN=4 + 2*formatlength) dumpnameformat
    CHARACTER(LEN=filenamelength) :: dumpname

    write (dumpnameformat, format_format) &
      '(i', dumpnamelength, '.', dumpnamelength, ')'

    write (dumpname, dumpnameformat) dump_index

    call writedata(trim(dump_prefix), '_'//trim(dumpname))

    call auxiliarydata(trim(dump_prefix), '_'//trim(dumpname))

    write (message, '(a,a,a,a,es14.7,es14.7)') &
      trim(dumpname), ' ', &
      ntimestepname, ' ', &
      time, dt
    call writemessage(recordfile, message)

  END SUBROUTINE dumpdata
  !
  !=======================================================================
  !
  SUBROUTINE auxiliarydata(prefix, suffix, debuglevel)
    CHARACTER(LEN=*), INTENT(IN) :: prefix
    CHARACTER(LEN=*), INTENT(IN) :: suffix
    INTEGER, INTENT(IN), OPTIONAL :: debuglevel

    CHARACTER(LEN=5) fieldname

    ! Consider decentralizing this: the user may like to
    ! introduce these either the full Zeus3D way or as an option
    ! of debug output getting produced (even at low debug levels)
    ! when the time to produce output is reached
    REALNUM, POINTER, SAVE, DIMENSION(:, :, :) :: &
      vi1 => null(), vi2 => null(), vi3 => null()

    if (imhd > 0) then
      if (with_ad_1f) then
        call ion_velocity_get(vi1, vi2, vi3)
        fieldname = 'vi1'
        call writedata(vi1, trim(prefix)//trim(fieldname)//trim(suffix), &
                       debuglevel)
        fieldname = 'vi2'
        call writedata(vi2, trim(prefix)//trim(fieldname)//trim(suffix), &
                       debuglevel)
        fieldname = 'vi3'
        call writedata(vi3, trim(prefix)//trim(fieldname)//trim(suffix), &
                       debuglevel)
        call ion_velocity_release(vi1, vi2, vi3)
      end if
    end if
  END SUBROUTINE auxiliarydata
  !
  !=======================================================================
  !
  SUBROUTINE iocheck
    ! Checks for input messages, output conditions, termination conditions,
    ! and sanity tests.
    !
    !   1) Checks if any of the physical (problem) time limit, cycle limit,
    !      or reserve time limit (time for making a restart dump before a
    !      time-limit abort occurs) has been exceeded.  If so, then the
    !      a flag that stops execution of the main loop is set.
    !   2) Checks if it is time to output data in the various formats possible
    !      If so the appropriate switches are turned on.
    !   3) Reads any interrupt messages from an runtime input file,
    !      and takes the appropriate action.
    !   4) If expected, do a sanity check.
    !
    !  SOME OF THE INPUT NEEDED [via modules]:
    !    clockused , clocktotal  cpu time consumed by job, and cpu limit (seconds)
    !    clockrem  , clocksave   remaining and reserve cpu time for job (seconds)
    !    time  , tlim            current problem time and limit
    !    ntimestep, nlim         current cycle value and limit
    !    tdump , dtdump          time of last data dump, and dump interval
    !    iotty                   output unit for crt messages
    !
    !  SOME OF THE OUTPUT GENERATED [via modules]:
    !    terminate_flag  (if set to 1 it terminates execution of the main loop)
    !    dump_switch     dump switch             (1 = on)

#ifdef MPI
    USE mpi
    INTEGER terminate_buffer, ierr
#endif /* MPI */
    INTEGER isanity
    REALNUM clockrem, clockused
    INTEGER iteration_counter
    CHARACTER(LEN=*), PARAMETER :: routinename = 'z_iocheck'
    CHARACTER(LEN=messagelinelength) message
    !
    !   Get the current timer status
    !   Do this first, so as to produce the updated and correct
    !   values for timestamps at termination time for any reason.
    !
    call timer_check(clockused)
    clockrem = clocktotal - clockused

    !
    !      Call for a sanity check
    !
    if (check_timestep(sanitycheckfrequency)) then
      call sanity(isanity)
      if (isanity /= 0) then
        write (message, '(a,i2,a)') &
          'IOCHECK: Infinities or NaNs present in ', &
          isanity, ' of the fields.'
        call threeoutput(routinename, message)
        call threeoutput(routinename, 'IOCHECK: Run will finish soon.')
        call get_out('IOCHECK: Sanity check failed')
      end if
    end if
    !
    !      Check the inputruntime file
    !
    if (check_timestep(iocheckfrequency)) then
      call read_inputruntime
    end if

    !
    !   RFK:  No tlim execution limit if tlim==zro
    !
    if ((tlim > zro) .and. (time >= tlim)) then
      call get_out('IOCHECK: Terminating on problem time limit')
    end if

    if ((nlim == 0) .or. ((nlim > 0) .and. (ntimestep >= nlim))) then
      call get_out('IOCHECK: Terminating on cycle limit')
    end if
    !

    !
    !  Check to see if "time limit", "cycle limit", or "reserve time"
    !  (time for making a dump before a time-limit abort occurs) has been
    !  exceeded.  If it has, set the flag that stops execution of the main loop,
    !  print message, and return.
    !
    if ((clocksave == zro) .or. &
        ((clocksave > zro) .and. (clockrem <= clocksave))) then
      call get_out('IOCHECK: Terminating on reserve clock time limit')
    end if

    !
    !  Check to see if too many warnings have been issued
    !
    if (nwarn >= 20) then
      write (message, '(a,i3,a)') &
        'IOCHECK: Terminating on warning limit (', nwarn, ' warnings)'
      call get_out(message)
    end if

    !
    !  Check if too many hot zone warnings have been issued
    !
    if (hotzonewarn >= hotzonemax) then
      write (message, '(a,i3,a)') &
        'IOCHECK: Terminating on hot zone warning limit (', &
        hotzonewarn, ' warnings)'
      call get_out(message)
    end if

#ifdef MPI
    !
    !      Raise termination flag for all nodes if it is up
    !      for at least one node.  Needed more than once
    !      because of interactions between the terminate_flag
    !      and the dump_switch and the clockdump_switch
    !
    terminate_buffer = terminate_flag
    call MPI_ALLREDUCE(terminate_buffer, terminate_flag, &
                       1, MPI_INTEGER, MPI_MAX, MPI_COMM_WORLD, ierr)
#endif /* MPI */

    !
    !  Check to see if it is time to write any dumps; if so turn on the
    !  appropriate switch to produce output in calling routine.
    !

    !dump_switch=0  ! Commented out for the sake of restart runs
    if (dtdump > zro) then
      ! Check this in only one node - result will be broadcast.
      if (node == rootnode) then
        if (time >= tdump .or. terminate_flag == 1) then
          dump_switch = 1
        end if
        if (dump_switch == 1 .and. terminate_flag == 0) then
          iteration_counter = 0
          do
            if (time < tdump) exit
            tdump = tdump + dtdump  ! tdump is neither updated nor relevant outside the rootnode
            iteration_counter = iteration_counter + 1
            if (iteration_counter > 10000) then
              call threeoutput(routinename, 'IOCHECK: Terminating on too many' &
                               //' dtdump iterations.')
              write (message, '(a,i10)') 'IOCHECK: Iteration counter= ', &
                iteration_counter
              call threeoutput(routinename, message)
              write (message, '(a,3es12.5)') 'IOCHECK: time, tdump, dtdump= ', &
                time, tdump, dtdump
              call get_out(message)
            end if
          end do
        end if
      end if
#ifdef MPI
      call MPI_BCAST(dump_switch, 1, MPI_INTEGER, rootnode, MPI_COMM_WORLD, &
                     ierr)
#endif /* MPI */
    end if

#ifdef MPI
    !
    !      Raise termination flag for all nodes if it is up
    !      for at least one node.  Needed more than once
    !      because of interactions between the terminate_flag
    !      and the dump_switch and the clockdump_switch
    !
    terminate_buffer = terminate_flag
    call MPI_ALLREDUCE(terminate_buffer, terminate_flag, &
                       1, MPI_INTEGER, MPI_MAX, MPI_COMM_WORLD, ierr)
#endif /* MPI */

    !
    !  Clockdump output
    !

    ! Skip if the terminate_flag is on

    clockdump_switch = 0
    if (clockdump_policy /= 0 .and. clockdump_dt > 0 &
        .and. terminate_flag == 0) then
      ! Check this in only one node - result will be broadcast.
      if (node == rootnode) then

        ! For clockdump_policy=-1, if there is already some other kind
        ! of output going to be produced, advance clockdump_t but not turn on
        ! the switch

        if (clockused >= clockdump_t &
            .and. .not. (dump_switch == 1 .and. clockdump_policy == 1)) then
          clockdump_switch = 1
        end if
        if (dump_switch == 1 .and. clockdump_policy == 1) then
          ! Advance but *not* to the next clockdump_t, but counting from the present
          clockdump_t = max(clockdump_t, clockused + clockdump_dt)
        end if
        if (clockdump_switch == 1) then
          iteration_counter = 0
          do
            if (clockused < clockdump_t) exit
            clockdump_t = clockdump_t + clockdump_dt  ! clockdump_t is neither updated nor relevant outside the rootnode
            iteration_counter = iteration_counter + 1
            if (iteration_counter > 10000) then
              call threeoutput(routinename, 'IOCHECK: Terminating on too many' &
                               //' clockdump_dt iterations.')
              write (message, '(a,i10)') 'IOCHECK: Iteration counter= ', &
                iteration_counter
              call threeoutput(routinename, message)
              write (message, '(a,3es12.5)') &
                'IOCHECK: clockused, clockdump_t, clockdump_dt= ', &
                clockused, clockdump_t, clockdump_dt
              call get_out(message)
            end if
          end do
        end if
      end if
    end if
#ifdef MPI
    call MPI_BCAST(clockdump_switch, 1, MPI_INTEGER, rootnode, &
                   MPI_COMM_WORLD, ierr)
#endif /* MPI */

#ifdef MPI
    !
    !      Raise termination flag for all nodes if it is up
    !      for at least one node.  Needed more than once
    !      because of interactions between the terminate_flag
    !      and the dump_switch and the clockdump_switch
    !
    terminate_buffer = terminate_flag
    call MPI_ALLREDUCE(terminate_buffer, terminate_flag, &
                       1, MPI_INTEGER, MPI_MAX, MPI_COMM_WORLD, ierr)
#endif /* MPI */

    ! Make the regular dump do double duty as final dump.  Do not care if tdump gets wrong at this stage.
    if (terminate_flag == 1) then
      dump_switch = 1
      clockdump_switch = 0
    end if

    !
    !  Check for other kinds of output
    !
    !
    call timeslice_check
    !
  CONTAINS
    SUBROUTINE read_inputruntime
      INTEGER, PARAMETER :: lunit = ioin_inputruntime
      CHARACTER(LEN=messagelinelength) input
      INTEGER :: ios

      open (unit=lunit, file='inputruntime', &
            status='unknown', &
            form='formatted', &
            position='rewind', &
            access='sequential', &
            action='readwrite') ! File is created if not existent
      do
        read (lunit, '(a)', iostat=ios) input
        if (ios /= 0) then
          if (ios > 0) then  ! Send an error message on error, and exit loop.
            write (message, '(a,i10,a)') 'IOCHECK: Error when reading file', ios
            call sendmessage
          end if
          exit ! Exit loop silently on end-of-file
        end if
        select case (input)
        case ('exit') ! Soft exit
          call sendmessage('IOCHECK: Exit requested')
          call get_out('IOCHECK: Exit requested')
        case ('dump') ! Force output
          call sendmessage('IOCHECK: Output requested')
          dump_switch = 1
        case ('dtdump') ! Change output frequency for the future
          call sendmessage('IOCHECK: Variable dtdump changed')
          read (lunit, *, iostat=ios) dtdump
          dtdump = dtdump*time_factor
          write (message, "(a,es12.5)") 'IOCHECK: New value: dtdump = ', dtdump
          call sendmessage
          if (ios /= 0) then  ! Send an error message on error or end of file, and exit loop.
            write (message, '(a,i10)') 'IOCHECK: Error when reading dtdump', ios
            call sendmessage
            exit
          end if
        case ('tdump') ! Change time for next output
          call sendmessage('IOCHECK: Variable tdump changed')
          read (lunit, *, iostat=ios) tdump
          tdump = tdump*time_factor
          write (message, "(a,es12.5)") 'IOCHECK: New value: tdump = ', tdump
          call sendmessage
          if (ios /= 0) then  ! Send an error message on error or end of file, and exit loop.
            write (message, '(a,i10)') 'IOCHECK: Error when reading tdump', ios
            call sendmessage
            exit
          end if
        case ('dtdump_reset') ! Change output frequency, and reset tdump
          call sendmessage('IOCHECK: Variables dtdump and tdump changed')
          read (lunit, *, iostat=ios) dtdump
          dtdump = dtdump*time_factor
          write (message, "(a,es12.5)") 'IOCHECK: New value: dtdump = ', dtdump
          call sendmessage
          if (ios /= 0) then  ! Send an error message on error or end of file, and exit loop.  Do not adjust tdump
            write (message, '(a,i10)') &
              'IOCHECK: Error when reading dtdump. tdump not adjusted', ios
            call sendmessage
            exit
          end if
          if (dtdump <= zro) exit
          if (tdump_initial_used) then
            tdump = tdump_initial
          else
            tdump = time_initial
          end if
          do
            if (time <= tdump) exit
            tdump = tdump + dtdump
          end do
          write (message, "(a,es12.5)") 'IOCHECK: New value: tdump = ', tdump
          call sendmessage
        case ('tlim') ! Change time limit
          call sendmessage('IOCHECK: Variable tlim changed')
          read (lunit, *, iostat=ios) tlim
          tlim = tlim*time_factor
          write (message, "(a,es12.5)") 'IOCHECK: New value: tlim = ', tlim
          call sendmessage
          if (ios /= 0) then  ! Send an error message on error or end of file, and exit loop.
            write (message, '(a,i10)') 'IOCHECK: Error when reading tlim', ios
            call sendmessage
            exit
          end if
        case ('nlim') ! Change timestep limit
          call sendmessage('IOCHECK: Variable nlim changed')
          read (lunit, *, iostat=ios) nlim
          write (message, "(a,i11)") 'IOCHECK: New value: nlim = ', nlim
          call sendmessage
          if (ios /= 0) then  ! Send an error message on error or end of file, and exit loop.
            write (message, '(a,i10)') 'IOCHECK: Error when reading nlim', ios
            call sendmessage
            exit
          end if
        case ('clocktotal') ! Change clocktotal limit
          call sendmessage('IOCHECK: Variable clocktotal changed')
          read (lunit, *, iostat=ios) clocktotal
          write (message, "(a,es12.5)") &
            'IOCHECK: New value: clocktotal = ', clocktotal
          call sendmessage
          if (ios /= 0) then  ! Send an error message on error or end of file, and exit loop.
            write (message, '(a,i10)') &
              'IOCHECK: Error when reading clocktotal', ios
            call sendmessage
            exit
          end if
        case ('stop') ! Sudden stop
          stop
        case ('debug') ! Adjust debug level
          call sendmessage('IOCHECK: Debug level reset')
          read (lunit, *, iostat=ios) debug
          write (message, "(a,i11)") 'IOCHECK: New value: debug = ', debug
          call sendmessage
          if (ios /= 0) then  ! Send an error message on error or end of file, and exit loop.
            write (message, '(a,i10)') &
              'IOCHECK: Error when reading the debug level variable', ios
            call sendmessage
            exit
          end if
        case default
          ! Do nothing
        end select
      end do
      close (lunit, iostat=ios)
      if (ios /= 0) then  ! Send an error message on error
        write (message, '(a,i10)') 'IOCHECK: Error when closing file', ios
        call sendmessage
      end if
    END SUBROUTINE read_inputruntime
    SUBROUTINE sendmessage(outputstring)
      CHARACTER(LEN=*), INTENT(IN), OPTIONAL :: outputstring
      if (present(outputstring)) then
        call threeoutput(routinename, &
                         outputstring//'. Timestep '//trim(ntimestepname))
      else
        call threeoutput(routinename, &
                         message//'. Timestep '//trim(ntimestepname))
      end if
    END SUBROUTINE sendmessage
    SUBROUTINE get_out(outputstring)
      CHARACTER(LEN=*) outputstring

      CHARACTER(LEN=*), PARAMETER :: formatlimits1 = &
                                     "('IOCHECK: tlim=',1p,g14.8,'  nlim=',i"
      CHARACTER(LEN=*), PARAMETER :: formatlimits2 = &
                                     ",' ctotal=',g11.5,' csave=',g11.5)"
      CHARACTER(LEN=LEN(formatlimits1) + formatlength + LEN(formatlimits2)) :: &
        formatlimits

      CHARACTER(LEN=*), PARAMETER :: formatstatus1 = &
                                     "('IOCHECK: time=',1p,g14.8,' #step=',i"
      CHARACTER(LEN=*), PARAMETER :: formatstatus2 = &
                                     ",' cused=',g11.5,'  crem=',g11.5)"
      CHARACTER(LEN=LEN(formatstatus1) + formatlength + LEN(formatstatus2)) :: &
        formatstatus

      write (formatlimits, format_format) &
        formatlimits1, &
        LEN(ntimestepname), '.', LEN(ntimestepname), &
        formatlimits2
      write (formatstatus, format_format) &
        formatstatus1, &
        LEN(ntimestepname), '.', LEN(ntimestepname), &
        formatstatus2

      call threeoutput(routinename, outputstring)
      write (message, formatlimits) tlim, nlim, clocktotal, clocksave
      call threeoutput(routinename, message)
      write (message, formatstatus) time, ntimestep, clockused, clockrem
      call threeoutput(routinename, message)
      terminate_flag = 1
    END SUBROUTINE get_out
  END SUBROUTINE iocheck
END MODULE zeus_iochecks
