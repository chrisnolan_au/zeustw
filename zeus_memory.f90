#include "macros"
MODULE zeus_memory
  USE zeus_variables
  USE zeus_io
  IMPLICIT NONE

  PRIVATE

#ifndef MEMORY_DEBUG
  PUBLIC :: get_memory, release_memory
#else /* MEMORY_DEBUG */
  PUBLIC :: get_memory_debug, release_memory_debug
#endif /* MEMORY_DEBUG */

  SAVE

  INTEGER, PARAMETER :: maximum_number_of_arrays = 20

#ifdef F2003_ALLOCATABLES
  !Fortran2003-style:
  !(this feature is already implemented in many Fortran compilers)
  TYPE WORKARRAY
    REALNUM, ALLOCATABLE :: wa(:, :, :)
  END TYPE WORKARRAY
  TYPE(WORKARRAY), TARGET, DIMENSION(maximum_number_of_arrays) :: workspace
#else /* F2003_ALLOCATABLES */
  !Fortran95-style:
  TYPE WORKARRAY
    REALNUM, POINTER :: wa(:, :, :) => null()
  END TYPE WORKARRAY
  TYPE(WORKARRAY), DIMENSION(maximum_number_of_arrays) :: workspace
#endif /* F2003_ALLOCATABLES */

  LOGICAL, DIMENSION(maximum_number_of_arrays) :: is_allocated = .false.
  LOGICAL, DIMENSION(maximum_number_of_arrays) :: is_busy = .false.

CONTAINS
  !
  !=======================================================================
  !
#ifndef MEMORY_DEBUG
  SUBROUTINE get_memory(array)
#else /* MEMORY_DEBUG */
    SUBROUTINE get_memory_debug(array, arrayname, line)
      CHARACTER(LEN=*), INTENT(IN) :: arrayname
      INTEGER, INTENT(IN) :: line
      CHARACTER(LEN=50) message
#endif /* MEMORY_DEBUG */
      REALNUM, POINTER :: array(:, :, :)
      CHARACTER(LEN=*), PARAMETER :: routinename = 'z_get_memory'

      INTEGER :: i, free_index

      if (associated(array)) then
#ifdef MEMORY_DEBUG
        write (message, '(a,i5)') trim(arrayname)//' '//trim(ntimestepname), &
          line
        call threeoutput(routinename, message)
#endif /* MEMORY_DEBUG */
        call finalstop(routinename, "Array already associated")
      end if

      ! Check if there is any free array space
      free_index = -1
      do i = 1, maximum_number_of_arrays
        if (.not. is_busy(i)) then
          free_index = i
          exit
        end if
      end do

      if (free_index < 0) then
        call threeoutput(routinename, "There is not enough workspace")
        call finalstop(routinename, "Check for memory leaks, or increase the" &
                       //" workspace size")
      end if

      ! No check for allocation error included in this version
      if (.not. is_allocated(free_index)) then
        allocate (workspace(free_index)%wa(isa:iea, jsa:jea, ksa:kea))
        is_allocated(free_index) = .true.
        !print *, 'B1 ', is_allocated, free_index
        !print *, 'B2 ', is_busy, free_index
      end if

      ! Assume allocation either was not needed or it has succeeded
      array => workspace(free_index)%wa

      ! Mark workspace array as busy
      is_busy(free_index) = .true.

#ifndef MEMORY_DEBUG
    END SUBROUTINE get_memory
#else /* MEMORY_DEBUG */
  END SUBROUTINE get_memory_debug
#endif /* MEMORY_DEBUG */

#ifndef MEMORY_DEBUG
  SUBROUTINE release_memory(array)
#else /* MEMORY_DEBUG */
    SUBROUTINE release_memory_debug(array, arrayname, line)
      CHARACTER(LEN=*), INTENT(IN) :: arrayname
      INTEGER, INTENT(IN) :: line
      CHARACTER(LEN=50) message
#endif /* MEMORY_DEBUG */
      REALNUM, POINTER :: array(:, :, :)
      CHARACTER(LEN=*), PARAMETER :: routinename = 'z_release_memory'
      INTEGER :: i
      INTEGER :: index

      ! Search for the array workspace index
      ! Optimizations of this search are conceivable.
      ! It is also conceivable keeping the integer index
      ! packed together with the pointer, obviating the
      ! need of any search; but keeping track of that package
      ! can be a pain for the calling routines.
      index = -1
      do i = 1, maximum_number_of_arrays
        if (associated(array, workspace(i)%wa)) then
          index = i
          exit
        end if
      end do
      if (index < 0) then
#ifdef MEMORY_DEBUG
        write (message, '(a,i5)') trim(arrayname)//' '//trim(ntimestepname), &
          line
        call threeoutput(routinename, message)
#endif /* MEMORY_DEBUG */
        call finalstop(routinename, "Array was not found in workspace.")
      end if

      array => null()
      is_busy(index) = .false.

#ifndef MEMORY_DEBUG
    END SUBROUTINE release_memory
#else /* MEMORY_DEBUG */
  END SUBROUTINE release_memory_debug
#endif /* MEMORY_DEBUG */

END MODULE zeus_memory
