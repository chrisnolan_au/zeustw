#include "macros"
MODULE zeus_mpi
  USE zeus_variables
  USE zeus_io
  IMPLICIT NONE

  PRIVATE
  PUBLIC :: mpisetup, mpifield, mpi_subsection_type, &
            nodefun, mpisetup_datatypes
  PUBLIC :: mpifield_e

  INTERFACE mpifield
    MODULE PROCEDURE mpifield_3d, mpifield_4d, consolidate
  END INTERFACE
  INTERFACE chain_sr
    MODULE PROCEDURE chain_sr_3d, chain_sr_4d
  END INTERFACE

  ! MPI derived datatypes used to pass boundary data
  SAVE
  INTEGER, PRIVATE :: chunkiw(3), chunktf(3)
  ! Used for message passing of the extended array gp_e
  INTEGER, PRIVATE :: chunk_e(3)

CONTAINS

  !** GROUP: MPI *********************************************************
  !**                                                                   **
  !**                   G R O U P :   M P I                             **
  !**                                                                   **
  !***********************************************************************
  !
  !=======================================================================
  !
  SUBROUTINE mpisetup
    ! This one has to be done before grid initialization because
    ! the grid initialization may need to know the node locations.

    CHARACTER(LEN=*), PARAMETER :: routinename = 'z_mpisetup'
    !
    !----------------   SET LOCAL NODE COORDINATES  ------------------------
    !
    !      Calculate local tile coordinates.  The inverse of this
    !      computation is the function nodefun, defined separately.
    !
    node3 = int((node + zro)/(nnodes1*nnodes2))
    node2 = int((node - node3*nnodes1*nnodes2 + zro)/nnodes1)
    node1 = node - node3*nnodes1*nnodes2 - node2*nnodes1
    !
    ! Calculate coordinates of the neighbor processes, then use them
    ! to calculate their IDs.  The coordinates are not needed, so they
    ! get overwritten.
    !
    nodeup1 = node1 + 1
    nodedn1 = node1 - 1
    nodeup2 = node2 + 1
    nodedn2 = node2 - 1
    nodeup3 = node3 + 1
    nodedn3 = node3 - 1
    if (nodedn1 < 0) nodedn1 = nodedn1 + nnodes1
    if (nodedn2 < 0) nodedn2 = nodedn2 + nnodes2
    if (nodedn3 < 0) nodedn3 = nodedn3 + nnodes3
    if (nodeup1 >= nnodes1) nodeup1 = nodeup1 - nnodes1
    if (nodeup2 >= nnodes2) nodeup2 = nodeup2 - nnodes2
    if (nodeup3 >= nnodes3) nodeup3 = nodeup3 - nnodes3

    nodeup1 = nodefun(nodeup1, node2, node3)
    nodeup2 = nodefun(node1, nodeup2, node3)
    nodeup3 = nodefun(node1, node2, nodeup3)
    nodedn1 = nodefun(nodedn1, node2, node3)
    nodedn2 = nodefun(node1, nodedn2, node3)
    nodedn3 = nodefun(node1, node2, nodedn3)
    !
    ! Disallow cycling types that are not properly implemented
    ! for an odd number of nodes.  The case of one node is special
    ! in that recovery is still possible, and it is treated separately.
    !
    if (nodecyc1 .and. nnodes1 == 1) then
      nodecyc1 = .false.
      call threeoutput(routinename, 'MPI condition in direction 1 corrected')

    end if
    if (nodecyc2 .and. nnodes2 == 1) then
      nodecyc2 = .false.
      call threeoutput(routinename, 'MPI condition in direction 2 corrected')
    end if
    if (nodecyc3 .and. nnodes3 == 1) then
      nodecyc3 = .false.
      call threeoutput(routinename, 'MPI condition in direction 3 corrected')
    end if
    if (nodecyc1 .and. mod(nnodes1, 2) == 1) then
      call finalstop(routinename, 'Defective MPI condition in direction 1')
    end if
    if (nodecyc2 .and. mod(nnodes2, 2) == 1) then
      call finalstop(routinename, 'Defective MPI condition in direction 2')
    end if
    if (nodecyc3 .and. mod(nnodes3, 2) == 1) then
      call finalstop(routinename, 'Defective MPI condition in direction 3')
    end if
    !
    ! Logical variables marking the first and last nodes on each grid
    ! dimension
    !
    lg_firstnode1 = (node1 == 0)
    lg_firstnode2 = (node2 == 0)
    lg_firstnode3 = (node3 == 0)
    lg_lastnode1 = (node1 == nnodes1 - 1)
    lg_lastnode2 = (node2 == nnodes2 - 1)
    lg_lastnode3 = (node3 == nnodes3 - 1)
    !
    ! Logical variables marking the conceptually first and last nodes
    !
    l_firstnode1 = lg_firstnode1 .and. (.not. nodecyc1)
    l_firstnode2 = lg_firstnode2 .and. (.not. nodecyc2)
    l_firstnode3 = lg_firstnode3 .and. (.not. nodecyc3)
    l_lastnode1 = lg_lastnode1 .and. (.not. nodecyc1)
    l_lastnode2 = lg_lastnode2 .and. (.not. nodecyc2)
    l_lastnode3 = lg_lastnode3 .and. (.not. nodecyc3)
    l_firstnodew(1, 1) = l_firstnode1
    l_firstnodew(2, 1) = l_firstnode2
    l_firstnodew(3, 1) = l_firstnode3
    l_firstnodew(1, 2) = l_lastnode1
    l_firstnodew(2, 2) = l_lastnode2
    l_firstnodew(3, 2) = l_lastnode3
  END SUBROUTINE mpisetup
  !
  !=======================================================================
  !
  SUBROUTINE mpisetup_datatypes
    CHARACTER(LEN=*), PARAMETER :: routinename = 'z_mpisetup_datatypes'
    INTEGER ic, jc, kc, nc
    INTEGER, PARAMETER :: depth_e = 1

    CHARACTER(LEN=messagelinelength) :: message

    ! This part of the setup has to be done after grid initialization
    ! because of some grid size information is needed

    !
    !      Define "chunk" datatypes used to pass field blocks
    !
    ic = iea - isa + 1
    jc = jea - jsa + 1
    kc = kea - ksa + 1

    if (debug >= 2) then
      write (message, *) routinename, &
        '  regular chunks: ic,jc,kc,nghosts=', ic, jc, kc, nghosts
      call threeoutput(routinename, message)
    end if

    call mpi_subsection_type(chunkiw(1), (/ic, jc, kc/), (/nghosts, jc, kc/))
    call mpi_subsection_type(chunkiw(2), (/ic, jc, kc/), (/ic, nghosts, kc/))
    call mpi_subsection_type(chunkiw(3), (/ic, jc, kc/), (/ic, jc, nghosts/))

    !
    if (number_of_tracer_fields > 0) then
      nc = number_of_tracer_fields
      call mpi_subsection_type(chunktf(1), (/ic, jc, kc, nc/), &
                               (/nghosts, jc, kc, nc/))
      call mpi_subsection_type(chunktf(2), (/ic, jc, kc, nc/), &
                               (/ic, nghosts, kc, nc/))
      call mpi_subsection_type(chunktf(3), (/ic, jc, kc, nc/), &
                               (/ic, jc, nghosts, nc/))
    end if

    !
    !      Define "chunk" datatypes used to pass field blocks on
    !      the extended grid.  Do that even when there is no algorithmic
    !      need for an extended grid.
    !

    ! Notice that ic may have different values for different processes
    ic = iea_e - isa_e + 1
    jc = jea - jsa + 1
    kc = kea - ksa + 1

    if (debug >= 2) then
      write (message, *) routinename, &
        ' extended chunks: ic,jc,kc,depth_e=', ic, jc, kc, depth_e
      call threeoutput(routinename, message)
    end if

    call mpi_subsection_type(chunk_e(1), (/ic, jc, kc/), (/depth_e, jc, kc/))
    call mpi_subsection_type(chunk_e(2), (/ic, jc, kc/), (/ic, depth_e, kc/))
    call mpi_subsection_type(chunk_e(3), (/ic, jc, kc/), (/ic, jc, depth_e/))

  END SUBROUTINE mpisetup_datatypes
  !
  !=======================================================================
  !
  INTEGER FUNCTION nodefun(i, j, k)
    INTEGER, INTENT(IN) :: i, j, k
    nodefun = i + nnodes1*j + nnodes1*nnodes2*k
  END FUNCTION nodefun
  !
  !=======================================================================
  !
  SUBROUTINE chain_sr_3d(field, face)
#ifdef MPI
    USE mpi
#endif /* MPI */
    INTEGER, INTENT(IN) ::  face
    REALNUM, INTENT(INOUT) :: field(isa:iea, jsa:jea, ksa:kea)
#ifdef MPI
    INTEGER chunk, dest, ierr, status(MPI_STATUS_SIZE)
    INTEGER i_send, j_send, k_send &
      , i_recv, j_recv, k_recv &
      , tagsend, tagrecv
    INTEGER parity
    INTEGER depth

    !  Check if grid direction is active
    if (iwnac(face) == 0) return

    depth = nghosts
    chunk = chunkiw(face)

    first_pass: do
      !
      !      First pass
      !
      i_send = isa
      j_send = jsa
      k_send = ksa
      i_recv = isa
      j_recv = jsa
      k_recv = ksa
      !
      if (face == 1) then
        parity = mod(node1, 2)
        if (parity == 1) then
          dest = nodedn1
          i_send = is
          i_recv = is - depth
        else
          if (.not. l_lastnode1) then
            dest = nodeup1
            i_send = iep1 - depth
            i_recv = iep1
          else
            exit first_pass
          end if
        end if
      end if
      !
      if (face == 2) then
        parity = mod(node2, 2)
        if (parity == 1) then
          dest = nodedn2
          j_send = js
          j_recv = js - depth
        else
          if (.not. l_lastnode2) then
            dest = nodeup2
            j_send = jep1 - depth
            j_recv = jep1
          else
            exit first_pass
          end if
        end if
      end if
      !
      if (face == 3) then
        parity = mod(node3, 2)
        if (parity == 1) then
          dest = nodedn3
          k_send = ks
          k_recv = ks - depth
        else
          if (.not. l_lastnode3) then
            dest = nodeup3
            k_send = kep1 - depth
            k_recv = kep1
          else
            exit first_pass
          end if
        end if
      end if
      !
      tagsend = 10*face + 1
      tagrecv = 10*face + 1
      call MPI_SENDRECV(field(i_send, j_send, k_send), 1, chunk &
                        , dest, tagsend &
                        , field(i_recv, j_recv, k_recv), 1, chunk &
                        , dest, tagrecv &
                        , MPI_COMM_WORLD, status, ierr)
      exit first_pass
    end do first_pass

    second_pass: do
      !
      !      Second pass
      !
      i_send = isa
      j_send = jsa
      k_send = ksa
      i_recv = isa
      j_recv = jsa
      k_recv = ksa
      !
      if (face == 1) then
        parity = mod(node1, 2)
        if (parity == 0) then
          if (.not. l_firstnode1) then
            dest = nodedn1
            i_send = is
            i_recv = is - depth
          else
            exit second_pass
          end if
        else
          if (.not. l_lastnode1) then
            dest = nodeup1
            i_send = iep1 - depth
            i_recv = iep1
          else
            exit second_pass
          end if
        end if
      end if
      !
      if (face == 2) then
        parity = mod(node2, 2)
        if (parity == 0) then
          if (.not. l_firstnode2) then
            dest = nodedn2
            j_send = js
            j_recv = js - depth
          else
            exit second_pass
          end if
        else
          if (.not. l_lastnode2) then
            dest = nodeup2
            j_send = jep1 - depth
            j_recv = jep1
          else
            exit second_pass
          end if
        end if
      end if
      !
      if (face == 3) then
        parity = mod(node3, 2)
        if (parity == 0) then
          if (.not. l_firstnode3) then
            dest = nodedn3
            k_send = ks
            k_recv = ks - depth
          else
            exit second_pass
          end if
        else
          if (.not. l_lastnode3) then
            dest = nodeup3
            k_send = kep1 - depth
            k_recv = kep1
          else
            exit second_pass
          end if
        end if
      end if
      !
      tagsend = 10*face + 2
      tagrecv = 10*face + 2
      call MPI_SENDRECV(field(i_send, j_send, k_send), 1, chunk &
                        , dest, tagsend &
                        , field(i_recv, j_recv, k_recv), 1, chunk &
                        , dest, tagrecv &
                        , MPI_COMM_WORLD, status, ierr)
      exit second_pass
    end do second_pass
#endif /* MPI */
  END SUBROUTINE chain_sr_3d
  !
  !=======================================================================
  !
  SUBROUTINE chain_sr_4d(field, face)
#ifdef MPI
    USE mpi
#endif /* MPI */
    INTEGER, INTENT(IN) ::  face
    REALNUM, INTENT(INOUT) :: &
      field(isa:iea, jsa:jea, ksa:kea, number_of_tracer_fields)
#ifdef MPI
    INTEGER chunk, dest, ierr, status(MPI_STATUS_SIZE)
    INTEGER i_send, j_send, k_send &
      , i_recv, j_recv, k_recv &
      , tagsend, tagrecv
    INTEGER f_send, f_recv
    INTEGER parity
    INTEGER depth

    !  Check if grid direction is active
    if (iwnac(face) == 0) return

    ! Check if there are tracer fields to pass
    if (number_of_tracer_fields <= 0) return

    depth = nghosts
    chunk = chunktf(face)

    first_pass: do
      !
      !      First pass
      !
      i_send = isa
      j_send = jsa
      k_send = ksa

      i_recv = isa
      j_recv = jsa
      k_recv = ksa

      f_send = 1
      f_recv = 1
      !
      if (face == 1) then
        parity = mod(node1, 2)
        if (parity == 1) then
          dest = nodedn1
          i_send = is
          i_recv = is - depth
        else
          if (.not. l_lastnode1) then
            dest = nodeup1
            i_send = iep1 - depth
            i_recv = iep1
          else
            exit first_pass
          end if
        end if
      end if
      !
      if (face == 2) then
        parity = mod(node2, 2)
        if (parity == 1) then
          dest = nodedn2
          j_send = js
          j_recv = js - depth
        else
          if (.not. l_lastnode2) then
            dest = nodeup2
            j_send = jep1 - depth
            j_recv = jep1
          else
            exit first_pass
          end if
        end if
      end if
      !
      if (face == 3) then
        parity = mod(node3, 2)
        if (parity == 1) then
          dest = nodedn3
          k_send = ks
          k_recv = ks - depth
        else
          if (.not. l_lastnode3) then
            dest = nodeup3
            k_send = kep1 - depth
            k_recv = kep1
          else
            exit first_pass
          end if
        end if
      end if
      !
      tagsend = 10*face + 1
      tagrecv = 10*face + 1
      call MPI_SENDRECV(field(i_send, j_send, k_send, f_send), 1, chunk &
                        , dest, tagsend &
                        , field(i_recv, j_recv, k_recv, f_recv), 1, chunk &
                        , dest, tagrecv &
                        , MPI_COMM_WORLD, status, ierr)
      exit first_pass
    end do first_pass

    second_pass: do
      !
      !      Second pass
      !
      i_send = isa
      j_send = jsa
      k_send = ksa
      i_recv = isa
      j_recv = jsa
      k_recv = ksa

      f_send = 1
      f_recv = 1
      !
      if (face == 1) then
        parity = mod(node1, 2)
        if (parity == 0) then
          if (.not. l_firstnode1) then
            dest = nodedn1
            i_send = is
            i_recv = is - depth
          else
            exit second_pass
          end if
        else
          if (.not. l_lastnode1) then
            dest = nodeup1
            i_send = iep1 - depth
            i_recv = iep1
          else
            exit second_pass
          end if
        end if
      end if
      !
      if (face == 2) then
        parity = mod(node2, 2)
        if (parity == 0) then
          if (.not. l_firstnode2) then
            dest = nodedn2
            j_send = js
            j_recv = js - depth
          else
            exit second_pass
          end if
        else
          if (.not. l_lastnode2) then
            dest = nodeup2
            j_send = jep1 - depth
            j_recv = jep1
          else
            exit second_pass
          end if
        end if
      end if
      !
      if (face == 3) then
        parity = mod(node3, 2)
        if (parity == 0) then
          if (.not. l_firstnode3) then
            dest = nodedn3
            k_send = ks
            k_recv = ks - depth
          else
            exit second_pass
          end if
        else
          if (.not. l_lastnode3) then
            dest = nodeup3
            k_send = kep1 - depth
            k_recv = kep1
          else
            exit second_pass
          end if
        end if
      end if
      !
      tagsend = 10*face + 2
      tagrecv = 10*face + 2
      call MPI_SENDRECV(field(i_send, j_send, k_send, f_send), 1, chunk &
                        , dest, tagsend &
                        , field(i_recv, j_recv, k_recv, f_recv), 1, chunk &
                        , dest, tagrecv &
                        , MPI_COMM_WORLD, status, ierr)
      exit second_pass
    end do second_pass
#endif /* MPI */
  END SUBROUTINE chain_sr_4d
  !
  !=======================================================================
  !
  SUBROUTINE mpifield_3d(field)
    REALNUM, INTENT(INOUT) :: field(isa:iea, jsa:jea, ksa:kea)
#ifdef MPI
    !
    INTEGER face
    !
    do face = 1, 3
      call chain_sr(field, face)
    end do
#endif /* MPI */
  END SUBROUTINE mpifield_3d
  !
  !=======================================================================
  !
  SUBROUTINE mpifield_4d(field)
    REALNUM, INTENT(INOUT) :: &
      field(isa:iea, jsa:jea, ksa:kea, number_of_tracer_fields)
#ifdef MPI
    !
    INTEGER face
    !
    do face = 1, 3
      call chain_sr(field, face)
    end do
#endif /* MPI */
  END SUBROUTINE mpifield_4d
  !
  !=======================================================================
  !
  SUBROUTINE consolidate
#ifdef MPI
    call mpifield_3d(v1)
    if (withv2) call mpifield_3d(v2)
    if (withv3) call mpifield_3d(v3)
    call mpifield_3d(d_)
    if (imhd > 0) then
      if (withbp) then
        call mpifield_3d(b1)
        call mpifield_3d(b2)
      end if
      if (withb3) call mpifield_3d(b3)
      if (withchem) then
        call mpifield_3d(ad_eta_array)
        call mpifield_3d(ohmic_eta_array)
        call mpifield_3d(hall_eta_array)
        call mpifield_3d(xr_array)
        call mpifield_3d(cr_array)
      end if
    end if
    if (withgp) call mpifield_3d(gp)
    if (ienergy == 1 .or. ienergy == 2) call mpifield_3d(e_)
    if (number_of_tracer_fields > 0) call mpifield_4d(tracerfields)
#endif /* MPI */
  END SUBROUTINE consolidate
  !
  !=======================================================================
  !
  SUBROUTINE mpi_subsection_type(datatype, arrayshape, subsectionshape)
#ifdef MPI
    USE mpi
#endif /* MPI */
    INTEGER, INTENT(OUT) :: datatype
    INTEGER, DIMENSION(:), INTENT(IN) :: arrayshape, subsectionshape

#ifdef MPI
    INTEGER, DIMENSION(size(arrayshape)) :: slice
    INTEGER :: ierr, sizeofreal
    INTEGER :: direction, rank, stride

    rank = size(arrayshape)

    call MPI_TYPE_EXTENT(MPI_FLOATMPI, sizeofreal, ierr)

    call MPI_TYPE_VECTOR(subsectionshape(1), 1, 1, MPI_FLOATMPI, slice(1), ierr)
    stride = sizeofreal

    do direction = 2, rank
      stride = stride*arrayshape(direction - 1)
      call MPI_TYPE_HVECTOR(subsectionshape(direction), 1, stride, &
                            slice(direction - 1), slice(direction), ierr)
    end do

    datatype = slice(rank)
    call MPI_TYPE_COMMIT(datatype, ierr)
#else /* MPI */
    datatype = 0
#endif /* MPI */
  END SUBROUTINE mpi_subsection_type
  !
  !=======================================================================
  !
  SUBROUTINE mpifield_e(field)
    REALNUM, INTENT(INOUT) :: field(isa_e:iea_e, jsa:jea, ksa:kea)
#ifdef MPI
    !
    INTEGER face
    !
    do face = 1, 3
      call chain_sr_e(field, face)
    end do
#endif /* MPI */
  END SUBROUTINE mpifield_e
  !
  !=======================================================================
  !
  SUBROUTINE chain_sr_e(field, face)
#ifdef MPI
    USE mpi
#endif /* MPI */
    INTEGER, INTENT(IN) ::  face
    REALNUM, INTENT(INOUT) :: field(isa_e:iea_e, jsa:jea, ksa:kea)
#ifdef MPI
    INTEGER chunk, dest, ierr, status(MPI_STATUS_SIZE)
    INTEGER i_send, j_send, k_send &
      , i_recv, j_recv, k_recv &
      , tagsend, tagrecv
    INTEGER parity
    INTEGER depth

    !  Check if grid direction is active
    if (iwnac(face) == 0) return

    depth = 1
    chunk = chunk_e(face)

    first_pass: do
      !
      !      First pass
      !
      i_send = isa_e
      j_send = jsa
      k_send = ksa
      i_recv = isa_e
      j_recv = jsa
      k_recv = ksa
      !
      if (face == 1) then
        parity = mod(node1, 2)
        if (parity == 1) then
          dest = nodedn1
          i_send = is_e
          i_recv = isg_e
        else
          if (.not. l_lastnode1) then
            dest = nodeup1
            i_send = ie_e
            i_recv = ieg_e
          else
            exit first_pass
          end if
        end if
      end if
      !
      if (face == 2) then
        parity = mod(node2, 2)
        if (parity == 1) then
          dest = nodedn2
          j_send = js
          j_recv = js - depth
        else
          if (.not. l_lastnode2) then
            dest = nodeup2
            j_send = jep1 - depth
            j_recv = jep1
          else
            exit first_pass
          end if
        end if
      end if
      !
      if (face == 3) then
        parity = mod(node3, 2)
        if (parity == 1) then
          dest = nodedn3
          k_send = ks
          k_recv = ks - depth
        else
          if (.not. l_lastnode3) then
            dest = nodeup3
            k_send = kep1 - depth
            k_recv = kep1
          else
            exit first_pass
          end if
        end if
      end if
      !
      tagsend = 30*face + 1
      tagrecv = 30*face + 1
      call MPI_SENDRECV(field(i_send, j_send, k_send), 1, chunk &
                        , dest, tagsend &
                        , field(i_recv, j_recv, k_recv), 1, chunk &
                        , dest, tagrecv &
                        , MPI_COMM_WORLD, status, ierr)
      exit first_pass
    end do first_pass

    second_pass: do
      !
      !      Second pass
      !
      i_send = isa_e
      j_send = jsa
      k_send = ksa
      i_recv = isa_e
      j_recv = jsa
      k_recv = ksa
      !
      if (face == 1) then
        parity = mod(node1, 2)
        if (parity == 0) then
          if (.not. l_firstnode1) then
            dest = nodedn1
            i_send = is_e
            i_recv = isg_e
          else
            exit second_pass
          end if
        else
          if (.not. l_lastnode1) then
            dest = nodeup1
            i_send = ie_e
            i_recv = ieg_e
          else
            exit second_pass
          end if
        end if
      end if
      !
      if (face == 2) then
        parity = mod(node2, 2)
        if (parity == 0) then
          if (.not. l_firstnode2) then
            dest = nodedn2
            j_send = js
            j_recv = js - depth
          else
            exit second_pass
          end if
        else
          if (.not. l_lastnode2) then
            dest = nodeup2
            j_send = jep1 - depth
            j_recv = jep1
          else
            exit second_pass
          end if
        end if
      end if
      !
      if (face == 3) then
        parity = mod(node3, 2)
        if (parity == 0) then
          if (.not. l_firstnode3) then
            dest = nodedn3
            k_send = ks
            k_recv = ks - depth
          else
            exit second_pass
          end if
        else
          if (.not. l_lastnode3) then
            dest = nodeup3
            k_send = kep1 - depth
            k_recv = kep1
          else
            exit second_pass
          end if
        end if
      end if
      !
      tagsend = 30*face + 2
      tagrecv = 30*face + 2
      call MPI_SENDRECV(field(i_send, j_send, k_send), 1, chunk &
                        , dest, tagsend &
                        , field(i_recv, j_recv, k_recv), 1, chunk &
                        , dest, tagrecv &
                        , MPI_COMM_WORLD, status, ierr)
      exit second_pass
    end do second_pass
#endif /* MPI */
  END SUBROUTINE chain_sr_e

END MODULE zeus_mpi
