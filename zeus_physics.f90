#include "macros"
#define hall_coefficient(x) (hall_coefficient_0)
MODULE zeus_physics
  USE zeus_memory
  USE zeus_interpolators
  USE zeus_io
  USE zeus_boundary
  USE zeus_variables
  USE zeus_chemistry
  IMPLICIT NONE
  PRIVATE
  PUBLIC :: artificial_viscosity, new_dt, source_terms
  PUBLIC :: hydro_transport, magnetic_transport, pdv
  PUBLIC :: ion_velocity_get, ion_velocity_release
  PUBLIC :: col_dens, xrays, cosmicrays
  PUBLIC :: pressure, chemistry

CONTAINS

  !** GROUP: PHYSICS *****************************************************
  !**                                                                   **
  !**                   G R O U P :   P H Y S I C S                     **
  !**                                                                   **
  !***********************************************************************

  !** SUBGROUP: WRAPPERS *************************************************
  !*                                                                     *
  !*                 S U B G R O U P : W R A P P E R S                   *
  !*                                                                     *
  !***********************************************************************
  !
  !=======================================================================
  !
  SUBROUTINE artificial_viscosity(dtqqi2)
    REALNUM, INTENT(OUT) :: dtqqi2
    call viscous(dtqqi2)
  END SUBROUTINE artificial_viscosity
  !
  !=======================================================================
  !
  SUBROUTINE new_dt(dtqqi2, initial_step)
    REALNUM, INTENT(IN) :: dtqqi2
    LOGICAL, INTENT(IN) :: initial_step
    call nudt(dtqqi2, initial_step)
  END SUBROUTINE new_dt
  !
  !=======================================================================
  !
  SUBROUTINE source_terms
    call srcstep
  END SUBROUTINE source_terms
  !
  !=======================================================================
  !
  SUBROUTINE hydro_transport
    call transprt
    if (withdensityfloor) then
      ! Density floor enforced *after* all transport is complete.
      ! Other choices are conceivable.
      call enforcedensityfloor
    end if
  END SUBROUTINE hydro_transport
  !
  !=======================================================================
  !
  SUBROUTINE magnetic_transport
    !  The magnetic field components are updated by CT which is a merger
    !  (as implemented by Jim Stone) of the method of characteristics and
    !  a variant of the constrained transport algorithm of Evans and Hawley.
    if (mhddimension == 2) then
      call ct_2d
    else
      call ct
    end if
  END SUBROUTINE magnetic_transport
  !
  !=======================================================================
  !
  SUBROUTINE pdv
    if (ienergy == 1 .and. itote == 0) call pdvterm
  END SUBROUTINE pdv

  !** SUBGROUP: NUMERICS *************************************************
  !*                                                                     *
  !*                 S U B G R O U P : N U M E R I C S                   *
  !*                                                                     *
  !***********************************************************************
  !
  !=======================================================================
  !
  SUBROUTINE nudt(dtqqi2, initial_step)
    !
    !    mln:zeus3d.nudt <------------------------ mhd time step calculation
    !                                                              may, 1986
    !
    !    written by: Mike Norman
    !    modified 1: May, 1986 by David Clarke; adapted for mhd.
    !    modified 2: October, 1987 by Mike Norman; reworked for covariant
    !                formulation.
    !    modified 3: June, 1988 by Jim Stone; incorporated into ZEUS2D.
    !    modified 4: February, 1990 by David Clarke; incorporated into
    !                ZEUS3D.
    !    modified 5: September, 1990 by David Clarke; moved magnetic fields
    !                to face-centres.
    !    Indexmax instead of ismax
    !
    !  PURPOSE:  Computes the new timestep for explicit calculations from
    !  the values of the field variables updated by the source and transport
    !  steps.
    !
    !  In explicit calculations, the timestep is given by:
    !
    !     dt = courno * sqrt [ min ( dtcs**2 + dtv1**2 + dtv2**2 + dtv3**2
    !                              + dtal**2 + dtqq**2 ) ]
    !
    !  where the variable names are described below.  The timestep can be
    !  reduced in size by any amount, but can be larger than the old timstep
    !  by no more than a factor of 1.26.
    !
    !  INPUT VARIABLES:
    !  dtqqi2    normally passed from VISCOUS, used to estimate the
    !            contribution of the artificial viscosity to dt
    !
    !  LOCAL VARIABLES:
    !
    !  i-sweep
    !  dr*i      inverse of distance across zone in 1-, 2-, or 3-direction
    !  drimax    maximum of dr1i, dr2i, and dr3i
    !  dt**i2i   square of the inverse time step of the ** physical process
    !            gathered during the i-sweep.  Possible values of ** are:
    !                cs = sound speed
    !                v1 = fluid motion in x1 direction
    !                v2 = fluid motion in x2 direction
    !                v3 = fluid motion in x2 direction
    !                al = Alfven speed
    !                qq = artificial viscosity
    !            The first five are vectors, while the last is a scalar
    !            which has been computed in ARTIFICIALVISC (passed in root).
    !  dttoi2i   sum of dt**i2i (without artificial viscosity contribution)
    !
    !  j-sweep
    !  dttoi2j   vector of maximum values of dttoi2i from each i-sweep.
    !            This vector is filled during a j-sweep.
    !  imaxj     i-index of each dttoi2j
    !  dt**i2j   values of above at zone i=imaxj
    !
    !  k-sweep
    !  dttoi2k   vector of maximum values of dttoi2j from each j-sweep.
    !  imaxk     i-index of each dttoi2k.
    !  jmaxk     j-index of each dttoi2k.
    !  dt**i2j   values of above at zone i=imaxk, j=jmaxk
    !
    !  grand maximum inverse time
    !  imax      i-index where dttoi2k is a maximum
    !  jmax      j-index where dttoi2k is a maximum
    !  kmax      k-index where dttoi2k is a maximum
    !  dt**      time step of the ** physical process at i=imax, j=jmax,
    !            and k=kmax
    !  dtnew     new timestep, which is limited to be no greater than 1.26
    !            times the previous timestep.
    !
    !-----------------------------------------------------------------------
    !
    USE problem_variables, only: epsilon, disk_angle_factor
#ifdef MPI
    USE mpi
#endif /* MPI */
    REALNUM, INTENT(IN) :: dtqqi2
    LOGICAL, INTENT(IN) :: initial_step
#ifdef MPI
    INTEGER ierr
    REALNUM buffer_send(6), buffer_recv(6)
    INTEGER, PARAMETER :: mpitag_nudt = 200
#endif /* MPI */
    !
    CHARACTER(LEN=20*messagelinelength) message

    CHARACTER(LEN=*), PARAMETER :: routinename = 'z_nudt'
    INTEGER dtnode, dtohmnode, dthallnode
    REALNUM dtcs, dtv1, dtv2, dtv3, dtal, dtqq, dtad, dthall
    REALNUM dtnew, dtohmnew, dthallnew, dthall_limit
    !
    INTEGER i, j, k
    INTEGER ip1, jp1, kp1
    INTEGER ::    imax, jmax, kmax
    INTEGER ::    imax_ohm, jmax_ohm, kmax_ohm
    INTEGER ::    imax_hall, jmax_hall, kmax_hall
    REALNUM q2, q3, q4
    REALNUM rho_ion, bsq, ad_coefficient, eta_ohm, eta_hal
    !
    REALNUM :: dr1i, dr2i, dr3i, drimax
    REALNUM :: dtcsi2, dtv1i2, dtv2i2, dtv3i2, dtali2, &
               dtadi2, dthalli2, dttoi2
    REALNUM :: q_ad_cap, q_alfven_cap, q_hall_cap

    REALNUM :: mass_gain_alfven_local, mass_gain_alfven_step
    REALNUM :: dtohmi
    REALNUM :: dtcsi2_max, dtv1i2_max, dtv2i2_max, dtv3i2_max, dtali2_max, &
               dtadi2_max, dthalli2_max, dttoi2_max
    REALNUM :: dtohmi_max
    !
    REALNUM, POINTER, SAVE, DIMENSION(:, :, :) :: cssq => null(), &
                                                  rho_old => null()
    INTEGER :: im1, jm1, km1

    INTEGER :: vd_startindex, vd_endindex

    REALNUM :: disk_angle, theta, f_theta
    !
    !-----------------------------------------------------------------------
    !
    !      Compute the specific energy density.
    !
    select case (ienergy)
    case (-1, 1, 2)
      call get_memory(cssq)
      call soundspeedsquare(cssq, 0)
    case (0)
      ! q5 = ciso**2
    end select

    dtcsi2 = zro
    dtv1i2 = zro
    dtv2i2 = zro
    dtv3i2 = zro
    dtali2 = zro
    dtadi2 = zro
    dthalli2 = zro
    dttoi2 = zro
    dtohmi = zro

    dtcsi2_max = zro
    dtv1i2_max = zro
    dtv2i2_max = zro
    dtv3i2_max = zro
    dtali2_max = zro
    dtadi2_max = zro
    if (hall_number_of_cycles == 0) then
      dthalli2_max = zro
    else
      dthalli2_max = -huge(one)
    end if
    if (hall_dt_floor > zro) then
      q_hall_cap = one/(hall_dt_floor**2)
    else
      q_hall_cap = huge(one)
    end if

    dttoi2_max = -huge(one)
    dtohmi_max = -huge(one)

    imax = isa - 1
    jmax = jsa - 1
    kmax = ksa - 1

    imax_ohm = isa - 1
    jmax_ohm = jsa - 1
    kmax_ohm = ksa - 1

    imax_hall = isa - 1
    jmax_hall = jsa - 1
    kmax_hall = ksa - 1

    if (alfven_dt_floor > zro) then
      q_alfven_cap = one/(alfven_dt_floor**2)
      if (imhd /= 0 .and. (alfven_dt_floor_conservative &
                           .or. alfven_dt_floor_vkill >= zro)) then
        call get_memory(rho_old)
        rho_old = d_
      end if
    else
      q_alfven_cap = huge(one)
    end if
    mass_gain_alfven_local = zro
    mass_gain_alfven_step = zro
    if (alfven_dt_floor_forcelimiter) then
      dfl = d_
    end if

    if (ad_dt_floor > zro) then
      q_ad_cap = one/(ad_dt_floor**2)
    else
      q_ad_cap = huge(one)
    end if

    !vd_temp=max(0,vd1_axiszero_start,vd2_axiszero
    if (lg_firstnode2) then
      vd_startindex = js + vd_axiszero_start
    else
      vd_startindex = js
    end if
    !vd_temp=max(0,vd1_axiszero_end,vd2_axiszero_end,vd3_axiszero_end)
    if (lg_lastnode2) then
      vd_endindex = je - vd_axiszero_end
    else
      vd_endindex = je
    end if

    disk_angle = atan(disk_angle_factor*epsilon)

    do k = ks, ke
      kp1 = k + kone
      q2 = dx3ai(k)*onek
      do j = js, je
        jp1 = j + jone
        q3 = dx2ai(j)*onej
        q4 = g32bi(j)*q2
        theta = x2a(j)
        f_theta = one
        if (theta < haf*pi - disk_angle) &
          f_theta = exp(-cos(theta + disk_angle)**2/(two*epsilon**2))
        if (theta > haf*pi + disk_angle) &
          f_theta = exp(-cos(theta - disk_angle)**2/(two*epsilon**2))
        do i = is, ie
          ip1 = i + ione
          dr1i = dx1ai(i)*onei
          dr2i = g2bi(i)*q3
          dr3i = g31bi(i)*q4
          drimax = max(dr1i, dr2i, dr3i)
          if (ienergy == 0) then
            dtcsi2 = (ciso*drimax)**2
          else
            dtcsi2 = cssq(i, j, k)*drimax**2
          end if
          dtv1i2 = ((v1(i, j, k) - vg1(i))*dr1i)**2
          if (withv2) then
            dtv2i2 = ((v2(i, j, k) - vg2(j))*dr2i)**2
          end if
          if (withv3) then
            dtv3i2 = ((v3(i, j, k) - vg3(k))*dr3i)**2
          end if
          if (imhd /= 0) then
            if (withbp) then
              if (withb3) then
                bsq = ((b1(i, j, k) + b1(ip1, j, k))**2 &
                       + (b2(i, j, k) + b2(i, jp1, k))**2 &
                       + (b3(i, j, k) + b3(i, j, kp1))**2) &
                      *quarter
              else
                bsq = ((b1(i, j, k) + b1(ip1, j, k))**2 &
                       + (b2(i, j, k) + b2(i, jp1, k))**2) &
                      *quarter
              end if
            else
              if (withb3) then
                bsq = b3(i, j, k)**2
              else
                bsq = zro
              end if
            end if
            dtali2 = bsq*drimax**2/d_(i, j, k)
            if (dtali2 > q_alfven_cap) then
              dtali2 = q_alfven_cap
              if (alfven_dt_floor_forcelimiter) then
                dfl(i, j, k) = bsq*drimax**2/q_alfven_cap
                ! CONSIDER RECOMPUTATION OF THIS VALUE IN THE ROUTINES
                ! THAT NEED IT.
                ! ADVANTAGES: more accurate. Better BCs. More robust
                ! in case of change of order of computation.
                ! DISADVANTAGES: more coding, slower computation
              else
                if (withfloormass) &
                  mass_gain_alfven_local = mass_gain_alfven_local + &
                                           (bsq*drimax**2/q_alfven_cap &
                                            - d_(i, j, k))* &
                                           dvl1a(i)*dvl2a(j)*dvl3a(k)
                d_(i, j, k) = bsq*drimax**2/q_alfven_cap
              end if
            end if

            ! ad_coefficient < ad_cfl*dxmin^2 / (4 dt_ad_floor B^2).
            ! with ad_coefficient=1.0/(ad_gamma*rho*rho_ion)
            if (with_ad_1f) then
              if (j >= vd_startindex .and. j <= vd_endindex) then
                if (withchem) then
                  ad_coefficient = ad_eta_array(i, j, k)
                else
                  rho_ion = ad_rhoi0*(d_(i, j, k)/ad_rhon0)**ad_alpha
                  ad_coefficient = one/(ad_gamma*d_(i, j, k)*rho_ion)
                end if
                dtadi2 = min((dtali2*four*f_theta*d_(i, j, k) &
                              *ad_coefficient)**2, q_ad_cap)
                !dtadi2=min( (dtali2*four*f_theta/(ad_gamma*rho_ion))**2,q_ad_cap)
              else
                dtadi2 = zro
              end if
            end if
            if (ihall > 0) then
              if (withchem) then
                eta_hal = hall_eta_array(i, j, k)
              else
                eta_hal = hall_coefficient(d_(i, j, k))
              end if
              dthalli2 = min(bsq*(four*pi*eta_hal*drimax**2)**2, q_hall_cap)
              if (hall_number_of_cycles /= 0) then
                if (dthalli2 > dthalli2_max) then
                  dthalli2_max = dthalli2
                  imax_hall = i
                  jmax_hall = j
                  kmax_hall = k
                end if
              end if
            end if

            if (iohmic /= 0) then
              if (withchem) then
                eta_ohm = ohmic_eta_array(i, j, k)
              else
                eta_ohm = ohmic_eta_factor
              end if
              select case (ohmic_exponent)
              case (1)
                dtohmi = &
                  eta_ohm*d_(i, j, k) &
                  *drimax**2
              case (0)
                dtohmi = &
                  eta_ohm &
                  *drimax**2
              case (100)
                dtohmi = &
                  eta_ohm* &
                  min(d_(i, j, k)/ohmic_rho_transition, one) &
                  *drimax**2
              case (200)
                dtohmi = eta_rb(i)*drimax**2
              end select
              if (dtohmi > dtohmi_max) then
                dtohmi_max = dtohmi
                imax_ohm = i
                jmax_ohm = j
                kmax_ohm = k
              end if
            end if
          end if
          if (hall_number_of_cycles /= 0) then
            dttoi2 = dtcsi2 + dtv1i2 + dtv2i2 + dtv3i2 + dtali2 + dtadi2
          else
            dttoi2 = dtcsi2 + dtv1i2 + dtv2i2 + dtv3i2 + dtali2 + dtadi2 &
                     + dthalli2
          end if
          if (dttoi2 > dttoi2_max) then
            dttoi2_max = dttoi2
            dtcsi2_max = dtcsi2
            dtv1i2_max = dtv1i2
            dtv2i2_max = dtv2i2
            dtv3i2_max = dtv3i2
            dtali2_max = dtali2
            dtadi2_max = dtadi2
            if (hall_number_of_cycles == 0) then
              dthalli2_max = dthalli2
            endif
            imax = i
            jmax = j
            kmax = k
          end if
        end do
      end do
    end do
    if (alfven_dt_floor_forcelimiter) then
      call boundary('dfl') ! CONSIDER NOT USING THIS FIELD
    else
      call boundary('d_')
    end if

    if (withfloormass .and. alfven_dt_floor > zro) then
#ifdef MPI
      call MPI_REDUCE(mass_gain_alfven_local, mass_gain_alfven_step, 1, &
                      MPI_FLOATMPI, MPI_SUM, rootnode, MPI_COMM_WORLD, ierr)
#else /* MPI */
      mass_gain_alfven_step = mass_gain_alfven_local
#endif /* MPI */
      if (node == rootnode) then
        mass_gain_alfven = mass_gain_alfven + mass_gain_alfven_step
      end if
    end if

    if (alfven_dt_floor > zro .and. imhd /= 0 &
        .and. alfven_dt_floor_conservative) then
      do k = ks, ke
        do j = js, je
          do i = is, ie
            im1 = i - ione
            v1(i, j, k) = v1(i, j, k)* &  ! Cell volume not taken into account here
                          (rho_old(i, j, k) + rho_old(im1, j, k))/ &
                          (d_(i, j, k) + d_(im1, j, k))
          end do
        end do
      end do
      call boundary('v1')
      do k = ks, ke
        do j = js, je
          jm1 = j - jone
          do i = is, ie
            v2(i, j, k) = v2(i, j, k)* &  ! Cell volume not taken into account here
                          (rho_old(i, j, k) + rho_old(i, jm1, k))/ &
                          (d_(i, j, k) + d_(i, jm1, k))
          end do
        end do
      end do
      call boundary('v2')
      do k = ks, ke
        km1 = k - kone
        do j = js, je
          do i = is, ie
            v3(i, j, k) = v3(i, j, k)* &  ! Cell volume not taken into account here
                          (rho_old(i, j, k) + rho_old(i, j, km1))/ &
                          (d_(i, j, k) + d_(i, j, km1))
          end do
        end do
      end do
      call boundary('v3')
    end if

    if (alfven_dt_floor > zro .and. imhd /= 0 &
        .and. alfven_dt_floor_vkill > zro) then
      do k = ks, ke
        do j = js, je
          do i = is, ie
            im1 = i - ione
            v1(i, j, k) = v1(i, j, k)* &  ! Cell volume not taken into account here
                          ((rho_old(i, j, k) + rho_old(im1, j, k))/ &
                           (d_(i, j, k) + d_(im1, j, k))) &
                          **(one/alfven_dt_floor_vkill)
          end do
        end do
      end do
      call boundary('v1')
      do k = ks, ke
        do j = js, je
          jm1 = j - jone
          do i = is, ie
            v2(i, j, k) = v2(i, j, k)* &  ! Cell volume not taken into account here
                          ((rho_old(i, j, k) + rho_old(i, jm1, k))/ &
                           (d_(i, j, k) + d_(i, jm1, k))) &
                          **(one/alfven_dt_floor_vkill)
          end do
        end do
      end do
      call boundary('v2')
      do k = ks, ke
        km1 = k - kone
        do j = js, je
          do i = is, ie
            v3(i, j, k) = v3(i, j, k)* &  ! Cell volume not taken into account here
                          ((rho_old(i, j, k) + rho_old(i, j, km1))/ &
                           (d_(i, j, k) + d_(i, j, km1))) &
                          **(one/alfven_dt_floor_vkill)
          end do
        end do
      end do
      call boundary('v3')
    end if

    if (alfven_dt_floor > zro .and. imhd /= 0 &
        .and. alfven_dt_floor_vkill == zro) then
      do k = ks, ke
        do j = js, je
          do i = is, ie
            if (rho_old(i, j, k) /= d_(i, j, k)) then
              v1(i, j, k) = zro
              v1(i + ione, j, k) = zro
              if (withv2) then
                v2(i, j, k) = zro
                v2(i, j + jone, k) = zro
              end if
              if (withv3) then
                v3(i, j, k) = zro
                v3(i, j, k + kone) = zro
              end if
            end if
          end do
        end do
      end do
      call boundary('v1')
      call boundary('v2')
      call boundary('v3')
    end if

    if (alfven_dt_floor > zro) then
    if (imhd /= 0 .and. (alfven_dt_floor_conservative &
                         .or. alfven_dt_floor_vkill >= zro)) then
      call release_memory(rho_old)
    end if
    end if

    if (ienergy /= 0) then
      call writedata(cssq, routinename//'_cssq', 4)
      call release_memory(cssq)
    end if

    dtcs = one/(sqrt(dtcsi2_max) + verysmall)
    dtv1 = one/(sqrt(dtv1i2_max) + verysmall)
    dtv2 = one/(sqrt(dtv2i2_max) + verysmall)
    dtv3 = one/(sqrt(dtv3i2_max) + verysmall)
    dtal = one/(sqrt(dtali2_max) + verysmall)
    dtad = one/(sqrt(dtadi2_max) + verysmall)
    dthall = one/(sqrt(max(dthalli2_max, zro)) + verysmall)
    dtqq = one/(sqrt(dtqqi2) + verysmall)

    dtnew = courno/(sqrt(dttoi2_max + dtqqi2) + verysmall)
    dtohmnew = courno_ohmic/(max(dtohmi_max, zro) + verysmall)
    if (hall_number_of_cycles /= 0) then
      dthallnew = courno_hall/(sqrt(max(dthalli2_max, zro)) + verysmall)
    end if

#ifdef MPI
    ! Find the smallest value of the raw dt amd dtohm, and the node where it has been found
    buffer_send(1) = dtnew
    buffer_send(2) = node
    buffer_send(3) = dtohmnew
    buffer_send(4) = node
    buffer_send(5) = dthallnew
    buffer_send(6) = node
    call MPI_REDUCE(buffer_send, buffer_recv, 3, &
                    MPI_2FLOATMPI, MPI_MINLOC, rootnode, MPI_COMM_WORLD, ierr)
    if (node == rootnode) then
      dtnew = buffer_recv(1)
      dtnode = nint(buffer_recv(2))
      dtohmnew = buffer_recv(3)
      dtohmnode = nint(buffer_recv(4))
      dthallnew = buffer_recv(5)
      dthallnode = nint(buffer_recv(6))
    end if
#else /* MPI */
    dtnode = node
    dtohmnode = node
#endif /* MPI */

    if (node == rootnode) then
      if (iohmic == 0) then
        dtohmnew = huge(dtohmnew)
      end if
      if (ihall == 0) then
        dthallnew = huge(dthallnew)
      end if
      if (ihall == 0 .or. hall_number_of_cycles == 0) then
        dthall_limit = huge(dthall_limit)
      else
        dthall_limit = dthallnew*hall_number_of_cycles
      end if
      if (initial_step) then
        ! Set the expected minimum timestep, used in NUDT to detect hot zones
        dtmin = dtminrat*min(dtnew, dtohmnew*max_ohmic_steps, dthall_limit)
        ! Set the effectively used initial timestep, and ohmic timestep
        dt = dtratini*min(dtnew, dtohmnew*max_ohmic_steps, dthall_limit)
      else
        dt = min(dtnew, max_dt_increase*dt)
        dt = min(dt, dtohmnew*max_ohmic_steps, dthall_limit)
      end if
      if (iohmic /= 0) then
        nsubcycles_ohm = ceiling(dt/dtohmnew)
        dtohm = dt/nsubcycles_ohm
      else
        dtohm = dtohmnew
      end if
    end if

#ifdef MPI
    if (node == rootnode) then
      buffer_send(1) = dt
      buffer_send(2) = dtnode
      buffer_send(3) = dtohm
      buffer_send(4) = dtohmnode
      buffer_send(5) = nsubcycles_ohm
    end if

    call MPI_BCAST(buffer_send, 5, MPI_FLOATMPI, rootnode, MPI_COMM_WORLD, ierr)

    dt = buffer_send(1)
    dtnode = buffer_send(2)
    dtohm = buffer_send(3)
    dtohmnode = buffer_send(4)
    nsubcycles_ohm = buffer_send(5)

#endif /* MPI */

    if (debug >= 2) then
      write (message, '(a,a,a,4i5)') 'I  ', ntimestepname, ' ', imax, jmax, &
        kmax, dtnode
      call writemessage(routinename, message)
      write (message, '(a,a,a,3es15.8)') 'A  ', ntimestepname, ' ', &
        dtqqi2, dtali2_max, dtcsi2_max
      call writemessage(routinename, message)
      write (message, '(a,a,a,3es15.8)') 'B  ', ntimestepname, ' ', &
        dtv1i2_max, dtv2i2_max, dtv3i2_max
      call writemessage(routinename, message)
      write (message, '(a,a,a,3es15.8)') 'C  ', ntimestepname, ' ', &
        dttoi2_max, dtadi2_max, dthalli2_max
      call writemessage(routinename, message)
      write (message, '(a,a,a,3es15.8)') 'D  ', ntimestepname, ' ', &
        time, dtnew, dt
      call writemessage(routinename, message)
      write (message, '(a,a,a,3es15.8)') 'E  ', ntimestepname, ' ', &
        dtohmi_max, dtohmnew, dtohm
      call writemessage(routinename, message)
      write (message, '(a,a,a,5i5)') 'F  ', ntimestepname, ' ', &
        imax_ohm, jmax_ohm, kmax_ohm, dtohmnode, nsubcycles_ohm
      call writemessage(routinename, message)
      call writemessage(routinename, '')
    end if

    if (node == dtnode) then
      if (check_timestep(iotimestepfrequency)) then
        write (message, '(a," ",2es12.5,3i5,3(" ",g9.2))') &
          ntimestepname, time, dt, &
          imax + node1*inac, jmax + node2*jnac, kmax + node3*knac, &
          x1a(imax), x2a(jmax), x3a(kmax)
        call writemessage('z_times', message)
        write (message, '(9(a,es10.3))') &
          ' cs', dtcs, &
          ' v1', dtv1, &
          ' v2', dtv2, &
          ' v3', dtv3, &
          ' al', dtal, &
          ' ad', dtad, &
          ' ha', dthall, &
          ' oh', dtohm, &
          ' qq', dtqq
        call writemessage('z_times', message)
      end if
    end if
    if (iohmic /= 0) then
      if (node == dtohmnode) then
        if (check_timestep(iotimestepfrequency)) then
          write (message, '(a," ",3es12.5,3i5,3(" ",g9.2))') ntimestepname, &
            time, dtohm, dt, imax_ohm + node1*inac, jmax_ohm + node2*jnac, &
            kmax_ohm + node3*knac, x1a(imax_ohm), x2a(jmax_ohm), x3a(kmax_ohm)
          call writemessage('z_times_ohm', message)
          write (message, '(a,es10.3,a,i5)') &
            ' oh', dtohmnew, &
            ' ns', nsubcycles_ohm
          call writemessage('z_times_ohm', message)
        end if
      end if
    endif
    !
    if (dt >= dtmin) then
      if (dt >= 1.5_pk*dtmin .and. hotzonereset) then
        hotzonewarn = 0
      end if
    else
      write (message, &
           "('NUDT    : **** WARNING **** Hot zone at i=',i4,' j=',i4,' k=',i4, &
          & ' (dt < dtmin)')") imax, jmax, kmax
      call warnoutput(routinename, message, hotzonewarn)
      write (message, &
           "('NUDT    : dt   = ',1p,e12.5,', dtmin= ',1e12.5, &
         & ', ntimestep=',a,',')") dt, dtmin, ntimestepname
      call threeoutput(routinename, message)
      write (message, &
           "('NUDT    : dtcs = ',1e12.5,', dtv1 = ',1e12.5, &
         & ', dtv2 = ',1e12.5,',')") dtcs, dtv1, dtv2
      call threeoutput(routinename, message)
      write (message, &
           "('NUDT    : dtv3 = ',1e12.5,', dtqq = ',1e12.5, &
         & ', dtal = ',1e12.5,'.')") dtv3, dtqq, dtal
      call threeoutput(routinename, message)
      call writemessage(routinename//'_warning', 1)
      call writedata(routinename//'_warning', 1)
    end if
    !
  END SUBROUTINE nudt
  !
  !=======================================================================
  !
  SUBROUTINE viscous(dtqqi2)
    !
    !    mln:zeus3d.viscous <------------- artificial viscosity source terms
    !                                                        ?????????, 19??
    !
    !    written by: Mike Norman
    !    modified 1: June, 1988 by Jim Stone; incorporated into ZEUS2D
    !    modified 2: February, 1990 by David Clarke; incorporated into
    !                ZEUS3D
    !    modified 3: June, 1992 by David Clarke; expunged "ISMIN", thereby
    !                decreasing execution time by 30%.
    !    modified 4: Oct., 1994 by Robert Fiedler to run in parallel on SGIs
    !
    !  PURPOSE: Computes the artificial viscosity source terms in the
    !  momentum and energy equations.  i.e., it computes
    !
    !             dv / dt = -DIV(Q) / rho             for v1, v2, and v3
    !      and    de / dt = -Q * delta(v) / delta(x)
    !
    !  This routine uses the von Neumann-Richtmyer form of the artificial
    !  viscosity.  This means that geometric terms are not included in
    !  DIV(Q).   In addition, a linear viscosity can be added (controlled
    !  by the value of qlin) to damp oscillations in stagnant flow.
    !
    !  OUTPUT VARIABLES:
    !    dtqqi2     used by nudt to estimate the artificial viscosity
    !               contribution to dt
    !
    !  LOCAL VARIABLES:
    !    qq         diagonal elements of viscous tensor.  Thus, this is a
    !               linear treatment of the artificial viscosity.  A full
    !               tensor treatment is possible although somewhat more
    !               complicated (see TAV in ZEUS2D).
    !    dvelb      v1(i+1,j,k)-v1(i,j,k) for i-sweep
    !               v2(i,j+1,k)-v2(i,j,k) for j-sweep
    !               v3(i,j,k+1)-v3(i,j,k) for k-sweep
    !    dvela      min ( zro, dvelb ) - ensures that only compressional
    !               waves (shocks) are affected.
    !    dv1dx1     min ( delta(v1) / delta(x1) )
    !    dv2dx2     min ( delta(v2) / delta(x2) )
    !    dv3dx3     min ( delta(v3) / delta(x3) )
    !
    !-----------------------------------------------------------------------
    !
#ifdef MPI
    USE mpi
#endif /* MPI */
    REALNUM, INTENT(OUT) :: dtqqi2
#ifdef MPI
    INTEGER ierr
#endif /* MPI */
    !
    CHARACTER(LEN=messagelinelength) message
    CHARACTER(LEN=*), PARAMETER :: routinename = 'z_viscous'
    !
    INTEGER i, j, k
    REALNUM dv1dx1, dv2dx2, dv3dx3, q1, q3, g3i, q4, q5
    REALNUM qqs, qqsm
    REALNUM dvelas, dvelasm, dvelbs, dvelbsm, dvdxmin
    !
    REALNUM, POINTER, SAVE :: cs(:, :, :) => null()
    REALNUM, POINTER, SAVE :: v_old(:, :, :) => null()
    REALNUM, POINTER, SAVE :: dvdx(:, :, :) => null()

    call get_memory(v_old)
    call get_memory(dvdx)

    if (ienergy > 0) call writedata(e_, routinename//'_0_e_', 4)

    !
    !-----------------------------------------------------------------------
    !
    !      Evaluate sound speeds for linear viscosity term.
    !
    if (ienergy == 0) then
      q4 = zro
      q5 = dt*qlin*ciso
    else
      if (qlin > zro) then
        call get_memory(cs)
        call soundspeed(cs, 0)
      end if
      q4 = dt*qlin
      q5 = zro
    end if
    !
    dv1dx1 = zro
    dv2dx2 = zro
    dv3dx3 = zro
    !
    !      Start von Neumann-Richtmyer artificial viscosity update,
    !  including both linear and quadratic terms.
    !
    q1 = dt*qcon
    !
    !      Do i sweeps in v1 (except for problems with X1-symmetry).  The
    !  evaluation of the viscous timestep is done by finding the minimum
    !  timestep over each sweep of the outer loop, then finding the minimum
    !  over all sweeps.  This was done in Zeus3D to aid the EDITOR autotasking
    !  process; probably irrelevant now that EDITOR is not used anymore.
    !
    if (inac /= 0) then
      if (qlin > zro) then
        do k = ks, ke
          do j = js, je
            do i = ism1, iep1
              v_old(i, j, k) = v1(i, j, k)
            end do
            do i = is, ie
              dvelbsm = v_old(i, j, k) - v_old(i - 1, j, k)
              dvelbs = v_old(i + 1, j, k) - v_old(i, j, k)
              dvelasm = min(zro, dvelbsm)
              dvelas = min(zro, dvelbs)
              if (ienergy == 0) then
                qqsm = d_(i - 1, j, k)*dvelbsm &
                       *(q1*dvelasm &
                         - (q4*ciso + q5))
                qqs = d_(i, j, k)*dvelbs &
                      *(q1*dvelas &
                        - (q4*ciso + q5))
              else
                qqsm = d_(i - 1, j, k)*dvelbsm &
                       *(q1*dvelasm &
                         - (q4*cs(i - 1, j, k) + q5))
                qqs = d_(i, j, k)*dvelbs &
                      *(q1*dvelas &
                        - (q4*cs(i, j, k) + q5))
              end if
              q3 = dvelbs*dx1ai(i)
              if (qqs == zro) q3 = zro
              dvdx(i, j, k) = min(zro, q3)
              if (ienergy == 1) e_(i, j, k) = e_(i, j, k) - q3*qqs
              v1(i, j, k) = v1(i, j, k) &
                            - (qqs - qqsm)*dx1bi(i) &
                            *two/(d_(i - 1, j, k) + d_(i, j, k))
            end do
          end do
        end do
      else
        do k = ks, ke
          do j = js, je
            do i = ism1, iep1
              v_old(i, j, k) = v1(i, j, k)
            end do
            do i = is, ie
              dvelasm = min(zro, v_old(i, j, k) - v_old(i - 1, j, k))
              dvelas = min(zro, v_old(i + 1, j, k) - v_old(i, j, k))
              qqsm = q1*d_(i - 1, j, k)*dvelasm*dvelasm
              qqs = q1*d_(i, j, k)*dvelas*dvelas
              q3 = dvelas*dx1ai(i)
              dvdx(i, j, k) = min(zro, q3)
              if (ienergy == 1) e_(i, j, k) = e_(i, j, k) - q3*qqs
              v1(i, j, k) = v1(i, j, k) &
                            - (qqs - qqsm)*dx1bi(i) &
                            *two/(d_(i - 1, j, k) + d_(i, j, k))
            end do
          end do
        end do
      end if
    else
      do k = ks, ke
        do j = js, je
          dvdx(is, j, k) = zro
        end do
      end do
    end if

    call writedata(dvdx, routinename//'_1_dvdx', 4)
    call writedata(v1, routinename//'_1_v1', 4)
    if (ienergy > 0) call writedata(e_, routinename//'_1_e_', 4)

    !
    !      Do j sweeps in v2 if needed
    !
    if (jnac /= 0 .and. withv2) then
      if (qlin > zro) then
        do k = ks, ke
          do j = jsm1, jep1
            do i = is, ie
              v_old(i, j, k) = v2(i, j, k)
            end do
          end do
        end do
        do k = ks, ke
          do j = js, je
            do i = is, ie
              dvelbsm = v_old(i, j, k) - v_old(i, j - 1, k)
              dvelbs = v_old(i, j + 1, k) - v_old(i, j, k)
              dvelasm = min(zro, dvelbsm)
              dvelas = min(zro, dvelbs)
              if (ienergy == 0) then
                qqsm = d_(i, j - 1, k)*dvelbsm &
                       *(q1*dvelasm &
                         - (q4*ciso + q5))
                qqs = d_(i, j, k)*dvelbs &
                      *(q1*dvelas &
                        - (q4*ciso + q5))
              else
                qqsm = d_(i, j - 1, k)*dvelbsm &
                       *(q1*dvelasm &
                         - (q4*cs(i, j - 1, k) + q5))
                qqs = d_(i, j, k)*dvelbs &
                      *(q1*dvelas &
                        - (q4*cs(i, j, k) + q5))
              end if
              q3 = dvelbs*dx2ai(j)*g2bi(i)
              if (qqs == zro) q3 = zro
              dvdx(i, j, k) = min(dvdx(i, j, k), q3)
              if (ienergy == 1) e_(i, j, k) = e_(i, j, k) - q3*qqs
              v2(i, j, k) = v2(i, j, k) &
                            - (qqs - qqsm)*dx2bi(j)*g2bi(i) &
                            *two/(d_(i, j - 1, k) + d_(i, j, k))
            end do
          end do
        end do
      else
        do k = ks, ke
          do j = jsm1, jep1
            do i = is, ie
              v_old(i, j, k) = v2(i, j, k)
            end do
          end do
        end do
        do k = ks, ke
          do j = js, je
            do i = is, ie
              dvelasm = min(zro, v_old(i, j, k) - v_old(i, j - 1, k))
              dvelas = min(zro, v_old(i, j + 1, k) - v_old(i, j, k))
              qqsm = q1*d_(i, j - 1, k)*dvelasm*dvelasm
              qqs = q1*d_(i, j, k)*dvelas*dvelas
              q3 = dvelas*dx2ai(j)*g2bi(i)
              dvdx(i, j, k) = min(dvdx(i, j, k), q3)
              if (ienergy == 1) e_(i, j, k) = e_(i, j, k) - q3*qqs
              v2(i, j, k) = v2(i, j, k) &
                            - (qqs - qqsm)*dx2bi(j)*g2bi(i) &
                            *two/(d_(i, j - 1, k) + d_(i, j, k))
            end do
          end do
        end do
      end if
    end if

    call writedata(dvdx, routinename//'_2_dvdx', 4)
    call writedata(v1, routinename//'_2_v2', 4)
    if (ienergy > 0) call writedata(e_, routinename//'_2_e_', 4)

    !
    !      Do k sweeps in v3 if needed
    !
    if (knac /= 0 .and. withv3) then
      if (qlin > zro) then
        do k = ksm1, kep1
          do j = js, je
            do i = is, ie
              v_old(i, j, k) = v3(i, j, k)
            end do
          end do
        end do
        do k = ks, ke
          do j = js, je
            do i = is, ie
              g3i = g31bi(i)*g32bi(j)
              dvelbsm = v_old(i, j, k) - v_old(i, j, k - 1)
              dvelbs = v_old(i, j, k + 1) - v_old(i, j, k)
              dvelasm = min(zro, dvelbsm)
              dvelas = min(zro, dvelbs)
              if (ienergy == 0) then
                qqsm = d_(i, j, k - 1)*dvelbsm &
                       *(q1*dvelasm &
                         - (q4*ciso + q5))
                qqs = d_(i, j, k)*dvelbs &
                      *(q1*dvelas &
                        - (q4*ciso + q5))
              else
                qqsm = d_(i, j, k - 1)*dvelbsm &
                       *(q1*dvelasm &
                         - (q4*cs(i, j, k - 1) + q5))
                qqs = d_(i, j, k)*dvelbs &
                      *(q1*dvelas &
                        - (q4*cs(i, j, k) + q5))
              end if
              q3 = dvelbs*dx3ai(k)*g3i
              if (qqs == zro) q3 = zro
              dvdx(i, j, k) = min(dvdx(i, j, k), q3)
              if (ienergy == 1) e_(i, j, k) = e_(i, j, k) - q3*qqs
              v3(i, j, k) = v3(i, j, k) &
                            - (qqs - qqsm)*dx3bi(k)*g3i &
                            *two/(d_(i, j, k - 1) + d_(i, j, k))
            end do
          end do
        end do
      else
        do k = ksm1, kep1
          do j = js, je
            do i = is, ie
              v_old(i, j, k) = v3(i, j, k)
            end do
          end do
        end do
        do k = ks, ke
          do j = js, je
            do i = is, ie
              g3i = g31bi(i)*g32bi(j)
              dvelasm = min(zro, v_old(i, j, k) - v_old(i, j, k - 1))
              dvelas = min(zro, v_old(i, j, k + 1) - v_old(i, j, k))
              qqsm = q1*d_(i, j, k - 1)*dvelasm*dvelasm
              qqs = q1*d_(i, j, k)*dvelas*dvelas
              q3 = dvelas*dx3ai(k)*g3i
              dvdx(i, j, k) = min(dvdx(i, j, k), q3)
              if (ienergy == 1) e_(i, j, k) = e_(i, j, k) - q3*qqs
              v3(i, j, k) = v3(i, j, k) &
                            - (qqs - qqsm)*dx3bi(k)*g3i &
                            *two/(d_(i, j, k - 1) + d_(i, j, k))
            end do
          end do
        end do
      end if
    end if
    call writedata(dvdx, routinename//'_3_dvdx', 4)
    call writedata(v1, routinename//'_3_v3', 4)
    if (ienergy > 0) call writedata(e_, routinename//'_3_e_', 4)

    !
    !      Compute viscous timestep.  Note that the minimum dv/dx is found
    !  since it is less than 0.  Thus the minimum dv/dx gives the maximum
    !  absolute value.
    !
    call min3dloc(dvdx, is, js, ks, ie, je, ke, dvdxmin, i, j, k)

    if (debug >= 4) then
      write (message, '(a,a,a,1es15.8)') 'A  ', ntimestepname, ' ', dvdxmin
      call writemessage(routinename, message)
    end if
#ifdef MPI
    dtqqi2 = dvdxmin ! dtqqi2 is here a temporary variable
    call MPI_ALLREDUCE(dtqqi2, dvdxmin, 1, MPI_FLOATMPI, MPI_MIN, &
                       MPI_COMM_WORLD, ierr)
#endif /* MPI */
    dtqqi2 = (four*qcon*dvdxmin)**2

    if (debug >= 4) then
      write (message, '(a,a,a,2es15.8)') 'B  ', ntimestepname, ' ', &
        dvdxmin, dtqqi2
      call writemessage(routinename, message)
    end if

    call writedata(v1, routinename//'_4_v1', 3)
    if (withv2) call writedata(v2, routinename//'_4_v2', 3)
    if (withv3) call writedata(v3, routinename//'_4_v3', 3)
    if (ienergy > 0) call writedata(e_, routinename//'_4_e_', 3)

    call boundary('v1')
    call boundary('v2')
    call boundary('v3')
    if (ienergy > 0) call boundary('e_')  ! Perhaps redundant

    call writedata(v1, routinename//'_5_v1', 3)
    if (withv2) call writedata(v2, routinename//'_5_v2', 3)
    if (withv3) call writedata(v3, routinename//'_5_v3', 3)
    if (ienergy > 0) call writedata(e_, routinename//'_5_e_', 3)

    if (ienergy /= 0) then
      if (qlin > zro) then
        call release_memory(cs)
      end if
    end if

    call release_memory(dvdx)
    call release_memory(v_old)
  END SUBROUTINE viscous
  !
  !=======================================================================
  !
  SUBROUTINE min3d(qty, i1, j1, k1, i2, j2, k2, qmin)
    REALNUM, INTENT(IN) :: qty(:, :, :)
    REALNUM, INTENT(OUT) :: qmin
    INTEGER, INTENT(IN) :: i1, j1, k1, i2, j2, k2
    !
    INTEGER i, j, k
    !
    !-----------------------------------------------------------------------
    !
    qmin = verylarge
    do k = k1, k2
      do j = j1, j2
        do i = i1, i2
          qmin = min(qmin, qty(i, j, k))
        end do
      end do
    end do
  END SUBROUTINE min3d
  !
  !=======================================================================
  !
  SUBROUTINE min3dloc(qty, i1, j1, k1, i2, j2, k2, qmin, im, jm, km)
    REALNUM, INTENT(IN) :: qty(:, :, :)
    REALNUM, INTENT(OUT) :: qmin
    INTEGER, INTENT(OUT) :: im, jm, km
    INTEGER, INTENT(IN) :: i1, j1, k1, i2, j2, k2
    !
    INTEGER i, j, k
    !
    !-----------------------------------------------------------------------
    !
    qmin = verylarge
    im = i1 - 1
    jm = j1 - 1
    km = k1 - 1
    do k = k1, k2
      do j = j1, j2
        do i = i1, i2
          if (qmin > qty(i, j, k)) then
            qmin = qty(i, j, k)
            im = i
            jm = j
            km = k
          end if
        end do
      end do
    end do
  END SUBROUTINE min3dloc

  !** SUBGROUP: MAGNETIC TRANSPORT ***************************************
  !*                                                                     *
  !*       S U B G R O U P : M A G N E T I C    T R A N S P O R T        *
  !*                                                                     *
  !***********************************************************************
  !
  !=======================================================================
  !
  SUBROUTINE ct
    ! RFK, 2007/11/14
    !   Added two new ways of calling the EMF boundary routines.
    ! RFK, 2004/01/04:
    ! A few changes useful for certain axisymmetric
    ! problems have been incorporated into the code.
    ! The variable ctboxtype is used to control these changes.
    ! To undo the changes, set ctboxtype to 0;
    ! consider undoing them completely in future versions.
    !
    !    dac:zeus3d.ct <-------- updates B-field using constrained transport
    !    from jms:zeus2d.ct                                    october, 1989
    !
    !    written by: David Clarke
    !    modified 1: May, 1990 by David Clarke; reworked to call the new
    !                interpolation routines which need only be called once.
    !    modified 2: August, 1990 by David Clarke; moved magnetic fields to
    !                the face-centres (cospatial with the velocities).
    !                Implemented a method of characteristics for evaluating
    !                the emf's.  The transverse Lorentz accelerations are
    !                now applied to the velocities during this step.
    !    modified 3: November, 1990 by David Clarke; resurrected non-MoC
    !                algorithm for evaluating emfs.  Added EDITOR alias MOC
    !                which needs to be defined if MoC is to be used.
    !    modified 4: June, 1992 by David Clarke; reworked singularity
    !                formalism.
    !    modified 5: December, 1992 by David Clarke (as suggested by John
    !                Hawley); split velocity update from emf computation.
    !                Velocities are now Lorentz-accelerated with *old*
    !                magnetic field values. emf's are then estimated with
    !                Lorentz-updated velocities.
    !
    !  PURPOSE:  This routine transports the three components of the
    !  magnetic field using a variation of the non-relativistic Constrained
    !  Transport scheme (CT), developed by Chuck Evans and John Hawley (Ap.
    !  J., 342, 700).  In this implementation, the magnetic field components
    !  are face-centred, cospatial with the velocities and are updated using
    !  edge-centred emf's which are cospatial with the current densities.
    !
    !
    !  The emf's may be evaluated by MOCEMFS in which the velocities and
    !  magnetic field components required to compute the emf's are estimated
    !  using the Method of Characteristics (MoC) algorithm developed by Jim
    !  Stone et al. for 2-D.  For self-consistency, the transverse Lorentz
    !  accelerations have been removed from STV* and are applied to the
    !  velocities in LORENTZ using the pre-updated magnetic fields.  By
    !  experimentation, it has been determined that performing the Lorentz
    !  update after the magnetic field update is unstable.
    !
    !
    !  LOCAL VARIABLES:
    !    emf1      emf along the 2-3 edges of the grid (= v2*b3 - v3*b2)
    !    emf2      emf along the 3-1 edges of the grid (= v3*b1 - v1*b3)
    !    emf3      emf along the 1-2 edges of the grid (= v1*b2 - v2*b1)
    !    srd[n]    sqrt of spatially averaged density at [n]-face n=1,2,3
    !
    !-----------------------------------------------------------------------
    !
    CHARACTER(LEN=*), PARAMETER :: routinename = 'z_ct'
    !
    REALNUM, POINTER, SAVE, DIMENSION(:, :, :) :: emf1 => null(), &
                                                  emf2 => null(), &
                                                  emf3 => null()
    REALNUM, POINTER, SAVE, DIMENSION(:, :, :) :: j1 => null(), j2 => null(), &
                                                  j3 => null()
    REALNUM, POINTER, SAVE, DIMENSION(:, :, :) :: srd1 => null(), &
                                                  srd2 => null(), &
                                                  srd3 => null()
    REALNUM, POINTER, SAVE, DIMENSION(:, :, :) :: vi1 => null(), &
                                                  vi2 => null(), &
                                                  vi3 => null()
    REALNUM, POINTER, SAVE, DIMENSION(:, :, :) :: baux1 => null(), &
                                                  baux2 => null(), &
                                                  baux3 => null()

    INTEGER :: subcycle_count
    LOGICAL :: heating_not_done
    CHARACTER(LEN=messagelinelength) message

    LOGICAL :: termination_flag
    INTEGER :: ncycle
    REALNUM :: dthall, time_cycle

    if (imhd <= 0) return

    !-----------------------------------------------------------------------
    !      Compute face-centered averages of density (if needed).
    call srds_get(srd1, srd2, srd3)

    !-----------------------------------------------------------------------
    !     Compute ion velocity (if needed); otherwise, equate it to the neutral speed
    call ion_velocity_get(vi1, vi2, vi3)  ! consider optimizing this call away
    ! Debug output
    call writedata(vi1, routinename//'_1_vi1', 4)
    call writedata(vi2, routinename//'_1_vi2', 4)
    call writedata(vi3, routinename//'_1_vi3', 4)

    if (with_ad_1f .and. ienergy == 1) then
      call ad_heating(vi1, vi2, vi3)
    end if
    !
    !-----------------------------------------------------------------------
    !-------------------------> Update velocities <-------------------------
    !-----------------------------------------------------------------------
    !
    !      Compute the transverse Lorentz forces and, if needed,
    !  accelerate the fluid velocities.
    !      Calculation can be based either on ionic or neutral speeds,
    !  probably as an option
    !
    if (with_ad_1f_vion_force) then
      call lorentz(srd1, srd2, srd3, vi1, vi2, vi3)
    else
      call lorentz(srd1, srd2, srd3, v1, v2, v3)
    end if
    ! Recompute ion speeds: consider optimizing this call away
    if (with_ad_1f) then
      call calculate_ion_velocity(vi1, vi2, vi3)
      ! Debug output
      call writedata(vi1, routinename//'_2_vi1', 4)
      call writedata(vi2, routinename//'_2_vi2', 4)
      call writedata(vi3, routinename//'_2_vi3', 4)
    end if
    !call ion_velocity_get(vi1,vi2,vi3)

    if (ihall == 1) then
      ! Modify the ion velocity by adding to it the Hall drift.
      ! This makes now the name ion velocity a misnomer,
      ! which will have to be corrected.  What's really meant is the transport
      ! velocity of the field, equal to v_neutral + v_iondrift + v_Halldrift.
      call hall_drift(vi1, vi2, vi3)
    end if

    !
    !-----------------------------------------------------------------------
    !--------------------------------> VixB <-------------------------------
    !-----------------------------------------------------------------------
    !
    ! Calculate VixB in the computational volume
    !
    call get_memory(emf1)
    call get_memory(emf2)
    call get_memory(emf3)
    if (imhd == 3) then
      call hsmoc(emf1, emf2, emf3, srd1, srd2, srd3, vi1, vi2, vi3)
    end if
    if (imhd == 2) then
      call mocemfs(emf1, emf2, emf3, srd1, srd2, srd3, vi1, vi2, vi3)
    end if
    if (imhd == 1) then
      call emfs(emf1, emf2, emf3, vi1, vi2, vi3)
    end if

    ! Marke arrays as unused, if necessary
    call srds_release(srd1, srd2, srd3)
    call ion_velocity_release(vi1, vi2, vi3)

    ! Debug output
    call writedata(emf1, routinename//'_1_emf1', 4)
    call writedata(emf2, routinename//'_1_emf2', 4)
    call writedata(emf3, routinename//'_1_emf3', 4)
    !
    !-----------------------------------------------------------------------
    !-----------------------------> currents <------------------------------
    !-----------------------------------------------------------------------
    ! For the Ohmic cases: allocate memory for the currents.
    ! Calculate currents in the computational volume for the unsplit method
    !
    if (iohmic /= 0) then
      call get_memory(j1)
      call get_memory(j2)
      call get_memory(j3)
    end if
    if (iohmic == 1 .or. iohmic == 2) then
      call currents(j1, j2, j3, b1, b2, b3)
      call ohmic_heating(j1, j2, j3, dt)
    end if
    if (iohmic == 1) then  ! Add together the EMF terms for an unsplit method:
      call add_emfs(emf1, emf2, emf3, j1, j2, j3, 1)
    end if
    !
    !-----------------------------------------------------------------------
    ! EMF boundary conditions, done in alternative and different ways:
    !

    if (with_bc_emf_threeroutines) then
      ! Alternatively, do each EMF BC separately
      call boundary('emf1', emf1, emf2, emf3)
      call boundary('emf2', emf1, emf2, emf3)
      call boundary('emf3', emf1, emf2, emf3)

      call writedata(emf1, routinename//'_2_emf1', 4)
      call writedata(emf2, routinename//'_2_emf2', 4)
      call writedata(emf3, routinename//'_2_emf3', 4)
    end if

    if (with_bc_emf_oneroutine) then
      ! Alternatively, do all three EMF BCs at the same time
      call boundary('emf', emf1, emf2, emf3)

      call writedata(emf1, routinename//'_3_emf1', 4)
      call writedata(emf2, routinename//'_3_emf2', 4)
      call writedata(emf3, routinename//'_3_emf3', 4)
    end if

    if (iohmic == 2) then  ! unsplit, but split BCs
      ! All BCs have been applied by now
      call add_emfs(emf1, emf2, emf3, j1, j2, j3, 0)
    end if

    call writedata(b1, routinename//'_1_b1', 4)
    call writedata(b2, routinename//'_1_b2', 4)
    call writedata(b3, routinename//'_1_b3', 4)
    ! Evolve the magnetic field.
    call update_b(b1, b2, b3, emf1, emf2, emf3, .false., dt, '')
    call writedata(b1, routinename//'_2_b1', 4)
    call writedata(b2, routinename//'_2_b2', 4)
    call writedata(b3, routinename//'_2_b3', 4)

    if (ihall == 2) then
      call get_memory(baux1)
      call get_memory(baux2)
      call get_memory(baux3)
      if (iohmic == 0) then
        call get_memory(j1)
        call get_memory(j2)
        call get_memory(j3)
      end if

      if (hall_number_of_cycles == 0) then
        ! Anti-aliasing paranoia?
        baux1 = b1
        baux2 = b2
        baux3 = b3

        !Half step
        call currents(j1, j2, j3, b1, b2, b3)
        call hall_emf(emf1, emf2, emf3, j1, j2, j3, b1, b2, b3)
        call update_b(baux1, baux2, baux3, emf1, emf2, emf3, .false., haf*dt, &
                      '_hall_h')

        !Full step
        call currents(j1, j2, j3, baux1, baux2, baux3)
        call hall_emf(emf1, emf2, emf3, j1, j2, j3, baux1, baux2, baux3)
        call update_b(baux1, baux2, baux3, emf1, emf2, emf3, .false., dt, &
                      '_hall_f')

        ! Anti-aliasing paranoia
        b1 = baux1
        b2 = baux2
        b3 = baux3
      else
        time_cycle = zro
        ncycle = 0
        termination_flag = .false.
        hall_subcycle: do
          call nudt_hall(dthall)
          if (time_cycle + dthall >= dt) then
            dthall = dt - time_cycle
            termination_flag = .true.
          end if

          baux1 = b1
          baux2 = b2
          baux3 = b3
          !Half step
          call currents(j1, j2, j3, b1, b2, b3)
          call hall_emf(emf1, emf2, emf3, j1, j2, j3, b1, b2, b3)
          call update_b(baux1, baux2, baux3, emf1, emf2, emf3, .false., &
                        haf*dthall, '_hall_h')
          !Full step
          call currents(j1, j2, j3, baux1, baux2, baux3)
          call hall_emf(emf1, emf2, emf3, j1, j2, j3, baux1, baux2, baux3)
          call update_b(baux1, baux2, baux3, emf1, emf2, emf3, .false., &
                        dthall, '_hall_f')
          ! Anti-aliasing paranoia
          b1 = baux1
          b2 = baux2
          b3 = baux3

          ncycle = ncycle + 1
          if (ncycle >= hall_max_number_of_cycles) then
            !termination_flag=.true.
            call finalstop(routinename, 'Hall did not converge')
          end if
          if (termination_flag) exit hall_subcycle
          time_cycle = time_cycle + dthall
        end do hall_subcycle

      end if

      if (iohmic == 0) then
        call release_memory(j3)
        call release_memory(j2)
        call release_memory(j1)
      end if
      call release_memory(baux3)
      call release_memory(baux2)
      call release_memory(baux1)
    end if

    ! EMF arrays are not needed anymore, and can be released
    call release_memory(emf3)
    call release_memory(emf2)
    call release_memory(emf1)

    if (iohmic == 3) then ! split algorithm, using the currents
      ! calculated after the first B-update
      if (iohmic_cycle == 1) then
        call currents(j1, j2, j3, b1, b2, b3)
        call ohmic_heating(j1, j2, j3, dt)
        call update_b(b1, b2, b3, j1, j2, j3, .true., dt, '_ohm')
      else
        heating_not_done = .true.
        do subcycle_count = 1, nsubcycles_ohm
          call currents(j1, j2, j3, b1, b2, b3)
          write (message, '(a,i5.5,a,i5.5)') '_ohm_', &
            subcycle_count, '_', nsubcycles_ohm
          call writedata(b1, routinename//'_1_b1'//trim(message), 4)
          call writedata(b2, routinename//'_1_b2'//trim(message), 4)
          call writedata(b3, routinename//'_1_b3'//trim(message), 4)
          call update_b(b1, b2, b3, j1, j2, j3, .true., dtohm, message)
          call writedata(b1, routinename//'_2_b1'//trim(message), 4)
          call writedata(b2, routinename//'_2_b2'//trim(message), 4)
          call writedata(b3, routinename//'_2_b3'//trim(message), 4)
        end do
        ! Low accuracy - but the algorithm is still faulty anyway
        call ohmic_heating(j1, j2, j3, dt)
      end if
    end if

    if (iohmic /= 0) then
      call release_memory(j3)
      call release_memory(j2)
      call release_memory(j1)
    end if
  END SUBROUTINE ct
  !
  !=======================================================================
  !
  SUBROUTINE nudt_hall(dthall)
#ifdef MPI
    USE mpi
    INTEGER ierr
#endif /* MPI */

    REALNUM, INTENT(OUT) :: dthall
    REALNUM bsq, dr1i, dr2i, dr3i, drimax, buffer
    INTEGER i, j, k, ip1, jp1, kp1
    REALNUM dthalli2_max, dthalli2, eta_hal, q_hall_cap
    if (hall_dt_floor > zro) then
      q_hall_cap = one/(hall_dt_floor**2)
    else
      q_hall_cap = huge(one)
    end if
    dthalli2 = zro
    dthalli2_max = -huge(one)
    do k = ks, ke
      kp1 = k + kone
      do j = js, je
        jp1 = j + jone
        do i = is, ie

          ip1 = i + ione
          dr1i = dx1ai(i)*onei
          dr2i = g2bi(i)*dx2ai(j)*onej
          dr3i = g31bi(i)*g32bi(j)*dx3ai(k)*onek
          drimax = max(dr1i, dr2i, dr3i)

          if (withbp) then
            if (withb3) then
              bsq = ((b1(i, j, k) + b1(ip1, j, k))**2 &
                     + (b2(i, j, k) + b2(i, jp1, k))**2 &
                     + (b3(i, j, k) + b3(i, j, kp1))**2) &
                    *quarter
            else
              bsq = ((b1(i, j, k) + b1(ip1, j, k))**2 &
                     + (b2(i, j, k) + b2(i, jp1, k))**2) &
                    *quarter
            end if
          else
            if (withb3) then
              bsq = b3(i, j, k)**2
            else
              bsq = zro
            end if
          end if

          if (withchem) then
            eta_hal = hall_eta_array(i, j, k)
          else
            eta_hal = hall_coefficient(d_(i, j, k))
          end if
          !dthalli2=bsq * &
          !     (four*pi*hall_coefficient(d_(i,j,k))*drimax**2)**2
          dthalli2 = min(bsq*(four*pi*eta_hal*drimax**2)**2, q_hall_cap)
          if (dthalli2 > dthalli2_max) then
            dthalli2_max = dthalli2
          end if
        end do
      end do
    end do

    dthall = courno_hall/(sqrt(max(dthalli2_max, zro)) + verysmall)

#ifdef MPI
    buffer = dthall
    call MPI_ALLREDUCE(buffer, dthall, 1, MPI_FLOATMPI, MPI_MIN, &
                       MPI_COMM_WORLD, ierr)
#endif
  END SUBROUTINE nudt_hall
  !
  !=======================================================================
  !
  SUBROUTINE ion_velocity_get(vi1, vi2, vi3)
    REALNUM, POINTER :: vi1(:, :, :), vi2(:, :, :), vi3(:, :, :)
    if (with_ad_1f .or. ihall == 1) then
      call get_memory(vi1)
      call get_memory(vi2)
      call get_memory(vi3)
      if (with_ad_1f) then
        call calculate_ion_velocity(vi1, vi2, vi3)
      else
        vi1 = v1
        vi2 = v2
        vi3 = v3
      end if
    else
      vi1 => v1
      vi2 => v2
      vi3 => v3
    end if
  END SUBROUTINE ion_velocity_get
  !
  !=======================================================================
  !
  SUBROUTINE calculate_ion_velocity(vi1, vi2, vi3)
    USE problem_variables, only: epsilon, disk_angle_factor
    REALNUM, DIMENSION(isa:iea, jsa:jea, ksa:kea), INTENT(OUT) :: vi1, vi2, vi3

    REALNUM, POINTER, SAVE, DIMENSION(:, :, :) :: j1 => null(), j2 => null(), &
                                                  j3 => null()

    REALNUM :: vdrift1, vdrift2, vdrift3
    REALNUM :: rho, rho_ion, ad_coefficient, ad_cfl, q_ad_floor
    REALNUM :: force1, force2, force3

    REALNUM :: b1a, b2a, b3a
    REALNUM :: drimax, bsq

    REALNUM :: disk_angle, theta, f_theta

    INTEGER i, j, k, im1, jm1, km1, ip1, jp1, kp1

    if (.not. with_ad_1f) return

    ! The current may be needed for other reasons - consider taking it from
    ! there in those cases.  Same for JxB.
    call get_memory(j1)
    call get_memory(j2)
    call get_memory(j3)
    call currents(j1, j2, j3, b1, b2, b3)

    ad_cfl = courno  ! Not an independent control in this version

    if (ad_dt_floor > zro) then
      q_ad_floor = ad_cfl/(four*ad_dt_floor)
    else
      q_ad_floor = huge(one)
    end if

    disk_angle = atan(disk_angle_factor*epsilon)

    ! vdrift = JxB / (gamma rho_ion rho) -- which needs some recentering,
    ! which is copied from the stv* force terms w/o MOC.

    ! This routine stores the drift in the vi arrays, which at this stage
    ! contain only the drift velocities, not vi
    ! (at this stage their name is a lie).
    ! This is done in order to facilitate imposing BCs on the drift
    ! velocity components.

    do k = ksgo, kego
      km1 = k - kone
      kp1 = k + kone
      do j = jsgo, jego
        jm1 = j - jone
        jp1 = j + jone

        theta = x2a(j)
        f_theta = one
        if (theta < haf*pi - disk_angle) &
          f_theta = exp(-cos(theta + disk_angle)**2/(two*epsilon**2))
        if (theta > haf*pi + disk_angle) &
          f_theta = exp(-cos(theta - disk_angle)**2/(two*epsilon**2))

        do i = isgo, iego
          im1 = i - ione
          ip1 = i + ione
          ! ad_coefficient < ad_cfl*dxmin^2 / (4 dt_ad_floor B^2).
          ! with ad_coefficient = 1.0/(ad_gamma*rho*rho_ion)

          !rho_total = rho_ion + rho
          b1a = b1(i, j, k)
          b2a = (b2(i, j, k) + b2(i, jp1, k) + &
                 b2(im1, j, k) + b2(im1, jp1, k))*quarter
          b3a = (b3(i, j, k) + b3(i, j, kp1) + &
                 b3(im1, j, k) + b3(im1, j, kp1))*quarter
          force1 = ((j2(i, j, k) + j2(i, j, kp1))*b3a - &
                    (j3(i, j, k) + j3(i, jp1, k))*b2a)*haf

          bsq = b1a**2 + b2a**2 + b3a**2
          drimax = max(onei*dx1bi(i), onej*g2bi(i)*dx2ai(j), &
                       onek*g31bi(i)*g32b(j)*dx3ai(k))
          if (withchem) then
            ad_coefficient = haf*(ad_eta_array(im1, j, k) &
                                  + ad_eta_array(i, j, k))
          else
            rho = haf*(d_(im1, j, k) + d_(i, j, k))
            rho_ion = ad_rhoi0*(rho/ad_rhon0)**ad_alpha
            ad_coefficient = one/(ad_gamma*rho*rho_ion)
          end if
          ad_coefficient = min(f_theta*ad_coefficient, &
                               q_ad_floor/(drimax**2*bsq))
          !vdrift1 = force1 / (rho_ion*rho_total*ad_gamma)
          vdrift1 = force1*f_theta*ad_coefficient
          vi1(i, j, k) = vdrift1

          !rho_total = rho_ion + rho
          b1a = (b1(i, j, k) + b1(ip1, j, k) + &
                 b1(i, jm1, k) + b1(ip1, jm1, k))*quarter
          b2a = b2(i, j, k)
          b3a = (b3(i, j, k) + b3(i, j, kp1) + &
                 b3(i, jm1, k) + b3(i, jm1, kp1))*quarter
          force2 = ((j3(i, j, k) + j3(ip1, j, k))*b1a - &
                    (j1(i, j, k) + j1(i, j, kp1))*b3a)*haf

          bsq = b1a**2 + b2a**2 + b3a**2
          drimax = max(onei*dx1ai(i), onej*g2ai(i)*dx2bi(j), &
                       onek*g31bi(i)*g32b(j)*dx3ai(k))
          if (withchem) then
            ad_coefficient = haf*(ad_eta_array(i, jm1, k) &
                                  + ad_eta_array(i, j, k))
          else
            rho = haf*(d_(i, jm1, k) + d_(i, j, k))
            rho_ion = ad_rhoi0*(rho/ad_rhon0)**ad_alpha
            ad_coefficient = one/(ad_gamma*rho*rho_ion)
          end if
          ad_coefficient = min(f_theta*ad_coefficient, &
                               q_ad_floor/(drimax**2*bsq))
          ! vdrift2 = force2 / (rho_ion*rho_total*ad_gamma)
          vdrift2 = force2*f_theta*ad_coefficient
          vi2(i, j, k) = vdrift2

          ! rho_total = rho_ion + rho
          b1a = (b1(i, j, k) + b1(ip1, j, k) + &
                 b1(i, j, km1) + b1(ip1, j, km1))*quarter
          b2a = (b2(i, j, k) + b2(i, jp1, k) + &
                 b2(i, j, km1) + b2(i, jp1, km1))*quarter
          b3a = b3(i, j, k)
          force3 = ((j1(i, j, k) + j1(i, jp1, k))*b2a - &
                    (j2(i, j, k) + j2(ip1, j, k))*b1a)*haf
          bsq = b1a**2 + b2a**2 + b3a**2
          drimax = max(onei*dx1ai(i), onej*g2bi(i)*dx2ai(j), &
                       onek*g31ai(i)*g32a(j)*dx3bi(k))
          if (withchem) then
            ad_coefficient = haf*(ad_eta_array(i, j, km1) &
                                  + ad_eta_array(i, j, k))
          else
            rho = haf*(d_(i, j, km1) + d_(i, j, k))
            rho_ion = ad_rhoi0*(rho/ad_rhon0)**ad_alpha
            ad_coefficient = one/(ad_gamma*rho*rho_ion)
          end if
          ad_coefficient = min(f_theta*ad_coefficient, &
                               q_ad_floor/(drimax**2*bsq))
          ! vdrift3 = force3 / (rho_ion*rho_total*ad_gamma)
          vdrift3 = force3*f_theta*ad_coefficient
          vi3(i, j, k) = vdrift3
        end do
      end do
    end do

    call release_memory(j3)
    call release_memory(j2)
    call release_memory(j1)

    if (with_bc_vd) then
      ! Impose BCs on the drift
      call boundary('vd', vi1, vi2, vi3)
    end if

    ! From here on the fields vi1 contain authentic values of the ion
    ! velocity
    vi1 = vi1 + v1
    vi2 = vi2 + v2
    vi3 = vi3 + v3

    if (with_bc_vi) then
      ! Impose BCs on the ion velocity
      call boundary('vi', vi1, vi2, vi3)
    end if

  END SUBROUTINE calculate_ion_velocity
  !
  !=======================================================================
  !
  SUBROUTINE hall_drift(vi1, vi2, vi3)
    REALNUM, DIMENSION(isa:iea, jsa:jea, ksa:kea), INTENT(INOUT) :: vi1, &
                                                                    vi2, &
                                                                    vi3

    REALNUM, POINTER, SAVE, DIMENSION(:, :, :) :: j1 => null(), j2 => null(), &
                                                  j3 => null()

    REALNUM :: jrecentered, rho, eta_hal, q_hall_floor, drimax, bsq

    INTEGER i, j, k, im1, jm1, km1, ip1, jp1, kp1

    ! The current may be needed for other reasons - consider taking it from
    ! there in those cases.  Same for JxB.

    if (ihall == 0) return

    call get_memory(j1)
    call get_memory(j2)
    call get_memory(j3)
    call currents(j1, j2, j3, b1, b2, b3)

    ! vdrift = JxB / (gamma rho_ion rho) -- which needs some recentering,
    ! which is copied from the stv* force terms w/o MOC.
    do k = ksgo, kego
      km1 = k - kone
      kp1 = k + kone
      do j = jsgo, jego
        jm1 = j - jone
        jp1 = j + jone
        do i = isgo, iego
          im1 = i - ione
          ip1 = i + ione

          if (withbp) then
            if (withb3) then
              bsq = ((b1(i, j, k) + b1(ip1, j, k))**2 &
                     + (b2(i, j, k) + b2(i, jp1, k))**2 &
                     + (b3(i, j, k) + b3(i, j, kp1))**2) &
                    *quarter
            else
              bsq = ((b1(i, j, k) + b1(ip1, j, k))**2 &
                     + (b2(i, j, k) + b2(i, jp1, k))**2) &
                    *quarter
            end if
          else
            if (withb3) then
              bsq = b3(i, j, k)**2
            else
              bsq = zro
            end if
          end if

          ! Recenter the current component 1
          ! to the position of the velocity component
          jrecentered = (j1(i, j, k) + j1(im1, j, k) + &
                         j1(i, jp1, k) + j1(im1, jp1, k) + &
                         j1(i, j, kp1) + j1(im1, j, kp1) + &
                         j1(i, jp1, kp1) + j1(im1, jp1, kp1))*eighth
          rho = (d_(i, j, k) + d_(im1, j, k))*haf
          drimax = max(dx1bi(i)*onei, &
                       g2ai(i)*dx2ai(j)*onej, &
                       g31ai(i)*g32bi(j)*dx3ai(k)*onek)
          if (withchem) then
            eta_hal = (hall_eta_array(i, j, k) + hall_eta_array(im1, j, k))*haf
          else
            eta_hal = hall_coefficient(rho)
          end if
          eta_hal = max(-q_hall_floor/(drimax**2)/sqrt(bsq), &
                        min(eta_hal, q_hall_floor/(drimax**2)/sqrt(bsq)))
          vi1(i, j, k) = vi1(i, j, k) - eta_hal*jrecentered

          jrecentered = (j2(i, j, k) + j2(i, jm1, k) + &
                         j2(ip1, j, k) + j2(ip1, jm1, k) + &
                         j2(i, j, kp1) + j2(i, jm1, kp1) + &
                         j2(ip1, j, kp1) + j2(ip1, jm1, kp1))*eighth
          rho = (d_(i, j, k) + d_(i, jm1, k))*haf
          drimax = max(dx1ai(i)*onei, &
                       g2bi(i)*dx2bi(j)*onej, &
                       g31bi(i)*g32ai(j)*dx3ai(k)*onek)
          if (withchem) then
            eta_hal = (hall_eta_array(i, j, k) + hall_eta_array(i, jm1, k))*haf
          else
            eta_hal = hall_coefficient(rho)
          end if
          eta_hal = max(-q_hall_floor/(drimax**2)/sqrt(bsq), &
                        min(eta_hal, q_hall_floor/(drimax**2)/sqrt(bsq)))
          vi2(i, j, k) = vi2(i, j, k) - eta_hal*jrecentered

          jrecentered = (j3(i, j, k) + j3(i, j, km1) + &
                         j3(ip1, j, k) + j3(ip1, j, km1) + &
                         j3(i, jp1, k) + j3(i, jp1, km1) + &
                         j3(ip1, jp1, k) + j3(ip1, jp1, km1))*eighth
          rho = (d_(i, j, k) + d_(i, j, km1))*haf
          drimax = max(dx1ai(i)*onei, &
                       g2bi(i)*dx2ai(j)*onej, &
                       g31bi(i)*g32bi(j)*dx3bi(k)*onek)
          if (withchem) then
            eta_hal = (hall_eta_array(i, j, k) + hall_eta_array(i, j, km1))*haf
          else
            eta_hal = hall_coefficient(rho)
          end if
          eta_hal = max(-q_hall_floor/(drimax**2)/sqrt(bsq), &
                        min(eta_hal, q_hall_floor/(drimax**2)/sqrt(bsq)))
          vi3(i, j, k) = vi3(i, j, k) - eta_hal*jrecentered
        end do
      end do
    end do

    call release_memory(j3)
    call release_memory(j2)
    call release_memory(j1)

    if (with_bc_vi) then
      call boundary('vi', vi1, vi2, vi3)
    end if

  END SUBROUTINE hall_drift
  !
  !=======================================================================
  !
! REALNUM FUNCTION hall_coefficient(rho)
!   REALNUM, INTENT(IN) :: rho
!
!   hall_coefficient=hall_coefficient_0 * (rho/hall_rhoreference)**hall_alpha
!
! END FUNCTION hall_coefficient
  !
  !=======================================================================
  !
  SUBROUTINE hall_emf(emf1, emf2, emf3, j1, j2, j3, b1, b2, b3)
    REALNUM, DIMENSION(isa:iea, jsa:jea, ksa:kea), INTENT(OUT) :: emf1, &
                                                                  emf2, &
                                                                  emf3
    REALNUM, DIMENSION(isa:iea, jsa:jea, ksa:kea), INTENT(IN) :: j1, j2, j3
    REALNUM, DIMENSION(isa:iea, jsa:jea, ksa:kea), INTENT(IN) :: b1, b2, b3

    INTEGER i, j, k, im1, jm1, km1, ip1, jp1, kp1

    REALNUM :: rho, eta_hal, q_hall_floor, drimax, bsq

    if (hall_dt_floor > zro) then
      q_hall_floor = 0.5*courno/(four*hall_dt_floor)
    else
      q_hall_floor = huge(one)
    end if

    do k = ksgo, kego
      km1 = k - kone
      kp1 = k + kone
      do j = jsgo, jego
        jm1 = j - jone
        jp1 = j + jone
        do i = isgo, iego
          im1 = i - ione
          ip1 = i + ione

          if (withbp) then
            if (withb3) then
              bsq = ((b1(i, j, k) + b1(ip1, j, k))**2 &
                     + (b2(i, j, k) + b2(i, jp1, k))**2 &
                     + (b3(i, j, k) + b3(i, j, kp1))**2) &
                    *quarter
            else
              bsq = ((b1(i, j, k) + b1(ip1, j, k))**2 &
                     + (b2(i, j, k) + b2(i, jp1, k))**2) &
                    *quarter
            end if
          else
            if (withb3) then
              bsq = b3(i, j, k)**2
            else
              bsq = zro
            end if
          end if

          rho = (d_(i, j, k) + d_(i, jm1, km1) &
                 + d_(i, jm1, k) + d_(i, j, km1))*quarter
          drimax = max(dx1ai(i)*onei, &
                       g2bi(i)*dx2bi(j)*onej, &
                       g31bi(i)*g32ai(j)*dx3bi(k)*onek)
          if (withchem) then
            eta_hal = (hall_eta_array(i, j, k) + hall_eta_array(i, jm1, k) &
                       + hall_eta_array(i, j, km1) &
                       + hall_eta_array(i, jm1, km1)) &
                      *quarter
          else
            eta_hal = hall_coefficient(rho)
          end if
          eta_hal = max(-q_hall_floor/(drimax**2)/sqrt(bsq), &
                        min(eta_hal, q_hall_floor/(drimax**2)/sqrt(bsq)))
          emf1(i, j, k) = &
            ( &
            (j2(i, j, k) + j2(ip1, j, k) + j2(i, jm1, k) + j2(ip1, jm1, k)) &
            *(b3(i, j, k) + b3(i, jm1, k)) - &
            (j3(i, j, k) + j3(ip1, j, k) + j3(i, j, km1) + j3(ip1, j, km1)) &
            *(b2(i, j, k) + b2(i, j, km1)) &
            )*eighth*(-eta_hal)

          rho = (d_(i, j, k) + d_(im1, j, km1) &
                 + d_(im1, j, k) + d_(i, j, km1))*quarter
          drimax = max(dx1bi(i)*onei, &
                       g2ai(i)*dx2ai(j)*onej, &
                       g31ai(i)*g32bi(j)*dx3bi(k)*onek)
          if (withchem) then
            eta_hal = (hall_eta_array(i, j, k) + hall_eta_array(im1, j, k) &
                       + hall_eta_array(i, j, km1) &
                       + hall_eta_array(im1, j, km1)) &
                      *quarter
          else
            eta_hal = hall_coefficient(rho)
          end if
          eta_hal = max(-q_hall_floor/(drimax**2)/sqrt(bsq), &
                        min(eta_hal, q_hall_floor/(drimax**2)/sqrt(bsq)))
          emf2(i, j, k) = &
            ( &
            (j3(i, j, k) + j3(i, jp1, k) + j3(i, j, km1) + j3(i, jp1, km1)) &
            *(b1(i, j, k) + b1(i, j, km1)) - &
            (j1(i, j, k) + j1(i, jp1, k) + j1(im1, j, k) + j1(im1, jp1, k)) &
            *(b3(i, j, k) + b3(im1, j, k)) &
            )*eighth*(-eta_hal)

          rho = (d_(i, j, k) + d_(im1, jm1, k) &
                 + d_(im1, j, k) + d_(i, jm1, k))*quarter
          drimax = max(dx1bi(i)*onei, &
                       g2ai(i)*dx2bi(j)*onej, &
                       g31ai(i)*g32ai(j)*dx3ai(k)*onek)
          if (withchem) then
            eta_hal = (hall_eta_array(i, j, k) + hall_eta_array(im1, j, k) &
                       + hall_eta_array(i, jm1, k) &
                       + hall_eta_array(im1, jm1, k)) &
                      *quarter
          else
            eta_hal = hall_coefficient(rho)
          end if
          eta_hal = max(-q_hall_floor/(drimax**2)/sqrt(bsq), &
                        min(eta_hal, q_hall_floor/(drimax**2)/sqrt(bsq)))
          emf3(i, j, k) = &
            ( &
            (j1(i, j, k) + j1(i, j, kp1) + j1(im1, j, k) + j1(im1, j, kp1)) &
            *(b2(i, j, k) + b2(im1, j, k)) - &
            (j2(i, j, k) + j2(i, j, kp1) + j2(i, jm1, k) + j2(i, jm1, kp1)) &
            *(b1(i, j, k) + b1(i, jm1, k)) &
            )*eighth*(-eta_hal)

        end do
      end do
    end do

    if (with_bc_emf_threeroutines) then
      call boundary('emf1', emf1, emf2, emf3)
      call boundary('emf2', emf1, emf2, emf3)
      call boundary('emf3', emf1, emf2, emf3)
    end if

    if (with_bc_emf_oneroutine) then
      ! Alternatively, do all three EMF BCs at the same time
      call boundary('emf', emf1, emf2, emf3)
    end if

  END SUBROUTINE hall_emf
  !
  !=======================================================================
  !
  SUBROUTINE ad_heating(vi1, vi2, vi3)
    USE problem_variables, only: epsilon, disk_angle_factor
    REALNUM, DIMENSION(isa:iea, jsa:jea, ksa:kea), INTENT(IN) :: vi1, vi2, vi3

    REALNUM :: vdrift1, vdrift2, vdrift3, rho, rho_ion, heating_rate, &
               ad_coefficient, disk_angle, theta, f_theta
    INTEGER i, j, k, ip1, jp1, kp1

    if (.not. (with_ad_1f .and. ienergy == 1)) return

    disk_angle = atan(disk_angle_factor*epsilon)

    do k = ksg, kego
      kp1 = k + kone
      do j = jsg, jego
        jp1 = j + jone

        theta = x2a(j)
        f_theta = one
        if (theta < haf*pi - disk_angle) &
          f_theta = exp(-cos(theta + disk_angle)**2/(two*epsilon**2))
        if (theta > haf*pi + disk_angle) &
          f_theta = exp(-cos(theta - disk_angle)**2/(two*epsilon**2))

        do i = isg, iego
          ip1 = i + ione

          if (withchem) then
            ad_coefficient = ad_eta_array(i, j, k)
          else
            rho = d_(i, j, k)
            rho_ion = ad_rhoi0*(rho/ad_rhon0)**ad_alpha
            ad_coefficient = one/(ad_gamma*rho*rho_ion)
          end if
          vdrift1 = haf*((vi1(i, j, k) - v1(i, j, k)) &
                         + (vi1(ip1, j, k) - v1(ip1, j, k)))
          vdrift2 = haf*((vi2(i, j, k) - v2(i, j, k)) &
                         + (vi2(i, jp1, k) - v2(i, jp1, k)))
          vdrift3 = haf*((vi3(i, j, k) - v3(i, j, k)) &
                         + (vi3(i, j, kp1) - v3(i, j, kp1)))

          heating_rate = (vdrift1**2 + vdrift2**2 + vdrift3**2) &
                         /(f_theta*ad_coefficient)

          e_(i, j, k) = e_(i, j, k) + dt*heating_rate

        end do
      end do
    end do

    call boundary('e_')

  END SUBROUTINE ad_heating
  !
  !=======================================================================
  !
  SUBROUTINE ion_velocity_release(vi1, vi2, vi3)
    REALNUM, POINTER :: vi1(:, :, :), vi2(:, :, :), vi3(:, :, :)

    if (with_ad_1f .or. ihall == 1) then
      call release_memory(vi1)
      call release_memory(vi2)
      call release_memory(vi3)
    end if

  END SUBROUTINE ion_velocity_release
  !
  !=======================================================================
  !
  SUBROUTINE add_emfs(emf1, emf2, emf3, j1, j2, j3, box)
    CHARACTER(LEN=*), PARAMETER :: routinename = 'z_add_emfs'
    REALNUM, DIMENSION(isa:iea, jsa:jea, ksa:kea), INTENT(INOUT) :: emf1, &
                                                                    emf2, &
                                                                    emf3
    REALNUM, DIMENSION(isa:iea, jsa:jea, ksa:kea), INTENT(IN) :: j1, j2, j3
    INTEGER, INTENT(IN) :: box ! control of box shape
    INTEGER :: i, j, k, im1, jm1, km1
    INTEGER :: imin, jmin, kmin
    INTEGER :: imax, jmax, kmax
    REALNUM :: rho, eta_ohm

    ! ohmic_exponent case 200 not implemented
    call finalstop(routinename, &
                   'Not actively maintained since at least 2012-03-26')

    select case (box)
    case (1)  ! Prepare the active zones
      kmin = ks
      kmax = kep1
      jmin = js
      jmax = jep1
      imin = is
      imax = iep1
    case (0)  ! Include all zones
      kmin = ksg
      kmax = keg
      jmin = jsg
      jmax = jeg
      imin = isg
      imax = ieg
    case default ! Strange error
      call threeoutput(routinename, 'Box shape not understood.')
      call finalstop(routinename, 'Stop.')
    end select

    eta_ohm = ohmic_eta_factor
    select case (ohmic_exponent)
    case (1)
      do k = kmin, kmax
        km1 = max(ksg, k - kone)
        do j = jmin, jmax
          jm1 = max(jsg, j - jone)
          do i = imin, imax - ione*box
            rho = quarter*(d_(i, j, k) + d_(i, jm1, k) &
                           + d_(i, j, km1) + d_(i, jm1, km1))
            if (withchem) then
              eta_ohm = quarter*(ohmic_eta_array(i, j, k) + &
                                 ohmic_eta_array(i, jm1, k) + &
                                 ohmic_eta_array(i, j, km1) + &
                                 ohmic_eta_array(i, jm1, km1))
            end if
            emf1(i, j, k) = emf1(i, j, k) + (-eta_ohm)*rho*j1(i, j, k)
          end do
        end do
      end do
      do k = kmin, kmax
        km1 = max(ksg, k - kone)
        do j = jmin, jmax - jone*box
          do i = imin, imax
            im1 = max(isg, i - ione)
            rho = quarter*(d_(i, j, k) + d_(im1, j, k) &
                           + d_(i, j, km1) + d_(im1, j, km1))
            if (withchem) then
              eta_ohm = quarter*(ohmic_eta_array(i, j, k) + &
                                 ohmic_eta_array(im1, j, k) + &
                                 ohmic_eta_array(i, j, km1) + &
                                 ohmic_eta_array(im1, j, km1))
            end if
            emf2(i, j, k) = emf2(i, j, k) + (-eta_ohm)*rho*j2(i, j, k)
          end do
        end do
      end do
      do k = kmin, kmax - kone*box
        do j = jmin, jmax
          jm1 = max(jsg, j - jone)
          do i = imin, imax
            im1 = max(isg, i - ione)
            rho = quarter*(d_(i, j, k) + d_(im1, j, k) &
                           + d_(i, jm1, k) + d_(im1, jm1, k))
            if (withchem) then
              eta_ohm = quarter*(ohmic_eta_array(i, j, k) + &
                                 ohmic_eta_array(im1, j, k) + &
                                 ohmic_eta_array(i, jm1, k) + &
                                 ohmic_eta_array(im1, jm1, k))
            end if
            emf3(i, j, k) = emf3(i, j, k) + (-eta_ohm)*rho*j3(i, j, k)
          end do
        end do
      end do
    case (0)
      do k = kmin, kmax
        km1 = max(ksg, k - kone)
        do j = jmin, jmax
          jm1 = max(jsg, j - jone)
          do i = imin, imax - ione*box
            if (withchem) then
              eta_ohm = quarter*(ohmic_eta_array(i, j, k) + &
                                 ohmic_eta_array(i, jm1, k) + &
                                 ohmic_eta_array(i, j, km1) + &
                                 ohmic_eta_array(i, jm1, km1))
            end if
            emf1(i, j, k) = emf1(i, j, k) + (-eta_ohm)*j1(i, j, k)
          end do
        end do
      end do
      do k = kmin, kmax
        km1 = max(ksg, k - kone)
        do j = jmin, jmax - jone*box
          do i = imin, imax
            im1 = max(isg, i - ione)
            if (withchem) then
              eta_ohm = quarter*(ohmic_eta_array(i, j, k) + &
                                 ohmic_eta_array(im1, j, k) + &
                                 ohmic_eta_array(i, j, km1) + &
                                 ohmic_eta_array(im1, j, km1))
            end if
            emf2(i, j, k) = emf2(i, j, k) + (-eta_ohm)*j2(i, j, k)
          end do
        end do
      end do
      do k = kmin, kmax - kone*box
        do j = jmin, jmax
          jm1 = max(jsg, j - jone)
          do i = imin, imax
            im1 = max(isg, i - ione)
            if (withchem) then
              eta_ohm = quarter*(ohmic_eta_array(i, j, k) + &
                                 ohmic_eta_array(im1, j, k) + &
                                 ohmic_eta_array(i, jm1, k) + &
                                 ohmic_eta_array(im1, jm1, k))
            end if
            emf3(i, j, k) = emf3(i, j, k) + (-eta_ohm)*j3(i, j, k)
          end do
        end do
      end do
    case default
      call finalstop(routinename, &
                     'This ohmic_exponent has not been implemented yet')
    end select
  END SUBROUTINE add_emfs
  !
  !=======================================================================
  !
  SUBROUTINE srds_get(srd1, srd2, srd3)
    REALNUM, POINTER :: srd1(:, :, :), srd2(:, :, :), srd3(:, :, :)
    INTEGER i, j, k, im1, jm1, km1

    if (imhd /= 3 .and. imhd /= 2) return

    call get_memory(srd1)
    call get_memory(srd2)
    call get_memory(srd3)

    do k = ksm1, kep1
      km1 = k - kone
      do j = jsm1, jep1
        jm1 = j - jone
        do i = ism1, iep1
          im1 = i - ione
          srd1(i, j, k) = sqrt(haf*(dfl(i, j, k) + dfl(im1, j, k)))
          srd2(i, j, k) = sqrt(haf*(dfl(i, j, k) + dfl(i, jm1, k)))
          srd3(i, j, k) = sqrt(haf*(dfl(i, j, k) + dfl(i, j, km1)))
        end do
      end do
    end do
  END SUBROUTINE srds_get
  !
  !=======================================================================
  !
  SUBROUTINE srds_release(srd1, srd2, srd3)
    REALNUM, POINTER :: srd1(:, :, :), srd2(:, :, :), srd3(:, :, :)

    if (imhd /= 3 .and. imhd /= 2) return

    call release_memory(srd3)
    call release_memory(srd2)
    call release_memory(srd1)
  END SUBROUTINE srds_release
  !
  !=======================================================================
  !
  SUBROUTINE ohmic_heating(j1, j2, j3, delta_t)
    CHARACTER(LEN=*), PARAMETER :: routinename = 'z_ohmic_heating'
    ! Resistive heating term
    REALNUM, DIMENSION(:, :, :), INTENT(IN) :: j1, j2, j3
    REALNUM, INTENT(IN) :: delta_t
    REALNUM :: eta_ohm

    INTEGER :: i, j, k

    if (iohmic == 0 .or. ienergy /= 1) return
#if 0
    eta_ohm = ohmic_eta_factor
    do k = ks, ke
      do j = js, je
        do i = is, ie
          if (withchem) eta_ohm = ohmic_eta_array(i, j, k)
          e_(i, j, k) = e_(i, j, k) + &
                        eta_ohm*d_(i, j, k)**ohmic_eta_exponent* &
                        delta_t*sixteenth* &
                        ((j1(i, j, k) + j1(i, j + 1, k) &
                          + j1(i, j, k + 1) + j1(i, j + 1, k + 1))**2 + &
                         (j2(i, j, k) + j2(i + 1, j, k) &
                          + j2(i, j, k + 1) + j2(i + 1, j, k + 1))**2 + &
                         (j3(i, j, k) + j3(i, j + 1, k) &
                          + j3(i + 1, j, k) + j3(i + 1, j + 1, k))**2)
        end do
      end do
    end do
    call boundary('e_')
#else
    continue
    !  call warnoutput(routinename, &
    !       'Ohmic heating disabled.  It seems to fail stability tests.')
#endif
  END SUBROUTINE ohmic_heating
  !
  !=======================================================================
  !
  SUBROUTINE update_b(bout1, bout2, bout3, emf1, emf2, emf3, has_ohmic, &
                      delta_t, text)
    USE zeus_mpi
    USE problem_variables, only: bc_sign
    CHARACTER(LEN=*), PARAMETER :: routinename = 'z_update_b'

    ! Conceptually, these are "intent in", but the multiplication
    ! by the line element requires the code to call them with intent(inout)
    ! The resulting output is *not* the emf anymore.  Which can create some problems.
    ! Maybe a version that does not mangle the emfs would be much better.
    REALNUM, DIMENSION(:, :, :), INTENT(INOUT) :: emf1, emf2, emf3
    REALNUM emf3bc

    ! INOUT to keep the (unnecessary?) edge values unchanged
    REALNUM, DIMENSION(:, :, :), INTENT(INOUT) :: bout1, bout2, bout3

    REALNUM, INTENT(IN) :: delta_t
    LOGICAL, INTENT(IN) :: has_ohmic
    CHARACTER(LEN=*), INTENT(IN) :: text

    INTEGER i, ip1, j, jp1, k, kp1
    INTEGER im1, jm1, km1
    ! INTEGER kpi
    INTEGER kplace

    INTEGER imin, imax, jmin, jmax
    REALNUM, DIMENSION(isa:iea) :: qty1, qty1ni
    REALNUM, DIMENSION(jsa:jea) :: qty2, qty2ni
    REALNUM :: rho, eta_ohm

    ! Multiply the EMFs by the line elements and the constant factor
    ! and the density power law
    ! From here on, the EMF arrays are *not* the electric field components any more.

    if (has_ohmic) then
      eta_ohm = ohmic_eta_factor
      select case (ohmic_exponent)
      case (1)
        do k = ksg, keg
          km1 = max(ksg, k - kone)
          do j = jsg, jeg
            jm1 = max(jsg, j - jone)
            do i = isg, ieg
              im1 = max(isg, i - ione)

              rho = quarter*(d_(i, j, k) + d_(i, jm1, k) &
                             + d_(i, j, km1) + d_(i, jm1, km1))
              if (withchem) then
                eta_ohm = quarter*(ohmic_eta_array(i, j, k) + &
                                   ohmic_eta_array(i, jm1, k) + &
                                   ohmic_eta_array(i, j, km1) + &
                                   ohmic_eta_array(i, jm1, km1))
              end if
              emf1(i, j, k) = emf1(i, j, k)*dx1ah(i)*(-eta_ohm)*rho

              rho = quarter*(d_(i, j, k) + d_(im1, j, k) &
                             + d_(i, j, km1) + d_(im1, j, km1))
              if (withchem) then
                eta_ohm = quarter*(ohmic_eta_array(i, j, k) + &
                                   ohmic_eta_array(im1, j, k) + &
                                   ohmic_eta_array(i, j, km1) + &
                                   ohmic_eta_array(im1, j, km1))
              end if
              emf2(i, j, k) = emf2(i, j, k)*dx2ah(j)*g2ah(i)*(-eta_ohm)*rho

              rho = quarter*(d_(i, j, k) + d_(im1, j, k) &
                             + d_(i, jm1, k) + d_(im1, jm1, k))
              if (withchem) then
                eta_ohm = quarter*(ohmic_eta_array(i, j, k) + &
                                   ohmic_eta_array(im1, j, k) + &
                                   ohmic_eta_array(i, jm1, k) + &
                                   ohmic_eta_array(im1, jm1, k))
              end if
              emf3(i, j, k) = emf3(i, j, k)*dx3ah(k)*g31ah(i)*g32ah(j) &
                              *(-eta_ohm)*rho
            end do
          end do
        end do
      case (0)
        do k = ksg, keg
          km1 = max(ksg, k - kone)
          do j = jsg, jeg
            jm1 = max(jsg, j - jone)
            do i = isg, ieg
              im1 = max(isg, i - ione)

              if (withchem) then
                eta_ohm = quarter*(ohmic_eta_array(i, j, k) + &
                                   ohmic_eta_array(i, jm1, k) + &
                                   ohmic_eta_array(i, j, km1) + &
                                   ohmic_eta_array(i, jm1, km1))
              end if
              emf1(i, j, k) = emf1(i, j, k)*dx1ah(i)*(-eta_ohm)

              if (withchem) then
                eta_ohm = quarter*(ohmic_eta_array(i, j, k) + &
                                   ohmic_eta_array(im1, j, k) + &
                                   ohmic_eta_array(i, j, km1) + &
                                   ohmic_eta_array(im1, j, km1))
              end if
              emf2(i, j, k) = emf2(i, j, k)*dx2ah(j)*g2ah(i)*(-eta_ohm)

              if (withchem) then
                eta_ohm = quarter*(ohmic_eta_array(i, j, k) + &
                                   ohmic_eta_array(im1, j, k) + &
                                   ohmic_eta_array(i, jm1, k) + &
                                   ohmic_eta_array(im1, jm1, k))
              end if
              emf3(i, j, k) = emf3(i, j, k)*dx3ah(k)*g31ah(i)*g32ah(j) &
                              *(-eta_ohm)
            end do
          end do
        end do
      case (100)
        do k = ksg, keg
          km1 = max(ksg, k - kone)
          do j = jsg, jeg
            jm1 = max(jsg, j - jone)
            do i = isg, ieg
              im1 = max(isg, i - ione)

              if (withchem) then
                eta_ohm = quarter*(ohmic_eta_array(i, j, k) + &
                                   ohmic_eta_array(i, jm1, k) + &
                                   ohmic_eta_array(i, j, km1) + &
                                   ohmic_eta_array(i, jm1, km1))
              end if
              emf1(i, j, k) = emf1(i, j, k)*dx1ah(i)* &
                              (-eta_ohm)*min(rho/ohmic_rho_transition, one)

              if (withchem) then
                eta_ohm = quarter*(ohmic_eta_array(i, j, k) + &
                                   ohmic_eta_array(im1, j, k) + &
                                   ohmic_eta_array(i, j, km1) + &
                                   ohmic_eta_array(im1, j, km1))
              end if
              emf2(i, j, k) = emf2(i, j, k)*dx2ah(j)*g2ah(i)* &
                              (-eta_ohm)*min(rho/ohmic_rho_transition, one)

              if (withchem) then
                eta_ohm = quarter*(ohmic_eta_array(i, j, k) + &
                                   ohmic_eta_array(im1, j, k) + &
                                   ohmic_eta_array(i, jm1, k) + &
                                   ohmic_eta_array(im1, jm1, k))
              end if
              emf3(i, j, k) = emf3(i, j, k)*dx3ah(k)*g31ah(i)*g32ah(j)* &
                              (-eta_ohm)*min(rho/ohmic_rho_transition, one)
            end do
          end do
        end do
      case (200)
        do k = ksg, keg
          do j = jsg, jeg
            do i = isg, ieg
              emf1(i, j, k) = emf1(i, j, k)*dx1ah(i)*(-eta_rb(i))
              emf2(i, j, k) = emf2(i, j, k)*dx2ah(j)*g2ah(i)*(-eta_ra(i))
              emf3(i, j, k) = emf3(i, j, k)*dx3ah(k)*g31ah(i)*g32ah(j) &
                              *(-eta_ra(i))
            end do
          end do
        end do
      case default
        call finalstop(routinename, &
                       'This ohmic_exponent has not been implemented yet')
      end select
    else
      do k = ksg, keg
        do j = jsg, jeg
          do i = isg, ieg
            emf1(i, j, k) = emf1(i, j, k)*dx1ah(i)
            emf2(i, j, k) = emf2(i, j, k)*dx2ah(j)*g2ah(i)
            emf3(i, j, k) = emf3(i, j, k)*dx3ah(k)*g31ah(i)*g32ah(j)
          end do
        end do
      end do
    end if

    call writedata(emf1, routinename//'_1_emf1dx'//trim(text), 4)
    call writedata(emf2, routinename//'_1_emf2dx'//trim(text), 4)
    call writedata(emf3, routinename//'_1_emf3dx'//trim(text), 4)

    if (with_bc_emf_dx_oneroutine) then
      ! Alternatively, apply BCs to the EMFs multiplied by the line element,
      ! instead of applying them to the EMF components inside the routine CT
      call boundary('emf_dx', emf1, emf2, emf3)
      call writedata(emf1, routinename//'_2_emf1dx'//trim(text), 4)
      call writedata(emf2, routinename//'_2_emf2dx'//trim(text), 4)
      call writedata(emf3, routinename//'_2_emf3dx'//trim(text), 4)
    end if

    call writedata(bout1, routinename//'_0_bout1'//trim(text), 4)
    call writedata(bout2, routinename//'_0_bout2'//trim(text), 4)
    call writedata(bout3, routinename//'_0_bout3'//trim(text), 4)

    call writedata(b1, routinename//'_0_b1'//trim(text), 4)
    call writedata(b2, routinename//'_0_b2'//trim(text), 4)
    call writedata(b3, routinename//'_0_b3'//trim(text), 4)

    !-----------------------------------------------------------------------
    !
    !      The emf's are finished, and have their metric factors set in.
    !      Ready to update b1, b2, and b3 using the emf's.
    !  Since the same emf's are used throughout the grid, div(b) will be
    !  conserved numerically to within truncation error.
    !
    !      Coordinate-imposed boundary conditions (e.g., reflecting at x2a=0
    !  in ZRP, periodic at x3a=2*pi in RTP and ZRP) are communicated to the
    !  magnetic field by the emfs.  Both the old and new zone face areas are
    !  used to account for grid compression.
    !
    !-----------------------------------------------------------------------
    !-----------------------------> Update b1 <-----------------------------
    !-----------------------------------------------------------------------
    !
    imin = is                         ! CHANGE: this block is new.
    if (lg_firstnode1) then
      select case (ctboxtype)
      case (1, 11)
        imin = isgo
      case (3, 13, 14, 20, 21)
        imin = isg
      end select
    end if

    imax = iep1
    if (lg_lastnode1) then
      select case (ctboxtype)
      case (20, 21)
        imax = ieg
      end select
    end if                          ! CHANGE: end of new block

    do i = isg, ieg
      qty1(i) = g2a(i)*g31a(i)
      qty1ni(i) = g2ani(i)*g31ani(i)
    end do
    do j = jsg, jeg
      qty2(j) = g32b(j)*dx2a(j)
      qty2ni(j) = g32bni(j)*dx2ani(j)
    end do

    if (moving_grid1 .or. moving_grid2 .or. moving_grid3 .or. allow_moving) then
      !do k=ksgo,kego
      do k = ksg, keg
        kp1 = k + kone
        if (kp1 > keg) kp1 = kp1 - knac ! Periodic or ignorable BC
        do j = jsg, jego                ! CHANGE: was jsgo,jego
          jp1 = j + jone
          do i = imin, imax            ! CHANGE: was is,iep1
            bout1(i, j, k) = (b1(i, j, k)*qty1(i)*qty2(j)*dx3a(k) &
                              + delta_t*(emf3(i, jp1, k) - emf3(i, j, k) &
                                         - emf2(i, j, kp1) + emf2(i, j, k))) &
                             *qty1ni(i)*qty2ni(j)*dx3ani(k)
          end do
        end do
        if (l_lastnode2) then
          j = jeg
          jp1 = j + jone
          !kpi=k+knac/2 ; if(kpi>ke) kpi=kpi-knac
          do i = imin, imax
            ! emf3bc=-emf3(i,2*jep1-jp1,k  )  ! Reflective BC
            emf3bc = -bc_sign*emf3(i, 2*jep1 - jp1, k)  ! Reflective BC
            !emf3bc=-emf3(i,2*jep1-jp1,kpi )  ! kpi BC
            bout1(i, j, k) = (b1(i, j, k)*qty1(i)*qty2(j)*dx3a(k) &
                              + delta_t*(emf3bc - emf3(i, j, k) &
                                         - emf2(i, j, kp1) + emf2(i, j, k))) &
                             *qty1ni(i)*qty2ni(j)*dx3ani(k)
          end do
        end if
      end do
    else
      !do k=ksgo,kego
      do k = ksg, keg
        kp1 = k + kone
        if (kp1 > keg) kp1 = kp1 - knac ! Periodic or ignorable BC
        do j = jsg, jego                ! CHANGE: was jsgo,jego
          jp1 = j + jone
          do i = imin, imax            ! CHANGE: was is,iep1
            bout1(i, j, k) = b1(i, j, k) &
                             + delta_t*(emf3(i, jp1, k) - emf3(i, j, k) &
                                        - emf2(i, j, kp1) + emf2(i, j, k)) &
                             *qty1ni(i)*qty2ni(j)*dx3ani(k)
          end do
        end do
        if (l_lastnode2) then
          j = jeg
          jp1 = j + jone
          !kpi=k+knac/2 ; if(kpi>ke) kpi=kpi-knac
          do i = imin, imax
            !emf3bc=-emf3(i,2*jep1-jp1,k  )  ! Reflective BC
            emf3bc = -bc_sign*emf3(i, 2*jep1 - jp1, k)  ! Reflective BC
            !emf3bc=-emf3(i,2*jep1-jp1,kpi )  ! kpi BC
            bout1(i, j, k) = b1(i, j, k) &
                             + delta_t*(emf3bc - emf3(i, j, k) &
                                        - emf2(i, j, kp1) + emf2(i, j, k)) &
                             *qty1ni(i)*qty2ni(j)*dx3ani(k)
          end do
        end if
      end do
    end if

    !
    !-----------------------------------------------------------------------
    !-----------------------------> Update b2 <-----------------------------
    !-----------------------------------------------------------------------
    !
    imin = isgo                       ! CHANGE: this block is new.
    if (lg_firstnode1) then
      select case (ctboxtype)
      case (3, 13, 14, 20, 21)
        imin = isg
      end select
    end if

    jmax = jep1
    if (lg_lastnode2) then
      select case (ctboxtype)
      case (20, 21)
        jmax = jeg
      end select
    end if                          ! CHANGE: end of new block

    do i = isg, ieg
      qty1(i) = g31b(i)*dx1a(i)
      qty1ni(i) = g31bni(i)*dx1ani(i)
    end do

    if (moving_grid1 .or. moving_grid2 .or. moving_grid3 .or. allow_moving) then
      !do k=ksgo,kego
      do k = ksg, keg
        kp1 = k + kone
        if (kp1 > keg) kp1 = kp1 - knac ! Periodic or ignorable BC
        do j = jsg, jmax                  ! CHANGE: was js,jep1
          do i = imin, ieg               ! CHANGE: was isgo,iego
            ip1 = i + ione
            if (ip1 > ieg) ip1 = i   ! Outflow BC
            bout2(i, j, k) = (b2(i, j, k)*qty1(i)*g32a(j)*dx3a(k) &
                              + delta_t*(emf1(i, j, kp1) - emf1(i, j, k) &
                                         - emf3(ip1, j, k) + emf3(i, j, k))) &
                             *qty1ni(i)*g32ani(j)*dx3ani(k)
          end do
        end do
      end do
    else
      !do k=ksgo,kego
      do k = ksg, keg
        kp1 = k + kone
        if (kp1 > keg) kp1 = kp1 - knac ! Periodic or ignorable BC
        do j = jsg, jmax                  ! CHANGE: was js,jep1
          do i = imin, ieg               ! CHANGE: was isgo,iego
            ip1 = i + ione
            if (ip1 > ieg) ip1 = i   ! Outflow BC
            bout2(i, j, k) = b2(i, j, k) &
                             + delta_t*(emf1(i, j, kp1) - emf1(i, j, k) &
                                        - emf3(ip1, j, k) + emf3(i, j, k)) &
                             *qty1ni(i)*g32ani(j)*dx3ani(k)
          end do
        end do
      end do
    end if
    !
    !-----------------------------------------------------------------------
    !-----------------------------> Update b3 <-----------------------------
    !-----------------------------------------------------------------------
    !
    imin = isgo                       ! CHANGE: this block is new.
    if (lg_firstnode1) then
      select case (ctboxtype)
      case (3, 13, 14, 20, 21)
        imin = isg
      end select
    end if
    jmax = jego
    if (lg_lastnode2) then
      select case (ctboxtype)
      case (11, 13, 14)
        jmax = je
      case (21)
        jmax = jeg
      end select
    end if

    imax = iego
    if (lg_lastnode1) then
      select case (ctboxtype)
      case (14)
        imax = ie
      case (21)
        imax = ieg
      end select
    end if                          ! CHANGE: new block ends here

    do i = isg, ieg
      qty1(i) = g2b(i)*dx1a(i)
      qty1ni(i) = g2bni(i)*dx1ani(i)
    end do

    if (moving_grid1 .or. moving_grid2 .or. moving_grid3 .or. allow_moving) then
      !do k=ks,kep1
      do k = ksg, keg
        !kpi=k+knac/2 ; if(kpi>ke) kpi=kpi-knac
        do j = jsg, jmax                ! CHANGE was: jsgo,jego
          jp1 = j + jone
          !if(jp1>jeg) then ! kpi BC
          !   jp1=2*jep1-jp1
          !   kplace=kpi
          !else
          !   kplace=k
          !end if
          if (jp1 > jeg) jp1 = 2*jep1 - jp1  ! Reflective BC
          kplace = k
          do i = imin, imax            ! CHANGE was: isgo,iego
            ip1 = i + ione
            if (ip1 > ieg) ip1 = i  ! Outflow BC
            bout3(i, j, k) = (b3(i, j, k)*qty1(i)*dx2a(j) &
                              + delta_t*(emf2(ip1, j, k) - emf2(i, j, k) &
                                         - emf1(i, jp1, kplace) &
                                         + emf1(i, j, k))) &
                             *qty1ni(i)*dx2ani(j)
          end do
        end do
      end do
    else
      !do k=ks,kep1
      do k = ksg, keg
        !kpi=k+knac/2 ; if(kpi>ke) kpi=kpi-knac
        do j = jsg, jmax                ! CHANGE was: jsgo,jego
          jp1 = j + jone
          !if(jp1>jeg) then ! kpi BC
          !   jp1=2*jep1-jp1
          !   kplace=kpi
          !else
          !   kplace=k
          !end if
          if (jp1 > jeg) jp1 = 2*jep1 - jp1  ! Reflective BC
          kplace = k
          do i = imin, imax            ! CHANGE was: isgo,iego
            ip1 = i + ione
            if (ip1 > ieg) ip1 = i  ! Outflow BC
            bout3(i, j, k) = b3(i, j, k) + delta_t &
                             *(emf2(ip1, j, k) - emf2(i, j, k) &
                               - emf1(i, jp1, kplace) + emf1(i, j, k)) &
                             *qty1ni(i)*dx2ani(j)
          end do
        end do
      end do
    end if

    if (withb3bc) then
      call boundary('b3')
    end if

    ! Extrapolation of bphi in certain ghost zones  ! CHANGE: new block
    ! Useful for certain axisymmetric problems.
    ! *Must* be rejected unless knac == 0.
    if (knac == 0) then
      if (lg_lastnode2) then
        select case (ctboxtype)
        case (10, 11, 13, 14)
          do k = ks, kep1
            do j = jmax + 1, jeg
              do i = isg, ieg
                bout3(i, j, k) = bout3(i, jmax, k)
              end do
            end do
          end do
        end select
      end if
      if (lg_lastnode1) then
        select case (ctboxtype)
        case (14)
          do k = ks, kep1
            do j = jsg, jeg
              do i = imax + 1, ieg
                bout3(i, j, k) = bout3(imax, j, k)
              end do
            end do
          end do
        end select
      end if                               ! CHANGE: new block ends
      if (lg_firstnode1) then
        select case (ctboxtype)
        case (15)
          do k = ks, kep1
            do j = jsg, jeg
              do i = isg, ism1
                bout3(i, j, k) = zro
              end do
            end do
          end do
        end select
      end if                               ! CHANGE: new block ends
      !
    end if
    !
    call writemessage(routinename//'_bupdate', 2)
    call writedata(bout1, routinename//'_1_bout1'//trim(text), 4)
    call writedata(bout2, routinename//'_1_bout2'//trim(text), 4)
    call writedata(bout3, routinename//'_1_bout3'//trim(text), 4)

    call writedata(b1, routinename//'_1_b1'//trim(text), 4)
    call writedata(b2, routinename//'_1_b2'//trim(text), 4)
    call writedata(b3, routinename//'_1_b3'//trim(text), 4)

#ifdef MPI
    call mpifield(bout1)
    call mpifield(bout2)
    call mpifield(bout3)
    !
    call writemessage(routinename//'_bchain', 2)
    call writedata(bout1, routinename//'_2_bout1'//trim(text), 4)
    call writedata(bout2, routinename//'_2_bout2'//trim(text), 4)
    call writedata(bout3, routinename//'_2_bout3'//trim(text), 4)

    call writedata(b1, routinename//'_2_b1'//trim(text), 4)
    call writedata(b2, routinename//'_2_b2'//trim(text), 4)
    call writedata(b3, routinename//'_2_b3'//trim(text), 4)
#endif /* MPI */

  END SUBROUTINE update_b
  !
  !=======================================================================
  !
  SUBROUTINE ct_2d
    CHARACTER(LEN=*), PARAMETER :: routinename = 'z_ct_2d'
    if (.not. (withv3 .and. withbp .and. withb3)) return
    call finalstop(routinename, &
                   'Full Zeus2D algorithm not yet implemented for this case')
  END SUBROUTINE ct_2d
  !
  !=======================================================================
  !
  SUBROUTINE emfs(emf1, emf2, emf3, vi1, vi2, vi3)
    !
    !    dac:zeus3d.emfs <----------------------- evaluates emfs without MoC
    !    from jms:zeus2d.ct                                   november, 1990
    !
    !    written by: David Clarke
    !    modified 1:
    !
    !  PURPOSE:  This subroutine evaluates the emfs to be used by CT in the
    !  most straight forward manner.  The velocities are zone-averaged while
    !  the magnetic fields are interpolated and upwinded with respect to the
    !  flow velocity.
    !
    !  INPUT VARIABLES:
    !
    !  OUTPUT VARIABLES:
    !    emf1      emf along the 1-edges of the grid (= v2*b3 - v3*b2)
    !    emf2      emf along the 2-edges of the grid (= v3*b1 - v1*b3)
    !    emf3      emf along the 3-edges of the grid (= v1*b2 - v2*b1)
    !
    !-----------------------------------------------------------------------
    !
    REALNUM, DIMENSION(:, :, :), INTENT(OUT) :: emf1, emf2, emf3
    REALNUM, DIMENSION(:, :, :), INTENT(IN)  :: vi1, vi2, vi3
    !
    CHARACTER(LEN=*), PARAMETER :: routinename = 'z_emfs'
    !
    INTEGER i, j, k
    INTEGER im1, jm1, km1
    !
    REALNUM, POINTER, SAVE :: vel21(:, :, :) => null(), vel31(:, :, :) => null()
    REALNUM, POINTER, SAVE :: vel32(:, :, :) => null(), vel12(:, :, :) => null()
    REALNUM, POINTER, SAVE :: vel13(:, :, :) => null(), vel23(:, :, :) => null()

    REALNUM, POINTER, SAVE :: b3twid2(:, :, :) => null(), &
                              b2twid3(:, :, :) => null()
    REALNUM, POINTER, SAVE :: b1twid3(:, :, :) => null(), &
                              b3twid1(:, :, :) => null()
    REALNUM, POINTER, SAVE :: b2twid1(:, :, :) => null(), &
                              b1twid2(:, :, :) => null()

    !
    !-----------------------------------------------------------------------
    !
    !-----------------------------------------------------------------------
    !---- 1.  emf1 ---------------------------------------------------------
    !-----------------------------------------------------------------------
    !
    !      Compute the two-point arithmetic averages for "vi2" and "vi3"
    !  necessary to place them on the 1-edges.
    !

    ! RFK: it is possible to reduce the number of arrays allocated
    ! here; see Zeus3D or ZeusMP for that.
    call get_memory(vel21); call get_memory(vel31)
    call get_memory(b3twid2); call get_memory(b2twid3)
    do k = ks, kep1
      km1 = k - kone
      do j = js, jep1
        jm1 = j - jone
        do i = is, ie
          vel21(i, j, k) = haf*(vi2(i, j, k) + vi2(i, j, km1))
          vel31(i, j, k) = haf*(vi3(i, j, k) + vi3(i, jm1, k))
        end do
      end do
    end do
    call writedata(vel21, routinename//'_vel21', 4)
    call writedata(vel31, routinename//'_vel31', 4)
    !
    !      For stability, interpolate "b3" in the 2-direction and "b2" in
    !  the 3-direction to place them at the 1-edges.
    !
    !
    ! Avoid aliasing arrays through subroutine calls.
    !
    call x2zc3d(b3, vel21, ks, is, kep1, ie, iordb3, istpb3, g2b, g2bi, b3twid2)
    ! RFK: wrong call of metric in Zeus-3D
    !      call x3zc3d ( b2, vel31, is, js, ie, jep1, iordb2, istpb2
    !    &             , g31b, g32a, g31bi, g32ai, b2twid3, p )
    call x3zc3d(b2, vel31, is, js, ie, jep1, iordb2, istpb2, g31b, g31bi, &
                g32a, g32ai, b2twid3)
    call writedata(b3twid2, routinename//'_b3twid2', 4)
    call writedata(b2twid3, routinename//'_b2twid3', 4)
    !
    !      Compute the vi2*b3 and the vi3*b2 terms in "emf1".
    !
    do k = ks, kep1
      do j = js, jep1
        do i = is, ie
          emf1(i, j, k) = (vel21(i, j, k) - vg2(j))*b3twid2(i, j, k) &
                          - (vel31(i, j, k) - vg3(k))*b2twid3(i, j, k)
        end do
      end do
    end do
    call release_memory(b2twid3); call release_memory(b3twid2)
    call release_memory(vel31); call release_memory(vel21)
    call writedata(emf1, routinename//'_emf1', 4)
    !
    !-----------------------------------------------------------------------
    !---- 2.  emf2 ---------------------------------------------------------
    !-----------------------------------------------------------------------
    !
    !      Compute the two-point arithmetic averages for "vi3" and "vi1"
    !  necessary to place them at the 2-edges.
    !
    call get_memory(vel32); call get_memory(vel12)
    call get_memory(b1twid3); call get_memory(b3twid1)
    do k = ks, kep1
      km1 = k - kone
      do j = js, je
        do i = is, iep1
          im1 = i - ione
          vel32(i, j, k) = haf*(vi3(i, j, k) + vi3(im1, j, k))
          vel12(i, j, k) = haf*(vi1(i, j, k) + vi1(i, j, km1))
        end do
      end do
    end do
    call writedata(vel32, routinename//'_vel32', 4)
    call writedata(vel12, routinename//'_vel12', 4)
    !
    !      For stability, interpolate "b1" in the 3-direction and "b3" in
    !  the 1-direction to place them at the 2-edges.
    !
    !
    ! Avoid aliasing arrays through subroutine calls.
    !
    ! RFK: wrong call of metric in Zeus-3D
    !      call x3zc3d ( b1, vel32, is, js, iep1, je, iordb1, istpb1
    !    &             , g31a, g32b, g31ai, g32bi, b1twid3, p )
    call x3zc3d(b1, vel32, is, js, iep1, je, iordb1, istpb1, g31a, g31ai, &
                g32b, g32bi, b1twid3)
    call x1zc3d(b3, vel12, js, ks, je, kep1, iordb3, istpb3, b3twid1)
    call writedata(b1twid3, routinename//'_b1w3', 4)
    call writedata(b3twid1, routinename//'_b3w1', 4)
    !
    !      Compute the vi3*b1 and the vi1*b3 terms in "emf2".
    !
    do k = ks, kep1
      do j = js, je
        do i = is, iep1
          emf2(i, j, k) = (vel32(i, j, k) - vg3(k))*b1twid3(i, j, k) &
                          - (vel12(i, j, k) - vg1(i))*b3twid1(i, j, k)
        end do
      end do
    end do
    call release_memory(b3twid1); call release_memory(b1twid3)
    call release_memory(vel12); call release_memory(vel32)
    call writedata(emf2, routinename//'_emf2', 4)
    !
    !-----------------------------------------------------------------------
    !---- 3.  emf3 ---------------------------------------------------------
    !-----------------------------------------------------------------------
    !
    !      Compute the two-point arithmetic averages for "vi1" and "vi2"
    !  necessary to place them at the 3-edges.
    !
    call get_memory(vel13); call get_memory(vel23)
    call get_memory(b2twid1); call get_memory(b1twid2)
    do k = ks, ke
      do j = js, jep1
        jm1 = j - jone
        do i = is, iep1
          im1 = i - ione
          vel13(i, j, k) = haf*(vi1(i, j, k) + vi1(i, jm1, k))
          vel23(i, j, k) = haf*(vi2(i, j, k) + vi2(im1, j, k))
        end do
      end do
    end do
    call writedata(vel13, routinename//'_vel13', 4)
    call writedata(vel23, routinename//'_vel23', 4)
    !
    !      For stability, interpolate "b2" in the 1-direction and "b1" in
    !  the 2-direction to place them at the 3-edges.
    !
    !
    ! Avoid aliasing arrays through subroutine calls.
    !
    call x1zc3d(b2, vel13, js, ks, jep1, ke, iordb2, istpb2, b2twid1)
    call x2zc3d(b1, vel23, ks, is, ke, iep1, iordb1, istpb1, g2a, g2ai, b1twid2)
    call writedata(b2twid1, routinename//'_b2w1', 4)
    call writedata(b1twid2, routinename//'_b1w2', 4)
    !
    !      Compute the vi1*b2 and the vi2*b1 terms in "emf3".
    !
    do k = ks, ke
      do j = js, jep1
        do i = is, iep1
          emf3(i, j, k) = (vel13(i, j, k) - vg1(i))*b2twid1(i, j, k) &
                          - (vel23(i, j, k) - vg2(j))*b1twid2(i, j, k)
        end do
      end do
    end do
    call release_memory(b1twid2); call release_memory(b2twid1)
    call release_memory(vel23); call release_memory(vel13)

    call writedata(emf3, routinename//'_emf3', 4)
  END SUBROUTINE emfs
  !
  !=======================================================================
  !
  SUBROUTINE lorentz(srd1, srd2, srd3, v_input1, v_input2, v_input3)
    !
    !    dac:zeus3d.lorentz <----- MoC estimate of transverse lorentz forces
    !                                                          october, 1992
    !
    !    written by: David Clarke
    !    modified 1:
    !
    !  PURPOSE:  Uses the Method of Characteristics (MoC) to compute the
    !  transverse components of the Lorentz force and then accelerate the
    !  velocities accordingly.  After a suggestion by John Hawley, these
    !  source terms are computed separately from the MoC estimate of the
    !  emfs (MOCEMFS) and before the magnetic field update.  This improves
    !  stability of multi-dimensional Alfven waves.
    !
    !  MOCEMFS solves the induction equation without the use of operator
    !  splitting.  Thus, the relavant characteristic velocities are v+/-va
    !  and includes the flow velocity v (from the advection term v grad(B))
    !  and the alfven velocity va (from the induction term va grad(B)).
    !  Conversely, Euler's equation is operator split, and the advection
    !  term (v grad(v)) is handled elsewhere (MOMX*).  Thus, the effective
    !  characteristic velocities when estimating the transverse Lorentz
    !  terms is simply +/-va (from the induction term va grad(v)).
    !
    !  See comments in MOCEMFS for further ideas regarding the Method of
    !  Characteristics.
    !
    !  INPUT VARIABLES: srd1, srd2, srd3
    !
    !  OUTPUT VARIABLES:
    !
    !  LOCAL VARIABLES:
    !    bave      spatially averaged density at edge
    !    srdpi     1.0/srd[n] along the plus  alfven characteristic (A+)
    !    srdmi     1.0/srd[n] along the minus alfven characteristic (A-)
    !    valp      characteristic velocity along A+ (-va)
    !    valm      characteristic velocity along A- (+va)
    !    vpal      velocity interpolated to the time-centred footpoint of A+
    !    vmal      velocity interpolated to the time-centred footpoint of A-
    !    bpal      B-field  interpolated to the time-centred footpoint of A+
    !    bmal      B-field  interpolated to the time-centred footpoint of A-
    !    bstar     MoC estimate of b[n] used to evaluate st[n], n=1,2,3
    !    st[n]     Transverse Lorentz force on the [n]-velocity, n=1,2,3
    !
    !-----------------------------------------------------------------------
    !
    REALNUM, DIMENSION(:, :, :), INTENT(IN) :: srd1, srd2, srd3
    REALNUM, DIMENSION(:, :, :), INTENT(INOUT) :: v_input1, v_input2, v_input3
    !
    CHARACTER(LEN=*), PARAMETER :: routinename = 'z_lorentz'
    !
    INTEGER i, j, k
    INTEGER im1, ip1, jm1, jp1, km1, kp1
    REALNUM absb, q1
    !
    REALNUM, DIMENSION(ijknsa:ijknea) :: &
      bave, srdpi, srdmi, valp, valm, vtmp, btmp, &
      vpal, vmal, bpal, bmal, bstar
    REALNUM, POINTER, SAVE :: st1(:, :, :) => null(), st2(:, :, :) => null(), &
                              st3(:, :, :) => null()

    if (imhd /= 3 .and. imhd /= 2) return

    call get_memory(st1)
    call get_memory(st2)
    call get_memory(st3)
    !
    !-----------------------------------------------------------------------
    !
    !      Initialize source terms to zero.
    !
    do k = ksm1, kep1
      do j = jsm1, jep1
        do i = ism1, iep1
          st1(i, j, k) = zro
          st2(i, j, k) = zro
          st3(i, j, k) = zro
        end do
      end do
    end do
    !
    !-----------------------------------------------------------------------
    !---- 1-force ----------------------------------------------------------
    !-----------------------------------------------------------------------
    !
    if (jnac /= 0) then
      !
      !  By following the Alfven velocity in the 2-direction, evaluate
      !  "bstar" from "b1" to estimate the 2-transverse Lorentz force.
      !
      do k = ks, ke
        do i = is, iep1
          im1 = i - ione
          !
          !  Select an effective density and determine the Alfven speed for
          !  each Alfven characteristic in the 2-direction.
          !
          do j = js, jep1
            jm1 = j - jone
            bave(j) = haf*(b2(i, j, k) + b2(im1, j, k))
            absb = abs(bave(j))
            srdpi(j) = one/srd1(i, j, k)
            srdmi(j) = one/srd1(i, jm1, k)
            valp(j) = -absb*srdpi(j)
            valm(j) = absb*srdmi(j)
          end do
          !
          !  Interpolate 1-D vectors of "v_input1" and "b1" in the 2-direction to
          !  the footpoints of both Alfven characteristics.
          !
          do j = jsm2, jep2
            vtmp(j) = v_input1(i, j, k) - vg1(i)
            btmp(j) = b1(i, j, k)
            !**            vpal(j) = zro
            !**            vmal(j) = zro
            !**            bpal(j) = zro
            !**            bmal(j) = zro
          end do
          call x2zc1d(vtmp, valp, valm, iords1, istps1, k, i, g2a, g2ai, &
                      vpal, vmal)
          call x2zc1d(btmp, valp, valm, iordb1, istpb1, k, i, g2a, g2ai, &
                      bpal, bmal)
          !
          !  Evaluate "bstar" by solving the characteristic equations.
          !
          do j = js, jep1
            q1 = sign(one, bave(j))
            bstar(j) = (bpal(j)*srdpi(j) + bmal(j)*srdmi(j) &
                        + q1*(vpal(j) - vmal(j))) &
                       /(srdpi(j) + srdmi(j))
          end do
          !
          !  Evaluate transverse Lorentz force.
          !
          do j = js, je
            jp1 = j + jone
            st1(i, j, k) = (bave(jp1) + bave(j))*g2ai(i) &
                           *(bstar(jp1) - bstar(j))*dx2ai(j)
          end do
        end do
      end do
    end if
    !
    !-----------------------------------------------------------------------
    if (knac /= 0) then
      !
      !  By following the Alfven velocity in the 3-direction, evaluate
      !  "bstar" from "b1" to estimate the 3-transverse Lorentz force.
      !
      do j = js, je
        do i = is, iep1
          im1 = i - ione
          !
          !  Select an effective density and determine the Alfven speed for
          !  each Alfven characteristic in the 3-direction.
          !
          do k = ks, kep1
            km1 = k - kone
            bave(k) = haf*(b3(i, j, k) + b3(im1, j, k))
            absb = abs(bave(k))
            srdpi(k) = one/srd1(i, j, k)
            srdmi(k) = one/srd1(i, j, km1)
            valp(k) = -absb*srdpi(k)
            valm(k) = absb*srdmi(k)
          end do
          !
          !  Interpolate 1-D vectors of "v_input1" and "b1" in the 3-direction to
          !  the footpoints of both Alfven characteristics.
          !
          do k = ksm2, kep2
            vtmp(k) = v_input1(i, j, k) - vg1(i)
            btmp(k) = b1(i, j, k)
            !**            vpal(k) = zro
            !**            vmal(k) = zro
            !**            bpal(k) = zro
            !**            bmal(k) = zro
          end do
          call x3zc1d(vtmp, valp, valm, iords1, istps1, i, j, g31a, g31ai, &
                      g32b, g32bi, vpal, vmal)
          call x3zc1d(btmp, valp, valm, iordb1, istpb1, i, j, g31a, g31ai, &
                      g32b, g32bi, bpal, bmal)
          !
          !  Evaluate "bstar" by solving the characteristic equations.
          !
          do k = ks, kep1
            q1 = sign(one, bave(k))
            bstar(k) = (bpal(k)*srdpi(k) + bmal(k)*srdmi(k) &
                        + q1*(vpal(k) - vmal(k))) &
                       /(srdpi(k) + srdmi(k))
          end do
          !
          !  Evaluate transverse Lorentz force.
          !
          q1 = g31ai(i)*g32bi(j)
          do k = ks, ke
            kp1 = k + kone
            st1(i, j, k) = st1(i, j, k) &
                           + (bave(kp1) + bave(k))*q1 &
                           *(bstar(kp1) - bstar(k))*dx3ai(k)
          end do
        end do
      end do
    end if
    !
    !-----------------------------------------------------------------------
    !---- 2-force ----------------------------------------------------------
    !-----------------------------------------------------------------------
    !
    if (knac /= 0) then
      do j = js, jep1
        jm1 = j - jone
        !
        !  By following the Alfven velocity in the 3-direction, evaluate
        !  "bstar" from "b2" to estimate the 3-transverse Lorentz force.
        !
        do i = is, ie
          !
          !  Select an effective density and determine the Alfven speed for
          !  each Alfven characteristic in the 3-direction.
          !
          do k = ks, kep1
            km1 = k - kone
            bave(k) = haf*(b3(i, j, k) + b3(i, jm1, k))
            absb = abs(bave(k))
            srdpi(k) = one/srd2(i, j, k)
            srdmi(k) = one/srd2(i, j, km1)
            valp(k) = -absb*srdpi(k)
            valm(k) = absb*srdmi(k)
          end do
          !
          !  Interpolate 1-D vectors of "v_input2" and "b2" in the 3-direction to
          !  the footpoints of both Alfven characteristics.
          !
          do k = ksm2, kep2
            vtmp(k) = v_input2(i, j, k) - vg2(j)
            btmp(k) = b2(i, j, k)
            !**            vpal(k) = zro
            !**            vmal(k) = zro
            !**            bpal(k) = zro
            !**            bmal(k) = zro
          end do
          call x3zc1d(vtmp, valp, valm, iords2, istps2, i, j, g31b, g31bi, &
                      g32a, g32ai, vpal, vmal)
          call x3zc1d(btmp, valp, valm, iordb2, istpb2, i, j, g31b, g31bi, &
                      g32a, g32ai, bpal, bmal)
          !
          !  Evaluate "bstar" by solving the characteristic equations.
          !
          do k = ks, kep1
            q1 = sign(one, bave(k))
            bstar(k) = (bpal(k)*srdpi(k) + bmal(k)*srdmi(k) &
                        + q1*(vpal(k) - vmal(k))) &
                       /(srdpi(k) + srdmi(k))
          end do
          !
          !  Evaluate transverse Lorentz force.
          !
          q1 = g31bi(i)*g32ai(j)
          do k = ks, ke
            kp1 = k + kone
            st2(i, j, k) = (bave(kp1) + bave(k))*q1 &
                           *(bstar(kp1) - bstar(k))*dx3ai(k)
          end do
        end do
      end do
    end if
    !
    !-----------------------------------------------------------------------
    if (inac /= 0) then
      !
      !  By following the Alfven velocity in the 1-direction, evaluate
      !  "bstar" from "b2" to estimate the 1-transverse Lorentz force.
      !
      do k = ks, ke
        do j = js, jep1
          jm1 = j - jone
          !
          !  Select an effective density and determine the Alfven speed for
          !  each Alfven characteristic in the 1-direction.
          !
          do i = is, iep1
            im1 = i - ione
            bave(i) = haf*(b1(i, j, k) + b1(i, jm1, k))
            absb = abs(bave(i))
            srdpi(i) = one/srd2(i, j, k)
            srdmi(i) = one/srd2(im1, j, k)
            valp(i) = -absb*srdpi(i)
            valm(i) = absb*srdmi(i)
          end do
          !
          !  Interpolate 1-D vectors of "v_input2" and "b2" in the 1-direction to
          !  the footpoints of both Alfven characteristics.
          !
          do i = ism2, iep2
            vtmp(i) = v_input2(i, j, k) - vg2(j)
            btmp(i) = b2(i, j, k)
            !**            vpal(i) = zro
            !**            vmal(i) = zro
            !**            bpal(i) = zro
            !**            bmal(i) = zro
          end do
          call x1zc1d(vtmp, valp, valm, iords2, istps2, j, k, vpal, vmal)
          call x1zc1d(btmp, valp, valm, iordb2, istpb2, j, k, bpal, bmal)
          !
          !  Evaluate "bstar" by solving the characteristic equations.
          !
          do i = is, iep1
            q1 = sign(one, bave(i))
            bstar(i) = (bpal(i)*srdpi(i) + bmal(i)*srdmi(i) &
                        + q1*(vpal(i) - vmal(i)))*g2a(i) &
                       /(srdpi(i) + srdmi(i))
          end do
          !
          !  Evaluate transverse Lorentz force.  Note that the metric
          !  factor "g2a" has been absorbed into "bstar".
          !
          do i = is, ie
            ip1 = i + ione
            st2(i, j, k) = st2(i, j, k) &
                           + (bave(ip1) + bave(i))*g2bi(i) &
                           *(bstar(ip1) - bstar(i))*dx1ai(i)
          end do
        end do
      end do
    end if
    !
    !-----------------------------------------------------------------------
    !---- 3-force ----------------------------------------------------------
    !-----------------------------------------------------------------------
    !
    do k = ks, kep1
      km1 = k - kone
      if (inac /= 0) then
        !
        !  By following the Alfven velocity in the 1-direction, evaluate
        !  "bstar" from "b3" to estimate the 1-transverse Lorentz force.
        !
        do j = js, je
          !
          !  Select an effective density and determine the Alfven speed for
          !  each Alfven characteristic in the 1-direction.
          !
          do i = is, iep1
            im1 = i - ione
            bave(i) = haf*(b1(i, j, k) + b1(i, j, km1))
            absb = abs(bave(i))
            srdpi(i) = one/srd3(i, j, k)
            srdmi(i) = one/srd3(im1, j, k)
            valp(i) = -absb*srdpi(i)
            valm(i) = absb*srdmi(i)
          end do
          !
          !  Interpolate 1-D vectors of "v_input3" and "b3" in the 1-direction to
          !  the footpoints of both Alfven characteristics.
          !
          do i = ism2, iep2
            vtmp(i) = v_input3(i, j, k) - vg3(k)
            btmp(i) = b3(i, j, k)
            !**            vpal(i) = zro
            !**            vmal(i) = zro
            !**            bpal(i) = zro
            !**            bmal(i) = zro
          end do
          call x1zc1d(vtmp, valp, valm, iords3, istps3, j, k, vpal, vmal)
          call x1zc1d(btmp, valp, valm, iordb3, istpb3, j, k, bpal, bmal)
          !
          !  Evaluate "bstar" by solving the characteristic equations.
          !
          do i = is, iep1
            q1 = sign(one, bave(i))
            bstar(i) = (bpal(i)*srdpi(i) + bmal(i)*srdmi(i) &
                        + q1*(vpal(i) - vmal(i)))*g31a(i) &
                       /(srdpi(i) + srdmi(i))
          end do
          !
          !  Evaluate transverse Lorentz force.  Note that the metric
          !  factor "g31a" has been absorbed into "bstar".
          !
          do i = is, ie
            ip1 = i + ione
            st3(i, j, k) = (bave(ip1) + bave(i))*g31bi(i) &
                           *(bstar(ip1) - bstar(i))*dx1ai(i)
          end do
        end do
      end if
      !
      !-----------------------------------------------------------------------
      if (jnac /= 0) then
        !
        !  By following the Alfven velocity in the 2-direction, evaluate
        !  "bstar" from "b3" to estimate the 2-transverse Lorentz force.
        !
        do i = is, ie
          !
          !  Select an effective density and determine the Alfven speed for
          !  each Alfven characteristic in the 2-direction.
          !
          do j = js, jep1
            jm1 = j - jone
            bave(j) = haf*(b2(i, j, k) + b2(i, j, km1))
            absb = abs(bave(j))
            srdpi(j) = one/srd3(i, j, k)
            srdmi(j) = one/srd3(i, jm1, k)
            valp(j) = -absb*srdpi(j)
            valm(j) = absb*srdmi(j)
          end do
          !
          !  Interpolate 1-D vectors of "v_input3" and "b3" in the 2-direction to
          !  the footpoints of both Alfven characteristics.
          !
          do j = jsm2, jep2
            vtmp(j) = v_input3(i, j, k) - vg3(k)
            btmp(j) = b3(i, j, k)
            !**            vpal(j) = zro
            !**            vmal(j) = zro
            !**            bpal(j) = zro
            !**            bmal(j) = zro
          end do
          call x2zc1d(vtmp, valp, valm, iords3, istps3, k, i, g2b, g2bi, &
                      vpal, vmal)
          call x2zc1d(btmp, valp, valm, iordb3, istpb3, k, i, g2b, g2bi, &
                      bpal, bmal)
          !
          !  Evaluate "bstar" by solving the characteristic equations.
          !
          do j = js, jep1
            q1 = sign(one, bave(j))
            bstar(j) = (bpal(j)*srdpi(j) + bmal(j)*srdmi(j) &
                        + q1*(vpal(j) - vmal(j)))*g32a(j) &
                       /(srdpi(j) + srdmi(j))
          end do
          !
          !  Evaluate transverse Lorentz force.  Note that the metric
          !  factor "g32a" has been absorbed into "bstar".
          !
          do j = js, je
            jp1 = j + jone
            st3(i, j, k) = st3(i, j, k) &
                           + (bave(jp1) + bave(j))*g2bi(i)*g32bi(j) &
                           *(bstar(jp1) - bstar(j))*dx2ai(j)
            !JH  changed g2ai to g2bi since v_input3 lives at x1b
            !RFK ZEUS-MP here has a check for x1a(i)==0 in RTP geometry
          end do
        end do
      end if
    end do
    !
    !-----------------------------------------------------------------------
    !
    !      Accelerate fluid velocities and set boundaries.
    !
    q1 = haf*dt
    do k = ks, ke
      do j = js, je
        do i = is, ie
          v1(i, j, k) = v1(i, j, k) + q1*st1(i, j, k)/srd1(i, j, k)**2
          v2(i, j, k) = v2(i, j, k) + q1*st2(i, j, k)/srd2(i, j, k)**2
          v3(i, j, k) = v3(i, j, k) + q1*st3(i, j, k)/srd3(i, j, k)**2
        end do
      end do
    end do
    !
    call writedata(st1, routinename//'_st1', 4)
    call writedata(st2, routinename//'_st2', 4)
    call writedata(st3, routinename//'_st3', 4)
    call writedata(v1, routinename//'_v1', 4)
    call writedata(v2, routinename//'_v2', 4)
    call writedata(v3, routinename//'_v3', 4)
    call writedata(srd1, routinename//'_srd1', 4)
    call writedata(srd2, routinename//'_srd2', 4)
    call writedata(srd3, routinename//'_srd3', 4)
    !
    call boundary('v1')
    call boundary('v2')
    call boundary('v3')
    call release_memory(st3)
    call release_memory(st2)
    call release_memory(st1)
    !
    call writedata(v1, routinename//'_2_v1', 4)
    call writedata(v2, routinename//'_2_v2', 4)
    call writedata(v3, routinename//'_2_v3', 4)
    !
  END SUBROUTINE lorentz
  !
  !=======================================================================
  !
  SUBROUTINE mocemfs(emf1, emf2, emf3, srd1, srd2, srd3, vi1, vi2, vi3)
    !
    !    dac:zeus3d.mocemfs <-------------------------- MoC estimate of emfs
    !                                                          october, 1992
    !
    !    written by: David Clarke
    !    modified 1:
    !
    !  PURPOSE:  Description is the same as in presented for the
    !    subroutine HSMOC below, here using the older formulation
    !    of the algorithm, without the modifications inspired by
    !    Hawley and Stone.
    !
    !  INPUT, OUTPUT, AND LOCAL VARIABLES: Description is the same as in
    !    HSMOC, except for the local 2-D variables, which are not used here.
    !
    !-----------------------------------------------------------------------
    !
    REALNUM, DIMENSION(:, :, :), INTENT(OUT) :: emf1, emf2, emf3
    REALNUM, DIMENSION(:, :, :), INTENT(IN) :: srd1, srd2, srd3
    REALNUM, DIMENSION(:, :, :), INTENT(IN)  :: vi1, vi2, vi3
    !
    CHARACTER(LEN=*), PARAMETER :: routinename = 'z_mocemfs'
    !
    INTEGER i, j, k
    INTEGER im1, jm1, km1
    REALNUM vave, absb, aave, sgnp, sgnm, q1, q2, src
    !
    REALNUM, DIMENSION(ijknsa:ijknea) :: &
      bave, srdp, srdm, srdpi, srdmi, vchp, vchm, vtmp, btmp, &
      vpch, vmch, bpch, bmch, vsnm1, bsnm1
    !
    REALNUM, POINTER, SAVE :: vsnp1(:, :, :) => null(), bsnp1(:, :, :) => null()
    call get_memory(vsnp1); call get_memory(bsnp1)
    ! RFK: Fiedler had equivalenced vsnp1 to emf3, this reduces the
    ! number of worker arrays needed here by one.

    !
    !-----------------------------------------------------------------------
    !---- 1.  emf1 ---------------------------------------------------------
    !-----------------------------------------------------------------------
    !
    !  By following the characteristics of the flow in the 3-direction,
    !  determine values for "vi2" and "b2" ("vsnp1" and "bsnp1") to be used
    !  in evaluating "emf1".
    !
    do j = js, jep1
      jm1 = j - jone
      do i = is, ie
        if (knac == 0) then
          vsnp1(i, j, k) = vi2(i, j, k) - vg2(j)
          bsnp1(i, j, k) = b2(i, j, k)
        else
          !
          !  Select an effective density and determine the characteristic
          !  velocity for each characteristic in the 3-direction.
          !
          do k = ks, kep1
            km1 = k - kone
            vave = haf*(vi3(i, j, k) + vi3(i, jm1, k)) - vg3(k)
            bave(k) = haf*(b3(i, j, k) + b3(i, jm1, k))
            absb = abs(bave(k))
            aave = haf*absb*(srd2(i, j, k) + srd2(i, j, km1)) &
                   /(srd2(i, j, k)*srd2(i, j, km1))
            sgnp = sign(haf, vave - aave)
            sgnm = sign(haf, vave + aave)
            srdp(k) = (haf + sgnp)*srd2(i, j, km1) &
                      + (haf - sgnp)*srd2(i, j, k)
            srdm(k) = (haf + sgnm)*srd2(i, j, km1) &
                      + (haf - sgnm)*srd2(i, j, k)
            srdpi(k) = one/srdp(k)
            srdmi(k) = one/srdm(k)
            vchp(k) = vave - absb*srdpi(k)
            vchm(k) = vave + absb*srdmi(k)
          end do
          !
          !  Interpolate 1-D vectors of "vi2" and "b2" in the 3-direction to
          !  the footpoints of both characteristics.
          !
          do k = ksm2, kep2
            vtmp(k) = vi2(i, j, k) - vg2(j)
            btmp(k) = b2(i, j, k)
            !**            vpch(k) = zro
            !**            vmch(k) = zro
            !**            bpch(k) = zro
            !**            bmch(k) = zro
          end do
          call x3zc1d(vtmp, vchp, vchm, iords2, istps2, i, j, g31b, g31bi, &
                      g32a, g32ai, vpch, vmch)
          call x3zc1d(btmp, vchp, vchm, iordb2, istpb2, i, j, g31b, g31bi, &
                      g32a, g32ai, bpch, bmch)
          !
          !  Evaluate "vsnp1" and "bsnp1" by solving the characteristic
          !  equations.  There is no source term since the metric factor "g2" has
          !  no explicit dependence on x3.
          !
          do k = ks, kep1
            q2 = sign(one, bave(k))
            vsnp1(i, j, k) = (vpch(k)*srdp(k) + vmch(k)*srdm(k) &
                              + q2*(bpch(k) - bmch(k))) &
                             /(srdp(k) + srdm(k))
            bsnp1(i, j, k) = (bpch(k)*srdpi(k) + bmch(k)*srdmi(k) &
                              + q2*(vpch(k) - vmch(k))) &
                             /(srdpi(k) + srdmi(k))
          end do
        end if
      end do
    end do
    call writemessage(routinename//'_e1c3', 2)
    !
    !-----------------------------------------------------------------------
    !
    !  By following the characteristics of the flow in the 2-direction,
    !  determine values for "vi3" and "b3" ("vsnm1" and "bsnm1") to be used
    !  in evaluating "emf1".
    !
    src = zro
    do k = ks, kep1
      km1 = k - kone
      do i = is, ie
        if (jnac == 0) then
          vsnp1(i, j, k) = vsnp1(i, j, k)*b3(i, j, k)
          bsnp1(i, j, k) = bsnp1(i, j, k)*(vi3(i, j, k) - vg3(k))
        else
          !
          !  Select an effective density and determine the characteristic
          !  velocity for each characteristic in the 2-direction.
          !
          do j = js, jep1
            jm1 = j - jone
            vave = haf*(vi2(i, j, k) + vi2(i, j, km1)) - vg2(j)
            bave(j) = haf*(b2(i, j, k) + b2(i, j, km1))
            absb = abs(bave(j))
            aave = haf*absb*(srd3(i, j, k) + srd3(i, jm1, k)) &
                   /(srd3(i, j, k)*srd3(i, jm1, k))
            sgnp = sign(haf, vave - aave)
            sgnm = sign(haf, vave + aave)
            srdp(j) = (haf + sgnp)*srd3(i, jm1, k) &
                      + (haf - sgnp)*srd3(i, j, k)
            srdm(j) = (haf + sgnm)*srd3(i, jm1, k) &
                      + (haf - sgnm)*srd3(i, j, k)
            srdpi(j) = one/srdp(j)
            srdmi(j) = one/srdm(j)
            vchp(j) = vave - absb*srdpi(j)
            vchm(j) = vave + absb*srdmi(j)
          end do
          !
          !  Interpolate 1-D vectors of "vi3" and "b3" in the 2-direction to
          !  the footpoints of both characteristics.
          !
          do j = jsm2, jep2
            vtmp(j) = vi3(i, j, k) - vg3(k)
            btmp(j) = b3(i, j, k)
            !**            vpch(j) = zro
            !**            vmch(j) = zro
            !**            bpch(j) = zro
            !**            bmch(j) = zro
          end do
          call x2zc1d(vtmp, vchp, vchm, iords3, istps3, k, i, g2b, g2bi, &
                      vpch, vmch)
          call x2zc1d(btmp, vchp, vchm, iordb3, istpb3, k, i, g2b, g2bi, &
                      bpch, bmch)
          !
          !  Evaluate "vsnm1" and "bsnm1" by solving the characteristic
          !  equations.  The source term is non-zero for RTP and ZRP coordinates
          !  since dg32/dx2  /=  0.  Compute the two terms in "emf1".
          !
          q1 = dt*g2bi(i)
          do j = js, jep1
            q2 = sign(one, bave(j))
            if (geometry == 'zrp' .or. geometry == 'rtp' &
                .or. geometry == 'rmp') then
              jm1 = j - jone
              src = q1*dg32ad2(j)*g32ai(j)*bave(j) &
                    *(b3(i, j, k) + b3(i, jm1, k)) &
                    /(srd3(i, j, k)**2 + srd3(i, jm1, k)**2)
            end if
            vsnm1(j) = (vpch(j)*srdp(j) + vmch(j)*srdm(j) &
                        + q2*(bpch(j) - bmch(j))) &
                       /(srdp(j) + srdm(j)) + src
            bsnm1(j) = (bpch(j)*srdpi(j) + bmch(j)*srdmi(j) &
                        + q2*(vpch(j) - vmch(j))) &
                       /(srdpi(j) + srdmi(j))
            vsnp1(i, j, k) = vsnp1(i, j, k)*bsnm1(j)
            bsnp1(i, j, k) = vsnm1(j)*bsnp1(i, j, k)
          end do
        end if
      end do
    end do
    do k = ks, kep1
      do j = js, jep1
        do i = is, ie
          emf1(i, j, k) = vsnp1(i, j, k) - bsnp1(i, j, k)
        end do
      end do
    end do
    call writemessage(routinename//'_e1', 2)
    !
    !-----------------------------------------------------------------------
    !---- 2.  emf2 ---------------------------------------------------------
    !-----------------------------------------------------------------------
    !
    !  By following the characteristics of the flow in the 1-direction,
    !  determine values for "vi3" and "b3" ("vsnp1" and "bsnp1") to be used
    !  in evaluating "emf2".
    !
    src = zro
    do k = ks, kep1
      km1 = k - kone
      do j = js, je
        if (inac == 0) then
          vsnp1(i, j, k) = vi3(i, j, k) - vg3(k)
          bsnp1(i, j, k) = b3(i, j, k)
        else
          !
          !  Select an effective density and determine the characteristic
          !  velocity for each characteristic in the 1-direction.
          !
          do i = is, iep1
            im1 = i - ione
            vave = haf*(vi1(i, j, k) + vi1(i, j, km1)) - vg1(i)
            bave(i) = haf*(b1(i, j, k) + b1(i, j, km1))
            absb = abs(bave(i))
            aave = haf*absb*(srd3(i, j, k) + srd3(im1, j, k)) &
                   /(srd3(i, j, k)*srd3(im1, j, k))
            sgnp = sign(haf, vave - aave)
            sgnm = sign(haf, vave + aave)
            srdp(i) = (haf + sgnp)*srd3(im1, j, k) &
                      + (haf - sgnp)*srd3(i, j, k)
            srdm(i) = (haf + sgnm)*srd3(im1, j, k) &
                      + (haf - sgnm)*srd3(i, j, k)
            srdpi(i) = one/srdp(i)
            srdmi(i) = one/srdm(i)
            vchp(i) = vave - absb*srdpi(i)
            vchm(i) = vave + absb*srdmi(i)
          end do
          !
          !  Interpolate 1-D vectors of "vi3" and "b3" in the 1-direction to
          !  the footpoints of both characteristics.
          !
          do i = ism2, iep2
            vtmp(i) = vi3(i, j, k) - vg3(k)
            btmp(i) = b3(i, j, k)
            !**            vpch(i) = zro
            !**            vmch(i) = zro
            !**            bpch(i) = zro
            !**            bmch(i) = zro
          end do
          call x1zc1d(vtmp, vchp, vchm, iords3, istps3, j, k, vpch, vmch)
          call x1zc1d(btmp, vchp, vchm, iordb3, istpb3, j, k, bpch, bmch)
          !
          !  Evaluate "vsnp1" and "bsnp1" by solving the characteristic
          !  equations.  The source term is non-zero for RTP coordinates since
          !  dg31/dx1 = 1.0.
          !
          do i = is, iep1
            q2 = sign(one, bave(i))
            if (geometry == 'rtp' .or. geometry == 'rmp') then
              im1 = i - ione
              src = dt*dg31ad1(i)*g31ai(i)*bave(i) &
                    *(b3(i, j, k) + b3(im1, j, k)) &
                    /(srd3(i, j, k)**2 + srd3(im1, j, k)**2)
            end if
            vsnp1(i, j, k) = (vpch(i)*srdp(i) + vmch(i)*srdm(i) &
                              + q2*(bpch(i) - bmch(i))) &
                             /(srdp(i) + srdm(i)) + src
            bsnp1(i, j, k) = (bpch(i)*srdpi(i) + bmch(i)*srdmi(i) &
                              + q2*(vpch(i) - vmch(i))) &
                             /(srdpi(i) + srdmi(i))
          end do
        end if
      end do
    end do
    call writemessage(routinename//'_e2c1', 2)
    !
    !-----------------------------------------------------------------------
    !
    !  By following the characteristics of the flow in the 3-direction,
    !  determine values for "vi1" and "b1" ("vsnm1" and "bsnm1") to be used
    !  in evaluating "emf2".
    !
    do j = js, je
      do i = is, iep1
        im1 = i - ione
        if (knac == 0) then
          vsnp1(i, j, k) = vsnp1(i, j, k)*b1(i, j, k)
          bsnp1(i, j, k) = bsnp1(i, j, k)*(vi1(i, j, k) - vg1(i))
        else
          !
          !  Select an effective density and determine the characteristic
          !  velocity for each characteristic in the 3-direction.
          !
          do k = ks, kep1
            km1 = k - kone
            vave = haf*(vi3(i, j, k) + vi3(im1, j, k)) - vg3(k)
            bave(k) = haf*(b3(i, j, k) + b3(im1, j, k))
            absb = abs(bave(k))
            aave = haf*absb*(srd1(i, j, k) + srd1(i, j, km1)) &
                   /(srd1(i, j, k)*srd1(i, j, km1))
            sgnp = sign(haf, vave - aave)
            sgnm = sign(haf, vave + aave)
            srdp(k) = (haf + sgnp)*srd1(i, j, km1) &
                      + (haf - sgnp)*srd1(i, j, k)
            srdm(k) = (haf + sgnm)*srd1(i, j, km1) &
                      + (haf - sgnm)*srd1(i, j, k)
            srdpi(k) = one/srdp(k)
            srdmi(k) = one/srdm(k)
            vchp(k) = vave - absb*srdpi(k)
            vchm(k) = vave + absb*srdmi(k)
          end do
          !
          !  Interpolate 1-D vectors of "vi1" and "b1" in the 3-direction to
          !  the footpoints of both characteristics.
          !
          do k = ksm2, kep2
            vtmp(k) = vi1(i, j, k) - vg1(i)
            btmp(k) = b1(i, j, k)
            !**            vpch(k) = zro
            !**            vmch(k) = zro
            !**            bpch(k) = zro
            !**            bmch(k) = zro
          end do
          call x3zc1d(vtmp, vchp, vchm, iords1, istps1, i, j, g31a, g31ai, &
                      g32b, g32bi, vpch, vmch)
          call x3zc1d(btmp, vchp, vchm, iordb1, istpb1, i, j, g31a, g31ai, &
                      g32b, g32bi, bpch, bmch)
          !
          !  Evaluate "vsnm1" and "bsnm1" by solving the characteristic
          !  equations.  There is no source term since the metric factor "g1" has
          !  no explicit dependence on x3.  Compute the two terms in "emf2".
          !
          do k = ks, kep1
            q2 = sign(one, bave(k))
            vsnm1(k) = (vpch(k)*srdp(k) + vmch(k)*srdm(k) &
                        + q2*(bpch(k) - bmch(k))) &
                       /(srdp(k) + srdm(k))
            bsnm1(k) = (bpch(k)*srdpi(k) + bmch(k)*srdmi(k) &
                        + q2*(vpch(k) - vmch(k))) &
                       /(srdpi(k) + srdmi(k))
            vsnp1(i, j, k) = vsnp1(i, j, k)*bsnm1(k)
            bsnp1(i, j, k) = vsnm1(k)*bsnp1(i, j, k)
          end do
        end if
      end do
    end do
    do k = ks, kep1
      do j = js, je
        do i = is, iep1
          emf2(i, j, k) = vsnp1(i, j, k) - bsnp1(i, j, k)
        end do
      end do
    end do
    call writemessage(routinename//'_e2', 2)
    !
    !-----------------------------------------------------------------------
    !---- 3.  emf3 ---------------------------------------------------------
    !-----------------------------------------------------------------------
    !
    !  By following the characteristics of the flow in the 2-direction,
    !  determine values for "vi1" and "b1" ("vsnp1" and "bsnp1") to be used
    !  in evaluating "emf3".
    !
    do k = ks, ke
      do i = is, iep1
        im1 = i - ione
        if (jnac == 0) then
          vsnp1(i, j, k) = vi1(i, j, k) - vg1(i)
          bsnp1(i, j, k) = b1(i, j, k)
        else
          !
          !  Select an effective density and determine the characteristic
          !  velocity for each characteristic in the 2-direction.
          !
          do j = js, jep1
            jm1 = j - jone
            vave = haf*(vi2(i, j, k) + vi2(im1, j, k)) - vg2(j)
            bave(j) = haf*(b2(i, j, k) + b2(im1, j, k))
            absb = abs(bave(j))
            aave = haf*absb*(srd1(i, j, k) + srd1(i, jm1, k)) &
                   /(srd1(i, j, k)*srd1(i, jm1, k))
            sgnp = sign(haf, vave - aave)
            sgnm = sign(haf, vave + aave)
            srdp(j) = (haf + sgnp)*srd1(i, jm1, k) &
                      + (haf - sgnp)*srd1(i, j, k)
            srdm(j) = (haf + sgnm)*srd1(i, jm1, k) &
                      + (haf - sgnm)*srd1(i, j, k)
            srdpi(j) = one/srdp(j)
            srdmi(j) = one/srdm(j)
            vchp(j) = vave - absb*srdpi(j)
            vchm(j) = vave + absb*srdmi(j)
          end do
          !
          !  Interpolate 1-D vectors of "vi1" and "b1" in the 2-direction to
          !  the footpoints of both characteristics.
          !
          do j = jsm2, jep2
            vtmp(j) = vi1(i, j, k) - vg1(i)
            btmp(j) = b1(i, j, k)
            !**            vpch(j) = zro
            !**            vmch(j) = zro
            !**            bpch(j) = zro
            !**            bmch(j) = zro
          end do
          call x2zc1d(vtmp, vchp, vchm, iords1, istps1, k, i, g2a, g2ai, &
                      vpch, vmch)
          call x2zc1d(btmp, vchp, vchm, iordb1, istpb1, k, i, g2a, g2ai, &
                      bpch, bmch)
          !
          !  Evaluate "vsnp1" and "bsnp1" by solving the characteristic
          !  equations.  There is no source term since the metric factor "g1" has
          !  no explicit dependence on x2.
          !
          do j = js, jep1
            q2 = sign(one, bave(j))
            vsnp1(i, j, k) = (vpch(j)*srdp(j) + vmch(j)*srdm(j) &
                              + q2*(bpch(j) - bmch(j))) &
                             /(srdp(j) + srdm(j))
            bsnp1(i, j, k) = (bpch(j)*srdpi(j) + bmch(j)*srdmi(j) &
                              + q2*(vpch(j) - vmch(j))) &
                             /(srdpi(j) + srdmi(j))
          end do
        end if
      end do
    end do
    call writemessage(routinename//'_e3c2', 2)
    !
    !-----------------------------------------------------------------------
    !
    !  By following the characteristics of the flow in the 1-direction,
    !  determine values for "vi2" and "b2" ("vsnm1" and "bsnm1") to be used
    !  in evaluating "emf3".
    !
    src = zro
    do k = ks, ke
      do j = js, jep1
        jm1 = j - jone
        if (inac == 0) then
          vsnp1(i, j, k) = vsnp1(i, j, k)*b2(i, j, k)
          bsnp1(i, j, k) = bsnp1(i, j, k)*(vi2(i, j, k) - vg2(j))
        else
          !
          !  Select an effective density and determine the characteristic
          !  velocity for each characteristic in the 1-direction.
          !
          do i = is, iep1
            im1 = i - ione
            vave = haf*(vi1(i, j, k) + vi1(i, jm1, k)) - vg1(i)
            bave(i) = haf*(b1(i, j, k) + b1(i, jm1, k))
            absb = abs(bave(i))
            aave = haf*absb*(srd2(i, j, k) + srd2(im1, j, k)) &
                   /(srd2(i, j, k)*srd2(im1, j, k))
            sgnp = sign(haf, vave - aave)
            sgnm = sign(haf, vave + aave)
            srdp(i) = (haf + sgnp)*srd2(im1, j, k) &
                      + (haf - sgnp)*srd2(i, j, k)
            srdm(i) = (haf + sgnm)*srd2(im1, j, k) &
                      + (haf - sgnm)*srd2(i, j, k)
            srdpi(i) = one/srdp(i)
            srdmi(i) = one/srdm(i)
            vchp(i) = vave - absb*srdpi(i)
            vchm(i) = vave + absb*srdmi(i)
          end do
          !
          !  Interpolate 1-D vectors of "vi2" and "b2" in the 1-direction to
          !  the footpoints of both characteristics.
          !
          do i = ism2, iep2
            vtmp(i) = vi2(i, j, k) - vg2(j)
            btmp(i) = b2(i, j, k)
            !**            vpch(i) = zro
            !**            vmch(i) = zro
            !**            bpch(i) = zro
            !**            bmch(i) = zro
          end do
          call x1zc1d(vtmp, vchp, vchm, iords2, istps2, j, k, vpch, vmch)
          call x1zc1d(btmp, vchp, vchm, iordb2, istpb2, j, k, bpch, bmch)
          !
          !  Evaluate "vsnm1" and "bsnm1" by solving the characteristic
          !  equations.  The source term is non-zero for RTP coordinates since
          !  dg2/dx1 = 1.0.  Compute the two terms in "emf3".
          !
          do i = is, iep1
            q2 = sign(one, bave(i))
            if (geometry == 'rtp' .or. geometry == 'rmp') then
              im1 = i - ione
              src = dt*dg2ad1(i)*g2ai(i)*bave(i) &
                    *(b2(i, j, k) + b2(im1, j, k)) &
                    /(srd2(i, j, k)**2 + srd2(im1, j, k)**2)
            end if
            vsnm1(i) = (vpch(i)*srdp(i) + vmch(i)*srdm(i) &
                        + q2*(bpch(i) - bmch(i))) &
                       /(srdp(i) + srdm(i)) + src
            bsnm1(i) = (bpch(i)*srdpi(i) + bmch(i)*srdmi(i) &
                        + q2*(vpch(i) - vmch(i))) &
                       /(srdpi(i) + srdmi(i))
            vsnp1(i, j, k) = vsnp1(i, j, k)*bsnm1(i)
            bsnp1(i, j, k) = vsnm1(i)*bsnp1(i, j, k)
          end do
        end if
      end do
    end do
    do k = ks, ke
      do j = js, jep1
        do i = is, iep1
          emf3(i, j, k) = vsnp1(i, j, k) - bsnp1(i, j, k)
        end do
      end do
    end do
    call writemessage(routinename//'_e3', 2)
    !
    call release_memory(bsnp1); call release_memory(vsnp1)
  END SUBROUTINE mocemfs
  !
  !=======================================================================
  !
  SUBROUTINE hsmoc(emf1, emf2, emf3, srd1, srd2, srd3, vi1, vi2, vi3)
    USE problem_variables, only: bc_sign
    !
    !    dac:zeus3d.mocemfs <-------------------------- MoC estimate of emfs
    !                                                          october, 1992
    !
    !    written by: David Clarke
    !    modified 1: Byung-Il Jun, July 1994
    !                implemented John Hawley and Jim Stone's scheme to
    !                fix pt. explosion of magnetic field in passive field.
    !                Basically, this scheme mixes emfs computed with simple
    !                upwinding (Evans and Hawley) and MoC.
    !                The upwinded values are used to compute the wave
    !                speeds for the characteristic cones for the MoC part.
    !    modified 2: Robert Fiedler, February 1995
    !                upgraded to ZEUS-3D version 3.4 -- improved cache
    !                utilization and added parallelization directives for
    !                SGI multiprocessors.
    !
    !
    !  PURPOSE:  Uses the Method of Characteristics (MoC, invented by Jim
    !  Stone, John Hawley, Chuck Evans, and Michael Norman; see Stone and
    !  Norman, ApJS, v80, p791) to evaluate the velocity and magnetic field
    !  needed to estimate emfs that are properly upwinded in the character-
    !  istic velocities for the set of equations describing transverse
    !  Alfven waves.  This is *not* the full MHD characteristic problem, but
    !  a subset which has been found (reference above) to yield good results
    !  for the propagation of Alfven waves.  This routine differs from the
    !  previous routines MOC1, MOC2, and MOC3 in version 3.1 in that the
    !  Lorentz forces are computed *before* the emfs are estimated.  Thus,
    !  the emfs now use the velocities which have been updated with all the
    !  source terms, including the transverse Lorenz forces.
    !
    !  The characteristic equations governing the propagation of Alfven
    !  waves in the 1-direction are (see ZEUS3D notes "Method of Character-
    !  istics"):
    !
    !  "plus" characteristic (C+):
    !
    !    ( db1/dt + (v2 - a2) * db1/dx2 + (v3 - a3) * db1/dx3 ) / sqrt(d)
    !  + ( dv1/dt + (v2 - a2) * dv1/dx2 + (v3 - a3) * dv1/dx3 )  =  S    (1)
    !
    !  "minus" characteristic (C-):
    !
    !    ( db1/dt + (v2 + a2) * db1/dx2 + (v3 + a3) * db1/dx3 ) / sqrt(d)
    !  - ( dv1/dt + (v2 + a2) * dv1/dx2 + (v3 + a3) * dv1/dx3 )  = -S    (2)
    !
    !  where   a2 = b2/sqrt(d) is the Alfven velocity in the 2-direction
    !          a3 = b3/sqrt(d) is the Alfven velocity in the 3-direction
    !          g1, g2, g3 are the metric factors
    !          S = b1 * ( b2/g2 * dg1/dx2 + b3/g3 * dg1/dx3 )
    !
    !  Equations (1) and (2) can be written in Lagrangian form:
    !
    !      1    D+/-           D+/-
    !   ------- ----(b1)  +/-  ----(v1)  =  +/- S                        (3)
    !   sqrt(d)  Dt             Dt
    !
    !  where the Lagrangian derivatives are given by
    !
    !  D+/-     d                   d                   d
    !  ----  =  --  +  (v2 -/+ a2) ---  +  (v3 -/+ a3) ---               (4)
    !   Dt      dt                 dx2                 dx3
    !
    !  Differencing equations (3) [e.g. D+(b1) = b* - b+; D-(b1) = b* - b-],
    !  and then solving for the advanced time values of b* and v*, one gets:
    !                             _                                _
    !           sqrt (d+ * d-)   |     b+          b-               |
    !  b* =  ------------------- |  -------- + --------- + v+ - v-  |    (5)
    !        sqrt(d+) + sqrt(d-) |_ sqrt(d+)    sqrt(d-)           _|
    !
    !                 1
    !  v* =  ------------------- [ v+*sqrt(d+) + v-*sqrt(d-) + b+ - b- ]
    !        sqrt(d+) + sqrt(d-)                                         (6)
    !
    !     + S Dt
    !
    !  where b+(-), and v+(-) are the upwinded values of the magnetic field
    !  and velocity interpolated to the time-averaged bases of C+ (C-), and
    !  d+(-) are estimates of the density along each characteristic path
    !  during the time interval Dt.
    !
    !  Equations (1) and (2) would suggest that when estimating "emf2" for
    !  example, that the interpolated values for "v1" and "b1" be upwinded
    !  in both the 2- and 3- components of the characteristic velocity.  It
    !  turns out that this is impractical numerically, and so only the
    !  "partial" characteristics are tracked.  While estimating "emf2", "v1"
    !  and "b1" are upwinded only in the 3-component of the characteristic
    !  velocities.  Conversely, while estimating "emf3", "v1" and "b1" are
    !  upwinded only in the 2-component of the characteristic velocities.
    !  Since updating "b1" requires both "emf2" and "emf3", the evolution of
    !  "b1" will ultimately depend upon the full characteristics.  This
    !  amounts to nothing more than directionally splitting the full MoC
    !  algorithm.  The effects of such a directionally split implementation
    !  are not fully known.  What is known is:
    !
    !  1) A non-directionally split implementation of the MoC algorithm is
    !     not possible without either relocating the emfs to the zone
    !     corners or the magnetic field components to the zone centres.
    !     The former has been tried (change deck mocemf) and was found to
    !     generate intolerable diffusion of magnetic field.  In addition,
    !     the algorithm is not unconditionally stable.  The latter has not
    !     been tried, but is dismissed on the grounds that div(B) will be
    !     determined by truncation errors rather than machine round-off
    !     errors.
    !
    !  2) A directionally split algorithm that is not also operator split
    !     (ie, performing the Lorentz updates of the velocities separately
    !     from the MoC estimation of the emfs) does not allow stable Alfven
    !     wave propagation in 2-D.  Operator splitting the MoC algorithm so
    !     that the transverse Lorentz forces are computed *before* the
    !     magnetic field update does allow Alfven waves to propagate stably
    !     in multi-dimensions but appears to introduce more diffusion for
    !     sub-Alfvenic flow.  On the other hand, super-Alfvenic flow does
    !     *not* appear to be more diffusive in the operator split scheme.
    !
    !  INPUT VARIABLES: srd1, srd2, srd3
    !
    !  OUTPUT VARIABLES:
    !    emf1      emf along 1-edge computed using MoC estimates of v2, b2,
    !              v3, and b3.
    !    emf2      emf along 2-edge computed using MoC estimates of v3, b3,
    !              v1, and b1.
    !    emf3      emf along 3-edge computed using MoC estimates of v1, b1,
    !              v2, and b2.
    !
    !  LOCAL VARIABLES:
    !
    !    1-D variables
    !    bave      spatially averaged magnetic field at edge
    !    srdp      sqrt(density) along the plus  characteristic (C+)
    !    srdm      sqrt(density) along the minus characteristic (C-)
    !    vchp      characteristic velocity along C+ (v - va)
    !    vchm      characteristic velocity along C- (v + va)
    !    vpch      velocity interpolated to the time-centred footpoint of C+
    !    vmch      velocity interpolated to the time-centred footpoint of C-
    !    bpch      B-field  interpolated to the time-centred footpoint of C+
    !    bmch      B-field  interpolated to the time-centred footpoint of C-
    !    vsnm1     MoC estimate of v[n-1] used to evaluate emf[n], n=1,2,3
    !    bsnm1     MoC estimate of b[n-1] used to evaluate emf[n], n=1,2,3
    !
    !    2-D variables
    !    v3intj   upwinded v3 in 2-direction
    !    b3intj   upwinded b3 in 2-direction
    !    etc..
    !
    !    3-D variables
    !    srd[n]    sqrt of spatially averaged density at [n]-face n=1,2,3
    !    vsnp1     MoC estimate of v[n+1] used to evaluate emf[n], n=1,2,3
    !    bsnp1     MoC estimate of b[n+1] used to evaluate emf[n], n=1,2,3
    !    vsnp1     is reused as the vsnp1*bsnm1 term in emf[n]
    !    bsnp1     is reused as the vsnm1*bsnp1 term in emf[n]
    !
    !-----------------------------------------------------------------------
    !
    REALNUM, DIMENSION(:, :, :), INTENT(OUT) :: emf1, emf2, emf3
    REALNUM, DIMENSION(:, :, :), INTENT(IN) :: srd1, srd2, srd3
    REALNUM, DIMENSION(:, :, :), INTENT(IN)  :: vi1, vi2, vi3
    !
    CHARACTER(LEN=*), PARAMETER :: routinename = 'z_hsmoc'
    !CHARACTER(LEN=messagelinelength) message
    !
    INTEGER i, j, k, im1, jm1, km1
    REALNUM absb, sgnp, sgnm, q1, q2, src
    !
    REALNUM, DIMENSION(ijknsa:ijknea) :: &
      bave, srdp, srdm, srdpi, srdmi, vchp, vchm, vtmp, btmp, vpch, &
      vmch, bpch, bmch, vsnm1, bsnm1, vave, aave, vfl, vt, bt, vint, bint

    ! REALNUM, DIMENSION(jsa:jea,ksa:kea) :: v3intj, b3intj, v2intk, b2intk
    REALNUM, DIMENSION(ksa:kea, isa:iea) :: v1intk, b1intk, v3inti, b3inti
    REALNUM, DIMENSION(isa:iea, jsa:jea) :: v2inti, b2inti, v1intj, b1intj

    REALNUM, POINTER, SAVE :: vsnp1(:, :, :) => null(), bsnp1(:, :, :) => null()
    call get_memory(vsnp1); call get_memory(bsnp1)
    ! RFK: Fiedler had equivalenced vsnp1 to emf3, this reduces the
    ! number of worker arrays needed here by one.

    ! Observe that some of the emf arrays are used here as scratch space
    ! during the computation: they are not always used to mean the emf
    ! until the end.

    call writemessage(routinename//'_begin', 2)
    !
    !-----------------------------------------------------------------------
    !---- 1.  emf1 ---------------------------------------------------------
    !-----------------------------------------------------------------------
    !
    !    BEGINNING OF I LOOPS
    !
    !
    !  By following the characteristics of the flow in the 3-direction,
    !  determine values for "v2" and "b2" ("vsnp1" and "bsnp1") to be used
    !  in evaluating "emf1".
    !
    !     Compute upwinded b3 and v3 in the 2-direction and use them to
    !     compute the wave speeds for the characteristic cones for the MoC.
    !
    call writemessage(routinename//'_a', 2)
    do k = ks, kep1
      km1 = k - kone
      do i = is, ie
        do j = jsm2, jep2
          vfl(j) = haf*(vi2(i, j, k) + vi2(i, j, km1)) - vg2(j)
          vt(j) = vi3(i, j, k) - vg3(k)
          bt(j) = b3(i, j, k)
          !**          bint  (j) = zro
          !**          vint  (j) = zro
        end do
        !
        !write(message,'(2i)') k, j
        !call writemessage(routinename//'_b',message,4)
        call x2int1d(bt, vfl, iordb3, istpb3, k, i, g2b, g2bi, bint)
        !call writemessage(routinename//'_c',message,4)
        call x2int1d(vt, vfl, iords3, istps3, k, i, g2b, g2bi, vint)
        !call writemessage(routinename//'_d',message,4)
        !
        do j = js, jep1
          emf1(i, j, k) = vint(j)
          emf2(i, j, k) = bint(j) ! emf2 is used here as scratch space
        end do
      end do
    end do
    !
    if (knac == 0) then
      k = ks
      do j = js, jep1
        do i = is, ie
          vsnp1(i, j, k) = (vi2(i, j, k) - vg2(j))*emf2(i, j, k)
          bsnp1(i, j, k) = b2(i, j, k)*emf1(i, j, k)
        end do
      end do
    else
      !
      !
      !  Select an effective density and determine the characteristic
      !  velocity using upwinded values for each characteristic in the
      !  3-direction.
      !
      do j = js, jep1
        jm1 = j - jone
        do i = is, ie
          do k = ks, kep1
            km1 = k - kone
            vave(k) = emf1(i, j, k)
            bave(k) = emf2(i, j, k)
            !          vave (k) = haf * ( vi3(i,j,k) + vi3(i,jm1,k) ) - vg3(k)
            !          bave (k) = haf * ( b3(i,j,k) + b3(i,jm1,k) )
            absb = abs(bave(k))
            aave(k) = haf*absb*(srd2(i, j, k) + srd2(i, j, km1)) &
                      /(srd2(i, j, k)*srd2(i, j, km1))
            sgnp = sign(haf, vave(k) - aave(k))
            sgnm = sign(haf, vave(k) + aave(k))
            srdp(k) = (haf + sgnp)*srd2(i, j, km1) &
                      + (haf - sgnp)*srd2(i, j, k)
            srdm(k) = (haf + sgnm)*srd2(i, j, km1) &
                      + (haf - sgnm)*srd2(i, j, k)
            srdpi(k) = one/srdp(k)
            srdmi(k) = one/srdm(k)
            vchp(k) = vave(k) - absb*srdpi(k)
            vchm(k) = vave(k) + absb*srdmi(k)
          end do
          !
          !  Interpolate 1-D vectors of "vi2" and "b2" in the 3-direction to
          !  the footpoints of both characteristics.
          !
          do k = ksm2, kep2
            vtmp(k) = vi2(i, j, k) - vg2(j)
            btmp(k) = b2(i, j, k)
            !**            vpch(k) = zro
            !**            vmch(k) = zro
            !**            bpch(k) = zro
            !**            bmch(k) = zro
          end do
          call x3zc1d(vtmp, vchp, vchm, iords2, istps2, i, j, g31b, g31bi, &
                      g32a, g32ai, vpch, vmch)
          call x3zc1d(btmp, vchp, vchm, iordb2, istpb2, i, j, g31b, g31bi, &
                      g32a, g32ai, bpch, bmch)
          !
          !
          !  Evaluate "vsnp1" and "bsnp1" by solving the characteristic
          !  equations.  There is no source term since the metric factor "g2" has
          !  no explicit dependence on x3.
          !
          do k = ks, kep1
            q2 = sign(one, bave(k))
            vsnp1(i, j, k) = (vpch(k)*srdp(k) + vmch(k)*srdm(k) &
                              + q2*(bpch(k) - bmch(k))) &
                             /(srdp(k) + srdm(k))
            bsnp1(i, j, k) = (bpch(k)*srdpi(k) + bmch(k)*srdmi(k) &
                              + q2*(vpch(k) - vmch(k))) &
                             /(srdpi(k) + srdmi(k))
            vsnp1(i, j, k) = vsnp1(i, j, k)*bave(k)
            bsnp1(i, j, k) = bsnp1(i, j, k)*vave(k)
          end do
          !
        end do
        !
      end do
    end if
    call writemessage(routinename//'_e1c3', 2)
    call writedata(vsnp1, routinename//'_e1c3v', 4)
    call writedata(bsnp1, routinename//'_e1c3b', 4)
    !
    !-----------------------------------------------------------------------
    !
    !  By following the characteristics of the flow in the 2-direction,
    !  determine values for "vi3" and "b3" ("vsnm1" and "bsnm1") to be used
    !  in evaluating "emf1".
    !
    src = zro
    !
    !  Compute upwinded b2 and vi2 in the 3-direction and use them to
    !  compute the wave speeds for the chracteristic cones for the MoC
    !
    do j = js, jep1
      jm1 = j - jone
      do i = is, ie
        do k = ksm2, kep2
          vfl(k) = haf*(vi3(i, j, k) + vi3(i, jm1, k)) - vg3(k)
          vt(k) = vi2(i, j, k) - vg2(j)
          bt(k) = b2(i, j, k)
          !**          bint  (k) = zro
          !**          vint  (k) = zro
        end do
        call x3int1d(bt, vfl, iordb2, istpb2, i, j, g31b, g31bi, g32a, &
                     g32ai, bint)
        call x3int1d(vt, vfl, iords2, istps2, i, j, g31b, g31bi, g32a, &
                     g32ai, vint)
        do k = ks, kep1
          emf1(i, j, k) = vint(k)
          emf2(i, j, k) = bint(k)
        end do
      end do
    end do
    !
    if (jnac == 0) then
      j = js
      do k = ks, kep1
        km1 = k - kone
        do i = is, ie
          vsnp1(i, j, k) = haf*(vsnp1(i, j, k) &
                                + b3(i, j, k)*emf1(i, j, k))
          bsnp1(i, j, k) = haf*(bsnp1(i, j, k) &
                                + (vi3(i, j, k) - vg3(k))*emf2(i, j, k))
        end do
      end do
    else
      !  Select an effective density and determine the characteristic
      !  velocity using upwinded values for each characteristic in the
      !  2-direction.
      !
      do k = ks, kep1
        km1 = k - kone
        do i = is, ie
          do j = js, jep1
            jm1 = j - jone
            vave(j) = emf1(i, j, k)
            bave(j) = emf2(i, j, k)
            !   vave (j) = haf * ( vi2(i,j,k) + vi2(i,j,km1) ) - vg2(j)
            !   bave (j) = haf * ( b2(i,j,k) + b2(i,j,km1) )
            absb = abs(bave(j))
            aave(j) = haf*absb*(srd3(i, j, k) + srd3(i, jm1, k)) &
                      /(srd3(i, j, k)*srd3(i, jm1, k))
            sgnp = sign(haf, vave(j) - aave(j))
            sgnm = sign(haf, vave(j) + aave(j))
            srdp(j) = (haf + sgnp)*srd3(i, jm1, k) &
                      + (haf - sgnp)*srd3(i, j, k)
            srdm(j) = (haf + sgnm)*srd3(i, jm1, k) &
                      + (haf - sgnm)*srd3(i, j, k)
            srdpi(j) = one/srdp(j)
            srdmi(j) = one/srdm(j)
            vchp(j) = vave(j) - absb*srdpi(j)
            vchm(j) = vave(j) + absb*srdmi(j)
          end do
          !
          !  Interpolate 1-D vectors of "vi3" and "b3" in the 2-direction to
          !  the footpoints of both characteristics.
          !
          do j = jsm2, jep2
            vtmp(j) = vi3(i, j, k) - vg3(k)
            btmp(j) = b3(i, j, k)
            !**            vpch(j) = zro
            !**            vmch(j) = zro
            !**            bpch(j) = zro
            !**            bmch(j) = zro
          end do
          call x2zc1d(vtmp, vchp, vchm, iords3, istps3, k, i, g2b, g2bi, &
                      vpch, vmch)
          call x2zc1d(btmp, vchp, vchm, iordb3, istpb3, k, i, g2b, g2bi, &
                      bpch, bmch)
          !
          !  Evaluate "vsnm1" and "bsnm1" by solving the characteristic
          !  equations.  The source term is non-zero for RTP and ZRP
          !  coordinates since dg32/dx2  /=  0.
          !  Compute the two terms in "emf1".
          !
          q1 = dt*g2bi(i)
          do j = js, jep1
            q2 = sign(one, bave(j))
            if (geometry == 'zrp' .or. geometry == 'rtp' &
                .or. geometry == 'rmp') then
              jm1 = j - jone
              src = q1*dg32ad2(j)*g32ai(j)*bave(j) &
                    *(b3(i, j, k) + b3(i, jm1, k)) &
                    /(srd3(i, j, k)**2 + srd3(i, jm1, k)**2)
            end if
            vsnm1(j) = (vpch(j)*srdp(j) + vmch(j)*srdm(j) &
                        + q2*(bpch(j) - bmch(j))) &
                       /(srdp(j) + srdm(j)) + src
            bsnm1(j) = (bpch(j)*srdpi(j) + bmch(j)*srdmi(j) &
                        + q2*(vpch(j) - vmch(j))) &
                       /(srdpi(j) + srdmi(j))
            vsnm1(j) = vsnm1(j)*bave(j)
            bsnm1(j) = bsnm1(j)*vave(j)
            !
            vsnp1(i, j, k) = haf*(vsnp1(i, j, k) + bsnm1(j))
            bsnp1(i, j, k) = haf*(vsnm1(j) + bsnp1(i, j, k))
          end do
        end do
      end do
    end if
    call writemessage(routinename//'_e1c2', 2)
    call writedata(vsnp1, routinename//'_e1c2vA', 4)
    call writedata(bsnp1, routinename//'_e1c2bA', 4)

    if (l_lastnode2) then
      vsnp1(:, jep1, :) = zro
      if (bc_sign == -one) bsnp1(:, jep1, :) = zro
    end if

    do k = ks, kep1
      do j = js, jep1
        do i = is, ie
          emf1(i, j, k) = vsnp1(i, j, k) - bsnp1(i, j, k)
        end do
      end do
    end do
    !
    !-----------------------------------------------------------------------
    !---- 2.  emf2 ---------------------------------------------------------
    !-----------------------------------------------------------------------
    !
    !     BEGINNING OF FIRST J LOOP
    !
    do j = js, je
      !
      !  By following the characteristics of the flow in the 1-direction,
      !  determine values for "vi3" and "b3" ("vsnp1" and "bsnp1") to be used
      !  in evaluating "emf2".
      !
      src = zro
      !
      !  Compute upwinded b1 and vi1 in the 3-direction and use them to
      !  compute the wave speeds for the chracteristic cones for the MoC.
      !
      do i = is, iep1
        im1 = i - ione
        do k = ksm2, kep2
          vfl(k) = haf*(vi3(i, j, k) + vi3(im1, j, k)) - vg3(k)
          vt(k) = vi1(i, j, k) - vg1(i)
          bt(k) = b1(i, j, k)
          !**          bint  (k) = zro
          !**          vint  (k) = zro
        end do
        !
        ! RFK: bad centering of metric factors
        !      call x3int1d(bt, vfl, iordb1, istpb1, i, j
        !    &                  , g31b, g31bi, g32a, g32ai, bint )
        !      call x3int1d(vt, vfl, iordb1, istpb1, i, j
        !    &                  , g31b, g31bi, g32a, g32ai, vint )
        !
        call x3int1d(bt, vfl, iordb1, istpb1, i, j, g31a, g31ai, g32b, &
                     g32bi, bint)
        call x3int1d(vt, vfl, iordb1, istpb1, i, j, g31a, g31ai, g32b, &
                     g32bi, vint)
        do k = ks, kep1
          v1intk(k, i) = vint(k)
          b1intk(k, i) = bint(k)
        end do
      end do
      !
      if (inac == 0) then
        i = is
        do k = ks, kep1
          km1 = k - kone
          vsnp1(i, j, k) = (vi3(i, j, k) - vg3(k))*b1intk(k, i)
          bsnp1(i, j, k) = b3(i, j, k)*v1intk(k, i)
        end do
      else
        !
        !  Select an effective density and determine the characteristic
        !  velocity using upwinded values for each characteristic in the
        !  1-direction.
        !
        do k = ks, kep1
          km1 = k - kone
          do i = is, iep1
            im1 = i - ione
            vave(i) = v1intk(k, i)
            bave(i) = b1intk(k, i)
            !          vave (i) = haf * ( vi1(i,j,k) + vi1(i,j,km1) ) - vg1(i)
            !          bave (i) = haf * ( b1(i,j,k) + b1(i,j,km1) )
            absb = abs(bave(i))
            aave(i) = haf*absb*(srd3(i, j, k) + srd3(im1, j, k)) &
                      /(srd3(i, j, k)*srd3(im1, j, k))
            sgnp = sign(haf, vave(i) - aave(i))
            sgnm = sign(haf, vave(i) + aave(i))
            srdp(i) = (haf + sgnp)*srd3(im1, j, k) &
                      + (haf - sgnp)*srd3(i, j, k)
            srdm(i) = (haf + sgnm)*srd3(im1, j, k) &
                      + (haf - sgnm)*srd3(i, j, k)
            srdpi(i) = one/srdp(i)
            srdmi(i) = one/srdm(i)
            vchp(i) = vave(i) - absb*srdpi(i)
            vchm(i) = vave(i) + absb*srdmi(i)
          end do
          !
          !  Interpolate 1-D vectors of "vi3" and "b3" in the 1-direction to
          !  the footpoints of both characteristics.
          !
          do i = ism2, iep2
            vtmp(i) = vi3(i, j, k) - vg3(k)
            btmp(i) = b3(i, j, k)
            !**            vpch(i) = zro
            !**            vmch(i) = zro
            !**            bpch(i) = zro
            !**            bmch(i) = zro
          end do
          call x1zc1d(vtmp, vchp, vchm, iords3, istps3, j, k, vpch, vmch)
          call x1zc1d(btmp, vchp, vchm, iordb3, istpb3, j, k, bpch, bmch)
          !
          !  Evaluate "vsnp1" and "bsnp1" by solving the characteristic
          !  equations.  The source term is non-zero for RTP coordinates since
          !  dg31/dx1 = 1.0.
          !
          do i = is, iep1
            q2 = sign(one, bave(i))
            if (geometry == 'rtp' .or. geometry == 'rmp') then
              im1 = i - ione
              src = dt*dg31ad1(i)*g31ai(i)*bave(i) &
                    *(b3(i, j, k) + b3(im1, j, k)) &
                    /(srd3(i, j, k)**2 + srd3(im1, j, k)**2)
            end if
            vsnp1(i, j, k) = (vpch(i)*srdp(i) + vmch(i)*srdm(i) &
                              + q2*(bpch(i) - bmch(i))) &
                             /(srdp(i) + srdm(i)) + src
            bsnp1(i, j, k) = (bpch(i)*srdpi(i) + bmch(i)*srdmi(i) &
                              + q2*(vpch(i) - vmch(i))) &
                             /(srdpi(i) + srdmi(i))
            vsnp1(i, j, k) = vsnp1(i, j, k)*bave(i)
            bsnp1(i, j, k) = bsnp1(i, j, k)*vave(i)
          end do
          !
        end do
      end if
      !
      !    END OF FIRST J LOOP
      !
    end do
    call writemessage(routinename//'_e2c1', 2)
    call writedata(vsnp1, routinename//'_e2c1v', 4)
    call writedata(bsnp1, routinename//'_e2c1b', 4)
    !
    !-----------------------------------------------------------------------
    !
    !
    !   BEGINNING OF SECOND J LOOP
    !
    !
    do j = js, je
      !
      !  By following the characteristics of the flow in the 3-direction,
      !  determine values for "vi1" and "b1" ("vsnm1" and "bsnm1") to be used
      !  in evaluating "emf2".
      !
      !   Compute upwinded b3 and vi3 in the 1-direction and use them to
      !   compute the wave speeds for the chracteristic cones for the MoC
      !
      do k = ks, kep1
        km1 = k - kone
        do i = ism2, iep2
          vfl(i) = haf*(vi1(i, j, k) + vi1(i, j, km1)) - vg1(i)
          vt(i) = vi3(i, j, k) - vg3(k)
          bt(i) = b3(i, j, k)
          !**          bint  (i) = zro
          !**          vint  (i) = zro
        end do
        !
        call x1int1d(bt, vfl, iordb1, istpb1, j, k, bint)
        call x1int1d(vt, vfl, iords1, istps1, j, k, vint)
        do i = is, iep1
          v3inti(k, i) = vint(i)
          b3inti(k, i) = bint(i)
        end do
      end do
      !
      if (knac == 0) then
        k = ks
        do i = is, iep1
          im1 = i - ione
          vsnp1(i, j, k) = haf*(vsnp1(i, j, k) &
                                + b1(i, j, k)*v3inti(k, i))
          bsnp1(i, j, k) = haf*(bsnp1(i, j, k) &
                                + (vi1(i, j, k) - vg1(i))*b3inti(k, i))
        end do
      else
        !
        !
        !  Select an effective density and determine the characteristic
        !  velocity using upwinded values for each characteristic in the
        !  3-direction.
        !
        do i = is, iep1
          im1 = i - ione
          do k = ks, kep1
            km1 = k - kone
            vave(k) = v3inti(k, i)
            bave(k) = b3inti(k, i)
            !  vave (k) = haf * ( vi3(i,j,k) + vi3(im1,j,k) ) - vg3(k)
            !  bave (k) = haf * ( b3(i,j,k) + b3(im1,j,k) )
            absb = abs(bave(k))
            aave(k) = haf*absb*(srd1(i, j, k) + srd1(i, j, km1)) &
                      /(srd1(i, j, k)*srd1(i, j, km1))
            sgnp = sign(haf, vave(k) - aave(k))
            sgnm = sign(haf, vave(k) + aave(k))
            srdp(k) = (haf + sgnp)*srd1(i, j, km1) &
                      + (haf - sgnp)*srd1(i, j, k)
            srdm(k) = (haf + sgnm)*srd1(i, j, km1) &
                      + (haf - sgnm)*srd1(i, j, k)
            srdpi(k) = one/srdp(k)
            srdmi(k) = one/srdm(k)
            vchp(k) = vave(k) - absb*srdpi(k)
            vchm(k) = vave(k) + absb*srdmi(k)
          end do
          !
          !  Interpolate 1-D vectors of "vi1" and "b1" in the 3-direction to
          !  the footpoints of both characteristics.
          !
          do k = ksm2, kep2
            vtmp(k) = vi1(i, j, k) - vg1(i)
            btmp(k) = b1(i, j, k)
            !**            vpch(k) = zro
            !**            vmch(k) = zro
            !**            bpch(k) = zro
            !**            bmch(k) = zro
          end do
          call x3zc1d(vtmp, vchp, vchm, iords1, istps1, i, j, g31a, g31ai, &
                      g32b, g32bi, vpch, vmch)
          call x3zc1d(btmp, vchp, vchm, iordb1, istpb1, i, j, g31a, g31ai, &
                      g32b, g32bi, bpch, bmch)
          !
          !  Evaluate "vsnm1" and "bsnm1" by solving the characteristic
          !  equations.  There is no source term since the metric factor
          !  "g1" has
          !  no explicit dependence on x3.  Compute the two terms in "emf2".
          !
          do k = ks, kep1
            q2 = sign(one, bave(k))
            vsnm1(k) = (vpch(k)*srdp(k) + vmch(k)*srdm(k) &
                        + q2*(bpch(k) - bmch(k))) &
                       /(srdp(k) + srdm(k))
            bsnm1(k) = (bpch(k)*srdpi(k) + bmch(k)*srdmi(k) &
                        + q2*(vpch(k) - vmch(k))) &
                       /(srdpi(k) + srdmi(k))
            vsnm1(k) = vsnm1(k)*bave(k)
            bsnm1(k) = bsnm1(k)*vave(k)
            !
            vsnp1(i, j, k) = haf*(vsnp1(i, j, k) + bsnm1(k))
            bsnp1(i, j, k) = haf*(vsnm1(k) + bsnp1(i, j, k))
          end do
        end do
      end if
      !
      !   END OF SECOND J LOOP
      !
    end do
    call writemessage(routinename//'_e2c3', 2)
    call writedata(vsnp1, routinename//'_e2c3vA', 4)
    call writedata(bsnp1, routinename//'_e2c3bA', 4)
    do k = ks, kep1
      do j = js, je
        do i = is, iep1
          emf2(i, j, k) = vsnp1(i, j, k) - bsnp1(i, j, k)
        end do
      end do
    end do
    !
    !-----------------------------------------------------------------------
    !---- 3.  emf3 ---------------------------------------------------------
    !-----------------------------------------------------------------------
    !
    !  BEGINNING OF K LOOP
    !
    do k = ks, ke
      !
      !  By following the characteristics of the flow in the 2-direction,
      !  determine values for "vi1" and "b1" ("vsnp1" and "bsnp1") to be used
      !  in evaluating "emf3".
      !
      !  Compute upwinded b2 and vi2 in the 1-direction and use them to
      !  compute the wave speeds for the chracteristic cones for MoC.
      !
      do j = js, jep1
        jm1 = j - jone
        do i = ism2, iep2
          vfl(i) = haf*(vi1(i, j, k) + vi1(i, jm1, k)) - vg1(i)
          vt(i) = vi2(i, j, k) - vg2(j)
          bt(i) = b2(i, j, k)
          !**          bint  (i) = zro
          !**          vint  (i) = zro
        end do
        call x1int1d(bt, vfl, iordb2, istpb2, j, k, bint)
        call x1int1d(vt, vfl, iords2, istps2, j, k, vint)
        do i = is, iep1
          v2inti(i, j) = vint(i)
          b2inti(i, j) = bint(i)
        end do
      end do
      !
      !
      if (jnac == 0) then
        j = js
        do i = is, iep1
          im1 = i - ione
          vsnp1(i, j, k) = (vi1(i, j, k) - vg1(i))*b2inti(i, j)
          bsnp1(i, j, k) = b1(i, j, k)*v2inti(i, j)
        end do
      else
        !
        !  Select an effective density and determine the characteristic
        !  velocity using upwinded values for each characteristic in the
        !  2-direction.
        !
        do i = is, iep1
          im1 = i - ione
          do j = js, jep1
            jm1 = j - jone
            vave(j) = v2inti(i, j)
            bave(j) = b2inti(i, j)
            !  vave (j) = haf * ( vi2(i,j,k) + vi2(im1,j,k) ) - vg2(j)
            !  bave (j) = haf * ( b2(i,j,k) + b2(im1,j,k) )
            absb = abs(bave(j))
            aave(j) = haf*absb*(srd1(i, j, k) + srd1(i, jm1, k)) &
                      /(srd1(i, j, k)*srd1(i, jm1, k))
            sgnp = sign(haf, vave(j) - aave(j))
            sgnm = sign(haf, vave(j) + aave(j))
            srdp(j) = (haf + sgnp)*srd1(i, jm1, k) &
                      + (haf - sgnp)*srd1(i, j, k)
            srdm(j) = (haf + sgnm)*srd1(i, jm1, k) &
                      + (haf - sgnm)*srd1(i, j, k)
            srdpi(j) = one/srdp(j)
            srdmi(j) = one/srdm(j)
            vchp(j) = vave(j) - absb*srdpi(j)
            vchm(j) = vave(j) + absb*srdmi(j)
          end do
          !
          !  Interpolate 1-D vectors of "vi1" and "b1" in the 2-direction to
          !  the footpoints of both characteristics.
          !
          do j = jsm2, jep2
            vtmp(j) = vi1(i, j, k) - vg1(i)
            btmp(j) = b1(i, j, k)
            !**            vpch(j) = zro
            !**            vmch(j) = zro
            !**            bpch(j) = zro
            !**            bmch(j) = zro
          end do
          call x2zc1d(vtmp, vchp, vchm, iords1, istps1, k, i, g2a, g2ai, &
                      vpch, vmch)
          call x2zc1d(btmp, vchp, vchm, iordb1, istpb1, k, i, g2a, g2ai, &
                      bpch, bmch)
          !
          !  Evaluate "vsnp1" and "bsnp1" by solving the characteristic
          !  equations.  There is no source term since the metric factor
          !  "g1" has no explicit dependence on x2.
          !
          do j = js, jep1
            q2 = sign(one, bave(j))
            vsnp1(i, j, k) = (vpch(j)*srdp(j) + vmch(j)*srdm(j) &
                              + q2*(bpch(j) - bmch(j))) &
                             /(srdp(j) + srdm(j))
            bsnp1(i, j, k) = (bpch(j)*srdpi(j) + bmch(j)*srdmi(j) &
                              + q2*(vpch(j) - vmch(j))) &
                             /(srdpi(j) + srdmi(j))
            vsnp1(i, j, k) = vsnp1(i, j, k)*bave(j)
            bsnp1(i, j, k) = bsnp1(i, j, k)*vave(j)
          end do
          !
        end do
      end if

      !
      !-----------------------------------------------------------------------
      !
      !  By following the characteristics of the flow in the 1-direction,
      !  determine values for "vi2" and "b2" ("vsnm1" and "bsnm1") to be used
      !  in evaluating "emf3".
      !
      src = zro
      !
      !  Compute upwinded b1 and vi1 in the 2-direction and use them to
      !  compute the wave speeds for the chracteristic cones for Moc
      !
      do i = is, iep1
        im1 = i - ione
        do j = jsm2, jep2
          vfl(j) = haf*(vi2(i, j, k) + vi2(im1, j, k)) - vg2(j)
          vt(j) = vi1(i, j, k) - vg1(i)
          bt(j) = b1(i, j, k)
          !**          bint  (j) = zro
          !**          vint  (j) = zro
        end do
        !
        ! RFK: wrong centering of metric factors
        !      call x2int1d(bt, vfl, iordb1, istpb1, k, i
        !    &                  , g2b, g2bi, bint )
        !      call x2int1d(vt, vfl, iords1, istps1, k, i
        !    &                  , g2b, g2bi, vint )
        !
        call x2int1d(bt, vfl, iordb1, istpb1, k, i, g2a, g2ai, bint)
        call x2int1d(vt, vfl, iords1, istps1, k, i, g2a, g2ai, vint)
        do j = js, jep1
          b1intj(i, j) = bint(j)
          v1intj(i, j) = vint(j)
        end do
      end do
      !
      if (inac == 0) then
        i = is
        do j = js, jep1
          jm1 = j - jone
          vsnp1(i, j, k) = haf*(vsnp1(i, j, k) &
                                + b2(i, j, k)*v1intj(i, j))
          bsnp1(i, j, k) = haf*(bsnp1(i, j, k) &
                                + (vi2(i, j, k) - vg2(j))*b1intj(i, j))
        end do
      else
        !
        !  Select an effective density and determine the characteristic
        !  velocity using upwinded values for each characteristic in the
        !  1-direction.
        !
        do j = js, jep1
          jm1 = j - jone
          do i = is, iep1
            im1 = i - ione
            vave(i) = v1intj(i, j)
            bave(i) = b1intj(i, j)
            !  vave (i) = haf * ( vi1(i,j,k) + vi1(i,jm1,k) ) - vg1(i)
            !  bave (i) = haf * ( b1(i,j,k) + b1(i,jm1,k) )
            absb = abs(bave(i))
            aave(i) = haf*absb*(srd2(i, j, k) + srd2(im1, j, k)) &
                      /(srd2(i, j, k)*srd2(im1, j, k))
            sgnp = sign(haf, vave(i) - aave(i))
            sgnm = sign(haf, vave(i) + aave(i))
            srdp(i) = (haf + sgnp)*srd2(im1, j, k) &
                      + (haf - sgnp)*srd2(i, j, k)
            srdm(i) = (haf + sgnm)*srd2(im1, j, k) &
                      + (haf - sgnm)*srd2(i, j, k)
            srdpi(i) = one/srdp(i)
            srdmi(i) = one/srdm(i)
            vchp(i) = vave(i) - absb*srdpi(i)
            vchm(i) = vave(i) + absb*srdmi(i)
          end do
          !
          !  Interpolate 1-D vectors of "vi2" and "b2" in the 1-direction to
          !  the footpoints of both characteristics.
          !
          do i = ism2, iep2
            vtmp(i) = vi2(i, j, k) - vg2(j)
            btmp(i) = b2(i, j, k)
            !**            vpch(i) = zro
            !**            vmch(i) = zro
            !**            bpch(i) = zro
            !**            bmch(i) = zro
          end do
          call x1zc1d(vtmp, vchp, vchm, iords2, istps2, j, k, vpch, vmch)
          call x1zc1d(btmp, vchp, vchm, iordb2, istpb2, j, k, bpch, bmch)
          !
          !  Evaluate "vsnm1" and "bsnm1" by solving the characteristic
          !  equations.  The source term is non-zero for RTP coordinates
          !  since dg2/dx1 = 1.0.  Compute the two terms in "emf3".
          !
          do i = is, iep1
            q2 = sign(one, bave(i))
            if (geometry == 'rtp' .or. geometry == 'rmp') then
              im1 = i - ione
              src = dt*dg2ad1(i)*g2ai(i)*bave(i) &
                    *(b2(i, j, k) + b2(im1, j, k)) &
                    /(srd2(i, j, k)**2 + srd2(im1, j, k)**2)
            end if
            vsnm1(i) = (vpch(i)*srdp(i) + vmch(i)*srdm(i) &
                        + q2*(bpch(i) - bmch(i))) &
                       /(srdp(i) + srdm(i)) + src
            bsnm1(i) = (bpch(i)*srdpi(i) + bmch(i)*srdmi(i) &
                        + q2*(vpch(i) - vmch(i))) &
                       /(srdpi(i) + srdmi(i))
            !
            vsnm1(i) = vsnm1(i)*bave(i)
            bsnm1(i) = bsnm1(i)*vave(i)
            !
            vsnp1(i, j, k) = haf*(vsnp1(i, j, k) + bsnm1(i))
            bsnp1(i, j, k) = haf*(vsnm1(i) + bsnp1(i, j, k))
          end do
        end do
      end if
      !
      !   END OF K LOOP
      !
    end do
    call writemessage(routinename//'_e3c1', 2)
    call writedata(vsnp1, routinename//'_e3c1vA', 4)
    call writedata(bsnp1, routinename//'_e3c1bA', 4)

    if (l_lastnode2) then
      bsnp1(:, jep1, :) = zro
      if (bc_sign == -one) vsnp1(:, jep1, :) = zro
    end if

    do k = ks, ke
      do j = js, jep1
        do i = is, iep1
          emf3(i, j, k) = vsnp1(i, j, k) - bsnp1(i, j, k)
        end do
      end do
    end do
    call release_memory(bsnp1); call release_memory(vsnp1)
  END SUBROUTINE hsmoc

  !** SUBGROUP: HYDRO TRANSPORT ******************************************
  !*                                                                     *
  !*          S U B G R O U P : H Y D R O    T R A N S P O R T           *
  !*                                                                     *
  !***********************************************************************
  !
  !=======================================================================
  !
  SUBROUTINE transprt
    !
    !    mln:zeus3d.transprt <------------------ controls the transport step
    !                                                          october, 1987
    !
    !    written by: Mike Norman
    !    modified 1: June, 1988 by Jim Stone; incorporated into ZEUS2D
    !    modified 2: February, 1990 by David Clarke; incorporated into
    !                ZEUS3D
    !
    !  PURPOSE: This subroutine transports the field variables through the
    !  mesh in a directionally split manner.  In each succesive call to
    !  TRANSPRT, the order of the directions is permuted (resulting in
    !  XYZ...YXZ...YZX...ZYX...ZXY...XZY...XYZ...etc.).  This MAY be better
    !  than leaving the order the same each time (XYZ...XYZ...etc), and
    !  seems to be better than unsplit schemes (Hawley).  Momenta are
    !  computed from velocities in VTOS and then transported.  Velocities
    !  are not updated until the end of the transport step in STOV.
    !  Note that the order in which variables are transported is important
    !  (especially d).
    !
    !  Also note that updating the boundaries is not really
    !  necessary during this step. [RFK: this statement may be not true.]
    !
    !  However, boundaries are updated anyway so that data dumps may be made,
    !  and so that eventual user-supplied routines inserted after the transport
    !  step need not worry about what boundaries are and are not set.
    !
    !  LOCAL VARIABLES:
    !    s*      momentum densities in the 1-, 2-, and 3-directions
    !    mflx*   mass fluxes in the 1-, 2-, and 3-directions
    !
    !-----------------------------------------------------------------------
    !
    CHARACTER(LEN=*), PARAMETER :: routinename = 'z_transprt'
    !
    REALNUM, POINTER, SAVE :: s1(:, :, :) => null(), s2(:, :, :) => null(), &
                              s3(:, :, :) => null()
    REALNUM, POINTER, SAVE :: massflux(:, :, :) => null()

    call get_memory(s1)
    if (withv2) call get_memory(s2)
    if (withv3) call get_memory(s3)
    call get_memory(massflux)
    ! mflx1 => massflux
    ! mflx2 => massflux
    ! mflx3 => massflux

    !
    !----------------------------------------------------------------------
    !
    !      Construct momentum densities from velocities.
    !
    call vtos(s1, s2, s3)
    call writedata(s1, routinename//'_1_s1', 3)
    if (withv2) call writedata(s2, routinename//'_1_s2', 3)
    if (withv3) call writedata(s3, routinename//'_1_s3', 3)
    call writedata(ix1x2x3, routinename//'_ix1x2x3', 4)

    if (sweepdimension == 3) then
      directional_split:select case(ix1x2x3)
      case (1) ! directional split in X1-X2-X3 fashion
      !
      call tranx1(massflux)
      call momx1(massflux, s1, s2, s3)
      !
      call tranx2(massflux)
      call momx2(massflux, s1, s2, s3)
      !
      call tranx3(massflux)
      call momx3(massflux, s1, s2, s3)
      !
      ix1x2x3 = 2

      case (2) ! directional split in X2-X1-X3 fashion
      !
      call tranx2(massflux)
      call momx2(massflux, s1, s2, s3)
      !
      call tranx1(massflux)
      call momx1(massflux, s1, s2, s3)
      !
      call tranx3(massflux)
      call momx3(massflux, s1, s2, s3)
      !
      ix1x2x3 = 3

      case (3) ! directional split in X2-X3-X1 fashion
      !
      call tranx2(massflux)
      call momx2(massflux, s1, s2, s3)
      !
      call tranx3(massflux)
      call momx3(massflux, s1, s2, s3)
      !
      call tranx1(massflux)
      call momx1(massflux, s1, s2, s3)
      !
      ix1x2x3 = 4

      case (4) ! directional split in X3-X2-X1 fashion
      !
      call tranx3(massflux)
      call momx3(massflux, s1, s2, s3)
      !
      call tranx2(massflux)
      call momx2(massflux, s1, s2, s3)
      !
      call tranx1(massflux)
      call momx1(massflux, s1, s2, s3)
      !
      ix1x2x3 = 5

      case (5) ! directional split in X3-X1-X2 fashion
      !
      call tranx3(massflux)
      call momx3(massflux, s1, s2, s3)
      !
      call tranx1(massflux)
      call momx1(massflux, s1, s2, s3)
      !
      call tranx2(massflux)
      call momx2(massflux, s1, s2, s3)
      !
      ix1x2x3 = 6

      case (6) ! directional split in X1-X3-X2 fashion
      !
      call tranx1(massflux)
      call momx1(massflux, s1, s2, s3)

      call tranx3(massflux)
      call momx3(massflux, s1, s2, s3)
      !
      call tranx2(massflux)
      call momx2(massflux, s1, s2, s3)
      !
      ix1x2x3 = 1
      case default ! Strange error
      call threeoutput(routinename, 'Value of ix1x2x3 not understood.')
      call finalstop(routinename, 'Execution stops.')
      end select directional_split
    else
      directional_split_2D:select case(ix1x2x3)
      case (1) ! directional split in X1-X2 fashion
      call tranx1(massflux)
      call momx1(massflux, s1, s2, s3)
      call tranx2(massflux)
      call momx2(massflux, s1, s2, s3)
      ix1x2x3 = 2
      case (2) ! directional split in X2-X1 fashion
      call tranx2(massflux)
      call momx2(massflux, s1, s2, s3)
      call tranx1(massflux)
      call momx1(massflux, s1, s2, s3)
      ix1x2x3 = 1
      case default ! Strange error
      call threeoutput(routinename, 'Value of ix1x2x3 not understood.')
      call threeoutput(routinename, 'Reset to 1.')
      end select directional_split_2D
    end if

    call release_memory(massflux)
    ! nullify(mflx1)
    ! nullify(mflx2)
    ! nullify(mflx3)
    !
    !-----------------------------------------------------------------------
    !
    !     Enforce the density and temperature floors
    !
    ! call density_floor
    ! call temperature_floor

    !
    !      Update boundary values for density and internal energy.
    !
    call boundary('d_')
    if (ienergy == 1) call boundary('e_')
    call writedata(d_, routinename//'_dY', 4)
    if (ienergy == 1) call writedata(e_, routinename//'_eY', 4)
    call writedata(s1, routinename//'_s1Y', 4)
    if (withv2) call writedata(s2, routinename//'_s2Y', 4)
    if (withv3) call writedata(s3, routinename//'_s3Y', 4)
    !
    !      Update velocities from momentum densities.
    !
    call stov(s1, s2, s3)
    if (withv3) call release_memory(s3)
    if (withv2) call release_memory(s2)
    call release_memory(s1)

    call writedata(v1, routinename//'_v1Y', 4)
    if (withv2) call writedata(v2, routinename//'_v2Y', 4)
    if (withv3) call writedata(v3, routinename//'_v3Y', 4)
    !
    !      Update velocity boundaries.
    !
    call boundary('v1')
    if (withv2) call boundary('v2')
    if (withv3) call boundary('v3')
    !
    if (withv2) call writedata(v2, routinename//'_v2Z', 4)
    if (withv3) call writedata(v3, routinename//'_v3Z', 4)
    !
  END SUBROUTINE transprt
  !
  !=======================================================================
  !
  SUBROUTINE tranx1(mflx1)
    !
    !    dac:zeus3d.tranx1 <----- transports zone-centred variables along x1
    !    from jms:zeus2d.tranx1, mln:zeus04.tranz                  may, 1990
    !
    !    written by: David Clarke
    !    modified 1: June 1992, by David Clarke; added the total energy
    !                option originally designed by Byung-IL Jun.
    !
    !  PURPOSE:  Transports all zone centred variables in the 1-direction
    !  only.  Currently transported are:
    !
    !                      mass   density
    !                      energy density
    !
    !  The consistent advection algorithm, in which mass fluxes are used to
    !  construct the fluxes of all variables across the interfaces, is used
    !  for all hydrodynamical variables.  Thus, the mass fluxes are passed
    !  to MOMX1 on order to transport the momenta as well.  The magnetic
    !  field components are treated separately from the mass fluxes in CT.
    !  Interpolations are done in X1ZC3D.  Note that the specific internal
    !  energy density (eod = e/d) is computed before the density is updated.
    !
    !  INPUT VARIABLES: [NONE]
    !
    !  OUTPUT VARIABLES:
    !    mflx1    mass flux in 1-direction
    !
    !  LOCAL VARIABLES:
    !    atwid1   (1-D) effective cross sectional area of the 1-interfaces
    !    eod      (3-D) (e+p)/d                        (itote=1)
    !                   e/d = specific internal energy (itote=0)
    !    p        (3-D) pressure - used by the interpolating routine
    !    etwid1   (3-D) interpolated specific energy densities (e/d) at all
    !             1-interfaces
    !    eflx1    (3-D) energy density flux across all 1-interfaces
    !    dtwid1   (3-D) interpolated mass densities at all 1-interfaces
    !    dflx1    (3-D) mass density flux across all 1-interfaces
    !
    !-----------------------------------------------------------------------
    !
    USE problem_variables, only: bc_sign
#ifdef MPI
    USE mpi
    INTEGER ierr
    REALNUM buffer(6), bufferi(6)
#endif /* MPI */

    REALNUM, INTENT(OUT) :: mflx1(:, :, :)
    !
    CHARACTER(LEN=*), PARAMETER :: routinename = 'z_tranx1'
    !
    INTEGER i, j, k
    !
    REALNUM atwid1(isa:iea)
    REALNUM, POINTER, SAVE, DIMENSION(:, :, :) :: eod => null(), &
                                                  dtwid1 => null(), &
                                                  b3od => null()
    REALNUM, POINTER, SAVE, DIMENSION(:, :, :) :: dflx1 => null(), &
                                                  etwid1 => null(), &
                                                  b3twid1 => null()

    REALNUM, POINTER, SAVE, DIMENSION(:, :, :) :: fieldflux => null(), &
                                                  fieldtwid => null()
    CHARACTER(LEN=filenamelength) :: fieldname
    INTEGER field

    REALNUM delta_x1_inner, delta_x1_inner_loss, delta_x1_inner_gain
    REALNUM delta_x1_outer, delta_x1_outer_loss, delta_x1_outer_gain

    !
    !-----------------------------------------------------------------------
    !
    !      Check for x1 symmetry.
    !
    if (inac == 0) return
    !
    !      Compute time-centered area factor.
    !
    ! RFK  do i=is,iep1
    do i = ism1, iep1
      atwid1(i) = g2ah(i)*g31ah(i)
    end do
    !
    !      Compute the quantity "eod", which will be used to determine the
    !  energy fluxes across the 1-interfaces.
    !
    if (ienergy == 1) then
      call get_memory(etwid1)
      call get_memory(eod)
      call spenergy(eod, 2)
      call writedata(eod, routinename//'_eod', 4)
      ! Perform interpolations at the 1-interfaces.
      call x1zc3d(eod, v1, js, ks, je, ke, iorde_, istpe_, etwid1)
      call release_memory(eod)
      call writedata(etwid1, routinename//'_etwA', 4)
    end if

    if (imhd /= 0 .and. withb3 .and. mhddimension == 2) then
      call get_memory(b3od)
      call get_memory(b3twid1)
      k = ks
      do j = jsg, jeg
        do i = isg, ieg
          b3od(i, j, k) = b3(i, j, k)/d_(i, j, k)
        end do
      end do
      call x1zc3d(b3od, v1, js, ks, je, ke, iorde_, istpe_, b3twid1)
      call writedata(b3od, routinename//'_b3od', 4)
      call release_memory(b3od)
      call writedata(b3twid1, routinename//'_b3twA', 4)
    end if

    call get_memory(dflx1)
    call get_memory(dtwid1)
    ! RFK  call x1zc3d(d_,v1,js  ,ks  ,je,ke,iordd_, istpd_, dtwid1 )
    call x1zc3d(d_, v1, jsm1, ksm1, je, ke, iordd_, istpd_, dtwid1)
    call writedata(dtwid1, routinename//'_dtw', 4)
    !
    !      For the purposes of consistent advection, construct the mass
    !  flux across each 1-interface.  The mass flux will be used to create
    !  the fluxes of all variables, including the momenta which are updated
    !  in MOMX1.
    !
    ! RFK  do k=ks,ke
    do k = ksm1, ke
      ! RFK    do j=js,je
      do j = jsm1, je
        ! RFK      do i=is,iep1
        do i = ism1, iep1
          mflx1(i, j, k) = dtwid1(i, j, k)*(v1(i, j, k) - vg1(i))*dt
          dflx1(i, j, k) = mflx1(i, j, k)*atwid1(i)
        end do
      end do
    end do
    call release_memory(dtwid1)
    call writedata(mflx1, routinename//'_mf', 4)
    call writedata(dflx1, routinename//'_df', 4)
    !
    if (ienergy == 1) then
      do k = ks, ke
        do j = js, je
          do i = is, iep1
            etwid1(i, j, k) = dflx1(i, j, k)*etwid1(i, j, k)
          end do
        end do
      end do
      call writedata(etwid1, routinename//'_etwB', 4)
    end if
    !
    !      Perform mass density and energy density advection.  Note that
    !  the timestep "dt" is hidden the fluxes "dflx1" and "eflx1".
    !
    call writedata(d_, routinename//'_1_d_', 4)
    if (moving_grid1 .or. allow_moving) then
      do k = ks, ke
        do j = js, je
          do i = is, ie
            d_(i, j, k) = (d_(i, j, k)*dvl1a(i) &
                           - dflx1(i + 1, j, k) + dflx1(i, j, k))*dvl1ani(i)
          end do
        end do
      end do
    else
      do k = ks, ke
        do j = js, je
          do i = is, ie
            d_(i, j, k) = d_(i, j, k) + &
                          (-dflx1(i + 1, j, k) + dflx1(i, j, k))*dvl1ani(i)
          end do
        end do
      end do
    end if

    if (withboundarymass) then

      delta_x1_inner = zro
      delta_x1_inner_loss = zro
      delta_x1_inner_gain = zro
      delta_x1_outer = zro
      delta_x1_outer_loss = zro
      delta_x1_outer_gain = zro

      if (l_firstnode1) then
        do k = ks, ke
          do j = js, je
            delta_x1_inner = delta_x1_inner + &
                             dflx1(is, j, k)*dvl2a(j)*dx3a(k)
            delta_x1_inner_loss = delta_x1_inner_loss + &
                                  max(zro, -dflx1(is, j, k))*dvl2a(j)*dx3a(k)
            delta_x1_inner_gain = delta_x1_inner_gain + &
                                  max(zro, dflx1(is, j, k))*dvl2a(j)*dx3a(k)
          end do
        end do
      end if
      if (l_lastnode1) then
        do k = ks, ke
          do j = js, je
            delta_x1_outer = delta_x1_outer + &
                             (-dflx1(iep1, j, k))*dvl2a(j)*dx3a(k)
            delta_x1_outer_loss = delta_x1_outer_loss + &
                                  max(zro, dflx1(iep1, j, k))*dvl2a(j)*dx3a(k)
            delta_x1_outer_gain = delta_x1_outer_gain + &
                                  max(zro, -dflx1(iep1, j, k))*dvl2a(j)*dx3a(k)
          end do
        end do
      end if
#ifdef MPI

      bufferi(1) = delta_x1_inner
      bufferi(2) = delta_x1_inner_loss
      bufferi(3) = delta_x1_inner_gain
      bufferi(4) = delta_x1_outer
      bufferi(5) = delta_x1_outer_loss
      bufferi(6) = delta_x1_outer_gain

      call MPI_REDUCE(bufferi, buffer, 6, &
                      MPI_FLOATMPI, MPI_SUM, rootnode, MPI_COMM_WORLD, ierr)

      delta_x1_inner = buffer(1)
      delta_x1_inner_loss = buffer(2)
      delta_x1_inner_gain = buffer(3)
      delta_x1_outer = buffer(4)
      delta_x1_outer_loss = buffer(5)
      delta_x1_outer_gain = buffer(6)

#endif /* MPI */

      if (node == rootnode) then
        mass_x1_inner = mass_x1_inner + delta_x1_inner
        mass_x1_inner_loss = mass_x1_inner_loss + delta_x1_inner_loss
        mass_x1_inner_gain = mass_x1_inner_gain + delta_x1_inner_gain
        mass_x1_outer = mass_x1_outer + delta_x1_outer
        mass_x1_outer_loss = mass_x1_outer_loss + delta_x1_outer_loss
        mass_x1_outer_gain = mass_x1_outer_gain + delta_x1_outer_gain
      end if

      if (withcentralmass) then
        if (node == rootnode) then
          if (bc_sign == -one) then ! Good only for certain BCs and geometries
            centralmass = centralmass + delta_x1_inner_loss
          else
            centralmass = centralmass + two*delta_x1_inner_loss
          end if
        end if
#ifdef MPI
        if (node == rootnode) then
          buffer(1) = centralmass
        end if
        call MPI_BCAST(buffer, 1, MPI_FLOATMPI, rootnode, &
                       MPI_COMM_WORLD, ierr)
        centralmass = buffer(1)
#endif /* MPI */
      end if
    end if

    call release_memory(dflx1)

    !RFK:  Sometime, check if the boundary call here is necessary
    call writedata(d_, routinename//'_2_d_', 4)
    call boundary('d_')
    call writedata(d_, routinename//'_3_d_', 4)

    if (ienergy == 1) then
      if (moving_grid1 .or. allow_moving) then
        do k = ks, ke
          do j = js, je
            do i = is, ie
              e_(i, j, k) = (e_(i, j, k)*dvl1a(i) &
                             - etwid1(i + 1, j, k) + etwid1(i, j, k))*dvl1ani(i)
            end do
          end do
        end do
      else
        do k = ks, ke
          do j = js, je
            do i = is, ie
              e_(i, j, k) = e_(i, j, k) + &
                            (-etwid1(i + 1, j, k) + etwid1(i, j, k))*dvl1ani(i)
            end do
          end do
        end do
      end if
      !RFK:  Sometime, check if the boundary call here is necessary
      call boundary('e_')
      call writedata(e_, routinename//'_e', 4)
      call release_memory(etwid1)
    end if
    !
    if (imhd /= 0 .and. withb3 .and. mhddimension == 2) then
      k = ks
      do j = js, je
        do i = is, iep1
          b3twid1(i, j, k) = mflx1(i, j, k)*b3twid1(i, j, k)*g2ah(i)
        end do
      end do
      if (moving_grid1 .or. allow_moving) then
        do j = js, je
          do i = is, ie
            b3(i, j, k) = (b3(i, j, k)*g2b(i)*dx1a(i) - &
                           (b3twid1(i + 1, j, k) - b3twid1(i, j, k))) &
                          *g2bni(i)*dx1ani(i)
          end do
        end do
      else
        do j = js, je
          do i = is, ie
            b3(i, j, k) = b3(i, j, k) - &
                          (b3twid1(i + 1, j, k) - b3twid1(i, j, k)) &
                          *g2bni(i)*dx1ani(i)
          end do
        end do
      end if
      call release_memory(b3twid1)
      call boundary('b3')
    end if

    if (number_of_tracer_fields > 0) then
      call writedata(tracerfields, routinename//'_tfA', 4)
      call get_memory(fieldflux)
      call get_memory(fieldtwid)
      do field = 1, number_of_tracer_fields
        call x1zc3d(tracerfields(:, :, :, field), &
                    v1, jsm1, ksm1, je, ke, iordd_, istpd_, fieldtwid)
        write (fieldname, '(i0)') field
        call writedata(fieldtwid, routinename//'_ttw'//trim(fieldname), 4)
        do k = ksm1, ke
          do j = jsm1, je
            do i = ism1, iep1
              fieldflux(i, j, k) = fieldtwid(i, j, k)* &
                                   (v1(i, j, k) - vg1(i))*dt*atwid1(i)
            end do
          end do
        end do
        call writedata(fieldflux, routinename//'_tflux'//trim(fieldname), 4)
        if (moving_grid1 .or. allow_moving) then
          do k = ks, ke
            do j = js, je
              do i = is, ie
                tracerfields(i, j, k, field) = &
                  (tracerfields(i, j, k, field)*dvl1a(i) &
                   - fieldflux(i + 1, j, k) + fieldflux(i, j, k))*dvl1ani(i)
              end do
            end do
          end do
        else
          do k = ks, ke
            do j = js, je
              do i = is, ie
                tracerfields(i, j, k, field) = &
                  tracerfields(i, j, k, field) + &
                  (-fieldflux(i + 1, j, k) + fieldflux(i, j, k))*dvl1ani(i)
              end do
            end do
          end do
        end if
      end do
      call writedata(tracerfields, routinename//'_tfB', 4)
      call release_memory(fieldtwid)
      call release_memory(fieldflux)
      call boundary('tf')
      call writedata(tracerfields, routinename//'_tfC', 4)
    end if
  END SUBROUTINE tranx1
  !
  !=======================================================================
  !
  SUBROUTINE tranx2(mflx2)
    !
    !    dac:zeus3d.tranx2 <----- transports zone-centred variables along x2
    !    from jms:zeus2d.tranx2, mln:zeus04.tranr                  may, 1990
    !
    !    written by: David Clarke
    !    modified 1: June 1992, by David Clarke; added the total energy
    !                option originally designed by Byung-IL Jun.
    !
    !  PURPOSE:  Transports all zone centred variables in the 2-direction
    !  only.  Currently transported are:
    !
    !                      mass   density
    !                      energy density
    !
    !  The consistent advection algorithm, in which mass fluxes are used to
    !  construct the fluxes of all variables across the interfaces, is used
    !  for all hydrodynamical variables.  Thus, the mass fluxes are passed
    !  to MOMX2 on order to transport the momenta as well.  The magnetic
    !  field components are treated separately from the mass fluxes in CT.
    !  Interpolations are done in X2ZC3D.  Note that the specific internal
    !  energy density (eod = e/d) is computed before the density is updated.
    !
    !  INPUT VARIABLES: [NONE]
    !
    !  OUTPUT VARIABLES:
    !    mflx2    mass flux in 2-direction
    !
    !  LOCAL VARIABLES:
    !    atwid1   (1-D) effective cross sectional area of the 1-interfaces
    !    eod      (3-D) (e+p)/d                        (itote=1)
    !                   e/d = specific internal energy (itote=0)
    !    p        (3-D) pressure - used by the interpolating routine
    !    etwid2   (3-D) interpolated specific energy densities (e/d) at all
    !             2-interfaces
    !    eflx2    (3-D) energy density flux across all 2-interfaces
    !    dtwid2   (3-D) interpolated mass densities at all 2-interfaces
    !    dflx2    (3-D) mass density flux across all 2-interfaces
    !
    !-----------------------------------------------------------------------
    !
    REALNUM, INTENT(OUT) :: mflx2(:, :, :)
    !
    CHARACTER(LEN=*), PARAMETER :: routinename = 'z_tranx2'
    !
    INTEGER i, j, k
    !
    REALNUM atwid1(isa:iea)
    REALNUM, POINTER, SAVE, DIMENSION(:, :, :) :: eod => null(), &
                                                  dtwid2 => null(), &
                                                  b3od => null()
    REALNUM, POINTER, SAVE, DIMENSION(:, :, :) :: dflx2 => null(), &
                                                  etwid2 => null(), &
                                                  b3twid2 => null()

    REALNUM, POINTER, SAVE, DIMENSION(:, :, :) :: fieldflux => null(), &
                                                  fieldtwid => null()
    CHARACTER(LEN=filenamelength) :: fieldname
    INTEGER field
    !
    !-----------------------------------------------------------------------
    !      Check for x2 symmetry and presence of v2
    if (jnac == 0 .or. .not. withv2) return
    !
    !      Compute time-centred area factor.
    !
    ! RFK  do i=is,ie
    do i = ism1, ie
      atwid1(i) = g31b(i)*dx1a(i)*dvl1ai(i)
    end do
    !
    !      Compute the quantity "eod", which will be used to determine the
    !  energy fluxes across the 2-interfaces.
    !
    if (ienergy == 1) then
      call get_memory(etwid2)
      call get_memory(eod)
      call spenergy(eod, 2)
      call writedata(eod, routinename//'_eod', 4)
      ! Perform interpolations at the 2-interfaces.
      call x2zc3d(eod, v2, ks, is, ke, ie, iorde_, istpe_, g2b, g2bi, etwid2)
      call writedata(etwid2, routinename//'_etwA', 4)
      call release_memory(eod)
    end if
    if (imhd /= 0 .and. withb3 .and. mhddimension == 2) then
      call get_memory(b3od)
      call get_memory(b3twid2)
      k = ks
      do j = jsg, jeg
        do i = isg, ieg
          b3od(i, j, k) = b3(i, j, k)/d_(i, j, k)
        end do
      end do
      call x2zc3d(b3od, v2, ks, is, ke, ie, iorde_, istpe_, g2b, g2bi, b3twid2)
      call writedata(b3od, routinename//'_b3od', 4)
      call release_memory(b3od)
      call writedata(b3twid2, routinename//'_b3twA', 4)
    end if

    call get_memory(dflx2)
    call get_memory(dtwid2)
    ! RFK  call x2zc3d(d_, v2, ks, is, ke, ie, iordd_, istpd_
    call x2zc3d(d_, v2, ksm1, ism1, ke, ie, iordd_, istpd_, g2b, g2bi, dtwid2)
    call writedata(dtwid2, routinename//'_dtw', 4)
    !
    !      For the purposes of consistent advection, construct the mass
    !  flux across each 2-interface.  The mass flux will be used to create
    !  the fluxes of all variables, including the momenta which are updated
    !  in MOMX2.
    !
    ! RFK  do k=ks,ke
    do k = ksm1, ke
      ! RFK    do j=js,jep1
      do j = jsm1, jep1
        ! RFK      do i=is,ie
        do i = ism1, ie
          mflx2(i, j, k) = dtwid2(i, j, k)*(v2(i, j, k) - vg2(j))*dt
          dflx2(i, j, k) = mflx2(i, j, k)*atwid1(i)*g32ah(j)
        end do
      end do
    end do
    call release_memory(dtwid2)

    call writedata(mflx2, routinename//'_mf', 4)
    call writedata(dflx2, routinename//'_df', 4)

    if (ienergy == 1) then
      do k = ks, ke
        do j = js, jep1
          do i = is, ie
            etwid2(i, j, k) = dflx2(i, j, k)*etwid2(i, j, k)
          end do
        end do
      end do
      call writedata(etwid2, routinename//'_etwB', 4)
    end if

    !
    !      Perform mass density and energy density advection.  Note that
    !  the timestep "dt" is hidden the fluxes "dflx2" and "eflx2".
    !
    call writedata(d_, routinename//'_1_d_', 4)
    if (moving_grid2 .or. allow_moving) then
      do k = ks, ke
        do j = js, je
          do i = is, ie
            d_(i, j, k) = (d_(i, j, k)*dvl2a(j) &
                           - dflx2(i, j + 1, k) + dflx2(i, j, k))*dvl2ani(j)
          end do
        end do
      end do
    else
      do k = ks, ke
        do j = js, je
          do i = is, ie
            d_(i, j, k) = d_(i, j, k) + &
                          (-dflx2(i, j + 1, k) + dflx2(i, j, k))*dvl2ani(j)
          end do
        end do
      end do
    end if

    call release_memory(dflx2)
    call writedata(d_, routinename//'_2_d_', 4)
    call boundary('d_')
    call writedata(d_, routinename//'_3_d_', 4)

    if (ienergy == 1) then
      if (moving_grid2 .or. allow_moving) then
        do k = ks, ke
          do j = js, je
            do i = is, ie
              e_(i, j, k) = (e_(i, j, k)*dvl2a(j) &
                             - etwid2(i, j + 1, k) + etwid2(i, j, k))*dvl2ani(j)
            end do
          end do
        end do
      else
        do k = ks, ke
          do j = js, je
            do i = is, ie
              e_(i, j, k) = e_(i, j, k) + &
                            (-etwid2(i, j + 1, k) + etwid2(i, j, k))*dvl2ani(j)
            end do
          end do
        end do
      end if
      call writedata(e_, routinename//'_eA', 4)
      call writedata(dvl2a, routinename//'_dvl2a', 4)
      call writedata(dvl2ani, routinename//'_dvl2ani', 4)
      call boundary('e_')
      call writedata(e_, routinename//'_eB', 4)
      call release_memory(etwid2)
    end if
    if (imhd /= 0 .and. withb3 .and. mhddimension == 2) then
      k = ks
      do j = js, jep1
        do i = is, ie
          b3twid2(i, j, k) = mflx2(i, j, k)*b3twid2(i, j, k)*g2bi(i)
        end do
      end do
      if (moving_grid2 .or. allow_moving) then
        do j = js, je
          do i = is, ie
            b3(i, j, k) = (b3(i, j, k)*dx2a(j) - &
                           (b3twid2(i, j + 1, k) - b3twid2(i, j, k)))*dx2ani(j)
          end do
        end do
      else
        do j = js, je
          do i = is, ie
            b3(i, j, k) = b3(i, j, k) - &
                          (b3twid2(i, j + 1, k) - b3twid2(i, j, k))*dx2ani(j)
          end do
        end do
      end if
      call release_memory(b3twid2)
      call boundary('b3')
    end if

    if (number_of_tracer_fields > 0) then
      call writedata(tracerfields, routinename//'_tfA', 4)
      call get_memory(fieldflux)
      call get_memory(fieldtwid)
      do field = 1, number_of_tracer_fields
        call x2zc3d(tracerfields(:, :, :, field), v2, ksm1, ism1, ke, ie, &
                    iordd_, istpd_, g2b, g2bi, fieldtwid)
        write (fieldname, '(i0)') field
        call writedata(fieldtwid, routinename//'_ttw'//trim(fieldname), 4)
        do k = ksm1, ke
          do j = jsm1, jep1
            do i = ism1, ie
              fieldflux(i, j, k) = fieldtwid(i, j, k)* &
                                   (v2(i, j, k) - vg2(j))*dt*atwid1(i)*g32ah(j)
            end do
          end do
        end do
        call writedata(fieldflux, routinename//'_tflux'//trim(fieldname), 4)
        if (moving_grid2 .or. allow_moving) then
          do k = ks, ke
            do j = js, je
              do i = is, ie
                tracerfields(i, j, k, field) = &
                  (tracerfields(i, j, k, field)*dvl2a(j) &
                   - fieldflux(i, j + 1, k) + fieldflux(i, j, k))*dvl2ani(j)
              end do
            end do
          end do
        else
          do k = ks, ke
            do j = js, je
              do i = is, ie
                tracerfields(i, j, k, field) = &
                  tracerfields(i, j, k, field) + &
                  (-fieldflux(i, j + 1, k) + fieldflux(i, j, k))*dvl2ani(j)
              end do
            end do
          end do
        end if
      end do
      call writedata(tracerfields, routinename//'_tfB', 4)
      call release_memory(fieldtwid)
      call release_memory(fieldflux)
      call boundary('tf')
      call writedata(tracerfields, routinename//'_tfC', 4)
    end if
  END SUBROUTINE tranx2
  !
  !=======================================================================
  !
  SUBROUTINE tranx3(mflx3)
    !
    !    dac:zeus3d.tranx3 <----- transports zone-centred variables along x3
    !                                                              may, 1990
    !
    !    written by: David Clarke
    !    modified 1: June 1992, by David Clarke; added the total energy
    !                option originally designed by Byung-IL Jun.
    !
    !  PURPOSE:  Transports all zone centred variables in the 3-direction
    !  only.  Currently transported are:
    !
    !                      mass   density
    !                      energy density
    !
    !  The consistent advection algorithm, in which mass fluxes are used to
    !  construct the fluxes of all variables across the interfaces, is used
    !  for all hydrodynamical variables.  Thus, the mass fluxes are passed
    !  to MOMX3 on order to transport the momenta as well.  The magnetic
    !  field components are treated separately from the mass fluxes in CT.
    !  Interpolations are done in X3ZC3D.  Note that the specific internal
    !  energy density (eod = e/d) is computed before the density is updated.
    !
    !  INPUT VARIABLES: [NONE]
    !
    !  OUTPUT VARIABLES:
    !    mflx3    mass flux in 3-direction
    !
    !  LOCAL VARIABLES:
    !    atwid1   (1-D) effective cross sectional area of the 1-interfaces
    !    atwid2   (1-D) effective cross sectional area of the 2-interfaces
    !    eod      (3-D) (e+p)/d                        (itote=1)
    !                   e/d = specific internal energy (itote=0)
    !    p        (3-D) pressure - used by the interpolating routine
    !    etwid3   (3-D) interpolated specific energy densities (e/d) at all
    !             3-interfaces
    !    eflx3    (3-D) energy density flux across all 3-interfaces
    !    dtwid3   (3-D) interpolated mass densities at all 3-interfaces
    !    dflx3    (3-D) mass density flux across all 3-interfaces
    !
    !-----------------------------------------------------------------------
    !
    REALNUM, INTENT(OUT) :: mflx3(:, :, :)
    !
    CHARACTER(LEN=*), PARAMETER :: routinename = 'z_tranx3'
    !
    INTEGER i, j, k
    !
    REALNUM atwid1(isa:iea), atwid2(jsa:jea)
    REALNUM, POINTER, SAVE :: eod(:, :, :) => null(), dtwid3(:, :, :) => null()
    REALNUM, POINTER, SAVE :: dflx3(:, :, :) => null(), &
                              etwid3(:, :, :) => null()

    REALNUM, POINTER, SAVE, DIMENSION(:, :, :) :: fieldflux => null(), &
                                                  fieldtwid => null()
    CHARACTER(LEN=filenamelength) :: fieldname
    INTEGER field
    !
    !-----------------------------------------------------------------------
    !      Check for x3 symmetry and presence of v3
    if (knac == 0 .or. .not. withv3) return
    !
    !      Compute time-centred area factors.
    !
    ! RFK  do i=is,ie
    do i = ism1, ie
      atwid1(i) = g2b(i)*dx1a(i)*dvl1ai(i)
    end do
    ! RFK  do j=js,je
    do j = jsm1, je
      atwid2(j) = dx2a(j)*dvl2ai(j)
    end do

    !
    !      Compute the quantity "eod", which will be used to determine the
    !  energy fluxes across the 3-interfaces.
    !
    if (ienergy == 1) then
      call get_memory(etwid3)
      call get_memory(eod)
      call spenergy(eod, 2)
      call writedata(eod, routinename//'_eod', 4)
      ! Perform interpolations at the 3-interfaces.
      call x3zc3d(eod, v3, is, js, ie, je, iorde_, istpe_, g31b, g31bi, g32b, &
                  g32bi, etwid3)
      call writedata(etwid3, routinename//'_etwA', 4)
      call release_memory(eod)
    end if

    call get_memory(dflx3)
    call get_memory(dtwid3)
    ! RFK  call x3zc3d(d_, v3, is, js, ie, je, iordd_, istpd_
    call x3zc3d(d_, v3, ism1, jsm1, ie, je, iordd_, istpd_, g31b, g31bi, g32b, &
                g32bi, dtwid3)
    call writedata(dtwid3, routinename//'_dtw', 4)
    !
    !      For the purposes of consistent advection, construct the mass
    !  flux across each 3-interface.  The mass flux will be used to create
    !  the fluxes of all variables, including the momenta which are updated
    !  in MOMX3.
    !
    ! RFK  do k=ks,kep1
    do k = ksm1, kep1
      ! RFK    do j=js,je
      do j = jsm1, je
        ! RFK      do i=is,ie
        do i = ism1, ie
          mflx3(i, j, k) = dtwid3(i, j, k)*(v3(i, j, k) - vg3(k))*dt
          dflx3(i, j, k) = mflx3(i, j, k)*atwid1(i)*atwid2(j)
        end do
      end do
    end do
    call writedata(mflx3, routinename//'_mf', 4)
    call writedata(dflx3, routinename//'_df', 4)

    if (ienergy == 1) then
      do k = ks, kep1
        do j = js, je
          do i = is, ie
            etwid3(i, j, k) = dflx3(i, j, k)*etwid3(i, j, k)
          end do
        end do
      end do
      call writedata(etwid3, routinename//'_etwB', 4)
    end if

    !
    !      Perform mass density and energy density advection.  Note that
    !  the timestep "dt" is hidden the fluxes "dflx3" and "eflx3".
    !
    call writedata(d_, routinename//'_1_d_', 4)
    if (moving_grid3 .or. allow_moving) then
      do k = ks, ke
        do j = js, je
          do i = is, ie
            d_(i, j, k) = (d_(i, j, k)*dvl3a(k) &
                           - dflx3(i, j, k + 1) + dflx3(i, j, k))*dvl3ani(k)
          end do
        end do
      end do
    else
      do k = ks, ke
        do j = js, je
          do i = is, ie
            d_(i, j, k) = d_(i, j, k) + &
                          (-dflx3(i, j, k + 1) + dflx3(i, j, k))*dvl3ani(k)
          end do
        end do
      end do
    end if
    call writedata(d_, routinename//'_2_d_', 4)
    call release_memory(dtwid3)
    call release_memory(dflx3)
    call boundary('d_')
    call writedata(d_, routinename//'_3_d_', 4)

    if (ienergy == 1) then
      if (moving_grid3 .or. allow_moving) then
        do k = ks, ke
          do j = js, je
            do i = is, ie
              e_(i, j, k) = (e_(i, j, k)*dvl3a(k) &
                             - etwid3(i, j, k + 1) + etwid3(i, j, k))*dvl3ani(k)
            end do
          end do
        end do
      else
        do k = ks, ke
          do j = js, je
            do i = is, ie
              e_(i, j, k) = e_(i, j, k) + &
                            (-etwid3(i, j, k + 1) + etwid3(i, j, k))*dvl3ani(k)
            end do
          end do
        end do
      end if
      call boundary('e_')
      call writedata(e_, routinename//'_e', 4)
      call release_memory(etwid3)
    end if
    !
    if (number_of_tracer_fields > 0) then
      call writedata(tracerfields, routinename//'_tfA', 4)
      call get_memory(fieldflux)
      call get_memory(fieldtwid)
      do field = 1, number_of_tracer_fields
        call x3zc3d(tracerfields(:, :, :, field), &
                    v3, ism1, jsm1, ie, je, iordd_, istpd_, &
                    g31b, g31bi, g32b, g32bi, fieldtwid)
        write (fieldname, '(i0)') field
        call writedata(fieldtwid, routinename//'_ttw'//trim(fieldname), 4)
        do k = ksm1, kep1
          do j = jsm1, je
            do i = ism1, ie
              fieldflux(i, j, k) = fieldtwid(i, j, k)* &
                                   (v3(i, j, k) - vg3(k))*dt*atwid1(i)*atwid2(j)
            end do
          end do
        end do
        call writedata(fieldflux, routinename//'_tflux'//trim(fieldname), 4)
        if (moving_grid3 .or. allow_moving) then
          do k = ks, ke
            do j = js, je
              do i = is, ie
                tracerfields(i, j, k, field) = &
                  (tracerfields(i, j, k, field)*dvl3a(k) &
                   - fieldflux(i, j, k + 1) + fieldflux(i, j, k))*dvl3ani(k)
              end do
            end do
          end do
        else
          do k = ks, ke
            do j = js, je
              do i = is, ie
                tracerfields(i, j, k, field) = &
                  tracerfields(i, j, k, field) + &
                  (-fieldflux(i, j, k + 1) + fieldflux(i, j, k))*dvl3ani(k)
              end do
            end do
          end do
        end if
      end do
      call writedata(tracerfields, routinename//'_tfB', 4)
      call release_memory(fieldtwid)
      call release_memory(fieldflux)
      call boundary('tf')
      call writedata(tracerfields, routinename//'_tfC', 4)
    end if
  END SUBROUTINE tranx3
  !
  !=======================================================================
  !
  SUBROUTINE momx1(mflx1, s1, s2, s3)
    !
    !    dac:zeus3d.momx1 <--------------- transports momenta in 1-direction
    !    from jms:zeus2d.momx1, mln:zeus04.momz                    may, 1990
    !
    !    written by: David Clarke
    !    modified 1: November, 1992 by David Clarke; momenta are now updated
    !                between and including i=is,ie, j=js,je, and k=ks,ke to
    !                allow for proper treatment of periodic boundaries.
    !
    !  PURPOSE:  Transports the three components of the momentum density in
    !  the 1-direction using the consistent transport algorithm, including
    !  the effects of grid compression.  The transported fluxes are thus
    !  given by the mass fluxes times the time centred area of the control
    !  volume faces times the interpolated velocities.  Interpolations are
    !  performed in X1FC3D and X1ZC3D.
    !
    !  INPUT VARIABLES:
    !    mflx1   mass flux in 1-direction (computed in TRANX1)
    !    s1      momentum density in 1-direction
    !    s2      momentum density in 2-direction
    !    s3      momentum density in 3-direction
    !
    !  OUTPUT VARIABLES:
    !    s1      momentum density in 1-direction updated in the 1-direction
    !    s2      momentum density in 2-direction updated in the 1-direction
    !    s3      momentum density in 3-direction updated in the 1-direction
    !
    !  LOCAL VARIABLES:
    !    vel1    velocity used for upwinding in interpolation routine
    !    vtwid1  interpolated velocity
    !    sflx1   momentum fluxes
    !
    !-----------------------------------------------------------------------
    !
    REALNUM, INTENT(IN) :: mflx1(:, :, :)
    REALNUM, DIMENSION(:, :, :), INTENT(INOUT) :: s1, s2, s3
    !
    CHARACTER(LEN=*), PARAMETER :: routinename = 'z_momx1'
    !
    INTEGER i, j, k, jm1, km1
    !
    REALNUM, DIMENSION(isa:iea) :: atwid1, dvol1a, dvol1ani, dvol1b, dvol1bni

    REALNUM, POINTER, SAVE :: vtwid1(:, :, :) => null(), &
                              sflx1(:, :, :) => null(), &
                              vel1(:, :, :) => null()
    REALNUM, POINTER, SAVE :: allocated_space(:, :, :) => null()
    !-----------------------------------------------------------------------
    !
    !      Check for X1 symmetry.
    !
    if (inac == 0) return
    !-----------------------------------------------------------------------
    !
    !      Allocate local arrays
    !
    call get_memory(allocated_space)
    vel1 => allocated_space
    sflx1 => allocated_space
    call get_memory(vtwid1)
    !
    !      Set volumes.
    !
    do i = is, ie
      dvol1a(i) = dvl1a(i)
      dvol1ani(i) = dvl1ani(i)
      dvol1b(i) = dvl1b(i)
      dvol1bni(i) = dvl1bni(i)
    end do
    !
    !---------------------------- TRANSPORT S1 -----------------------------
    !
    !      Compute time-centred area factor.
    !
    ! RFK  do i=is,ie
    do i = ism1, ie
      atwid1(i) = haf*g2bh(i)*g31bh(i)
    end do
    !
    !      Construct an i-average of "v1-vg1" to be used for interpolation.
    !
    do k = ks, ke
      do j = js, je
        ! RFK      do i=is,ie
        do i = ism1, ie
          vel1(i, j, k) = haf*(v1(i, j, k) - vg1(i) &
                               + v1(i + 1, j, k) - vg1(i + 1))
        end do
      end do
    end do
    call writedata(vel1, routinename//'_vel1A', 4)
    !
    !      Interpolate "v1" at the zone centres.
    !
    call x1fc3d(v1, vel1, js, ks, je, ke, iords1, vtwid1)
    call writedata(vtwid1, routinename//'_vtw1A', 4)
    !
    !      Construct the 1-momentum flux at the zone centres and perform
    !  1-momentum advection.  Note that the timestep "dt" is hidden in the
    !  mass flux.
    !
    if (moving_grid1 .or. allow_moving) then
      do k = ks, ke
        do j = js, je
          ! RFK      do i=is,ie
          do i = ism1, ie
            sflx1(i, j, k) = (mflx1(i, j, k) + mflx1(i + 1, j, k)) &
                             *vtwid1(i, j, k)*atwid1(i)
          end do
          do i = is, ie
            s1(i, j, k) = (s1(i, j, k)*dvol1b(i) &
                           - sflx1(i, j, k) + sflx1(i - 1, j, k))*dvol1bni(i)
          end do
        end do
      end do
    else
      do k = ks, ke
        do j = js, je
          ! RFK      do i=is,ie
          do i = ism1, ie
            sflx1(i, j, k) = (mflx1(i, j, k) + mflx1(i + 1, j, k)) &
                             *vtwid1(i, j, k)*atwid1(i)
          end do
          do i = is, ie
            s1(i, j, k) = s1(i, j, k) + &
                          (-sflx1(i, j, k) + sflx1(i - 1, j, k))*dvol1bni(i)
          end do
        end do
      end do
    end if
    call writedata(sflx1, routinename//'_sflx1A', 4)
    call writedata(s1, routinename//'_s1', 4)
    if (withv2) then
      !
      !---------------------------- TRANSPORT S2 -----------------------------
      !
      !      Compute time centred area factor.
      !
      do i = is, iep1
        atwid1(i) = haf*g2a(i)*g2ah(i)*g31ah(i)
      end do
      !
      !      Construct a j-average of "v1" to be used for interpolation.
      !
      do k = ks, ke
        do j = js, je
          jm1 = j - jone
          do i = is, iep1
            vel1(i, j, k) = haf*(v1(i, jm1, k) + v1(i, j, k))
          end do
        end do
      end do
      call writedata(vel1, routinename//'_vel1B', 4)
      !
      !      Interpolate "v2" at the 1-interfaces.
      !
      call x1zc3d(v2, vel1, js, ks, je, ke, iords2, istps2, vtwid1)
      call writedata(vtwid1, routinename//'_vtw1B', 4)
      !
      !      Construct the 2-momentum flux at the 1-interfaces and perform
      !  2-momentum advection.  Note that the timestep "dt" is hidden in the
      !  mass flux.
      !
      if (moving_grid1 .or. allow_moving) then
        do j = js, je
          jm1 = j - jone
          do k = ks, ke
            do i = is, iep1
              sflx1(i, j, k) = (mflx1(i, jm1, k) + mflx1(i, j, k)) &
                               *vtwid1(i, j, k)*atwid1(i)
            end do
            do i = is, ie
              s2(i, j, k) = (s2(i, j, k)*dvol1a(i) &
                             - sflx1(i + 1, j, k) + sflx1(i, j, k))*dvol1ani(i)
            end do
          end do
        end do
      else
        do j = js, je
          jm1 = j - jone
          do k = ks, ke
            do i = is, iep1
              sflx1(i, j, k) = (mflx1(i, jm1, k) + mflx1(i, j, k)) &
                               *vtwid1(i, j, k)*atwid1(i)
            end do
            do i = is, ie
              s2(i, j, k) = s2(i, j, k) + &
                            (-sflx1(i + 1, j, k) + sflx1(i, j, k))*dvol1ani(i)
            end do
          end do
        end do
      end if
      call writedata(sflx1, routinename//'_sflx1B', 4)
      call writedata(s2, routinename//'_s2', 4)
    end if
    if (withv3) then
      !
      !---------------------------- TRANSPORT S3 -----------------------------
      !
      !      Compute time centred area factor.
      !
      do i = is, iep1
        atwid1(i) = haf*g31a(i)*g2ah(i)*g31ah(i)
      end do
      !
      !      Construct a k-average of "v1" to be used for interpolation.
      !
      do k = ks, ke
        km1 = k - kone
        do j = js, je
          do i = is, iep1
            vel1(i, j, k) = haf*(v1(i, j, km1) + v1(i, j, k))
          end do
        end do
      end do
      call writedata(vel1, routinename//'_vel1C', 4)
      !
      !      Interpolate "v3" at the 1-interfaces.
      !
      call x1zc3d(v3, vel1, js, ks, je, ke, iords3, istps3, vtwid1)
      call writedata(vtwid1, routinename//'_vtw1C', 4)
      !
      !      Construct the 3-momentum flux at the 1-interfaces and perform
      !  3-momentum advection.  Note that the timestep "dt" is hidden in the
      !  mass flux.
      !
      if (moving_grid1 .or. allow_moving) then
        do k = ks, ke
          km1 = k - kone
          do j = js, je
            do i = is, iep1
              sflx1(i, j, k) = (mflx1(i, j, km1) + mflx1(i, j, k)) &
                               *vtwid1(i, j, k)*g32b(j)*atwid1(i)
            end do
            do i = is, ie
              s3(i, j, k) = (s3(i, j, k)*dvol1a(i) &
                             - sflx1(i + 1, j, k) + sflx1(i, j, k))*dvol1ani(i)
            end do
          end do
        end do
      else
        do k = ks, ke
          km1 = k - kone
          do j = js, je
            do i = is, iep1
              sflx1(i, j, k) = (mflx1(i, j, km1) + mflx1(i, j, k)) &
                               *vtwid1(i, j, k)*g32b(j)*atwid1(i)
            end do
            do i = is, ie
              s3(i, j, k) = s3(i, j, k) + &
                            (-sflx1(i + 1, j, k) + sflx1(i, j, k))*dvol1ani(i)
            end do
          end do
        end do
      end if
      call writedata(sflx1, routinename//'_sflx1C', 4)
      call writedata(s3, routinename//'_s3', 4)
    end if
    !----------------------------
    call release_memory(vtwid1)
    call release_memory(allocated_space)
    nullify (vel1)
    nullify (sflx1)
  END SUBROUTINE momx1
  !
  !=======================================================================
  !
  SUBROUTINE momx2(mflx2, s1, s2, s3)
    !
    !    dac:zeus3d.momx2 <--------------- transports momenta in 2-direction
    !    from jms:zeus2d.momx2, mln:zeus04.momr                    may, 1990
    !
    !    written by: David Clarke
    !    modified 1: November, 1992 by David Clarke; momenta are now updated
    !                between and including i=is,ie, j=js,je, and k=ks,ke to
    !                allow for proper treatment of periodic boundaries.
    !
    !  PURPOSE:  Transports the three components of the momentum density in
    !  the 2-direction using the consistent transport algorithm, including
    !  the effects of grid compression.  The transported fluxes are thus
    !  given by the mass fluxes times the time centred area of the control
    !  volume faces times the interpolated velocities.  Interpolations are
    !  performed in X2FC3D and X2ZC3D.
    !
    !  INPUT VARIABLES:
    !    mflx2   mass flux in 2-direction
    !    s1      momentum density in 1-direction
    !    s2      momentum density in 2-direction
    !    s3      momentum density in 3-direction
    !
    !  OUTPUT VARIABLES:
    !    s1      momentum density in 1-direction updated in the 2-direction
    !    s2      momentum density in 2-direction updated in the 2-direction
    !    s3      momentum density in 3-direction updated in the 2-direction
    !
    !  LOCAL VARIABLES:
    !    vel2    velocity used for upwinding in interpolation routine
    !    vtwid2  interpolated velocity
    !    sflx2   momentum fluxes
    !
    !-----------------------------------------------------------------------
    !
    REALNUM, INTENT(IN) :: mflx2(:, :, :)
    REALNUM, DIMENSION(:, :, :), INTENT(INOUT) :: s1, s2, s3
    !
    CHARACTER(LEN=*), PARAMETER :: routinename = 'z_momx2'
    !
    INTEGER i, j, k, im1, km1
    !
    REALNUM, DIMENSION(isa:iea) :: atwid1, dvol1, dvol1ni
    REALNUM, DIMENSION(jsa:jea) :: atwid2, dvol2a, dvol2ani, dvol2b, dvol2bni
    !
    REALNUM, POINTER, SAVE :: vtwid2(:, :, :) => null(), &
                              sflx2(:, :, :) => null(), &
                              vel2(:, :, :) => null()
    REALNUM, POINTER, SAVE :: allocated_space(:, :, :) => null()
    !
    !-----------------------------------------------------------------------
    !      Check for x2 symmetry and presence of v2
    if (jnac == 0 .or. .not. withv2) return
    !-----------------------------------------------------------------------
    !
    !      Allocate local arrays
    !
    call get_memory(allocated_space)
    vel2 => allocated_space
    sflx2 => allocated_space
    call get_memory(vtwid2)
    !
    !      Set volumes.
    !
    do i = is, ie
      dvol1(i) = one
      dvol1ni(i) = one
    end do
    do j = js, je
      dvol2a(j) = dvl2a(j)
      dvol2ani(j) = dvl2ani(j)
      dvol2b(j) = dvl2b(j)
      dvol2bni(j) = dvl2bni(j)
    end do
    !
    !---------------------------- TRANSPORT S1 -----------------------------
    !
    !      Compute time-centred area factor.
    !
    do i = is, ie
      atwid1(i) = haf*g31a(i)*dx1b(i)*dvl1bi(i)
    end do
    !
    !      Construct an i-average of "v2" to be used for interpolation.
    !
    do k = ks, ke
      do j = js, jep1
        do i = is, ie
          im1 = i - ione
          vel2(i, j, k) = haf*(v2(im1, j, k) + v2(i, j, k))
        end do
      end do
    end do
    call writedata(vel2, routinename//'_vel2A', 4)
    !
    !      Interpolate "v1" at the 2-interfaces.
    !
    call x2zc3d(v1, vel2, ks, is, ke, ie, iords1, istps1, g2a, g2ai, vtwid2)
    call writedata(vtwid2, routinename//'_vtw2A', 4)
    !
    !      Construct the 1-momentum flux at the 2-interfaces and perform
    !  1-momentum advection.  Note that the timestep "dt" is hidden in the
    !  mass flux.
    !
    if (moving_grid2 .or. allow_moving) then
      do k = ks, ke
        do j = js, jep1
          do i = is, ie
            im1 = i - ione
            sflx2(i, j, k) = (mflx2(im1, j, k) + mflx2(i, j, k)) &
                             *vtwid2(i, j, k)*g32ah(j)*atwid1(i)
          end do
        end do
        do j = js, je
          do i = is, ie
            s1(i, j, k) = (s1(i, j, k)*dvl2a(j) &
                           - sflx2(i, j + 1, k) + sflx2(i, j, k))*dvl2ani(j)
          end do
        end do
      end do
    else
      do k = ks, ke
        do j = js, jep1
          do i = is, ie
            im1 = i - ione
            sflx2(i, j, k) = (mflx2(im1, j, k) + mflx2(i, j, k)) &
                             *vtwid2(i, j, k)*g32ah(j)*atwid1(i)
          end do
        end do
        do j = js, je
          do i = is, ie
            s1(i, j, k) = s1(i, j, k) + &
                          (-sflx2(i, j + 1, k) + sflx2(i, j, k))*dvl2ani(j)
            !     ( - sflx2(i,j+1,k) + sflx2(i,j,k) ) * dvl2ai(j)
          end do
        end do
      end do
    end if
    call writedata(sflx2, routinename//'_sflx2A', 4)
    call writedata(s1, routinename//'_s1', 4)
    if (withv2) then
      !
      !---------------------------- TRANSPORT S2 -----------------------------
      !
      !      Compute time-centred area factor.
      !
      do i = is, ie
        atwid1(i) = haf*g2b(i)*g31b(i)*dx1a(i)*dvl1ai(i)
      end do
      !
      !      Construct a j-average of "v2-vg2" to be used for interpolation.
      !
      do k = ks, ke
        do j = jsm1, je
          do i = is, ie
            vel2(i, j, k) = haf*(v2(i, j, k) - vg2(j) &
                                 + v2(i, j + 1, k) - vg2(j + 1))
          end do
        end do
      end do
      call writedata(vel2, routinename//'_vel2B', 4)
      !
      !      Interpolate "v2" to the zone centres.
      !
      call x2fc3d(v2, vel2, ks, is, ke, ie, iords2, vtwid2)
      call writedata(vtwid2, routinename//'_vtw2B', 4)
      !
      !      Construct the 2-momentum flux at the zone centres and perform
      !  2-momentum advection.  Note that the timestep "dt" is hidden in the
      !  mass flux.
      !
      if (moving_grid1 .or. moving_grid2 .or. allow_moving) then
        do k = ks, ke
          do j = jsm1, je
            do i = is, ie
              sflx2(i, j, k) = (mflx2(i, j, k) + mflx2(i, j + 1, k)) &
                               *vtwid2(i, j, k)*abs(g32bh(j))*atwid1(i)
              !JH  taking abs value of g32b (or g32bh) to account for
              !JH  coordinate reflection in RTP geometry
              !RFK Not checked how correct and general this is or is not.
              !RFK Also not checked if this makes trouble or advantage or
              !RFK no difference in ZRP geometry.
            end do
          end do
          do j = js, je
            do i = is, ie
              s2(i, j, k) = (s2(i, j, k)*dvol1(i)*dvol2b(j) &
                             - sflx2(i, j, k) + sflx2(i, j - 1, k)) &
                            *dvol1ni(i)*dvol2bni(j)
              !s2(i,j,k) = s2(i,j,k) + &
              !     ( - sflx2(i,j,k) + sflx2(i,j-1,k) ) &
              !     * dvol1ni(i) * dvol2bni(j)
            end do
          end do
        end do
      else
        do k = ks, ke
          do j = jsm1, je
            do i = is, ie
              sflx2(i, j, k) = (mflx2(i, j, k) + mflx2(i, j + 1, k)) &
                               *vtwid2(i, j, k)*abs(g32bh(j))*atwid1(i)
              !JH  taking abs value of g32b (or g32bh) to account for
              !JH  coordinate reflection in RTP geometry
              !RFK Not checked how correct and general this is or is not.
              !RFK Also not checked if this makes trouble or advantage or
              !RFK no difference in ZRP geometry.
            end do
          end do
          do j = js, je
            do i = is, ie
              s2(i, j, k) = s2(i, j, k) + &
                            (-sflx2(i, j, k) + sflx2(i, j - 1, k)) &
                            *dvol1ni(i)*dvol2bni(j)
            end do
          end do
        end do
      end if
      call writedata(sflx2, routinename//'_sflx2B', 4)
      call writedata(s2, routinename//'_s2', 4)
    end if
    if (withv3) then
      !
      !---------------------------- TRANSPORT S3 -----------------------------
      !
      !      Compute time-centred area factors.
      !
      do i = is, ie
        atwid1(i) = haf*g31b(i)*g31b(i)*dx1a(i)*dvl1ai(i)
      end do
      do j = js, jep1
        atwid2(j) = g32a(j)*g32ah(j)
      end do
      !
      !      Construct a k-average of "v2" to be used for interpolation.
      !
      do k = ks, ke
        km1 = k - kone
        do j = js, jep1
          do i = is, ie
            vel2(i, j, k) = haf*(v2(i, j, km1) + v2(i, j, k))
          end do
        end do
      end do
      call writedata(vel2, routinename//'_vel2C', 4)
      !
      !      Interpolate "v3" at the 2-interfaces.
      !
      call x2zc3d(v3, vel2, ks, is, ke, ie, iords3, istps3, g2b, g2bi, vtwid2)
      call writedata(vtwid2, routinename//'_vtw2C', 4)
      !
      !      Construct the 3-momentum flux at the 2-interfaces and perform
      !  3-momentum advection.  Note that the timestep "dt" is hidden in the
      !  mass flux.
      !
      if (moving_grid1 .or. moving_grid2 .or. allow_moving) then
        do k = ks, ke
          km1 = k - kone
          do j = js, jep1
            do i = is, ie
              sflx2(i, j, k) = (mflx2(i, j, km1) + mflx2(i, j, k)) &
                               *vtwid2(i, j, k)*atwid1(i)*atwid2(j)
            end do
          end do
          !  RFK: The following lines have no clear function, and have been
          !       commented out
          !        do i=is,ie
          !          sflx2(i,j,k) = ( mflx2(i,j,km1) + mflx2(i,j,k) )
          !        end do
          do j = js, je
            do i = is, ie
              s3(i, j, k) = (s3(i, j, k)*dvol1(i)*dvol2a(j) &
                             - sflx2(i, j + 1, k) + sflx2(i, j, k)) &
                            *dvol1ni(i)*dvol2ani(j)
              !s3(i,j,k) =  s3(i,j,k) + &
              !     ( - sflx2(i,j+1,k) + sflx2(i,j,k) ) &
              !     * dvol1ni(i) * dvol2ani(j)
            end do
          end do
        end do
      else
        do k = ks, ke
          km1 = k - kone
          do j = js, jep1
            do i = is, ie
              sflx2(i, j, k) = (mflx2(i, j, km1) + mflx2(i, j, k)) &
                               *vtwid2(i, j, k)*atwid1(i)*atwid2(j)
            end do
          end do
          !  RFK: The following lines have no clear function, and have been
          !       commented out
          !        do i=is,ie
          !          sflx2(i,j,k) = ( mflx2(i,j,km1) + mflx2(i,j,k) )
          !        end do
          do j = js, je
            do i = is, ie
              s3(i, j, k) = s3(i, j, k) + &
                            (-sflx2(i, j + 1, k) + sflx2(i, j, k)) &
                            *dvol1ni(i)*dvol2ani(j)
            end do
          end do
        end do
      end if
      call writedata(sflx2, routinename//'_sflx2C', 4)
      call writedata(s3, routinename//'_s3', 4)
    end if

    !-----------------------------------
    call release_memory(vtwid2)
    call release_memory(allocated_space)
    nullify (vel2)
    nullify (sflx2)
  END SUBROUTINE momx2
  !
  !=======================================================================
  !
  SUBROUTINE momx3(mflx3, s1, s2, s3)
    !
    !    dac:zeus3d.momx3 <--------------- transports momenta in 3-direction
    !                                                         february, 1990
    !
    !    written by: David Clarke
    !    modified 1: November, 1992 by David Clarke; momenta are now updated
    !                between and including i=is,ie, j=js,je, and k=ks,ke to
    !                allow for proper treatment of periodic boundaries.
    !
    !  PURPOSE:  Transports the three components of the momentum density in
    !  the 3-direction using the consistent transport algorithm, including
    !  the effects of grid compression.  The transported fluxes are thus
    !  given by the mass fluxes times the time centred area of the control
    !  volume faces times the interpolated velocities.  Interpolations are
    !  performed in X3FC3D and X3ZC3D.
    !
    !  INPUT VARIABLES:
    !    mflx3   mass flux in 3-direction
    !    s1      momentum density in 1-direction
    !    s2      momentum density in 2-direction
    !    s3      momentum density in 3-direction
    !
    !  OUTPUT VARIABLES:
    !    s1      momentum density in 1-direction updated in the 3-direction
    !    s2      momentum density in 2-direction updated in the 3-direction
    !    s3      momentum density in 3-direction updated in the 3-direction
    !
    !  LOCAL VARIABLES:
    !    vel3    velocity used for upwinding in interpolation routine
    !    vtwid3  interpolated velocity
    !    sflx3   momentum fluxes
    !
    !-----------------------------------------------------------------------
    !
    REALNUM, INTENT(IN) :: mflx3(:, :, :)
    REALNUM, DIMENSION(:, :, :), INTENT(INOUT) :: s1, s2, s3
    !
    CHARACTER(LEN=*), PARAMETER :: routinename = 'z_momx3'
    !
    INTEGER i, j, k, im1, jm1
    !
    REALNUM atwid1(isa:iea), atwid2(jsa:jea)
    !
    REALNUM, POINTER, SAVE :: vtwid3(:, :, :) => null(), &
                              sflx3(:, :, :) => null(), &
                              vel3(:, :, :) => null()
    REALNUM, POINTER, SAVE :: allocated_space(:, :, :) => null()
    !
    !-----------------------------------------------------------------------
    !      Check for x3 symmetry and presence of v3
    if (knac == 0 .or. .not. withv3) return
    !-----------------------------------------------------------------------
    !
    !      Allocate local arrays
    !
    call get_memory(allocated_space)
    vel3 => allocated_space
    sflx3 => allocated_space
    call get_memory(vtwid3)
    !
    !------------------------------ TRANSPORT S1 ---------------------------
    !
    !      Compute time-centred area factors.
    !
    do i = is, ie
      atwid1(i) = haf*g2a(i)*dx1b(i)*dvl1bi(i)
    end do
    do j = js, je
      atwid2(j) = dx2a(j)*dvl2ai(j)
    end do
    !
    !      Construct an i-average of "v3" to be used for interpolation.
    !
    do k = ks, kep1
      do j = js, je
        do i = is, ie
          im1 = i - ione
          vel3(i, j, k) = haf*(v3(im1, j, k) + v3(i, j, k))
        end do
      end do
    end do
    call writedata(vel3, routinename//'_vel3A', 4)
    !
    !      Interpolate "v1" at the 3-interfaces.
    !
    call x3zc3d(v1, vel3, is, js, ie, je, iords1, istps1, g31a, g31ai, g32b, &
                g32bi, vtwid3)
    call writedata(vtwid3, routinename//'_vtw3A', 4)
    !
    !      Construct the 1-momentum flux at the 3-interfaces and perform
    !  1-momentum advection.  Note that the timestep "dt" is hidden in the
    !  mass flux.
    !
    do k = ks, kep1
      do j = js, je
        do i = is, ie
          im1 = i - ione
          sflx3(i, j, k) = (mflx3(im1, j, k) + mflx3(i, j, k)) &
                           *vtwid3(i, j, k)*atwid1(i)*atwid2(j)
        end do
      end do
    end do
    if (moving_grid3 .or. allow_moving) then
      do k = ks, ke
        do j = js, je
          do i = is, ie
            s1(i, j, k) = (s1(i, j, k)*dvl3a(k) &
                           - sflx3(i, j, k + 1) + sflx3(i, j, k))*dvl3ani(k)
          end do
        end do
      end do
    else
      do k = ks, ke
        do j = js, je
          do i = is, ie
            s1(i, j, k) = s1(i, j, k) + &
                          (-sflx3(i, j, k + 1) + sflx3(i, j, k))*dvl3ani(k)
          end do
        end do
      end do
    end if
    call writedata(sflx3, routinename//'_sflx3A', 4)
    call writedata(s1, routinename//'_s1', 4)
    if (withv2) then
      !
      !------------------------------ TRANSPORT S2 ---------------------------
      !
      !      Compute time-centred area factors.
      !
      do i = is, ie
        atwid1(i) = haf*g2b(i)*g2b(i)*dx1a(i)*dvl1ai(i)
      end do
      do j = js, je
        atwid2(j) = dx2b(j)*dvl2bi(j)
      end do
      !
      !      Construct a j-average of "v3" to be used for interpolation.
      !
      do k = ks, kep1
        do j = js, je
          jm1 = j - jone
          do i = is, ie
            vel3(i, j, k) = haf*(v3(i, jm1, k) + v3(i, j, k))
          end do
        end do
      end do
      call writedata(vel3, routinename//'_vel3B', 4)
      !
      !      Interpolate "v2" at the 3-interfaces.
      !
      call x3zc3d(v2, vel3, is, js, ie, je, iords2, istps2, g31b, g31bi, g32a, &
                  g32ai, vtwid3)
      call writedata(vtwid3, routinename//'_vtw3B', 4)
      !
      !      Construct the 2-momentum flux at the 3-interfaces and perform
      !  2-momentum advection.  Note that the timestep "dt" is hidden in the
      !  mass flux.
      !
      do k = ks, kep1
        do j = js, je
          jm1 = j - jone
          do i = is, ie
            sflx3(i, j, k) = (mflx3(i, jm1, k) + mflx3(i, j, k)) &
                             *vtwid3(i, j, k)*atwid1(i)*atwid2(j)
          end do
        end do
      end do
      if (moving_grid3 .or. allow_moving) then
        do k = ks, ke
          do j = js, je
            do i = is, ie
              s2(i, j, k) = (s2(i, j, k)*dvl3a(k) &
                             - sflx3(i, j, k + 1) + sflx3(i, j, k))*dvl3ani(k)
            end do
          end do
        end do
      else
        do k = ks, ke
          do j = js, je
            do i = is, ie
              s2(i, j, k) = s2(i, j, k) + &
                            (-sflx3(i, j, k + 1) + sflx3(i, j, k))*dvl3ani(k)
            end do
          end do
        end do
      end if
      call writedata(sflx3, routinename//'_sflx3B', 4)
      call writedata(s2, routinename//'_s2', 4)
    end if
    if (withv3) then
      !
      !------------------------------ TRANSPORT S3 ---------------------------
      !
      !      Compute time-centred area factors.
      !
      do i = is, ie
        atwid1(i) = haf*g31b(i)*g2b(i)*dx1a(i)*dvl1ai(i)
      end do
      do j = js, je
        atwid2(j) = g32b(j)*dx2a(j)*dvl2ai(j)
      end do
      !
      !      Construct a k-average of "v3-vg3" to be used for interpolation.
      !
      ! RFK  do k=ks,ke
      do k = ksm1, ke
        do j = js, je
          do i = is, ie
            vel3(i, j, k) = haf*(v3(i, j, k) - vg3(k) &
                                 + v3(i, j, k + 1) - vg3(k + 1))
          end do
        end do
      end do
      call writedata(vel3, routinename//'_vel3C', 4)
      !
      !      Interpolate "v3" at the zone centres.
      !
      call x3fc3d(v3, vel3, is, js, ie, je, iords3, vtwid3)
      call writedata(vtwid3, routinename//'_vtw3C', 4)
      !
      !      Construct the 3-momentum flux at the 3-interfaces and perform
      !  3-momentum advection.  Note that the timestep "dt" is hidden in the
      !  mass flux.
      !
      ! RFK  do k=ks,ke
      do k = ksm1, ke
        do j = js, je
          do i = is, ie
            sflx3(i, j, k) = (mflx3(i, j, k) + mflx3(i, j, k + 1)) &
                             *vtwid3(i, j, k)*atwid1(i)*atwid2(j)
          end do
        end do
      end do
      if (moving_grid3 .or. allow_moving) then
        do k = ks, ke
          do j = js, je
            do i = is, ie
              s3(i, j, k) = (s3(i, j, k)*dvl3b(k) &
                             - sflx3(i, j, k) + sflx3(i, j, k - 1))*dvl3bni(k)
            end do
          end do
        end do
      else
        do k = ks, ke
          do j = js, je
            do i = is, ie
              s3(i, j, k) = s3(i, j, k) + &
                            (-sflx3(i, j, k) + sflx3(i, j, k - 1))*dvl3bni(k)
            end do
          end do
        end do
      end if
      call writedata(sflx3, routinename//'_sflx3C', 4)
      call writedata(s3, routinename//'_s3', 4)
    end if
    !---------------------------------------
    call release_memory(vtwid3)
    call release_memory(allocated_space)
    nullify (vel3)
    nullify (sflx3)
  END SUBROUTINE momx3

  !** SUBGROUP: SOURCE TERMS *********************************************
  !*                                                                     *
  !*          S U B G R O U P : S O U R C E   T E R M S                  *
  !*                                                                     *
  !***********************************************************************
  !
  !=======================================================================
  !
  SUBROUTINE srcstep
    !
    !    jms:zeus3d.srcstep <------------------------ source step controller
    !                                                          october, 1987
    !
    !    written by: Jim Stone
    !    modified 1: June, 1988 by Jim Stone; incorporated into ZEUS2D
    !    modified 2: Spring, 1989 by Jim Stone; rewritten
    !    modified 3: February, 1990 by David Clarke; incorporated into
    !                ZEUS3D
    !    modified 4: July, 1990 by David Clarke; because current densities
    !                are not needed to compute source terms in the new CT
    !                algorithm (MOCCT), workers can be used to store the
    !                momenta, thereby saving redundant computations in STV1
    !                and STV2.
    !    modified 5: June 1992, by David Clarke; added the total energy
    !                option originally designed by Byung-IL Jun.
    !
    !
    !  PURPOSE: Controls the update of velocities (v1, v2, v3) and internal
    !  energy (e) from source terms in the equation of motion and energy
    !  equations respectively.
    !
    !  LOCAL VARIABLES:
    !    p        pressure
    !    s2       2-momentum
    !    s3       3-momentum
    !    j1       1-current density
    !    j2       2-current density
    !    j3       3-current density
    !    rin_cd   radially inward column density
    !    rout_cd  radially outward column density
    !    top_cd   theta above column density
    !    bottom_cd theta below column density
    !
    !-----------------------------------------------------------------------
    !
    CHARACTER(LEN=*), PARAMETER :: routinename = 'z_srcstep'
    !
    REALNUM, POINTER, SAVE :: p(:, :, :) => null(), s2(:, :, :) => null(), &
                              s3(:, :, :) => null()
    REALNUM, POINTER, SAVE :: j1(:, :, :) => null(), j2(:, :, :) => null(), &
                              j3(:, :, :) => null()
    REALNUM, POINTER, SAVE :: rin_cd(:, :, :) => null(), rout_cd(:, :, :) => null(), &
                              top_cd(:, :, :) => null(), bottom_cd(:, :, :) => null()
    !
    !-----------------------------------------------------------------------
    !
    call writemessage(routinename//'_entry', 2)
    !
    !      Construct momentum densities and currents if needed.
    !
    if ((geometry == 'zrp' .or. geometry == 'rtp' .or. geometry == 'rmp') &
        .and. withv3) then
      call get_memory(s3)
      call v3tos3(s3)
      call writedata(s3, routinename//'_s3', 4)
    end if
    if ((geometry == 'rtp' .or. geometry == 'rmp') .and. withv2) then
      call get_memory(s2)
      call v2tos2(s2)
      call writedata(s2, routinename//'_s2', 4)
    end if
    if (imhd == 1 .and. mhddimension == 3) then
      call get_memory(j1)
      call get_memory(j2)
      call get_memory(j3)
      call currents(j1, j2, j3, b1, b2, b3)
      call writedata(j1, routinename//'_j1', 3)
      call writedata(j2, routinename//'_j2', 3)
      call writedata(j3, routinename//'_j3', 3)
    end if
    !
    !      Perform an explicit update for all velocity variables.
    !      The boundary updates are necessary for the following artificial
    !      viscosity step, which uses one line of ghost data of the
    !      velocity fields.
    !
    call get_memory(p)
    call pressure(p, 1)
    call writemessage(routinename//'_pressure', 2)
    call writedata(p, routinename//'_p', 3)

    call stv1(p, s2, s3, j2, j3)
    call writedata(v1, routinename//'_v1A', 3)
    call boundary('v1')
    call writedata(v1, routinename//'_v1B', 3)

    if (withv2) then
      call stv2(p, s3, j3, j1)
      call writedata(v2, routinename//'_v2A', 3)
      call boundary('v2')
      call writedata(v2, routinename//'_v2B', 3)
    end if

    if (withv3) then
      call stv3(p, j1, j2)
      call writedata(v3, routinename//'_v3A', 3)
      call boundary('v3')
      call writedata(v3, routinename//'_v3B', 3)
    end if

    if (withchem) then
      call get_memory(rin_cd)
      call get_memory(rout_cd)
      call get_memory(top_cd)
      call get_memory(bottom_cd)
      call col_dens(rin_cd, rout_cd, top_cd, bottom_cd)
      call writemessage(routinename//'_col_dens', 2)
      call xrays(rin_cd, top_cd, bottom_cd, xr_array)
      call writemessage(routinename//'_xrays', 2)
      call cosmicrays(top_cd, bottom_cd, cr_array)
      call writemessage(routinename//'_cosmicrays', 2)
      call chemistry(p, ad_eta_array, ohmic_eta_array, hall_eta_array)!, xhco_array)
    end if

    call release_memory(p)
    if (imhd == 1) then
      call release_memory(j3)
      call release_memory(j2)
      call release_memory(j1)
    end if
    if ((geometry == 'rtp' .or. geometry == 'rmp') .and. withv2) then
      call release_memory(s2)
    end if
    if ((geometry == 'zrp' .or. geometry == 'rtp' .or. geometry == 'rmp') &
        .and. withv3) then
      call release_memory(s3)
    end if

    if (withchem) then
      call release_memory(rin_cd)
      call release_memory(rout_cd)
      call release_memory(top_cd)
      call release_memory(bottom_cd)
    end if
    !
  END SUBROUTINE srcstep
  !
  !=======================================================================
  !
  SUBROUTINE stv1(p, s2, s3, j2, j3)
    !
    !    jms:zeus3d.stv1 <------------------------------ source terms for V1
    !    from dac:zeus04.source; mln:a2.acc                    october, 1987
    !
    !    written by: Jim Stone
    !    modified 1: June, 1988 by Jim Stone; incorporated into ZEUS2D
    !    modified 2: February, 1990 by David Clarke; incorporated into
    !                ZEUS3D
    !    modified 3: June, 1990 by David Clarke; moved the transverse
    !                Lorentz forces (those responsible for Alfven waves)
    !                to the new CT scheme (MOCCT), moved the magnetic fields
    !                to the face-centres (cospatial with the velocities),
    !                and eliminated the need for passing the current
    !                densities.  Instead, components of the momenta are
    !                passed for computing the rotational pseudo-forces.
    !    modified 4: November, 1992 by David Clarke; v1 is now accelerated
    !                at i=is to allow for a proper treatment of periodic
    !                boundaries.
    !
    !  PURPOSE: Calculates the source terms in the equation of motion for
    !  "v1".  Currently, the source terms are:
    !
    !      - (GRAD(p))/rho      -- pressure gradient
    !      + (GRAD(gp))         -- gravitational potential gradient
    !      - G*M/R**2           -- gravitational force from a point mass
    !      + (SROT*VROT)/R/rho  -- rotation pseudo-force
    !      + (GRAD(B_t**2))     -- longitudinal Lorentz forces
    !
    !  Source terms from the artificial viscosity are computed in
    !  ATRIFICIALVISC.
    !
    !  INPUT VARIABLES:
    !    p         pressure
    !    s2        2-momentum density
    !    s3        3-momentum density
    !    j2        2-current density
    !    j3        3-current density
    !
    !  OUTPUT VARIABLES:
    !
    !  LOCAL VARIABLES:
    !    st1       source terms for v1 during i-sweep
    !    rhoi      inverse density at interface in 1-direction
    !    r2i       square of inverse distance from gravitating point mass
    !    d1b2sq    1-difference of b2**2 over entire grid
    !    d1b3sq    1-difference of b3**2 over entire grid
    !
    !-----------------------------------------------------------------------
    !
    REALNUM, DIMENSION(:, :, :), INTENT(IN) :: p, s2, s3, j2, j3
    !
    CHARACTER(LEN=*), PARAMETER :: routinename = 'z_stv1'
    !
    INTEGER i, j, k
    INTEGER im1, jp1, kp1
    !
    REALNUM st1, rhoi, r2, r2i
    REALNUM rhoi_f
    REALNUM pzpq, prpq, pzpt, prpt
    REALNUM, POINTER, SAVE :: d1b2sq(:, :, :) => null(), &
                              d1b3sq(:, :, :) => null()
    !
    !-----------------------------------------------------------------------
    if (imhd == 3 .or. imhd == 2 .or. (imhd /= 0 .and. mhddimension == 2)) then
      if (inac /= 0) then
        !
        !       Compute the 1-differences in the squares of the transverse
        !  magnetic field components for the longitudinal Lorentz forces.
        !
        call writemessage(routinename//'_to_getmemory_d1b3sq', 4)
        call get_memory(d1b3sq)
        call writemessage(routinename//'_getmemory_d1b3sq', 4)
        if (withbp) then
          call get_memory(d1b2sq)
          do k = ks, kep1
            do j = js, jep1
              do i = is, ie
                im1 = i - ione
                d1b2sq(i, j, k) = ((g2b(i)*b2(i, j, k))**2 &
                                   - (g2b(im1)*b2(im1, j, k))**2) &
                                  *g2ai(i)*g2ai(i)
                d1b3sq(i, j, k) = ((g31b(i)*b3(i, j, k))**2 &
                                   - (g31b(im1)*b3(im1, j, k))**2) &
                                  *g31ai(i)*g31ai(i)
              end do
            end do
          end do
        else
          k = ks
          do j = js, jep1
            do i = is, ie
              im1 = i - ione
              d1b3sq(i, j, k) = ((g31b(i)*b3(i, j, k))**2 &
                                 - (g31b(im1)*b3(im1, j, k))**2) &
                                *g31ai(i)*g31ai(i)
            end do
          end do
        end if
      end if
    end if
    !
    !      Add source terms to "v1".
    !
    do k = ks, ke
      kp1 = k + kone
      do j = js, je
        jp1 = j + jone
        do i = is, ie
          im1 = i - ione
          if (inac == 0) then
            rhoi = one/d_(i, j, k)
            rhoi_f = one/dfl(i, j, k)
            st1 = zro
          else
            rhoi = two/(d_(im1, j, k) + d_(i, j, k))
            rhoi_f = two/(dfl(im1, j, k) + dfl(i, j, k))
            !
            !  1.  pressure gradient
            !
            st1 = -rhoi &
                  *(p(i, j, k) - p(im1, j, k))*dx1bi(i)
            !
            !  2.  gravitational potential gradient
            !
            if (withgp) then
              st1 = st1 &
                    + (gp(i, j, k) - gp(im1, j, k))*dx1bi(i)
            end if
          end if
          if (geometry == 'rtp' .or. geometry == 'rmp') then
            !
            !  3.  rotational pseudo-forces
            !
            if (withv2) then
              st1 = st1 &
                    + (s2(i, j, k) + s2(i, jp1, k) &
                       + s2(im1, j, k) + s2(im1, jp1, k)) &
                    *(v2(i, j, k) + v2(i, jp1, k) &
                      + v2(im1, j, k) + v2(im1, jp1, k))/sixteen &
                    *rhoi*dg2ad1(i)*g2ai(i)*g2ai(i)
            end if
            if (withv3) then
              st1 = st1 &
                    + (s3(i, j, k) + s3(i, j, kp1) &
                       + s3(im1, j, k) + s3(im1, j, kp1)) &
                    *(v3(i, j, k) + v3(i, j, kp1) &
                      + v3(im1, j, k) + v3(im1, j, kp1))/sixteen &
                    *rhoi &
                    *dg31ad1(i)*g31ai(i)*g31ai(i)*g32bi(j)
            end if
          end if
          if (imhd == 3 .or. imhd == 2 &
              .or. (imhd /= 0 .and. mhddimension == 2)) then
            !
            !  4.  longitudinal Lorentz force
            !
            if (inac /= 0) then
              if (withbp) then
                st1 = st1 &
                      - (d1b2sq(i, j, k) + d1b2sq(i, jp1, k) &
                         + d1b3sq(i, j, k) + d1b3sq(i, j, kp1)) &
                      *rhoi_f*dx1bi(i)*quarter
              else
                st1 = st1 &
                      - (d1b3sq(i, j, k)) &
                      *rhoi_f*dx1bi(i)*haf
              end if
            end if
          end if
          !
          !  4.  Lorentz force
          !
          if (imhd == 1) then
            st1 = st1 &
                  + ((j2(i, j, k) + j2(i, j, kp1)) &
                     *(b3(i, j, k) + b3(i, j, kp1) &
                       + b3(im1, j, k) + b3(im1, j, kp1)) &
                     - (j3(i, j, k) + j3(i, jp1, k)) &
                     *(b2(i, j, k) + b2(i, jp1, k) &
                       + b2(im1, j, k) + b2(im1, jp1, k)))/eight &
                  *rhoi_f
          end if
          !
          v1(i, j, k) = v1(i, j, k) + dt*st1
        end do
      end do
    end do

    if (imhd == 3 .or. imhd == 2) then
      if (inac /= 0) then
        call writemessage(routinename//'_to_releasememory_d1b3sq', 4)
        call release_memory(d1b3sq)
        call writemessage(routinename//'_releasememory_d1b3sq', 4)
        if (withbp) call release_memory(d1b2sq)
      end if
    end if

    !
    !  5.  point mass gravitational potential gradient.
    !
    if (gptmass == zro) then
      ! All done
    else
      do k = ks, ke
        do j = js, je
          do i = is, ie
            if (geometry == 'xyz') then
              r2 = (x1a(i) - x1ptm)**2 &
                   + (x2b(j) - x2ptm)**2 &
                   + (x3b(k) - x3ptm)**2
              r2i = (x1a(i) - x1ptm) &
                    /(r2*sqrt(r2) + verysmall)
            end if
            if (geometry == 'zrp') then
              r2 = (x1a(i) - x1ptm)**2 &
                   + (x2b(j) - x2ptm)**2 &
                   + two*x2b(j)*x2ptm &
                   *(one - cos(x3b(k) - x3ptm))
              r2i = (x1a(i) - x1ptm) &
                    /(r2*sqrt(r2) + verysmall)
            end if
            if (geometry == 'rtp' .or. geometry == 'rmp') then
              !JH: correction due to Sean Matt
              ! -- enforce condition that radial
              ! force due to point mass depend only on radius.
              if (x1ptm == zro) then
                r2i = x1ai(i)**2
              else
                prpq = x1a(i)*sin(x2b(j))
                pzpq = x1a(i)*cos(x2b(j))
                prpt = x1ptm*sin(x2ptm)
                pzpt = x1ptm*cos(x2ptm)
                r2 = (pzpq - pzpt)**2 &
                     + (prpq - prpt)**2 &
                     + two*prpq*prpt &
                     *(one - cos(x3b(k) - x3ptm))
                r2i = ((pzpq - pzpt)*cos(x2b(j)) &
                       + (prpq - prpt*cos(x3b(k) - x3ptm)) &
                       *sin(x2b(j)))/(r2*sqrt(r2) + verysmall)
              end if
            end if
            v1(i, j, k) = v1(i, j, k) - dt*gptmass*r2i
          end do
        end do
      end do
    end if
    !
  END SUBROUTINE stv1
  !
  !=======================================================================
  !
  SUBROUTINE stv2(p, s3, j3, j1)
    !
    !    jms:zeus3d.stv2 <------------------------------ source terms for V2
    !    from dac:zeus04.source; mln:a2.acc                    october, 1987
    !
    !    written by: Jim Stone
    !    modified 1: June, 1988 by Jim Stone; incorporated into ZEUS2D
    !    modified 2: February, 1990 by David Clarke; incorporated into
    !                ZEUS3D
    !    modified 3: June, 1990 by David Clarke; moved the transverse
    !                Lorentz forces (those responsible for Alfven waves)
    !                to the new CT scheme (MOCCT), moved the magnetic fields
    !                to the face-centres (cospatial with the velocities),
    !                and eliminated the need for passing the current
    !                densities.  Instead, components of the momenta are
    !                passed for computing the rotational pseudo-forces.
    !    modified 4: November, 1992 by David Clarke; v2 is now accelerated
    !                at j=js to allow for a proper treatment of periodic
    !                boundaries.
    !
    !  PURPOSE: Calculates the source terms in the equation of motion for
    !  v2.  Currently, the source terms are:
    !
    !      - (GRAD(p))/rho      -- pressure gradient
    !      + (GRAD(gp))         -- gravitational potential gradient
    !      - G*M/R**2           -- gravitational force from a point mass
    !      + (SROT*VROT)/R/rho  -- rotation pseudo-force
    !      + (GRAD(B_t**2))     -- longitudinal Lorentz forces
    !
    !  Source terms from the artificial viscosity are computed in
    !  ARTIFICIALVISC.
    !
    !  INPUT VARIABLES:
    !    p         pressure
    !    s3        3-momentum density
    !    j3        3-current density
    !    j1        1-current density
    !
    !  OUTPUT VARIABLES:
    !
    !  LOCAL VARIABLES:
    !    st2       source terms for v2 during j-sweep
    !    rhoi      inverse density at interface in 2-direction
    !    r2i       square of inverse distance from gravitating point mass
    !    d2b3sq    2-difference of b3**2 over entire grid
    !    d2b1sq    2-difference of b1**2 over entire grid
    !
    !-----------------------------------------------------------------------
    !
    REALNUM, DIMENSION(:, :, :), INTENT(IN) :: p, s3, j3, j1
    !
    CHARACTER(LEN=*), PARAMETER :: routinename = 'z_stv2'
    !
    INTEGER i, j, k
    INTEGER ip1, jm1, kp1
    REALNUM st2, rhoi, r2, r2i
    REALNUM rhoi_f
    REALNUM pzpq, prpq, pzpt, prpt
    REALNUM, POINTER, SAVE :: d2b3sq(:, :, :) => null(), &
                              d2b1sq(:, :, :) => null()

    if (.not. withv2) return
    !
    !-----------------------------------------------------------------------
    if (imhd == 3 .or. imhd == 2 .or. (imhd /= 0 .and. mhddimension == 2)) then
      !
      !       Compute the 2-differences in the squares of the transverse
      !  magnetic field components for the longitudinal Lorentz forces.
      !
      if (jnac /= 0) then
        call writemessage(routinename//'_to_getmemory_d2b3sq', 4)
        call get_memory(d2b3sq)
        call writemessage(routinename//'_getmemory_d2b3sq', 4)
        if (withbp) then
          call get_memory(d2b1sq)
          do k = ks, kep1
            do j = js, je
              jm1 = j - jone
              do i = is, iep1
                d2b3sq(i, j, k) = ((g32b(j)*b3(i, j, k))**2 &
                                   - (g32b(jm1)*b3(i, jm1, k))**2) &
                                  *g32ai(j)*g32ai(j)
                d2b1sq(i, j, k) = b1(i, j, k)*b1(i, j, k) &
                                  - b1(i, jm1, k)*b1(i, jm1, k)
              end do
            end do
          end do
        else
          k = ks
          do j = js, je
            jm1 = j - jone
            do i = is, iep1
              d2b3sq(i, j, k) = ((g32b(j)*b3(i, j, k))**2 &
                                 - (g32b(jm1)*b3(i, jm1, k))**2) &
                                *g32ai(j)*g32ai(j)
            end do
          end do
        end if
      end if
    end if
    !
    !      Add source terms to "v2".
    !
    do k = ks, ke
      kp1 = k + kone
      do j = js, je
        jm1 = j - jone
        do i = is, ie
          ip1 = i + ione
          if (jnac == 0) then
            rhoi = one/d_(i, j, k)
            rhoi_f = one/dfl(i, j, k)
            st2 = zro
          else
            rhoi = two/(d_(i, jm1, k) + d_(i, j, k))
            rhoi_f = two/(dfl(i, jm1, k) + dfl(i, j, k))
            !
            !  1.  pressure gradient
            !
            st2 = -rhoi &
                  *(p(i, j, k) - p(i, jm1, k))*dx2bi(j) &
                  *g2bi(i)
            !
            !  2.  gravitational potential gradient
            !
            if (withgp) then
              st2 = st2 &
                    + (gp(i, j, k) - gp(i, jm1, k))*dx2bi(j) &
                    *g2bi(i)
            end if
          end if
          !
          !  3.  rotational pseudo-force
          !
          if (geometry == 'zrp' .or. geometry == 'rtp' &
              .or. geometry == 'rmp') then
            if (withv3) then
              st2 = st2 &
                    + (s3(i, j, k) + s3(i, j, kp1) &
                       + s3(i, jm1, k) + s3(i, jm1, kp1)) &
                    *(v3(i, j, k) + v3(i, j, kp1) &
                      + v3(i, jm1, k) + v3(i, jm1, kp1))/sixteen &
                    *rhoi*g2bi(i) &
                    *dg32ad2(j)*g31bi(i)*g32ai(j)*g32ai(j)
              !JH  -- changed dg32bd2 to dg32ad2 in line above
            end if
          end if
          if (imhd == 3 .or. imhd == 2 &
              .or. (imhd /= 0 .and. mhddimension == 2)) then
            !
            !  4.  longitudinal Lorentz force
            !
            if (jnac /= 0) then
              if (withbp) then
                st2 = st2 &
                      - (d2b3sq(i, j, k) + d2b3sq(i, j, kp1) &
                         + d2b1sq(i, j, k) + d2b1sq(ip1, j, k)) &
                      *rhoi_f*dx2bi(j)*g2bi(i)*quarter
              else
                st2 = st2 &
                      - d2b3sq(i, j, k) &
                      *rhoi_f*dx2bi(j)*g2bi(i)*haf
              end if
            end if
          end if
          !
          !  4.  Lorentz force
          !
          if (imhd == 1) then
            st2 = st2 &
                  + ((j3(i, j, k) + j3(ip1, j, k)) &
                     *(b1(i, j, k) + b1(ip1, j, k) &
                       + b1(i, jm1, k) + b1(ip1, jm1, k)) &
                     - (j1(i, j, k) + j1(i, j, kp1)) &
                     *(b3(i, j, k) + b3(i, j, kp1) &
                       + b3(i, jm1, k) + b3(i, jm1, kp1)))/eight &
                  *rhoi_f
          end if
          !
          v2(i, j, k) = v2(i, j, k) + dt*st2
        end do
      end do
    end do

    if (imhd == 3 .or. imhd == 2) then
      if (jnac /= 0) then
        if (withbp) call release_memory(d2b1sq)
        call writemessage(routinename//'_to_releasememory_d2b3sq', 4)
        call release_memory(d2b3sq)
        call writemessage(routinename//'_releasememory_d2b3sq', 4)
      end if
    end if

    !
    !  5.  point mass gravitational potential gradient.
    !
    if (gptmass == zro) then
      ! All done
    else
      do k = ks, ke
        do j = js, je
          do i = is, ie
            if (geometry == 'xyz') then
              r2 = (x1b(i) - x1ptm)**2 &
                   + (x2a(j) - x2ptm)**2 &
                   + (x3b(k) - x3ptm)**2
              r2i = (x2a(j) - x2ptm) &
                    /(r2*sqrt(r2) + verysmall)
            end if
            if (geometry == 'zrp') then
              r2 = (x1b(i) - x1ptm)**2 &
                   + (x2a(j) - x2ptm)**2 &
                   + two*x2a(j)*x2ptm &
                   *(one - cos(x3b(k) - x3ptm))
              r2i = (x2a(j) - x2ptm*cos(x3b(k) - x3ptm)) &
                    /(r2*sqrt(r2) + verysmall)
            end if
            if (geometry == 'rtp' .or. geometry == 'rmp') then
              !JH  correction due to Sean Matt --
              !enforces condition that theta
              !component of force be zero if point mass located at origin.
              if (x1ptm == zro) then
                r2i = zro
              else
                prpq = x1b(i)*sin(x2a(j))
                pzpq = x1b(i)*cos(x2a(j))
                prpt = x1ptm*sin(x2ptm)
                pzpt = x1ptm*cos(x2ptm)
                r2 = (pzpq - pzpt)**2 &
                     + (prpq - prpt)**2 &
                     + two*prpq*prpt &
                     *(one - cos(x3b(k) - x3ptm))
                r2i = (-(pzpq - pzpt)*sin(x2a(j)) &
                       + (prpq - prpt*cos(x3b(k) - x3ptm)) &
                       *cos(x2a(j)))/(r2*sqrt(r2) + verysmall)
              end if
            end if
            v2(i, j, k) = v2(i, j, k) - dt*gptmass*r2i
          end do
        end do
      end do
    end if
    !
  END SUBROUTINE stv2
  !
  !=======================================================================
  !
  SUBROUTINE stv3(p, j1, j2)
    !
    !    dac:zeus3d.stv3 <------------------------------ source terms for V3
    !    from jms:zeus3d.stv3                                 february, 1990
    !
    !    written by: David Clarke
    !    modified 1: June, 1990 by David Clarke; moved the transverse
    !                Lorentz forces (those responsible for Alfven waves)
    !                to the new CT scheme (MOCCT), moved the magnetic fields
    !                to the face-centres (cospatial with the velocities),
    !                and eliminated the need for passing the current
    !                densities.
    !    modified 2: November, 1992 by David Clarke; v3 is now accelerated
    !                at k=ks to allow for a proper treatment of periodic
    !                boundaries.
    !
    !  PURPOSE: Calculates the source terms in the equation of motion for
    !  v3.  Currently, the source terms are:
    !
    !      - (GRAD(p))/rho      -- pressure gradient
    !      + (GRAD(gp))         -- gravitational potential gradient
    !      - G*M/R**2           -- gravitational force from a point mass
    !      + (GRAD(B_t**2))     -- longitudinal Lorentz forces
    !
    !  Source terms from the artificial viscosity are computed in
    !  ARTIFICIALVISC.
    !
    !  Note that there are no rotational pseudo-forces.
    !
    !  INPUT VARIABLES:
    !    p         pressure
    !    j1        1-current density
    !    j2        2-current density
    !
    !  OUTPUT VARIABLES:
    !
    !  LOCAL VARIABLES:
    !    st3       source terms for v3 during k-sweep
    !    rhoi      inverse density at interface in 3-direction
    !    r2i       square of inverse distance from gravitating point mass
    !    d3b1sq    3-difference of b1**2 over entire grid
    !    d3b2sq    3-difference of b2**2 over entire grid
    !
    !-----------------------------------------------------------------------
    !
    REALNUM, DIMENSION(:, :, :), INTENT(IN) :: p, j1, j2
    !
    CHARACTER(LEN=*), PARAMETER :: routinename = 'z_stv3'
    !
    INTEGER i, j, k
    INTEGER ip1, jp1, km1
    !
    REALNUM st3, rhoi, r2, r2i
    REALNUM rhoi_f
    REALNUM pzpq, prpq, pzpt, prpt
    REALNUM, POINTER, SAVE :: d3b1sq(:, :, :) => null(), &
                              d3b2sq(:, :, :) => null()
    !
    !-----------------------------------------------------------------------
    !
    if (.not. withv3) return
    if (imhd == 3 .or. imhd == 2) then
      if (knac /= 0) then
        call get_memory(d3b1sq)
        call get_memory(d3b2sq)
        !
        !       Compute the 3-differences in the squares of the transverse
        !  magnetic field components for the longitudinal Lorentz forces.
        !
        do k = ks, ke
          km1 = k - kone
          do j = js, jep1
            do i = is, iep1
              d3b1sq(i, j, k) = b1(i, j, k)*b1(i, j, k) &
                                - b1(i, j, km1)*b1(i, j, km1)
              d3b2sq(i, j, k) = b2(i, j, k)*b2(i, j, k) &
                                - b2(i, j, km1)*b2(i, j, km1)
            end do
          end do
        end do
      end if
    end if
    !
    !      Add source terms to v3.
    !
    do k = ks, ke
      km1 = k - kone
      do j = js, je
        jp1 = j + jone
        do i = is, ie
          ip1 = i + ione
          if (knac == 0) then
            rhoi = one/d_(i, j, k)
            rhoi_f = one/dfl(i, j, k)
            st3 = zro
          else
            rhoi = two/(d_(i, j, km1) + d_(i, j, k))
            rhoi_f = two/(dfl(i, j, km1) + dfl(i, j, k))
            !
            !  1.  pressure gradient
            !
            st3 = -rhoi &
                  *(p(i, j, k) - p(i, j, km1))*dx3bi(k) &
                  *g31bi(i)*g32bi(j)
            !
            !  2.  gravitational potential gradient
            !
            if (withgp) then
              st3 = st3 &
                    + (gp(i, j, k) - gp(i, j, km1))*dx3bi(k) &
                    *g31bi(i)*g32bi(j)
            end if
          end if
          if (imhd == 3 .or. imhd == 2) then
            !
            !  3.  longitudinal Lorentz forces
            !
            if (knac /= 0) then
              st3 = st3 &
                    - (d3b1sq(i, j, k) + d3b1sq(ip1, j, k) &
                       + d3b2sq(i, j, k) + d3b2sq(i, jp1, k)) &
                    *rhoi_f*dx3bi(k)*g31bi(i)*g32bi(j)*quarter
            end if
          end if
          !
          !  3.  Lorentz forces
          !
          if (imhd == 1) then
            st3 = st3 &
                  + ((j1(i, j, k) + j1(i, jp1, k)) &
                     *(b2(i, j, k) + b2(i, jp1, k) &
                       + b2(i, j, km1) + b2(i, jp1, km1)) &
                     - (j2(i, j, k) + j2(ip1, j, k)) &
                     *(b1(i, j, k) + b1(ip1, j, k) &
                       + b1(i, j, km1) + b1(ip1, j, km1)))/eight &
                  *rhoi_f
          end if
          !
          v3(i, j, k) = v3(i, j, k) + dt*st3
        end do
      end do
    end do

    if (imhd == 3 .or. imhd == 2) then
      if (knac /= 0) then
        call release_memory(d3b2sq)
        call release_memory(d3b1sq)
      end if
    end if

    !
    !  4.  point mass gravitational potential gradient.
    !
    if (gptmass == zro) then
      ! All done
    else
      do k = ks, ke
        do j = js, je
          do i = is, ie
            if (geometry == 'xyz') then
              r2 = (x1b(i) - x1ptm)**2 &
                   + (x2b(j) - x2ptm)**2 &
                   + (x3a(k) - x3ptm)**2
              r2i = (x3a(k) - x3ptm) &
                    /(r2*sqrt(r2) + verysmall)
            end if
            if (geometry == 'zrp') then
              r2 = (x1b(i) - x1ptm)**2 &
                   + (x2b(j) - x2ptm)**2 &
                   + two*x2b(j)*x2ptm &
                   *(one - cos(x3a(k) - x3ptm))
              r2i = x2ptm*sin(x3a(k) - x3ptm) &
                    /(r2*sqrt(r2) + verysmall)
            end if
            if (geometry == 'rtp' .or. geometry == 'rmp') then
              !JH  correction due to Sean Matt --
              !enforces condition that phi component of force be
              !zero if point mass located at origin.
              if (x1ptm == zro) then
                r2i = zro
              else
                prpq = x1b(i)*sin(x2b(j))
                pzpq = x1b(i)*cos(x2b(j))
                prpt = x1ptm*sin(x2ptm)
                pzpt = x1ptm*cos(x2ptm)
                !
                r2 = (pzpq - pzpt)**2 &
                     + (prpq - prpt)**2 &
                     + two*prpq*prpt &
                     *(one - cos(x3a(k) - x3ptm))
                r2i = prpt*sin(x3a(k) - x3ptm) &
                      /(r2*sqrt(r2) + verysmall)
              end if
            end if
            v3(i, j, k) = v3(i, j, k) - dt*gptmass*r2i
          end do
        end do
      end do
    end if
    !
  END SUBROUTINE stv3
  !
  !=======================================================================
  !
  SUBROUTINE enforcedensityfloor
#ifdef MPI
    USE mpi
    INTEGER ierr
#endif /* MPI */

    ! Set a floor to density.  Mostly to avoid stringent Alfven-Courant
    ! conditions.  Internal energy is not touched.
    integer i, j, k
    REALNUM mass_gain_floor_local, mass_gain_floor_step

    mass_gain_floor_local = zro
    do k = ks, ke
      if (x3b(k) < densityfloor_x3_min) cycle
      if (x3b(k) > densityfloor_x3_max) exit
      do j = js, je
        if (x2b(j) < densityfloor_x2_min) cycle
        if (x2b(j) > densityfloor_x2_max) exit
        do i = is, ie
          if (x1b(i) < densityfloor_x1_min) cycle
          if (x1b(i) > densityfloor_x1_max) exit
          if (withfloormass) &
            mass_gain_floor_local = max(densityfloor - d_(i, j, k), zro)* &
                                    dvl1a(i)*dvl2a(j)*dvl3a(k)
          if (densityfloor_vkill == zro .and. densityfloor > d_(i, j, k)) then
            v1(i, j, k) = zro
            v1(i + ione, j, k) = zro
            if (withv2) then
              v2(i, j, k) = zro
              v2(i, j + jone, k) = zro
            end if
            if (withv3) then
              v3(i, j, k) = zro
              v3(i, j, k + kone) = zro
            end if
          end if
          d_(i, j, k) = max(d_(i, j, k), densityfloor)
        end do
      end do
    end do
    call boundary('d_')
    if (densityfloor_vkill >= zro) then
      call boundary('v1')
      call boundary('v2')
      call boundary('v3')
    end if

    if (withfloormass) then
#ifdef MPI
      call MPI_REDUCE(mass_gain_floor_local, mass_gain_floor_step, 1, &
                      MPI_FLOATMPI, MPI_SUM, rootnode, MPI_COMM_WORLD, ierr)
#else /* MPI */
      mass_gain_floor_step = mass_gain_floor_local
#endif /* MPI */
      if (node == rootnode) then
        mass_gain_rhofloor = mass_gain_rhofloor + mass_gain_floor_step
      end if
    end if

  END SUBROUTINE enforcedensityfloor
  !
  !=======================================================================
  !
  SUBROUTINE pdvterm
    !
    !    mln:zeus3d.pdv <----- compressional heating term in energy equation
    !                                                        ?????????, 19??
    !
    !    written by: Mike Norman
    !    modified 1: June, 1988 by Jim Stone; incorporated into ZEUS2D
    !    modified 2: February, 1990 by David Clarke; incorporated into
    !                ZEUS3D
    !
    !  PURPOSE: Computes the compressional heating source term in the energy
    !  equation, given updated velocities from the source step, namely:
    !
    !                    de/dt = -p*DIV(v), p=gam*e
    !
    !  A simple predict-correct method is used, which assumes an adiabatic
    !  flow in the pressure predict step.  For non-adiabatic flows (energy
    !  equation with an arbitrary cooling function), the routine PDVCOOL
    !  must be used.  The operator DIV is expressed in an integral formalism
    !  to reduce discretisation errors near the origin in RTP geometry.
    !
    !  INPUT VARIABLES:
    !
    !  LOCAL VARIABLES:
    !    divv   DIV(v)
    !
    !-----------------------------------------------------------------------
    !
    INTEGER i, j, k
    INTEGER ip1, jp1, kp1
    REALNUM q1, q2
    !
    REALNUM :: divv

    if (ienergy /= 1) return
    if (itote /= 0) return

    !
    !      Add pdv source term to energy density
    !
    q1 = haf*dt*(gamma - one)

    if (withv2) then
      if (withv3) then
        do k = ks, ke
          kp1 = k + kone
          do j = js, je
            jp1 = j + jone
            do i = is, ie
              ip1 = i + ione
              !      Compute divergence of velocity field.
              divv = (g2a(ip1)*g31a(ip1)*v1(ip1, j, k) &
                      - g2a(i)*g31a(i)*v1(i, j, k)) &
                     *dvl1ai(i) &
                     + (g32a(jp1)*v2(i, jp1, k) &
                        - g32a(j)*v2(i, j, k)) &
                     *g2bi(i)*dvl2ai(j) &
                     + (v3(i, j, kp1) - v3(i, j, k)) &
                     *g31bi(i)*g32bi(j)*dvl3ai(k)
              q2 = q1*divv
              e_(i, j, k) = (one - q2)/(one + q2)*e_(i, j, k)
            end do
          end do
        end do
      else
        do k = ks, ke
          kp1 = k + kone
          do j = js, je
            jp1 = j + jone
            do i = is, ie
              ip1 = i + ione
              !      Compute divergence of velocity field.
              divv = (g2a(ip1)*g31a(ip1)*v1(ip1, j, k) &
                      - g2a(i)*g31a(i)*v1(i, j, k)) &
                     *dvl1ai(i) &
                     + (g32a(jp1)*v2(i, jp1, k) &
                        - g32a(j)*v2(i, j, k)) &
                     *g2bi(i)*dvl2ai(j)
              q2 = q1*divv
              e_(i, j, k) = (one - q2)/(one + q2)*e_(i, j, k)
            end do
          end do
        end do
      end if
    else
      if (withv3) then
        do k = ks, ke
          kp1 = k + kone
          do j = js, je
            jp1 = j + jone
            do i = is, ie
              ip1 = i + ione
              !      Compute divergence of velocity field.
              divv = (g2a(ip1)*g31a(ip1)*v1(ip1, j, k) &
                      - g2a(i)*g31a(i)*v1(i, j, k)) &
                     *dvl1ai(i) &
                     + (v3(i, j, kp1) - v3(i, j, k)) &
                     *g31bi(i)*g32bi(j)*dvl3ai(k)
              q2 = q1*divv
              e_(i, j, k) = (one - q2)/(one + q2)*e_(i, j, k)
            end do
          end do
        end do
      else
        do k = ks, ke
          kp1 = k + kone
          do j = js, je
            jp1 = j + jone
            do i = is, ie
              ip1 = i + ione
              !      Compute divergence of velocity field.
              divv = (g2a(ip1)*g31a(ip1)*v1(ip1, j, k) &
                      - g2a(i)*g31a(i)*v1(i, j, k)) &
                     *dvl1ai(i)
              q2 = q1*divv
              e_(i, j, k) = (one - q2)/(one + q2)*e_(i, j, k)
            end do
          end do
        end do
      end if
    end if
  END SUBROUTINE pdvterm

  !** SUBGROUP: FIELDS ***************************************************
  !*                                                                     *
  !*                    S U B G R O U P :   F I E L D S                  *
  !*                                                                     *
  !***********************************************************************
  !
  !=======================================================================
  !
  SUBROUTINE currents(j1, j2, j3, b1, b2, b3)
    !
    !    jms:zeus3d.currents <----- computes currents densities from curl(B)
    !                                                             july, 1988
    !
    !    written by: Jim Stone
    !    modified 1: January, 1990 by David Clarke; added toggle (iall) to
    !                allow all or a portion of the grid to be computed (for
    !                use with the grid stretching feature, see EXTEND).
    !    modified 2: February, 1990 by David Clarke; incorporated into
    !                ZEUS3D.
    !    modified 3: September, 1990 by David Clarke; moved magnetic fields
    !                to face-centres.
    !    modified 4: June, 1992 by David Clarke; reworked singularity
    !                formalism.
    !
    !  PURPOSE:  Computes the (dimensionless) vector current densities from
    !  the curl of the magnetic field.  The j's are given by:
    !
    !            j = CURL(B)
    !
    !  They are edge-centred quantities and thus, coincident with the emf's.
    !  The current densities are not needed for the source terms with the
    !  MoC algorithm, but are still needed for the original CT algorithm and
    !  for graphics.  The ranges for each of the current densities are:
    !
    !            j1:    i=is,ie      j=js,jep1    k=ks,kep1
    !            j2:    i=is,iep1    j=js,je      k=ks,kep1
    !            j3:    i=is,iep1    j=js,jep1    k=ks,ke
    !
    !      Careful treatment at coordinate singularities (axes in ZRP and
    !  RTP coordinates, origin in RTP coordinates) is needed.
    !
    !  OUTPUT VARIABLES:
    !    j1       current density in 1-direction
    !    j2       current density in 2-direction
    !    j3       current density in 3-direction
    !
    !-----------------------------------------------------------------------
    !
    USE problem_variables, only: bc_sign, singularity_treatment
#ifdef MPI
    USE mpi
#endif /* MPI */
    CHARACTER(LEN=*), PARAMETER :: routinename = 'z_currents'
    REALNUM, DIMENSION(:, :, :), INTENT(OUT) :: j1, j2, j3
    REALNUM, DIMENSION(:, :, :), INTENT(IN) :: b1, b2, b3
    !
    INTEGER i, im1, ilower
    INTEGER j, jm1, jlower, jupper
    INTEGER k, km1
    !
    INTEGER imin, imax
    INTEGER jmin, jmax
    INTEGER kmin, kmax
    !
    REALNUM q1, q2, q3
    REALNUM, PARAMETER :: small = thousandth

    INTEGER ierr
    INTEGER key, color
    INTEGER, SAVE :: northaxiscomm, southaxiscomm
    INTEGER :: testcomm, testrank, testsize
    CHARACTER(LEN=messagelinelength) message
    LOGICAL, SAVE :: firstcall = .true.
    REALNUM :: buffer(isa:iea), bufferout(isa:iea)

    ! LOGICAL singularity_treatment
    !
    !---------------------------- Compute j1 -------------------------------
    !
    kmin = ks
    kmax = kep1 ! consider changing this to ke, some BCs, and the axial sum
    jmin = js
    jmax = jep1
    imin = isg  !RFK
    imax = ieg  !RFK
    !

    ! singularity_treatment=(axisshift==zro)
    ! singularity_treatment=(axisshift==zro) ! GOOD ONE

    if (l_firstnode2 .and. singularity_treatment) then
      ! Good only for certain BCs and geometries
      jlower = jmin + jone ! north pole special treatment
    else
      jlower = jmin ! not at pole, or no special treatment
    end if

    if (bc_sign == -one .and. singularity_treatment .and. l_lastnode2) then
      ! Good only for certain BCs and geometries
      jupper = jmax - jone ! south pole special treatment
    else
      jupper = jmax ! not at pole, or no special treatment
    end if

    if (firstcall) then
#ifdef MPI
      ! Define groups of processors
      key = node
      if (jlower /= jmin) then
        color = node1
      else
        color = MPI_UNDEFINED
      end if
      call mpi_comm_split(MPI_COMM_WORLD, color, key, northaxiscomm, ierr)

      key = node
      if (jupper /= jmax) then
        color = node1
      else
        color = MPI_UNDEFINED
      end if
      call mpi_comm_split(MPI_COMM_WORLD, color, key, southaxiscomm, ierr)

      key = node
      if (node == rootnode) then
        color = node1
      else
        color = MPI_UNDEFINED
      end if
      call mpi_comm_split(MPI_COMM_WORLD, color, key, testcomm, ierr)

      testrank = -99
      testsize = -99
      if (testcomm /= MPI_COMM_NULL) then
        call MPI_COMM_RANK(testcomm, testrank, ierr)
        call MPI_COMM_SIZE(testcomm, testsize, ierr)
      end if
      write (message, '(a,i5,a,i5,a,i5)') 'Testcomm rank:', testrank, &
        ' Size: ', testsize, 'Comm: ', testcomm
      call writemessage(routinename//'_comms', message)

      testrank = -99
      testsize = -99
      if (northaxiscomm /= MPI_COMM_NULL) then
        call MPI_COMM_RANK(northaxiscomm, testrank, ierr)
        call MPI_COMM_SIZE(northaxiscomm, testsize, ierr)
      end if
      write (message, '(a,i5,a,i5,a,i5)') 'Northaxiscomm rank:', testrank, &
        ' Size: ', testsize, 'Comm: ', northaxiscomm
      call writemessage(routinename//'_comms', message)

      testrank = -99
      testsize = -99
      if (southaxiscomm /= MPI_COMM_NULL) then
        call MPI_COMM_RANK(southaxiscomm, testrank, ierr)
        call MPI_COMM_SIZE(southaxiscomm, testsize, ierr)
      end if
      write (message, '(a,i5,a,i5,a,i5)') 'Southaxiscomm rank:', testrank, &
        ' Size: ', testsize, 'Comm: ', southaxiscomm
      call writemessage(routinename//'_comms', message)

#endif /* MPI */
      firstcall = .false.
    end if

    !if (geometry=='zrp') then
    !   if (g32a(jmin) == zro) jlower = jmin + jone
    !end if
    !if (geometry=='rtp'.or.geometry=='rmp') then
    !   if (g32a(jmin) < small) jlower = jmin + jone
    !   if (g32a(jmax) < small) jupper = jmax - jone
    !end if
    !
    do k = kmin, kmax
      km1 = k - kone
      do j = jlower, jupper
        jm1 = j - jone
        do i = imin, imax
          j1(i, j, k) = (g32b(j)*b3(i, j, k) &
                         - g32b(jm1)*b3(i, jm1, k)) &
                        *g2bi(i)*dvl2bi(j) &
                        - (b2(i, j, k) - b2(i, j, km1)) &
                        *g31bi(i)*g32ai(j)*dvl3bi(k)
        end do
      end do
    end do

    call writedata(j1, routinename//'_0_j1', 4)

    !
    !      Compute "j1" along the axis singularities (if detected)
    !
    if ((geometry == 'zrp' .or. geometry == 'rtp' .or. geometry == 'rmp') &
        .and. jlower /= jmin) then
      if (kmax == kmin) then
        q1 = dx3a(kmin)
      else
        q1 = x3ap(kep1p) - x3ap(ksp)
      end if
      q2 = four*g32b(jmin)*dvl2ai(jmin)/q1
      do i = imin, imax
        q3 = q2*g31bi(i)
        buffer(i) = zro
        do k = kmin, kmax - kone ! CHECK q3
          buffer(i) = buffer(i) + q3*b3(i, jmin, k)*dx3a(k)
        end do
      end do
      call writedata(buffer, routinename//'_buffern_1', 4)
#ifdef MPI
      if (nnodes3 /= 1) then ! STUDY IF THIS SHOULD BE OUTSIDE if(jlower/=jmin)
        call MPI_ALLREDUCE(buffer(imin), bufferout(imin), imax - imin + 1, &
                           MPI_FLOATMPI, MPI_SUM, northaxiscomm, ierr)
        buffer(imin:imax) = bufferout(imin:imax)
      end if
#endif /* MPI */
      call writedata(buffer, routinename//'_buffern_2', 4)
      do i = imin, imax
        do k = kmin, kmax
          j1(i, jmin, k) = buffer(i)
        end do
      end do
    end if

    if ((geometry == 'rtp' .or. geometry == 'rmp') .and. &
        jupper /= jmax) then
      if (kmax == kmin) then
        q1 = dx3a(kmin)
      else
        q1 = x3ap(kep1p) - x3ap(ksp)
      end if
      q2 = four*g32b(jmax)*dvl2ai(jmax)/q1
      do i = imin, imax
        q3 = q2*g31bi(i)
        buffer(i) = zro
        do k = kmin, kmax - kone
          buffer(i) = buffer(i) + q3*b3(i, jmax, k)*dx3a(k)
        end do
      end do
      call writedata(buffer, routinename//'_buffers_1', 4)
#ifdef MPI
      if (nnodes3 /= 1) then
        call MPI_ALLREDUCE(buffer(imin), bufferout(imin), imax - imin + 1, &
                           MPI_FLOATMPI, MPI_SUM, southaxiscomm, ierr)
        buffer(imin:imax) = bufferout(imin:imax)
      end if
#endif /* MPI */
      call writedata(buffer, routinename//'_buffers_2', 4)
      do i = imin, imax
        do k = kmin, kmax
          j1(i, jmax, k) = buffer(i)
        end do
      end do
    end if
    !
    !---------------------------- Compute j2 -------------------------------
    !
    kmin = ks
    kmax = kep1
    jmin = js
    jmax = je
    imin = isgo
    imax = ieg
    !
    ilower = imin
    if (geometry == 'rtp' .or. geometry == 'rmp') then
      if (g31a(imin) == zro) ilower = imin + ione
    end if ! Wrong, but relevant only if the grid reaches the center
    !
    do k = kmin, kmax
      km1 = k - kone
      do j = jmin, jmax
        do i = ilower, imax
          im1 = i - ione
          j2(i, j, k) = (b1(i, j, k) - b1(i, j, km1)) &
                        *g31ai(i)*g32bi(j)*dx3bi(k) &
                        - (g31b(i)*b3(i, j, k) &
                           - g31b(im1)*b3(im1, j, k)) &
                        *g31ai(i)*dx1bi(i)
        end do
      end do
    end do
    !
    !      Set "j2" at the point singularity (if detected).
    !
    if (geometry == 'rtp' .or. geometry == 'rmp') then
      if (ilower /= imin) then
        do k = kmin, kmax
          do j = jmin, jmax
            j2(imin, j, k) = zro
          end do
        end do
      end if
    end if
    !
    !---------------------------- Compute j3 -------------------------------
    !
    kmin = ks
    kmax = ke
    jmin = js
    jmax = jep1
    imin = isgo
    imax = ieg
    !
    ilower = imin
    if (geometry == 'rtp' .or. geometry == 'rmp') then
      if (g2a(imin) == zro) ilower = imin + ione
    end if ! Wrong, but relevant only if the grid reaches the center
    !
    do k = kmin, kmax
      do j = jmin, jmax
        jm1 = j - jone
        do i = ilower, imax
          im1 = i - ione
          j3(i, j, k) = (g2b(i)*b2(i, j, k) &
                         - g2b(im1)*b2(im1, j, k)) &
                        *g2ai(i)*dx1bi(i) &
                        - (b1(i, j, k) - b1(i, jm1, k)) &
                        *g2ai(i)*dx2bi(j)
        end do
      end do
    end do
    !
    !      Set "j3" at the point singularity (if detected).
    !
    if (geometry == 'rtp' .or. geometry == 'rmp') then
      if (ilower /= imin) then
        do k = kmin, kmax
          do j = jmin, jmax
            j3(imin, j, k) = zro
          end do
        end do
      end if
    end if

    call writedata(j1, routinename//'_1_j1', 4)
    call writedata(j2, routinename//'_1_j2', 4)
    call writedata(j3, routinename//'_1_j3', 4)
    !
    ! Apply (if so desired) boundary conditions directly on the currents.
    ! It may be useful for some non-ideal MHD methods.
    ! Part of the curl calculation already did some of the BC work
    ! - consider moving that to a specialized BC routine.

    call boundary('currents', j1, j2, j3)

    call writedata(j1, routinename//'_2_j1', 4)
    call writedata(j2, routinename//'_2_j2', 4)
    call writedata(j3, routinename//'_2_j3', 4)

  END SUBROUTINE currents
  !
  !=======================================================================
  !
  SUBROUTINE pressure(p, iprs)
    !
    !    dac:zeus3d.pressure <---------------------------- computes pressure
    !    from mln:zeus3d.pgas                                    march, 1991
    !
    !    written by: David Clarke
    !    modified 1: June 1992, by David Clarke; added the total energy
    !                option originally designed by Byung-IL Jun.
    !
    !  PURPOSE: Calculates the gas pressure (thermal, magnetic, or both) at
    !  each zone centred point on the grid.  If gamma < 1, then the
    !  pressure is set to 0 (allows for pressure free (dust) calculations).
    !
    !  INPUT VARIABLES:
    !    iprs     =1 => thermal pressure only
    !             =2 => magnetic pressure only
    !             =3 => total (thermal plus magnetic) pressure
    !
    !  OUTPUT VARIABLES:
    !    p        array of pressure.
    !
    !-----------------------------------------------------------------------
    !
    REALNUM, INTENT(OUT) :: p(:, :, :)
    INTEGER, INTENT(IN) :: iprs
    !
    CHARACTER(LEN=*), PARAMETER :: routinename = 'z_pressure'
    !
    INTEGER i, ip1
    INTEGER j, jp1
    INTEGER k, kp1
    REALNUM q1, q2, q3
    REALNUM gammamone
    REALNUM isothermal_factor, polytrope_factor, rho
    REALNUM tracer, qt0, qt1
    !
    !
    !-----------------------------------------------------------------------
    !
    call writemessage(routinename//'_entry', 3)
    gammamone = gamma - one
    q1 = max(gammamone, zro)
    !
    if (itote == 1) then
      !
      !-----------------------------------------------------------------------
      !---------------------- For total energy equation ----------------------
      !-----------------------------------------------------------------------
      !
      if (ienergy == 1) then
        if (withv2) then
          if (withv3) then
            do k = ksg, kego
              kp1 = k + kone
              do j = jsg, jego
                jp1 = j + jone
                do i = isg, iego
                  ip1 = i + ione
                  p(i, j, k) = e_(i, j, k) + efloor &
                               - ((v1(i, j, k) + v1(ip1, j, k))**2 &
                                  + (v2(i, j, k) + v2(i, jp1, k))**2 &
                                  + (v3(i, j, k) + v3(i, j, kp1))**2)*eighth &
                               *d_(i, j, k)
                  p(i, j, k) = q1*p(i, j, k)
                end do
              end do
            end do
          else
            do k = ksg, kego
              do j = jsg, jego
                jp1 = j + jone
                do i = isg, iego
                  ip1 = i + ione
                  p(i, j, k) = e_(i, j, k) + efloor &
                               - ((v1(i, j, k) + v1(ip1, j, k))**2 &
                                  + (v2(i, j, k) + v2(i, jp1, k))**2 &
                                  )*eighth &
                               *d_(i, j, k)
                  p(i, j, k) = q1*p(i, j, k)
                end do
              end do
            end do
          end if
        else
          if (withv3) then
            do k = ksg, kego
              kp1 = k + kone
              do j = jsg, jego
                do i = isg, iego
                  ip1 = i + ione
                  p(i, j, k) = e_(i, j, k) + efloor &
                               - ((v1(i, j, k) + v1(ip1, j, k))**2 &
                                  + (v3(i, j, k) + v3(i, j, kp1))**2)*eighth &
                               *d_(i, j, k)
                  p(i, j, k) = q1*p(i, j, k)
                end do
              end do
            end do
          else
            do k = ksg, kego
              do j = jsg, jego
                do i = isg, iego
                  ip1 = i + ione
                  p(i, j, k) = e_(i, j, k) + efloor &
                               - ((v1(i, j, k) + v1(ip1, j, k))**2 &
                                  )*eighth &
                               *d_(i, j, k)
                  p(i, j, k) = q1*p(i, j, k)
                end do
              end do
            end do
          end if
        end if
        if (withgp) then
          do k = ksg, kego
            do j = jsg, jego
              do i = isg, iego
                p(i, j, k) = p(i, j, k) - gp(i, j, k)*d_(i, j, k)*q1
              end do
            end do
          end do
        end if
      end if
    else
      !
      !-----------------------------------------------------------------------
      !-------------------- For internal energy equation ---------------------
      !-----------------------------------------------------------------------
      !
      !      Contribution from the internal energy per unit volume (e).
      !
      if ((iprs == 1) .or. (iprs == 3)) then
        if (ienergy == 1) then
          do k = ksg, kego
            do j = jsg, jego
              do i = isg, iego
                p(i, j, k) = q1*(e_(i, j, k) + efloor)
              end do
            end do
          end do
        end if
        if (ienergy == 2) then
          do k = ksg, kego
            do j = jsg, jego
              do i = isg, iego
                p(i, j, k) = e_(i, j, k)*(d_(i, j, k) + dfloor)
              end do
            end do
          end do
        end if
        if (ienergy == 0) then
          do k = ksg, kego
            do j = jsg, jego
              do i = isg, iego
                p(i, j, k) = ciso**2*(d_(i, j, k) + dfloor)
              end do
            end do
          end do
        end if
        if (ienergy == -1) then
          isothermal_factor = ciso**2
          polytrope_factor = isothermal_factor/rho_stiffen**(gammamone)

          select case (eoskind)
          case (1)
            do k = ksg, kego
              do j = jsg, jego
                do i = isg, iego
                  rho = d_(i, j, k)
                  if (rho <= rho_stiffen) then
                    p(i, j, k) = isothermal_factor*rho
                  else
                    p(i, j, k) = polytrope_factor*rho**gamma
                  end if
                end do
              end do
            end do
          case (2)
            do k = ksg, kego
              do j = jsg, jego
                do i = isg, iego
                  rho = d_(i, j, k)
                  p(i, j, k) = isothermal_factor*rho + &
                               polytrope_factor*rho**gamma
                end do
              end do
            end do
          case (3)
            do k = ksg, kego
              do j = jsg, jego
                do i = isg, iego
                  p(i, j, k) = polytrope_factor*d_(i, j, k)**gamma
                end do
              end do
            end do
          case (0)
            do k = ksg, kego
              do j = jsg, jego
                do i = isg, iego
                  p(i, j, k) = isothermal_factor*d_(i, j, k)
                end do
              end do
            end do
          case (4)
            qt0 = ciso**2
            qt1 = ciso1**2
            do k = ksg, kego
              do j = jsg, jego
                do i = isg, iego
                  tracer = tracerfields(i, j, k, 1)
                  p(i, j, k) = d_(i, j, k)* &
                               (qt0*(one - tracer) + qt1*tracer)
                end do
              end do
            end do
          case default
            call finalstop(routinename, 'EOSKIND not assigned')
          end select

        end if
      end if
      !
      !      Contribution from the magnetic pressure.
      !
      if ((iprs == 2) .or. (iprs == 3)) then
        if (imhd /= 0) then
          select case (iprs)
          case (2)
            q2 = q1
            q3 = zro
          case (3)
            q2 = zro
            q3 = one
          case default
            call threeoutput(routinename, 'Pressure code not assigned.')
            call finalstop(routinename, 'Execution stops.')
          end select
          if (withbp) then
            if (withb3) then
              do k = ksg, kego
                kp1 = k + kone
                do j = jsg, jego
                  jp1 = j + jone
                  do i = isg, iego
                    ip1 = i + ione
                    p(i, j, k) = q2*efloor + q3*p(i, j, k) &
                                 + ((b1(i, j, k) + b1(ip1, j, k))**2 &
                                    + (b2(i, j, k) + b2(i, jp1, k))**2 &
                                    + (b3(i, j, k) + b3(i, j, kp1))**2) &
                                 *eighth
                  end do
                end do
              end do
            else
              do k = ksg, keg
                do j = jsg, jego
                  jp1 = j + jone
                  do i = isg, iego
                    ip1 = i + ione
                    p(i, j, k) = q2*efloor + q3*p(i, j, k) &
                                 + ((b1(i, j, k) + b1(ip1, j, k))**2 &
                                    + (b2(i, j, k) + b2(i, jp1, k))**2 &
                                    ) &
                                 *eighth
                  end do
                end do
              end do
            end if
          else
            if (withb3) then
              do k = ksg, kego
                kp1 = k + kone
                do j = jsg, jeg
                  do i = isg, ieg
                    p(i, j, k) = q2*efloor + q3*p(i, j, k) &
                                 + ( &
                                 +(b3(i, j, k) + b3(i, j, kp1))**2) &
                                 *eighth
                  end do
                end do
              end do
            else
              do k = ksg, keg
                do j = jsg, jeg
                  do i = isg, ieg
                    p(i, j, k) = q2*efloor + q3*p(i, j, k)
                  end do
                end do
              end do
            end if
          end if
        end if
      end if
    end if

    call writemessage(routinename//'_calculation_done', 3)
    call writedata(gamma, routinename//'_p_gamma_', 4)
    call writedata(q1, routinename//'_p_q1_', 4)
    call writedata(q2, routinename//'_p_q2_', 4)
    call writedata(q3, routinename//'_p_q3_', 4)
    !
  END SUBROUTINE pressure
  !
  !=======================================================================
  !
  SUBROUTINE soundspeed(cs, iall)
    CHARACTER(LEN=*), PARAMETER :: routinename = 'z_soundspeed'
    !
    !    dac:zeus3d.sndspd <----------------- computes adiabatic sound speed
    !                                                             june, 1992
    !
    !    written by: David Clarke
    !    modified 1:
    !
    !  PURPOSE: Calculates a zone centred sound speed for a gas with a
    !  non-isothermal equation of state.
    !
    !  INPUT VARIABLES:
    !    iall     =0 => compute block bounded by "ism1"   to "ie"  ,
    !                                            "jsm1"   to "je"  ,
    !                                            "ksm1"   to "ke"
    !             =1 => compute block bounded by "ism2" to "iep2",
    !                                            "jsm2" to "jep2",
    !                                            "ksm2" to "kep2"
    !
    !  OUTPUT VARIABLES:
    !    cs       array of sound speeds.
    !
    !-----------------------------------------------------------------------
    !
    REALNUM, INTENT(OUT) :: cs(:, :, :)
    INTEGER, INTENT(IN) :: iall
    !
    INTEGER i, ip1, imin, imax
    INTEGER j, jp1, jmin, jmax
    INTEGER k, kp1, kmin, kmax
    REALNUM q1
    REALNUM gammamone
    REALNUM isothermal_factor, gamma_polytrope_factor, rho
    REALNUM tracer, qt0, qt1
    !
    !-----------------------------------------------------------------------
    !
    if (iall == 1) then
      kmin = ksm2
      kmax = kep2
      jmin = jsm2
      jmax = jep2
      imin = ism2
      imax = iep2
    else
      kmin = ksm1
      kmax = ke
      jmin = jsm1
      jmax = je
      imin = ism1
      imax = ie
    end if
    !
    !      Compute the sound speeds.
    !
    gammamone = gamma - one
    q1 = gamma*(gammamone)
    if (itote == 1) then
      !
      !-----------------------------------------------------------------------
      !---------------------- For total energy equation ----------------------
      !-----------------------------------------------------------------------
      !
      if (.not. withv2) call finalstop(routinename, &
                                       '.not.withv2 incompatible with itote==1')
      if (.not. withv3) call finalstop(routinename, &
                                       '.not.withv3 incompatible with itote==1')
      if (withgp) then
        do k = kmin, kmax
          kp1 = k + kone
          do j = jmin, jmax
            jp1 = j + jone
            do i = imin, imax
              ip1 = i + ione
              cs(i, j, k) = e_(i, j, k)/d_(i, j, k) &
                            - ((v1(i, j, k) + v1(ip1, j, k))**2 &
                               + (v2(i, j, k) + v2(i, jp1, k))**2 &
                               + (v3(i, j, k) + v3(i, j, kp1))**2)/eight &
                            - gp(i, j, k)
              cs(i, j, k) = sqrt(q1*cs(i, j, k))
            end do
          end do
        end do
      else
        do k = kmin, kmax
          kp1 = k + kone
          do j = jmin, jmax
            jp1 = j + jone
            do i = imin, imax
              ip1 = i + ione
              cs(i, j, k) = e_(i, j, k)/d_(i, j, k) &
                            - ((v1(i, j, k) + v1(ip1, j, k))**2 &
                               + (v2(i, j, k) + v2(i, jp1, k))**2 &
                               + (v3(i, j, k) + v3(i, j, kp1))**2)/eight
              cs(i, j, k) = sqrt(q1*cs(i, j, k))
            end do
          end do
        end do
      end if
      !
    else
      !
      !-----------------------------------------------------------------------
      !-------------------- For internal energy equation ---------------------
      !-----------------------------------------------------------------------
      !
      if (ienergy == 1) then
        do k = kmin, kmax
          do j = jmin, jmax
            do i = imin, imax
              cs(i, j, k) = sqrt(q1*e_(i, j, k)/d_(i, j, k))
            end do
          end do
        end do
      end if
      if (ienergy == 2) then
        do k = kmin, kmax
          do j = jmin, jmax
            do i = imin, imax
              cs(i, j, k) = sqrt(e_(i, j, k))
            end do
          end do
        end do
      end if
      if (ienergy == -1) then
        isothermal_factor = ciso**2
        gamma_polytrope_factor = gamma*isothermal_factor &
                                 /rho_stiffen**(gammamone)

        select case (eoskind)
        case (1)
          do k = ksg, kego
            do j = jsg, jego
              do i = isg, iego
                rho = d_(i, j, k)
                if (rho <= rho_stiffen) then
                  cs(i, j, k) = ciso
                else
                  cs(i, j, k) = sqrt(gamma_polytrope_factor*rho**(gammamone))
                end if
              end do
            end do
          end do
        case (2)
          do k = ksg, kego
            do j = jsg, jego
              do i = isg, iego
                cs(i, j, k) = sqrt(isothermal_factor + &
                                   gamma_polytrope_factor &
                                   *d_(i, j, k)**(gammamone))
              end do
            end do
          end do
        case (3)
          do k = ksg, kego
            do j = jsg, jego
              do i = isg, iego
                cs(i, j, k) = sqrt(gamma_polytrope_factor &
                                   *d_(i, j, k)**(gammamone))
              end do
            end do
          end do
        case (0)
          do k = ksg, kego
            do j = jsg, jego
              do i = isg, iego
                cs(i, j, k) = ciso
              end do
            end do
          end do
        case (4)
          qt0 = ciso**2
          qt1 = ciso1**2
          do k = ksg, kego
            do j = jsg, jego
              do i = isg, iego
                tracer = tracerfields(i, j, k, 1)
                cs(i, j, k) = sqrt(qt0*(one - tracer) + qt1*tracer)
              end do
            end do
          end do
        case default
          call finalstop(routinename, 'EOSKIND not assigned')
        end select
      end if
    end if
    !
  END SUBROUTINE soundspeed
  !
  !=======================================================================
  !
  SUBROUTINE soundspeedsquare(cssq, iall)
    CHARACTER(LEN=*), PARAMETER :: routinename = 'z_soundspeedsquare'
    !  INPUT VARIABLES:
    !    iall     =0 => compute block bounded by "ism1"   to "ie"  ,
    !                                            "jsm1"   to "je"  ,
    !                                            "ksm1"   to "ke"
    !             =1 => compute block bounded by "ism2" to "iep2",
    !                                            "jsm2" to "jep2",
    !                                            "ksm2" to "kep2"
    !
    !  OUTPUT VARIABLES:
    !    cssq       array of sound speeds, squared
    !
    !-----------------------------------------------------------------------
    !
    REALNUM, INTENT(OUT) :: cssq(:, :, :)
    INTEGER, INTENT(IN) :: iall
    !
    INTEGER i, ip1, imin, imax
    INTEGER j, jp1, jmin, jmax
    INTEGER k, kp1, kmin, kmax
    REALNUM q1
    REALNUM gammamone
    REALNUM isothermal_factor, gamma_polytrope_factor, rho
    REALNUM tracer, qt0, qt1
    !
    !-----------------------------------------------------------------------
    !
    if (iall == 1) then
      kmin = ksm2
      kmax = kep2
      jmin = jsm2
      jmax = jep2
      imin = ism2
      imax = iep2
    else
      kmin = ksm1
      kmax = ke
      jmin = jsm1
      jmax = je
      imin = ism1
      imax = ie
    end if

    gammamone = gamma - one
    q1 = gamma*(gammamone)
    if (itote == 1) then
      !
      !-----------------------------------------------------------------------
      !---------------------- For total energy equation ----------------------
      !-----------------------------------------------------------------------
      !
      if (.not. withv2) call finalstop(routinename, &
                                       '.not.withv2 incompatible with itote==1')
      if (.not. withv3) call finalstop(routinename, &
                                       '.not.withv3 incompatible with itote==1')
      if (withgp) then
        do k = kmin, kmax
          kp1 = k + kone
          do j = jmin, jmax
            jp1 = j + jone
            do i = imin, imax
              ip1 = i + ione
              cssq(i, j, k) = e_(i, j, k)/d_(i, j, k) &
                              - ((v1(i, j, k) + v1(ip1, j, k))**2 &
                                 + (v2(i, j, k) + v2(i, jp1, k))**2 &
                                 + (v3(i, j, k) + v3(i, j, kp1))**2)/eight &
                              - gp(i, j, k)
              cssq(i, j, k) = q1*cssq(i, j, k)
            end do
          end do
        end do
      else
        do k = kmin, kmax
          kp1 = k + kone
          do j = jmin, jmax
            jp1 = j + jone
            do i = imin, imax
              ip1 = i + ione
              cssq(i, j, k) = e_(i, j, k)/d_(i, j, k) &
                              - ((v1(i, j, k) + v1(ip1, j, k))**2 &
                                 + (v2(i, j, k) + v2(i, jp1, k))**2 &
                                 + (v3(i, j, k) + v3(i, j, kp1))**2)/eight
              cssq(i, j, k) = q1*cssq(i, j, k)
            end do
          end do
        end do
      end if
      !
    else
      !
      !-----------------------------------------------------------------------
      !-------------------- For internal energy equation ---------------------
      !-----------------------------------------------------------------------
      !
      if (ienergy == 1) then
        do k = kmin, kmax
          do j = jmin, jmax
            do i = imin, imax
              cssq(i, j, k) = q1*e_(i, j, k)/d_(i, j, k)
            end do
          end do
        end do
      end if
      if (ienergy == 2) then
        do k = kmin, kmax
          do j = jmin, jmax
            do i = imin, imax
              cssq(i, j, k) = e_(i, j, k)
            end do
          end do
        end do
      end if
      if (ienergy == -1) then
        isothermal_factor = ciso**2
        gamma_polytrope_factor = gamma*isothermal_factor &
                                 /rho_stiffen**(gammamone)

        select case (eoskind)
        case (1)
          do k = ksg, kego
            do j = jsg, jego
              do i = isg, iego
                rho = d_(i, j, k)
                if (rho <= rho_stiffen) then
                  cssq(i, j, k) = isothermal_factor
                else
                  cssq(i, j, k) = gamma_polytrope_factor*rho**(gammamone)
                end if
              end do
            end do
          end do
        case (2)
          do k = ksg, kego
            do j = jsg, jego
              do i = isg, iego
                cssq(i, j, k) = isothermal_factor + &
                                gamma_polytrope_factor*d_(i, j, k)**(gammamone)
              end do
            end do
          end do
        case (3)
          do k = ksg, kego
            do j = jsg, jego
              do i = isg, iego
                cssq(i, j, k) = gamma_polytrope_factor*d_(i, j, k)**(gammamone)
              end do
            end do
          end do
        case (0)
          do k = ksg, kego
            do j = jsg, jego
              do i = isg, iego
                cssq(i, j, k) = isothermal_factor
              end do
            end do
          end do
        case (4)
          qt0 = ciso**2
          qt1 = ciso1**2
          do k = ksg, kego
            do j = jsg, jego
              do i = isg, iego
                tracer = tracerfields(i, j, k, 1)
                cssq(i, j, k) = qt0*(one - tracer) + qt1*tracer
              end do
            end do
          end do
        case default
          call finalstop(routinename, 'EOSKIND not assigned')
        end select
      end if
    end if
  END SUBROUTINE soundspeedsquare
  !
  !=======================================================================
  !
  SUBROUTINE spenergy(se, ise)
    CHARACTER(LEN=*), PARAMETER :: routinename = 'z_spenergy'
    !
    !    dac:zeus3d.spenergy <------------ computes specific internal energy
    !                                                             june, 1992
    !
    !    written by: David Clarke
    !    modified 1:
    !
    !  PURPOSE: Calculates the specific internal energy at each zone centred
    !  point on the grid.
    !
    !  INPUT VARIABLES:
    !    ise      =1 => compute internal energy density per unit mass
    !    ise      =2 => compute (e+p)/d for itote=1, e/d otherwise.
    !
    !  OUTPUT VARIABLES:
    !    se       array of specific internal energy.
    !
    !-----------------------------------------------------------------------
    !
    REALNUM, INTENT(OUT) :: se(isa:, jsa:, ksa:)
    INTEGER, INTENT(IN) :: ise
    !
    INTEGER i, ip1
    INTEGER j, jp1
    INTEGER k, kp1
    REALNUM q1, q2
    !
    !-----------------------------------------------------------------------
    !
    !      Compute the internal energy per unit mass.
    !
    if (itote == 1) then
      if (.not. withv2) &
        call finalstop(routinename, '.not.withv2 incompatible with itote==1')
      if (.not. withv3) &
        call finalstop(routinename, '.not.withv3 incompatible with itote==1')
      !
      !-----------------------------------------------------------------------
      !---------------------- For total energy equation ----------------------
      !-----------------------------------------------------------------------
      !
      if (ise == 2) then
        q1 = gamma
        q2 = gamma - one
      else
        q1 = one
        q2 = one
      end if
      do k = ksg, kego
        kp1 = k + kone
        do j = jsg, jego
          jp1 = j + jone
          do i = isg, iego
            ip1 = i + ione
            se(i, j, k) = q1*e_(i, j, k)/d_(i, j, k) &
                          - q2*((v1(i, j, k) + v1(ip1, j, k))**2 &
                                + (v2(i, j, k) + v2(i, jp1, k))**2 &
                                + (v3(i, j, k) + v3(i, j, kp1))**2) &
                          *eighth
          end do
        end do
      end do
      if (withgp) then
        do k = ksg, kego
          do j = jsg, jego
            do i = isg, iego
              se(i, j, k) = se(i, j, k) - q2*gp(i, j, k)
            end do
          end do
        end do
      end if
      !
    else
      !
      !-----------------------------------------------------------------------
      !-------------------- For internal energy equation ---------------------
      !-----------------------------------------------------------------------
      !
      do k = ksg, kego
        do j = jsg, jego
          do i = isg, iego
            se(i, j, k) = e_(i, j, k)/d_(i, j, k)
          end do
        end do
      end do
      !
    end if
    !
  END SUBROUTINE spenergy
  !
  !=======================================================================
  !
  SUBROUTINE stov(s1, s2, s3)
    !
    !    mln:zeus3d.stov <----------------- computes velocities from momenta
    !                                                          october, 1987
    !
    !    written by: Mike Norman
    !    modified 1: June, 1988 by Jim Stone; incorporated into ZEUS2D
    !    modified 2: January, 1990 by David Clarke; incorporated into ZEUS3D
    !
    !  PURPOSE:  Computes linear velocities "v1", "v2", and "v3" from
    !  momentum densities "s1", "s2", and "s3" and appropriate metric
    !  factors.  Velocities are computed between and including i=is,ie,
    !  j=js,je, and k=ks,ke.
    !
    !  INPUT VARIABLES:
    !    s1,s2,s3 = momentum densities in the 1-, 2-, and 3-directions.
    !
    !-----------------------------------------------------------------------
    !
    REALNUM, DIMENSION(:, :, :), INTENT(IN) :: s1, s2, s3
    !
    INTEGER i, j, k
    INTEGER im1, jm1, km1
    !
    !-----------------------------------------------------------------------
    !
    do k = ks, ke
      km1 = k - kone
      do j = js, je
        jm1 = j - jone
        do i = is, ie
          im1 = i - ione
          v1(i, j, k) = s1(i, j, k)*two/(d_(im1, j, k) + d_(i, j, k))
          if (withv2) &
            v2(i, j, k) = s2(i, j, k)*two/(d_(i, jm1, k) + d_(i, j, k)) &
                          *g2bi(i)
          if (withv3) &
            v3(i, j, k) = s3(i, j, k)*two/(d_(i, j, km1) + d_(i, j, k)) &
                          *g31bi(i)*g32bi(j)
        end do
      end do
    end do
    !
  END SUBROUTINE stov
  !
  !=======================================================================
  !
  SUBROUTINE v1tos1(s1)
    !
    !    dac:zeus3d.v1tos1 <----------- computes 1-momentum from 1- velocity
    !                                                             july, 1990
    !
    !    written by: David Clarke
    !    modified 1:
    !
    !  PURPOSE: Computes the momentum density S1 from the linear velocity
    !  V1 and the density D.  In all geometries, S1 is a linear momentum
    !  density.
    !                   compute block bounded by "ism1"   to "iep2"  ,
    !                                            "jsm2"   to "jep2"  ,
    !                                            "ksm2"   to "kep2"
    !
    !  INPUT VARIABLES:
    !
    !  OUTPUT VARIABLES:
    !    s1       = momentum density in the 1-direction
    !
    !-----------------------------------------------------------------------
    !
    REALNUM, INTENT(OUT) :: s1(:, :, :)
    !
    INTEGER i, im1
    INTEGER j
    INTEGER k
    !
    !-----------------------------------------------------------------------
    !
    do k = ksm2, kep2
      do j = jsm2, jep2
        do i = ism1, iep2
          im1 = i - ione
          s1(i, j, k) = v1(i, j, k)*haf*(d_(im1, j, k) + d_(i, j, k))
        end do
      end do
    end do
    !
  END SUBROUTINE v1tos1
  !
  !=======================================================================
  !
  SUBROUTINE v2tos2(s2)
    !
    !    dac:zeus3d.v2tos2 <------------ computes 2-momentum from 2-velocity
    !                                                             july, 1990
    !
    !    written by: David Clarke
    !    modified 1:
    !
    !  PURPOSE: Computes the momentum density S2 from the linear velocity
    !  V2, the density D, and the metric scaling factor G2B.  In XYZ and ZRP
    !  geometries, S2 is a linear momentum while in RTP coordinates, S2 is
    !  an angular momentum about the origin.
    !
    !                   compute block bounded by "ism1"   to "ie"  ,
    !                                            "js"     to "jep1"  ,
    !                                            "ks"     to "ke"
    !
    !  OUTPUT VARIABLES:
    !    s2       = momentum density in the 2-direction
    !
    !-----------------------------------------------------------------------
    !
    REALNUM, INTENT(OUT) :: s2(:, :, :)
    !
    INTEGER i
    INTEGER j, jm1
    INTEGER k
    !
    !-----------------------------------------------------------------------
    !
    do k = ks, ke
      do j = js, jep1
        jm1 = j - jone
        do i = ism1, ie
          s2(i, j, k) = v2(i, j, k)*haf*(d_(i, jm1, k) + d_(i, j, k))*g2b(i)
        end do
      end do
    end do
    !
  END SUBROUTINE v2tos2
  !
  !=======================================================================
  !
  SUBROUTINE v3tos3(s3)
    !
    !    dac:zeus3d.v3tos3 <------------ computes 3-momentum from 3-velocity
    !                                                             july, 1990
    !
    !    written by: David Clarke
    !    modified 1:
    !
    !  PURPOSE: Computes the momentum density S3 from the linear velocity
    !  V3, the density D, and the metric scaling factors G31B and G32B.
    !  In XYZ geometry, S3 is a linear momentum while in ZRP and RTP
    !  geometries S3 is an angular momentum about the 2-axis (ijb).
    !
    !                   compute block bounded by "ism1"   to "ie"  ,
    !                                            "jsm1"   to "je"  ,
    !                                            "ks"     to "kep1"
    !
    !  OUTPUT VARIABLES:
    !    s3       = momentum density in the 3-direction
    !
    !-----------------------------------------------------------------------
    !
    REALNUM, INTENT(OUT) :: s3(:, :, :)
    !
    INTEGER i
    INTEGER j
    INTEGER k, km1
    !
    !-----------------------------------------------------------------------
    !
    do k = ks, kep1
      km1 = k - kone
      do j = jsm1, je
        do i = ism1, ie
          s3(i, j, k) = v3(i, j, k)*haf*(d_(i, j, km1) + d_(i, j, k)) &
                        *g31b(i)*g32b(j)
        end do
      end do
    end do
    !
  END SUBROUTINE v3tos3
  !
  !=======================================================================
  !
  SUBROUTINE vtos(s1, s2, s3)
    !
    !    mln:zeus3d.vtos <----------------- computes momenta from velocities
    !                                                          october, 1987
    !
    !    written by: Mike Norman
    !    modified 1: June, 1988 by Jim Stone; incorporated into ZEUS2D
    !    modified 2: February, 1990 by David Clarke; incorporated into
    !                ZEUS3D
    !
    !  PURPOSE: Computes the momentum densities S1, S2, and S3 from the
    !  linear velocities V1, V2, and V3, the density D, and the appropriate
    !  metric scaling factors, while the subroutine STOV does the reverse.
    !
    !  OUTPUT VARIABLES:
    !    s1,s2,s3 = momentum densities in the 1-, 2-, and 3-directions
    !
    !-----------------------------------------------------------------------
    !
    REALNUM, DIMENSION(:, :, :), INTENT(OUT) :: s1, s2, s3
    !
    INTEGER i, j, k
    INTEGER im1, jm1, km1
    !
    !-----------------------------------------------------------------------
    !
    do k = ks, ke
      km1 = k - kone
      do j = js, je
        jm1 = j - jone
        do i = is, ie
          im1 = i - ione
          s1(i, j, k) = v1(i, j, k)*haf*(d_(im1, j, k) + d_(i, j, k))
          if (withv2) &
            s2(i, j, k) = v2(i, j, k)*haf*(d_(i, jm1, k) + d_(i, j, k)) &
                          *g2b(i)
          if (withv3) &
            s3(i, j, k) = v3(i, j, k)*haf*(d_(i, j, km1) + d_(i, j, k)) &
                          *g31b(i)*g32b(j)
        end do
      end do
    end do
    !
  END SUBROUTINE vtos
  !
  !=======================================================================
  !
  SUBROUTINE col_dens(rin_cd, rout_cd, top_cd, bottom_cd)
#ifdef MPI
    USE mpi
    USE zeus_mpi
#endif /* MPI */
    REALNUM, DIMENSION(:, :, :), INTENT(INOUT) :: rin_cd, rout_cd, top_cd, &
                                                  bottom_cd
    ! Calculates inner and outer radial and polar column densities and returns
    ! these arrays filled.
    CHARACTER(LEN=*), PARAMETER :: routinename = 'z_col_dens'
    INTEGER :: i, j, k, iint, jint, nodeidx
#ifdef MPI
    INTEGER ierr
    REAL, DIMENSION(jeg, keg) :: local_rcd
    REAL, DIMENSION(ieg, keg) :: local_tcd
    REAL, DIMENSION(jeg, keg, nnodes1*nnodes2*nnodes3) :: total_rcd
    REAL, DIMENSION(ieg, keg, nnodes1*nnodes2*nnodes3) :: total_tcd
#endif /* MPI */

    ! Clear arrays
    rin_cd = zro
    rout_cd = zro
    top_cd = zro
    bottom_cd = zro
#ifdef MPI
    local_rcd = zro
    local_tcd = zro
    total_rcd = zro
    total_tcd = zro

    ! Calculate the radially integrated values for the tile
    DO k = ks, ke
      DO j = js, je
        DO i = is, ie
          local_rcd(j, k) = local_rcd(j, k) + d_(i, j, k)*dx1a(i)
        END DO
      END DO
    END DO
    DO k = ks, ke
      DO i = is, ie
        DO j = js, je
          local_tcd(i, k) = local_tcd(i, k) + d_(i, j, k)*x1b(i)*dx2a(j)
        END DO
      END DO
    END DO

    ! Communicate with all other tiles
    CALL MPI_ALLGATHER(local_rcd, jeg*keg, MPI_FLOATMPI, &
                       total_rcd, jeg*keg, MPI_FLOATMPI, &
                       MPI_COMM_WORLD, ierr)
    CALL MPI_ALLGATHER(local_tcd, ieg*keg, MPI_FLOATMPI, &
                       total_tcd, ieg*keg, MPI_FLOATMPI, &
                       MPI_COMM_WORLD, ierr)
#endif /* MPI */

    ! Calculate the local column density in each radial direction for each
    ! point of the tile
    DO k = ks, ke
      DO j = js, je
        ! Compute the integrated column density for the cells around the inner
        ! radial boundary
        rin_cd(is, j, k) = d_(is, j, k)*haf*dx1a(is)
        DO i = is + 1, ie
          ! Compute the integrated column density from the origin
          DO iint = is, i - 1
            rin_cd(i, j, k) = rin_cd(i, j, k) + d_(iint, j, k)*dx1a(iint)
          END DO
          ! Compute the additional column density within the cell itself
          rin_cd(i, j, k) = rin_cd(i, j, k) + d_(i, j, k)*haf*dx1a(i)
        END DO

        ! Compute the column density for the cells next to the outer radial
        ! boundary
        rout_cd(ie, j, k) = d_(ie, j, k)*haf*dx1a(ie)
        DO i = is, ie - 1
          ! Compute the integrated column density from the boundary
          DO iint = i + 1, ie
            rout_cd(i, j, k) = rout_cd(i, j, k) + d_(iint, j, k)*dx1a(iint)
          END DO
          ! Compute the additional column density within the cell itself
          rout_cd(i, j, k) = rout_cd(i, j, k) + d_(i, j, k)*haf*dx1a(i)
        END DO
      END DO
    END DO
    ! Calculate the local column density in each polar direction for each
    ! point of the tile
    DO k = ks, ke
      DO i = is, ie
        ! Compute the integrated column density for the cells next to the bottom
        ! polar boundary
        bottom_cd(i, js, k) = d_(i, js, k)*haf*x1b(i)*dx2a(js)
        DO j = js + 1, je
          ! Compute the integrated column density from the bottom polar bound
          DO jint = js, j - 1
            bottom_cd(i, j, k) = bottom_cd(i, j, k) + d_(i, jint, k)*x1b(i)*dx2a(jint)
          END DO
          ! Compute the additional column density within the cell itself
          bottom_cd(i, j, k) = bottom_cd(i, j, k) + d_(i, j, k)*haf*x1b(i)*dx2a(j)
        END DO

        ! Compute the column density for the cells next to the top polar
        ! boundary
        top_cd(i, je, k) = d_(i, je, k)*haf*x1b(i)*dx2a(je)
        DO j = js, je - 1
          ! Compute the integrated column density from the boundary
          DO jint = j + 1, je
            top_cd(i, j, k) = top_cd(i, j, k) + d_(i, jint, k)*x1b(i)*dx2a(jint)
          END DO
          ! Compute the additional column density within the cell itself
          top_cd(i, j, k) = top_cd(i, j, k) + d_(i, j, k)*haf*x1b(i)*dx2a(j)
        END DO
      END DO
    END DO

#ifdef MPI
    ! Add the radially integrated blocks from other tiles
    IF (node1 > 0) THEN
      DO i = 0, node1 - 1
        nodeidx = nodefun(i, node2, node3)
        DO j = js, je
          DO k = ks, ke
            rin_cd(:, j, k) = rin_cd(:, j, k) + total_rcd(j, k, nodeidx + 1)
          END DO
        END DO
      END DO
    END IF
    IF (node1 < nnodes1 - 1) THEN
      DO i = node1 + 1, nnodes1 - 1
        nodeidx = nodefun(i, node2, node3)
        DO j = js, je
          DO k = ks, ke
            rout_cd(:, j, k) = rout_cd(:, j, k) + total_rcd(j, k, nodeidx + 1)
          END DO
        END DO
      END DO
    END IF
    ! Add the polarly integrated blocks from other tiles
    IF (node2 > 0) THEN
      DO j = 0, node2 - 1
        nodeidx = nodefun(node1, j, node3)
        DO i = is, ie
          DO k = ks, ke
            bottom_cd(i, :, k) = bottom_cd(i, :, k) + total_tcd(i, k, nodeidx + 1)
          END DO
        END DO
      END DO
    END IF
    IF (node2 < nnodes2 - 1) THEN
      DO j = node2 + 1, nnodes2 - 1
        nodeidx = nodefun(node1, j, node3)
        DO i = is, ie
          DO k = ks, ke
            top_cd(i, :, k) = top_cd(i, :, k) + total_tcd(i, k, nodeidx + 1)
          END DO
        END DO
      END DO
    END IF
#endif /* MPI */
  END SUBROUTINE col_dens
  !
  !=======================================================================
  !
  SUBROUTINE xrays(rin_cd, top_cd, bottom_cd, xr_array)
    REALNUM, PARAMETER :: au = 1.495978707e13_pk, zeta_1 = 6e-11_pk, &
                          zeta_2 = 1e-14_pk, sig_Xa = 3.6e-3_pk, sig_Xs = 1.7_pk, &
                          alpha = 0.4, beta = 0.65, msun = 1.98847e33_pk
    REALNUM, DIMENSION(:, :, :), INTENT(IN) :: rin_cd, top_cd, bottom_cd
    REALNUM, DIMENSION(:, :, :), INTENT(INOUT) :: xr_array
    CHARACTER(LEN=*), PARAMETER :: routinename = 'z_xrays'

    ! Calculate the xray ionisation rate
    INTEGER :: i, j, k
    REALNUM :: LX

    LX = 10.**(30.37 + 1.44*log10(gptmass/gnewton/msun))
    DO i = is, ie
      DO j = js, je
        DO k = ks, ke
          xr_array(i, j, k) = (x1b(i)/au)**(-2.2)*LX/1e30 &
                              *(zeta_1*exp(-(rin_cd(i, j, k)/(5.*sig_Xa))**alpha) &
                                + zeta_2*(exp(-(top_cd(i, j, k)/sig_Xs)**beta) &
                                          + exp(-(bottom_cd(i, j, k)/sig_Xs)**beta)))
        END DO
      END DO
    END DO
  END SUBROUTINE xrays
  !
  !=======================================================================
  !
  SUBROUTINE cosmicrays(top_cd, bottom_cd, cr_array)
    REALNUM, DIMENSION(:, :, :), INTENT(IN) :: top_cd, bottom_cd
    REALNUM, DIMENSION(:, :, :), INTENT(INOUT) :: cr_array
    REALNUM, DIMENSION(10), PARAMETER :: ckarrL = (/ -3.331056497233e6_pk, &
                                                    1.207744586503e6_pk, &
                                                    -1.913914106234e5_pk, &
                                                    1.731822350618e4_pk, &
                                                    -9.790557206178e2_pk, &
                                                    3.543830893824e1_pk, &
                                                    -8.034869454520e-1_pk, &
                                                    1.048808593086e-2_pk, &
                                                    -6.188760100997e-5_pk, &
                                                    3.122820990797e-8_pk /)
    REALNUM, DIMENSION(10), PARAMETER :: ckarrH = (/ 1.001098610761e7_pk, &
                                                    -4.231294690194e6_pk, &
                                                    7.921914432011e5_pk, &
                                                    -8.623677095423e4_pk, &
                                                    6.015889127529e3_pk, &
                                                    -2.789238383353e2_pk, &
                                                    8.595814402406e0_pk, &
                                                    -1.698029737474e-1_pk, &
                                                    1.951179287567e-3_pk, &
                                                    -9.937499546711e-6_pk /)
    REALNUM, PARAMETER :: m_p = 1.6726219e-24_pk, k_b = 1.3806485e-16_pk, &
                          mu = 2.36, N2Sig = mu*m_p
    REALNUM, PARAMETER :: crmax = 3e-17_pk
    REALNUM, PARAMETER :: cdmax = 1e27*N2Sig, cdmin = 1e19*N2Sig
    CHARACTER(LEN=*), PARAMETER :: routinename = 'z_cosmicrays'
    ! Calculates the cosmic ray ionisation rate using the model from
    ! PadovaniEtAl2018. As stated in the paper, this model is only applicable for
    ! 10^19 <= N <= 10^27 cm^-2, and hence we limit the column density to be
    ! between these values.
    REALNUM :: logztmp1, logztmp2, top_cd_tmp, bottom_cd_tmp
    INTEGER :: i, j, k, ck

    IF (cr_model == 'L') THEN
      DO i = is, ie
        DO j = js, je
          DO k = ks, ke
            logztmp1 = 0.0; logztmp2 = 0.0
            top_cd_tmp = MAX(cdmin, MIN(top_cd(i, j, k), cdmax))
            bottom_cd_tmp = MAX(cdmin, MIN(bottom_cd(i, j, k), cdmax))
            DO ck = 1, 10
              logztmp1 = logztmp1 + ckarrL(ck)*log10(top_cd_tmp/N2Sig)**(ck - 1)
              logztmp2 = logztmp2 + ckarrL(ck)*log10(bottom_cd_tmp/N2Sig)**(ck - 1)
            END DO
            cr_array(i, j, k) = MIN(10.**(logztmp1) + 10.**(logztmp2), crmax)
          END DO
        END DO
      END DO
    ELSEIF (cr_model == 'H') THEN
      DO i = is, ie
        DO j = js, je
          DO k = ks, ke
            logztmp1 = 0.0; logztmp2 = 0.0
            top_cd_tmp = MAX(cdmin, MIN(top_cd(i, j, k), cdmax))
            bottom_cd_tmp = MAX(cdmin, MIN(bottom_cd(i, j, k), cdmax))
            DO ck = 1, 10
              logztmp1 = logztmp1 + ckarrH(ck)*log10(top_cd_tmp/N2Sig)**(ck - 1)
              logztmp2 = logztmp2 + ckarrH(ck)*log10(bottom_cd_tmp/N2Sig)**(ck - 1)
            END DO
            cr_array(i, j, k) = MIN(10.**(logztmp1) + 10.**(logztmp2), crmax)
          END DO
        END DO
      END DO
    ELSE
      call finalstop(routinename, 'Error. Cr_model must be either L or H.')
    END IF
  END SUBROUTINE cosmicrays
  !
  !=======================================================================
  !
  SUBROUTINE chemistry(p, ad_eta_array, ohmic_eta_array, hall_eta_array)
    !
    INTEGER, PARAMETER :: cr_lv = 0, grow = 0
    REALNUM, PARAMETER :: m_p = 1.6726219e-24_pk, k_b = 1.3806485e-16_pk, &
                          mu = 2.36, au = 1.495978707e13_pk
    REALNUM, DIMENSION(:, :, :), INTENT(IN) :: p
    REALNUM, DIMENSION(:, :, :), INTENT(OUT) :: ad_eta_array, ohmic_eta_array, &
                                                hall_eta_array
    REALNUM :: rho, Bfield, T, eta_ad, eta_ohm, eta_hal, rho_ion, Sigma, &
               b1b, b2b, b3b, zeta_H2, zeta_anly, rinf
    INTEGER :: chem, i, j, k, ip1, jp1, kp1
    REALNUM, DIMENSION(4, 3) :: zeta_coef
    REALNUM, DIMENSION(4) :: rho_ord
    !
    !Set Initial CR ionization rate
    zeta_H2 = cr_zeta_17*1.0e-17
    zeta_anly = zeta_H2
    zeta_coef = reshape((/-16.68, -1.071e-2, 0., 0., &
                          -14.71, -3.225e-1, 1.220e-2, 1.736e-5, &
                          -12.73, -6.139e-1, 2.308e-2, 2.852e-4/), &
                        shape(zeta_coef))
    !Clear eta_array
    ad_eta_array = zro; ohmic_eta_array = zro; hall_eta_array = zro!; xhco_array=zro
    !ad_eta_array=one/(ad_gamma*d_**(three/two))
    !ohmic_eta_array=ohmic_eta_factor; hall_eta_array=hall_coefficient_0
    rinf = huge(one)
    !
    do k = ksgo, kego
      kp1 = k + kone
      do j = jsgo, jego
        jp1 = j + jone
        do i = isgo, iego
          ip1 = i + ione
          !Compute B field strength
          b1b = haf*(b1(i, j, k) + b1(ip1, j, k))
          b2b = haf*(b2(i, j, k) + b2(i, jp1, k))
          b3b = haf*(b3(i, j, k) + b3(i, j, kp1))
          Bfield = sqrt(four*pi*(b1b**2 + b2b**2 + b3b**2))
          !Set absolute density floor everywhere. Kill velocity in that cell.
          if (densityfloor_vkill == zro .and. d_(i, j, k) < (100*mu*m_p)) then
            v1(i, j, k) = zro
            v1(ip1, j, k) = zro
            if (withv2) then
              v2(i, j, k) = zro
              v2(i, jp1, k) = zro
            end if
            if (withv3) then
              v3(i, j, k) = zro
              v3(i, j, kp1) = zro
            end if
          end if
          ! Set lower limit for density greater than 100 per cm^3
          d_(i, j, k) = max(d_(i, j, k), 100*mu*m_p)
          ! Calculate the number density
          rho = d_(i, j, k)/(mu*m_p)
          ! Calculate the temperature
          if (temp_profile) then
            ! Calculate the temperature given variables temp_0 and temp_q
            T = temp_0*(x1b(i)/au)**(-1.0*temp_q)
          else
            ! Calculate the isothermal temperature from P = rho*cs^2 = rho*k_b*T/(mu*m_H)
            T = mu*m_p/k_b*p(i, j, k)/d_(i, j, k)
          end if

          !Different Attenuate Formula: only cr_lv=-1 is used
          rho_ord = (/1.0, log10(rho), (log10(rho))**2, (log10(rho))**3/)
          if (cr_lv > 0) then
            zeta_anly = 10**(sum(zeta_coef(:, cr_lv)*log10(rho_ord)))
          elseif (cr_lv == -1) then
            Sigma = 1.6231e-4*sqrt(rho*10./10.)
            zeta_anly = zeta_H2*exp(-Sigma/2./96.)
            zeta_anly = max(zeta_anly, 7.3e-19) !1.1e-22)
          elseif (cr_lv == 0) then
             zeta_anly = xr_array(i,j,k) + cr_array(i,j,k) + 1.1e-22
          end if
          !if ((T < 1.0) .or. (T > 2.0e3)) then
          !   print *, "Abnormal Temp at: (", i,",", j,",", k,")"
          !   print *, x1b(i), x2b(j)*180/pi, x3b(k)*180/pi
          !   print *, "rho=", rho, 'B=', Bfield, 'T=', T, 'Pres=', p(i,j,k)
          !end if

          ! Set temperature safety ceiling and floor
          T = max(7.0, min(T, 2.0e3))

          ! First modify the rho, T, and zeta_anly values so that they are within the
          ! bounds of the table.
          if (rho < allRec(1, 1)) rho = allRec(1, 1)
          if (rho > allRec(1, nbins)) rho = allRec(1, nbins)
          if (T < allRec(2, 1)) T = allRec(2, 1)
          if (T > allRec(2, (tbins-1)*nbins+1)) T = allRec(2, (tbins-1)*nbins+1)
          if (zeta_anly < allRec(3, 1)) zeta_anly = allRec(3, 1)
          if (zeta_anly > allRec(3, (ibins-1)*nbins*tbins+1)) zeta_anly = allRec(3, (ibins-1)*nbins*tbins+1)

          !
          chem = 0
          if (grow == 1) a_min = 5e-7*min(max(sqrt(rho/300.), 0.6), 20.)
          ! Calculate the diffusivities from the chemistry
          !If an abundance table is given, call full_network in zeus_chemistry
          if (tabulate > 0) then
            chem = full_network(rho, Bfield, T, zeta_anly, a_min, a_max, &
                                eta_ad, eta_ohm, eta_hal)
          end if
          !If no table is provided, call minimal_network
          if (tabulate == 0 .or. chem == 0) then
            chem = minimal_network(rho, Bfield, T, zeta_anly, a_min, a_max, &
                                   eta_ad, eta_ohm, eta_hal)!, xHCO)
          end if
          !Convert into zeus's eta formulation
          eta_ad = four*pi/Bfield/Bfield*abs(eta_ad)
          eta_hal = sqrt(four*pi)/Bfield*eta_hal
          !If failed, use default eta
          if (chem == 0 .or. eta_ohm /= eta_ohm .or. abs(eta_ohm) > rinf .or. &
              eta_ad /= eta_ad .or. abs(eta_ad) > rinf .or. &
              eta_hal /= eta_hal .or. abs(eta_hal) > rinf .or. eta_ad < 0) then
            write(*,*) "Default MHD Coeffs at (", i, ",", j, ",", k, ")"
            write(*,*) x1b(i), x2b(j)*180./pi, x3b(k)*180./pi
            write(*,'(3(a10,ES16.8))') "rho=", rho, ' B=', &
              Bfield, ' T=', T
            write(*,'(3(a10,ES16.8))') "Zeta_XR=", xr_array(i, j, k), &
              " Zeta_CR=", cr_array(i, j, k), " Zeta_Tot=", zeta_anly
            write(*,'(3(a10,ES16.8))') "Eta_Ohm=", eta_ohm, &
              "Eta_AD=", eta_ad, "Eta_Hall=", eta_hal

            if (tabulate > 0) then
              chem = full_network(rho, Bfield, T, zeta_anly, a_min, a_max, &
                                  eta_ad, eta_ohm, eta_hal, 1)
            end if

            rho_ion = ad_rhoi0*(d_(i, j, k)/ad_rhon0)**ad_alpha
            eta_ohm = ohmic_eta_factor/sqrt(zeta_anly/1.e-17)
            eta_hal = hall_coefficient(d_(i, j, k))/sqrt(zeta_anly/1.e-17)
            eta_ad = one/(ad_gamma*d_(i, j, k)*rho_ion)/sqrt(zeta_anly/1.e-17)
          end if

          ! Set safety ceiling & floor
          ! eta_ohm = max(1.0e11, min(eta_ohm, 1.0e21))
          ! if (ihall > zro) then
          !   eta_ohm=ohmic_eta_factor
          !   eta_hal = max(-3.0e22, min(eta_hal, 3.0e22))
          !   if (eta_ohm < ohmic_eta_factor) then
          !     eta_ohm = min(ohmic_eta_factor, &
          !                   max(eta_ohm, abs(eta_hal*Bfield/sqrt(four*pi))))
          !   end if
          ! end if
          ! if (x1b(i) < 5.e13 .and. knac /= 0) then
          !   eta_ohm = ohmic_eta_factor
          !   eta_hal = max(-3.0e20, min(eta_hal, 3.0e20))
          ! end if
          !
          ad_eta_array(i, j, k) = eta_ad
          ohmic_eta_array(i, j, k) = eta_ohm
          hall_eta_array(i, j, k) = eta_hal
          !xhco_array(i,j,k)=xHCO
        end do
      end do
    end do
    !
    call boundary('etas')
    call boundary('d_')
    if (densityfloor_vkill >= zro) then
      call boundary('v1')
      call boundary('v2')
      call boundary('v3')
    end if
    !
  END SUBROUTINE chemistry
  !
  SUBROUTINE print_chemvals
    INTEGER :: i
    if (ntimestep == 0 .or. MOD(ntimestep, 100) == 0) then
      if (withchem) then
        write(*,*) ' EtaA'
        write(*,'(100ES20.12)') ad_eta_array(14, jsg:js+5, ks)
        do i = 0, 8
          write(*,'(ES20.12)', advance='no') ad_eta_array(14, jeg-i, ks)
        end do
        write(*,'(a2)') ''
        do i = 0, 8
          write(*,'(ES20.12)', advance='no') abs(ad_eta_array(14, jsg + i, ks) - &
                                                 ad_eta_array(14, jeg-i, ks)) / &
                                             ad_eta_array(14, jsg + i, ks)
        end do
        write(*,'(a2)') ''
        write(*,'(a2)') ''
      end if
      write(*,*) ' Rho'
      write(*,'(100ES20.12)') d_(14, jsg:js+5, ks)
      do i = 0, 8
        write(*,'(ES20.12)', advance='no') d_(14, jeg-i, ks)
      end do
      write(*,'(a2)') ''
      do i = 0, 8
        write(*,'(ES20.12)', advance='no') abs(d_(14, jsg + i, ks) - &
                                               d_(14, jeg-i, ks)) / &
                                           d_(14, jsg + i, ks)
      end do
      write(*,'(a2)') ''
      write(*,*) ' V1'
      write(*,'(100ES20.12)') v1(14, jsg:js+5, ks)
      do i = 0, 8
        write(*,'(ES20.12)', advance='no') v1(14, jeg-i, ks)
      end do
      write(*,'(a2)') ''
      do i = 0, 8
        write(*,'(ES20.12)', advance='no') abs(v1(14, jsg + i, ks) - &
                                               v1(14, jeg-i, ks)) / &
                                           v1(14, jsg + i, ks)
      end do
      write(*,'(a2)') ''
      write(*,*) ' V2'
      write(*,'(100ES20.12)') v2(14, jsg:js+5, ks)
      write(*,'(ES20.12)', advance='no') 0.
      do i = 0, 7
        write(*,'(ES20.12)', advance='no') v2(14, jeg-i, ks)
      end do
      write(*,'(a2)') ''
      write(*,'(ES20.12)', advance='no') 0.
      do i = 0, 7
        write(*,'(ES20.12)', advance='no') abs(v2(14, jsg + i + 1, ks) + &
                                               v2(14, jeg - i, ks)) / &
                                           v2(14, jsg + i + 1, ks)
      end do
      write(*,'(a2)') ''
      write(*,*) ' V3'
      write(*,'(100ES20.12)') v3(14, jsg:js+5, ks)
      do i = 0, 8
        write(*,'(ES20.12)', advance='no') v3(14, jeg-i, ks)
      end do
      write(*,'(a2)') ''
      do i = 0, 8
        write(*,'(ES20.12)', advance='no') abs(v3(14, jsg + i, ks) - &
                                               v3(14, jeg-i, ks)) / &
                                           v3(14, jsg + i, ks)
      end do
      write(*,'(a2)') ''
      write(*,*) ' B1'
      write(*,'(100ES20.12)') b1(14, jsg:js+5, ks)
      do i = 0, 8
        write(*,'(ES20.12)', advance='no') b1(14, jeg-i, ks)*-1.
      end do
      write(*,'(a2)') ''
      do i = 0, 8
        write(*,'(ES20.12)', advance='no') abs(b1(14, jsg + i, ks) + &
                                               b1(14, jeg-i, ks)) / &
                                           b1(14, jsg + i, ks)
      end do
      write(*,'(a2)') ''
      write(*,*) ' B2'
      write(*,'(100ES20.12)') b2(14, jsg:js+5, ks)
      write(*,'(ES20.12)', advance='no') 0.
      do i = 0, 7
        write(*,'(ES20.12)', advance='no') b2(14, jeg-i, ks)
      end do
      write(*,'(a2)') ''
      write(*,'(ES20.12)', advance='no') 0.
      do i = 0, 7
        write(*,'(ES20.12)', advance='no') abs(b2(14, jsg + i + 1, ks) - &
                                               b2(14, jeg - i, ks)) / &
                                           b2(14, jsg + i + 1, ks)
      end do
      write(*,'(a2)') ''
      write(*,*) ' B3'
      write(*,'(100ES20.12)') b3(14, jsg:js+5, ks)
      do i = 0, 8
        write(*,'(ES20.12)', advance='no') b3(14, jeg-i, ks)
      end do
      write(*,'(a2)') ''
      do i = 0, 8
        write(*,'(ES20.12)', advance='no') abs(b3(14, jsg + i, ks) - &
                                               b3(14, jeg-i, ks)) / &
                                           b3(14, jsg + i, ks)
      end do
      write(*,'(a2)') ''
      write(*,*) ''
      if (withchem) then
        write(*,*) ' CR'
        write(*,'(100ES20.12)') cr_array(14, jsg:js+5, ks)
        do i = 0, 8
          write(*,'(ES20.12)', advance='no') cr_array(14, jeg-i, ks)
        end do
        write(*,'(a2)') ''
        do i = 0, 8
          write(*,'(ES20.12)', advance='no') abs(cr_array(14, jsg + i, ks) - &
                                                 cr_array(14, jeg-i, ks)) / &
                                             cr_array(14, jsg + i, ks)
        end do
        write(*,'(a2)') ''
        write(*,*) ' XR'
        write(*,'(100ES20.12)') xr_array(14, jsg:js+5, ks)
        do i = 0, 8
          write(*,'(ES20.12)', advance='no') xr_array(14, jeg-i, ks)
        end do
        write(*,'(a2)') ''
        do i = 0, 8
          write(*,'(ES20.12)', advance='no') abs(xr_array(14, jsg + i, ks) - &
                                                 xr_array(14, jeg-i, ks)) / &
                                             xr_array(14, jsg + i, ks)
        end do
        write(*,'(a2)') ''
        write(*,*) ''
      end if
    end if
  END SUBROUTINE print_chemvals
END MODULE zeus_physics
