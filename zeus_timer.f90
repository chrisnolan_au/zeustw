#include "macros"
MODULE zeus_timer
  USE zeus_variables
  USE zeus_io
  IMPLICIT NONE
  SAVE

  PRIVATE
  PUBLIC :: timer_start, timer_loopstart, timer_end, timer_step, timer_check

  REALNUM :: cpu_start, wc_start

  INTEGER ::      nzcount
  REALNUM ::      zcspeak
  REALNUM ::      wc1, wc2

CONTAINS
  !** GROUP: TIMERS ******************************************************
  !**                                                                   **
  !**                   G R O U P :   T I M E R S                       **
  !**                                                                   **
  !***********************************************************************
  !
  !=======================================================================
  !
  SUBROUTINE timer_start
    CHARACTER(LEN=*), PARAMETER :: routinename = 'z_timer_start'
    !-----------------------------------------------------------------------
    !
    !      Initialize cpu and wall clocks.  "cputot" will be the total cpu
    !  time (in seconds) consumed by this job.  "wctot" will be the total
    !  elapsed wall clock time (in seconds) since the job began.
    !
    call CPU_TIME(cpu_start) !RFK: Consider using MPI_WTIME() for MPI jobs
    wc_start = seconds()
    zcspeak = zro
    nzcount = 0
  END SUBROUTINE timer_start
  !
  !=======================================================================
  !
  SUBROUTINE timer_loopstart
    !call CPU_TIME(cpu1)   !RFK: Consider using MPI_WTIME() for MPI jobs
    wc1 = seconds()
  END SUBROUTINE timer_loopstart
  !
  !=======================================================================
  !
  SUBROUTINE timer_end
    CHARACTER(LEN=*), PARAMETER :: routinename = 'z_timer_end'
    CHARACTER(LEN=messagelinelength) message
    REALNUM :: cputot, wctot
    REALNUM, PARAMETER :: hundred = 100.0_pk
    REALNUM :: ptime, zcseff, zcsact
    !
    !      Final wall clock time and CPU usage, expressed in hours, minutes,
    !      and seconds.
    !
    !      Results may be unreliable if the run took longer than one day,
    !      or for the MPI case.
    !
    call CPU_TIME(cputot) !RFK: Consider using MPI_WTIME() for MPI jobs
    wctot = seconds()

    cputot = cputot - cpu_start
    wctot = wctot - wc_start
    !
    !      Percent of real time that job was serviced.
    !
    if (wctot <= zro) wctot = max(cputot, verysmall)
    ptime = hundred*cputot/wctot
    !
    !      Determine number of zone-cycles per second.
    !      The product of the number of zones and the number of timesteps can be large,
    !      and (if done in integers) it could overflow.
    !      That's why the computation is done here in floating point.
    !
    zcseff = real(iea - isa + 1, pk)*real(jea - jsa + 1, pk) &
             *real(kea - ksa + 1, pk)*real(nzcount, pk)/wctot
    zcsact = real(ie - is + 1, pk)*real(je - js + 1, pk) &
             *real(ke - ks + 1, pk)*real(nzcount, pk)/wctot
    !Older: zcsact = real(inac*jnac*knac*nzcount, pk) / clockused
    !
    write (message, "('ZEUS-TW : Peak      zone-cycles per second for this &
         &job = ',1p,e12.5)") zcspeak
    call threeoutput(routinename, message)
    write (message, "('ZEUS-TW : Effective zone-cycles per second for this &
         &job = ',1p,e12.5)") zcseff
    call threeoutput(routinename, message)
    write (message, "('ZEUS-TW : Active    zone-cycles per second for this &
         &job = ',1p,e12.5)") zcsact
    call threeoutput(routinename, message)
    write (message, "('ZEUS-TW : Wall clock time = ', f12.2, &
         &', CPU time = ', f12.2, ' (',f6.2,'%)')") &
         wctot, cputot, ptime
    call threeoutput(routinename, message)
    !
    !      Final messages to crt and log.
    !
    if (nwarn + hotzonewarn == 0) &
      write (message, '(a)') &
      'ZEUS-TW : Execution terminating with no warnings issued.'
    if (nwarn + hotzonewarn == 1) &
      write (message, '(a)') &
      'ZEUS-TW : Execution terminating with one warning issued.'
    if (nwarn + hotzonewarn > 1) &
      write (message, '(a,i2,a)') &
      'ZEUS-TW : Execution terminating with ', nwarn + hotzonewarn, &
      ' warnings issued.'
    call threeoutput(routinename, message)
  END SUBROUTINE timer_end
  !
  !=======================================================================
  !
  SUBROUTINE timer_check(clockused)
    !
    !call CPU_TIME(clockused) !RFK: Consider using MPI_WTIME() for MPI jobs
    !clockused=clockused-cpu_start ! Consider using authentic wallclock
    REALNUM, INTENT(OUT) :: clockused
    clockused = seconds() - wc_start
  END SUBROUTINE timer_check
  !
  !=======================================================================
  !
  SUBROUTINE timer_step
    wc2 = wc1
    !call CPU_TIME(cpu1)   !RFK: Consider using MPI_WTIME() for MPI jobs
    !cpu2=cpu1-cpu2        !Consider using authentic wallclock
    wc1 = seconds()
    wc2 = wc1 - wc2
    if (wc2 <= zro) wc2 = -one
    zcspeak = max(zcspeak, &
                  real(iea - isa + 1, pk) &
                  *real(jea - jsa + 1, pk)*real(kea - ksa + 1, pk)/wc2)
    nzcount = nzcount + 1    ! Extensible grids not allowed any more
  END SUBROUTINE timer_step
  !
  !=======================================================================
  !
  REALNUM FUNCTION seconds()
    ! Finds the number of seconds since 1 Jan 2000
    IMPLICIT NONE
    CHARACTER(LEN=8) wcdate
    CHARACTER(LEN=10) wctime
    CHARACTER(LEN=5) wczone
    INTEGER, DIMENSION(8) :: wcvalues
    INTEGER :: year, month, day, timezone_min, &
               hour, minute, second, millisecond
    REALNUM, PARAMETER :: secondsinthehour = 3600.0_pk, &
                          secondsintheminute = 60.0_pk, hoursintheday = 24.0_pk

    call DATE_AND_TIME(wcdate, wctime, wczone, wcvalues)

    year = wcvalues(1)
    month = wcvalues(2)
    day = wcvalues(3)
    timezone_min = wcvalues(4)
    hour = wcvalues(5)
    minute = wcvalues(6)
    second = wcvalues(7)
    millisecond = wcvalues(8)

    day = julday(month, day, year) - julday(1, 1, 2000)
    seconds = (thousandth*millisecond) &
              + second &
              + secondsintheminute*(minute + timezone_min) &
              + secondsinthehour*(hour + hoursintheday*day)
  CONTAINS
    !
    !=======================================================================
    !
    INTEGER FUNCTION julday(month, day, year)
      ! From the Numerical Recipes
      IMPLICIT NONE
      INTEGER, INTENT(IN) :: month, day, year
      !
      CHARACTER(LEN=*), PARAMETER :: routinename = 'z_julday'
      !
      INTEGER, PARAMETER :: igreg = 15 + 31*(10 + 12*1582) !Gregorian Calendar starts
      INTEGER            :: ja, jm, jy

      jy = year

      if (jy == 0) then
        call finalstop(routinename, 'There is no year zero')
      end if

      if (jy < 0) jy = jy + 1  !The year after 1BCE is 1CE

      if (month > 2) then
        jm = month + 1
      else
        jy = jy - 1
        jm = month + 13
      end if

      julday = int(365.25_pk*jy) + int(30.6001_pk*jm) + day + 1720995

      if (day + 31*(month + 12*year) >= igreg) then
        ja = int(0.01_pk*jy)
        julday = julday + 2 - ja + int(0.25_pk*ja)
      end if
    END FUNCTION julday
  END FUNCTION seconds
END MODULE zeus_timer
