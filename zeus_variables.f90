#include "macros"
MODULE zeus_variables

  USE zeus_variables_fixed_hardwired
  USE zeus_variables_fixed_namelists
  USE zeus_variables_fixed_other
  USE zeus_variables_grid
  USE zeus_variables_changing
  IMPLICIT NONE
  PUBLIC
  SAVE

END MODULE zeus_variables
