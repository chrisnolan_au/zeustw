#include "macros"
MODULE zeus_variables_changing
  USE zeus_variables_fixed_hardwired
  IMPLICIT NONE
  PUBLIC
  SAVE

  !=============================================================
  ! Global variables that can potentially change at any timestep

  !- - - - - - - - - - - - - - -
  ! The fields: density, velocity, internal energy, gravitational potential
  !             magnetic field.
  REALNUM, ALLOCATABLE, DIMENSION(:, :, :) :: &
    e_, b1, b2, b3

  REALNUM, ALLOCATABLE, DIMENSION(:, :, :), TARGET :: &
    d_, gp, v1, v2, v3

  REALNUM, ALLOCATABLE, DIMENSION(:, :, :, :) :: &
    tracerfields

  ! Extended fields
  REALNUM, POINTER, DIMENSION(:, :, :) :: &
    d_e, gp_e

  REALNUM, POINTER, DIMENSION(:, :, :) :: &
    dfl

  REALNUM, DIMENSION(:), ALLOCATABLE :: eta_ra, eta_rb

  REALNUM, DIMENSION(:, :, :), ALLOCATABLE :: &
    ad_eta_array, ohmic_eta_array, hall_eta_array, xr_array, cr_array

  REALNUM, DIMENSION(:, :), ALLOCATABLE :: allRec

  REALNUM :: centralmass = zro
  REALNUM :: mass_x1_inner = zro, mass_x1_inner_loss = zro, &
             mass_x1_inner_gain = zro
  REALNUM :: mass_x1_outer = zro, mass_x1_outer_loss = zro, &
             mass_x1_outer_gain = zro
  REALNUM :: mass_gain_alfven = zro
  REALNUM :: mass_gain_rhofloor = zro

  !- - - - - - - - - - - - - - -
  ! Problem termination variables
  INTEGER :: terminate_flag = 0 ! When set to 1, it ends the main loop
  INTEGER :: nwarn = 0          ! No warnings yet
  INTEGER :: hotzonewarn = 0    ! No warnings yet

  !- - - - - - - - - - - - - - -
  ! Timestep variables
  REALNUM :: time, dt, dtohm
  INTEGER :: nsubcycles_ohm

  !- - - - - - - - - - - - - - -
  ! Dump variables
  INTEGER :: dump_switch = 0
  INTEGER :: ndump = -1 ! A fanciful token value
  REALNUM :: tdump = zro

  INTEGER :: clockdump_n = 1
  REALNUM :: clockdump_t = zro

  !- - - - - - - - - - - - - - -
  ! Hydrodynamic control variables
  INTEGER :: ix1x2x3

  !- - - - - - - - - - - - - - -
  ! Timestep name, in integer and character form.  Character form fixed
  ! to eight characters, which can be limiting.

  INTEGER :: ntimestep = -1 ! A fanciful token value
  CHARACTER(LEN=8) :: &
    ntimestepname = "a2345678"  ! A fanciful token value

END MODULE zeus_variables_changing
