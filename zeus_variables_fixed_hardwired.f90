#include "macros"
MODULE zeus_variables_fixed_hardwired
  IMPLICIT NONE
  SAVE

  !=============================================================
  ! This module contains only variables that are hardwired into the
  ! code.  Some might have to be changed for portability or to change
  ! numerical precision.  Otherwise, no change is expected.

  !=============================================================
  ! I/O Logical Unit names.
  ! They must be collected here to prevent unit-name collisions
  ! There should be a way to do it without the need for global infomration.

  INTEGER, PARAMETER :: ioin = 65              ! Input namelists
  INTEGER, PARAMETER :: io_output = 66         ! Used to output text files
  INTEGER, PARAMETER :: ioin_inputruntime = 67 ! Runtime input
  INTEGER, PARAMETER :: ioin_newgrid = 68      ! Reserved, but not used
  INTEGER, PARAMETER :: ioin_tslice = 69      ! Reserved, but not used
  INTEGER, PARAMETER :: io_binary = 70         ! Used to output binary files
  INTEGER, PARAMETER :: io_input = 71          ! Used to input other text files
  INTEGER, PARAMETER :: IOTTY_DEFAULT = 6     ! Special value, stdout
  INTEGER, PARAMETER :: IOLOG_DEFAULT = 30    ! Used for log output
  INTEGER, PARAMETER :: IOTSL_DEFAULT = 34    ! Reserved, but not used
  INTEGER, PARAMETER :: IOWARN_DEFAULT = 35    ! Used for warn output
  INTEGER, PARAMETER :: NO_OUTPUT = -1         ! Special value for no output

  !=============================================================
  ! This part of the module contains only fixed floating point constants
  ! Some care about real KIND must be taken, here, and at compilation time.

  ! The test variable has a kind defined by the REALNUM macro.
  REALNUM, PRIVATE :: testvariable
  INTEGER, PARAMETER :: pk = KIND(testvariable)

  REALNUM, PARAMETER :: zro = 0.0_pk
  REALNUM, PARAMETER :: haf = 0.5_pk, one = 1.0_pk, two = 2.0_pk
  REALNUM, PARAMETER :: three = 3.0_pk, four = 4.0_pk, five = 5.0_pk
  REALNUM, PARAMETER :: eight = 8.0_pk, ten = 10.0_pk, sixteen = 16.0_pk
  REALNUM, PARAMETER :: third = one/three
  REALNUM, PARAMETER :: quarter = one/four
  REALNUM, PARAMETER :: eighth = one/eight, sixteenth = one/sixteen
  REALNUM, PARAMETER :: hundredth = 1.0e-2_pk, thousandth = 1.0e-3_pk
  REALNUM, PARAMETER :: millionth = 1.0e-6_pk

  REALNUM, PARAMETER :: max_dt_increase = 1.26_pk
  REALNUM, PARAMETER :: pi = 3.1415926535897932384626433832795029_pk

  REALNUM, PARAMETER :: degree = pi/180.0_pk

  !=============================================================
  ! Index of the lower boundaries for arrays.  Hardwired to 1,
  ! coinciding with the default value
  ! This is done because Fortran90 does not pass lower boundaries
  ! for arrays, and so the only option outside of accepting the
  ! default is fixing the lower bounds on every array.
  ! This is easily doable, but the case when array happens to be not
  ! allocated would create problems.
  INTEGER, PARAMETER, PRIVATE :: lowerbound = 1
  INTEGER, PARAMETER :: isa = lowerbound, jsa = lowerbound, ksa = lowerbound
  INTEGER, PARAMETER :: isap = lowerbound, jsap = lowerbound, ksap = lowerbound
  INTEGER, PARAMETER :: ijknsa = lowerbound

  !=============================================================
  ! The following constants may depend on the real datatype chosen,
  ! Maybe it is a good idea to calculate some of these quantities at
  ! compile time using the TINY and HUGE intrinsics; it is not done
  ! here for compatibility with previous versions of the code.

#ifdef DOUBLEP
  REALNUM, PARAMETER    :: verysmall = 1.0e-99_pk, verylarge = 1.0e+99_pk
  INTEGER, PARAMETER :: isig = 9
#else /* DOUBLEP */
  REALNUM, PARAMETER    :: verysmall = 1.0e-32_pk, verylarge = 1.0e+32_pk
  INTEGER, PARAMETER :: isig = 5
#endif /* DOUBLEP */

  !=============================================================
  ! Hardwired length for character strings used to store filenames
  ! and many of the filename portions
  INTEGER, PARAMETER          :: filenamelength = 100

  !=============================================================
  ! Hardwired length used for many I/O message lines
  INTEGER, PARAMETER          :: messagelinelength = 800  !80

END MODULE zeus_variables_fixed_hardwired
