#include "macros"
MODULE zeus_variables_fixed_namelists
  USE zeus_variables_fixed_hardwired
  IMPLICIT NONE
  SAVE

  !=============================================================
  ! This module is designed to contain exclusively namelist
  ! variables that do not change during regular execution.
  ! after initialization is complete.
  !

  !- - - - - - - - - - - - - - -
  !I/O control

  ! Set to IOTTY_DEFAULT to enable tty output, to NO_OUTPUT to disable it.
  namelist_variable(iocon, INTEGER, iotty, IOTTY_DEFAULT)

  ! Set to IOLOG_DEFAULT to enable log output, to NO_OUTPUT to disable it.
  namelist_variable(iocon, INTEGER, iolog, IOLOG_DEFAULT)

  ! Set to IOLOG_DEFAULT to enable log output, to NO_OUTPUT to disable it.
  namelist_variable(iocon, INTEGER, iowarn, IOWARN_DEFAULT)

  ! Debug levels currently from 0 to 4 in the main Zeus code. (2004-02-19)
  namelist_variable(iocon, INTEGER, debug, 0)
  namelist_variable(iocon, INTEGER, ntimestepdebug, -1)

  ! Frequency of runtime input check
  namelist_variable(iocon, INTEGER, iocheckfrequency, 200)

  ! Frequency of sanity check
  namelist_variable(iocon, INTEGER, sanitycheckfrequency, 1)

  ! Frequency of the timestep information dump
  namelist_variable(iocon, INTEGER, iotimestepfrequency, 200)

  namelist_variable(iocon, CHARACTER(LEN=4), time_unit, '')
  namelist_variable(iocon, REALNUM, time_factor, -one)

  !- - - - - - - - - - - - - - -
  !Namelist gridcon
  ! Grid control

  ! Number of tiles on each of the three directions
  ! Set to 1 for serial computing
  namelist_variable(gridcon, INTEGER, nnodes1, 1)
  namelist_variable(gridcon, INTEGER, nnodes2, 1)
  namelist_variable(gridcon, INTEGER, nnodes3, 1)

  !Geometry control.  Values: xyz, zrp, rtp, rmp
  namelist_variable(gridcon, CHARACTER(LEN=3), geometry, 'xyz')

  !Set to true for periodic BCs if there is more than one
  !processor in the given direction.
  namelist_variable(gridcon, LOGICAL, nodecyc1, .false.)
  namelist_variable(gridcon, LOGICAL, nodecyc2, .false.)
  namelist_variable(gridcon, LOGICAL, nodecyc3, .false.)

  !Set to nonzero if a grid in rtp geometry has axial zones that
  !need to be slightly shifted by axisshift degrees from 0 and pi.
  namelist_variable(gridcon, REALNUM, axisshift, zro)

  !- - - - - - - - - - - - - - -
  !Namelist parcon
  !Variables controlling physics and algorithm

  !Physics

  !Control existence of certain field components
  namelist_variable(parcon, LOGICAL, withv2, .true.)
  namelist_variable(parcon, LOGICAL, withv3, .true.)
  namelist_variable(parcon, LOGICAL, withbp, .true.)
  namelist_variable(parcon, LOGICAL, withb3, .true.)
  namelist_variable(parcon, INTEGER, number_of_tracer_fields, 0)

  !MHD support.  Values: 0: no mhd; 1: EMF ; 2: MOC ; 3: HSMOC
  namelist_variable(parcon, INTEGER, imhd, 3)
  namelist_variable(parcon, REALNUM, alfven_dt_floor, zro)
  namelist_variable(parcon, LOGICAL, withchem, .false.)
  namelist_variable(parcon, INTEGER, interpchem, 0)
  namelist_variable(parcon, INTEGER, tabulate, 0)
  namelist_variable(parcon, INTEGER, gbins, 20)
  namelist_variable(parcon, INTEGER, numIons, 32)
  namelist_variable(parcon, INTEGER, nbins, 131)
  namelist_variable(parcon, INTEGER, tbins, 31)
  namelist_variable(parcon, INTEGER, ibins, 10)
  namelist_variable(parcon, REALNUM, PAH, 0.0)
  namelist_variable(parcon, CHARACTER(LEN=1), cr_model, 'L')
  namelist_variable(parcon, CHARACTER(LEN=80), tablename, '')
  namelist_variable(parcon, LOGICAL, temp_profile, .false.)
  namelist_variable(parcon, REALNUM, temp_0, 280.0)
  namelist_variable(parcon, REALNUM, temp_q, 0.5)

  ! Turn on the force limiter, inspired by Miller&Stone 2000, Tony code,
  ! and Rick code about clim and clim2
  namelist_variable(parcon, LOGICAL, alfven_dt_floor_forcelimiter, .false.)

  namelist_variable(parcon, LOGICAL, alfven_dt_floor_conservative, .false.)

  ! alfven_dt_floor_vkill: negative: no vkill.
  ! positive: conserve rho*v**alfven_dt_floor_vkill (one would be equivalent
  ! to conserving momentum)
  ! zero: send v components to zero.
  namelist_variable(parcon, REALNUM, alfven_dt_floor_vkill, -one)

  !MHD algorithm.  Values: 3: Zeus3D; 2: Zeus2D (triggers knac=0)
  namelist_variable(parcon, INTEGER, mhddimension, 3)
  !Hydro sweep order.
  ! Values: 3: Zeus3D sweep; 2: Zeus2D sweep (triggers knac=0)
  namelist_variable(parcon, INTEGER, sweepdimension, 3)

  ! Turns on the monitoring of floor masses.
  namelist_variable(parcon, LOGICAL, withfloormass, .true.)
  ! Turns on the monitoring of mass across the x1 boundaries.
  ! If withcentralmass is on, this one is also forced to be on.
  namelist_variable(parcon, LOGICAL, withboundarymass, .true.)

  ! Flag for BC conditions for b3.
  ! Set to true to activate them after the B field update using
  ! the 3D algorithm.
  ! Normally it should be used *only* if knac is set to zero:
  ! Otherwise, magnetic monopoles could form.  A warning is produced.
  namelist_variable(parcon, LOGICAL, withb3bc, .false.)

  ! Flags for BC conditions for EMF and VI
  namelist_variable(parcon, LOGICAL, with_bc_emf_threeroutines, .true.)
  namelist_variable(parcon, LOGICAL, with_bc_emf_dx_oneroutine, .false.)
  namelist_variable(parcon, LOGICAL, with_bc_emf_oneroutine, .false.)
  namelist_variable(parcon, LOGICAL, with_bc_vd, .true.)
  namelist_variable(parcon, LOGICAL, with_bc_vi, .false.)

  ! Number of active zones near the axis where drift is set to zero.
  ! If the value is set to <=0, it is inoperative.
  ! However, vd2 has the drift set to zero on the axis anyway, consistent
  ! with an axial wall symmetry
  !namelist_variable(parcon, INTEGER, vd1_axiszero, 0)
  !namelist_variable(parcon, INTEGER, vd2_axiszero, 0)
  !namelist_variable(parcon, INTEGER, vd3_axiszero, 0)

  namelist_variable(parcon, INTEGER, vd_axiszero_start, 1)
  namelist_variable(parcon, INTEGER, vd_axiszero_end, 1)

  !namelist_variable(parcon, INTEGER, vd1_axiszero_start, 0)
  !namelist_variable(parcon, INTEGER, vd1_axiszero_end, 0)
  !namelist_variable(parcon, INTEGER, vd2_axiszero_start, 0)
  !namelist_variable(parcon, INTEGER, vd2_axiszero_end, 0)
  !namelist_variable(parcon, INTEGER, vd3_axiszero_start, 0)
  !namelist_variable(parcon, INTEGER, vd3_axiszero_end, 0)

  namelist_variable(parcon, REALNUM, cr_zeta_17, one)
  namelist_variable(parcon, REALNUM, a_min, 1.0e-6)
  namelist_variable(parcon, REALNUM, a_max, 2.5e-5)

  !Ohmic term.   Values: 0: no Ohmic; 1: unsplit; 2:unsplit (but split BC); 3: split
  namelist_variable(parcon, INTEGER, iohmic, 0)
  !Ohmic resistivity factor
  namelist_variable(parcon, REALNUM, ohmic_eta_factor, zro)
  !Choice of Ohmic style (exponent is a misnomer)
  namelist_variable(parcon, INTEGER, ohmic_exponent, 0)
  ! 0: eta uniform
  ! 1: eta \propto rho
  ! 200: eta(r)
  ! 100: eta \propto rho, capped
  namelist_variable(parcon, REALNUM, ohmic_rho_transition, one)

  !Ohmic time treatment  Values: 1: no subcycling
  !  2: subcycling only the Ohmic term (compatible only with a split treatment)
  !  3: Ohmic-subcycling all of CT (unimplemented as of 2009-06-03)
  namelist_variable(parcon, INTEGER, iohmic_cycle, 1)
  namelist_variable(parcon, INTEGER, max_ohmic_steps, 1)
  namelist_variable(parcon, REALNUM, courno_ohmic, one)
  namelist_variable(parcon, REALNUM, ohm_dt_floor, zro)

  namelist_variable(parcon, LOGICAL, with_ad_1f, .false.)
  namelist_variable(parcon, REALNUM, courno_ad_1f, one)
  namelist_variable(parcon, REALNUM, ad_rhoi0, one)
  namelist_variable(parcon, REALNUM, ad_rhon0, one)
  namelist_variable(parcon, REALNUM, ad_alpha, one)
  namelist_variable(parcon, REALNUM, ad_gamma, one)
  namelist_variable(parcon, LOGICAL, with_ad_1f_vion_force, .true.)
  namelist_variable(parcon, REALNUM, ad_dt_floor, zro)

  !Hall term.   Values: 0:no Hall; 1:drift, unsplit;
  !                     2:emf, split; 3 drift, split
  namelist_variable(parcon, INTEGER, ihall, 0)
  namelist_variable(parcon, REALNUM, hall_coefficient_0, zro)
  namelist_variable(parcon, REALNUM, hall_coefficient_1, zro)
  namelist_variable(parcon, REALNUM, hall_rhoreference, one)
  namelist_variable(parcon, REALNUM, hall_alpha, haf)
  ! Expected number of Hall cycles.  Set to zero for no subcycling
  namelist_variable(parcon, INTEGER, hall_number_of_cycles, 100)
  ! Maximum number of Hall cycles.  Provokes a crash if exceeded
  namelist_variable(parcon, INTEGER, hall_max_number_of_cycles, 10000)
  namelist_variable(parcon, REALNUM, courno_hall, one)
  namelist_variable(parcon, REALNUM, hall_dt_floor, zro)
  namelist_variable(parcon, REALNUM, hall_smoothing, zro)

  ! MHD BC treatment
  ! Many values: 0, 1, 3, 11, 13, 14, and many others.
  !  Used to control details of the magnetic field update,
  !  especially in the boundary regions, and for b3 in zrp geometry.
  !  See usage in subroutine CT.
  namelist_variable(parcon, INTEGER, ctboxtype, 0)

  ! EQOS field support
  ! Values: 0: no e_ array, isothermal EQOS
  !        -1: no e_ array, barotropic EQOS, or based on a tracer field
  !         1: e_ array evolves in the usual way
  !         2: e_ array is fixed or evolves in some user-defined way,
  !            and it is not necessarily equal to the internal energy
  !            The main code takes no responsibilities for its parallelization
  !            and BC; that responsibility lies on the user.
  ! Check for existence of e_ array: ienergy>0
  ! Check for inexistence of e_ array: ienergy<=0

  namelist_variable(parcon, INTEGER, ienergy, 1)

  !- - - - - - - - - - - - - - -
  ! Restart control (namelist rescon)

  ! Restart run flag:
  ! 0: run starts from scratch, no restart
  ! 1: run restarts from a previous output, seamlessly
  ! >1: run restarts from a previous output, with some modifications
  ! See file zeus_initial.f90 for details

  namelist_variable(rescon, INTEGER, irestart, 0)
  namelist_variable(rescon, CHARACTER(LEN=filenamelength), restartprefix, 'z_arrayout_')
  namelist_variable(rescon, CHARACTER(LEN=filenamelength), restartsuffix, '_00000000')
  namelist_variable(rescon, REALNUM, time_initial, zro)

  !- - - - - - - - - - - - - - -
  ! Problem control variables (namelist pcon)
  !
  !  nlim          cycles to run
  !  tlim          physical (problem) time to stop calculation
  !  clocktotal    total seconds of execution time permitted for job
  !  clocksave     seconds of execution (cpu) time reserved for cleanup

  namelist_variable(pcon, INTEGER, nlim, huge(nlim))
  namelist_variable(pcon, REALNUM, tlim, zro)
  namelist_variable(pcon, REALNUM, clocktotal, 86400.0_pk)
  namelist_variable(pcon, REALNUM, clocksave, 1800.0_pk)

  ! Maximum number of hot zone warnings
  namelist_variable(pcon, INTEGER, hotzonemax, 20)
  ! Reset the hot zone counter in case the hot zone situation improves
  namelist_variable(pcon, LOGICAL, hotzonereset, .false.)

  !- - - - - - - - - - - - - - -
  ! Hydrodynamic control variables (namelist hycon)
  !
  !Artificial viscosity control:
  !  qcon      quadratic artificial viscosity (q) constant
  !  qlin      linear    artificial viscosity (q) constant
  !
  !Timestep control:
  !  courno    Courant number
  !  dtminrat  ratio of nominal initial dt to dtmin
  !            (used to compute dtmin)
  !  dtratini  ratio of nominal initial dt to effective initial dt
  !
  !Field floor control:
  !  efloor    Used in subroutine PRESSURE; it is added to the
  !            internal energy, effectively setting a minimal pressure
  !  dfloor    Similar to efloor, used for isothermal problems.
  !
  !Advection order and steepener control
  !  iord**    order of advection scheme to be used for variable ** (1, 2
  !            or 3) (iord will set them all to same value if desired)
  !  istp**    contact discontinuity steepener for third order advection.
  !            Steepens variable ** when istp** = 1 (istp will set them
  !            all to same value if desired).
  !

  !  densityfloor     An authentic density floor
  !  withdensityfloor Flag for its usage

  ! Numerical viscosity control
  namelist_variable(hycon, REALNUM, qcon, two)
  namelist_variable(hycon, REALNUM, qlin, zro)

  ! Timestep size control
  namelist_variable(hycon, REALNUM, courno, haf)
  namelist_variable(hycon, REALNUM, dtminrat, five*thousandth)
  namelist_variable(hycon, REALNUM, dtratini, hundredth)

  ! Interpolation orders and presence of PPA steepeners
  namelist_variable(hycon, INTEGER, iord, 2)
  namelist_variable(hycon, INTEGER, iordd_, 2)
  namelist_variable(hycon, INTEGER, iorde_, 2)
  namelist_variable(hycon, INTEGER, iords1, 2)
  namelist_variable(hycon, INTEGER, iords2, 2)
  namelist_variable(hycon, INTEGER, iords3, 2)
  namelist_variable(hycon, INTEGER, iordb1, 2)
  namelist_variable(hycon, INTEGER, iordb2, 2)
  namelist_variable(hycon, INTEGER, iordb3, 2)
  namelist_variable(hycon, INTEGER, istp, 0)
  namelist_variable(hycon, INTEGER, istpd_, -1)
  namelist_variable(hycon, INTEGER, istpe_, -1)
  namelist_variable(hycon, INTEGER, istps1, -1)
  namelist_variable(hycon, INTEGER, istps2, -1)
  namelist_variable(hycon, INTEGER, istps3, -1)
  namelist_variable(hycon, INTEGER, istpb1, -1)
  namelist_variable(hycon, INTEGER, istpb2, -1)
  namelist_variable(hycon, INTEGER, istpb3, -1)

  ! Floor values, used to calculate pressures
  namelist_variable(hycon, REALNUM, dfloor, zro)
  namelist_variable(hycon, REALNUM, efloor, zro)

  ! Authentic floor values

  namelist_variable(hycon, REALNUM, densityfloor, zro)
  namelist_variable(hycon, LOGICAL, withdensityfloor, .false.)

  ! Locations where the floor is applied (defaults to everywhere)
  namelist_variable(hycon, REALNUM, densityfloor_x1_min, -huge(zro))
  namelist_variable(hycon, REALNUM, densityfloor_x1_max, huge(zro))
  namelist_variable(hycon, REALNUM, densityfloor_x2_min, -huge(zro))
  namelist_variable(hycon, REALNUM, densityfloor_x2_max, huge(zro))
  namelist_variable(hycon, REALNUM, densityfloor_x3_min, -huge(zro))
  namelist_variable(hycon, REALNUM, densityfloor_x3_max, huge(zro))

  ! densityfloor_vkill: negative: no vkill.
  ! zero: send v components to zero.
  ! positive: conserve rho*v*densityfloor_vkill (one would be equivalent
  ! to conserving momentum) [at least at first, not implemented]
  namelist_variable(hycon, REALNUM, densityfloor_vkill, -one)

  !- - - - - - - - - - - - - - -
  ! Namelist outflowcon
  ! Variables related to the BC geometry, used by some of the interpolators
  ! Default values: all boundaries have outflow-type BCs.
  !
  ! Must be set to zero on any surface boundary on which the BC
  ! is *not* set to outflow.
  ! In this version, these flags are set to a constant value on
  ! each surface.  This must be rewritten for a simulation
  ! using mixed BCs, such as a jet inlet.
  !
  namelist_variable(outflowcon, LOGICAL, inner_i_outflow, .true.)
  namelist_variable(outflowcon, LOGICAL, outer_i_outflow, .true.)
  namelist_variable(outflowcon, LOGICAL, inner_j_outflow, .true.)
  namelist_variable(outflowcon, LOGICAL, outer_j_outflow, .true.)
  namelist_variable(outflowcon, LOGICAL, inner_k_outflow, .true.)
  namelist_variable(outflowcon, LOGICAL, outer_k_outflow, .true.)

  !- - - - - - - - - - - - - - -
  ! Grid motion control (namelist gcon)
  namelist_variable(gcon, LOGICAL, moving_grid1, .false.)
  namelist_variable(gcon, LOGICAL, moving_grid2, .false.)
  namelist_variable(gcon, LOGICAL, moving_grid3, .false.)
  namelist_variable(gcon, LOGICAL, allow_moving, .true.)

  !- - - - - - - - - - - - - - -
  ! Gravity control variables (namelist grvcon)
  !
  !  Gravitational self-potentials can be included in both 1-D and 2-D
  !  problems by solving the Poisson equation in the NEWGRAVITY subroutine.
  !  Point mass potentials are included directly in the momentum equation
  !  by using a non-zero value for the variable "gptmass".  Point mass
  !  potentials do not require defining gp, do not call the NEWGRAVITY
  !  subroutine, and are not included in the array "gp".  They are explicitly
  !  added to the momentum equation source terms in the routines STV1,
  !  STV2, and STV3.
  !
  !  gptmass   the gravitational constant times the point mass
  !  x1ptm     x1 location of the point mass
  !  x2ptm     x2 location
  !  x3ptm     x3 location

  namelist_variable(grvcon, REALNUM, gptmass, zro)
  namelist_variable(grvcon, REALNUM, x1ptm, zro)
  namelist_variable(grvcon, REALNUM, x2ptm, zro)
  namelist_variable(grvcon, REALNUM, x3ptm, zro)

  ! Newton gravity constant
  namelist_variable(grvcon, REALNUM, gnewton, one)

  ! gravity_algorithm choices:
  !   'none': no gp array, no enclosed mass self-gravity, no central mass
  !   'onlycentralmass': turns on evolving central mass, and nothing else
  !   'monopole': enclosed mass self-gravity. turns central mass on
  !   'sor': gp array, evolved using sor. turns central mass on
  !   'fixed': gp array, not evolving. no central mass
  namelist_variable(grvcon, CHARACTER(LEN=30), gravity_algorithm, 'none')

  ! The following two variables are turned on based on the
  ! gravity_algorithm choices.
  ! If this is turned on, an evolving central mass is defined, which grows
  ! as matter crosses the x1a(is) boundary.
  LOGICAL :: withcentralmass = .false.
  ! If this is turned on, there is a gp array
  LOGICAL :: withgp = .false.
  ! If this is turned on, there is an extended grid and extended arrays
  LOGICAL :: withextendedgrid = .false.

  ! Iteration control for 'sor'
  namelist_variable(grvcon, INTEGER, maximum_sor_iterations, 10000)
  namelist_variable(grvcon, REALNUM, graverr, 1e-6_pk)

  ! The 'sor' algorithm requires BCs.
  ! In 2D, they are calculated by direct sum.
  ! In 3D, there are two choices: direct sum, and multipolar expansion.
  !    These choices are selected here
  ! Maximum value of L in the multipolar expansion used for BCs in 3D,
  ! in the inner and in the outer direction separately.
  ! A negative value disables that multipolar expansion,
  ! enabling direct sum instead.
  namelist_variable(grvcon, INTEGER, lmax_lower, 5)
  namelist_variable(grvcon, INTEGER, lmax_upper, 5)
  !The values of lmax_lower and lmax_upper will be reset to -1 in case
  !they are not utilized.

  ! If either lmax_upper or lmax_lower are on, there is a need to extend
  ! the grid.  In case the grid is extended, there is the question of
  ! allowing the extended grid to override ghost zones wherever present,
  ! or not.  This variable controls that.
  namelist_variable(grvcon, LOGICAL, ghost_override, .false.)

  !- - - - - - - - - - - - - - -
  ! Equation of state variables (namelist eqos)
  !
  !  gamma     ratio of specific heats
  !  ciso      isothermal sound speed
  !  itote     =0 => solve the internal energy equation (positive definite
  !                  pressures but energy not strictly conserved)
  !            =1 => solve the total energy equation (energy strictly
  !                  conserved but pressures not positive definite)
  !  Note that with either ienergy == 0 or imhd /= 0, "itote" must be 0.
  !  itote==1 is also not compatible with some of the with__ options which
  !  set certain fields to zero
  !
  !  rho_stiffen : with ienergy==-1, it is used to set the
  !                the coefficient K of the polytropic EOS
  !                p=K*d**gamma as K=ciso**2 / rho_stiffen**(gamma-1)
  !
  !  eoskind     : with ienergy==-1, it is used to set the kind of EOS
  !            =1 : choice of power laws: exponent 1 for rho<rho_stiffen,
  !                 gamma for larger rho
  !            =2 : sum of the two power laws
  !            =3 : polytropic only
  !            =0 : isothermal only
  !            =4 : Locally isothermal.
  !                 EOS based on a tracer field and the ciso and ciso1 speeds
  !                 ciso^2 corresponds to the value 0 of the tracer, ciso1^2
  !                 corresponds to the value 1 of the tracer

  namelist_variable(eqos, REALNUM, gamma, five/three)
  namelist_variable(eqos, REALNUM, ciso, one)
  namelist_variable(eqos, INTEGER, itote, 0)
  namelist_variable(eqos, REALNUM, rho_stiffen, one)
  namelist_variable(eqos, INTEGER, eoskind, 1)
  namelist_variable(eqos, REALNUM, ciso1, one)

  !- - - - - - - - - - - - - - -
  ! Data dump variables (namelist dumpcon)
  !  dtdump      physical (problem) time interval between data dumps
  !  clockdump_dt   wall clock time interval between safety data dumps
  !  clockdump_policy: 0: no clock dumps
  !                    1: later dumps overwrite previous ones
  !                    2: continuous clock dump production

  namelist_variable(dumpcon, REALNUM, dtdump, zro)
  namelist_variable(dumpcon, LOGICAL, tdump_initial_used, .false.)
  namelist_variable(dumpcon, REALNUM, tdump_initial, zro)
  namelist_variable(dumpcon, CHARACTER(LEN=filenamelength), outputprefix, 'z_r_')
  namelist_variable(dumpcon, INTEGER, dumpnamelength, 5)

  namelist_variable(dumpcon, REALNUM, clockdump_dt, 3600.0_pk)
  namelist_variable(dumpcon, CHARACTER(LEN=filenamelength), clockdump_prefix, 'z_clock_')
  namelist_variable(dumpcon, INTEGER, clockdump_policy, 2)

END MODULE zeus_variables_fixed_namelists
