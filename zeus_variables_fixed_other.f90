#include "macros"
MODULE zeus_variables_fixed_other
  USE zeus_variables_fixed_hardwired
  IMPLICIT NONE
  SAVE

  ! These are variables that become fixed after initialization is done, but
  ! which currently do not belong either to the namelists, to the reserved
  ! set of hardwired numerical constants, or to the grid definition.

  !=============================================================
  ! Node identification and location

  INTEGER :: node                        ! Process ID
  INTEGER, PARAMETER :: rootnode = 0     ! Root process ID

  ! Process ID, in character form, convenient for I/O:
#ifdef MPI
  ! CHARACTER(LEN="_a234") ) :: nodename="_a234" ! A fanciful token value
  ! Commented out because some compilers do not accept it.
  ! Pity having to count characters, though.
  CHARACTER(LEN=5) :: nodename = "_a234" ! A fanciful token value
#else /* MPI */
  CHARACTER(LEN=0) :: nodename = ""
#endif /* MPI */

  INTEGER node1, node2, node3   ! i,j,k coordinates of the current tile

  INTEGER nodeup1, nodeup2, nodeup3 ! Process ID of the six neighbor tiles
  INTEGER nodedn1, nodedn2, nodedn3 !

  ! Is this the first *logical* tile? (taking into account periodicity)
  LOGICAL l_firstnode1, l_firstnode2, l_firstnode3
  ! Is this the last logical tile? (taking into account periodicity)
  LOGICAL l_lastnode1, l_lastnode2, l_lastnode3

  ! Is this the first grid tile? (w/o taking into account periodicity)
  LOGICAL lg_firstnode1, lg_firstnode2, lg_firstnode3
  ! Is this the last grid tile? (w/o taking into account periodicity)
  LOGICAL lg_lastnode1, lg_lastnode2, lg_lastnode3

  ! Arrays collecting the above information
  LOGICAL l_firstnodew(3, 2)

  !=============================================================
  !  Timestep variables
  REALNUM :: dtmin      ! Minimum dt expected, used to detect "hot zones"

  !Assume less than 1000=10**3 characters when formatting
  ! ntimestepname and nodename.
  !Assuming less than 10 could be risky.
  INTEGER, PARAMETER :: formatlength = 2*3 + 2
  CHARACTER(LEN=*), PARAMETER, PRIVATE :: iformat = 'i3.3'
  CHARACTER(LEN=*), PARAMETER :: format_format = &
                                 '(a,'//iformat//',a,'//iformat//',a)'

END MODULE zeus_variables_fixed_other
