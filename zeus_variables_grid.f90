#include "macros"
MODULE zeus_variables_grid
  IMPLICIT NONE
  SAVE
  !=============================================================
  ! The variables in this module cannot change after initialization
  ! is complete (unless NEWGRID is activated).
  !=============================================================

  ! Variables taken out of the namelist

  !=============================================================
  ! Grid indices

  ! Number of ghost zones to each of the two sides
  ! nghosts=3 is the only currently (2004/02/19) tested value
  ! namelist_variable(arraycon, INTEGER, nghosts, 3)
  INTEGER, PARAMETER :: nghosts = 3

  !Derived variables controlling array size
  INTEGER :: inac, jnac, knac

  ! Array carrying the values of inac, jnac, knac
  INTEGER :: iwnac(3)
  !End point for multi-dimensional 1-D arrays,
  ! currently (2007/04/04) used only in a few routines related to MoC.
  INTEGER :: ijknea

  !The following indices point to specific locations inside the grid
  !Explanation:
  !isa, iea: total array extent, from lowest to highest bound.
  !          To be used in declarations and allocation statements.
  !ione: set to 1 if the grid x1 is active, to 0 if it is symmetric
  !is: start of active zones
  !ie: end of active zones
  !p1, p2, m1, m2: obvious meaning
  !isg, ieg: beginning and end of ghost zones.
  !          The interval isg:ieg brackets the full
  !          computational volume, including both active and ghost zones
  !isgo: isg+ione
  !iego: ieg-ione
  !
  !The letters i, j, k are the directional part of each index name
  !A letter p at the end of the name refers to the parallel
  !(multiprocessor) grid, spanning all processes in a given direction.

  !Regular grid

  INTEGER :: is, ism1, ism2, isg, isgo
  INTEGER :: ie, iep1, iep2, ieg, iego, iea

  INTEGER :: js, jsm1, jsm2, jsg, jsgo
  INTEGER :: je, jep1, jep2, jeg, jego, jea

  INTEGER :: ks, ksm1, ksm2, ksg, ksgo
  INTEGER :: ke, kep1, kep2, keg, kego, kea

  INTEGER  :: ione, jone, kone
  REALNUM  :: onei, onej, onek

  !Multiprocessor grid

  INTEGER :: isp, ism1p, ism2p, isgp, isgop
  INTEGER :: iep, iep1p, iep2p, iegp, iegop, ieap

  INTEGER :: jsp, jsm1p, jsm2p, jsgp, jsgop
  INTEGER :: jep, jep1p, jep2p, jegp, jegop, jeap

  INTEGER :: ksp, ksm1p, ksm2p, ksgp, ksgop
  INTEGER :: kep, kep1p, kep2p, kegp, kegop, keap

  !=============================================================
  ! Grid arrays

  ! The long arrays used in parallel work
  ! These arrays contain all of the grid extent,
  ! including those parts that are not current in the local tile
  ! of a given process.  This is sometimes useful.
  REALNUM, ALLOCATABLE, DIMENSION(:), TARGET :: &
    x1ap, dx1ap, dvl1ap, x1bp, dx1bp, dvl1bp, x1apl
  REALNUM, ALLOCATABLE, DIMENSION(:) :: &
    x2ap, dx2ap, dvl2ap, x2bp, dx2bp, dvl2bp, x2apl, &
    x3ap, dx3ap, dvl3ap, x3bp, dx3bp, dvl3bp, x3apl

  ! The regular grid arrays
  !
  ! x1a: the main a-grid, located at the beginning of each zone (edge-centered)
  ! x1b: the main b-grid, located at the center of each zone (zone-centered)
  ! dx1a: differences of the x1a grid, zone-centered location; all other
  !       arrays having the letter "a" as part of their name, are edge-centered
  ! dx1b: differences of the x1b grid, edge-centered location; all other
  !       arrays having the letter "b" as part of their name, are zone-centered
  ! dvl1a: volume elements (edge-centered)
  ! g2a, g31a, dg2ad1, dg31ad1: geometry factors
  !
  ! Array names ending in "h" and "n" refer to half and full steps
  ! in array modification, which is relevant only if moving grids are active.
  ! Currently (2004/02/19) they are not, and so the "h" and "n" grids have
  ! the same values as the other arrays.
  !
  ! Array names ending in "i" have the inverse value.
  !
  ! vg1: velocity of motion of the moving grids, currently (2004/02/19)
  !      irrelevant, until grid motion is reactivated.

  REALNUM, ALLOCATABLE, TARGET, DIMENSION(:) :: &
    x1a, x1ah, x1an, &
    dx1a, dx1ah, dx1an, &
    dvl1a, dvl1ah, dvl1an, &
    g2a, g2ah, g2an, &
    g31a, g31ah, g31an, &
    dg2ad1, &
    dg31ad1
  REALNUM, ALLOCATABLE, TARGET, DIMENSION(:) :: &
    x1ai, x1ahi, x1ani, &
    dx1ai, dx1ahi, dx1ani, &
    dvl1ai, dvl1ahi, dvl1ani, &
    g2ai, g2ahi, g2ani, &
    g31ai, g31ahi, g31ani
  REALNUM, ALLOCATABLE, TARGET, DIMENSION(:) :: &
    x1b, x1bh, x1bn, &
    dx1b, dx1bh, dx1bn, &
    dvl1b, dvl1bh, dvl1bn, &
    g2b, g2bh, g2bn, &
    g31b, g31bh, g31bn, &
    dg2bd1, &
    dg31bd1
  REALNUM, ALLOCATABLE, TARGET, DIMENSION(:) :: &
    x1bi, x1bhi, x1bni, &
    dx1bi, dx1bhi, dx1bni, &
    dvl1bi, dvl1bhi, dvl1bni, &
    g2bi, g2bhi, g2bni, &
    g31bi, g31bhi, g31bni
  REALNUM, ALLOCATABLE, TARGET, DIMENSION(:) :: &
    vg1, x1al

  REALNUM, ALLOCATABLE, DIMENSION(:) :: &
    x2a, x2ah, x2an, &
    dx2a, dx2ah, dx2an, &
    dvl2a, dvl2ah, dvl2an, &
    g32a, g32ah, g32an, &
    dg32ad2
  REALNUM, ALLOCATABLE, DIMENSION(:) :: &
    x2ai, x2ahi, x2ani, &
    dx2ai, dx2ahi, dx2ani, &
    dvl2ai, dvl2ahi, dvl2ani, &
    g32ai, g32ahi, g32ani
  REALNUM, ALLOCATABLE, DIMENSION(:) :: &
    x2b, x2bh, x2bn, &
    dx2b, dx2bh, dx2bn, &
    dvl2b, dvl2bh, dvl2bn, &
    g32b, g32bh, g32bn, &
    dg32bd2
  REALNUM, ALLOCATABLE, DIMENSION(:) :: &
    x2bi, x2bhi, x2bni, &
    dx2bi, dx2bhi, dx2bni, &
    dvl2bi, dvl2bhi, dvl2bni, &
    g32bi, g32bhi, g32bni
  REALNUM, ALLOCATABLE, DIMENSION(:) :: &
    vg2, x2al

  REALNUM, ALLOCATABLE, DIMENSION(:) :: &
    x3a, x3ah, x3an, &
    dx3a, dx3ah, dx3an, &
    dvl3a, dvl3ah, dvl3an
  REALNUM, ALLOCATABLE, DIMENSION(:) :: &
    x3ai, x3ahi, x3ani, &
    dx3ai, dx3ahi, dx3ani, &
    dvl3ai, dvl3ahi, dvl3ani
  REALNUM, ALLOCATABLE, DIMENSION(:) :: &
    x3b, x3bh, x3bn, &
    dx3b, dx3bh, dx3bn, &
    dvl3b, dvl3bh, dvl3bn
  REALNUM, ALLOCATABLE, DIMENSION(:) :: &
    x3bi, x3bhi, x3bni, &
    dx3bi, dx3bhi, dx3bni, &
    dvl3bi, dvl3bhi, dvl3bni
  REALNUM, ALLOCATABLE, DIMENSION(:) :: &
    vg3, x3al
!
! Extended grid (used for gravity)
!
  INTEGER :: isap_e, ieap_e
  INTEGER :: isa_e, iea_e
  INTEGER :: is_e, ie_e
  INTEGER :: isg_e, ieg_e

  REALNUM, POINTER, DIMENSION(:) :: &
    x1ap_e, dx1ap_e, x1bp_e, dx1bp_e
  REALNUM, POINTER, DIMENSION(:) :: &
    x1a_e, dx1a_e, x1b_e, dx1b_e, &
    x1ai_e, dx1ai_e, x1bi_e, dx1bi_e

END MODULE zeus_variables_grid
